from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.await_expression import (
    build_await_expression_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    childs = match_ast(args.ast_graph, args.n_id, "await")
    expr_id = childs.get("__0__")
    if not expr_id:
        expr_id = adj_ast(args.ast_graph, args.n_id)[-1]
    return build_await_expression_node(args, expr_id)
