from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.rest_pattern import (
    build_rest_pattern_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    match = match_ast(args.ast_graph, args.n_id, "*", "**")
    identifier_id = match.get("__0__")
    if not identifier_id:
        identifier_id = adj_ast(args.ast_graph, args.n_id)[0]
    return build_rest_pattern_node(args, identifier_id)
