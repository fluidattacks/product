from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.if_statement import (
    build_if_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    condition_id = adj_ast(args.ast_graph, args.n_id)[-1]
    return build_if_node(args, condition_id)
