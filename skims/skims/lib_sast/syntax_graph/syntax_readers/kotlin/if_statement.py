from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.if_statement import (
    build_if_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    child_ids = adj_ast(graph, args.n_id)
    condition_id = child_ids[2]
    if (
        graph.nodes[condition_id]["label_type"] == "parenthesized_expression"
        and (childs := adj_ast(graph, condition_id))
        and len(childs) > 1
    ):
        condition_id = childs[1]

    true_id = child_ids[4]
    false_id = None
    if len(child_ids) == 7:
        false_id = child_ids[6]
    return build_if_node(args, condition_id, true_id, false_id)
