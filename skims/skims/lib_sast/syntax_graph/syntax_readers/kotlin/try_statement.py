from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.try_statement import (
    build_try_statement_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_d,
    match_ast_group_d,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    block_id = match_ast_d(graph, args.n_id, "block")
    if not block_id:
        block_id = adj_ast(graph, args.n_id)[-1]

    catch_blocks = match_ast_group_d(args.ast_graph, args.n_id, "catch_block")
    finally_block = match_ast_d(graph, args.n_id, "label_field_finally_block")

    return build_try_statement_node(args, block_id, catch_blocks, finally_block, None)
