from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.method_invocation import (
    build_method_invocation_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_d,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    expr_id = adj_ast(graph, args.n_id)[0]
    expr = node_to_str(graph, expr_id)
    args_id = match_ast_d(graph, args.n_id, "value_arguments")
    return build_method_invocation_node(args, expr, expr_id, args_id, None)
