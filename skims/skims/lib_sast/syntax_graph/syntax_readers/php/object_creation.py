from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.object_creation import (
    build_object_creation_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils import (
    graph as g,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    nodes = graph.nodes

    c_ids = g.match_ast(graph, args.n_id, "name", "arguments", "qualified_name")
    name = ""

    if name_n_id := c_ids.get("name"):
        name = nodes[name_n_id].get("label_text")
    elif name_n_id := c_ids.get("qualified_name"):
        name = node_to_str(graph, name_n_id).replace("\\", "/")

    arguments_id = c_ids.get("arguments")

    return build_object_creation_node(args, name, arguments_id, None)
