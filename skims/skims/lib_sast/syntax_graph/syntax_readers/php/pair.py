from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.pair import (
    build_pair_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils import (
    graph as g,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    c_ids = [
        _id for _id in g.adj_ast(graph, args.n_id) if graph.nodes[_id].get("label_type") != "=>"
    ]
    key_id, value_id, *_ = c_ids
    return build_pair_node(args, key_id, value_id)
