from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.parameter import (
    build_parameter_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    match_ast_d,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    n_attrs = graph.nodes[args.n_id]

    name_n_id = match_ast_d(graph, n_attrs.get("label_field_name"), "name")

    var_name = graph.nodes[name_n_id].get("label_text")

    return build_parameter_node(args=args, variable=var_name, variable_type=None, value_id=None)
