from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.import_global import (
    build_import_global_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils import (
    graph as g,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    expression: str = ""
    c_ids = g.match_ast_group(
        graph,
        args.n_id,
        "string",
        "encapsed_string",
        "variable_name",
        depth=-1,
    )
    if c_ids:
        if exp_n_ids := c_ids.get("string") or (exp_n_ids := c_ids.get("encapsed_string")):
            expression = node_to_str(graph, exp_n_ids[0])[1:-1]
        if var_id := c_ids.get("variable_name"):
            args.syntax_graph.add_edge(
                args.n_id,
                args.generic(args.fork_n_id(var_id[0])),
                label_ast="AST",
            )
            expression = node_to_str(graph, var_id[0])

    return build_import_global_node(args, expression, set(), None)
