from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.return_statement import (
    build_return_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils import (
    graph as g,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    ignored_labels = {"return", ";"}
    val_id: NId | None = None
    c_ids = [
        _id
        for _id in g.adj_ast(graph, args.n_id)
        if graph.nodes[_id].get("label_type") not in ignored_labels
    ]
    if c_ids:
        val_id = c_ids[0]

    return build_return_node(args, val_id)
