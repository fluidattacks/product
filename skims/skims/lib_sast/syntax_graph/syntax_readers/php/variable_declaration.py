from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.variable_declaration import (
    build_variable_declaration_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils import (
    graph as g,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    nodes = graph.nodes

    if name_n_id := g.get_node_by_path(
        graph,
        args.n_id,
        "property_element",
        "variable_name",
        "name",
    ):
        var_name = nodes[name_n_id].get("label_text")
    else:
        var_name = node_to_str(graph, args.n_id)

    val_id: NId | None = None
    if (
        val_p_id := g.get_node_by_path(
            graph,
            args.n_id,
            "property_element",
            "property_initializer",
        )
    ) and (c_ids := g.match_ast(graph, val_p_id)):
        val_id = c_ids.get("__1__")

    return build_variable_declaration_node(args, var_name, None, val_id)
