from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.pair import (
    build_pair_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils import (
    graph as g,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph

    p_id = g.pred_ast(graph, args.n_id)[0]
    c_ids = [_id for _id in g.adj_ast(graph, p_id) if _id != args.n_id]

    key_id = c_ids[0]
    value_id = c_ids[-1]

    return build_pair_node(args, key_id, value_id)
