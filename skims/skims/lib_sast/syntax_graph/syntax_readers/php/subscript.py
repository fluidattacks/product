from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.element_access import (
    build_element_access_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils import (
    graph as g,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    c_ids = g.match_ast(graph, args.n_id)
    expr_id = c_ids.get("__0__") or args.n_id
    arguments_id = None

    if len(c_ids) == 4:
        arguments_id = c_ids.get("__2__")

    return build_element_access_node(args, expr_id, arguments_id)
