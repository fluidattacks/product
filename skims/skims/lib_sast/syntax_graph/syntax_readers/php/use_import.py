from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.import_global import (
    build_import_global_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils import (
    graph as g,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    expression: str = ""
    alias: str | None = None

    if (exp_n_id := g.match_ast_d(graph, args.n_id, "namespace_use_clause")) and (
        name_n_id := g.match_ast(graph, exp_n_id).get("__0__")
    ):
        expression = node_to_str(graph, name_n_id).replace("\\", "/")

        if alias_n_id := g.get_node_by_path(graph, exp_n_id, "namespace_aliasing_clause", "name"):
            alias = graph.nodes[alias_n_id].get("label_text")

    return build_import_global_node(args, expression, set(), alias)
