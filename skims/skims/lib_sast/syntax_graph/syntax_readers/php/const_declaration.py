from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.variable_declaration import (
    build_variable_declaration_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    match_ast,
    match_ast_d,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    nodes = graph.nodes

    element_id = match_ast_d(graph, args.n_id, "const_element") or args.n_id
    c_ids = match_ast(graph, element_id)

    var_id = c_ids.get("__0__")
    var_name = nodes[var_id].get("label_text") or node_to_str(graph, args.n_id)
    val_id = c_ids.get("__2__")

    return build_variable_declaration_node(args, var_name, None, val_id)
