from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.string_literal import (
    build_string_literal_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils import (
    graph as g,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    value = node_to_str(graph, args.n_id)[1:-1]

    c_ids = (
        _id
        for _id in g.adj_ast(graph, args.n_id)
        if graph.nodes[_id].get("label_type") in {"variable_name", "subscript_expression"}
    )

    return build_string_literal_node(args, value, c_ids)
