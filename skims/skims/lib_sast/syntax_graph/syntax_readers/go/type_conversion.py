from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.unary_expression import (
    build_unary_expression_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    n_attrs = graph.nodes[args.n_id]
    value_id = n_attrs["label_field_operand"]
    operand = node_to_str(graph, n_attrs["label_field_type"])

    return build_unary_expression_node(args, operand, value_id)
