from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.import_statement import (
    build_import_statement_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    node_attrs: dict[str, str] = {
        "expression": node_to_str(args.ast_graph, args.n_id),
    }
    return build_import_statement_node(args, node_attrs)
