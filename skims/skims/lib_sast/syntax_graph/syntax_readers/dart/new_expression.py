from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.new_expression import (
    build_new_expression_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_d,
)


def reader(args: SyntaxGraphArgs) -> NId:
    childs = adj_ast(args.ast_graph, args.n_id)
    args_id = match_ast_d(args.ast_graph, args.n_id, "arguments")
    const_id = childs[1]
    return build_new_expression_node(args, const_id, args_id)
