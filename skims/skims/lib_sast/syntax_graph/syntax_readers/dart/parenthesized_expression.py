from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.parenthesized_expression import (
    build_parenthesized_expression_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    expr_id = adj_ast(args.ast_graph, args.n_id)[1]
    return build_parenthesized_expression_node(args, expr_id)
