from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.method_invocation import (
    build_method_invocation_node,
)
from lib_sast.syntax_graph.syntax_nodes.string_literal import (
    build_string_literal_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    match_ast_d,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph

    if identifier_id := match_ast_d(graph, args.n_id, "identifier"):
        params_id = match_ast_d(graph, args.n_id, "formal_parameter_list")
        name = node_to_str(graph, identifier_id)
        return build_method_invocation_node(args, name, identifier_id, params_id, None)

    return build_string_literal_node(args, node_to_str(graph, args.n_id))
