from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.string_literal import (
    build_string_literal_node,
)
from lib_sast.syntax_graph.syntax_nodes.variable_declaration import (
    build_variable_declaration_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    match_ast,
    match_ast_d,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    definition_id = match_ast_d(graph, args.n_id, "initialized_variable_definition")
    if not definition_id:
        return build_string_literal_node(args, args.n_id)

    var_type = (
        fr_child
        if (fr_child := match_ast(graph, args.n_id).get("__0__"))
        and graph.nodes[fr_child]["label_type"] == "type_identifier"
        else None
    )

    var_id = graph.nodes[definition_id]["label_field_name"]
    var_name = node_to_str(graph, var_id)

    value_id = graph.nodes[definition_id].get("label_field_value")

    return build_variable_declaration_node(args, var_name, var_type, value_id)
