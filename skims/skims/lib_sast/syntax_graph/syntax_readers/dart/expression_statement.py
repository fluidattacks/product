from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.assignment import (
    build_assignment_node,
)
from lib_sast.syntax_graph.syntax_nodes.expression_statement import (
    build_expression_statement_node,
)
from lib_sast.syntax_graph.syntax_nodes.method_invocation import (
    build_method_invocation_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
    get_ast_children,
    match_ast,
    match_ast_d,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    c_ids = adj_ast(args.ast_graph, args.n_id)
    ignored_types = {";", "(", ")", "super", "comment"}
    filtered_ids = [
        _id for _id in c_ids if args.ast_graph.nodes[_id]["label_type"] not in ignored_types
    ]
    if (
        (sel := get_ast_children(graph, args.n_id, label_type="selector"))
        and len(sel) == 2
        and (select_1 := match_ast_d(graph, sel[0], "unconditional_assignable_selector"))
        and (select_2 := match_ast_d(graph, sel[1], "argument_part"))
    ):
        child_s1 = adj_ast(graph, select_1)
        expr_id = child_s1[1] if len(child_s1) > 1 else child_s1[0]
        expr = node_to_str(graph, filtered_ids[0]) + "." + node_to_str(graph, expr_id)
        args_id = match_ast_d(graph, select_2, "arguments")
        return build_method_invocation_node(
            args,
            expr,
            expr_id,
            args_id,
            {
                "object_id": filtered_ids[0],
            },
        )
    if (
        len(filtered_ids) == 1
        and args.ast_graph.nodes[filtered_ids[0]]["label_type"] == "assignment_expression"
    ):
        n_attr = graph.nodes[filtered_ids[0]]
        var_id = n_attr["label_field_left"]
        op_id = n_attr.get("label_field_operator")
        if (
            graph.nodes[var_id]["label_type"] == "assignable_expression"
            and len(adj_ast(graph, var_id)) == 1
        ):
            var_id = match_ast(graph, var_id).get("__0__")
        return build_assignment_node(
            args,
            var_id,
            n_attr["label_field_right"],
            graph.nodes[op_id]["label_text"],
        )

    return build_expression_statement_node(args, iter(filtered_ids))
