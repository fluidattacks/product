from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.switch_section import (
    build_switch_section_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_d,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    statements_id = match_ast_d(graph, args.n_id, "statements")
    if not statements_id:
        statements_id = adj_ast(graph, args.n_id)[-1]
    child_ids = adj_ast(graph, statements_id)

    val_id = match_ast_d(graph, args.n_id, "switch_pattern")
    if not val_id:
        val_id = adj_ast(graph, args.n_id)[0]
    case_expr = node_to_str(graph, val_id)

    return build_switch_section_node(args, case_expr, child_ids)
