from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.try_statement import (
    build_try_statement_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    block_id = graph.nodes[args.n_id].get("label_field_expr")
    if not block_id:
        block_id = adj_ast(graph, args.n_id)[0]
    return build_try_statement_node(args, block_id, None, None, None)
