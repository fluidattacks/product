from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.binary_operation import (
    build_binary_operation_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)


def reader(args: SyntaxGraphArgs) -> NId:
    left_id = args.ast_graph.nodes[args.n_id]["label_field_lhs"]
    right_id = args.ast_graph.nodes[args.n_id]["label_field_rhs"]
    operator_id = args.ast_graph.nodes[args.n_id]["label_field_op"]
    operator = args.ast_graph.nodes[operator_id]["label_text"]
    return build_binary_operation_node(args, operator, left_id, right_id)
