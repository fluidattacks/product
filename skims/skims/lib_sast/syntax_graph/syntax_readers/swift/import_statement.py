from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.import_global import (
    build_import_global_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils import (
    graph as g,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    expression = ""
    if identifier_n_id := g.match_ast_d(graph, args.n_id, "identifier"):
        expression = node_to_str(graph, identifier_n_id)
    return build_import_global_node(args, expression, set(), None)
