from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.parameter_list import (
    build_parameter_list_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    labels_to_ignore = {"(", ")", ","}

    c_ids = (
        _id
        for _id in adj_ast(graph, args.n_id)
        if graph.nodes[_id]["label_type"] not in labels_to_ignore
    )

    return build_parameter_list_node(
        args,
        c_ids,
    )
