from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.binary_operation import (
    build_binary_operation_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    n_attrs = graph.nodes[args.n_id]

    operator_str = node_to_str(graph, n_attrs["label_field_operator"])
    left_id = n_attrs["label_field_left"]
    right_id = n_attrs["label_field_right"]

    return build_binary_operation_node(args, operator_str, left_id, right_id)
