from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.import_statement import (
    build_import_statement_node,
)
from lib_sast.syntax_graph.syntax_nodes.method_invocation import (
    build_method_invocation_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    get_nodes_by_path,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def process_ruby_imports(args: SyntaxGraphArgs, arguments_id: NId, method_name: str) -> NId:
    graph = args.ast_graph
    module_nodes = set(
        get_nodes_by_path(graph, arguments_id, (), "string", "string_content"),
    )
    import_attrs: list[dict[str, str]] = []
    for module_id in module_nodes:
        module_expression = node_to_str(graph, module_id)
        import_attrs.append(
            {
                "corrected_n_id": module_id,
                "method_name": method_name,
                "expression": module_expression,
            },
        )
    return build_import_statement_node(args, *import_attrs)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    node_data = graph.nodes[args.n_id]

    method_id = node_data["label_field_method"]
    method_name = node_to_str(graph, method_id)
    arguments_id = node_data.get("label_field_arguments")

    ruby_imports = {"require", "require_relative", "load"}
    if arguments_id and method_name in ruby_imports:
        return process_ruby_imports(args, arguments_id, method_name)

    object_id = node_data.get("label_field_receiver")
    obj_dict = {"object_id": object_id} if object_id else None

    return build_method_invocation_node(args, method_name, method_id, arguments_id, obj_dict)
