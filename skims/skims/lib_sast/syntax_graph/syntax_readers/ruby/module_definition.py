from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.namespace import (
    build_namespace_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    n_attrs = graph.nodes[args.n_id]

    name_id = n_attrs["label_field_name"]
    name_str = node_to_str(graph, name_id)
    block_id = n_attrs.get("label_field_body")

    return build_namespace_node(args, name_str, block_id)
