from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.pair import (
    build_pair_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_d,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    key_id = graph.nodes[args.n_id].get("label_field_key") or match_ast_d(
        graph,
        args.n_id,
        "identifier",
    )
    if not key_id:
        key_id = adj_ast(graph, args.n_id)[0]

    value_id = graph.nodes[args.n_id].get("label_field_val") or match_ast_d(
        graph,
        args.n_id,
        "expression",
    )
    if not value_id:
        value_id = adj_ast(graph, args.n_id)[-1]

    return build_pair_node(args, key_id, value_id)
