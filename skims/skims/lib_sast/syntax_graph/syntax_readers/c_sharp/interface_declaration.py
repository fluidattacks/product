from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.class_decl import (
    build_class_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    match_ast,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    n_attrs = graph.nodes[args.n_id]
    name_id = n_attrs["label_field_name"]
    name = node_to_str(graph, name_id)

    block_id = n_attrs["label_field_body"]
    params_id = n_attrs.get("label_field_type_parameters", [])
    if params_id:
        params_id = [params_id]
        if not match_ast(graph, params_id[0], "(", ")").get("__0__"):
            params_id = []

    return build_class_node(args, name, block_id, params_id)
