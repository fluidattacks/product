from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.unary_expression import (
    build_unary_expression_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    childs = adj_ast(graph, args.n_id)
    operator = graph.nodes[childs[1]]["label_text"]
    return build_unary_expression_node(args, operator, childs[0])
