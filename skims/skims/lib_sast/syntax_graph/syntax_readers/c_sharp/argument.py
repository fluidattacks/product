from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.constants import (
    C_SHARP_EXPRESSION,
)
from lib_sast.syntax_graph.syntax_nodes.argument import (
    build_argument_node,
)
from lib_sast.syntax_graph.syntax_nodes.named_argument import (
    build_named_argument_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph

    match = match_ast(graph, args.n_id, ":")
    if (
        match
        and (var_id := graph.nodes[args.n_id].get("label_field_name"))
        and (val_id := match.get("__1__"))
    ):
        arg_name = node_to_str(graph, var_id)
        return build_named_argument_node(args, arg_name, val_id)

    valid_childs = [
        _id
        for _id in adj_ast(graph, args.n_id)
        if graph.nodes[_id]["label_type"] in C_SHARP_EXPRESSION.union({"declaration_expression"})
    ]

    return build_argument_node(args, iter(valid_childs))
