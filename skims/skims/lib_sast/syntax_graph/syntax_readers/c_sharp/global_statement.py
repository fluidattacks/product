from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.file import (
    build_file_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    c_ids = adj_ast(args.ast_graph, args.n_id)
    return build_file_node(args, iter(c_ids))
