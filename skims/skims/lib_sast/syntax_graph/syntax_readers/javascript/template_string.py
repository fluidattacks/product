from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.string_literal import (
    build_string_literal_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_group_d,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    text = node_to_str(graph, args.n_id)
    ints = match_ast_group_d(graph, args.n_id, "template_substitution")
    int_subs = [adj_ast(graph, _id)[1] for _id in ints]
    if len(int_subs) > 0:
        return build_string_literal_node(args, text, iter(int_subs))

    return build_string_literal_node(args, text)
