from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.else_clause import (
    build_else_clause_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    childs = match_ast(graph, args.n_id, "else")
    body_id = childs.get("__0__")

    if body_id and graph.nodes[body_id]["label_type"] == "expression_statement":
        body_id = match_ast(graph, body_id).get("__0__")

    if body_id and graph.nodes[body_id]["label_type"] == "parenthesized_expression":
        body_id = match_ast(graph, body_id).get("__1__")

    if not body_id:
        body_id = adj_ast(graph, args.n_id)[-1]

    return build_else_clause_node(args, body_id)
