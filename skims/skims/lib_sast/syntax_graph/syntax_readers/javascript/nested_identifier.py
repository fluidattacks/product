from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.symbol_lookup import (
    build_symbol_lookup_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    match_ast_group_d,
)
from lib_sast.utils.graph.text_nodes import (
    n_ids_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    identifiers = match_ast_group_d(graph, args.n_id, "identifier")
    var_name = n_ids_to_str(graph, (_id for _id in identifiers), ".") if identifiers else ""
    return build_symbol_lookup_node(args, var_name)
