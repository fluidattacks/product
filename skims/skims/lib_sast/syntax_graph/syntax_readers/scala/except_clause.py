from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.catch_clause import (
    build_catch_clause_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    match_ast_d,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    block_id = match_ast_d(graph, args.n_id, "case_block")
    return build_catch_clause_node(args, block_id)
