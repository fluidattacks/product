from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.try_statement import (
    build_try_statement_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    get_nodes_by_path,
    match_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    block_node = args.ast_graph.nodes[args.n_id]["label_field_body"]
    childs = match_ast(
        args.ast_graph,
        args.n_id,
        "finally_clause",
    )
    catch_blocks = get_nodes_by_path(
        args.ast_graph,
        args.n_id,
        (),
        "catch_clause",
        "case_block",
        "case_clause",
    )
    try_block = childs.get("finally_clause")
    return build_try_statement_node(args, block_node, catch_blocks, try_block, None)
