from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.catch_clause import (
    build_catch_clause_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    match_ast_d,
)


def reader(args: SyntaxGraphArgs) -> NId:
    block = args.ast_graph.nodes[args.n_id]["label_field_body"]
    param_id = match_ast_d(args.ast_graph, args.n_id, "catch_formal_parameter")
    return build_catch_clause_node(args, block, param_id)
