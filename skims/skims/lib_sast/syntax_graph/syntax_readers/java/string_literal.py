from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.string_literal import (
    build_string_literal_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    value = args.ast_graph.nodes[args.n_id].get("label_text")
    if not value:
        value = node_to_str(args.ast_graph, args.n_id)
    return build_string_literal_node(args, value)
