from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.parameter import (
    build_parameter_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    match_ast_group_d,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    param_node = graph.nodes[args.n_id]
    type_id = param_node["label_field_type"]
    identifier_id = param_node["label_field_name"]

    var_type = node_to_str(graph, type_id)
    var_name = node_to_str(graph, identifier_id)

    c_ids = match_ast_group_d(graph, args.n_id, "modifiers")

    return build_parameter_node(
        args=args,
        variable=var_name,
        variable_type=var_type,
        value_id=None,
        c_ids=iter(c_ids),
    )
