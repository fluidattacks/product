from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.array_node import (
    build_array_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast,
    match_ast_d,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    arr_value = graph.nodes[args.n_id].get("label_field_value") or adj_ast(graph, args.n_id)[-1]

    if (
        (dim_expr := match_ast_d(graph, args.n_id, "dimensions_expr"))
        and (childs := match_ast(graph, dim_expr, "[", "]"))
        and childs.get("__0__")
    ):
        arr_value = childs["__0__"]

    return build_array_node(args, (arr_value,))
