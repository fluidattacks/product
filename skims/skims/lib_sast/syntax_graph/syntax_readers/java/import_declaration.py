from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.syntax_nodes.import_global import (
    build_import_global_node,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)
from lib_sast.utils import (
    graph as g,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    alias: str | None = None
    expression: str = ""

    if identifier := g.match_ast_d(graph, args.n_id, "scoped_identifier"):
        expression = node_to_str(graph, identifier)
    if g.match_ast_d(graph, args.n_id, "asterisk"):
        expression += ".*"
    if metadata_node := args.syntax_graph.nodes.get("0"):
        metadata_node["imports"].append(expression)

    return build_import_global_node(args, expression, set(), alias)
