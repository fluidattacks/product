import json
import os
import sys
from collections.abc import (
    Iterator,
)
from ctypes import (
    c_void_p,
    cdll,
)
from itertools import (
    count,
)
from pathlib import Path

import ctx
import tree_sitter_c_sharp
import tree_sitter_go
import tree_sitter_java
import tree_sitter_javascript
import tree_sitter_json
import tree_sitter_kotlin
import tree_sitter_php
import tree_sitter_python
import tree_sitter_ruby
import tree_sitter_scala
import tree_sitter_typescript
import tree_sitter_yaml
from ctx import (
    TREE_SITTER_PARSERS,
)
from lib_sast.sast_model import (
    SUPPORTED_MULTIFILE,
    Graph,
    GraphDB,
    GraphShard,
    GraphShardMetadataLanguage,
    decide_language,
)
from lib_sast.syntax_cfg.generate import (
    add_syntax_cfg,
)
from lib_sast.syntax_graph.generate import (
    build_syntax_graph,
)
from lib_sast.syntax_graph.parse_graph.multifile_ctx import (
    set_context_by_lang,
)
from lib_sast.utils.custom_parsers import (
    load_java_properties,
)
from lib_sast.utils.graph import (
    copy_ast,
    styles,
    to_svg,
)
from tree_sitter import (
    Language,
    Node,
    Parser,
    Tree,
)
from utils.fs import (
    get_file_content_block,
    safe_sync_get_file_raw_content,
)
from utils.logs import (
    log_blocking,
    log_to_remote_blocking,
)
from utils.string_handlers import (
    get_debug_path,
)


class ParsingError(Exception):
    pass


def lang_from_so(language: GraphShardMetadataLanguage) -> Language:
    so_library_path: str = os.path.join(  # noqa: PTH118
        TREE_SITTER_PARSERS,
        f"{language.value}.so",
    )
    lib = cdll.LoadLibrary(os.fspath(so_library_path))
    language_function = getattr(lib, f"tree_sitter_{language.value}")
    language_function.restype = c_void_p
    language_ptr = language_function()
    return Language(language_ptr)


PARSER_LANGUAGES: dict[GraphShardMetadataLanguage, Language] = {
    GraphShardMetadataLanguage.CSHARP: Language(tree_sitter_c_sharp.language()),
    GraphShardMetadataLanguage.GO: Language(tree_sitter_go.language()),
    GraphShardMetadataLanguage.JAVA: Language(tree_sitter_java.language()),
    GraphShardMetadataLanguage.JAVASCRIPT: Language(tree_sitter_javascript.language()),
    GraphShardMetadataLanguage.TYPESCRIPT: Language(tree_sitter_typescript.language_tsx()),
    GraphShardMetadataLanguage.JSON: Language(tree_sitter_json.language()),
    GraphShardMetadataLanguage.KOTLIN: Language(tree_sitter_kotlin.language()),
    GraphShardMetadataLanguage.PHP: Language(tree_sitter_php.language_php()),
    GraphShardMetadataLanguage.PYTHON: Language(tree_sitter_python.language()),
    GraphShardMetadataLanguage.RUBY: Language(tree_sitter_ruby.language()),
    GraphShardMetadataLanguage.SCALA: Language(tree_sitter_scala.language()),
    GraphShardMetadataLanguage.YAML: Language(tree_sitter_yaml.language()),
    # Not supported on pypi as of 12/24
    GraphShardMetadataLanguage.HCL: lang_from_so(GraphShardMetadataLanguage.HCL),
    GraphShardMetadataLanguage.SWIFT: lang_from_so(GraphShardMetadataLanguage.SWIFT),
    GraphShardMetadataLanguage.DART: lang_from_so(GraphShardMetadataLanguage.DART),
}

FIELDS_BY_LANGUAGE: dict[GraphShardMetadataLanguage, dict[str, tuple[str, ...]]] = {}


def _get_fields_by_language() -> None:
    for lang in GraphShardMetadataLanguage:
        if lang == GraphShardMetadataLanguage.NOT_SUPPORTED:
            continue

        path = Path(TREE_SITTER_PARSERS) / f"{lang.value}-fields.json"

        with path.open(encoding="utf-8") as file:
            FIELDS_BY_LANGUAGE[lang] = json.load(file)


_get_fields_by_language()


def hash_node(node: Node) -> int:
    return hash((node.end_point, node.start_point, node.type))


def _has_content_node(node: Node, language: GraphShardMetadataLanguage) -> bool:
    return (
        (
            language == GraphShardMetadataLanguage.CSHARP
            and node.type
            in {
                "character_literal",
                "predefined_type",
                "string_literal",
                "verbatim_string_literal",
            }
        )
        or (
            language == GraphShardMetadataLanguage.DART
            and node.type
            in {
                "list_literal",
                "set_or_map_literal",
                "string_literal",
            }
        )
        or (
            language == GraphShardMetadataLanguage.GO
            and node.type
            in {
                "interpreted_string_literal",
            }
        )
        or (
            language
            in {
                GraphShardMetadataLanguage.JAVASCRIPT,
                GraphShardMetadataLanguage.TYPESCRIPT,
            }
            and node.type
            in {
                "template_string",
                "regex",
            }
        )
        or (
            language == GraphShardMetadataLanguage.KOTLIN
            and node.type
            in {
                "string_literal",
            }
        )
        or (
            language == GraphShardMetadataLanguage.YAML
            and node.type
            in {
                "single_quote_scalar",
                "double_quote_scalar",
                "flow_mapping",
            }
        )
    )


def _is_node_terminal(node: Node, language: GraphShardMetadataLanguage) -> bool:
    return language == GraphShardMetadataLanguage.YAML and node.type in [
        "flow_mapping",
        "single_quote_scalar",
        "double_quote_scalar",
    ]


def _build_ast_graph(
    content: bytes,
    language: GraphShardMetadataLanguage,
    node: Node,
    counter: Iterator[str],
    graph: Graph,
    *,
    _edge_index: str | None = None,
    _parent: str | None = None,
    _parent_fields: dict[int, str] | None = None,
) -> Graph:
    if not isinstance(node, Node):
        raise NotImplementedError

    if node.has_error:
        raise ParsingError

    n_id = next(counter)
    raw_l, raw_c = node.start_point

    graph.add_node(n_id, label_l=raw_l + 1, label_c=raw_c + 1, label_type=node.type)

    if _parent is not None:
        graph.add_edge(_parent, n_id, label_ast="AST", label_index=_edge_index)

        # if the node is a parent field according node_type file, associate id
        # example node-types files at https://github.com/
        # tree-sitter/tree-sitter-c-sharp/blob/master/src/node-types.json
        # tree-sitter/tree-sitter-java/blob/master/src/node-types.json
        if field := (_parent_fields or {}).get(hash_node(node)):
            graph.nodes[_parent][f"label_field_{field}"] = n_id

    if not node.children or _has_content_node(node, language):
        # Extract the text from it
        node_content = content[node.start_byte : node.end_byte]
        graph.nodes[n_id]["label_text"] = node_content.decode("latin-1")

    if node.children and not _is_node_terminal(node, language):
        # It's not a final node, recurse
        for edge_index, child in enumerate(node.children):
            _build_ast_graph(
                content,
                language,
                child,
                counter,
                graph,
                _edge_index=str(edge_index),
                _parent=n_id,
                _parent_fields={
                    hash_node(child): fld
                    for fld in FIELDS_BY_LANGUAGE[language].get(node.type, ())
                    for child in [node.child_by_field_name(fld)]
                    if child
                },
            )

    return graph


def parse_content(
    content: bytes,
    language: GraphShardMetadataLanguage,
) -> Tree:
    parser_language = PARSER_LANGUAGES[language]
    parser = Parser(parser_language)
    return parser.parse(content)


def parse_graphs(
    *,
    content: bytes,
    path: str,
    language: GraphShardMetadataLanguage,
    with_metadata: bool,
    debug_mode: bool,
) -> tuple[Graph, Graph] | None:
    raw_tree: Tree = parse_content(content, language)
    node: Node = raw_tree.root_node

    counter = map(str, count(1))
    try:
        graph: Graph = _build_ast_graph(content, language, node, counter, Graph())
    except ParsingError:
        return None

    syntax_graph = build_syntax_graph(path, language, graph, with_metadata=with_metadata)
    if not syntax_graph:
        return None

    syntax_graph = add_syntax_cfg(syntax_graph, is_multifile=with_metadata)

    if debug_mode:
        styles.add(graph)
        styles.add(syntax_graph)

    return (graph, syntax_graph)


def parse_one(
    path: str,
    language: GraphShardMetadataLanguage,
    content: tuple[bytes, str] | None = None,
    *,
    with_metadata: bool = False,
    debug_mode: bool = False,
) -> GraphShard | None:
    if not content or language == GraphShardMetadataLanguage.NOT_SUPPORTED:
        return None
    try:
        graphs = parse_graphs(
            content=content[0],
            path=path,
            language=language,
            with_metadata=with_metadata,
            debug_mode=debug_mode,
        )
    except (
        ArithmeticError,
        AttributeError,
        BufferError,
        EOFError,
        LookupError,
        MemoryError,
        NameError,
        OSError,
        ReferenceError,
        RuntimeError,
        SystemError,
        TypeError,
        ValueError,
        ParsingError,
    ):
        exc_type, exc_value, exc_traceback = sys.exc_info()
        log_blocking("warning", "%s while parsing: %s, ignoring", exc_type, path)
        log_to_remote_blocking(
            msg=(exc_type, exc_value, exc_traceback),
            severity="error",
            path=path,
        )
        return None

    if not graphs:
        return None
    ast_graph, syntax_graph = graphs
    if debug_mode:
        output = get_debug_path("tree-sitter-" + path)
        to_svg(copy_ast(ast_graph), f"{output}.ast")
        to_svg(syntax_graph, f"{output}.syntax_graph")

    return GraphShard(
        graph=ast_graph,
        path=path,
        syntax_graph=syntax_graph,
        content_as_str=content[1],
    )


def _get_content(path: str, working_dir: str) -> tuple[bytes, str] | None:
    full_path = os.path.join(working_dir, path)  # noqa: PTH118
    return safe_sync_get_file_raw_content(full_path)


def get_graph_db(paths: tuple[str, ...], working_dir: str) -> GraphDB:
    paths = tuple(sorted(paths))

    graph_db = GraphDB(
        context={},
        shards={},
    )

    for path in paths:
        language = decide_language(path)
        if (
            language in SUPPORTED_MULTIFILE
            and (content := _get_content(path, working_dir))
            and (
                path_shard := parse_one(
                    path,
                    language,
                    content,
                    with_metadata=True,
                    debug_mode=ctx.SKIMS_CONFIG.debug,
                )
            )
        ):
            set_context_by_lang(path_shard, graph_db.context, language)
            graph_db.shards.update({path: path_shard})
        elif path.endswith(".properties"):
            file_name = path.rsplit("/", maxsplit=1)[-1]
            content_str = get_file_content_block(path)
            graph_db.properties.update(
                {file_name: dict(load_java_properties(content_str).values())},
            )

    return graph_db


def get_shard(
    path: str,
    language: GraphShardMetadataLanguage,
    working_dir: str,
) -> GraphShard | None:
    content = _get_content(path, working_dir)
    return parse_one(
        path,
        language,
        content,
        with_metadata=False,
        debug_mode=ctx.SKIMS_CONFIG.debug,
    )
