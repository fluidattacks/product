import time

import ctx
from lib_sast.generic_paths import (
    analyze_not_ok_paths,
)
from lib_sast.path.analyze import (
    analyze as analyze_lib_path,
)
from lib_sast.root.analyze import (
    analyze as analyze_lib_root,
)
from utils.fs import (
    resolve_paths,
)
from utils.logs import (
    log_blocking,
)
from utils.state import (
    ExecutionStores,
)


async def analyze(stores: ExecutionStores) -> None:
    init_time = time.time()

    log_blocking("info", "Starting SAST analysis")
    paths = resolve_paths(
        working_dir=ctx.SKIMS_CONFIG.working_dir,
        include=ctx.SKIMS_CONFIG.sast.include,
        exclude=ctx.SKIMS_CONFIG.sast.exclude,
    )
    analyze_lib_path(paths=paths, stores=stores)
    await analyze_lib_root(paths=paths.ok_paths, stores=stores)
    analyze_not_ok_paths(
        nv_paths=paths.nv_paths,
        nu_paths=paths.nu_paths,
        stores=stores.vuln_stores,
    )
    log_blocking("info", "SAST analysis completed!")

    sast_time = ("sast", time.time() - init_time)
    stores.technique_stores["technique_times"].store(sast_time)
