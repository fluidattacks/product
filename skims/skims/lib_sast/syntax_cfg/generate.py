from typing import (
    cast,
)

from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.syntax_cfg.dispatchers import (
    DISPATCHERS,
)
from lib_sast.syntax_cfg.model import (
    MissingCfgBuilderError,
    SyntaxCfgArgs,
)
from utils.logs import (
    log_blocking,
)


def generic(args: SyntaxCfgArgs) -> NId:
    node_type = args.graph.nodes[args.n_id]["label_type"]

    if dispatcher := DISPATCHERS.get(node_type):
        return dispatcher(args)

    exc_log = f"Missing cfg builder for {node_type}"
    raise MissingCfgBuilderError(exc_log)


def add_syntax_cfg(graph: Graph, *, is_multifile: bool = False) -> Graph:
    try:
        generic(
            args=SyntaxCfgArgs(
                generic,
                graph,
                n_id="1",
                nxt_id=None,
                is_multifile=is_multifile,
            ),
        )
    except MissingCfgBuilderError as error:
        log_blocking("warning", cast(str, error))
    return graph
