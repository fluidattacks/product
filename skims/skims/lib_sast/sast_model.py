from enum import (
    Enum,
)
from typing import (
    NamedTuple,
)

import networkx as nx
from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)

NId = str


class Graph(nx.DiGraph):
    pass


# Add a lang only when there is at least 1 method, otherwise it is unnecessary
class GraphShardMetadataLanguage(Enum):
    CSHARP = "c_sharp"
    DART = "dart"
    GO = "go"
    HCL = "hcl"
    JAVA = "java"
    JAVASCRIPT = "javascript"
    JSON = "json"
    KOTLIN = "kotlin"
    NOT_SUPPORTED = "not_supported"
    PHP = "php"
    PYTHON = "python"
    RUBY = "ruby"
    SCALA = "scala"
    SWIFT = "swift"
    TYPESCRIPT = "tsx"
    YAML = "yaml"


class GraphShard(NamedTuple):
    path: str
    graph: Graph
    syntax_graph: Graph
    content_as_str: str


class GraphDB(NamedTuple):
    context: dict[GraphShardMetadataLanguage, dict[str, dict]]
    shards: dict[str, GraphShard]
    properties: dict[str, dict[str, str]] = {}  # noqa: RUF012

    def get_path_shard(self, path: str) -> GraphShard | None:
        return self.shards.get(path)


class MethodSupplies(NamedTuple):
    selected_nodes: list[NId]
    graph_db: GraphDB | None


class QuerySupplies(NamedTuple):
    method: MethodsEnum
    method_calls: int
    graph_shard: GraphShard


language_extensions_map: dict[GraphShardMetadataLanguage, tuple[str, ...]] = {
    GraphShardMetadataLanguage.CSHARP: (".cs",),
    GraphShardMetadataLanguage.DART: (".dart",),
    GraphShardMetadataLanguage.GO: (".go",),
    GraphShardMetadataLanguage.HCL: (".hcl", ".tf"),
    GraphShardMetadataLanguage.JAVA: (".java",),
    GraphShardMetadataLanguage.JAVASCRIPT: (".js", ".jsx"),
    GraphShardMetadataLanguage.JSON: (".json",),
    GraphShardMetadataLanguage.KOTLIN: (".kt", ".ktm", ".kts"),
    GraphShardMetadataLanguage.PHP: (".php",),
    GraphShardMetadataLanguage.PYTHON: (".py",),
    GraphShardMetadataLanguage.RUBY: (".rb",),
    GraphShardMetadataLanguage.SCALA: (".scala",),
    GraphShardMetadataLanguage.SWIFT: (".swift",),
    GraphShardMetadataLanguage.TYPESCRIPT: (".ts", ".tsx"),
    GraphShardMetadataLanguage.YAML: (".yaml", ".yml"),
}


def decide_language(path: str) -> GraphShardMetadataLanguage:
    for language, extensions in language_extensions_map.items():
        if path.endswith(extensions):
            return language
    return GraphShardMetadataLanguage.NOT_SUPPORTED


SUPPORTED_MULTIFILE = {
    GraphShardMetadataLanguage.JAVA,
}

SUPPORTED_LANGUAGES = {
    GraphShardMetadataLanguage.CSHARP,
    GraphShardMetadataLanguage.JAVA,
    GraphShardMetadataLanguage.JAVASCRIPT,
    GraphShardMetadataLanguage.PYTHON,
    GraphShardMetadataLanguage.TYPESCRIPT,
}


POSSIBLE_LOCK_FILES = {
    "Pipfile": ["Pipfile.lock"],
    "package.json": ["package-lock.json", "yarn.lock", "pnpm-lock.yaml"],
    "pyproject.toml": ["poetry.lock"],
    "build.gradle": ["gradle.lockfile"],
    ".csproj": ["packages.lock.json"],
}


DEPENDENCY_FILES = {
    GraphShardMetadataLanguage.CSHARP: [".csproj"],
    GraphShardMetadataLanguage.JAVA: ["pom.xml", "build.gradle"],
    GraphShardMetadataLanguage.JAVASCRIPT: ["package.json"],
    GraphShardMetadataLanguage.PYTHON: [
        "requirements.txt",
        "Pipfile",
        "pyproject.toml",
    ],
    GraphShardMetadataLanguage.TYPESCRIPT: ["package.json"],
}
