from collections.abc import (
    Iterator,
)

from bs4 import (
    BeautifulSoup,
)
from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def keyboard_cache_exposure(content: str, path: str, soup: BeautifulSoup) -> MethodExecutionResult:
    def iterator() -> Iterator[tuple[int, int]]:
        for edit_text in list(
            soup.find_all("edittext")  # EditText
            + soup.find_all("textview"),  # TextView
        ):
            if (
                input_type := edit_text.attrs.get("android:inputtype")
            ) and "NoSuggestions" not in input_type:
                yield edit_text.sourceline, edit_text.sourcepos

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=MethodsEnum.ANDROID_APK_KEYBOARD_CACHE_EXPOSURE,
    )
