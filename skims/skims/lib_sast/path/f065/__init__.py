from lib_sast.path.f065.html.html_has_autocomplete import (
    has_autocomplete as run_has_autocomplete,
)
from lib_sast.path.f065.java import (
    keyboard_cache_exposure as run_keyboard_cache_exposure,
)

__all__ = ["run_has_autocomplete", "run_keyboard_cache_exposure"]
