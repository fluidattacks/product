from collections.abc import (
    Callable,
)
from contextlib import (
    suppress,
)
from typing import (
    NamedTuple,
)

from bs4 import (
    BeautifulSoup,
)
from pypugjs.ext.html import (
    process_pugjs,
)
from utils.function import (
    shield_blocking,
)
from utils.logs import (
    log_blocking,
)

SOUP_EXTENSIONS = (
    "config",
    "xml",
    "jmx",
    "html",
    "cshtml",
    "pug",
)


REGEX_EXTENSIONS = (
    "bashrc",
    "bash_profile",
    "cfg",
    "com",
    "conf",
    "Dockerfile",
    "env",
    "env.dev",
    "env.development",
    "env.example",
    "env.local",
    "env.prod",
    "env.production",
    "groovy",
    "httpsF5",
    "ini",
    "jmx",
    "jpage",
    "properties",
    "ps1",
    "Renviron",
    "sbt",
    "settings",
    "sh",
    "sql",
)

PATH_EXTENSIONS = (*SOUP_EXTENSIONS, *REGEX_EXTENSIONS)

SHIELD_SOUP: Callable = shield_blocking(
    on_error_return=None,
)


class PathContext(NamedTuple):
    path: str
    content: str
    soup_content: BeautifulSoup | None = None


@SHIELD_SOUP
def get_soup_content(path: str, content: str) -> BeautifulSoup | None:
    if path.endswith(".pug"):
        pug_parsed_content = None
        with suppress(Exception):
            pug_parsed_content = process_pugjs(content)

        if not pug_parsed_content:
            return None

        content = pug_parsed_content

    soup = None
    if path.endswith(SOUP_EXTENSIONS):
        try:
            soup = BeautifulSoup(content, features="html.parser")
        except AssertionError:
            log_blocking("error", "Error parsing %s using BeautifulSoup", path)

    return soup


def get_check_ctx(path: str, content: str) -> PathContext:
    return PathContext(
        path=path,
        content=content,
        soup_content=get_soup_content(path, content),
    )
