import re
from collections.abc import (
    Iterator,
)

DOCKERFILE_KEYWORDS = {
    "ADD",
    "ARG",
    "CMD",
    "COPY",
    "ENTRYPOINT",
    "ENV",
    "EXPOSE",
    "FROM",
    "HEALTHCHECK",
    "LABEL",
    "MAINTAINER",
    "ONBUILD",
    "RUN",
    "SHELL",
    "STOPSIGNAL",
    "USER",
    "VOLUME",
    "WORKDIR",
}


def get_commands_with_lines(content: str) -> Iterator[tuple[int, str]]:
    command_delimiters = r"RUN|&&|;"
    current_instruction = ""

    for line_number, line in enumerate(content.splitlines(), start=1):
        if (instruction := line.split(" ")[0]) in DOCKERFILE_KEYWORDS:
            current_instruction = instruction
        if current_instruction == "RUN" and not line.strip().startswith("#"):
            for command in re.split(command_delimiters, line):
                yield line_number, command
