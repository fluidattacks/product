import re
from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def container_with_disabled_ssl(content: str, path: str) -> MethodExecutionResult:
    method = MethodsEnum.DOCKER_DISABLED_SSL

    def iterator() -> Iterator[tuple[int, int]]:
        for idx, line in enumerate(content.splitlines(), start=1):
            if re.search(r"\s+\-Dcom.sun.management.jmxremote.ssl=false\s?", line):
                yield (idx, 0)

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
