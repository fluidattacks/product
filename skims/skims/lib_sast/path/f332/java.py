import re
from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from lib_sast.utils.custom_parsers import (
    load_java_properties,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def java_properties_unencrypted_transport(content: str, path: str) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_PROPERTIES_UNENCRYPTED_TRANSPORT

    url_pattern = r"(?i)(ftp|http|https)://([^/]+)"

    def iterator() -> Iterator[tuple[int, int]]:
        data = load_java_properties(
            content,
            include_comments=False,
            exclude_protected_values=True,
        )
        for line_no, (key, val) in data.items():
            match = re.search(url_pattern, val)
            protocol = match.group(1) if match else ""
            hostname = match.group(2) if match else ""
            all_elements_condition = bool(protocol and hostname and key)
            if (
                all_elements_condition
                and (protocol in {"http", "ftp"})
                and (hostname not in {"127.0.0.1", "0.0.0.0"})  # noqa: S104
                and ("." in hostname)
            ):
                yield line_no, 0

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
