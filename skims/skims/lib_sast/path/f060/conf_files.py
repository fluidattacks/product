from collections.abc import (
    Iterator,
)

from bs4 import (
    BeautifulSoup,
)
from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from lib_sast.path.utilities.xml_utils import (
    get_dang_attr_line,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def allow_acces_from_any_domain(
    content: str,
    path: str,
    soup: BeautifulSoup,
) -> MethodExecutionResult:
    method = MethodsEnum.XML_ALLOWS_ALL_DOMAINS

    def iterator() -> Iterator[tuple[int, int]]:
        for domain_tag in soup.find_all("cross-domain-policy"):
            for access_tag in domain_tag.find_all("allow-access-from"):
                if access_tag.attrs.get("domain", "").lower() == "*":
                    line_no, col_no = get_dang_attr_line(access_tag, content, "domain")

                    yield line_no, col_no

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
