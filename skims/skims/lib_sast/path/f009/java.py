from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from lib_sast.utils.custom_parsers import (
    load_java_properties,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def java_properties_sensitive_data(content: str, path: str) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_PROPERTIES_SENSITIVE_DATA
    sensible_key_smells = {
        "amazon.aws.key",
        "amazon.aws.secret",
        "artifactory_user",
        "artifactory_password",
        "aws.accesskey",
        "aws.secretkey",
        "bg.ws.aws.password",
        "bg.ws.key-store-password",
        "bg.ws.trust-store-password",
        "certificate.password",
        "crypto.password",
        "db.password",
        "database.password",
        "facephi.password",
        "jasypt.encryptor.password",
        "jwt.token.basic.signing.secret",
        "key.alias.password",
        "lambda.credentials2.key",
        "lambda.credentials2.secret",
        "mbda.credentials2.secret",
        "micro.password",
        "org.apache.ws.security.crypto.merlin.alias.password",
        "org.apache.ws.security.crypto.merlin.keystore.password",
        "passwordkeystore",
        "sonar.password",
        "spring.datasource.password",
        "spring.mail.password",
        "spring.mail.username",
        "transv-amq-lido4d-user",
        "transv-amq-lido4d-passwd",
        "truststore.password",
        "user_producer_amq",
        "pass_producer_amq",
        "wk-db-fup-lido4d-user",
        "wk-db-fup-lido4d-password",
        "wk-db-lido4d-wabi-user",
        "wk-db-lido4d-wabi-password",
        "wk-db-opshis-lido4d-password",
        "wk-sftp-cms-password",
        "wk-sftp-cms-username",
        "wk-sftp-fup-user",
        "wk-sftp-fup-password",
        "ws.aws.password",
    }

    def iterator() -> Iterator[tuple[int, int]]:
        data = load_java_properties(
            content,
            include_comments=True,
            exclude_protected_values=True,
        )
        for line_no, (key, val) in data.items():
            for sensible_key_smell in sensible_key_smells:
                if sensible_key_smell in key.lower() and val:
                    yield line_no, 0

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
