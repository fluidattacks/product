import re

from jwt import (
    PyJWT,
)
from jwt.exceptions import (
    DecodeError,
)

AWS_CREDENTIALS_PATTERN = re.compile(r"AKIA[A-Z0-9]{16}")
SONAR_TOKEN_PATTERN = re.compile(r"\s-Dsonar.token=sqp_[a-z0-9]{40}")
TOKEN = r"[A-Za-z0-9-_.+\/=]{20,}"  # noqa: S105
JWT_TOKEN_PATTERN = re.compile(rf"\b{TOKEN}\.{TOKEN}\.{TOKEN}\b")
DB_CONNECT_PATTERN = re.compile(r'connectionString=".*password=[^{]+.*"', flags=re.IGNORECASE)
USER_PASS_PATTERN = re.compile(r'(username|password)="[^"]+"', flags=re.IGNORECASE)


def is_key_sensitive(key: str) -> bool:
    return any(
        key.lower().endswith(suffix)
        for suffix in [
            "key",
            "pass",
            "passwd",
            "user",
            "username",
        ]
    )


def validate_jwt(token: str) -> bool:
    try:
        PyJWT().decode(
            jwt=token,
            key="",
            options={
                "verify_signature": False,
                "verify_aud": False,
                "verify_iat": False,
                "verify_exp": False,
                "verify_nbf": False,
                "verify_iss": False,
                "verify_sub": False,
                "verify_jti": False,
                "verify_at_hash": False,
                "leeway": 0,
            },
        )
    except DecodeError:
        return False
    else:
        return True
