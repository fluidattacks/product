import re
from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from model.core import (
    MethodExecutionResult,
)

SHA_256_PATTERN = r".*@sha256:[a-fA-F0-9]{43,}"
VARIABLE_PATTERN = r"(#\{.+\}#|\$\{.+\}|\$.+)"

FROM_PATTERN = r"FROM\s+(?P<source>.*?)(?:\s+AS\s+(?P<source_alias>.*))?$"


@SHIELD_BLOCKING
def unpinned_docker_image(content: str, path: str) -> MethodExecutionResult:
    method = MethodsEnum.UNPINNED_DOCKER_IMAGE

    def iterator() -> Iterator[tuple[int, int]]:
        stages_names = []
        for line_number, line in enumerate(content.splitlines(), start=1):
            match = re.match(FROM_PATTERN, line)
            if not match:
                continue
            source = match.group("source")
            if alias := match.group("source_alias"):
                stages_names.append(alias)

            if re.match(SHA_256_PATTERN, source) or re.match(VARIABLE_PATTERN, source):
                continue

            if source not in stages_names:
                yield line_number, 0

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
