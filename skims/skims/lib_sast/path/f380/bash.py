import re

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from model.core import (
    MethodExecutionResult,
)


def check_digest(line: str) -> bool:
    digest_re: re.Pattern = re.compile(".*@sha256:[a-fA-F0-9]{64}")
    return bool(digest_re.search(line))


def get_joint_line(line_no: int, lines: list) -> str:
    joint_line: str = lines[line_no - 1].strip()[:-1] + " "
    for line in lines[line_no:]:
        joint_line += line[:-1] + " "
        if line.strip()[-1] != "\\":
            break
    return joint_line


@SHIELD_BLOCKING
def image_has_digest(content: str, path: str) -> MethodExecutionResult:
    method = MethodsEnum.BASH_IMAGE_HAS_DIGEST
    commands_to_check = {"docker run", "podman run"}
    line_breaks: set = set()
    vulns_found: set[tuple[int, int]] = set()
    lines = content.splitlines()
    for line_number, line in enumerate(lines, start=1):
        if (
            not line.lstrip().startswith("#")
            and any(command in line for command in commands_to_check)
            and (valid_line := line.split(" #")[0])
        ):
            if valid_line.rstrip().endswith("\\"):
                line_breaks.add(line_number)
            elif not check_digest(line):
                vulns_found.add((line_number, 0))

    for line_no in line_breaks:
        complete_line: str = get_joint_line(line_no, lines)
        if not check_digest(complete_line):
            vulns_found.add((line_no, 0))

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iter(vulns_found),
        path=path,
        method=method,
    )
