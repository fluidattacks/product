from lib_sast.path.f056.dotnetconfig import (
    anon_auth_enabled as run_anon_auth_enabled,
)

__all__ = ["run_anon_auth_enabled"]
