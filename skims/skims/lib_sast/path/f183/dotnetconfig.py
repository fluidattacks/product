from collections.abc import (
    Iterator,
)

from bs4 import (
    BeautifulSoup,
)
from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from lib_sast.path.utilities.xml_utils import (
    get_dang_attr_line,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def has_debug_enabled(content: str, path: str, soup: BeautifulSoup) -> MethodExecutionResult:
    method = MethodsEnum.DOTNETCONFIG_HAS_DEBUG_ENABLED

    def iterator() -> Iterator[tuple[int, int]]:
        for system_tag in soup.find_all("system.web"):
            for comp_tag in system_tag.find_all("compilation", attrs={"debug": "true"}):
                line_no, col_no = get_dang_attr_line(comp_tag, content, "debug")
                yield line_no, col_no

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
