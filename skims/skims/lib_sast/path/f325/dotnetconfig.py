from collections.abc import (
    Iterator,
)

from bs4 import (
    BeautifulSoup,
)
from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from lib_sast.path.utilities.xml_utils import (
    get_dang_attr_line,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def excessive_auth_privileges(
    content: str,
    path: str,
    soup: BeautifulSoup,
) -> MethodExecutionResult:
    method = MethodsEnum.DOTNETCONFIG_EXCESSIVE_AUTH_PRIVILEGES

    def iterator() -> Iterator[tuple[int, int]]:
        """Check if authorization to users is configured with excessive privileges.

        https://docs.microsoft.com/en-us/iis/configuration/
        system.webserver/security/authorization/add
        """
        for auth_tag in soup.find_all("authorization"):
            for add_tag in auth_tag.find_all("add", attrs={"accesstype": "Allow", "users": "*"}):
                line_no, col_no = get_dang_attr_line(add_tag, content, "users")
                yield line_no, col_no

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
