from lib_sast.path.f044.conf_files import (
    header_allow_all_methods as run_header_allow_all_methods,
)
from lib_sast.path.f044.conf_files import (
    header_allow_dangerous_methods as run_allow_danger_methods,
)

__all__ = [
    "run_allow_danger_methods",
    "run_header_allow_all_methods",
]
