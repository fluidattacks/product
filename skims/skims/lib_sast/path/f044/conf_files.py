from collections.abc import (
    Iterator,
)

from bs4 import (
    BeautifulSoup,
)
from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from lib_sast.path.utilities.xml_utils import (
    get_dang_attr_line,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def header_allow_dangerous_methods(
    content: str,
    path: str,
    soup: BeautifulSoup,
) -> MethodExecutionResult:
    method = MethodsEnum.XML_HEADER_ALLOW_DANGER_METHODS
    danger_methods = {"debug", "trace"}

    def iterator() -> Iterator[tuple[int, int]]:
        for tag in soup.find_all("add"):
            if (
                (tag_value := tag.attrs.get("verb"))
                and any(danger_m in tag_value.lower().split(",") for danger_m in danger_methods)
                and (
                    not (allowed_value := tag.attrs.get("allowed"))
                    or str(allowed_value).lower() != "false"
                )
            ):
                line_no, col_no = get_dang_attr_line(tag, content, "verb")
                yield line_no, col_no

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )


@SHIELD_BLOCKING
def header_allow_all_methods(content: str, path: str, soup: BeautifulSoup) -> MethodExecutionResult:
    def iterator() -> Iterator[tuple[int, int]]:
        for tag in soup.find_all("add"):
            if (
                (tag_value := tag.attrs.get("verb"))
                and tag_value.lower() == "*"
                and (
                    not (allowed_value := tag.attrs.get("allowed"))
                    or str(allowed_value).lower() != "false"
                )
            ):
                line_no, col_no = get_dang_attr_line(tag, content, "verb")
                yield line_no, col_no

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=MethodsEnum.XML_HEADER_ALLOW_ALL_METHODS,
    )
