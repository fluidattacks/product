from collections.abc import (
    Iterator,
)

from bs4 import (
    BeautifulSoup,
)
from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def not_suppress_vuln_header(content: str, path: str, soup: BeautifulSoup) -> MethodExecutionResult:
    method = MethodsEnum.DOTNETCONFIG_NOT_SUPPRESS_VULN_HEADER

    def iterator() -> Iterator[tuple[int, int]]:
        for header_tag in soup.find_all("customheaders"):
            for remove_tag in header_tag.find_all("remove"):
                if remove_tag.attrs.get("name", "").lower() == "x-powered-by":
                    break
            else:
                line_no: int = header_tag.sourceline
                col_no: int = 0
                yield line_no, col_no

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
