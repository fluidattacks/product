from lib_sast.path.f097.html.html_has_reverse_tabnabbing import (
    has_reverse_tabnabbing as run_has_reverse_tabnabbing,
)

__all__ = ["run_has_reverse_tabnabbing"]
