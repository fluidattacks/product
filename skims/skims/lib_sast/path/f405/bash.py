import re
from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def excessive_privileges_for_others(content: str, path: str) -> MethodExecutionResult:
    method = MethodsEnum.EXCESSIVE_PRIVILEGES_FOR_OTHERS
    pattern = re.compile(r"^(?!#).*?(?:COPY\s+--chmod=(\d{3})|chmod\s+(\d{3}|\+x|u\+s))\s+.+")

    def iterator() -> Iterator[tuple[int, int]]:
        for index, line in enumerate(content.splitlines(), 1):
            if (
                (match := pattern.search(line))
                and (chmod_value := match.group(1) or match.group(2))
                and chmod_value.isdigit()
                and chmod_value[-1] in ("3", "7")
            ):
                yield index, 0

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
