from collections.abc import (
    Iterator,
)

from bs4 import (
    BeautifulSoup,
)
from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from lib_sast.path.utilities.xml_utils import (
    get_dang_attr_line,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def not_custom_errors(content: str, path: str, soup: BeautifulSoup) -> MethodExecutionResult:
    method = MethodsEnum.DOTNETCONFIG_NOT_CUSTOM_ERRORS

    def iterator() -> Iterator[tuple[int, int]]:
        for system_tag in soup.find_all("system.web"):
            for error_tag in system_tag.find_all("customerrors", attrs={"mode": "Off"}):
                line_no, col_no = get_dang_attr_line(error_tag, content, "mode")
                yield line_no, col_no

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
