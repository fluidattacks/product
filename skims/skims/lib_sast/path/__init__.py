from collections.abc import (
    Callable,
)
from configparser import (
    ConfigParser,
)

from bs4 import (
    BeautifulSoup,
)
from lib_sast.path.common import (
    validate_extension_in_java_properties,
    validate_is_android_manifest,
    validate_is_bash_extension,
    validate_is_config_extension,
    validate_is_file_with_credentials,
    validate_is_html_extension,
    validate_is_script_type_file,
    validate_is_xml_jmx_extension,
    validate_name_is_dockerfile,
    validate_php_ini,
    validate_web_config,
)
from lib_sast.path.f009 import (
    aws_credentials,
    docker_hardcoded_credentials,
    dockerfile_env_secrets,
    java_properties_sensitive_data,
    jwt_token,
    web_config_db_connection,
    web_config_user_pass,
)
from lib_sast.path.f015 import (
    run_basic_auth_method,
    run_jmx_header_basic,
)
from lib_sast.path.f037 import (
    run_not_suppress_vuln_header,
)
from lib_sast.path.f044 import (
    run_allow_danger_methods,
    run_header_allow_all_methods,
)
from lib_sast.path.f052 import (
    run_java_properties_missing_ssl,
    run_java_properties_weak_cipher_suite,
)
from lib_sast.path.f055 import (
    run_android_backups_enabled,
)
from lib_sast.path.f056 import (
    run_anon_auth_enabled,
)
from lib_sast.path.f058 import (
    run_android_debugging_enabled,
)
from lib_sast.path.f060 import (
    run_allow_acces_from_any_domain,
)
from lib_sast.path.f065 import (
    run_has_autocomplete,
    run_keyboard_cache_exposure,
)
from lib_sast.path.f068 import (
    run_php_insecure_expiration_time,
)
from lib_sast.path.f075 import (
    run_android_exported_cp,
    run_android_uri_permissions,
)
from lib_sast.path.f086 import (
    run_has_not_subresource_integrity,
)
from lib_sast.path.f097 import (
    run_has_reverse_tabnabbing,
)
from lib_sast.path.f128 import (
    run_php_http_only_disabled,
)
from lib_sast.path.f135 import (
    run_xml_has_x_xss_protection_header,
)
from lib_sast.path.f149 import (
    run_network_ssl_disabled,
)
from lib_sast.path.f152 import (
    run_xml_x_frame_options,
)
from lib_sast.path.f153 import (
    run_xml_accept_header,
)
from lib_sast.path.f183 import (
    run_docker_debugging_enabled,
    run_has_debug_enabled,
)
from lib_sast.path.f235 import (
    run_asp_version_enabled,
    run_php_discloses_server_version,
)
from lib_sast.path.f239 import (
    run_not_custom_errors,
    run_php_server_leaks_errors,
)
from lib_sast.path.f266 import (
    run_container_with_user_root,
    run_container_without_user,
    run_docker_sensitive_mount,
)
from lib_sast.path.f313 import (
    curl_insecure_certificates,
    run_has_ssl_disabled,
)
from lib_sast.path.f325 import (
    run_excessive_auth_privileges,
)
from lib_sast.path.f332 import (
    container_with_disabled_ssl,
    java_properties_unencrypted_transport,
)
from lib_sast.path.f346 import (
    run_has_dangerous_permissions,
)
from lib_sast.path.f359 import (
    run_bash_using_sshpass,
    run_container_using_sshpass,
)
from lib_sast.path.f372 import (
    run_docker_downgrade_protocol,
)
from lib_sast.path.f380 import (
    run_image_has_digest,
    run_unpinned_docker_image,
)
from lib_sast.path.f403 import (
    run_insecure_configuration,
)
from lib_sast.path.f405 import (
    run_excessive_privileges_for_others,
)
from lib_sast.path.f418 import (
    docker_weak_hash_algorithm,
    run_docker_cleartext_protocol,
    run_docker_insecure_builder_sandbox,
    run_docker_insecure_context_directory,
    run_docker_insecure_network_host,
    run_docker_socket_mount,
    run_docker_using_add_command,
    run_docker_weak_ssl_tls,
)
from model.core import (
    FindingEnum,
    MethodExecutionResult,
)

REGEX_CHECKS: set[
    tuple[
        FindingEnum,
        Callable[[str, str], bool],
        Callable[[str, str], MethodExecutionResult],
    ]
] = {
    (
        FindingEnum.F009,
        validate_is_file_with_credentials,
        aws_credentials,
    ),
    (
        FindingEnum.F009,
        validate_is_file_with_credentials,
        jwt_token,
    ),
    (
        FindingEnum.F009,
        validate_name_is_dockerfile,
        dockerfile_env_secrets,
    ),
    (
        FindingEnum.F009,
        validate_name_is_dockerfile,
        docker_hardcoded_credentials,
    ),
    (
        FindingEnum.F009,
        validate_extension_in_java_properties,
        java_properties_sensitive_data,
    ),
    (
        FindingEnum.F009,
        validate_web_config,
        web_config_db_connection,
    ),
    (
        FindingEnum.F009,
        validate_web_config,
        web_config_user_pass,
    ),
    (
        FindingEnum.F052,
        validate_extension_in_java_properties,
        run_java_properties_missing_ssl,
    ),
    (
        FindingEnum.F052,
        validate_extension_in_java_properties,
        run_java_properties_weak_cipher_suite,
    ),
    (
        FindingEnum.F183,
        validate_name_is_dockerfile,
        run_docker_debugging_enabled,
    ),
    (
        FindingEnum.F266,
        validate_name_is_dockerfile,
        run_container_without_user,
    ),
    (
        FindingEnum.F266,
        validate_name_is_dockerfile,
        run_container_with_user_root,
    ),
    (
        FindingEnum.F266,
        validate_name_is_dockerfile,
        run_docker_sensitive_mount,
    ),
    (
        FindingEnum.F313,
        validate_is_script_type_file,
        curl_insecure_certificates,
    ),
    (
        FindingEnum.F332,
        validate_name_is_dockerfile,
        container_with_disabled_ssl,
    ),
    (
        FindingEnum.F332,
        validate_extension_in_java_properties,
        java_properties_unencrypted_transport,
    ),
    (
        FindingEnum.F359,
        validate_name_is_dockerfile,
        run_container_using_sshpass,
    ),
    (
        FindingEnum.F359,
        validate_is_bash_extension,
        run_bash_using_sshpass,
    ),
    (
        FindingEnum.F372,
        validate_name_is_dockerfile,
        run_docker_downgrade_protocol,
    ),
    (
        FindingEnum.F380,
        validate_name_is_dockerfile,
        run_unpinned_docker_image,
    ),
    (
        FindingEnum.F380,
        validate_is_bash_extension,
        run_image_has_digest,
    ),
    (
        FindingEnum.F405,
        validate_is_script_type_file,
        run_excessive_privileges_for_others,
    ),
    (
        FindingEnum.F418,
        validate_name_is_dockerfile,
        run_docker_socket_mount,
    ),
    (
        FindingEnum.F418,
        validate_name_is_dockerfile,
        run_docker_using_add_command,
    ),
    (
        FindingEnum.F418,
        validate_name_is_dockerfile,
        run_docker_weak_ssl_tls,
    ),
    (
        FindingEnum.F418,
        validate_name_is_dockerfile,
        run_docker_insecure_builder_sandbox,
    ),
    (
        FindingEnum.F418,
        validate_name_is_dockerfile,
        run_docker_cleartext_protocol,
    ),
    (
        FindingEnum.F418,
        validate_name_is_dockerfile,
        docker_weak_hash_algorithm,
    ),
    (
        FindingEnum.F418,
        validate_name_is_dockerfile,
        run_docker_insecure_network_host,
    ),
    (
        FindingEnum.F418,
        validate_name_is_dockerfile,
        run_docker_insecure_context_directory,
    ),
}

SOUP_CHECKS: set[
    tuple[
        FindingEnum,
        Callable[[str, str], bool],
        Callable[[str, str, BeautifulSoup], MethodExecutionResult],
    ]
] = {
    (
        FindingEnum.F015,
        validate_is_xml_jmx_extension,
        run_basic_auth_method,
    ),
    (
        FindingEnum.F015,
        validate_is_xml_jmx_extension,
        run_jmx_header_basic,
    ),
    (
        FindingEnum.F037,
        validate_is_config_extension,
        run_not_suppress_vuln_header,
    ),
    (
        FindingEnum.F044,
        validate_is_xml_jmx_extension,
        run_allow_danger_methods,
    ),
    (
        FindingEnum.F044,
        validate_is_xml_jmx_extension,
        run_header_allow_all_methods,
    ),
    (
        FindingEnum.F055,
        validate_is_android_manifest,
        run_android_backups_enabled,
    ),
    (
        FindingEnum.F056,
        validate_is_config_extension,
        run_anon_auth_enabled,
    ),
    (
        FindingEnum.F058,
        validate_is_android_manifest,
        run_android_debugging_enabled,
    ),
    (
        FindingEnum.F060,
        validate_is_xml_jmx_extension,
        run_allow_acces_from_any_domain,
    ),
    (
        FindingEnum.F065,
        validate_is_html_extension,
        run_has_autocomplete,
    ),
    (
        FindingEnum.F065,
        validate_is_xml_jmx_extension,
        run_keyboard_cache_exposure,
    ),
    (
        FindingEnum.F075,
        validate_is_android_manifest,
        run_android_exported_cp,
    ),
    (
        FindingEnum.F075,
        validate_is_android_manifest,
        run_android_uri_permissions,
    ),
    (
        FindingEnum.F086,
        validate_is_html_extension,
        run_has_not_subresource_integrity,
    ),
    (
        FindingEnum.F097,
        validate_is_html_extension,
        run_has_reverse_tabnabbing,
    ),
    (
        FindingEnum.F135,
        validate_is_config_extension,
        run_xml_has_x_xss_protection_header,
    ),
    (
        FindingEnum.F149,
        validate_is_config_extension,
        run_network_ssl_disabled,
    ),
    (
        FindingEnum.F152,
        validate_is_xml_jmx_extension,
        run_xml_x_frame_options,
    ),
    (
        FindingEnum.F153,
        validate_is_xml_jmx_extension,
        run_xml_accept_header,
    ),
    (
        FindingEnum.F183,
        validate_is_config_extension,
        run_has_debug_enabled,
    ),
    (
        FindingEnum.F235,
        validate_is_config_extension,
        run_asp_version_enabled,
    ),
    (
        FindingEnum.F239,
        validate_is_config_extension,
        run_not_custom_errors,
    ),
    (
        FindingEnum.F313,
        validate_is_config_extension,
        run_has_ssl_disabled,
    ),
    (
        FindingEnum.F325,
        validate_is_config_extension,
        run_excessive_auth_privileges,
    ),
    (
        FindingEnum.F346,
        validate_is_android_manifest,
        run_has_dangerous_permissions,
    ),
    (
        FindingEnum.F403,
        validate_is_xml_jmx_extension,
        run_insecure_configuration,
    ),
}

PHP_INI_CHECKS: set[
    tuple[
        FindingEnum,
        Callable[[str, str], bool],
        Callable[[str, str, ConfigParser], MethodExecutionResult],
    ]
] = {
    (
        FindingEnum.F068,
        validate_php_ini,
        run_php_insecure_expiration_time,
    ),
    (
        FindingEnum.F128,
        validate_php_ini,
        run_php_http_only_disabled,
    ),
    (
        FindingEnum.F235,
        validate_php_ini,
        run_php_discloses_server_version,
    ),
    (
        FindingEnum.F239,
        validate_php_ini,
        run_php_server_leaks_errors,
    ),
}
