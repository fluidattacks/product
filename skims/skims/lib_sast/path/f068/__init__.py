from lib_sast.path.f068.php import (
    php_insecure_expiration_time as run_php_insecure_expiration_time,
)

__all__ = [
    "run_php_insecure_expiration_time",
]
