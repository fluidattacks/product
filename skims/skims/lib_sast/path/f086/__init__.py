from lib_sast.path.f086.html.html_has_not_sub_resource_integrity import (
    has_not_subresource_integrity as run_has_not_subresource_integrity,
)

__all__ = ["run_has_not_subresource_integrity"]
