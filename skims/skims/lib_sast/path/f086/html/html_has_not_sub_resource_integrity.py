from collections.abc import (
    Iterator,
)

from bs4 import (
    BeautifulSoup,
)
from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    SHIELD_BLOCKING,
    get_vulnerabilities_from_iterator_blocking,
)
from model.core import (
    MethodExecutionResult,
)


def is_external_resource(ref_ext: str) -> bool:
    return ref_ext.startswith(("http", "//"))


def has_supported_format(ref_ext: str, elem_type: str) -> bool:
    return ref_ext.endswith((".js", ".css")) or elem_type in {"text/javascript", "text/css"}


@SHIELD_BLOCKING
def has_not_subresource_integrity(
    content: str,
    path: str,
    soup: BeautifulSoup,
) -> MethodExecutionResult:
    method = MethodsEnum.HTML_HAS_NOT_SUB_RESOURCE_INTEGRITY

    def iterator() -> Iterator[tuple[int, int]]:
        """Check if elements fetched by the provided HTML have `SRI`.

        See: `Documentation <https://developer.mozilla.org/en-US/
        docs/Web/Security/Subresource_Integrity>`_.
        """
        for elem_types in ("link", "script"):
            for elem in soup(elem_types):
                ref_ext = elem.get("href") or elem.get("src")
                if (
                    ref_ext
                    and is_external_resource(ref_ext)
                    and has_supported_format(ref_ext, elem.get("type"))
                    and not elem.get("integrity")
                ):
                    yield elem.sourceline, elem.sourcepos

    return get_vulnerabilities_from_iterator_blocking(
        content=content,
        iterator=iterator(),
        path=path,
        method=method,
    )
