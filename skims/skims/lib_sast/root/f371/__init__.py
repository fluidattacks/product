from lib_sast.root.f371.javascript import (
    js_bypass_security_trust_url as _js_bypass_security_trust_url,
)
from lib_sast.root.f371.javascript import (
    js_dangerously_set_innerhtml as _js_dangerously_set_innerhtml,
)
from lib_sast.root.f371.javascript import (
    uses_innerhtml as _js_uses_innerhtml,
)
from lib_sast.root.f371.typescript import (
    ts_bypass_security_trust_url as _ts_bypass_security_trust_url,
)
from lib_sast.root.f371.typescript import (
    ts_dangerously_set_innerhtml as _ts_dangerously_set_innerhtml,
)
from lib_sast.root.f371.typescript import (
    uses_innerhtml as _ts_uses_innerhtml,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def js_bypass_security_trust_url(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_bypass_security_trust_url(shard, method_supplies)


@SHIELD_BLOCKING
def js_dangerously_set_innerhtml(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_dangerously_set_innerhtml(shard, method_supplies)


@SHIELD_BLOCKING
def js_uses_innerhtml(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _js_uses_innerhtml(shard, method_supplies)


@SHIELD_BLOCKING
def ts_bypass_security_trust_url(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_bypass_security_trust_url(shard, method_supplies)


@SHIELD_BLOCKING
def ts_dangerously_set_innerhtml(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_dangerously_set_innerhtml(shard, method_supplies)


@SHIELD_BLOCKING
def ts_uses_innerhtml(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _ts_uses_innerhtml(shard, method_supplies)
