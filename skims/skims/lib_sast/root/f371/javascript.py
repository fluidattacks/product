from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.f371.common import (
    danger_bypass_security,
    has_set_inner_html,
    nid_danger_inner_html,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def uses_innerhtml(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    method = MethodsEnum.JS_USES_INNERHTML

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        yield from nid_danger_inner_html(method, graph, method_supplies)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def js_bypass_security_trust_url(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JS_USE_OF_BYPASS_SECURITY_TRUST_URL

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        yield from danger_bypass_security(graph, method, method_supplies)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def js_dangerously_set_innerhtml(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JS_USES_DANGEROUSLY_SET_HTML

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        yield from has_set_inner_html(graph, method, method_supplies.selected_nodes)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
