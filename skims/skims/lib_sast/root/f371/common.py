from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.utilities.common import (
    get_func_parameters_names,
)
from lib_sast.root.utilities.javascript import (
    file_imports_module,
)
from lib_sast.sast_model import (
    Graph,
    MethodSupplies,
    NId,
)
from lib_sast.symbolic_eval.evaluate import (
    evaluate,
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_group_d,
    pred_ast,
)


def nid_danger_inner_html(
    method: MethodsEnum,
    graph: Graph,
    method_supplies: MethodSupplies,
) -> list[NId]:
    if not file_imports_module(graph, "express"):
        return []
    return [
        n_id
        for n_id in method_supplies.selected_nodes
        if (
            graph.nodes[n_id]["member"] == "innerHTML"
            and (parent_id := pred_ast(graph, n_id)[0])
            and graph.nodes[parent_id]["label_type"] == "Assignment"
            and (val_id := graph.nodes[parent_id].get("value_id"))
            and get_node_evaluation_results(method, graph, val_id, set())
        )
    ]


def get_danger_method_calls(graph: Graph, jsx_nodes: list[NId]) -> set[NId]:
    danger_n_ids: set[NId] = set()

    for n_id in jsx_nodes:
        danger_n_ids.update(
            n_id
            for n_id in match_ast_group_d(graph, n_id, "VariableDeclaration", -1)
            if graph.nodes[n_id]["variable"] == "dangerouslySetInnerHTML"
        )
    return danger_n_ids


def has_set_inner_html(graph: Graph, method: MethodsEnum, jsx_nodes: list[NId]) -> list[NId]:
    danger_n_ids = get_danger_method_calls(graph, jsx_nodes)

    return [
        n_id
        for n_id in danger_n_ids
        if get_node_evaluation_results(method, graph, n_id, set(), danger_goal=False)
    ]


def get_danger_n_id(graph: Graph, n_id: NId, method: MethodsEnum) -> NId | None:
    parameters_names = get_func_parameters_names(graph, n_id)
    for path in get_backward_paths(graph, n_id):
        evaluation = evaluate(method, graph, path, n_id)
        if evaluation:
            if evaluation.danger and "sanitized" not in evaluation.triggers:
                return n_id
            for trigger in list(evaluation.triggers):
                if trigger in parameters_names:
                    return n_id
    return None


def danger_bypass_security(
    graph: Graph,
    method: MethodsEnum,
    method_supplies: MethodSupplies,
) -> list[NId]:
    risky_methods = {
        "bypassSecurityTrustHtml",
        "bypassSecurityTrustScript",
        "bypassSecurityTrustUrl",
        "bypassSecurityTrustResourceUrl",
    }
    return [
        args_ids[0]
        for n_id in method_supplies.selected_nodes
        if (
            graph.nodes[n_id]["expression"].split(".")[-1] in risky_methods
            and (al_id := graph.nodes[n_id].get("arguments_id"))
            and (args_ids := adj_ast(graph, al_id))
            and len(args_ids) > 0
            and get_danger_n_id(graph, args_ids[0], method)
        )
    ]
