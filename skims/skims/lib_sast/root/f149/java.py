from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    has_string_definition,
    is_node_definition_unsafe,
)
from lib_sast.root.utilities.java import (
    is_any_library_imported_prefix,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    match_ast_group_d,
    search_pred_until_type,
)
from model.core import (
    MethodExecutionResult,
)


def has_ssl_check(graph: Graph, md_n_id: NId, object_id: NId) -> bool:
    nodes = graph.nodes
    danger_name = nodes[object_id].get("symbol", "")

    return any(
        (
            (nodes[n_id].get("expression") == "setSSLCheckServerIdentity")
            and (method_obj := nodes[n_id].get("object_id"))
            and nodes[method_obj].get("symbol") == danger_name
            and (f_arg := get_n_arg(graph, n_id, 0))
            and (has_string_definition(graph, f_arg) == "true")
        )
        for n_id in match_ast_group_d(graph, md_n_id, "MethodInvocation", -1)
    )


def comes_from_simple_email(graph: Graph, n_id: NId) -> bool:
    return (
        graph.nodes[n_id]["label_type"] == "ObjectCreation"
        and graph.nodes[n_id].get("name", "") == "SimpleEmail"
    )


def java_insecure_smtp_connection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_INSECURE_SMTP_CONNECTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported_prefix(graph, ("org.apache.commons.mail",)):
            return

        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("expression", "") == "send"
                and (object_id := graph.nodes[n_id].get("object_id"))
                and is_node_definition_unsafe(graph, object_id, comes_from_simple_email)
                and (md_n_id := search_pred_until_type(graph, n_id, {"MethodDeclaration"}))
                and (not has_ssl_check(graph, md_n_id[0], object_id))
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def has_ssl_protection(graph: Graph, md_n_id: NId, object_id: NId) -> bool:
    nodes = graph.nodes
    danger_name = nodes[object_id].get("symbol", "")

    return any(
        (
            nodes[n_id].get("expression")
            in {"setSSLCheckServerIdentity", "setSSLOnConnect", "setStartTLSRequired"}
            and (method_obj := nodes[n_id].get("object_id"))
            and nodes[method_obj].get("symbol") == danger_name
            and (f_arg := get_n_arg(graph, n_id, 0))
            and (has_string_definition(graph, f_arg) == "true")
        )
        for n_id in match_ast_group_d(graph, md_n_id, "MethodInvocation", -1)
    )


def is_email_instance(graph: Graph, n_id: NId) -> bool:
    return graph.nodes[n_id]["label_type"] == "ObjectCreation" and graph.nodes[n_id].get(
        "name",
        "",
    ) in {"MultiPartEmail", "HtmlEmail", "ImageHtmlEmail"}


def java_insecure_smtp_ssl(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_INSECURE_SMTP_SSL

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported_prefix(graph, ("org.apache.commons.mail",)):
            return

        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("expression", "") == "send"
                and (object_id := graph.nodes[n_id].get("object_id"))
                and is_node_definition_unsafe(graph, object_id, is_email_instance)
                and (md_n_id := search_pred_until_type(graph, n_id, {"MethodDeclaration"}))
                and (not has_ssl_protection(graph, md_n_id[0], object_id))
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
