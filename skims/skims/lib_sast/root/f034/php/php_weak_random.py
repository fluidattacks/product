from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from model.core import (
    MethodExecutionResult,
)


def _method_invocation_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    danger_invocations = {
        "rand",
        "mt_rand",
        "lcg_value",
        "array_rand",
        "uniqid",
    }

    nodes = args.graph.nodes
    if (exp := nodes[args.n_id].get("expression")) and exp in danger_invocations:
        args.triggers.add("random")
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "method_invocation": _method_invocation_evaluator,
}


def php_weak_random(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    method = MethodsEnum.PHP_WEAK_RANDOM

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            exp = graph.nodes[n_id].get("expression")
            if (
                (exp == "hash")
                and (vuln_arg := get_n_arg(graph, n_id, 1))
                and get_node_evaluation_results(
                    method,
                    graph,
                    vuln_arg,
                    {"random"},
                    danger_goal=False,
                    method_evaluators=METHOD_EVALUATORS,
                )
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
