from lib_sast.root.f034.java.java_weak_random import (
    java_weak_random as _java_weak_random,
)
from lib_sast.root.f034.java.java_weak_random import (
    java_weak_random_add_cookie as _java_weak_random_add_cookie,
)
from lib_sast.root.f034.javascript.javascript_weak_random import (
    javascript_weak_random as _javascript_weak_random,
)
from lib_sast.root.f034.kotlin.kotlin_weak_random import (
    kotlin_weak_random as _kotlin_weak_random,
)
from lib_sast.root.f034.php.php_weak_random import (
    php_weak_random as _php_weak_random,
)
from lib_sast.root.f034.typescript.typescript_weak_random import (
    typescript_weak_random as _typescript_weak_random,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def java_weak_random(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _java_weak_random(shard, method_supplies)


@SHIELD_BLOCKING
def java_weak_random_add_cookie(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_weak_random_add_cookie(shard, method_supplies)


@SHIELD_BLOCKING
def js_weak_random(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _javascript_weak_random(shard, method_supplies)


@SHIELD_BLOCKING
def kotlin_weak_random(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _kotlin_weak_random(shard, method_supplies)


@SHIELD_BLOCKING
def php_weak_random(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _php_weak_random(shard, method_supplies)


@SHIELD_BLOCKING
def ts_weak_random(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _typescript_weak_random(shard, method_supplies)
