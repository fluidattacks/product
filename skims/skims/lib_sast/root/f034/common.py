from collections.abc import (
    Callable,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.sast_model import (
    Graph,
    MethodSupplies,
    NId,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    match_ast,
)


def _method_invocation_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    if "Math.random()" in args.graph.nodes[args.n_id]["expression"]:
        args.triggers.add("Random")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "method_invocation": _method_invocation_evaluator,
}


def _aux_weak_random(graph: Graph, n_id: NId) -> bool:
    for path in get_backward_paths(graph, n_id):
        for node_id in path:
            if (
                graph.nodes[node_id]["label_type"] == "MethodInvocation"
                and graph.nodes[node_id].get("expression") == "expressJwt"
            ):
                return True
    return False


def weak_random(graph: Graph, method: MethodsEnum, method_supplies: MethodSupplies) -> list[NId]:
    vuln_nodes: list[NId] = []
    for n_id in method_supplies.selected_nodes:
        if (
            graph.nodes[n_id]["expression"].split(".")[-1] == "cookie"
            and (al_id := graph.nodes[n_id].get("arguments_id"))
            and (test_nid := match_ast(graph, al_id).get("__1__"))
            and get_node_evaluation_results(
                method,
                graph,
                test_nid,
                {"Random"},
                danger_goal=False,
                method_evaluators=METHOD_EVALUATORS,
            )
        ):
            vuln_nodes.append(n_id)
        if graph.nodes[n_id]["expression"] == "Math.random" and _aux_weak_random(graph, n_id):
            vuln_nodes.append(n_id)
    return vuln_nodes
