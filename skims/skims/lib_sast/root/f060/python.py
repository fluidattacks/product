from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils.graph import (
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def is_unsafe_context(method: MethodsEnum, graph: Graph, n_id: NId) -> bool:
    return bool(
        (val_id := graph.nodes[n_id].get("value_id"))
        and get_node_evaluation_results(method, graph, val_id, set()),
    )


def python_unsafe_ssl_hostname(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PYTHON_UNSAFE_SSL_HOSTNAME

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            parent_id = pred_ast(graph, n_id)[0]
            if (
                n_attrs.get("member", "") == "check_hostname"
                and is_unsafe_context(method, graph, parent_id)
                and get_node_evaluation_results(
                    method,
                    graph,
                    n_attrs["expression_id"],
                    {"sslcontext"},
                )
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
