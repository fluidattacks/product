from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    is_node_definition_unsafe,
)
from lib_sast.root.utilities.java import (
    is_any_library_imported_prefix,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import adj_ast, pred_ast
from model.core import (
    MethodExecutionResult,
)


def is_true_boolean(graph: Graph, n_id: NId) -> bool:
    return (
        graph.nodes[n_id].get("value_type", "") == "bool"
        and graph.nodes[n_id].get("value", "") == "true"
    )


def java_mongo_hostname_verification_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_MONGO_HOSTNAME_VERIFICATION_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported_prefix(
            graph,
            (
                "com.mongodb",
                "org.mongodb",
                "org.springframework.data.mongodb",
            ),
        ):
            return

        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("expression", "") == "invalidHostNameAllowed"
                and (first_arg := get_n_arg(graph, n_id, 0))
                and is_node_definition_unsafe(graph, first_arg, is_true_boolean)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def parent_is_hostname_verifier(graph: Graph, n_id: NId) -> bool:
    if (
        (parents_ids := pred_ast(graph, n_id, 2))
        and len(parents_ids) > 1
        and (grandparent := parents_ids[1])
    ):
        if graph.nodes[grandparent]["label_type"] == "ObjectCreation":
            return graph.nodes[grandparent].get("name", "") == "HostnameVerifier"

        if graph.nodes[grandparent]["label_type"] == "Class":
            return "HostnameVerifier" in graph.nodes[grandparent].get("inherited_class", "")

    return False


def is_insecure_host_verifier(graph: Graph, n_id: NId) -> bool:
    if graph.nodes[n_id].get("name", "") == "verify" and (
        block_id := graph.nodes[n_id].get("block_id")
    ):
        for child in adj_ast(graph, block_id):
            if graph.nodes[child]["label_type"] != "Return":
                continue

            return bool(
                (val_id := graph.nodes[child].get("value_id"))
                and graph.nodes[val_id].get("value_type", "") == "bool"
                and graph.nodes[val_id].get("value", "") == "true"
                and parent_is_hostname_verifier(graph, n_id),
            )

    return False


def java_unsafe_hostname_verifier(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_UNSAFE_HOSTNAME_VERIFIER

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if is_insecure_host_verifier(graph, n_id):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def java_noophostnameverifier_use(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_NOOPHOSTNAMEVERIFIER_USE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("expression", "")
                == "org.apache.http.conn.ssl.NoopHostnameVerifier"
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
