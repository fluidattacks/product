from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.json_utils import (
    get_key_value,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _scripts_with_insecure_flag(graph: Graph, nid: NId) -> Iterator[NId]:
    c_ids = adj_ast(graph, nid)
    for c_id in c_ids:
        value_id = graph.nodes[c_id].get("value_id")
        if (
            value_id
            and (value := graph.nodes[value_id].get("value"))
            and ("ng serve " in value or "ng s " in value)
            and " --disable-host-check" in value
        ):
            yield c_id


def allowed_hosts(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    if not shard.path.endswith("appsettings.json"):
        return MethodExecutionResult(vulnerabilities=())

    method = MethodsEnum.JSON_ALLOWED_HOSTS

    def n_ids() -> Iterator[NId]:
        if shard.path.endswith("appsettings.json"):
            graph = shard.syntax_graph
            for nid in method_supplies.selected_nodes:
                key, value = get_key_value(graph, nid)
                if key == "AllowedHosts" and value == "*":
                    yield nid

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def disable_host_check(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    method = MethodsEnum.JSON_DISABLE_HOST_CHECK

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for nid in method_supplies.selected_nodes:
            key = graph.nodes[graph.nodes[nid]["key_id"]]["value"]
            if key != "scripts":
                continue
            yield from _scripts_with_insecure_flag(graph, graph.nodes[nid]["value_id"])

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
