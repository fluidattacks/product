from lib_sast.sast_model import (
    Graph,
    MethodSupplies,
    NId,
)
from lib_sast.utils import (
    graph as g,
)


def _has_dangerous_literal(graph: Graph, args: dict) -> bool:
    if (  # noqa: SIM103
        len(args) == 2
        and (scnd_param := graph.nodes[args["__1__"]])
        and (scnd_param.get("label_type") == "Literal")
        and scnd_param.get("value") == "*"
    ):
        return True
    return False


def has_dangerous_param(graph: Graph, method_supplies: MethodSupplies) -> list[NId]:
    sensitive_methods = {"contentWindow.postMessage"}
    return [
        member
        for member in method_supplies.selected_nodes
        if (
            graph.nodes[member].get("expression")[-25:] in sensitive_methods
            and (args_id := graph.nodes[member].get("arguments_id"))
            and (args := g.match_ast(graph, args_id))
            and (_has_dangerous_literal(graph, args))
        )
    ]
