from lib_sast.root.f060.c_sharp import (
    c_sharp_http_listener_wildcard as _c_sharp_http_listener_wildcard,
)
from lib_sast.root.f060.conf_files import (
    allowed_hosts as _json_allowed_hosts,
)
from lib_sast.root.f060.conf_files import (
    disable_host_check as _json_disable_host_check,
)
from lib_sast.root.f060.java import (
    java_mongo_hostname_verification_disabled as _java_mongo_hostname_verification_disabled,
)
from lib_sast.root.f060.java import java_noophostnameverifier_use as _java_noophostnameverifier_use
from lib_sast.root.f060.java import java_unsafe_hostname_verifier as _java_unsafe_hostname_verifier
from lib_sast.root.f060.javascript import (
    unsafe_origin as _js_unsafe_origin,
)
from lib_sast.root.f060.php import (
    php_insecure_ssl_tls_http as _php_insecure_ssl_tls_http,
)
from lib_sast.root.f060.python import (
    python_unsafe_ssl_hostname as _python_unsafe_ssl_hostname,
)
from lib_sast.root.f060.typescript import (
    unsafe_origin as _ts_unsafe_origin,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_http_listener_wildcard(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_http_listener_wildcard(shard, method_supplies)


@SHIELD_BLOCKING
def json_allowed_hosts(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _json_allowed_hosts(shard, method_supplies)


@SHIELD_BLOCKING
def json_disable_host_check(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _json_disable_host_check(shard, method_supplies)


@SHIELD_BLOCKING
def js_unsafe_origin(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _js_unsafe_origin(shard, method_supplies)


@SHIELD_BLOCKING
def java_mongo_hostname_verification_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_mongo_hostname_verification_disabled(shard, method_supplies)


@SHIELD_BLOCKING
def java_unsafe_hostname_verifier(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_unsafe_hostname_verifier(shard, method_supplies)


@SHIELD_BLOCKING
def java_noophostnameverifier_use(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_noophostnameverifier_use(shard, method_supplies)


@SHIELD_BLOCKING
def php_insecure_ssl_tls_http(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _php_insecure_ssl_tls_http(shard, method_supplies)


@SHIELD_BLOCKING
def python_unsafe_ssl_hostname(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_unsafe_ssl_hostname(shard, method_supplies)


@SHIELD_BLOCKING
def ts_unsafe_origin(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _ts_unsafe_origin(shard, method_supplies)
