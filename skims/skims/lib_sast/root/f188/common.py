from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.symbolic_eval.evaluate import (
    evaluate,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_d,
)


def is_insec_invocation(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    for path in get_backward_paths(graph, n_id):
        evaluation = evaluate(method, graph, path, n_id)
        if evaluation and evaluation.triggers != {"origin_comparison"}:
            return True
    return False


def is_message_on_args(
    graph: Graph,
    n_id: NId,
) -> bool:
    return bool(
        (arg_list := match_ast_d(graph, n_id, "ArgumentList"))
        and (args_childs := adj_ast(graph, arg_list))
        and (len(args_childs) > 1)
        and (graph.nodes[args_childs[0]].get("value") == "message")
        and (graph.nodes[args_childs[1]]["label_type"] == "MethodDeclaration"),
    )


def has_dangerous_param(graph: Graph, n_id: NId, method: MethodsEnum) -> NId | None:
    sensitive_methods = {"window.addEventListener"}
    if (
        graph.nodes[n_id].get("expression") in sensitive_methods
        and is_message_on_args(graph, n_id)
        and is_insec_invocation(graph, n_id, method)
    ):
        return n_id
    return None
