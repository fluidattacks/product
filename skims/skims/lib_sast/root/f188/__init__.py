from lib_sast.root.f188.javascript import (
    js_lack_of_validation_dom_window as _js_lack_of_validation_dom_window,
)
from lib_sast.root.f188.typescript import (
    ts_lack_of_validation_dom_window as _ts_lack_of_validation_dom_window,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def js_lack_of_validation_dom_window(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_lack_of_validation_dom_window(shard, method_supplies)


@SHIELD_BLOCKING
def ts_lack_of_validation_dom_window(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_lack_of_validation_dom_window(shard, method_supplies)
