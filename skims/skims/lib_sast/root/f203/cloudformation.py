from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_list_from_node,
    get_optional_attribute,
    yield_statements_from_policy,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)
from utils.aws.iam import (
    ACTIONS,
)


def _has_danger_actions(graph: Graph, nid: NId) -> bool:
    if (  # noqa: SIM103
        (actions := get_optional_attribute(graph, nid, "Action"))
        and (actions_list := get_list_from_node(graph, actions[2]))
        and (
            any(action.split(":")[-1] not in ACTIONS["s3"]["read"] for action in actions_list)
            or "s3:*" in actions_list
        )
    ):
        return True
    return False


def _unauthorized_bucket_policy(graph: Graph, nid: NId) -> Iterator[NId]:
    if (
        (props := get_optional_attribute(graph, nid, "Properties"))
        and (val_id := graph.nodes[props[2]]["value_id"])
        and (pol_doc := get_optional_attribute(graph, val_id, "PolicyDocument"))
    ):
        for stmt in yield_statements_from_policy(graph, graph.nodes[pol_doc[2]]["value_id"]):
            if (
                (effect := get_optional_attribute(graph, stmt, "Effect"))
                and effect[1] == "Allow"
                and (principal := get_optional_attribute(graph, stmt, "Principal"))
                and principal[1] == "*"
                and _has_danger_actions(graph, stmt)
            ):
                yield stmt


def cfn_buckets_unauthorized_access(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_S3_BUCKETS_ALLOW_UNAUTHORIZED_PUBLIC_ACCESS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (resource := get_optional_attribute(graph, nid, "Type")) and resource[
                1
            ] == "AWS::S3::BucketPolicy":
                yield from _unauthorized_bucket_policy(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
