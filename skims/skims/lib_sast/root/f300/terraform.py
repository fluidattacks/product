from collections.abc import (
    Iterator,
)
from itertools import chain

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_argument,
    get_argument_iterator,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)

resources = {
    "azurerm_app_service",
    "azurerm_windows_web_app",
    "azurerm_linux_web_app",
}


def _certificates_enabled(graph: Graph, nid: NId, azure_client_type: str) -> NId | None:
    if azure_client_type == "azurerm_app_service":
        enabled = get_optional_attribute(graph, nid, "client_cert_enabled")
    else:
        enabled = get_optional_attribute(graph, nid, "client_certificate_enabled")

    if not enabled:
        return nid
    if enabled[1].lower() == "false":
        return enabled[2]
    return None


def tfm_azure_as_client_certificates_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AZURE_AS_CLIENT_CERTIFICATES_ENABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            name = graph.nodes[nid].get("name")
            if name and name in resources and (vuln_id := _certificates_enabled(graph, nid, name)):
                yield vuln_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_azure_app_service_authentication_is_not_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AZURE_APP_SERVICE_AUTHENTICATION_IS_NOT_ENABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        resource_nids = list(
            filter(
                lambda nid: graph.nodes[nid].get("name") in resources,
                method_supplies.selected_nodes,
            ),
        )
        without_auth = list(
            filter(
                lambda nid: get_argument(graph, nid, "auth_settings") is None,
                resource_nids,
            ),
        )
        with_auth = set(resource_nids) - set(without_auth)
        auth_settings = list(
            chain.from_iterable(
                get_argument_iterator(graph, nid, "auth_settings") for nid in with_auth
            ),
        )
        auth_disabled: list[NId] = list(
            filter(
                lambda nid: (attr := get_optional_attribute(graph, nid, "enabled")) is not None
                and attr[1] == "false",
                auth_settings,
            ),
        )
        yield from without_auth + auth_disabled

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_redis_cache_authnotrequired_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_REDIS_CACHE_AUTHNOTREQUIRED_ENABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        resource_nids = list(
            filter(
                lambda nid: graph.nodes[nid].get("name") == "azurerm_redis_cache",
                method_supplies.selected_nodes,
            ),
        )
        redis_configuration = list(
            chain.from_iterable(
                get_argument_iterator(graph, nid, "redis_configuration") for nid in resource_nids
            ),
        )
        auth_disabled: list[NId] = [
            attr[2]
            for nid in redis_configuration
            if (attr := get_optional_attribute(graph, nid, "authentication_enabled"))
            and attr[1] == "false"
        ]
        yield from auth_disabled

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_azure_dev_portal_has_auth_methods_inactive(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AZURE_DEV_PORTAL_HAS_AUTH_METHODS_INACTIVE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        resource_nids = list(
            filter(
                lambda nid: graph.nodes[nid].get("name") == "azurerm_api_management",
                method_supplies.selected_nodes,
            ),
        )
        without_sign_in = list(
            filter(
                lambda nid: get_argument(graph, nid, "sign_in") is None,
                resource_nids,
            ),
        )
        with_sign_in = set(resource_nids) - set(without_sign_in)
        sign_in = list(
            chain.from_iterable(
                get_argument_iterator(graph, nid, "sign_in") for nid in with_sign_in
            ),
        )
        sign_in_disabled: list[NId] = [
            attr[2]
            for nid in sign_in
            if (attr := get_optional_attribute(graph, nid, "enabled")) and attr[1] == "false"
        ]
        yield from without_sign_in + sign_in_disabled

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
