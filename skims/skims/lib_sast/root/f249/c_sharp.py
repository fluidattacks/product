import re
from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    is_node_definition_unsafe,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def is_hardcoded_password(graph: Graph, n_id: NId) -> bool:
    pwd_pattern = re.compile(r'(.*;(Password|pwd|password)=(?!<.*>)(?!")(?!;))')
    return (
        graph.nodes[n_id]["label_type"] == "Literal"
        and (arg_value := graph.nodes[n_id].get("value"))
        and pwd_pattern.match(arg_value) is not None
    )


def c_sharp_sql_conn_hardcoded_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_SQL_CONN_HARDCODED_SECRET

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("name", "") == "SqlConnection"
                and (first_arg := get_n_arg(graph, n_id, 0))
                and is_node_definition_unsafe(graph, first_arg, is_hardcoded_password)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
