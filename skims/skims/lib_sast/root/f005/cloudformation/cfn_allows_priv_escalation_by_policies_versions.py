from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_attribute,
    get_list_from_node,
    iterate_iam_policy_resources,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _allows_priv_escalation_by_policies_versions(graph: Graph, nid: NId) -> bool:
    effect, effect_val, _ = get_attribute(graph, nid, "Effect")
    action, _, action_id = get_attribute(graph, nid, "Action")
    resource, _, _ = get_attribute(graph, nid, "Resource")
    if effect and action and effect_val == "Allow" and resource:
        action_list = get_list_from_node(graph, action_id)
        if (
            "iam:CreatePolicyVersion" in action_list
            and "iam:SetDefaultPolicyVersion" in action_list
        ):
            return True
    return False


def cfn_allows_priv_escalation_by_policies_versions(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_ALLOWS_PRIV_ESCALATION_BY_POLICIES_VERSIONS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for nid in iterate_iam_policy_resources(graph, method_supplies):
            if _allows_priv_escalation_by_policies_versions(graph, nid):
                yield nid

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
