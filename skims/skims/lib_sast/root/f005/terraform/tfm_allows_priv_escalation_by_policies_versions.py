from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_dict_from_attr,
    get_dict_values,
    get_list_from_node,
    get_optional_attribute,
    iter_statements_from_policy_document,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _allows_priv_escalation_by_policies_versions_resource(graph: Graph, nid: NId) -> Iterator[NId]:
    for stmt in iter_statements_from_policy_document(graph, nid):
        effect = get_optional_attribute(graph, stmt, "effect")
        if effect and effect[1] != "Allow":
            continue
        resources = get_optional_attribute(graph, stmt, "resources")
        if not resources:
            continue

        if (
            (actions := get_optional_attribute(graph, stmt, "actions"))
            and (action_list := get_list_from_node(graph, actions[2]))
            and "iam:CreatePolicyVersion" in action_list
            and "iam:SetDefaultPolicyVersion" in action_list
        ):
            yield stmt


def _allows_priv_escalation_by_pol_versions_jsonencode(graph: Graph, nid: NId) -> Iterator[NId]:
    child_id = adj_ast(graph, graph.nodes[nid]["arguments_id"])[0]
    statements = get_optional_attribute(graph, child_id, "Statement")
    if not statements:
        return
    value_id = graph.nodes[statements[2]]["value_id"]
    for c_id in adj_ast(graph, value_id, label_type="Object"):
        effect = get_optional_attribute(graph, c_id, "Effect")
        if effect and effect[1] == "Allow":
            resource = get_optional_attribute(graph, c_id, "Resource")
            if not resource:
                continue

            actions = get_optional_attribute(graph, c_id, "Action")
            if (
                actions
                and (action_list := get_list_from_node(graph, actions[2]))
                and "iam:CreatePolicyVersion" in action_list
                and "iam:SetDefaultPolicyVersion" in action_list
            ):
                yield c_id


def _aux_allows_priv_escalation_by_policies_versions(attr_val: str, attr_id: NId) -> Iterator[NId]:
    dict_value = get_dict_from_attr(attr_val)
    statements = get_dict_values(dict_value, "Statement")
    for stmt in statements if isinstance(statements, list) else []:
        effect = stmt.get("Effect")
        actions = stmt.get("Action", [])
        resource = stmt.get("Resource")
        if (
            effect == "Allow"
            and "iam:CreatePolicyVersion" in actions
            and "iam:SetDefaultPolicyVersion" in actions
            and resource
        ):
            yield attr_id


def _allows_priv_escalation_by_policies_versions(graph: Graph, nid: NId) -> Iterator[NId]:
    if graph.nodes[nid]["name"] == "aws_iam_policy_document":
        yield from _allows_priv_escalation_by_policies_versions_resource(graph, nid)
    else:
        pol_attr = get_optional_attribute(graph, nid, "policy")
        if not pol_attr:
            return

        value_id = graph.nodes[pol_attr[2]]["value_id"]
        if graph.nodes[value_id]["label_type"] == "Literal":
            yield from _aux_allows_priv_escalation_by_policies_versions(pol_attr[1], pol_attr[2])
        elif (
            graph.nodes[value_id]["label_type"] == "MethodInvocation"
            and graph.nodes[value_id]["expression"] == "jsonencode"
        ):
            yield from _allows_priv_escalation_by_pol_versions_jsonencode(graph, value_id)


def _allows_priv_escalation_by_policies_versions_interpolated(
    graph: Graph,
    array_parent: NId,
) -> bool:
    parent = pred_ast(graph, array_parent)[0]
    effect = get_optional_attribute(graph, parent, "Effect")
    if not effect or effect[1] != "Allow":
        return False

    resource = get_optional_attribute(graph, parent, "Resource")
    if not resource:
        return False

    actions_list = get_list_from_node(graph, array_parent)
    return (
        "iam:CreatePolicyVersion" in actions_list and "iam:SetDefaultPolicyVersion" in actions_list
    ) or "iam:*" in actions_list


def tfm_allows_priv_escalation_by_policies_versions(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_ALLOWS_PRIV_ESCALATION_BY_POLICIES_VERSIONS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in {
                "aws_iam_group_policy",
                "aws_iam_policy",
                "aws_iam_role_policy",
                "aws_iam_user_policy",
                "aws_iam_policy_document",
            }:
                yield from _allows_priv_escalation_by_policies_versions(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_escalation_versions_interpolated(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_ALLOWS_PRIV_ESCALATION_BY_POLICIES_VERSIONS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (
                (array_parent := pred_ast(graph, nid)[0])
                and (is_action := graph.nodes[array_parent].get("key_id"))
                and graph.nodes[is_action]["value"] == "Action"
                and _allows_priv_escalation_by_policies_versions_interpolated(graph, array_parent)
            ):
                yield nid

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
