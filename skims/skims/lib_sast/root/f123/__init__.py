from lib_sast.root.f123.typescript.ts_file_unauthorized_access import (
    ts_file_unauthorized_access as _ts_file_unauthorized_access,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def ts_file_unauthorized_access(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_file_unauthorized_access(shard, method_supplies)
