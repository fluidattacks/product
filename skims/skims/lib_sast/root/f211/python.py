from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils.graph import (
    adj_ast,
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def is_vuln_regex(method: MethodsEnum, graph: Graph, al_id: NId) -> bool:
    args_ids = adj_ast(graph, al_id)
    return (
        len(args_ids) >= 2
        and get_node_evaluation_results(method, graph, args_ids[0], {"danger_regex"})
        and get_node_evaluation_results(method, graph, args_ids[1], {"userparams"})
    )


def python_regex_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PYTHON_REGEX_INJECTION
    danger_set = {"re.match", "re.findall", "re.search"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            parent_id = pred_ast(graph, n_id)[0]
            if (
                f"{n_attrs['expression']}.{n_attrs['member']}" in danger_set
                and (al_id := graph.nodes[parent_id].get("arguments_id"))
                and is_vuln_regex(method, graph, al_id)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def python_regex_dos(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    method = MethodsEnum.PYTHON_REGEX_DOS
    danger_set = {"re.match", "re.findall", "re.search"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            expr = f"{n_attrs['expression']}.{n_attrs['member']}"
            parent_id = pred_ast(graph, n_id)[0]
            if (
                expr in danger_set
                and (al_id := graph.nodes[parent_id].get("arguments_id"))
                and (args_ids := adj_ast(graph, al_id))
                and len(args_ids) >= 2
                and get_node_evaluation_results(method, graph, args_ids[0], {"userparams"})
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
