from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    evaluate,
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    adj_ast,
    get_ast_children,
    match_ast,
    matching_nodes,
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def get_regex_node(graph: Graph, expr: str) -> NId | None:
    if regex_name := expr.split(".")[0]:
        for vid in matching_nodes(graph, label_type="VariableDeclaration"):
            if graph.nodes[vid].get("variable") == regex_name:
                return vid
    return None


def sets_timespan(method: MethodsEnum, graph: Graph, n_id: NId) -> bool:
    for path in get_backward_paths(graph, n_id):
        evaluation = evaluate(method, graph, path, n_id)
        if evaluation and "hastimespan" in evaluation.triggers:
            return True
    return False


def is_pattern_danger(method: MethodsEnum, graph: Graph, n_id: NId) -> bool:
    for path in get_backward_paths(graph, n_id):
        evaluation = evaluate(method, graph, path, n_id)
        if evaluation and evaluation.danger and "safepattern" not in evaluation.triggers:
            return True
    return False


def is_regex_vuln(method: MethodsEnum, graph: Graph, n_id: NId) -> bool:
    obj_c = match_ast(graph, n_id, "ObjectCreation").get("ObjectCreation")
    if (
        obj_c
        and graph.nodes[obj_c].get("name") == "Regex"
        and (al_id := get_ast_children(graph, obj_c, "ArgumentList")[0])
    ):
        args_n_ids = adj_ast(graph, al_id)
        regpat_nid = args_n_ids[0]
        is_danger_pattern = is_pattern_danger(method, graph, regpat_nid)
        has_timespan = False
        if len(args_n_ids) == 3:
            timespan_nid = args_n_ids[2]
            has_timespan = sets_timespan(method, graph, timespan_nid)

        return is_danger_pattern and not has_timespan

    return False


def analyze_method_vuln(method: MethodsEnum, graph: Graph, method_id: NId) -> bool:
    method_n = graph.nodes[method_id]
    expr = method_n.get("expression")
    args_id = get_ast_children(graph, method_id, "ArgumentList")
    if args_id:
        args_n_ids = adj_ast(graph, args_id[0])

        if len(args_n_ids) == 0 or not get_node_evaluation_results(
            method,
            graph,
            args_n_ids[0],
            {"user_parameters", "user_connection"},
            danger_goal=False,
        ):
            return False

        if len(args_n_ids) == 1:
            is_danger_method = True
        elif len(args_n_ids) >= 2:
            reg_pat_nid = args_n_ids[1]
            is_danger_method = is_pattern_danger(method, graph, reg_pat_nid)

        if len(args_n_ids) == 4:
            timespan_nid = args_n_ids[3]
            is_danger_method = not sets_timespan(method, graph, timespan_nid)

        if (
            is_danger_method
            and (regex_node := get_regex_node(graph, expr))
            and is_regex_vuln(method, graph, regex_node)
        ):
            return True
    return False


def c_sharp_vuln_regular_expression(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CSHARP_VULN_REGULAR_EXPRESSION
    regex_methods = {"IsMatch", "Match", "Matches"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            method_tuple = pred_ast(graph, node)
            method_id = method_tuple[0] if len(method_tuple) > 0 else None
            if (
                method_id
                and graph.nodes[node].get("member") in regex_methods
                and analyze_method_vuln(method, graph, method_id)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def c_sharp_regex_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CSHARP_REGEX_INJECTION
    danger_methods = {"Regex"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if (
                graph.nodes[node].get("expression") in danger_methods
                and graph.nodes[node]["member"] == "Match"
                and (parent_id := pred_ast(graph, node)[0])
                and get_node_evaluation_results(method, graph, parent_id, set())
            ):
                yield parent_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
