from collections.abc import (
    Iterator,
)
from itertools import chain

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_argument,
    get_argument_iterator,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def tfm_azure_db_postgresql_insecure_log_retention_days(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AZURE_DB_POSTGRESQL_INSECURE_LOG_RETENTION_DAYS

    def n_ids() -> Iterator[NId]:
        def is_unsafe_retention(value: str) -> bool:
            return value.isdigit() and int(value) < 3

        graph = shard.syntax_graph
        resource_nids = list(
            filter(
                lambda nid: graph.nodes[nid].get("name") == "azurerm_postgresql_configuration",
                method_supplies.selected_nodes,
            ),
        )
        retention_days: list[NId] = [
            nid
            for nid in resource_nids
            if (attr := get_optional_attribute(graph, nid, "name"))
            and attr[1] == "log_retention_days"
        ]
        unsafe_retention_days: list[NId] = [
            attr[2]
            for nid in retention_days
            if (attr := get_optional_attribute(graph, nid, "value"))
            and is_unsafe_retention(attr[1])
        ]

        yield from unsafe_retention_days

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_azure_storage_account_geo_replication_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AZURE_STORAGE_ACCOUNT_GEO_REPLICATION_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        resource_nids = list(
            filter(
                lambda nid: graph.nodes[nid].get("name") == "azurerm_storage_account",
                method_supplies.selected_nodes,
            ),
        )
        account_replication_type: list[NId] = [
            attr[2]
            for nid in resource_nids
            if (attr := get_optional_attribute(graph, nid, "account_replication_type"))
            and attr[1] not in {"GRS", "RAGRS", "GZRS", "RAGZRS"}
        ]

        yield from account_replication_type

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_azure_storage_account_blob_soft_delete_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AZURE_STORAGE_ACCOUNT_BLOB_SOFT_DELETE_DISABLED

    def n_ids() -> Iterator[NId]:
        def is_unsafe_retention(value: str) -> bool:
            return value.isdigit() and int(value) < 7

        graph = shard.syntax_graph
        resource_nids = list(
            filter(
                lambda nid: graph.nodes[nid].get("name") == "azurerm_storage_account",
                method_supplies.selected_nodes,
            ),
        )
        without_blob = list(
            filter(
                lambda nid: get_argument(graph, nid, "blob_properties") is None,
                resource_nids,
            ),
        )
        with_blob = set(resource_nids) - set(without_blob)
        retention_policy = chain.from_iterable(
            get_argument_iterator(graph, nid, "delete_retention_policy")
            for nid in chain.from_iterable(
                get_argument_iterator(graph, n, "blob_properties") for n in with_blob
            )
        )
        unsafe_retention_days: list[NId] = [
            attr[2]
            for nid in retention_policy
            if (attr := get_optional_attribute(graph, nid, "days")) and is_unsafe_retention(attr[1])
        ]
        yield from without_blob + unsafe_retention_days

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_azure_key_vault_purge_protection_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AZURE_KEY_VAULT_PURGE_PROTECTION_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        resource_nids = list(
            filter(
                lambda nid: graph.nodes[nid].get("name") == "azurerm_key_vault",
                method_supplies.selected_nodes,
            ),
        )
        without_purge = list(
            filter(
                lambda nid: get_optional_attribute(graph, nid, "purge_protection_enabled") is None,
                resource_nids,
            ),
        )
        with_purge = set(resource_nids) - set(without_purge)
        unsafe_purge: list[NId] = [
            attr[2]
            for nid in with_purge
            if (attr := get_optional_attribute(graph, nid, "purge_protection_enabled"))
            and attr[1] == "false"
        ]

        yield from without_purge + unsafe_purge

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
