from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _get_member(graph: Graph, n_id: NId) -> str:
    exp_id = graph.nodes[n_id].get("expression_id")
    if exp_id is None:
        return ""
    return graph.nodes[exp_id].get("member", "")


def _is_sensitive_request(graph: Graph, n_id: NId) -> bool:
    if (
        _get_member(graph, n_id) == "request"
        and (protocol_id := get_n_arg(graph, n_id, 0))
        and graph.nodes[protocol_id].get("value") == "GET"
        and (methods := get_n_arg(graph, n_id, 2))
    ):
        for method_id in adj_ast(graph, methods, label_type="Pair"):
            key_id = graph.nodes[method_id]["key_id"]
            key = graph.nodes[key_id].get("value")
            if key == "query":
                return True

    return False


def _is_insecure_protocol(graph: Graph, n_id: NId) -> bool:
    if (  # noqa: SIM103
        _get_member(graph, n_id) == "request"
        and (url_id := get_n_arg(graph, n_id, 1))
        and (url := graph.nodes[url_id].get("value"))
        and str(url).startswith("http://")
    ):
        return True
    return False


def _eval_sanitization(graph: Graph, n_ids: list[NId]) -> bool:
    for n_id in n_ids:
        symbol = graph.nodes[n_id].get("symbol")
        if not symbol:
            continue

        for path in get_backward_paths(graph, n_id):
            def_id = definition_search(graph, path, symbol)
            if (
                def_id
                and (val_id := graph.nodes[def_id].get("value_id"))
                and (member := _get_member(graph, val_id))
                and member == "input"
            ):
                return True
    return False


def _is_insecure_sanitize(graph: Graph, n_id: NId) -> bool:
    parameters: list[NId] = []
    if _get_member(graph, n_id) == "request" and (methods := get_n_arg(graph, n_id, 2)):
        for method_id in adj_ast(graph, methods, label_type="Pair"):
            value_id = graph.nodes[method_id]["value_id"]
            for info in adj_ast(graph, value_id, label_type="Pair"):
                vid = graph.nodes[info]["value_id"]
                parameters.append(vid)
    return _eval_sanitization(graph, parameters)


def php_sensitive_http_sent(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PHP_SENSITIVE_HTTP_SENT

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                _is_insecure_sanitize(graph, n_id)
                or _is_insecure_protocol(graph, n_id)
                or _is_sensitive_request(graph, n_id)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
