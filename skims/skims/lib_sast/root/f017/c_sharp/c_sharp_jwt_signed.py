from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils import (
    graph as g,
)
from model.core import (
    MethodExecutionResult,
)


def _check_pred(graph: Graph, depth: int = 1, elem_jwt: str = "0") -> bool:
    pred = g.pred(graph, elem_jwt, depth)[0]
    if (
        graph.nodes[pred].get("label_type") == "MemberAccess"
        and graph.nodes[pred].get("member") == "MustVerifySignature"
    ):
        return True
    if graph.nodes[pred].get("label_type") != "VariableDeclaration":
        signed = _check_pred(graph, depth + 1, pred)
    else:
        return False
    return signed


def c_sharp_jwt_signed_object_creation(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_JWT_SIGNED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        nodes = graph.nodes
        for node in method_supplies.selected_nodes:
            if (
                (
                    (obj_name := nodes[node].get("name"))
                    or (obj_name := nodes[node].get("expression"))
                )
                and obj_name == "JwtBuilder"
                and not _check_pred(
                    graph,
                    elem_jwt=node,
                )
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
