from collections.abc import (
    Callable,
    Iterable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.symbolic_eval.utils import (
    get_object_identifiers,
)
from lib_sast.utils import (
    graph as g,
)
from model.core import (
    MethodExecutionResult,
)


def _literal_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id]["value"] == "false":
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "literal": _literal_evaluator,
}


def _is_insecure_decoder(
    method: MethodsEnum,
    graph: Graph,
    n_id: str,
    obj_identifiers: Iterable[str],
) -> bool:
    exp = graph.nodes[n_id]["expression"]
    member = graph.nodes[n_id]["member"]
    if (  # noqa: SIM103
        exp in obj_identifiers
        and member == "Decode"
        and (al_id := graph.nodes[g.pred(graph, n_id)[0]].get("arguments_id"))
        and (test_nid := g.match_ast(graph, al_id).get("__2__"))
        and get_node_evaluation_results(
            method,
            graph,
            test_nid,
            set(),
            method_evaluators=METHOD_EVALUATORS,
        )
    ):
        return True
    return False


def c_sharp_verify_decoder(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_VERIFY_DECODER

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        obj_jwt = get_object_identifiers(graph, {"JwtDecoder"})
        if not obj_jwt:
            return

        for node in method_supplies.selected_nodes:
            if _is_insecure_decoder(method, graph, node, obj_jwt):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
