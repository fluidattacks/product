from lib_sast.root.f159.java.java_dangerous_permission_combination import (
    java_dangerous_permission_combination as _java_dangerous_permission_combination,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def java_dangerous_permission_combination(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_dangerous_permission_combination(shard, method_supplies)
