from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    has_string_definition,
)
from lib_sast.root.utilities.java import (
    is_any_library_imported_prefix,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def java_dangerous_permission_combination(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_DANGEROUS_PERMISSION_COMBINATION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported_prefix(
            graph,
            (
                "java.security.RuntimePermission",
                "java.lang.reflect.ReflectPermission",
            ),
        ):
            return
        bad_combinations = {
            "ReflectPermission": "suppressAccessChecks",
            "RuntimePermission": "createClassLoader",
        }
        for n_id in method_supplies.selected_nodes:
            if (
                (obj_name := graph.nodes[n_id].get("name", "")) in bad_combinations
                and (f_arg := get_n_arg(graph, n_id, 0))
                and has_string_definition(graph, f_arg) == bad_combinations.get(obj_name)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
