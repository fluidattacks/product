from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.utilities.javascript import (
    file_imports_module,
)
from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils import (
    graph as g,
)
from utils.string_handlers import (
    split_on_last_dot,
)


def get_vuln_nodes(graph: Graph, nid: NId, method: MethodsEnum) -> bool:
    f_name = split_on_last_dot(graph.nodes[nid]["expression"])
    if (  # noqa: SIM103
        f_name[1] == "parseXmlString"
        and (args := g.match_ast_d(graph, nid, "ArgumentList"))
        and (childs := g.adj_ast(graph, args))
        and len(childs) > 1
        and get_node_evaluation_results(method, graph, childs[1], set())
    ):
        return True
    return False


def is_var_used_to_create_context(graph: Graph, nid: NId) -> bool:
    for path in get_backward_paths(graph, nid):
        for child_id in path:
            if graph.nodes[child_id][
                "label_type"
            ] == "MethodInvocation" and ".createContext" in graph.nodes[child_id].get(
                "expression",
                "",
            ):
                return True
    return False


def xml_parser_inside_context(graph: Graph, nid: NId, method: MethodsEnum) -> bool:
    if not file_imports_module(graph, "vm") or not file_imports_module(graph, "libxmljs2"):
        return False
    f_name = split_on_last_dot(graph.nodes[nid]["expression"])
    if (
        f_name[1] == "runInContext"
        and (args := g.match_ast_d(graph, nid, "ArgumentList"))
        and (childs := g.adj_ast(graph, args))
        and len(childs) > 1
    ):
        exec_command = graph.nodes[childs[0]].get("value", "")
        if (
            ".parseXml(" in exec_command
            and "noent: true" in exec_command
            and get_node_evaluation_results(method, graph, childs[1], set())
            and is_var_used_to_create_context(graph, childs[1])
        ):
            return True
    return False
