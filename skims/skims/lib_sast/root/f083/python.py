from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils import (
    graph as g,
)
from model.core import (
    MethodExecutionResult,
)


def get_eval_danger(graph: Graph, n_ids: list[NId], method: MethodsEnum) -> bool:
    for _id in n_ids:
        if graph.nodes[_id]["argument_name"] != "resolve_entities":
            continue
        val_id = graph.nodes[_id]["value_id"]
        return get_node_evaluation_results(method, graph, val_id, set())
    return False


def is_xml_parser_vuln(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    al_id = g.match_ast_d(graph, n_id, "ArgumentList")
    if al_id and (  # noqa: SIM103
        not (args := g.match_ast_group_d(graph, al_id, "NamedArgument"))
        or get_eval_danger(graph, args, method)
    ):
        return True
    return False


def python_xml_parser(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    method = MethodsEnum.PYTHON_XML_PARSER

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if graph.nodes[n_id]["expression"] == "etree.XMLParser" and is_xml_parser_vuln(
                graph,
                n_id,
                method,
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
