from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    is_node_definition_unsafe,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.utils import (
    get_object_identifiers,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_d,
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)
from utils.string_handlers import (
    split_on_last_dot as split_last,
)


def c_sharp_xsl_transform_object(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_XSL_TRANSFORM_OBJECT

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if graph.nodes[node].get("name") == "XslTransform":
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def c_sharp_schema_by_url(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_SCHEMA_BY_URL

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        obj_identifiers = get_object_identifiers(graph, {"XmlSchemaCollection"})

        if not obj_identifiers:
            return

        for n_id in method_supplies.selected_nodes:
            split_expr = split_last(graph.nodes[n_id].get("expression"))
            if (
                split_expr[0] in obj_identifiers
                and split_expr[1] == "Add"
                and (args := match_ast_d(graph, n_id, "ArgumentList"))
                and all(graph.nodes[_id]["label_type"] == "Literal" for _id in adj_ast(graph, args))
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def _is_xml_document(graph: Graph, n_id: NId) -> bool:
    return (
        graph.nodes[n_id].get("label_type") == "ObjectCreation"
        and graph.nodes[n_id].get("name") == "XmlDocument"
    )


def _is_xml_resolver(graph: Graph, n_id: NId) -> bool:
    return (
        graph.nodes[n_id].get("label_type") == "ObjectCreation"
        and graph.nodes[n_id].get("name") == "XmlUrlResolver"
    )


def is_insecure_load(graph: Graph, n_id: NId) -> bool:
    if (  # noqa: SIM103
        (expr_id := graph.nodes[n_id].get("expression_id"))
        and is_node_definition_unsafe(graph, expr_id, _is_xml_document)
        and (parent_id := pred_ast(graph, n_id)[0])
        and (var_value := graph.nodes[parent_id].get("value_id"))
        and is_node_definition_unsafe(graph, var_value, _is_xml_resolver)
    ):
        return True
    return False


def c_sharp_xxe_resolver(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_XXE_RESOLVER

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if graph.nodes[n_id].get("member") == "XmlResolver" and is_insecure_load(graph, n_id):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
