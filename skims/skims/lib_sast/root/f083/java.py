from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.java import (
    is_any_library_imported_prefix,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.evaluate import (
    evaluate,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)

PARSER_INITIALIZERS = {
    "createXMLReader",
    "newDocumentBuilder",
    "newSAXParser",
    "DocumentBuilderFactory",
}


def is_sanitized(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    for path in get_backward_paths(graph, n_id):
        if (
            evaluation := evaluate(method, graph, path, n_id)
        ) and "sanitizedParser" in evaluation.triggers:
            return True

    return False


def is_parser_initializer(graph: Graph, n_id: NId) -> bool:
    return (
        graph.nodes[n_id]["label_type"] == "MethodInvocation"
        and graph.nodes[n_id]["expression"] in PARSER_INITIALIZERS
    )


def is_xml_parser(graph: Graph, obj_id: NId) -> bool:
    if graph.nodes[obj_id]["label_type"] == "SymbolLookup":
        symbol = graph.nodes[obj_id].get("symbol")
        for path in get_backward_paths(graph, obj_id):
            if (
                (def_id := definition_search(graph, path, symbol))
                and (val_id := graph.nodes[def_id].get("value_id"))
                and is_parser_initializer(graph, val_id)
            ):
                return True
    return False


def java_insecure_parser(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_XML_PARSER

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            expression = graph.nodes[node].get("expression")
            if (
                expression == "parse"
                and (object_id := graph.nodes[node].get("object_id"))
                and is_xml_parser(graph, object_id)
                and not is_sanitized(graph, object_id, method)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def is_external_entities_in_true(graph: Graph, n_id: NId) -> bool:
    if not (args_id := graph.nodes[n_id].get("arguments_id")):
        return False

    return bool(
        (arguments := adj_ast(graph, args_id, label_type="Literal"))
        and len(arguments) == 2
        and graph.nodes[arguments[0]].get("value")
        in {
            "http://xml.org/sax/features/external-general-entities",
            "http://xml.org/sax/features/external-parameter-entities",
        }
        and graph.nodes[arguments[1]].get("value_type") == "bool"
        and graph.nodes[arguments[1]].get("value") == "true",
    )


def java_allowed_external_entities(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_ALLOWED_EXTERNAL_ENTITIES

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if graph.nodes[n_id].get(
                "expression",
                "",
            ) == "setFeature" and is_external_entities_in_true(graph, n_id):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def is_xmlinputfactory_external_entities_in_true(graph: Graph, n_id: NId) -> bool:
    if not (args_id := graph.nodes[n_id].get("arguments_id")):
        return False

    return bool(
        (args := adj_ast(graph, args_id))
        and len(args) == 2
        and (
            graph.nodes[args[0]].get("value", "") == "javax.xml.stream.isSupportingExternalEntities"
            or graph.nodes[args[0]].get("symbol", "") == "XMLInputFactory.SUPPORT_DTD"
        )
        and graph.nodes[args[1]].get("value_type", "") == "bool"
        and graph.nodes[args[1]].get("value", "") == "true",
    )


def java_xmlinputfactory_external_entities(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_XMLINPUTFACTORY_EXTERNAL_ENTITIES

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported_prefix(graph, ("javax.xml.stream",)):
            return

        for n_id in method_supplies.selected_nodes:
            if graph.nodes[n_id].get(
                "expression",
                "",
            ) == "setProperty" and is_xmlinputfactory_external_entities_in_true(graph, n_id):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
