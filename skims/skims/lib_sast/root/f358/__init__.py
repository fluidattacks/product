from lib_sast.root.f358.c_sharp import (
    c_sharp_cert_validation_disabled as _c_sharp_cert_validation_disabled,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_cert_validation_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_cert_validation_disabled(shard, method_supplies)
