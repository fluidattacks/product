from lib_sast.root.f335.cloudformation import (
    cfn_s3_bucket_versioning_disabled as _cfn_s3_bucket_versioning_disabled,
)
from lib_sast.root.f335.terraform import (
    tfm_s3_bucket_versioning_disabled as _tfm_s3_bucket_versioning_disabled,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_s3_bucket_versioning_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_s3_bucket_versioning_disabled(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_s3_bucket_versioning_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_s3_bucket_versioning_disabled(shard, method_supplies)
