from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.utilities.javascript import (
    file_imports_module,
)
from lib_sast.sast_model import (
    Graph,
    MethodSupplies,
    NId,
)
from lib_sast.symbolic_eval.evaluate import (
    evaluate,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def get_eval_danger(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    if (
        (al_id := graph.nodes[n_id].get("arguments_id"))
        and (args_ids := adj_ast(graph, al_id))
        and len(args_ids) >= 1
        and graph.nodes[args_ids[0]]["label_type"] == "Literal"
        and len(adj_ast(graph, args_ids[0])) > 1
    ):
        return False
    if (
        graph.nodes[n_id]["label_type"] == "BinaryOperation"
        and (right_id := graph.nodes[n_id].get("right_id"))
        and graph.nodes[right_id].get("value", "").startswith(".")
    ):
        return False
    for path in get_backward_paths(graph, n_id):
        evaluation = evaluate(method, graph, path, n_id)
        if (
            evaluation
            and evaluation.danger
            and evaluation.triggers != {"resolve", "sanitize"}
            and "forceExtension" not in evaluation.triggers
        ):
            return True
    return False


def insecure_path_traversal(
    graph: Graph,
    method: MethodsEnum,
    method_supplies: MethodSupplies,
) -> list[NId]:
    vuln_nodes: list[NId] = []
    danger_methods = {
        "readdir",
        "readdirSync",
        "readFile",
        "readFileSync",
        "unlink",
        "unlinkSync",
        "writeFile",
        "writeFileSync",
    }

    if not file_imports_module(graph, "fs"):
        return vuln_nodes

    for n_id in method_supplies.selected_nodes:
        expr = graph.nodes[n_id].get("expression")
        if (
            expr.split(".")[-1] in danger_methods
            and (al_id := graph.nodes[n_id].get("arguments_id"))
            and (args_ids := adj_ast(graph, al_id))
            and len(args_ids) >= 1
            and get_eval_danger(graph, args_ids[0], method)
        ):
            vuln_nodes.append(n_id)
    return vuln_nodes
