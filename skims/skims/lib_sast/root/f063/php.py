from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.common import (
    PHP_INPUTS,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    Path,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def find_any_condition_or_method_in_path(graph: Graph, path: Path) -> bool:
    for node_in_path in path:
        node_label_type = graph.nodes[node_in_path].get("label_type")
        if node_label_type == "MethodInvocation":
            return True
        if node_label_type == "If":
            condition_id = graph.nodes[node_in_path].get("condition_id")
            nodes_in_condition = (condition_id, *adj_ast(graph, condition_id, -1))
            for child in nodes_in_condition:
                if graph.nodes[child].get("label_type") == "MethodInvocation":
                    return True
    return False


def check_node_is_php_input(graph: Graph, path: Path, unique_child: NId) -> bool:
    unique_child_symbol = graph.nodes[unique_child].get("symbol")
    if unique_child_symbol in PHP_INPUTS:
        return True
    definition_unique_child = definition_search(graph, path, unique_child_symbol)
    if definition_unique_child:
        for child in adj_ast(graph, definition_unique_child, -1):
            if (symbol := graph.nodes[child].get("symbol")) and symbol in PHP_INPUTS:
                return True
    return False


def check_for_lack_of_input_validation(graph: Graph, n_id: NId) -> bool:
    children = adj_ast(graph, n_id)
    if len(children) < 1:
        return False
    unique_child = children[0]
    for path in get_backward_paths(graph, unique_child):
        if not check_node_is_php_input(graph, path, unique_child):
            return False
        if not find_any_condition_or_method_in_path(graph, path):
            return True
    return False


def php_unsafe_path_traversal(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PHP_UNSAFE_PATH_TRAVERSAL
    danger_invocations = {"move_uploaded_file", "mkdir"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id]["label_type"] == "Import"
                and check_for_lack_of_input_validation(graph, n_id)
            ) or (
                (graph.nodes[n_id].get("expression") in danger_invocations)
                and get_node_evaluation_results(
                    method,
                    graph,
                    n_id,
                    {"user_input"},
                    danger_goal=False,
                )
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
