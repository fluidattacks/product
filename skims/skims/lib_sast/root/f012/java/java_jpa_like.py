import re
from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    evaluate,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    adj_ast,
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)

# Constants
WS = r"\s*"
SEP = f"{WS},{WS}"


def _literal_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    if args.graph.nodes[args.n_id]["value_type"] == "string":
        member_str = args.graph.nodes[args.n_id]["value"]
        if len(args.triggers) == 0:
            args.triggers.add(member_str)
        else:
            curr_value = next(iter(args.triggers))
            args.triggers.clear()
            args.triggers.add(curr_value + member_str)

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "literal": _literal_evaluator,
}


def _has_like_injection(statements: set[str]) -> bool:
    roots = (
        r"like\s+%{}",
        r"like\s+{}%",
        r"like\s+%{}%",
        rf"like\s+concat\('%'{SEP}{{}}\)",
        rf"like\s+concat\({{}}{SEP}'%'\)",
        rf"like\s+concat\('%'{SEP}{{}}{SEP}'%'\)",
    )
    variables = (
        r":\#\{\[\d+\]\}",
        r":[a-z0-9_\$]+",
        r"\?\d+",
    )

    expressions = [re.compile(root.format(var)) for var in variables for root in roots]

    return any(any(expr.search(statement) for expr in expressions) for statement in statements)


def _is_argument_vuln(method: MethodsEnum, graph: Graph, n_id: NId) -> bool:
    statements = set()
    for path in get_backward_paths(graph, n_id):
        if (
            (
                evaluation := evaluate(
                    method,
                    graph,
                    path,
                    n_id,
                    method_evaluators=METHOD_EVALUATORS,
                )
            )
            and (stmt := "".join(list(evaluation.triggers)))
            and ("like" in (statement := stmt.lower()))
        ):
            statements.add(statement)

    return _has_like_injection(statements)


def _analyze_jpa_node(method: MethodsEnum, graph: Graph, annotation_id: str) -> bool:
    args_n_id = graph.nodes[annotation_id].get("arguments_id")

    if not args_n_id:
        return False

    for n_id in adj_ast(graph, args_n_id):
        if _is_argument_vuln(method, graph, n_id):
            m_id = pred_ast(graph, annotation_id, depth=2)[1]
            pm_id = graph.nodes[m_id].get("parameters_id")
            annotations = adj_ast(graph, pm_id, depth=3, label_type="Annotation")
            for annotation in annotations:
                if graph.nodes[annotation].get("name") != "Bind":
                    return True
    return False


def java_jpa_like(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_JPA_LIKE
    danger_decorators = {"Query", "SqlQuery"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if (
                (expr := graph.nodes[node]["name"])
                and expr.rsplit(".", maxsplit=1)[-1] in danger_decorators
                and _analyze_jpa_node(method, graph, node)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
