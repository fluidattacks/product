from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    adj_ast,
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _method_invocation_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    ma_attr = args.graph.nodes[args.n_id]
    if ma_attr["expression"] == "Runtime.getRuntime":
        args.evaluation[args.n_id] = True
        args.triggers.add("getRuntime")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def _parameter_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id].get("variable_type") == "HttpServletRequest":
        args.evaluation[args.n_id] = True
        args.triggers.add("UserConnection")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "method_invocation": _method_invocation_evaluator,
    "parameter": _parameter_evaluator,
}


def kt_remote_command_exec(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.KT_REMOTE_COMMAND_EXECUTION
    danger_methods = {"loadLibrary", "exec"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if (
                graph.nodes[node]["member"] in danger_methods
                and (child := adj_ast(graph, node)[0])
                and get_node_evaluation_results(
                    method,
                    graph,
                    child,
                    {"getRuntime"},
                    method_evaluators=METHOD_EVALUATORS,
                )
                and (arg_list := graph.nodes[pred_ast(graph, node)[0]].get("arguments_id"))
                and get_node_evaluation_results(
                    method,
                    graph,
                    arg_list,
                    {"UserConnection"},
                    method_evaluators=METHOD_EVALUATORS,
                )
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
