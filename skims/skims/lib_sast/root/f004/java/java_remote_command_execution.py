from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.common import (
    owasp_user_connection,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from model.core import (
    MethodExecutionResult,
)


def _method_invocation_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    if owasp_user_connection(args):
        args.triggers.add("userparameters")

    ma_attr = args.graph.nodes[args.n_id]
    if (
        ma_attr["expression"] == "getRuntime"
        and (obj_id := ma_attr.get("object_id"))
        and args.graph.nodes[obj_id].get("symbol") == "Runtime"
    ):
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def _object_creation_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id]["name"] == "ProcessBuilder":
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def _parameter_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id].get("variable_type") == "HttpServletRequest":
        args.triggers.add("userconnection")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "method_invocation": _method_invocation_evaluator,
    "object_creation": _object_creation_evaluator,
    "parameter": _parameter_evaluator,
}


def _is_executing_builder(graph: Graph, n_id: NId) -> bool:
    if graph.nodes[n_id]["label_type"] == "SymbolLookup":
        symbol = graph.nodes[n_id]["symbol"]
        for path in get_backward_paths(graph, n_id):
            if (
                (def_id := definition_search(graph, path, symbol))
                and (val_id := graph.nodes[def_id].get("value_id"))
                and (graph.nodes[val_id]["label_type"] == "ObjectCreation")
                and graph.nodes[val_id].get("name") == "ProcessBuilder"
            ):
                return True
    return False


def java_remote_command_1(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_REMOTE_COMMAND_EXECUTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            n_attrs = graph.nodes[node]
            if (
                n_attrs["expression"] == "start"
                and not n_attrs.get("arguments_id")
                and (obj_id := n_attrs.get("object_id"))
                and _is_executing_builder(graph, obj_id)
                and get_node_evaluation_results(
                    method,
                    graph,
                    obj_id,
                    {"userparameters", "userconnection"},
                    graph_db=method_supplies.graph_db,
                    method_evaluators=METHOD_EVALUATORS,
                )
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def _is_executing_runtime(graph: Graph, n_id: NId) -> bool:
    if graph.nodes[n_id]["label_type"] == "SymbolLookup":
        symbol = graph.nodes[n_id]["symbol"]
        for path in get_backward_paths(graph, n_id):
            if (
                (def_id := definition_search(graph, path, symbol))
                and (val_id := graph.nodes[def_id].get("value_id"))
                and (graph.nodes[val_id]["label_type"] == "MethodInvocation")
                and graph.nodes[val_id].get("expression") == "getRuntime"
            ):
                return True
    return False


def java_remote_command_2(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_REMOTE_COMMAND_EXECUTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            n_attrs = graph.nodes[node]
            if (
                n_attrs["expression"] == "exec"
                and (obj_id := n_attrs.get("object_id"))
                and (al_id := n_attrs.get("arguments_id"))
                and _is_executing_runtime(graph, obj_id)
                and get_node_evaluation_results(
                    method,
                    graph,
                    al_id,
                    {"userparameters", "userconnection"},
                    danger_goal=False,
                    graph_db=method_supplies.graph_db,
                    method_evaluators=METHOD_EVALUATORS,
                )
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
