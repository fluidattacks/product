from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.common import (
    PHP_INPUTS,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from model.core import (
    MethodExecutionResult,
)


def _method_invocation_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    sanitize_methods = {"escapeshellcmd", "escapeshellarg"}
    if args.graph.nodes[args.n_id].get("expression") in sanitize_methods:
        args.triggers.add("sanitized")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def _element_access_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    nodes = args.graph.nodes
    if (exp_n_id := nodes[args.n_id].get("expression_id")) and (
        nodes[exp_n_id].get("symbol") in PHP_INPUTS
    ):
        args.triggers.add("user_input")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "method_invocation": _method_invocation_evaluator,
    "element_access": _element_access_evaluator,
}


def php_remote_command_execution(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PHP_REMOTE_COMMAND_EXECUTION
    danger_invocations = {
        "exec",
        "shell_exec",
        "system",
        "passthru",
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("expression") in danger_invocations
            ) and get_node_evaluation_results(
                method,
                graph,
                n_id,
                {"user_input"},
                danger_goal=False,
                method_evaluators=METHOD_EVALUATORS,
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
