from lib_sast.root.f250.cloudformation import (
    cfn_ec2_has_unencrypted_volumes as _cfn_ec2_has_unencrypted_volumes,
)
from lib_sast.root.f250.cloudformation import (
    cfn_ec2_unencrypted_ebs_block as _cfn_ec2_unencrypted_ebs_block,
)
from lib_sast.root.f250.terraform import (
    tfm_ebs_unencrypted_by_default as _tfm_ebs_unencrypted_by_default,
)
from lib_sast.root.f250.terraform import (
    tfm_ebs_unencrypted_volumes as _tfm_ebs_unencrypted_volumes,
)
from lib_sast.root.f250.terraform import (
    tfm_ec2_unencrypted_ebs_block as _tfm_ec2_unencrypted_ebs_block,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_ec2_has_unencrypted_volumes(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_ec2_has_unencrypted_volumes(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_ec2_unencrypted_ebs_block(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_ec2_unencrypted_ebs_block(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_ebs_unencrypted_by_default(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_ebs_unencrypted_by_default(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_ebs_unencrypted_volumes(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_ebs_unencrypted_volumes(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_ec2_unencrypted_ebs_block(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_ec2_unencrypted_ebs_block(shard, method_supplies)
