from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _ebs_is_unencrypted(graph: Graph, nid: NId) -> NId | None:
    if not (attr := get_optional_attribute(graph, nid, "encrypted")):
        return nid
    if attr[1].lower() == "false":
        return attr[2]
    return None


def _ec2_unencrypted_ebs_block(graph: Graph, nid: NId) -> Iterator[NId]:
    for c_id in adj_ast(graph, nid, label_type="Object"):
        if graph.nodes[c_id]["name"] in {
            "root_block_device",
            "ebs_block_device",
        } and (report_id := _ebs_is_unencrypted(graph, c_id)):
            yield report_id


def tfm_ec2_unencrypted_ebs_block(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_EC2_INSTANCE_UNENCRYPTED_EBS_BLOCK_DEVICES

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "aws_instance":
                yield from _ec2_unencrypted_ebs_block(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_ebs_unencrypted_volumes(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_EBS_UNENCRYPTED_VOLUMES

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "aws_ebs_volume" and (
                report := _ebs_is_unencrypted(graph, nid)
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_ebs_unencrypted_by_default(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_EBS_UNENCRYPTED_BY_DEFAULT

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (
                graph.nodes[nid].get("name") == "aws_ebs_encryption_by_default"
                and (attr := get_optional_attribute(graph, nid, "enabled"))
                and attr[1].lower() == "false"
            ):
                yield attr[2]

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
