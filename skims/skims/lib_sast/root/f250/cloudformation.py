from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    FALSE_OPTIONS,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _ec2_unencrypted_ebs_block(graph: Graph, nid: NId) -> Iterator[NId]:
    properties = get_optional_attribute(graph, nid, "Properties")
    if not properties:
        return
    val_id = graph.nodes[properties[2]]["value_id"]
    if maps := get_optional_attribute(graph, val_id, "BlockDeviceMappings"):
        mappings_attr = graph.nodes[maps[2]]["value_id"]
        for c_id in adj_ast(graph, mappings_attr):
            if ebs := get_optional_attribute(graph, c_id, "Ebs"):
                ebs_attrs = graph.nodes[ebs[2]]["value_id"]
                encrypted = get_optional_attribute(graph, ebs_attrs, "Encrypted")
                if not encrypted:
                    yield ebs[2]
                elif encrypted[1] in FALSE_OPTIONS:
                    yield encrypted[2]


def _ec2_has_unencrypted_volumes(graph: Graph, nid: NId) -> NId | None:
    danger_id = None
    if (props := get_optional_attribute(graph, nid, "Properties")) and (
        val_id := graph.nodes[props[2]]["value_id"]
    ):
        encrypted = get_optional_attribute(graph, val_id, "Encrypted")
        if not encrypted:
            danger_id = props[2]
        elif encrypted[1] in FALSE_OPTIONS:
            danger_id = encrypted[2]
        elif not get_optional_attribute(graph, val_id, "KmsKeyId"):
            danger_id = props[2]
    return danger_id


def cfn_ec2_has_unencrypted_volumes(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_EC2_HAS_UNENCRYPTED_VOLUMES

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (
                (resource := get_optional_attribute(graph, nid, "Type"))
                and resource[1] == "AWS::EC2::Volume"
                and (report := _ec2_has_unencrypted_volumes(graph, nid))
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def cfn_ec2_unencrypted_ebs_block(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_EC2_INSTANCE_UNENCRYPTED_EBS_BLOCK_DEVICES

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (resource := get_optional_attribute(graph, nid, "Type")) and resource[
                1
            ] == "AWS::EC2::Instance":
                yield from _ec2_unencrypted_ebs_block(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
