from collections.abc import Iterator

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    has_string_definition,
    is_node_definition_unsafe,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _is_url_or_uri_with_insecure_http(graph: Graph, n_id: NId) -> bool:
    return bool(
        graph.nodes[n_id].get("label_type") == "ObjectCreation"
        and graph.nodes[n_id].get("name", "") in {"URL", "URI"}
        and (first_arg := get_n_arg(graph, n_id, 0))
        and (arg_val := has_string_definition(graph, first_arg))
        and arg_val.startswith("http://")  # noqa: COM812
    )


def java_insecure_http_open_connection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_INSECURE_HTTP_OPEN_CONNECTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("expression", "") == "openConnection"
                and (obj_id := graph.nodes[n_id].get("object_id"))
                and is_node_definition_unsafe(graph, obj_id, _is_url_or_uri_with_insecure_http)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
