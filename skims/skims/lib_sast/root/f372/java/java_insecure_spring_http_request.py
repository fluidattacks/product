from collections.abc import Iterator

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    has_string_definition,
    is_node_definition_unsafe,
)
from lib_sast.root.utilities.java import (
    is_any_library_imported_prefix,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _is_http_url(graph: Graph, n_id: NId) -> bool:
    plain_http = bool((val := has_string_definition(graph, n_id)) and val.startswith("http://"))

    uri_http = bool(
        graph.nodes[n_id].get("name", "") == "URI"
        and (first_arg := get_n_arg(graph, n_id, 0))
        and (val := has_string_definition(graph, first_arg))
        and val.startswith("http://")  # noqa: COM812
    )

    create_http = bool(
        graph.nodes[n_id].get("expression", "") == "create"
        and (obj_id := graph.nodes[n_id].get("object_id"))
        and graph.nodes[obj_id].get("symbol", "") == "URI"
        and (first_arg := get_n_arg(graph, n_id, 0))
        and (val := has_string_definition(graph, first_arg))
        and val.startswith("http://")  # noqa: COM812
    )

    return plain_http or uri_http or create_http


def _is_rest_template_object(graph: Graph, n_id: NId) -> bool:
    return (
        graph.nodes[n_id].get("label_type") == "ObjectCreation"
        and graph.nodes[n_id].get("name", "") == "RestTemplate"
    )


def java_insecure_spring_http_request(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_INSECURE_SPRING_HTTP_REQUEST

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported_prefix(graph, ("org.springframework.web.client",)):
            return

        suspicious_methods = {
            "delete",
            "doExecute",
            "exchange",
            "getForEntity",
            "getForObject",
            "headForHeaders",
            "optionsForAllow",
            "patchForObject",
            "postForEntity",
            "postForLocation",
            "postForObject",
            "put",
        }
        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("expression", "") in suspicious_methods
                and (first_arg := get_n_arg(graph, n_id, 0))
                and is_node_definition_unsafe(graph, first_arg, _is_http_url)
                and (obj_id := graph.nodes[n_id].get("object_id"))
                and is_node_definition_unsafe(graph, obj_id, _is_rest_template_object)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
