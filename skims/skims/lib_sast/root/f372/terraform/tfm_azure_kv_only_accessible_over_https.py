from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _azure_kv_only_accessible_over_https(graph: Graph, nid: NId) -> NId | None:
    https_only = get_optional_attribute(graph, nid, "https_only")
    if not https_only:
        return nid
    if https_only[1].lower() == "false":
        return https_only[2]
    return None


def tfm_azure_kv_only_accessible_over_https(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AZURE_KV_ONLY_ACCESSIBLE_OVER_HTTPS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in {
                "azurerm_app_service",
                "azurerm_windows_web_app",
                "azurerm_linux_web_app",
                "azurerm_function_app",
            } and (report := _azure_kv_only_accessible_over_https(graph, nid)):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
