from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.json_utils import (
    get_key_value,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _insecure_http_server(graph: Graph, nid: NId, key: str) -> Iterator[NId]:
    required_flags = {" -S", " --tls", " --ssl"}
    if key == "scripts":
        for c_id in adj_ast(graph, nid):
            if (
                (value_id := graph.nodes[c_id].get("value_id"))
                and (value := graph.nodes[value_id].get("value"))
                and "http-server" in value
                and "-a localhost" not in value
                and not any(flag in value for flag in required_flags)
            ):
                yield c_id


def https_flag_missing(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    method = MethodsEnum.JSON_HTTPS_FLAG_MISSING

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            key, _ = get_key_value(graph, nid)
            value_id = graph.nodes[nid]["value_id"]
            yield from _insecure_http_server(graph, value_id, key)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
