from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_attribute,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _get_id_vulnerable_protocol(graph: Graph, nid: NId) -> Iterator[NId]:
    prop, _, props_id = get_attribute(graph, nid, "Properties")
    if prop:
        props_val_id = graph.nodes[props_id]["value_id"]
        listeners = get_attribute(graph, props_val_id, "Listeners")
        listeners_id = graph.nodes[listeners[2]]["value_id"]
        for listener in adj_ast(graph, listeners_id):
            _, protocol, protocol_id = get_attribute(graph, listener, "Protocol")
            if protocol.lower() == "http":
                yield protocol_id


def cfn_aws_elb_listener_on_http(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_AWS_ELB_LISTENER_ON_HTTP

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (resource := get_optional_attribute(graph, nid, "Type")) and resource[
                1
            ] == "AWS::ElasticLoadBalancing::LoadBalancer":
                yield from _get_id_vulnerable_protocol(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
