from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.f183.common import (
    debug_mode_enabled,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from model.core import (
    MethodExecutionResult,
)


def _variable_declaration_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    graph = args.graph
    if (
        (val_id := graph.nodes[args.n_id].get("value_id"))
        and (graph.nodes[val_id].get("label_type") == "MethodInvocation")
        and (exp_id := graph.nodes[val_id].get("expression_id"))
        and (graph.nodes[exp_id].get("symbol") == "express")
    ):
        args.triggers.add("express_instance")
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "variable_declaration": _variable_declaration_evaluator,
}


def js_express_debug_mode_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JS_EXPRESS_DEBUG_MODE_ENABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        yield from debug_mode_enabled(
            graph,
            method_supplies,
            method,
            METHOD_EVALUATORS,
        )

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
