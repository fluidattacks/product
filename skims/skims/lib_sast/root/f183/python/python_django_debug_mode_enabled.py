from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _is_debug_mode_on(graph: Graph, n_id: NId) -> bool:
    if (
        graph.nodes[n_id].get("variable") == "DEBUG"
        and (val_id := graph.nodes[n_id].get("value_id"))
        and graph.nodes[val_id]["label_type"] == "Literal"
        and graph.nodes[val_id].get("value") == "True"
    ):
        parent_id = pred_ast(graph, n_id)[0]
        return bool(graph.nodes[parent_id]["label_type"] == "File")
    return False


def python_django_debug_mode_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PYTHON_DJANGO_DEBUG_MODE_ENABLED

    def n_ids() -> Iterator[NId]:
        if not shard.path.endswith("settings.py"):
            return
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if _is_debug_mode_on(graph, n_id):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
