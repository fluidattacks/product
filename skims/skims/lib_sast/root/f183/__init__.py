from lib_sast.root.f183.java.java_webview_debug_mode_enabled import (
    java_webview_debug_mode_enabled as _java_webview_debug_mode_enabled,
)
from lib_sast.root.f183.javascript.js_debugger_enabled import (
    js_debugging_enabled as _js_debugging_enabled,
)
from lib_sast.root.f183.javascript.js_express_debug_mode_enabled import (
    js_express_debug_mode_enabled as _js_express_debug_mode_enabled,
)
from lib_sast.root.f183.python.python_django_debug_mode_enabled import (
    python_django_debug_mode_enabled as _python_django_debug_mode_enabled,
)
from lib_sast.root.f183.python.python_fastapi_starlette_debug_on import (
    python_fastapi_starlette_debug_on as _python_fastapi_starlette_debug_on,
)
from lib_sast.root.f183.python.python_flask_debug_mode_enabled import (
    python_flask_debug_mode_enabled as _python_flask_debug_mode_enabled,
)
from lib_sast.root.f183.typescript.ts_debugger_enabled import (
    ts_debugging_enabled as _ts_debugging_enabled,
)
from lib_sast.root.f183.typescript.ts_express_debug_mode_enabled import (
    ts_express_debug_mode_enabled as _ts_express_debug_mode_enabled,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def java_webview_debug_mode_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_webview_debug_mode_enabled(shard, method_supplies)


@SHIELD_BLOCKING
def js_debugging_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_debugging_enabled(shard, method_supplies)


@SHIELD_BLOCKING
def js_express_debug_mode_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_express_debug_mode_enabled(shard, method_supplies)


@SHIELD_BLOCKING
def python_django_debug_mode_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_django_debug_mode_enabled(shard, method_supplies)


@SHIELD_BLOCKING
def python_fastapi_starlette_debug_on(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_fastapi_starlette_debug_on(shard, method_supplies)


@SHIELD_BLOCKING
def python_flask_debug_mode_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_flask_debug_mode_enabled(shard, method_supplies)


@SHIELD_BLOCKING
def ts_debugging_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_debugging_enabled(shard, method_supplies)


@SHIELD_BLOCKING
def ts_express_debug_mode_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_express_debug_mode_enabled(shard, method_supplies)
