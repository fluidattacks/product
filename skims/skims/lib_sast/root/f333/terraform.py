from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _aux_ec2_associate_public_ip_address(graph: Graph, nid: NId) -> NId | None:
    ip_address = get_optional_attribute(graph, nid, "associate_public_ip_address")
    if ip_address and ip_address[1].lower() == "true":
        return ip_address[2]
    return None


def _ec2_associate_public_ip_address(graph: Graph, nid: NId) -> NId | None:
    obj_type = graph.nodes[nid].get("name")
    if obj_type and obj_type == "aws_instance":
        return _aux_ec2_associate_public_ip_address(graph, nid)
    expected_block = "network_interfaces"
    for c_id in adj_ast(graph, nid, name=expected_block):
        return _aux_ec2_associate_public_ip_address(graph, c_id)
    return None


def tfm_ec2_associate_public_ip_address(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_EC2_ASSOCIATE_PUBLIC_IP_ADDRESS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in {
                "aws_instance",
                "aws_launch_template",
            } and (report := _ec2_associate_public_ip_address(graph, nid)):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_ec2_has_not_an_iam_instance_profile(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_EC2_HAS_NOT_AN_IAM_INSTANCE_PROFILE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "aws_instance" and not get_optional_attribute(
                graph,
                nid,
                "iam_instance_profile",
            ):
                yield nid

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
