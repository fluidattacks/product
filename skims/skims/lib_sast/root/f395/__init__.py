from lib_sast.root.f395.c_sharp import (
    c_sharp_hardcoded_init_vector as _c_sharp_hardcoded_init_vector,
)
from lib_sast.root.f395.c_sharp import (
    c_sharp_insecure_cbc_iv as _c_sharp_insecure_cbc_iv,
)
from lib_sast.root.f395.java import (
    java_hardcoded_init_vector as _java_hardcoded_init_vector,
)
from lib_sast.root.f395.php import (
    php_hardcoded_init_vector as _php_hardcoded_init_vector,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_hardcoded_init_vector(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_hardcoded_init_vector(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_insecure_cbc_iv(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_insecure_cbc_iv(shard, method_supplies)


@SHIELD_BLOCKING
def java_hardcoded_init_vector(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_hardcoded_init_vector(shard, method_supplies)


@SHIELD_BLOCKING
def php_hardcoded_init_vector(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _php_hardcoded_init_vector(shard, method_supplies)
