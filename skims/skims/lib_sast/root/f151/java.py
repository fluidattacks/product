from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_parent_scope_n_id,
)
from lib_sast.root.utilities.java import (
    get_variable_name,
    is_any_library_imported_prefix,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils import (
    graph as g,
)
from model.core import (
    MethodExecutionResult,
)


def java_telnet_request(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_TELNET_REQUEST

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        nodes = graph.nodes

        if not is_any_library_imported_prefix(graph, ("org.apache.commons.net.telnet",)):
            return None

        for n_id in method_supplies.selected_nodes:
            if (
                nodes[n_id].get("name", "") == "TelnetClient"
                and (var_name := get_variable_name(graph, n_id))
                and (p_n_id := get_parent_scope_n_id(graph, n_id))
            ):
                yield from [
                    n_id
                    for n_id in g.adj_ast(
                        graph,
                        p_n_id,
                        -1,
                        label_type="MethodInvocation",
                        expression="connect",
                    )
                    if (obj_n_id := nodes[n_id].get("object_id"))
                    and nodes[obj_n_id].get("symbol") == var_name
                ]

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
