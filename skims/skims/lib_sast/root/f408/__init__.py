from lib_sast.root.f408.cloudformation import (
    cfn_api_gateway_access_logging_disabled as _cfn_gateway_logging_disabled,
)
from lib_sast.root.f408.terraform import (
    tfm_api_gateway_access_logging_disabled as _tfm_gateway_logging_disabled,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_api_gateway_access_logging_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_gateway_logging_disabled(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_api_gateway_access_logging_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_gateway_logging_disabled(shard, method_supplies)
