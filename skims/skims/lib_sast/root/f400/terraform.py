from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_argument,
    get_argument_iterator,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _redshift_has_audit_logging_disabled(graph: Graph, nid: NId) -> NId | None:
    if not (logging := get_argument(graph, nid, "logging")):
        return nid
    enable = get_optional_attribute(graph, logging, "enable")
    if enable and enable[1].lower() != "true":
        return enable[2]
    return None


def _redshift_is_user_activity_logging_disabled(graph: Graph, nid: NId) -> NId | None:
    exist = False
    for parameter in get_argument_iterator(graph, nid, "parameter"):
        p_name = get_optional_attribute(graph, parameter, "name")
        if p_name and p_name[1] == "enable_user_activity_logging":
            exist = True
            p_value = get_optional_attribute(graph, parameter, "value")
            if p_value and p_value[1] == "false":
                return p_name[2]
    if not exist:
        return nid
    return None


def _trails_not_multiregion(graph: Graph, nid: NId) -> NId | None:
    reg_trail = get_optional_attribute(graph, nid, "is_multi_region_trail")
    if not reg_trail:
        return nid
    if reg_trail[1].lower() in {"false", "0"}:
        return reg_trail[2]
    return None


def _lb_logging_disabled(graph: Graph, nid: NId) -> NId | None:
    access = get_argument(graph, nid, "access_logs")
    if not access:
        return nid
    enabled = get_optional_attribute(graph, access, "enabled")
    if enabled and enabled[1].lower() == "false":
        return enabled[2]
    return None


def tfm_load_balancers_logging_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_ELB_LOGGING_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in {"aws_elb", "aws_lb"} and (
                report := _lb_logging_disabled(graph, nid)
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_trails_not_multiregion(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_TRAILS_NOT_MULTIREGION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "aws_cloudtrail" and (
                report := _trails_not_multiregion(graph, nid)
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_distribution_has_logging_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_DISTRIBUTION_HAS_LOGGING_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "aws_cloudfront_distribution" and not get_argument(
                graph,
                nid,
                "logging_config",
            ):
                yield nid

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_redshift_user_act_logging_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_REDSHIFT_IS_USER_ACTIVITY_LOGGING_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "aws_redshift_parameter_group" and (
                vuln_id := _redshift_is_user_activity_logging_disabled(graph, nid)
            ):
                yield vuln_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_redshift_has_audit_logging_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_REDSHIFT_HAS_AUDIT_LOGGING_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "aws_redshift_cluster" and (
                report := _redshift_has_audit_logging_disabled(graph, nid)
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
