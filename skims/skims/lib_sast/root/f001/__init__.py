from lib_sast.root.f001.c_sharp.c_sharp_sql_injection import (
    c_sharp_sql_injection_execution as _c_sharp_sql_injection_execution,
)
from lib_sast.root.f001.c_sharp.c_sharp_sql_injection import (
    c_sharp_sql_injection_object as _c_sharp_sql_injection_object,
)
from lib_sast.root.f001.c_sharp.c_sharp_sql_injection_request import (
    c_sharp_sql_injection_request as _c_sharp_sql_injection_request,
)
from lib_sast.root.f001.c_sharp.c_sharp_sql_user_params import (
    c_sharp_sql_user_params as _c_sharp_sql_user_params,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_sql_injection_execution(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_sql_injection_execution(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_sql_injection_object(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_sql_injection_object(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_sql_user_params(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_sql_user_params(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_sql_injection_request(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_sql_injection_request(shard, method_supplies)
