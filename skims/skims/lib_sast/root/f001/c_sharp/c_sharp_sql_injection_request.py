from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.common import (
    NET_SYSTEM_HTTP_METHODS,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _member_access_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    ma_attr = args.graph.nodes[args.n_id]

    if ma_attr["member"] in NET_SYSTEM_HTTP_METHODS and ma_attr["expression"] == "Request":
        args.evaluation[args.n_id] = True
        args.triggers.add("user_connection")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "member_access": _member_access_evaluator,
}


def _is_sql_injection(
    graph: Graph,
    n_id: str,
    method: MethodsEnum,
    triggers_goal: set[str],
) -> bool:
    return bool(
        (al_id := graph.nodes[n_id].get("arguments_id"))
        and (args_ids := adj_ast(graph, al_id))
        and len(args_ids) > 0
        and get_node_evaluation_results(
            method,
            graph,
            args_ids[0],
            triggers_goal,
            method_evaluators=METHOD_EVALUATORS,
        ),
    )


def c_sharp_sql_injection_request(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_SQL_INJECTION_REQUEST

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if (
                (name := graph.nodes[node].get("name"))
                and name == "SqlCommand"
                and _is_sql_injection(graph, node, method, {"user_connection"})
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
