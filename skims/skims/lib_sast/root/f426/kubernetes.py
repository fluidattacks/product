import re
from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.kubernetes import (
    Natural,
    get_container_spec,
    get_key_value,
    get_nodes_with_key,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _image_has_digest(value: str) -> bool:
    env_var_re: re.Pattern = re.compile(r"\{.+\}")
    digest_re: re.Pattern = re.compile(r".*@sha256:[a-fA-F0-9]{64}")
    return not (env_var_re.search(value) or digest_re.search(value))


def k8s_image_has_digest(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.K8S_IMAGE_HAS_DIGEST

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        spec_ids = get_container_spec(graph, method_supplies.selected_nodes)
        containers_ids = get_nodes_with_key(
            graph,
            spec_ids,
            "containers",
            Natural.assert_natural(2),
        )
        images = get_nodes_with_key(
            graph,
            containers_ids,
            "image",
            Natural.assert_natural(4),
        )
        vuln_images = list(
            filter(
                lambda nid: _image_has_digest(get_key_value(graph, nid)[1]),
                images,
            ),
        )
        yield from vuln_images

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
