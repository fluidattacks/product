import re
from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    K8S_CONTAINER_RESOURCE_DEPTHS,
    get_argument_iterator_d,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _image_has_digest(value: str) -> bool:
    env_var_re: re.Pattern = re.compile(r"\${.+\}")
    digest_re: re.Pattern = re.compile(".*@sha256:[a-fA-F0-9]{64}")
    return not (env_var_re.search(value) or digest_re.search(value))


def _check_digest(graph: Graph, nid: NId) -> Iterator[NId]:
    cont_childs_nids = adj_ast(graph, nid, label_type="Object")
    images_attr_id: list[tuple[str, str, NId] | None] = [
        get_optional_attribute(graph, spec_nid, "image") for spec_nid in cont_childs_nids
    ]
    valid_images = [
        attr for attr in images_attr_id if attr is not None and _image_has_digest(attr[1])
    ]
    yield from (item[2] for item in valid_images)


def tfm_k8s_image_has_digest(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_K8S_IMAGE_HAS_DIGEST

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in K8S_CONTAINER_RESOURCE_DEPTHS:
                resource_name = graph.nodes[nid].get("name")
                for spec_nid in get_argument_iterator_d(
                    graph,
                    nid,
                    "spec",
                    depth=K8S_CONTAINER_RESOURCE_DEPTHS[resource_name],
                ):
                    yield from _check_digest(graph, spec_nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
