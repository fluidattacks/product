from lib_sast.root.f426.kubernetes import (
    k8s_image_has_digest as _k8s_image_has_digest,
)
from lib_sast.root.f426.terraform import (
    tfm_k8s_image_has_digest as _tfm_k8s_image_has_digest,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def k8s_image_has_digest(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _k8s_image_has_digest(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_k8s_image_has_digest(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_k8s_image_has_digest(shard, method_supplies)
