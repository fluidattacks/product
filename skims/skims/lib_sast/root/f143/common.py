from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.utilities.javascript import (
    file_imports_module,
)
from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils import (
    graph as g,
)


def only_one_argument(graph: Graph, n_id: NId) -> bool:
    if (args := g.match_ast(graph, n_id)) and (len(args) == 1):  # noqa: SIM103
        return True
    return False


def has_eval(method: MethodsEnum, graph: Graph, n_id: NId) -> bool:
    sensitive_methods = {"eval", "Function"}
    if file_imports_module(graph, "notevil"):
        sensitive_methods.add("safeEval")
    if (  # noqa: SIM103
        (
            graph.nodes[n_id].get("expression") in sensitive_methods
            or graph.nodes[n_id].get("name") in sensitive_methods
        )
        and (args_id := graph.nodes[n_id].get("arguments_id"))
        and only_one_argument(graph, args_id)
        and get_node_evaluation_results(method, graph, n_id, set())
    ):
        return True
    return False
