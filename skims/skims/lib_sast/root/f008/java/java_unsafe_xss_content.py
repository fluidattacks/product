from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    get_parent_scope_n_id,
    has_string_definition,
    is_node_definition_unsafe,
)
from lib_sast.root.utilities.java import (
    is_any_library_imported_prefix,
)
from lib_sast.sast_model import (
    Graph,
    GraphDB,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.common import (
    owasp_user_connection,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _parameter_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id].get("variable_type") == "HttpServletRequest":
        args.triggers.add("userconnection")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def _method_invocation_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    graph = args.graph
    n_id = args.n_id

    if owasp_user_connection(args):
        args.triggers.discard("userconnection")
        args.evaluation[n_id] = True
        args.triggers.add("userparameters")

    ma_attr = graph.nodes[n_id]

    expression = ma_attr["expression"]
    if expression in {"encodeForHTML", "htmlEscape", "escapeHtml"}:
        args.triggers.add("sanitized_parameters")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "method_invocation": _method_invocation_evaluator,
    "parameter": _parameter_evaluator,
}


def _has_text_plain_content(graph: Graph, obj_n_id: NId) -> bool:
    danger_obj_name = graph.nodes[obj_n_id].get("symbol")
    return bool(
        (p_n_id := get_parent_scope_n_id(graph, obj_n_id))
        and any(
            (obj_id := graph.nodes[n_id].get("object_id"))
            and (graph.nodes[obj_id].get("symbol") == danger_obj_name)
            and (f_arg := get_n_arg(graph, n_id, 0))
            and (has_string_definition(graph, f_arg) == "text/plain")
            for n_id in adj_ast(
                graph,
                p_n_id,
                -1,
                label_type="MethodInvocation",
                expression="setContentType",
            )
        )  # noqa:  COM812
    )


def _is_writing_to_response(graph: Graph, n_id: NId) -> bool:
    return bool(
        (graph.nodes[n_id]["label_type"] == "MethodInvocation")
        and graph.nodes[n_id].get("expression") == "getWriter"
        and not _has_text_plain_content(
            graph,
            graph.nodes[n_id].get("object_id"),
        ),
    )


def _is_xss_content_creation(
    method: MethodsEnum,
    graph: Graph,
    al_id: NId,
    obj_id: NId,
    graph_db: GraphDB | None,
) -> bool:
    return bool(
        is_node_definition_unsafe(
            graph,
            obj_id,
            _is_writing_to_response,
        )
        and get_node_evaluation_results(
            method,
            graph,
            al_id,
            {"userparameters"},
            danger_goal=True,
            graph_db=graph_db,
            method_evaluators=METHOD_EVALUATORS,
        ),
    )


def _is_danger_method(
    n_id: NId,
    graph: Graph,
) -> bool:
    n_attrs = graph.nodes[n_id]
    expression = n_attrs["expression"]

    return bool(
        (obj_id := n_attrs.get("object_id"))
        and (
            (
                expression in {"write", "println", "printf", "print"}
                and graph.nodes[obj_id].get("symbol") not in {"String", "System.out", "out"}
            )
            or (expression == "format" and graph.nodes[obj_id].get("symbol") != "String")
        ),
    )


def java_unsafe_xss_content(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_UNSAFE_XSS_CONTENT

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported_prefix(
            graph,
            ("java.io.PrintWriter", "javax.servlet.http.HttpServletResponse"),
        ):
            return
        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            if (
                _is_danger_method(n_id, graph)
                and (al_id := graph.nodes[n_id].get("arguments_id"))
                and _is_xss_content_creation(
                    method,
                    graph,
                    al_id,
                    n_attrs["object_id"],
                    method_supplies.graph_db,
                )
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
