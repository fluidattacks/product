from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.common import (
    PHP_INPUTS,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    adj_ast,
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _is_user_input(graph: Graph, n_id: NId) -> bool:
    if graph.nodes[n_id]["label_type"] == "ElementAccess":
        exp_n_id = graph.nodes[n_id].get("expression_id")
        return bool(exp_n_id) and graph.nodes[exp_n_id].get("symbol") in PHP_INPUTS
    return False


def _is_variable_dangerous(graph: Graph, n_id: NId) -> bool:
    if _is_user_input(graph, n_id):
        return True

    if graph.nodes[n_id]["label_type"] == "SymbolLookup":
        symbol = graph.nodes[n_id]["symbol"]
        for path in get_backward_paths(graph, n_id):
            if (
                (def_id := definition_search(graph, path, symbol))
                and (val_id := graph.nodes[def_id].get("value_id"))
                and _is_user_input(graph, val_id)
            ):
                return True
    return False


def php_unsafe_xss_content(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PHP_UNSAFE_XSS_CONTENT

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("expression") == "echo"
                and (al_id := graph.nodes[n_id].get("arguments_id"))
                and any(_is_variable_dangerous(graph, arg_id) for arg_id in adj_ast(graph, al_id))
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def _is_directly_called(graph: Graph, n_id: NId) -> bool:
    parents_id = pred_ast(graph, n_id)
    return len(parents_id) == 1 and graph.nodes[parents_id[0]]["label_type"] == "File"


def php_unsafe_xss_content_direct(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PHP_UNSAFE_XSS_CONTENT

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if _is_directly_called(graph, n_id) and _is_variable_dangerous(graph, n_id):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
