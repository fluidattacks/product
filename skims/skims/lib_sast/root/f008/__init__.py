from lib_sast.root.f008.c_sharp.c_sharp_insec_addheader_write import (
    c_sharp_unsafe_addheader_write as _c_sharp_unsafe_addheader_write,
)
from lib_sast.root.f008.c_sharp.c_sharp_insec_addheader_write import (
    c_sharp_unsafe_status_write as _c_sharp_unsafe_status_write,
)
from lib_sast.root.f008.c_sharp.c_sharp_insec_direct_write import (
    c_sharp_unsafe_direct_write as _c_sharp_unsafe_direct_write,
)
from lib_sast.root.f008.java.java_unsafe_xss_content import (
    java_unsafe_xss_content as _java_unsafe_xss_content,
)
from lib_sast.root.f008.javascript.javascript_unsafe_xss_content import (
    js_unsafe_xss_content as _js_unsafe_xss_content,
)
from lib_sast.root.f008.php.php_unsafe_xss_content import (
    php_unsafe_xss_content as _php_unsafe_xss_content,
)
from lib_sast.root.f008.php.php_unsafe_xss_content import (
    php_unsafe_xss_content_direct as _php_unsafe_xss_content_direct,
)
from lib_sast.root.f008.typescript.typescript_unsafe_xss_content import (
    ts_unsafe_xss_content as _ts_unsafe_xss_content,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_unsafe_addheader_write(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_unsafe_addheader_write(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_unsafe_direct_write(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_unsafe_direct_write(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_unsafe_status_write(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_unsafe_status_write(shard, method_supplies)


@SHIELD_BLOCKING
def java_unsafe_xss_content(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_unsafe_xss_content(shard, method_supplies)


@SHIELD_BLOCKING
def js_unsafe_xss_content(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_unsafe_xss_content(shard, method_supplies)


@SHIELD_BLOCKING
def php_unsafe_xss_content(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _php_unsafe_xss_content(shard, method_supplies)


@SHIELD_BLOCKING
def php_unsafe_xss_content_direct(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _php_unsafe_xss_content_direct(shard, method_supplies)


@SHIELD_BLOCKING
def ts_unsafe_xss_content(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_unsafe_xss_content(shard, method_supplies)
