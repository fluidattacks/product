from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.utilities.javascript import (
    get_default_alias,
)
from lib_sast.sast_model import (
    Graph,
    MethodSupplies,
    NId,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils.graph import (
    adj_ast,
    filter_nodes,
    match_ast_group,
    pred_ast,
)


def get_dang_instances_names_method(graph: Graph, dang_classes: set[str]) -> set[str]:
    def predicate_matcher(node: dict[str, str]) -> bool:
        nodes = graph.nodes
        return bool(
            node.get("label_type") == "VariableDeclaration"
            and (val_n_id := node.get("value_id"))
            and (
                (
                    (nodes[val_n_id].get("label_type") == "ObjectCreation")
                    and (nodes[val_n_id].get("name") in dang_classes)
                )
                or (
                    (nodes[val_n_id].get("label_type") == "MethodInvocation")
                    and (expr := nodes[val_n_id].get("expression"))
                    and (expr.split(".")[0] in dang_classes)
                )
            ),
        )

    dang_names: set[str] = set()
    for n_id in filter_nodes(graph, graph.nodes, predicate_matcher):
        dang_names.add(graph.nodes[n_id].get("variable"))
    return dang_names


def get_danger_invocations_method(graph: Graph) -> set[str]:
    dang_methods: set[str] = {"setRequestHeader", "append", "set"}
    dang_objects = get_dang_instances_names_method(graph, {"XMLHttpRequest", "Headers"})

    if imported_alias := get_default_alias(graph, "superagent"):
        dang_objects.add(imported_alias)

    return {
        f"{dang_name}.{dang_method}" for dang_name in dang_objects for dang_method in dang_methods
    }


def get_dang_instances_names_object(graph: Graph) -> set[str]:
    dang_libraries: set[str] = {"axios", "ky", "jquery"}
    dang_aliases: set[str] = {"fetch"}

    for lib in dang_libraries:
        if dang_alias := get_default_alias(graph, lib):
            dang_aliases.add(dang_alias)
    return dang_aliases


def get_accept_header_vulns_method_1(
    graph: Graph,
    n_ids: list[NId],
    method: MethodsEnum,
) -> list[NId]:
    danger_invocations = get_danger_invocations_method(graph)
    if not danger_invocations:
        return []
    return [
        n_id
        for n_id in n_ids
        if (
            graph.nodes[n_id].get("expression") in danger_invocations
            and (args_n_id := graph.nodes[n_id].get("arguments_id"))
            and get_node_evaluation_results(
                method,
                graph,
                args_n_id,
                {"accept", "*/*"},
                danger_goal=False,
            )
        )
    ]


def get_suspect_nodes(graph: Graph, n_id: NId, danger_instances: set[str]) -> list[NId] | None:
    expr = graph.nodes[n_id].get("expression")

    if not expr or str(expr).split(".", maxsplit=1)[0] not in danger_instances:
        return None

    if str(expr).startswith("$") and expr != "$.ajax":
        return None

    if (args_n_id := graph.nodes[n_id].get("arguments_id")) and (
        childs := match_ast_group(graph, args_n_id, "SymbolLookup", "Object")
    ):
        return childs["SymbolLookup"] + childs["Object"]

    return None


def get_accept_header_vulns_method_2(
    graph: Graph,
    n_ids: list[NId],
    method: MethodsEnum,
) -> list[NId]:
    danger_instances = get_dang_instances_names_object(graph)
    if not danger_instances:
        return []

    vuln_nodes = []
    for n_id in n_ids:
        susp_nodes = get_suspect_nodes(graph, n_id, danger_instances)
        if not susp_nodes:
            continue
        if any(
            get_node_evaluation_results(
                method,
                graph,
                susp_id,
                {"accept", "*/*"},
                danger_goal=False,
            )
            for susp_id in susp_nodes
        ):
            vuln_nodes.append(n_id)
    return vuln_nodes


def get_accept_header_vulns_default(
    graph: Graph,
    n_ids: list[NId],
    method: MethodsEnum,
) -> list[NId]:
    if not (library_alias := get_default_alias(graph, "axios")):
        return []

    dang_members = {
        "defaults",
        "headers",
        "common",
        "get",
        "post",
        "put",
        "patch",
        "delete",
    }
    dang_members.add(library_alias)

    for dang_instance in get_dang_instances_names_method(graph, {library_alias}):
        dang_members.add(dang_instance)

    vuln_nodes: list[NId] = []
    for n_id in n_ids:
        expr_id = graph.nodes[n_id].get("expression_id")
        if not expr_id or graph.nodes[expr_id]["label_type"] != "MemberAccess":
            continue

        if (
            (members := set(graph.nodes[expr_id].get("expression").split(".")))
            and members.issubset(dang_members)
            and (arg_n_id := graph.nodes[n_id].get("arguments_id"))
            and (graph.nodes[arg_n_id].get("value") == "Accept")
        ):
            p_id = pred_ast(graph, n_id)[0]
            if graph.nodes[p_id].get("label_type") == "Assignment" and get_node_evaluation_results(
                method,
                graph,
                p_id,
                {"*/*"},
                danger_goal=False,
            ):
                vuln_nodes.append(n_id)
    return vuln_nodes


def aux_express_accepts_any_mime(
    graph: Graph,
    n_id: NId,
    var_name: str,
    method: MethodsEnum,
) -> bool:
    n_attrs = graph.nodes[n_id]
    if n_attrs["expression"] == "app.use":
        al_id = graph.nodes[n_id].get("arguments_id")
        if (
            al_id
            and (args_ids := adj_ast(graph, al_id))
            and len(args_ids) == 1
            and graph.nodes[args_ids[0]].get("expression") == var_name + ".text"
        ):
            options_id = graph.nodes[args_ids[0]].get("arguments_id")
            if options_id and (args_list := adj_ast(graph, al_id)) and len(args_list) == 1:
                return get_node_evaluation_results(method, graph, args_list[0], set())
    return False


def express_accepts_any_mime(
    graph: Graph,
    method_supplies: MethodSupplies,
    method: MethodsEnum,
) -> list[NId]:
    vuln_nodes: list[NId] = []
    var_name: str = ""
    for n_id in method_supplies.selected_nodes:
        if var_name != "":
            if aux_express_accepts_any_mime(graph, n_id, var_name, method):
                vuln_nodes.append(n_id)
        else:
            n_attrs = graph.nodes[n_id]
            if n_attrs["expression"] == "require":
                al_id = graph.nodes[n_id].get("arguments_id")
                if (
                    al_id
                    and (args_ids := adj_ast(graph, al_id))
                    and len(args_ids) == 1
                    and graph.nodes[args_ids[0]].get("value") == "body-parser"
                ):
                    var_name = graph.nodes[pred_ast(graph, n_id)[0]].get("variable", "")

    return vuln_nodes
