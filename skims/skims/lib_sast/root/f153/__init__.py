from lib_sast.root.f153.c_sharp import (
    c_sharp_accepts_any_mime_type as _c_sharp_accepts_any_mime_type,
)
from lib_sast.root.f153.go import (
    go_accepts_any_mime_type as _go_accepts_any_mime_type,
)
from lib_sast.root.f153.java import (
    java_accepts_any_mime_type_chain as _java_accepts_any_mime_type_chain,
)
from lib_sast.root.f153.java import (
    java_http_accepts_any_mime_type as _java_http_accepts_any_mime_type,
)
from lib_sast.root.f153.java import (
    java_http_accepts_any_mime_type_obj as _java_http_accepts_mime_type_obj,
)
from lib_sast.root.f153.javascript import (
    javascript_express_accepts_any_mime as _js_express_accepts_any_mime,
)
from lib_sast.root.f153.javascript import (
    js_accepts_any_mime_default as _js_accepts_any_mime_default,
)
from lib_sast.root.f153.javascript import (
    js_accepts_any_mime_method as _js_accepts_any_mime_method,
)
from lib_sast.root.f153.kotlin import (
    kt_accepts_any_mime_type as _kt_accepts_any_mime_type,
)
from lib_sast.root.f153.python import (
    python_danger_accept_header as _python_danger_accept_header,
)
from lib_sast.root.f153.typescript import (
    ts_accepts_any_mime_default as _ts_accepts_any_mime_default,
)
from lib_sast.root.f153.typescript import (
    ts_accepts_any_mime_method as _ts_accepts_any_mime_method,
)
from lib_sast.root.f153.typescript import (
    typescript_express_accepts_any_mime as _ts_express_accepts_any_mime,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_accepts_any_mime_type(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_accepts_any_mime_type(shard, method_supplies)


@SHIELD_BLOCKING
def go_accepts_any_mime_type(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _go_accepts_any_mime_type(shard, method_supplies)


@SHIELD_BLOCKING
def java_accepts_any_mime_type_chain(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_accepts_any_mime_type_chain(shard, method_supplies)


@SHIELD_BLOCKING
def java_http_accepts_any_mime_type(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_http_accepts_any_mime_type(shard, method_supplies)


@SHIELD_BLOCKING
def java_http_accepts_any_mime_type_obj(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_http_accepts_mime_type_obj(shard, method_supplies)


@SHIELD_BLOCKING
def js_accepts_any_mime_default(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_accepts_any_mime_default(shard, method_supplies)


@SHIELD_BLOCKING
def js_accepts_any_mime_method(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_accepts_any_mime_method(shard, method_supplies)


@SHIELD_BLOCKING
def js_express_accepts_any_mime(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_express_accepts_any_mime(shard, method_supplies)


@SHIELD_BLOCKING
def kt_accepts_any_mime_type(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _kt_accepts_any_mime_type(shard, method_supplies)


@SHIELD_BLOCKING
def python_danger_accept_header(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_danger_accept_header(shard, method_supplies)


@SHIELD_BLOCKING
def ts_accepts_any_mime_default(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_accepts_any_mime_default(shard, method_supplies)


@SHIELD_BLOCKING
def ts_accepts_any_mime_method(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_accepts_any_mime_method(shard, method_supplies)


@SHIELD_BLOCKING
def ts_express_accepts_any_mime(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_express_accepts_any_mime(shard, method_supplies)
