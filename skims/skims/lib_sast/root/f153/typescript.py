from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.f153.common import (
    express_accepts_any_mime,
    get_accept_header_vulns_default,
    get_accept_header_vulns_method_1,
    get_accept_header_vulns_method_2,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def ts_accepts_any_mime_method(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TYPESCRIPT_ACCEPTS_ANY_MIME_METHOD

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        n_ids = method_supplies.selected_nodes
        yield from get_accept_header_vulns_method_1(graph, n_ids, method)
        yield from get_accept_header_vulns_method_2(graph, n_ids, method)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def ts_accepts_any_mime_default(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TYPESCRIPT_ACCEPTS_ANY_MIME_DEFAULT

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        yield from get_accept_header_vulns_default(graph, method_supplies.selected_nodes, method)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def typescript_express_accepts_any_mime(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TYPESCRIPT_EXPRESS_ACCEPTS_ANY_MIME

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        yield from express_accepts_any_mime(graph, method_supplies, method)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
