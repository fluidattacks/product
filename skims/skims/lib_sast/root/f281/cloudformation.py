from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    FALSE_OPTIONS,
    TRUE_OPTIONS,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_optional_attribute,
    get_optional_attribute_inside_path,
    yield_statements_from_policy,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _bucket_policy_has_secure_transport(graph: Graph, nid: NId) -> Iterator[NId]:
    if (
        (props := get_optional_attribute(graph, nid, "Properties"))
        and (val_id := graph.nodes[props[2]]["value_id"])
        and (pol_doc := get_optional_attribute(graph, val_id, "PolicyDocument"))
    ):
        for stmt in yield_statements_from_policy(graph, graph.nodes[pol_doc[2]]["value_id"]):
            if not (effect := get_optional_attribute(graph, stmt, "Effect")):
                continue
            sec_trans = get_optional_attribute_inside_path(
                graph,
                stmt,
                ["Condition", "Bool", "aws:SecureTransport"],
            )
            if sec_trans and (
                (effect[1] == "Deny" and sec_trans[1] in TRUE_OPTIONS)
                or (effect[1] == "Allow" and sec_trans[1] in FALSE_OPTIONS)
            ):
                yield sec_trans[2]


def cfn_bucket_policy_has_secure_transport(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_BUCKET_POLICY_HAS_SECURE_TRANSPORT

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (resource := get_optional_attribute(graph, nid, "Type")) and resource[
                1
            ] == "AWS::S3::BucketPolicy":
                yield from _bucket_policy_has_secure_transport(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
