from lib_sast.root.f281.cloudformation import (
    cfn_bucket_policy_has_secure_transport as _cfn_bucket_secure_transport,
)
from lib_sast.root.f281.terraform import (
    tfm_bucket_policy_has_secure_transport as _tfm_bucket_secure_transport,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_bucket_policy_has_secure_transport(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_bucket_secure_transport(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_bucket_policy_has_secure_transport(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_bucket_secure_transport(shard, method_supplies)
