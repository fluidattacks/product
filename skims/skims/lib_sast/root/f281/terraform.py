from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    FALSE_OPTIONS,
    TRUE_OPTIONS,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_attr_inside_attrs,
    get_dict_from_attr,
    get_dict_values,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _bucket_policy_has_secure_transport_in_jsonencode(graph: Graph, nid: NId) -> Iterator[NId]:
    child_id = adj_ast(graph, graph.nodes[nid]["arguments_id"])[0]
    statements = get_optional_attribute(graph, child_id, "Statement")
    if not statements:
        return
    value_id = graph.nodes[statements[2]]["value_id"]
    for c_id in adj_ast(graph, value_id, label_type="Object"):
        effect = get_optional_attribute(graph, c_id, "Effect")
        if not effect:
            continue

        sec_trans = get_attr_inside_attrs(
            graph,
            c_id,
            ["Condition", "Bool", "aws:SecureTransport"],
        )
        if sec_trans and (
            (effect[1] == "Deny" and sec_trans[1] in TRUE_OPTIONS)
            or (effect[1] == "Allow" and sec_trans[1] in FALSE_OPTIONS)
        ):
            yield sec_trans[2]


def _aux_bucket_policy_sec_trans(attr_val: str, attr_id: NId) -> Iterator[NId]:
    dict_value = get_dict_from_attr(attr_val)
    statements = get_dict_values(dict_value, "Statement")
    for stmt in statements if isinstance(statements, list) else []:
        effect = stmt.get("Effect")
        secure_transport = get_dict_values(stmt, "Condition", "Bool", "aws:SecureTransport")
        if secure_transport and (
            (effect == "Deny" and secure_transport in TRUE_OPTIONS)
            or (effect == "Allow" and secure_transport in FALSE_OPTIONS)
        ):
            yield attr_id


def _bucket_policy_has_secure_transport(graph: Graph, nid: NId) -> Iterator[NId]:
    pol_attr = get_optional_attribute(graph, nid, "policy")
    if not pol_attr:
        return
    value_id = graph.nodes[pol_attr[2]]["value_id"]

    if graph.nodes[value_id]["label_type"] == "Literal":
        yield from _aux_bucket_policy_sec_trans(pol_attr[1], pol_attr[2])
    elif (
        graph.nodes[value_id]["label_type"] == "MethodInvocation"
        and graph.nodes[value_id]["expression"] == "jsonencode"
    ):
        yield from _bucket_policy_has_secure_transport_in_jsonencode(graph, value_id)


def tfm_bucket_policy_has_secure_transport(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_BUCKET_POLICY_HAS_SECURE_TRANSPORT

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "aws_s3_bucket_policy":
                yield from _bucket_policy_has_secure_transport(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
