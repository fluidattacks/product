from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    has_string_definition,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    matching_nodes,
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _is_secure_protocol(
    graph: Graph,
    n_id: NId,
) -> bool:
    return any(
        (
            graph.nodes[p_id].get("label_type") == "MethodInvocation"
            and graph.nodes[p_id].get("expression") == "to"
            and (f_arg := get_n_arg(graph, p_id, 0))
            and (val := has_string_definition(graph, f_arg))
            and val.startswith("https")
        )
        for p_id in pred_ast(graph, n_id, -1)
    )


def _is_basic_auth(
    graph: Graph,
    n_id: NId,
) -> bool:
    nodes = graph.nodes
    return bool(
        (f_arg := get_n_arg(graph, n_id, 0))
        and (has_string_definition(graph, f_arg) == "Authorization")
        and (s_arg := get_n_arg(graph, n_id, 1))
        and (
            ((auth_val := has_string_definition(graph, s_arg)) and auth_val.startswith("Basic"))
            or (
                nodes[s_arg].get("label_type") == "MethodInvocation"
                and (fm_arg := get_n_arg(graph, s_arg, 0))
                and (str_val := has_string_definition(graph, fm_arg))
                and str_val.startswith("Basic")
            )
        ),
    )


def _get_invocation_nodes(
    graph: Graph,
    selected_nodes: list[NId],
) -> tuple[NId, ...]:
    uses_org_apache = False

    for n_id in selected_nodes:
        library = graph.nodes[n_id].get("expression", "")
        if library.startswith(("org.apache.camel", "org.apache.http")):
            uses_org_apache = True
        elif library.startswith("org.junit"):
            return ()

    if uses_org_apache:
        return matching_nodes(graph, label_type="MethodInvocation", expression="setHeader")
    return ()


def java_basic_authentication(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_BASIC_AUTHENTICATION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        set_header_nodes = _get_invocation_nodes(graph, method_supplies.selected_nodes)

        for n_id in set_header_nodes:
            if _is_basic_auth(graph, n_id) and not _is_secure_protocol(graph, n_id):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
