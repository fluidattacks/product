from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils import (
    graph as g,
)
from model.core import (
    MethodExecutionResult,
)


def _member_access_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    n_attrs = args.graph.nodes[args.n_id]
    if f"{n_attrs['expression']}.{n_attrs['member']}" == "WebRequest.Create":
        args.evaluation[args.n_id] = True
        args.triggers.add("webreq")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def _literal_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    if args.graph.nodes[args.n_id]["value"] == "Basic ":
        args.evaluation[args.n_id] = True
        args.triggers.add("basicauth")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "member_access": _member_access_evaluator,
    "literal": _literal_evaluator,
}


def c_sharp_insecure_authentication(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_INSECURE_AUTHENTICATION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            n_attrs = graph.nodes[node]
            if (
                n_attrs["expression"].endswith("Headers.Add")
                and (al_id := n_attrs.get("arguments_id"))
                and get_node_evaluation_results(
                    method,
                    graph,
                    n_attrs["expression_id"],
                    {"webreq"},
                    graph_db=method_supplies.graph_db,
                    method_evaluators=METHOD_EVALUATORS,
                )
                and (arg_id := g.match_ast(graph, al_id).get("__1__"))
                and get_node_evaluation_results(
                    method,
                    graph,
                    arg_id,
                    {"basicauth"},
                    graph_db=method_supplies.graph_db,
                    method_evaluators=METHOD_EVALUATORS,
                )
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
