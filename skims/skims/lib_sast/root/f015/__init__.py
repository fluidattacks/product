from lib_sast.root.f015.c_sharp.c_sharp_insecure_authentication import (
    c_sharp_insecure_authentication as _c_sharp_insecure_authentication,
)
from lib_sast.root.f015.java.java_basic_authentication import (
    java_basic_authentication as _java_basic_authentication,
)
from lib_sast.root.f015.java.java_insecure_authentication import (
    java_insecure_authentication as _java_insecure_authentication,
)
from lib_sast.root.f015.php.php_basic_authentication import (
    php_basic_authentication as _php_basic_authentication,
)
from lib_sast.root.f015.python.python_insecure_authentication import (
    python_danger_basic_auth as _python_danger_basic_auth,
)
from lib_sast.root.f015.terraform.tfm_azure_linux_vm_insecure_authentication import (
    tfm_azure_linux_vm_insecure_authentication as lnx_vm_insecure_auth,
)
from lib_sast.root.f015.terraform.tfm_azure_virtual_machine_insecure_authentication import (
    tfm_azure_virtual_machine_insecure_authentication as vm_insecure_auth,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_insecure_authentication(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_insecure_authentication(shard, method_supplies)


@SHIELD_BLOCKING
def java_basic_authentication(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_basic_authentication(shard, method_supplies)


@SHIELD_BLOCKING
def java_insecure_authentication(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_insecure_authentication(shard, method_supplies)


@SHIELD_BLOCKING
def php_basic_authentication(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _php_basic_authentication(shard, method_supplies)


@SHIELD_BLOCKING
def python_danger_basic_auth(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_danger_basic_auth(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_azure_linux_vm_insecure_authentication(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return lnx_vm_insecure_auth(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_azure_virtual_machine_insecure_authentication(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return vm_insecure_auth(shard, method_supplies)
