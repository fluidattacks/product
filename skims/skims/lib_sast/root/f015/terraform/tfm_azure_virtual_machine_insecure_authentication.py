from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_argument,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _az_vm_insecure_authentication(graph: Graph, nid: NId) -> NId | None:
    if (
        argument := get_argument(graph, nid, "os_profile_linux_config")
    ) and not get_optional_attribute(graph, argument, "ssh_keys"):
        return argument
    return None


def tfm_azure_virtual_machine_insecure_authentication(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AZURE_VIRTUAL_MACHINE_INSECURE_AUTHENTICATION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if (
                (name := graph.nodes[node].get("name"))
                and name == "azurerm_virtual_machine"
                and (report_id := _az_vm_insecure_authentication(graph, node))
            ):
                yield report_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
