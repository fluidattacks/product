from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_argument,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _az_vm_no_admin_auth(graph: Graph, nid: NId) -> bool:
    return get_argument(graph, nid, "admin_ssh_key") is None and not get_optional_attribute(
        graph,
        nid,
        "admin_password",
    )


def tfm_azure_linux_vm_insecure_authentication(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AZURE_LINUX_VM_INSECURE_AUTHENTICATION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if (
                (name := graph.nodes[node].get("name"))
                and name == "azurerm_linux_virtual_machine"
                and _az_vm_no_admin_auth(graph, node)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
