from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from model.core import (
    MethodExecutionResult,
)


def is_danger_request(
    method: MethodsEnum,
    graph: Graph,
    obj_id: NId,
    method_supplies: MethodSupplies,
) -> bool:
    n_attrs = graph.nodes[obj_id]
    if (  # noqa: SIM103
        n_attrs.get("expression") == "getSession"
        and (sess_id := n_attrs.get("object_id"))
        and get_node_evaluation_results(
            method,
            graph,
            sess_id,
            {"userconnection"},
            graph_db=method_supplies.graph_db,
        )
    ):
        return True
    return False


def java_trust_boundary_violation(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_TRUST_BOUNDARY_VIOLATION
    danger_methods = {"setAttribute", "putValue"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            n_attrs = graph.nodes[node]
            if (
                n_attrs["expression"] in danger_methods
                and (obj_id := n_attrs.get("object_id"))
                and is_danger_request(method, graph, obj_id, method_supplies)
                and (al_id := graph.nodes[node].get("arguments_id"))
                and get_node_evaluation_results(
                    method,
                    graph,
                    al_id,
                    {"userparameters"},
                    graph_db=method_supplies.graph_db,
                )
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
