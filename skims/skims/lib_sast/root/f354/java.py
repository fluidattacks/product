from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.utils import (
    get_forward_paths,
)
from lib_sast.utils.graph import (
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def no_size_limit(method: MethodsEnum, graph: Graph, var_node: NId) -> bool:
    parent = pred_ast(graph, var_node)[0]
    var_name = graph.nodes[parent].get("variable")
    for path in get_forward_paths(graph, parent):
        for n_id in path:
            n_attrs = graph.nodes[n_id]
            if (
                n_attrs["label_type"] == "MethodInvocation"
                and n_attrs["expression"] == "setMaxUploadSize"
                and (symbol_id := n_attrs.get("object_id"))
                and graph.nodes[symbol_id]["symbol"] == var_name
            ):
                return get_node_evaluation_results(method, graph, n_id, set())
    return True


def java_insecure_file_upload_size(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_UPLOAD_SIZE_LIMIT
    danger_objs = {"CommonsMultipartResolver", "MultipartConfigFactory"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            n_name = graph.nodes[n_id].get("name")
            if n_name in danger_objs and no_size_limit(method, graph, n_id):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
