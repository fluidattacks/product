from lib_sast.root.f354.java import (
    java_insecure_file_upload_size as _java_insecure_file_upload_size,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def java_insecure_file_upload_size(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_insecure_file_upload_size(shard, method_supplies)
