from lib_sast.root.f043.php.php_insecure_content_security_policy import (
    insecure_content_security_policy as _php_insecure_content_security_policy,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def php_insecure_content_security_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _php_insecure_content_security_policy(shard, method_supplies)
