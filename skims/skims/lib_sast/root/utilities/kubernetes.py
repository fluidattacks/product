from __future__ import (
    annotations,
)

import functools
import operator
from dataclasses import dataclass
from typing import TYPE_CHECKING, TypeVar

from lib_sast.utils.graph import (
    adj_ast,
    match_ast_group_d,
)

if TYPE_CHECKING:
    from collections.abc import Callable

    from lib_sast.sast_model import (
        Graph,
        NId,
    )

CONTAINERS_TYPE = {
    "containers",
    "ephemeralContainers",
    "initContainers",
}

K8S_CONTAINER_RESOURCE = {
    "Pod",
    "Deployment",
    "StatefulSet",
    "DaemonSet",
    "ReplicaSet",
    "ReplicationController",
    "Job",
    "CronJob",
}

_A = TypeVar("_A")
_B = TypeVar("_B")


def _map(function: Callable[[_A], _B], items: list[_A]) -> list[_B]:
    return list(map(function, items))


def _filter(predicate: Callable[[_A], bool], items: list[_A]) -> list[_A]:
    return list(filter(predicate, items))


@dataclass(frozen=True)
class _Private:
    pass


@dataclass(frozen=True)
class Natural:
    _private: _Private
    _value: int

    @staticmethod
    def new(value: int) -> Natural | Exception:
        if value >= 0:
            return Natural(_Private(), value)
        return ValueError("Natural cannot be < 0")

    @classmethod
    def assert_natural(cls, value: int) -> Natural:
        result = cls.new(value)
        if isinstance(result, Exception):
            raise result
        return result

    def map(self, int_case: Callable[[int], _A]) -> _A:
        return int_case(self._value)

    def __repr__(self) -> str:
        return f"Natural({self._value})"


def get_nodes_with_key(
    graph: Graph,
    n_ids: list[NId],
    key: str,
    depth: Natural | None,
) -> list[NId]:
    depth_value = -1 if depth is None else depth.map(lambda x: x)

    return [
        p_id
        for n_id in n_ids
        for p_id in match_ast_group_d(graph, n_id, "Pair", depth=depth_value)
        if get_key_value(graph, p_id)[0] == key
    ]


def get_spec_ids(graph: Graph, n_ids: list[NId]) -> list[NId]:
    valid_nodes = _filter(
        lambda nid: check_api_version(graph, nid),
        n_ids,
    )
    return get_nodes_with_key(graph, valid_nodes, "spec", None)


def get_list_from_node(graph: Graph, nid: NId | None) -> list:
    if nid:
        value_id = graph.nodes[nid]["value_id"]
        if graph.nodes[value_id]["label_type"] == "ArrayInitializer":
            child_ids = adj_ast(graph, value_id)
            return [graph.nodes[c_id].get("value") for c_id in child_ids]
        return [graph.nodes[value_id].get("value")]
    return []


def get_attr_inside_attrs(graph: Graph, nid: NId, attrs: list) -> tuple[str | None, str, NId]:
    curr_nid = nid
    final_key, final_val, final_id = None, "", ""
    attr, attr_val, attr_id = None, "", ""
    for key in attrs:
        if not curr_nid:
            break
        attr, attr_val, attr_id = get_attribute(graph, curr_nid, key)
        if not attr:
            break
        curr_nid = graph.nodes[attr_id].get("value_id")
    else:
        final_key, final_val, final_id = attr, attr_val, attr_id

    return final_key, final_val, final_id


def get_key_value(graph: Graph, nid: NId) -> tuple[str, str]:
    key_id = graph.nodes[nid]["key_id"]
    key = graph.nodes[key_id]["value"]
    value_id = graph.nodes[nid]["value_id"]
    value = ""
    if graph.nodes[value_id]["label_type"] == "ArrayInitializer":
        child_id = adj_ast(graph, value_id, label_type="Literal")
        if len(child_id) > 0:
            value = graph.nodes[child_id[0]].get("value", "")
    else:
        value = graph.nodes[value_id].get("value", "")
    return key, value


def get_container_spec(graph: Graph, n_ids: list[NId]) -> list[NId]:
    valid_nodes = _filter(
        lambda nid: check_resources(graph, nid),
        n_ids,
    )
    spec_nids = get_nodes_with_key(graph, valid_nodes, "spec", None)
    spec_whitout_cont = _filter(
        lambda n_id: not get_nodes_with_key(
            graph,
            [n_id],
            "containers",
            Natural.assert_natural(2),
        ),
        spec_nids,
    )
    return list(set(spec_nids) - set(spec_whitout_cont))


def get_container_objects(graph: Graph, spec_ids: list[NId]) -> list[NId]:
    return functools.reduce(
        operator.iadd,
        (
            match_ast_group_d(graph, n_id, "Object", depth=2)
            for n_id in _get_containers(
                graph,
                spec_ids,
                Natural.assert_natural(2),
            )
        ),
        [],
    )


def _get_containers(graph: Graph, n_ids: list[NId], depth: Natural | None) -> list[NId]:
    depth_value = -1 if depth is None else depth.map(lambda x: x)

    return functools.reduce(
        operator.iadd,
        (
            filter(
                lambda p_id: get_key_value(graph, p_id)[0] in CONTAINERS_TYPE,
                match_ast_group_d(graph, nid, "Pair", depth=depth_value),
            )
            for nid in n_ids
        ),
        [],
    )


def get_attribute(graph: Graph, object_id: NId, expected_attr: str) -> tuple[str | None, str, NId]:
    if object_id != "":
        for attr_id in adj_ast(graph, object_id, label_type="Pair"):
            key, value = get_key_value(graph, attr_id)
            if key == expected_attr:
                return key, value, attr_id
    return None, "", ""


def get_optional_attribute(
    graph: Graph,
    object_id: NId,
    expected_attr: str,
) -> tuple[str, str, NId] | None:
    """Search an optional attribute in an object node.

    If found, return a tuple with [key_name, key_value, Node_Id]
    """
    for n_id in match_ast_group_d(graph, object_id, "Pair"):
        key, value = get_key_value(graph, n_id)
        if key == expected_attr:
            return key, value, n_id
    return None


def get_optional_attribute_d(
    graph: Graph,
    n_id: NId,
    expected_attr: str,
    depth: int,
) -> tuple[str, str, NId] | None:
    def check_attr(n_id: NId) -> tuple[str, str, NId] | None:
        key, value = get_key_value(graph, n_id)
        return (key, value, n_id) if key == expected_attr else None

    matching_attrs = map(check_attr, match_ast_group_d(graph, n_id, "Pair", depth))
    return next(filter(lambda x: x is not None, matching_attrs), None)


def get_pod_spec(graph: Graph, nid: NId) -> NId | None:
    if (
        check_template_integrity(graph, nid)
        and (kind := get_attribute(graph, nid, "kind"))
        and kind[1] == "Pod"
    ):
        spec, _, spec_id = get_attribute(graph, nid, "spec")
        if spec:
            return graph.nodes[spec_id]["value_id"]
    return None


def get_containers_capabilities(graph: Graph, sec_ctx: NId, type_cap: str) -> list:
    cap = get_attribute(graph, sec_ctx, "capabilities")
    if cap[0]:
        cap_attrs = graph.nodes[cap[2]]["value_id"]
        specific_cap = get_attribute(graph, cap_attrs, type_cap)
        if specific_cap[0]:
            return get_list_from_node(graph, specific_cap[2])
    return []


def check_api_version(graph: Graph, nid: NId) -> bool:
    return bool(get_attribute(graph, nid, "apiVersion")[0])


def check_template_integrity(graph: Graph, nid: NId) -> bool:
    valid_template = get_attribute(graph, nid, "apiVersion")
    return bool(valid_template[0])


def check_resources(graph: Graph, nid: NId) -> bool:
    return bool(
        get_optional_attribute(graph, nid, "apiVersion")
        and (pod_kind := get_optional_attribute(graph, nid, "kind"))
        and pod_kind[1].lower() in {res.lower() for res in K8S_CONTAINER_RESOURCE},
    )


def check_template_integrity_for_all_nodes(graph: Graph, n_ids: list[str]) -> bool:
    for nid in n_ids:
        if (
            get_optional_attribute(graph, nid, "apiVersion")
            and (pod_kind := get_optional_attribute(graph, nid, "kind"))
            and pod_kind[1].lower() == "pod"
        ):
            return True
    return False
