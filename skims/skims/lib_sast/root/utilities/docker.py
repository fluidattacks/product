from collections.abc import (
    Iterator,
)

from lib_sast.root.utilities.cloudformation import (
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    MethodSupplies,
    NId,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def get_key_value(graph: Graph, nid: NId) -> tuple[str, str]:
    key_id = graph.nodes[nid]["key_id"]
    key = graph.nodes[key_id]["value"]
    value_id = graph.nodes[nid]["value_id"]
    value = ""
    if graph.nodes[value_id]["label_type"] == "ArrayInitializer":
        child_id = adj_ast(graph, value_id, label_type="Literal")
        if len(child_id) > 0:
            value = graph.nodes[child_id[0]].get("value", "")
    else:
        value = graph.nodes[value_id].get("value", "")
    return key, value


def validate_path(path: str) -> bool:
    last_slash_index = path.rfind("/")
    filename = path[last_slash_index:]
    return "docker" in filename.lower()


def iterate_services_obj(graph: Graph, method_supplies: MethodSupplies) -> Iterator[NId]:
    for nid in method_supplies.selected_nodes:
        services = get_optional_attribute(graph, nid, "services")
        if not services:
            continue
        for c_id in adj_ast(graph, graph.nodes[services[2]]["value_id"]):
            if props := graph.nodes[c_id].get("value_id"):
                yield props


def iterate_env_variables(graph: Graph, method_supplies: MethodSupplies) -> Iterator[NId]:
    for nid in iterate_services_obj(graph, method_supplies):
        environment = get_optional_attribute(graph, nid, "environment")
        if not environment:
            continue

        env_variables = graph.nodes[environment[2]]["value_id"]
        if graph.nodes[env_variables]["label_type"] == "ArrayInitializer":
            yield from adj_ast(graph, env_variables)
        else:
            yield env_variables
