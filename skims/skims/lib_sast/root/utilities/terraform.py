import json
from collections.abc import (
    Collection,
    Iterator,
)
from contextlib import (
    suppress,
)
from ipaddress import (
    AddressValueError,
    IPv4Network,
    IPv6Network,
)

from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_group_d,
)

K8S_CONTAINER_RESOURCE_DEPTHS = {
    "kubernetes_pod": 1,  # 'spec.containers' is directly inside 'spec'
    "kubernetes_pod_v1": 1,  # Same structure as 'kubernetes_pod'
    "kubernetes_deployment": 3,  # 'spec.template.spec.containers'
    "kubernetes_deployment_v1": 3,  # Same structure as 'kubernetes_deployment'
    "kubernetes_stateful_set": 3,  # 'spec.template.spec.containers'
    "kubernetes_stateful_set_v1": 3,  # Same structure as above
    "kubernetes_daemon_set": 3,  # 'spec.template.spec.containers'
    "kubernetes_daemon_set_v1": 3,  # Same structure as 'kubernetes_daemon_set'
    "kubernetes_replica_set": 3,  # 'spec.template.spec.containers'
    "kubernetes_replica_set_v1": 3,  # Same structure as above
    "kubernetes_replication_controller": 3,  # 'spec.template.spec.containers'
    "kubernetes_replication_controller_v1": 3,  # Same structure as above
    "kubernetes_job": 3,  # 'spec.template.spec.containers'
    "kubernetes_job_v1": 3,  # Same structure as 'kubernetes_job'
    "kubernetes_cron_job": 5,  # spec.jobTemplate.spec.template.spec.containers
    "kubernetes_cron_job_v1": 5,  # Same structure as 'kubernetes_cron_job'
}


def get_dict_values(dict_val: dict, *keys: str) -> Collection | None:
    cur_dict = dict_val
    for key in keys:
        if key in cur_dict:
            cur_dict = cur_dict[key]
        else:
            return None
    return cur_dict


def get_dict_from_attr(attr_val: str) -> dict:
    try:
        dict_value = json.loads(attr_val)
    except (TypeError, json.decoder.JSONDecodeError):
        dict_value = {}
    return dict_value


def is_cidr(cidr: str) -> bool:
    """Validate if a string is a valid CIDR."""
    result = False
    with suppress(AddressValueError, ValueError):
        IPv4Network(cidr, strict=False)
        result = True
    with suppress(AddressValueError, ValueError):
        IPv6Network(cidr, strict=False)
        result = True
    return result


def get_list_from_node(graph: Graph, nid: NId | None) -> list:
    if nid:
        value_id = graph.nodes[nid]["value_id"]
        if graph.nodes[value_id]["label_type"] == "ArrayInitializer":
            child_ids = adj_ast(graph, value_id)
            return [graph.nodes[c_id].get("value") for c_id in child_ids]
        return [graph.nodes[value_id].get("value")]
    return []


def get_argument(graph: Graph, nid: NId, expected_resource: str) -> NId | None:
    for block_id in adj_ast(graph, nid, label_type="Object"):
        if graph.nodes[block_id].get("name") == expected_resource:
            return block_id
    return None


def get_argument_iterator(graph: Graph, nid: NId, expected_resource: str) -> Iterator[NId]:
    for block_id in adj_ast(graph, nid, label_type="Object"):
        if graph.nodes[block_id].get("name") == expected_resource:
            yield block_id


def get_argument_iterator_d(
    graph: Graph,
    nid: NId,
    expected_resource: str,
    depth: int = 1,
) -> Iterator[NId]:
    for block_id in adj_ast(graph, nid, depth, label_type="Object"):
        if graph.nodes[block_id].get("name") == expected_resource:
            yield block_id


def iter_statements_from_policy_document(graph: Graph, nid: NId) -> Iterator[NId]:
    for block_id in adj_ast(graph, nid, label_type="Object"):
        if graph.nodes[block_id].get("name") == "statement":
            yield block_id


def get_key_value(graph: Graph, nid: NId) -> tuple[str, str]:
    key_id = graph.nodes[nid]["key_id"]
    key = graph.nodes[key_id]["value"]
    value_id = graph.nodes[nid]["value_id"]
    value = ""
    if graph.nodes[value_id]["label_type"] == "ArrayInitializer":
        child_id = adj_ast(graph, value_id, label_type="Literal")
        if len(child_id) > 0:
            value = graph.nodes[child_id[0]].get("value", "")
    else:
        value = graph.nodes[value_id].get("value", "")
    return key, value


def get_optional_attribute(
    graph: Graph,
    object_id: NId,
    expected_attr: str,
) -> tuple[str, str, NId] | None:
    """Search an optional attribute in an object node.

    If found, return a tuple with [key_name, key_value, Node_Id]
    """
    for n_id in match_ast_group_d(graph, object_id, "Pair"):
        key, value = get_key_value(graph, n_id)
        if key == expected_attr:
            return key, value, n_id
    return None


def get_key_value_list(graph: Graph, nid: NId) -> tuple[str, list]:
    key_id = graph.nodes[nid]["key_id"]
    key = graph.nodes[key_id]["value"]
    value_id = graph.nodes[nid]["value_id"]
    if graph.nodes[value_id]["label_type"] == "ArrayInitializer":
        child_ids = adj_ast(graph, value_id, label_type="Literal")
        result = [graph.nodes[c_id].get("value") for c_id in child_ids]
        return key, result
    return key, []


def get_optional_list(
    graph: Graph,
    object_id: NId,
    expected_attr: str,
) -> tuple[str, list[str], NId] | None:
    """Search an optional attribute in an object node.

    If found, return a tuple with [key_name, key_value_list, Node_Id]
    """
    for n_id in match_ast_group_d(graph, object_id, "Pair"):
        key, value = get_key_value_list(graph, n_id)
        if key == expected_attr:
            return key, value, n_id
    return None


def get_attr_inside_attrs(graph: Graph, nid: NId, attrs: list) -> tuple[str, str, NId] | None:
    curr_nid = nid
    attr = None
    for key in attrs:
        if curr_nid and (attr := get_optional_attribute(graph, curr_nid, key)):
            curr_nid = graph.nodes[attr[2]].get("value_id")
    return attr


def list_has_string(graph: Graph, nid: NId, value: str) -> bool:
    child_ids = adj_ast(graph, nid)
    for c_id in child_ids:
        curr_value = graph.nodes[c_id].get("value")
        if curr_value and curr_value == value:
            return True
    return False


def get_safe_port_range(from_port_val: str, to_port_val: str) -> set[int]:
    try:
        port_range = set(
            range(
                int(from_port_val),
                int(to_port_val) + 1,
            ),
        )
    except (TypeError, ValueError):
        port_range = set()

    return port_range
