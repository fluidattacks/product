from lib_sast.root.f402.terraform import (
    tfm_azure_app_service_logging_disabled as _tfm_service_logging_disabled,
)
from lib_sast.root.f402.terraform import (
    tfm_azure_sql_server_audit_log_retention as _tfm_sql_server_log_retention,
)
from lib_sast.root.f402.terraform import (
    tfm_azure_storage_logging_disabled as _tfm_azure_storage_logging_disabled,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def tfm_azure_app_service_logging_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_service_logging_disabled(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_azure_sql_server_audit_log_retention(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_sql_server_log_retention(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_azure_storage_logging_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_azure_storage_logging_disabled(shard, method_supplies)
