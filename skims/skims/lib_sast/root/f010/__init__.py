from lib_sast.root.f010.typescript.ts_xss_pug_from_file import (
    ts_xss_pug_from_file as _ts_xss_pug_from_file,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def ts_xss_pug_from_file(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_xss_pug_from_file(shard, method_supplies)
