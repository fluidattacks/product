from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.root.utilities.javascript import (
    file_imports_module,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils import (
    graph as g,
)
from model.core import (
    MethodExecutionResult,
)


def _get_parent_var_name(graph: Graph, n_id: NId) -> str | None:
    nodes = graph.nodes
    if (p_ids := g.pred_ast(graph, n_id)) and (p_id := p_ids[0]):
        if (label_type := nodes[p_id].get("label_type")) == "VariableDeclaration":
            return nodes[p_id].get("variable")
        if label_type == "Assignment" and (var_id := nodes[p_id].get("variable_id")):
            return nodes[var_id].get("symbol")

    return None


def _get_invocation_if_exists(
    graph: Graph,
    invocation_n_ids: list[NId],
    expression: str,
) -> NId | None:
    for n_id in invocation_n_ids:
        if graph.nodes[n_id].get("expression") == expression:
            return n_id
    return None


def _get_definition_n_id(graph: Graph, symbol_n_id: NId) -> NId | None:
    symbol = graph.nodes[symbol_n_id].get("symbol")

    for path in get_backward_paths(graph, symbol_n_id):
        if (def_id := definition_search(graph, path, symbol)) and (
            val_id := graph.nodes[def_id].get("value_id")
        ):
            return val_id
    return None


def _declaration_access_filesystem(graph: Graph, function_name: str) -> bool:
    for n_id in g.matching_nodes(graph, label_type="MethodDeclaration", name=function_name):
        if g.adj_ast(
            graph,
            n_id,
            -1,
            label_type="MethodInvocation",
            expression="fs.readFileSync",
        ):
            return True
    return False


def _any_argument_comes_from_file(graph: Graph, args_n_id: NId) -> bool:
    symbols: list[NId] = g.match_ast_group(graph, args_n_id, "SymbolLookup", depth=-1).get(
        "SymbolLookup",
        [],
    )

    for symbol_n_id in symbols:
        if (
            (function_n_id := _get_definition_n_id(graph, symbol_n_id))
            and (function_name := graph.nodes[function_n_id].get("expression"))
            and (_declaration_access_filesystem(graph, function_name))
        ):
            return True
    return False


def has_xss_danger(
    graph: Graph,
    compiler_func_name: str,
    invocation_n_ids: list[NId],
) -> NId | None:
    if (
        (compiler_n_id := _get_invocation_if_exists(graph, invocation_n_ids, compiler_func_name))
        and (compiled_obj_name := _get_parent_var_name(graph, compiler_n_id))
        and (
            danger_invocation_n_id := _get_invocation_if_exists(
                graph,
                invocation_n_ids,
                f"{compiled_obj_name}.replace",
            )
        )
        and (args_n_id := graph.nodes[danger_invocation_n_id].get("arguments_id"))
        and (_any_argument_comes_from_file(graph, args_n_id))
    ):
        return danger_invocation_n_id
    return None


def response_with_injected_template(
    graph: Graph,
    template_n_id: NId,
    invocation_n_ids: list[NId],
) -> bool:
    nodes = graph.nodes
    if not (var_name := _get_parent_var_name(graph, template_n_id)):
        return False
    for n_id in invocation_n_ids:
        if (expression := nodes[n_id].get("expression")) and (
            expression.split(".", maxsplit=1)[-1] == "send"
            and (f_arg := get_n_arg(graph, n_id, 0))
            and nodes[f_arg].get("symbol") == var_name
        ):
            return True

    return False


def ts_xss_pug_from_file(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TS_XSS_PUG_FROM_FILE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        if file_imports_module(graph, "dompurify") or not (
            file_imports_module(graph, "pug")
            and file_imports_module(graph, "fs")
            and file_imports_module(graph, "express")
        ):
            return

        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("expression") == "pug.compile"
                and (compiler_func_name := _get_parent_var_name(graph, n_id))
                and (
                    vuln_n_id := has_xss_danger(
                        graph,
                        compiler_func_name,
                        method_supplies.selected_nodes,
                    )
                )
                and (
                    response_with_injected_template(
                        graph,
                        vuln_n_id,
                        method_supplies.selected_nodes,
                    )
                )
            ):
                yield vuln_n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
