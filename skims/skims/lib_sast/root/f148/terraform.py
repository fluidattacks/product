from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import get_argument_iterator, get_optional_attribute
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def tfm_azure_app_service_ftp_deployments_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AZURE_APP_SERVICE_FTP_DEPLOYMENTS_ENABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        resource_nids = list(
            filter(
                lambda nid: graph.nodes[nid].get("name") == "azurerm_app_service",
                method_supplies.selected_nodes,
            ),
        )
        site_config: list[NId] = [
            attr
            for nid in resource_nids
            for attr in get_argument_iterator(graph, nid, "site_config")
        ]
        unsafe_ftps_state: list[NId] = [
            attr[2]
            for nid in site_config
            if (attr := get_optional_attribute(graph, nid, "ftps_state"))
            and attr[1] == "AllAllowed"
        ]
        yield from unsafe_ftps_state

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
