from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.f325.utils import (
    is_s3_action_writeable,
    is_wildcard_in_principals,
    policy_has_excessive_permissions,
    policy_has_excessive_permissions_json_encode,
    policy_has_excessive_permissions_policy_document,
)
from lib_sast.root.utilities.terraform import (
    get_argument,
    get_attr_inside_attrs,
    get_dict_from_attr,
    get_dict_values,
    get_list_from_node,
    get_optional_attribute,
    iter_statements_from_policy_document,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)
from utils.aws.iam import (
    is_action_permissive,
    is_public_principal,
    is_resource_permissive,
)


def _kms_key_exposed_in_jsonencode(graph: Graph, nid: NId) -> Iterator[NId]:
    child_id = adj_ast(graph, graph.nodes[nid]["arguments_id"])[0]
    if stmt := get_optional_attribute(graph, child_id, "Statement"):
        value_id = graph.nodes[stmt[2]]["value_id"]
        for c_id in adj_ast(graph, value_id, label_type="Object"):
            if (
                (effect := get_optional_attribute(graph, c_id, "Effect"))
                and effect[1] == "Allow"
                and (aws := get_attr_inside_attrs(graph, c_id, ["Principal", "AWS"]))
                and aws[1] == "*"
            ):
                yield aws[2]


def _kms_key_exposed_in_literal(attr_val: str, attr_id: NId) -> Iterator[NId]:
    dict_value = get_dict_from_attr(attr_val)
    statements = get_dict_values(dict_value, "Statement")
    for stmt in statements if isinstance(statements, list) else []:
        effect = stmt.get("Effect")
        aws = get_dict_values(stmt, "Principal", "AWS")
        if effect == "Allow" and aws and aws == "*":
            yield attr_id


def _kms_key_keys_exposed(graph: Graph, nid: NId) -> Iterator[NId]:
    if attr := get_optional_attribute(graph, nid, "policy"):
        value_id = graph.nodes[attr[2]]["value_id"]
        if graph.nodes[value_id]["label_type"] == "Literal":
            yield from _kms_key_exposed_in_literal(attr[1], attr[2])
        elif (
            graph.nodes[value_id]["label_type"] == "MethodInvocation"
            and graph.nodes[value_id]["expression"] == "jsonencode"
        ):
            yield from _kms_key_exposed_in_jsonencode(graph, value_id)


def _has_wildcard_on_policy_document(graph: Graph, nid: NId) -> Iterator[NId]:
    for stmt in iter_statements_from_policy_document(graph, nid):
        if policy_has_excessive_permissions_policy_document(graph, stmt):
            yield stmt


def _has_wildcard_on_policy_in_literal(attr_val: str) -> bool:
    dict_value = get_dict_from_attr(attr_val)
    statements = get_dict_values(dict_value, "Statement")
    for stmt in statements if isinstance(statements, list) else []:
        if policy_has_excessive_permissions(stmt):
            return True
    return False


def _has_wildcard_on_policy_in_jsonencode(
    graph: Graph,
    nid: NId,
) -> Iterator[NId]:
    child_id = adj_ast(graph, graph.nodes[nid]["arguments_id"])[0]
    if stmt := get_optional_attribute(graph, child_id, "Statement"):
        value_id = graph.nodes[stmt[2]]["value_id"]
        for c_id in adj_ast(graph, value_id, label_type="Object"):
            if policy_has_excessive_permissions_json_encode(graph, c_id):
                yield c_id


def _iam_wildcard_on_trust_policy(graph: Graph, nid: NId) -> Iterator[NId]:
    if attr := get_optional_attribute(graph, nid, "assume_role_policy"):
        value_id = graph.nodes[attr[2]]["value_id"]
        if graph.nodes[value_id]["label_type"] == "Literal" and _has_wildcard_on_policy_in_literal(
            attr[1],
        ):
            yield attr[2]
        elif (
            graph.nodes[value_id]["label_type"] == "MethodInvocation"
            and graph.nodes[value_id]["expression"] == "jsonencode"
        ):
            yield from _has_wildcard_on_policy_in_jsonencode(graph, value_id)


def _iam_has_wildcard_on_action_or_resource(graph: Graph, nid: NId) -> Iterator[NId]:
    if graph.nodes[nid]["name"] == "aws_iam_policy_document":
        yield from _has_wildcard_on_policy_document(graph, nid)
    elif attr := get_optional_attribute(graph, nid, "policy"):
        value_id = graph.nodes[attr[2]]["value_id"]
        if graph.nodes[value_id]["label_type"] == "Literal" and _has_wildcard_on_policy_in_literal(
            attr[1],
        ):
            yield attr[2]
        elif (
            graph.nodes[value_id]["label_type"] == "MethodInvocation"
            and graph.nodes[value_id]["expression"] == "jsonencode"
        ):
            yield from _has_wildcard_on_policy_in_jsonencode(graph, value_id)


def _permissive_policy_in_document(graph: Graph, nid: NId) -> Iterator[NId]:
    if get_argument(graph, nid, "condition") or get_argument(graph, nid, "principals"):
        return

    for stmt in iter_statements_from_policy_document(graph, nid):
        if (
            (not (effect := get_optional_attribute(graph, stmt, "effect")) or effect[1] == "Allow")
            and (action := get_optional_attribute(graph, stmt, "actions"))
            and (resources := get_optional_attribute(graph, stmt, "resources"))
        ):
            action_list = get_list_from_node(graph, action[2])
            resources_list = get_list_from_node(graph, resources[2])
            if all(
                (
                    any(map(is_action_permissive, action_list)),
                    any(map(is_resource_permissive, resources_list)),
                ),
            ):
                yield stmt


def _permissive_policy_in_literal(
    attr_val: str,
    attr_id: NId,
) -> Iterator[NId]:
    dict_value = get_dict_from_attr(attr_val)
    statements = get_dict_values(dict_value, "Statement")
    for stmt in statements if isinstance(statements, list) else []:
        effect = stmt.get("Effect")
        principal = stmt.get("Principal")
        condition = stmt.get("Condition")
        if effect == "Allow" and not principal and not condition:
            actions = stmt.get("Action", [])
            resource = stmt.get("Resource", [])
            if isinstance(actions, str):
                actions = [actions]
            if isinstance(resource, str):
                resource = [resource]
            if all(
                (
                    any(map(is_action_permissive, actions)),
                    any(map(is_resource_permissive, resource)),
                ),
            ):
                yield attr_id


def _permissive_policy_in_jsonencode(graph: Graph, nid: NId) -> Iterator[NId]:
    child_id = adj_ast(graph, graph.nodes[nid]["arguments_id"])[0]
    if stmt := get_optional_attribute(graph, child_id, "Statement"):
        value_id = graph.nodes[stmt[2]]["value_id"]
        for c_id in adj_ast(graph, value_id, label_type="Object"):
            if get_argument(graph, c_id, "Principal") or get_argument(graph, c_id, "Condition"):
                continue
            effect = get_optional_attribute(graph, c_id, "Effect")
            if (
                effect
                and effect[1] == "Allow"
                and (action := get_optional_attribute(graph, c_id, "Action"))
                and (resources := get_optional_attribute(graph, c_id, "Resource"))
            ):
                action_list = get_list_from_node(graph, action[2])
                resources_list = get_list_from_node(graph, resources[2])
                if all(
                    (
                        any(map(is_action_permissive, action_list)),
                        any(map(is_resource_permissive, resources_list)),
                    ),
                ):
                    yield c_id


def _permissive_policy(graph: Graph, nid: NId) -> Iterator[NId]:
    if graph.nodes[nid]["name"] == "aws_iam_policy_document":
        yield from _permissive_policy_in_document(graph, nid)
    elif attr := get_optional_attribute(graph, nid, "policy"):
        value_id = graph.nodes[attr[2]]["value_id"]
        if graph.nodes[value_id]["label_type"] == "Literal":
            yield from _permissive_policy_in_literal(attr[1], attr[2])
        elif (
            graph.nodes[value_id]["label_type"] == "MethodInvocation"
            and graph.nodes[value_id]["expression"] == "jsonencode"
        ):
            yield from _permissive_policy_in_jsonencode(graph, value_id)


def _bucket_policy_allows_public_access_in_jsonencode(graph: Graph, nid: NId) -> Iterator[NId]:
    child_id = adj_ast(graph, graph.nodes[nid]["arguments_id"])[0]
    if stmts := get_optional_attribute(graph, child_id, "Statement"):
        value_id = graph.nodes[stmts[2]]["value_id"]
        for c_id in adj_ast(graph, value_id, label_type="Object"):
            effect = get_optional_attribute(graph, c_id, "Effect")
            if not effect or effect[1] != "Allow":
                continue
            actions = get_optional_attribute(graph, c_id, "Action")
            if actions:
                action_list = get_list_from_node(graph, actions[2])
                if (
                    is_s3_action_writeable(action_list)
                    and (principal := get_optional_attribute(graph, c_id, "Principal"))
                    and is_public_principal(principal[1])
                ):
                    yield c_id


def _bucket_policy_allows_public_access_policy_resource(graph: Graph, nid: NId) -> Iterator[NId]:
    for stmt in iter_statements_from_policy_document(graph, nid):
        action = get_optional_attribute(graph, stmt, "actions")
        if not action:
            continue
        action_list = get_list_from_node(graph, action[2])
        effect = get_optional_attribute(graph, stmt, "effect")
        if (
            (not effect or effect[1] == "Allow")
            and is_wildcard_in_principals(graph, stmt)
            and is_s3_action_writeable(action_list)
        ):
            yield stmt


def _aux_bucket_policy_public(attr_val: str, attr_id: NId) -> Iterator[NId]:
    dict_value = get_dict_from_attr(attr_val)
    statements = get_dict_values(dict_value, "Statement")
    for stmt in statements if isinstance(statements, list) else []:
        effect = stmt.get("Effect")
        principal = stmt.get("Principal", "")
        actions = stmt.get("Action", [])
        if effect == "Allow" and is_public_principal(principal) and is_s3_action_writeable(actions):
            yield attr_id


def _bucket_policy_allows_public_access(graph: Graph, nid: NId) -> Iterator[NId]:
    if graph.nodes[nid]["name"] == "aws_iam_policy_document":
        yield from _bucket_policy_allows_public_access_policy_resource(graph, nid)
    else:
        pol_attr = get_optional_attribute(graph, nid, "policy")
        if not pol_attr:
            return
        value_id = graph.nodes[pol_attr[2]]["value_id"]
        if graph.nodes[value_id]["label_type"] == "Literal":
            yield from _aux_bucket_policy_public(pol_attr[1], pol_attr[2])
        elif (
            graph.nodes[value_id]["label_type"] == "MethodInvocation"
            and graph.nodes[value_id]["expression"] == "jsonencode"
        ):
            yield from _bucket_policy_allows_public_access_in_jsonencode(graph, value_id)


def _sqs_is_public_in_jsonencode(graph: Graph, nid: NId) -> Iterator[NId]:
    child_id = adj_ast(graph, graph.nodes[nid]["arguments_id"])[0]
    if stmts := get_optional_attribute(graph, child_id, "Statement"):
        value_id = graph.nodes[stmts[2]]["value_id"]
        for c_id in adj_ast(graph, value_id, label_type="Object"):
            principal = get_optional_attribute(graph, c_id, "Principal")
            condition = get_optional_attribute(graph, c_id, "Condition")
            if principal and is_public_principal(principal[1]) and not condition:
                yield c_id


def _sqs_is_public_policy_resource(graph: Graph, nid: NId) -> Iterator[NId]:
    for stmt in iter_statements_from_policy_document(graph, nid):
        principal = get_optional_attribute(graph, stmt, "principals")
        condition = get_optional_attribute(graph, stmt, "condition")
        if principal and is_public_principal(principal[1]) and not condition:
            yield stmt


def _aux_sqs_is_public(attr_val: str, attr_id: NId) -> Iterator[NId]:
    dict_value = get_dict_from_attr(attr_val)
    statements = get_dict_values(dict_value, "Statement")
    for stmt in statements if isinstance(statements, list) else []:
        principal = stmt.get("Principal", "")
        condition = stmt.get("Condition")
        if is_public_principal(principal) and not condition:
            yield attr_id


def _sqs_is_public(graph: Graph, nid: NId) -> Iterator[NId]:
    if graph.nodes[nid]["name"] == "aws_iam_policy_document":
        yield from _sqs_is_public_policy_resource(graph, nid)
    else:
        pol_attr = get_optional_attribute(graph, nid, "policy")
        if not pol_attr:
            return
        value_id = graph.nodes[pol_attr[2]]["value_id"]
        if graph.nodes[value_id]["label_type"] == "Literal":
            yield from _aux_sqs_is_public(pol_attr[1], pol_attr[2])
        elif (
            graph.nodes[value_id]["label_type"] == "MethodInvocation"
            and graph.nodes[value_id]["expression"] == "jsonencode"
        ):
            yield from _sqs_is_public_in_jsonencode(graph, value_id)


def tfm_sqs_is_public(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_SQS_IS_PUBLIC

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in {
                "aws_sqs_queue",
                "aws_sqs_queue_policy",
            }:
                yield from _sqs_is_public(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_bucket_policy_allows_public_access(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_BUCKET_POLICY_ALLOWS_PUBLIC_ACCESS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in {
                "aws_iam_group_policy",
                "aws_iam_policy",
                "aws_iam_role_policy",
                "aws_iam_user_policy",
                "aws_iam_policy_document",
            }:
                yield from _bucket_policy_allows_public_access(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_iam_has_wildcard_on_trust_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_IAM_TRUST_POLICY_ALLOW_WILDCARD_ACTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "aws_iam_role":
                yield from _iam_wildcard_on_trust_policy(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_iam_has_wildcard_on_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_IAM_HAS_WILDCARD_RESOURCE_ON_WRITE_ACTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in {
                "aws_iam_group_policy",
                "aws_iam_policy",
                "aws_iam_role_policy",
                "aws_iam_user_policy",
                "aws_iam_policy_document",
            }:
                yield from _iam_has_wildcard_on_action_or_resource(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_iam_permissive_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TERRAFORM_PERMISSIVE_POLICY

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in {
                "aws_iam_group_policy",
                "aws_iam_policy",
                "aws_iam_role_policy",
                "aws_iam_user_policy",
                "aws_iam_policy_document",
            }:
                yield from _permissive_policy(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_kms_master_keys_exposed_to_everyone(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_KMS_KEY_HAS_MASTER_KEYS_EXPOSED_TO_EVERYONE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "aws_kms_key":
                yield from _kms_key_keys_exposed(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
