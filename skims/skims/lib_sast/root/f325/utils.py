import re

from lib_sast.root.utilities.terraform import (
    get_argument,
    get_list_from_node,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    NId,
)
from utils.aws.iam import (
    ACTIONS,
)


def has_attribute_wildcard(attribute: str | list) -> bool:
    result = False
    for value in attribute if isinstance(attribute, list) else [attribute]:
        if value == "*":
            result = True
            break
    return result


def has_write_actions(actions: str | list) -> bool:
    for entry in actions if isinstance(actions, list) else [actions]:
        if len(entry.split(":")) != 2:
            continue

        service, action = entry.split(":")
        if service not in ACTIONS:
            continue

        service_write_actions = ACTIONS[service].get("write", [])
        wildcard_res = ACTIONS[service].get("wildcard_resource", [])

        if action in service_write_actions and action not in wildcard_res:
            return True

        if (
            "*" in action
            and (wild_act := action.replace("*", ".*"))
            and any(
                re.match(wild_act, act) and act not in wildcard_res for act in service_write_actions
            )
        ):
            return True
    return False


def policy_has_excessive_permissions(stmt: dict) -> bool:
    has_excessive_permissions = False
    if stmt.get("Effect") == "Allow":
        actions = stmt.get("Action", [])
        resource = stmt.get("Resource", [])
        if has_attribute_wildcard(resource) and (
            has_attribute_wildcard(actions) or has_write_actions(actions)
        ):
            has_excessive_permissions = True
    return has_excessive_permissions


def policy_has_excessive_permissions_policy_document(graph: Graph, stmt: NId) -> bool:
    has_excessive_permissions = False
    if (
        (not (effect := get_optional_attribute(graph, stmt, "effect")) or effect[1] == "Allow")
        and (actions := get_optional_attribute(graph, stmt, "actions"))
        and (resources := get_optional_attribute(graph, stmt, "resources"))
    ):
        action_list = get_list_from_node(graph, actions[2])
        resources_list = get_list_from_node(graph, resources[2])
        if has_attribute_wildcard(resources_list) and (
            has_attribute_wildcard(action_list) or has_write_actions(action_list)
        ):
            has_excessive_permissions = True
    return has_excessive_permissions


def policy_has_excessive_permissions_json_encode(graph: Graph, stmt: NId) -> bool:
    has_excessive_permissions = False
    if (
        (not (effect := get_optional_attribute(graph, stmt, "effect")) or effect[1] == "Allow")
        and (actions := get_optional_attribute(graph, stmt, "Action"))
        and (resources := get_optional_attribute(graph, stmt, "Resource"))
    ):
        action_list = get_list_from_node(graph, actions[2])
        resources_list = get_list_from_node(graph, resources[2])
        if has_attribute_wildcard(resources_list) and (
            has_attribute_wildcard(action_list) or has_write_actions(action_list)
        ):
            has_excessive_permissions = True
    return has_excessive_permissions


def is_s3_action_writeable(actions: list | str) -> bool:
    actions_list = actions if isinstance(actions, list) else [actions]
    action_start_with = [
        "Copy",
        "Create",
        "Delete",
        "Put",
        "Restore",
        "Update",
        "Upload",
        "Write",
    ]
    for action in actions_list:
        if any(action.startswith(f"s3:{atw}") for atw in action_start_with):
            return True
    return False


def is_wildcard_in_principals(graph: Graph, stmt: NId) -> bool:
    if (
        (principal := get_argument(graph, stmt, "principals"))
        and (types := get_optional_attribute(graph, principal, "type"))
        and (identifier := get_optional_attribute(graph, principal, "identifiers"))
    ):
        identifier_list = get_list_from_node(graph, identifier[2])
        if types[1] == "*" and "*" in identifier_list:
            return True
    return False
