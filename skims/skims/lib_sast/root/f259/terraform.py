from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_argument,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _db_no_point_in_time_recovery(graph: Graph, nid: NId) -> NId | None:
    if point := get_argument(graph, nid, "point_in_time_recovery"):
        enabled = get_optional_attribute(graph, point, "enabled")
        if not enabled:
            return nid
        if enabled[1].lower() == "false":
            return enabled[2]
    return None


def _dynamo_has_not_deletion_protection(graph: Graph, nid: NId) -> NId | None:
    del_protection = get_optional_attribute(graph, nid, "deletion_protection_enabled")
    if not del_protection:
        return nid
    if del_protection[1].lower() == "false":
        return del_protection[2]
    return None


def tfm_dynamo_has_not_deletion_protection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_DYNAMO_HAS_NOT_DELETION_PROTECTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "aws_dynamodb_table" and (
                report := _dynamo_has_not_deletion_protection(graph, nid)
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def tfm_db_no_point_in_time_recovery(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_DB_NO_POINT_IN_TIME_RECOVERY

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "aws_dynamodb_table" and (
                report := _db_no_point_in_time_recovery(graph, nid)
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
