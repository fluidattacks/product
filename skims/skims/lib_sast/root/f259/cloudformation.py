from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    FALSE_OPTIONS,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_attribute,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _has_not_point_in_time_recovery(graph: Graph, nid: NId) -> Iterator[NId]:
    prop, _, prop_id = get_attribute(graph, nid, "Properties")
    if prop:
        val_id = graph.nodes[prop_id]["value_id"]
        recovery, _, recovery_id = get_attribute(graph, val_id, "PointInTimeRecoverySpecification")
        if not recovery:
            yield prop_id
        else:
            recovery_attrs = graph.nodes[recovery_id]["value_id"]
            point_rec, point_rec_val, point_rec_id = get_attribute(
                graph,
                recovery_attrs,
                "PointInTimeRecoveryEnabled",
            )
            if point_rec and point_rec_val in FALSE_OPTIONS:
                yield point_rec_id


def _dynamo_has_not_deletion_protection(graph: Graph, nid: NId) -> Iterator[NId]:
    prop, _, prop_id = get_attribute(graph, nid, "Properties")
    if prop:
        val_id = graph.nodes[prop_id]["value_id"]
        deletion_prot, del_val, del_id = get_attribute(graph, val_id, "DeletionProtectionEnabled")
        if not deletion_prot:
            yield prop_id
        elif del_val in FALSE_OPTIONS:
            yield del_id


def cfn_dynamo_has_not_deletion_protection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_DYNAMO_HAS_NOT_DELETION_PROTECTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (resource := get_optional_attribute(graph, nid, "Type")) and resource[
                1
            ] == "AWS::DynamoDB::Table":
                yield from _dynamo_has_not_deletion_protection(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def cfn_has_not_point_in_time_recovery(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_HAS_NOT_POINT_IN_TIME_RECOVERY

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (resource := get_optional_attribute(graph, nid, "Type")) and resource[
                1
            ] == "AWS::DynamoDB::Table":
                yield from _has_not_point_in_time_recovery(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
