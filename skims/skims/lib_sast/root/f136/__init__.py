from lib_sast.root.f136.c_sharp import (
    cs_has_public_cache_header as _cs_has_public_cache_header,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cs_has_public_cache_header(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cs_has_public_cache_header(shard, method_supplies)
