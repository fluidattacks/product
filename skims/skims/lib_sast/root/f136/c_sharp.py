from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils import (
    graph as g,
)
from model.core import (
    MethodExecutionResult,
)


def value_set_true(graph: Graph, n_id: NId) -> bool:
    return bool(
        (p_id := next(iter(g.pred_ast(graph, n_id)), None))
        and (any(graph.nodes[n_id].get("value") == "true" for n_id in g.adj_ast(graph, p_id))),
    )


def check_danger_instantiation(graph: Graph, n_id: NId) -> bool:
    n_attrs = graph.nodes[n_id]

    return bool(
        n_attrs.get("name") == "CacheControlHeaderValue"
        and (
            public_attr := next(
                iter(
                    g.adj_ast(
                        graph,
                        n_id,
                        -1,
                        strict=False,
                        label_type="SymbolLookup",
                        symbol="Public",
                    ),
                ),
                None,
            )
        )
        and (value_set_true(graph, public_attr)),
    )


def check_danger_assignment(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    return bool(
        graph.nodes[n_id].get("member") == "Public"
        and value_set_true(graph, n_id)
        and get_node_evaluation_results(method, graph, n_id, set()),
    )


def cs_has_public_cache_header(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CS_HAS_PUBLIC_CACHE_HEADER

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            label_type = graph.nodes[node].get("label_type")

            if (label_type == "ObjectCreation" and check_danger_instantiation(graph, node)) or (
                label_type == "MemberAccess" and check_danger_assignment(graph, node, method)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
