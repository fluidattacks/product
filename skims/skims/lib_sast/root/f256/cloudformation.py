from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    FALSE_OPTIONS,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _rds_has_not_termination_protection(graph: Graph, nid: NId) -> NId | None:
    if (
        (props := get_optional_attribute(graph, nid, "Properties"))
        and (val_id := graph.nodes[props[2]]["value_id"])
        and (deletion_prot := get_optional_attribute(graph, val_id, "DeletionProtection"))
        and deletion_prot[1] in FALSE_OPTIONS
    ):
        return deletion_prot[2]
    return None


def _rds_has_not_automated_backups(graph: Graph, nid: NId) -> NId | None:
    props = get_optional_attribute(graph, nid, "Properties")
    if not props:
        return None
    val_id = graph.nodes[props[2]]["value_id"]
    backup = get_optional_attribute(graph, val_id, "BackupRetentionPeriod")
    if backup and backup[1] in (0, "0"):
        return backup[2]
    return None


def cfn_rds_has_not_automated_backups(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_RDS_HAS_NOT_AUTOMATED_BACKUPS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (
                (resource := get_optional_attribute(graph, nid, "Type"))
                and resource[1] in {"AWS::RDS::DBCluster", "AWS::RDS::DBInstance"}
                and (report := _rds_has_not_automated_backups(graph, nid))
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def cfn_rds_has_not_termination_protection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_RDS_HAS_NOT_TERMINATION_PROTECTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (
                (resource := get_optional_attribute(graph, nid, "Type"))
                and resource[1] in {"AWS::RDS::DBCluster", "AWS::RDS::DBInstance"}
                and (report := _rds_has_not_termination_protection(graph, nid))
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
