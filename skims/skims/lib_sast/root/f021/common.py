from collections.abc import Callable

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.root.utilities.javascript import (
    file_imports_module,
)
from lib_sast.sast_model import (
    Graph,
    MethodSupplies,
    NId,
)
from lib_sast.symbolic_eval.common import (
    check_js_ts_http_inputs,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def _member_access_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if check_js_ts_http_inputs(args):
        args.evaluation[args.n_id] = True
        args.triggers.add("userparameters")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "member_access": _member_access_evaluator,
}


def insecure_dynamic_xpath(
    graph: Graph,
    method: MethodsEnum,
    method_supplies: MethodSupplies,
) -> list[NId]:
    vuln_nodes: list[NId] = []
    danger_methods = {"select", "parse"}
    danger_set = {"userparameters"}
    if not (file_imports_module(graph, "fs") and file_imports_module(graph, "xpath")):
        return vuln_nodes

    for n_id in method_supplies.selected_nodes:
        expr = graph.nodes[n_id].get("expression")
        if (
            expr.split(".")[-1] in danger_methods
            and (f_arg := get_n_arg(graph, n_id, 0))
            and get_node_evaluation_results(
                method,
                graph,
                f_arg,
                danger_set,
                method_evaluators=METHOD_EVALUATORS,
            )
        ):
            vuln_nodes.append(n_id)
    return vuln_nodes
