from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.common import (
    owasp_user_connection,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from model.core import (
    MethodExecutionResult,
)


def _method_invocation_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if owasp_user_connection(args):
        args.triggers.clear()
        args.evaluation[args.n_id] = True
        args.triggers.add("userparameters")
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def _parameter_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id].get("variable_type") == "HttpServletRequest":
        args.triggers.add("userconnection")
    if args.graph.nodes[args.n_id].get("variable_type") == "HttpServletResponse":
        args.triggers.add("userresponse")
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "parameter": _parameter_evaluator,
    "method_invocation": _method_invocation_evaluator,
}


def java_unsafe_xpath_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_XPATH_INJECTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            n_attrs = graph.nodes[node]
            if n_attrs["expression"] == "evaluate" and get_node_evaluation_results(
                method,
                graph,
                node,
                {"userparameters"},
                graph_db=method_supplies.graph_db,
                method_evaluators=METHOD_EVALUATORS,
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
