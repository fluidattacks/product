from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _azure_key_vault_not_recoverable(graph: Graph, nid: NId) -> NId | None:
    protection = get_optional_attribute(graph, nid, "purge_protection_enabled")
    if not protection:
        return nid
    if protection[1] == "false":
        return protection[2]
    if (
        (soft_del := get_optional_attribute(graph, nid, "soft_delete_retention_days"))
        and soft_del[1].isdigit()
        and int(soft_del[1]) < 90
    ):
        return soft_del[2]
    return None


def tfm_azure_key_vault_not_recoverable(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AZURE_KEY_VAULT_NOT_RECOVERABLE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "azurerm_key_vault" and (
                report := _azure_key_vault_not_recoverable(graph, nid)
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
