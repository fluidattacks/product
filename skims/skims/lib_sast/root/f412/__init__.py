from lib_sast.root.f412.terraform import (
    tfm_azure_key_vault_not_recoverable as _tfm_key_vault_not_recoverable,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def tfm_azure_key_vault_not_recoverable(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_key_vault_not_recoverable(shard, method_supplies)
