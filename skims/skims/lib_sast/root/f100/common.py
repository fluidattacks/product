from lib_sast.root.utilities.common import (
    get_n_arg,
    is_node_definition_unsafe,
    is_sanitized,
)
from lib_sast.root.utilities.javascript import (
    file_imports_module,
    get_default_alias,
)
from lib_sast.sast_model import (
    Graph,
    MethodSupplies,
    NId,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def get_url_value_from_object(graph: Graph, n_id: NId) -> NId | None:
    if graph.nodes[n_id]["label_type"] == "Object":
        for child in adj_ast(graph, n_id, label_type="Pair"):
            if (
                (key_id := graph.nodes[child].get("key_id"))
                and graph.nodes[key_id].get("symbol", "") == "url"
                and (value_id := graph.nodes[child].get("value_id"))
                and graph.nodes[value_id]["label_type"] == "SymbolLookup"
            ):
                return value_id

    return None


def is_url_arg_controlled_by_user(graph: Graph, n_id: NId, axios_alias: str) -> NId | None:
    base_call = {axios_alias, f"{axios_alias}.request"}
    http_methods = ["get", "delete", "head", "options", "post", "put", "patch"]
    method_call = {f"{axios_alias}.{method}" for method in http_methods}
    expression = graph.nodes[n_id].get("expression", "")
    if (
        expression in base_call
        and (first_arg := get_n_arg(graph, n_id, 0))
        and (url_arg := get_url_value_from_object(graph, first_arg))
    ):
        return url_arg

    if expression in method_call and (first_arg := get_n_arg(graph, n_id, 0)):
        return first_arg

    return None


def contains_user_input(graph: Graph, n_id: NId) -> bool:
    for child in (n_id, *adj_ast(graph, n_id, -1, label_type="MemberAccess")):
        if graph.nodes[child].get("expression", "").startswith("req") and graph.nodes[child].get(
            "member",
            "",
        ) in {"query", "body"}:
            return True

    return False


def is_unsanitized_input(graph: Graph, url_arg: NId) -> bool:
    if contains_user_input(graph, url_arg):
        return True

    return is_node_definition_unsafe(graph, url_arg, contains_user_input) and not is_sanitized(
        graph,
        url_arg,
    )


def express_ssrf(graph: Graph, method_supplies: MethodSupplies) -> list[NId]:
    if not (
        file_imports_module(graph, "express") and (axios_alias := get_default_alias(graph, "axios"))
    ):
        return []

    return [
        n_id
        for n_id in method_supplies.selected_nodes
        if (
            graph.nodes[n_id].get("expression", "").startswith(axios_alias)
            and (url_arg := is_url_arg_controlled_by_user(graph, n_id, axios_alias))
            and is_unsanitized_input(graph, url_arg)
        )
    ]
