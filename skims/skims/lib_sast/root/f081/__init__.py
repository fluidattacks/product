from lib_sast.root.f081.cloudformation import (
    cfn_cognito_has_mfa_disabled as _cfn_cognito_has_mfa_disabled,
)
from lib_sast.root.f081.terraform import (
    tfm_cognito_has_mfa_disabled as _tfm_cognito_has_mfa_disabled,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_cognito_has_mfa_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_cognito_has_mfa_disabled(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_cognito_has_mfa_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_cognito_has_mfa_disabled(shard, method_supplies)
