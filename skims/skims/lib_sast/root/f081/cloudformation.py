from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_attribute,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _cognito_has_mfa_disabled(graph: Graph, nid: NId) -> Iterator[NId]:
    prop, _, prop_id = get_attribute(graph, nid, "Properties")
    if prop:
        val_id = graph.nodes[prop_id]["value_id"]
        mfa, mfa_val, mfa_id = get_attribute(graph, val_id, "MfaConfiguration")

        if mfa and mfa_val not in {"ON", "true"}:
            yield mfa_id


def cfn_cognito_has_mfa_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_COGNITO_HAS_MFA_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (resource := get_optional_attribute(graph, nid, "Type")) and resource[
                1
            ] == "AWS::Cognito::UserPool":
                yield from _cognito_has_mfa_disabled(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
