from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.f338.common import (
    get_created_hash_variables,
    get_vuln_nodes,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def ts_salting_is_harcoded(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TS_SALT_IS_HARCODED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        hashes = get_created_hash_variables(graph)
        for n_id in method_supplies.selected_nodes:
            if get_vuln_nodes(graph, n_id, hashes):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
