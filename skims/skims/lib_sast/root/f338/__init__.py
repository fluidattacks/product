from lib_sast.root.f338.c_sharp import (
    check_hashes_salt as _c_sharp_check_hashes_salt,
)
from lib_sast.root.f338.dart import (
    dart_salting_is_harcoded as _dart_salting_is_harcoded,
)
from lib_sast.root.f338.go import (
    go_salting_is_harcoded as _go_salting_is_harcoded,
)
from lib_sast.root.f338.java import (
    java_salting_is_hardcoded as _java_salting_is_hardcoded,
)
from lib_sast.root.f338.javascript import (
    js_salting_is_harcoded as _js_salting_is_harcoded,
)
from lib_sast.root.f338.kotlin import (
    kt_salting_is_harcoded as _kt_salting_is_harcoded,
)
from lib_sast.root.f338.typescript import (
    ts_salting_is_harcoded as _ts_salting_is_harcoded,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_check_hashes_salt(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_check_hashes_salt(shard, method_supplies)


@SHIELD_BLOCKING
def dart_salting_is_harcoded(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _dart_salting_is_harcoded(shard, method_supplies)


@SHIELD_BLOCKING
def go_salting_is_harcoded(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _go_salting_is_harcoded(shard, method_supplies)


@SHIELD_BLOCKING
def java_salting_is_hardcoded(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_salting_is_hardcoded(shard, method_supplies)


@SHIELD_BLOCKING
def js_salting_is_harcoded(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_salting_is_harcoded(shard, method_supplies)


@SHIELD_BLOCKING
def kt_salting_is_harcoded(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _kt_salting_is_harcoded(shard, method_supplies)


@SHIELD_BLOCKING
def ts_salting_is_harcoded(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_salting_is_harcoded(shard, method_supplies)
