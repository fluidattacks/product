from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    FALSE_OPTIONS,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _sec_group_uses_insecure_protocol(graph: Graph, nid: NId) -> Iterator[NId]:
    properties = get_optional_attribute(graph, nid, "Properties")
    if not properties:
        return
    val_id = graph.nodes[properties[2]]["value_id"]
    if ingress := get_optional_attribute(graph, val_id, "SecurityGroupIngress"):
        traffic_attrs = graph.nodes[ingress[2]]["value_id"]
        for c_id in adj_ast(graph, traffic_attrs):
            if (
                (protocol := get_optional_attribute(graph, c_id, "IpProtocol"))
                and protocol[1] in ("tcp", "-1")
                and (init_port := get_optional_attribute(graph, c_id, "FromPort"))
                and init_port[1] == "80"
            ):
                yield protocol[2]


def cfn_sec_group_uses_insecure_protocol(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_AWS_SEC_GROUP_USING_TCP

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (resource := get_optional_attribute(graph, nid, "Type")) and resource[
                1
            ] == "AWS::EC2::SecurityGroup":
                yield from _sec_group_uses_insecure_protocol(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def _server_ssl_disabled(graph: Graph, n_id: NId) -> NId | None:
    val_id = graph.nodes[n_id]["value_id"]
    ssl_config = get_optional_attribute(graph, val_id, "ssl")
    if (
        ssl_config
        and (config_id := graph.nodes[ssl_config[2]]["value_id"])
        and (ssl_enabled := get_optional_attribute(graph, config_id, "enabled"))
        and ssl_enabled[1] in FALSE_OPTIONS
    ):
        return ssl_enabled[2]
    return None


def cfn_server_disabled_ssl(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_SERVER_SSL_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            key_id = graph.nodes[nid]["key_id"]
            if graph.nodes[key_id]["value"] == "server" and (
                vuln_id := _server_ssl_disabled(graph, nid)
            ):
                yield vuln_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
