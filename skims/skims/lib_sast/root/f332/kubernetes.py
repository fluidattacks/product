from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_optional_attribute,
)
from lib_sast.root.utilities.kubernetes import (
    check_template_integrity,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_d,
    match_ast_group_d,
)
from model.core import (
    MethodExecutionResult,
)


def _insecure_port(graph: Graph, nid: NId) -> Iterator[NId]:
    if ports := get_optional_attribute(graph, nid, "ports"):
        ports_attrs = graph.nodes[ports[2]]["value_id"]
        for port_id in adj_ast(graph, ports_attrs):
            if (
                (port := get_optional_attribute(graph, port_id, "port"))
                and (protocol := get_optional_attribute(graph, port_id, "protocol"))
                and port[1] == "80"
                and protocol[1] == "TCP"
            ):
                yield port_id


def k8s_insecure_port(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    method = MethodsEnum.KUBERNETES_INSECURE_PORT

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (
                check_template_integrity(graph, nid)
                and (spec_id := get_optional_attribute(graph, nid, "spec"))
                and (t_id := graph.nodes[spec_id[2]]["value_id"])
            ):
                yield from _insecure_port(graph, t_id)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def _insecure_protocol(graph: Graph, nid: NId) -> Iterator[NId]:
    key_id = graph.nodes[nid]["key_id"]
    if graph.nodes[key_id]["value"] != "servers":
        return

    value_id = graph.nodes[nid]["value_id"]
    for server_id in match_ast_group_d(graph, value_id, "Object"):
        if (
            (port := get_optional_attribute(graph, server_id, "port"))
            and (specs_attrs := graph.nodes[port[2]]["value_id"])
            and (prot := get_optional_attribute(graph, specs_attrs, "protocol"))
            and prot[1].lower() == "http"
        ):
            yield prot[2]


def is_gateway_template(graph: Graph) -> bool:
    if (  # noqa: SIM103
        (main_id := match_ast_d(graph, "1", "Object"))
        and (api_version := get_optional_attribute(graph, main_id, "apiVersion"))
        and api_version[1].lower().startswith("networking.istio.io")
        and (pod_kind := get_optional_attribute(graph, main_id, "kind"))
        and pod_kind[1].lower() == "gateway"
    ):
        return True
    return False


def k8s_insecure_server_protocol(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.KUBERNETES_SERVER_USES_HTTP

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if is_gateway_template(graph):
            for nid in method_supplies.selected_nodes:
                yield from _insecure_protocol(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
