from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.json_utils import (
    is_parent,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def cfn_insecure_certificate(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_UNSAFE_CERTIFICATE

    def n_ids() -> Iterator[NId]:
        """Report insecure certificate.

        Source: https://nodejs.org/api/cli.html#node_tls_reject_unauthorizedvalue
        """
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (
                (key_id := graph.nodes[nid].get("key_id"))
                and (value_id := graph.nodes[nid].get("value_id"))
                and graph.nodes[key_id]["value"] == "NODE_TLS_REJECT_UNAUTHORIZED"
                and graph.nodes[value_id].get("value") == "0"
                and is_parent(
                    graph,
                    nid,
                    [
                        "environment",
                    ],
                )
            ):
                yield nid

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
