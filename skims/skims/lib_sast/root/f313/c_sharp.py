from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    is_any_library_imported,
    is_node_definition_unsafe,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_d,
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def is_lambda_danger(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    block_id = graph.nodes[n_id]["block_id"]
    if graph.nodes[block_id]["label_type"] == "ExecutionBlock" and (
        return_id := match_ast_d(graph, block_id, "Return")
    ):
        return get_node_evaluation_results(method, graph, return_id, set())
    return get_node_evaluation_results(method, graph, block_id, set())


def is_validation_dangerous(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    parent_id = pred_ast(graph, n_id)[0]
    if graph.nodes[parent_id]["label_type"] == "Assignment":
        assign_id = adj_ast(graph, parent_id)[1]
        label_type = graph.nodes[assign_id]["label_type"]
        if label_type == "MethodDeclaration" and is_lambda_danger(graph, assign_id, method):
            return True
    return False


def c_sharp_insecure_certificate_validation(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_INSECURE_CERTIFICATE_VALIDATION
    danger_m = "ServerCertificateValidationCallback"

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if graph.nodes[node].get("member") == danger_m and is_validation_dangerous(
                graph,
                node,
                method,
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def is_x509_certificate_2_object(graph: Graph, n_id: NId) -> bool:
    return (
        graph.nodes[n_id]["label_type"] == "ObjectCreation"
        and graph.nodes[n_id].get("name", "") == "X509Certificate2"
    )


def c_sharp_insecure_x509_cert_2(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_INSECURE_X509_CERT_2

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if is_any_library_imported(graph, {"System.Security.Cryptography"}):
            for node in method_supplies.selected_nodes:
                if (
                    graph.nodes[node].get("member", "") == "PrivateKey"
                    and (child := adj_ast(graph, node)[0])
                    and is_node_definition_unsafe(graph, child, is_x509_certificate_2_object)
                ):
                    yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
