from lib_sast.root.f106.javascript import (
    js_nosql_injection as _js_nosql_injection,
)
from lib_sast.root.f106.javascript import (
    js_nosql_injection_ternary as _js_nosql_injection_ternary,
)
from lib_sast.root.f106.typescript import (
    ts_nosql_injection as _ts_nosql_injection,
)
from lib_sast.root.f106.typescript import (
    ts_nosql_injection_ternary as _ts_nosql_injection_ternary,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def js_nosql_injection(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _js_nosql_injection(shard, method_supplies)


@SHIELD_BLOCKING
def ts_nosql_injection(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _ts_nosql_injection(shard, method_supplies)


@SHIELD_BLOCKING
def ts_nosql_injection_ternary(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_nosql_injection_ternary(shard, method_supplies)


@SHIELD_BLOCKING
def js_nosql_injection_ternary(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_nosql_injection_ternary(shard, method_supplies)
