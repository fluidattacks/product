from lib_sast.root.f156.python import (
    python_insecure_redirect as _python_insecure_redirect,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def python_insecure_redirect(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_insecure_redirect(shard, method_supplies)
