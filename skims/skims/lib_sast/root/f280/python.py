from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils.graph import (
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def is_danger_member(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    danger_set1 = {
        "httpresponse",
    }
    danger_set2 = {
        "sessionid",
        "userparams",
    }
    parent_id = pred_ast(graph, n_id)[0]
    if (  # noqa: SIM103
        graph.nodes[n_id]["member"] == "set_cookie"
        and get_node_evaluation_results(method, graph, n_id, danger_set1)
        and graph.nodes[parent_id]["label_type"] == "MethodInvocation"
        and (al_id := graph.nodes[parent_id].get("arguments_id"))
        and get_node_evaluation_results(method, graph, al_id, danger_set2)
    ):
        return True

    return False


def is_danger_access(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    n_attrs = graph.nodes[n_id]
    dng_set1 = {
        "httpresponse",
    }
    dng_set2 = {
        "userparams",
    }
    parent_id = pred_ast(graph, n_id)[0]
    if graph.nodes[parent_id]["label_type"] != "Assignment":
        return False

    if (  # noqa: SIM103
        (val_str := graph.nodes[n_attrs["arguments_id"]].get("value"))
        and val_str == "Set-Cookie"
        and get_node_evaluation_results(method, graph, n_attrs["expression_id"], dng_set1)
        and (val_id := graph.nodes[parent_id].get("value_id"))
        and get_node_evaluation_results(method, graph, val_id, dng_set2)
    ):
        return True

    return False


def python_xml_parser(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    method = MethodsEnum.PYTHON_SESSION_FIXATION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id]["label_type"] == "MemberAccess"
                and is_danger_member(graph, n_id, method)
            ) or (
                graph.nodes[n_id]["label_type"] == "ElementAccess"
                and is_danger_access(graph, n_id, method)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
