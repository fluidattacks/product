from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    is_any_library_imported,
    is_node_definition_unsafe,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model import (
    core,
)


def custom_has_string_definition(graph: Graph, n_id: NId) -> bool:
    return (
        graph.nodes[n_id].get("label_type") == "Literal"
        and graph.nodes[n_id].get("value_type", "") == "string"
    )


def is_hardcoded_password_auth(graph: Graph, n_id: NId) -> bool:
    if (
        custom_has_string_definition(graph, n_id)
        or graph.nodes[n_id].get("label_type") == "ArrayInitializer"
        or graph.nodes[n_id].get("value_type", "") == "null"
    ):
        return True

    return (
        graph.nodes[n_id].get("label_type") == "MethodInvocation"
        and graph.nodes[n_id].get("expression", "") == "toCharArray"
        and (obj_id := graph.nodes[n_id].get("object_id"))
        and is_node_definition_unsafe(graph, obj_id, custom_has_string_definition)
    )


def java_password_authentication_hardcoded_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> core.MethodExecutionResult:
    method = MethodsEnum.JAVA_PASSWORD_AUTHENTICATION_HARDCODED_SECRET

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported(graph, {"java.net.PasswordAuthentication"}):
            return

        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("name", "") == "PasswordAuthentication"
                and (second_arg := get_n_arg(graph, n_id, 1))
                and is_node_definition_unsafe(graph, second_arg, is_hardcoded_password_auth)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
