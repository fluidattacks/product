from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    is_node_definition_unsafe,
)
from lib_sast.root.utilities.java import (
    is_any_library_imported_prefix,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model import (
    core,
)


def has_non_empty_string_definition(graph: Graph, n_id: NId) -> bool:
    return (
        graph.nodes[n_id].get("label_type") == "Literal"
        and graph.nodes[n_id].get("value_type", "") == "string"
        and graph.nodes[n_id].get("value", "") != ""
    )


def has_hardcoded_password_in_args(graph: Graph, args_n_id: NId) -> bool:
    arg_values = [
        is_node_definition_unsafe(graph, n_id, has_non_empty_string_definition)
        for n_id in adj_ast(graph, args_n_id)
        if graph.nodes[n_id].get("label_type") != "Comment"
    ]

    return bool(arg_values and arg_values[-1])


def is_jedis_instance(graph: Graph, n_id: NId) -> bool:
    return (
        graph.nodes[n_id].get("label_type") == "ObjectCreation"
        and graph.nodes[n_id].get("name", "") == "Jedis"
    )


def java_jedis_hardcoded_secret_auth(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> core.MethodExecutionResult:
    method = MethodsEnum.JAVA_JEDIS_HARDCODED_SECRET_AUTH

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported_prefix(graph, ("redis.clients.jedis",)):
            return

        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("expression", "") == "auth"
                and (args_id := graph.nodes[n_id].get("arguments_id"))
                and has_hardcoded_password_in_args(graph, args_id)
                and (obj_id := graph.nodes[n_id].get("object_id"))
                and is_node_definition_unsafe(graph, obj_id, is_jedis_instance)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
