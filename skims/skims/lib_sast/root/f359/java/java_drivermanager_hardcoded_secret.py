from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    get_parent_scope_n_id,
    has_string_definition,
)
from lib_sast.root.utilities.java import (
    get_variable_name,
    is_any_library_imported_prefix,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model import (
    core,
)


def hc_set_password(graph: Graph, n_id: NId, obj_name: str) -> bool:
    nodes = graph.nodes
    return bool(
        (obj_n_id := nodes[n_id].get("object_id"))
        and (nodes[obj_n_id].get("symbol") == obj_name)
        and (f_arg := get_n_arg(graph, n_id, 0))
        and has_string_definition(graph, f_arg)  # noqa: COM812
    )


def get_hc_secret_in_set_password_n_ids(graph: Graph, n_id: NId) -> Iterator[NId]:
    if (var_name := get_variable_name(graph, n_id)) and (
        p_n_id := get_parent_scope_n_id(graph, n_id)
    ):
        yield from [
            node_id
            for node_id in adj_ast(
                graph,
                p_n_id,
                -1,
                label_type="MethodInvocation",
                expression="setPassword",
            )
            if hc_set_password(graph, node_id, var_name)
        ]


def java_drivermanager_hc_secret_object(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> core.MethodExecutionResult:
    method = MethodsEnum.JAVA_DRIVERMANAGER_HARDCODED_SECRET

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported_prefix(graph, ("java.sql",)):
            return

        for n_id in method_supplies.selected_nodes:
            if graph.nodes[n_id].get("name") != "DriverManagerDataSource":
                continue
            if (third_arg := get_n_arg(graph, n_id, 2)) and has_string_definition(
                graph,
                third_arg,
            ):
                yield n_id
            yield from get_hc_secret_in_set_password_n_ids(graph, n_id)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def java_drivermanager_hc_secret_method(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> core.MethodExecutionResult:
    method = MethodsEnum.JAVA_DRIVERMANAGER_HARDCODED_SECRET

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported_prefix(graph, ("java.sql",)):
            return

        for n_id in method_supplies.selected_nodes:
            if (
                (graph.nodes[n_id].get("expression") == "getConnection")
                and (third_arg := get_n_arg(graph, n_id, 2))
                and (has_string_definition(graph, third_arg))
                and (obj_id := graph.nodes[n_id].get("object_id"))
                and graph.nodes[obj_id].get("symbol", "") == "DriverManager"
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
