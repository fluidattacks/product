import re
from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    has_string_definition,
)
from lib_sast.root.utilities.java import (
    is_any_library_imported_prefix,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model import (
    core,
)


def concatenate_string(graph: Graph, n_id: NId) -> str:
    concat_string = ""
    for child in sorted((n_id, *adj_ast(graph, n_id, -1)), key=int):
        if graph.nodes[child].get("label_type") in {"Literal", "SymbolLookup"} and (
            str_definition := has_string_definition(graph, child)
        ):
            concat_string += str_definition

    return concat_string


def has_hardcoded_credential(arg_value: str) -> bool:
    pattern = r"^redis://([^:]*):([^@]*)@"

    user = password = None

    if match := re.search(pattern, arg_value):
        user = match.group(1)
        password = match.group(2)

    return bool(any(value and len(value) > 0 for value in {user, password}))


def java_jedis_hardcoded_credentials(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> core.MethodExecutionResult:
    method = MethodsEnum.JAVA_JEDIS_HARDCODED_CREDENTIALS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported_prefix(graph, ("redis",)):
            return

        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("name", "") == "Jedis"
                and (f_arg := get_n_arg(graph, n_id, 0))
                and (arg_val := concatenate_string(graph, f_arg))
                and has_hardcoded_credential(arg_val)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
