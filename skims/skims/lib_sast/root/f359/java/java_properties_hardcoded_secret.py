from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_args_n_ids,
    get_parent_scope_n_id,
    has_string_definition,
    is_any_library_imported,
)
from lib_sast.root.utilities.java import (
    get_variable_name,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model import (
    core,
)


def hc_password(graph: Graph, n_id: NId, obj_name: str) -> bool:
    nodes = graph.nodes
    return bool(
        (obj_n_id := nodes[n_id].get("object_id"))
        and (nodes[obj_n_id].get("symbol") == obj_name)
        and (args_n_ids := get_args_n_ids(graph, n_id))
        and len(args_n_ids) == 2
        and (nodes[args_n_ids[0]].get("symbol") == "Context.SECURITY_CREDENTIALS")
        and has_string_definition(graph, args_n_ids[1])  # noqa: COM812
    )


def java_properties_hardcoded_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> core.MethodExecutionResult:
    method = MethodsEnum.JAVA_PROPERTIES_HARDCODED_SECRET

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported(graph, {"java.util.Properties"}):
            return

        for n_id in method_supplies.selected_nodes:
            if (
                (graph.nodes[n_id].get("name", "") == "Properties")
                and (var_name := get_variable_name(graph, n_id))
                and (p_n_id := get_parent_scope_n_id(graph, n_id))
            ):
                yield from [
                    node_id
                    for node_id in adj_ast(
                        graph,
                        p_n_id,
                        -1,
                        label_type="MethodInvocation",
                        expression="put",
                    )
                    if hc_password(graph, node_id, var_name)
                ]

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
