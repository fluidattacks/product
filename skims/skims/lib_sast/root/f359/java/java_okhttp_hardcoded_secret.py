from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    has_string_definition,
)
from lib_sast.root.utilities.java import (
    is_any_library_imported_prefix,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model import (
    core,
)


def java_okhttp_hardcoded_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> core.MethodExecutionResult:
    method = MethodsEnum.JAVA_OKHTTP_HARDCODED_SECRET

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        if not is_any_library_imported_prefix(
            graph,
            ("okhttp3",),
        ):
            return

        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get(
                    "expression",
                    "",
                )
                == "basic"
                and (obj_id := graph.nodes[n_id].get("object_id"))
                and graph.nodes[obj_id].get("symbol", "") == "Credentials"
                and (second_arg := get_n_arg(graph, n_id, 1))
                and has_string_definition(graph, second_arg)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
