from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    has_string_definition,
    is_node_definition_unsafe,
)
from lib_sast.root.utilities.java import (
    is_any_library_imported_prefix,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model import (
    core,
)


def is_jdopersistencemanagerfactory_instance(graph: Graph, n_id: NId) -> bool:
    return (
        graph.nodes[n_id].get("label_type") == "ObjectCreation"
        and graph.nodes[n_id].get("name", "") == "JDOPersistenceManagerFactory"
    )


def java_datanucleus_hardcoded_connect_password(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> core.MethodExecutionResult:
    method = MethodsEnum.JAVA_DATANUCLEUS_HARDCODED_CONNECT_PASSWORD

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported_prefix(graph, ("org.datanucleus",)):
            return

        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get(
                    "expression",
                    "",
                )
                == "setConnectionPassword"
                and (obj_id := graph.nodes[n_id].get("object_id"))
                and is_node_definition_unsafe(
                    graph,
                    obj_id,
                    is_jdopersistencemanagerfactory_instance,
                )
                and (first_arg := get_n_arg(graph, n_id, 0))
                and has_string_definition(graph, first_arg)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
