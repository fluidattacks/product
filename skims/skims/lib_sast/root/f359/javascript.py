from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
    is_test_file,
)
from lib_sast.root.f359.common import (
    hardcoded_credentials_in_test,
    hardcoded_password,
    hardcoded_session_secret_in_session_method,
    is_jwt_and_use_member,
    second_arg_is_hardcoded,
)
from lib_sast.root.utilities.javascript import (
    get_default_alias,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model import (
    core,
)


def js_hardcoded_password(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> core.MethodExecutionResult:
    method = MethodsEnum.JS_HARDCODED_PASSWORD

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        yield from hardcoded_password(graph, method_supplies)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def js_hardcoded_credentials_in_test(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> core.MethodExecutionResult:
    method = MethodsEnum.JS_HARDCODED_CREDENTIALS_IN_TEST

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        yield from hardcoded_credentials_in_test(graph, method_supplies, method)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def js_hardcoded_jwt_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> core.MethodExecutionResult:
    method = MethodsEnum.JS_HARDCODED_JWT_SECRET

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if is_test_file(shard.path):
            return
        for n_id in method_supplies.selected_nodes:
            if (
                (alias_jsonwebtoken := get_default_alias(graph, "jsonwebtoken"))
                and (expression := graph.nodes[n_id].get("expression"))
                and is_jwt_and_use_member(expression, alias_jsonwebtoken)
                and second_arg_is_hardcoded(graph, n_id)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def js_expressjs_hardcoded_sess_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> core.MethodExecutionResult:
    method = MethodsEnum.JS_EXPRESSJS_HARDCODED_SESS_SECRET

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                (alias_express_session := get_default_alias(graph, "express-session"))
                and (arg_id := graph.nodes[n_id].get("arguments_id"))
                and hardcoded_session_secret_in_session_method(graph, arg_id, alias_express_session)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
