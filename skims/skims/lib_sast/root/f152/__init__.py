from lib_sast.root.f152.javascript import (
    js_insecure_header_xframe_options as _js_insecure_header_xframe_options,
)
from lib_sast.root.f152.typescript import (
    ts_insecure_header_xframe_options as _ts_insecure_header_xframe_options,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def js_insecure_header_xframe_options(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_insecure_header_xframe_options(shard, method_supplies)


@SHIELD_BLOCKING
def ts_insecure_header_xframe_options(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_insecure_header_xframe_options(shard, method_supplies)
