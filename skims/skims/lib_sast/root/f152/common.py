from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)


def insecure_http_headers(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    return graph.nodes[n_id].get("name") == "HttpHeaders" and get_node_evaluation_results(
        method,
        graph,
        n_id,
        set(),
    )
