import re

from lib_sast.root.utilities.common import (
    get_n_arg,
    has_string_definition,
)
from lib_sast.sast_model import (
    Graph,
    NId,
)

RSA_KEY_PATTERN = (
    r"-----BEGIN RSA PRIVATE KEY-----\\r?\\n?\r?\n?"
    r"([a-zA-Z0-9+/=\s]+)\\r?\\n?\r?\n?-----END RSA PRIVATE KEY-----"
)


def exposed_private_key(graph: Graph, n_id: NId) -> bool:
    if (literal_value := graph.nodes[n_id].get("value")) and bool(  # noqa: SIM103
        re.match(RSA_KEY_PATTERN, literal_value),
    ):
        return True
    return False


def hardcoded_key(graph: Graph, n_id: NId) -> bool:
    # RSA_KEY_PATTERN negation aims to avoid duplicated reports on F385
    return bool(
        (key_arg_id := get_n_arg(graph, n_id, 1))
        and (literal_value := has_string_definition(graph, key_arg_id))
        and not bool(re.match(RSA_KEY_PATTERN, literal_value)),
    )
