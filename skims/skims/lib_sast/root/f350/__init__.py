from lib_sast.root.f350.java.java_declare_insecure_trust_manager import (
    declare_insecure_trust_manager_class as _decl_insecure_trust_manager_class,
)
from lib_sast.root.f350.java.java_declare_insecure_trust_manager import (
    declare_insecure_trust_manager_obj as _declare_insecure_trust_manager_obj,
)
from lib_sast.root.f350.java.java_hostname_verification_off import (
    hostname_verification_off as _java_hostname_verification_off,
)
from lib_sast.root.f350.java.java_ignore_ssl_certificate_errors import (
    java_ignore_ssl_certificate_errors as _java_ignore_ssl_certificate_errors,
)
from lib_sast.root.f350.java.java_use_insecure_trust_manager import (
    use_insecure_trust_manager as _java_use_insecure_trust_manager,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def java_hostname_verification_off(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_hostname_verification_off(shard, method_supplies)


@SHIELD_BLOCKING
def java_use_insecure_trust_manager(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_use_insecure_trust_manager(shard, method_supplies)


@SHIELD_BLOCKING
def java_declare_insecure_trust_manager_obj(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _declare_insecure_trust_manager_obj(shard, method_supplies)


@SHIELD_BLOCKING
def java_ignore_ssl_certificate_errors(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_ignore_ssl_certificate_errors(shard, method_supplies)


@SHIELD_BLOCKING
def java_declare_insecure_trust_manager_class(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _decl_insecure_trust_manager_class(shard, method_supplies)
