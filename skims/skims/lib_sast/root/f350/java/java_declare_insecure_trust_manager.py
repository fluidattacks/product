from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_d,
    match_ast_group_d,
)
from model.core import (
    MethodExecutionResult,
)

TRUST_MANAGER_INTERFACES = {
    "X509TrustManager",
    "X509ExtendedTrustManager",
}


def _has_void_logic(graph: Graph, n_id: NId) -> bool:
    non_logical_nodes = {"Return", "Comment", "Literal"}

    if block_n_id := graph.nodes[n_id].get("block_id"):
        for child_id in adj_ast(graph, block_n_id, -1):
            if graph.nodes[child_id].get("label_type") not in non_logical_nodes:
                return False
    return True


def _overrides_trust_verification(graph: Graph, n_id: NId) -> bool:
    nodes = graph.nodes

    trust_verification_methods = {
        "checkClientTrusted",
        "checkServerTrusted",
    }

    return bool(
        nodes[n_id].get("name") in trust_verification_methods
        and (modifiers_n_id := nodes[n_id].get("modifiers_id"))
        and (annotation_n_id := match_ast_d(graph, modifiers_n_id, "Annotation"))
        and nodes[annotation_n_id].get("name") == "Override"
        and _has_void_logic(graph, n_id),
    )


def _overrides_accepted_issuers(graph: Graph, n_id: NId) -> bool:
    if graph.nodes[n_id].get("name", "") == "getAcceptedIssuers" and (
        block_id := graph.nodes[n_id].get("block_id")
    ):
        for child_id in adj_ast(graph, block_id, -1):
            if graph.nodes[child_id]["label_type"] == "Comment":
                continue

            return bool(
                graph.nodes[child_id]["label_type"] == "Return"
                and (val_id := graph.nodes[child_id].get("value_id"))
                and graph.nodes[val_id].get("value", "") == "null",
            )

    return False


def _methods_overriding_trust_verification_or_issuers(graph: Graph, n_id: NId) -> list[NId]:
    return [
        method_id
        for method_id in match_ast_group_d(graph, n_id, "MethodDeclaration", 2)
        if _overrides_trust_verification(graph, method_id)
        or _overrides_accepted_issuers(graph, method_id)
    ]


def declare_insecure_trust_manager_class(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_DECLARE_INSECURE_TRUST_MANAGER

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (inherited_class := graph.nodes[n_id].get("inherited_class")) and any(
                class_item in TRUST_MANAGER_INTERFACES for class_item in inherited_class.split(",")
            ):
                yield from _methods_overriding_trust_verification_or_issuers(graph, n_id)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def declare_insecure_trust_manager_obj(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_DECLARE_INSECURE_TRUST_MANAGER

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if graph.nodes[n_id].get("name", "") in TRUST_MANAGER_INTERFACES:
                yield from _methods_overriding_trust_verification_or_issuers(graph, n_id)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
