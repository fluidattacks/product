from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from model.core import (
    MethodExecutionResult,
)


def _symbol_lookup_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    if (args.graph.nodes[args.n_id]["symbol"].lower() == "noophostnameverifier.instance") or (
        "ALLOW_ALL_HOSTNAME_VERIFIER".lower() in args.graph.nodes[args.n_id]["symbol"].lower()
    ):
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "symbol_lookup": _symbol_lookup_evaluator,
}


def hostname_verification_off(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_HOSTNAME_VERIFICATION_OFF

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id]["expression"] in ("setSSLHostnameVerifier", "setHostnameVerifier")
                and (args_id := graph.nodes[n_id].get("arguments_id"))
                and get_node_evaluation_results(
                    method,
                    graph,
                    args_id,
                    set(),
                    method_evaluators=METHOD_EVALUATORS,
                )
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
