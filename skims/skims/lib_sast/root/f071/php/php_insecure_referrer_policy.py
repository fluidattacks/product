import re
from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    match_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _is_dangerous_referrer(graph: Graph, n_id: NId) -> bool:
    regex_insecure_config = re.compile(
        r"Referrer-Policy:\s*(?:no-referrer-when-downgrade|origin|unsafe-url)",
    )

    if (  # noqa: SIM103
        graph.nodes[n_id]["label_type"] == "Literal"
        and (value := graph.nodes[n_id].get("value"))
        and regex_insecure_config.search(value)
    ):
        return True
    return False


def _eval_danger_header(graph: Graph, n_id: NId) -> bool:
    if _is_dangerous_referrer(graph, n_id):
        return True
    if graph.nodes[n_id]["label_type"] == "SymbolLookup":
        symbol = graph.nodes[n_id].get("symbol")
        for path in get_backward_paths(graph, n_id):
            if (
                (def_id := definition_search(graph, path, symbol))
                and (val_id := graph.nodes[def_id].get("value_id"))
                and _is_dangerous_referrer(graph, val_id)
            ):
                return True
    return False


def php_insecure_referrer_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PHP_INSECURE_REFERRER_POLICY

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("expression") == "header"
                and (args_id := graph.nodes[n_id].get("arguments_id"))
                and (first_arg := match_ast(graph, args_id).get("__0__"))
                and _eval_danger_header(graph, first_arg)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
