from lib_sast.root.f157.terraform import (
    tfm_aws_acl_broad_network_access as _tfm_aws_acl_broad_network_access,
)
from lib_sast.root.f157.terraform import (
    tfm_azure_kv_danger_bypass as _tfm_azure_kv_danger_bypass,
)
from lib_sast.root.f157.terraform import (
    tfm_azure_kv_default_network_access as _tfm_kv_default_network_access,
)
from lib_sast.root.f157.terraform import (
    tfm_azure_nsg_unrestricted_netbios_access as _tfm_azure_nsg_unrestricted_netbios_access,
)
from lib_sast.root.f157.terraform import (
    tfm_azure_sa_default_network_access as _tfm_sa_default_network_access,
)
from lib_sast.root.f157.terraform import (
    tfm_azure_unrestricted_access_network_segments as _tfm_access_net_segment,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def tfm_aws_acl_broad_network_access(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_aws_acl_broad_network_access(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_azure_kv_danger_bypass(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_azure_kv_danger_bypass(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_azure_kv_default_network_access(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_kv_default_network_access(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_azure_sa_default_network_access(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_sa_default_network_access(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_azure_unrestricted_access_network_segments(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_access_net_segment(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_azure_nsg_unrestricted_netbios_access(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_azure_nsg_unrestricted_netbios_access(shard, method_supplies)
