from lib_sast.root.f097.javascript import (
    has_reverse_tabnabbing as _js_has_reverse_tabnabbing,
)
from lib_sast.root.f097.typescript import (
    has_reverse_tabnabbing as _ts_has_reverse_tabnabbing,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def js_has_reverse_tabnabbing(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_has_reverse_tabnabbing(shard, method_supplies)


@SHIELD_BLOCKING
def ts_has_reverse_tabnabbing(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_has_reverse_tabnabbing(shard, method_supplies)
