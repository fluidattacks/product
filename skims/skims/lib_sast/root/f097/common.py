import re

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.symbolic_eval.evaluate import (
    evaluate,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_d,
)


def node_is_vulnerable(graph: Graph, params: tuple[NId, ...]) -> bool:
    if (url := graph.nodes[params[0]].get("value")) and not str(url).startswith(
        ("https://", "http://"),
    ):
        return False

    if len(params) < 3:
        return True
    matcher = re.compile(r"(?=.*noopener)(?=.*noreferrer)")
    param_w_features = params[2]
    method = MethodsEnum.JAVASCRIPT_HAS_REVERSE_TABNABBING
    for path in get_backward_paths(graph, param_w_features):
        evaluation = evaluate(method, graph, path, param_w_features)
        if (
            evaluation
            and evaluation.danger
            and evaluation.triggers != set()
            and not matcher.match(next(iter(evaluation.triggers)))
        ):
            return True
    return False


def get_vulns_n_ids(graph: Graph, n_id: NId) -> bool:
    n_attrs = match_ast_d(graph, n_id, "ArgumentList")
    if n_attrs and node_is_vulnerable(  # noqa: SIM103
        graph,
        adj_ast(graph, n_attrs),
    ):
        return True
    return False
