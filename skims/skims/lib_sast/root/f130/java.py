from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    get_parent_scope_n_id,
    is_node_definition_unsafe,
)
from lib_sast.root.utilities.java import (
    is_any_library_imported_prefix,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils import (
    graph as g,
)
from model.core import (
    MethodExecutionResult,
)


def has_set_secure_attr(
    method: MethodsEnum,
    graph: Graph,
    n_id: NId,
    cookie_name: str,
    method_supplies: MethodSupplies,
) -> bool:
    for path in get_backward_paths(graph, n_id):
        for node in path:
            n_attrs = graph.nodes[node]
            if (
                n_attrs.get("label_type") == "MethodInvocation"
                and n_attrs.get("expression") == "setSecure"
                and n_attrs.get("label_type") == "MethodInvocation"
                and n_attrs.get("arguments_id")
                and graph.nodes.get(n_attrs.get("object_id")).get("symbol") == cookie_name
            ):
                return get_node_evaluation_results(
                    method,
                    graph,
                    n_attrs["arguments_id"],
                    set(),
                    graph_db=method_supplies.graph_db,
                )
    return True


def analyze_insecure_cookie(
    method: MethodsEnum,
    graph: Graph,
    obj_id: NId,
    al_id: NId,
    method_supplies: MethodSupplies,
) -> bool:
    args_ids = g.adj_ast(graph, al_id)
    if (
        len(args_ids) == 1
        and get_node_evaluation_results(
            method,
            graph,
            obj_id,
            {"user_response"},
            graph_db=method_supplies.graph_db,
        )
        and (cookie_name := graph.nodes[args_ids[0]].get("symbol"))
        and get_node_evaluation_results(
            method,
            graph,
            args_ids[0],
            {"isCookieObject"},
            graph_db=method_supplies.graph_db,
        )
    ):
        return has_set_secure_attr(method, graph, args_ids[0], cookie_name, method_supplies)
    return False


def java_cookie_set_secure(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_SECURE_COOKIE
    danger_methods = {"addCookie"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            n_attrs = graph.nodes[node]
            expr = n_attrs["expression"].split(".")
            if (
                expr[-1] in danger_methods
                and (obj_id := n_attrs.get("object_id"))
                and (al_id := graph.nodes[node].get("arguments_id"))
                and analyze_insecure_cookie(method, graph, obj_id, al_id, method_supplies)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def is_true_boolean(graph: Graph, n_id: NId) -> bool:
    return (
        graph.nodes[n_id].get("value_type", "") == "bool"
        and graph.nodes[n_id].get("value", "") == "true"
    )


def has_secure_method_in_following_lines(
    graph: Graph,
    var_n_id: NId,
    secure_method_name: str,
) -> bool:
    nodes = graph.nodes

    return bool(
        (p_n_id := get_parent_scope_n_id(graph, var_n_id))
        and any(
            (obj_id := nodes[n_id].get("object_id"))
            and nodes[obj_id].get("symbol", "") == nodes[var_n_id].get("variable")
            and (f_arg := get_n_arg(graph, n_id, 0))
            and is_node_definition_unsafe(graph, f_arg, is_true_boolean)
            for n_id in g.adj_ast(
                graph,
                p_n_id,
                -1,
                label_type="MethodInvocation",
                expression=secure_method_name,
            )
        )  # noqa: COM812
    )


def has_secure_method(graph: Graph, n_id: NId, secure_method_name: str) -> bool:
    for parent_id in g.pred_ast(graph, n_id, -1):
        if (
            graph.nodes[parent_id].get("label_type") == "MethodInvocation"
            and graph.nodes[parent_id].get("expression", "") == secure_method_name
        ):
            return bool(
                (first_arg := get_n_arg(graph, parent_id, 0))
                and is_node_definition_unsafe(graph, first_arg, is_true_boolean),
            )

        if graph.nodes[parent_id].get("label_type") == "VariableDeclaration":
            return has_secure_method_in_following_lines(graph, parent_id, secure_method_name)

    return False


def java_cookie_missing_secure(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_COOKIE_MISSING_SECURE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported_prefix(graph, ("io.micronaut.http",)):
            return

        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("name", "")
                in {
                    "NettyCookie",
                    "SimpleCookie",
                }
                or (
                    graph.nodes[n_id].get("expression", "") == "of"
                    and (obj_id := graph.nodes[n_id].get("object_id"))
                    and graph.nodes[obj_id].get("symbol") == "Cookie"
                )
            ) and not has_secure_method(graph, n_id, "secure"):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def java_cookie_serializer_secure_false(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_COOKIE_SERIALIZER_SECURE_FALSE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported_prefix(graph, ("org.springframework.session.web.http",)):
            return

        for n_id in method_supplies.selected_nodes:
            if graph.nodes[n_id].get(
                "name",
                "",
            ) == "DefaultCookieSerializer" and not has_secure_method(
                graph,
                n_id,
                "setUseSecureCookie",
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
