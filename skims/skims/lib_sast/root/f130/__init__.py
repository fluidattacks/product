from lib_sast.root.f130.java import (
    java_cookie_missing_secure as _java_cookie_missing_secure,
)
from lib_sast.root.f130.java import (
    java_cookie_serializer_secure_false as _java_cookie_serializer_secure_false,
)
from lib_sast.root.f130.java import (
    java_cookie_set_secure as _java_cookie_set_secure,
)
from lib_sast.root.f130.javascript import (
    js_express_cookie_secure as _js_express_cookie_secure,
)
from lib_sast.root.f130.kotlin import (
    kt_cookie_set_secure as _kt_cookie_set_secure,
)
from lib_sast.root.f130.python import (
    python_secure_cookie as _python_secure_cookie,
)
from lib_sast.root.f130.typescript import (
    ts_express_cookie_secure as _ts_express_cookie_secure,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def java_cookie_missing_secure(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_cookie_missing_secure(shard, method_supplies)


@SHIELD_BLOCKING
def java_cookie_serializer_secure_false(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_cookie_serializer_secure_false(shard, method_supplies)


@SHIELD_BLOCKING
def java_cookie_set_secure(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_cookie_set_secure(shard, method_supplies)


@SHIELD_BLOCKING
def kt_cookie_set_secure(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _kt_cookie_set_secure(shard, method_supplies)


@SHIELD_BLOCKING
def python_secure_cookie(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_secure_cookie(shard, method_supplies)


@SHIELD_BLOCKING
def js_express_cookie_secure(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_express_cookie_secure(shard, method_supplies)


@SHIELD_BLOCKING
def ts_express_cookie_secure(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_express_cookie_secure(shard, method_supplies)
