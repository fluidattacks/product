from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.java import (
    concatenate_name,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    search_pred_until_type,
)
from model.core import (
    MethodExecutionResult,
)


def is_method_used_on_main(graph: Graph, n_id: str) -> str | None:
    m_parents = search_pred_until_type(graph, n_id, {"MethodDeclaration"})
    if m_parents:
        return graph.nodes[m_parents[0]]["name"] == "main"
    return None


def uses_exit_method(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_USES_EXIT_METHOD
    exit_methods = {
        "System.exit",
        "Runtime.getRuntime.exit",
        "Runtime.getRuntime.halt",
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (
                graph.nodes[nid]["expression"] in {"exit", "halt"}
                and concatenate_name(graph, nid) in exit_methods
                and not is_method_used_on_main(graph, nid)
            ):
                yield nid

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
