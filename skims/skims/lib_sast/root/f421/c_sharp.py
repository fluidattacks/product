from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def is_insecure_key_size(graph: Graph, n_id: NId) -> bool:
    if (  # noqa: SIM103
        (graph.nodes[n_id].get("name") == "ECDiffieHellmanCng")
        and (first_arg := get_n_arg(graph, n_id, 0))
        and (graph.nodes[first_arg].get("value_type") == "number")
        and (int(graph.nodes[first_arg]["value"]) <= 256)
    ):
        return True
    return False


def c_sharp_insecure_elliptic_curve(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_INSECURE_ELLIPTIC_CURVE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            if is_insecure_key_size(graph, n_id):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
