from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from model.core import (
    MethodExecutionResult,
)


def is_weak_crypto(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    first_arg = get_n_arg(graph, n_id, 0)
    if first_arg is None:
        return False

    if graph.nodes[first_arg].get("value") == "md5":
        return True

    return get_node_evaluation_results(method, graph, first_arg, {"hash"}, danger_goal=False)


def php_insecure_elliptic_curve(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PHP_INSECURE_ELLIPTIC_CURVE

    hash_methods = {"setHash", "setMGFHash"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                (exp_id := graph.nodes[n_id].get("expression_id"))
                and graph.nodes[exp_id].get("member") in hash_methods
                and is_weak_crypto(graph, n_id, method)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
