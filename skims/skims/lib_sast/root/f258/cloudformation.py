from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    FALSE_OPTIONS,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_attribute,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _elb2_has_not_deletion_protection(graph: Graph, nid: NId) -> Iterator[NId]:
    prop, _, prop_id = get_attribute(graph, nid, "Properties")
    if not prop:
        return
    val_id = graph.nodes[prop_id]["value_id"]
    load_balancer, _, load_id = get_attribute(graph, val_id, "LoadBalancerAttributes")
    if not load_balancer:
        yield prop_id
    else:
        key_exist = False
        load_attrs = graph.nodes[load_id]["value_id"]
        for c_id in adj_ast(graph, load_attrs):
            key, key_val, _ = get_attribute(graph, c_id, "Key")
            if key and key_val == "deletion_protection.enabled":
                key_exist = True
                _, value, value_id = get_attribute(graph, c_id, "Value")
                if value in FALSE_OPTIONS:
                    yield value_id
        if not key_exist:
            yield load_id


def cfn_elb2_has_not_deletion_protection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_ELB2_HAS_NOT_DELETION_PROTECTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (resource := get_optional_attribute(graph, nid, "Type")) and resource[
                1
            ] == "AWS::ElasticLoadBalancingV2::LoadBalancer":
                yield from _elb2_has_not_deletion_protection(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
