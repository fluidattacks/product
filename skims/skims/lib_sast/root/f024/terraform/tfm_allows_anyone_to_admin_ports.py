from collections.abc import (
    Iterator,
)
from contextlib import (
    suppress,
)
from ipaddress import (
    AddressValueError,
    IPv4Network,
    IPv6Network,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.f024.constants import (
    ADMIN_PORTS,
    UNRESTRICTED_IPV4,
    UNRESTRICTED_IPV6,
)
from lib_sast.root.utilities.terraform import (
    get_optional_attribute,
    get_safe_port_range,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _aws_allows_anyone_to_admin_ports(graph: Graph, nid: NId) -> Iterator[NId]:
    type_attr = get_optional_attribute(graph, nid, "type")
    if type_attr and type_attr[1] != "ingress":
        return

    unrestricted_ip = False
    cidr_ip = get_optional_attribute(graph, nid, "cidr_blocks")
    cidr_ipv6 = get_optional_attribute(graph, nid, "ipv6_cidr_blocks")
    unrestricted_ip = False
    with suppress(AddressValueError, KeyError):
        unrestricted_ipv6 = bool(
            cidr_ipv6 and IPv6Network(cidr_ipv6[1], strict=False) == UNRESTRICTED_IPV6,
        )
        unrestricted_ip = (
            bool(cidr_ip and IPv4Network(cidr_ip[1], strict=False) == UNRESTRICTED_IPV4)
            or unrestricted_ipv6
        )

    if (
        unrestricted_ip
        and (from_port := get_optional_attribute(graph, nid, "from_port"))
        and (to_port := get_optional_attribute(graph, nid, "to_port"))
        and (port_range := get_safe_port_range(from_port[1], to_port[1]))
        and ADMIN_PORTS.intersection(port_range)
    ):
        yield from_port[2]
        yield to_port[2]


def tfm_aws_allows_anyone_to_admin_ports(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_ALLOWS_ANYONE_TO_ADMIN_PORTS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in {
                "aws_security_group",
                "aws_security_group_rule",
                "ingress",
            }:
                yield from _aws_allows_anyone_to_admin_ports(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
