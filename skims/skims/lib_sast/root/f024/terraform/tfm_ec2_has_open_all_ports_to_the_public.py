from collections.abc import (
    Iterator,
)
from contextlib import (
    suppress,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.f024.constants import (
    PUBLIC_CIDRS,
)
from lib_sast.root.utilities.terraform import (
    get_optional_attribute,
    is_cidr,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _ec2_has_open_all_ports_to_the_public(graph: Graph, nid: NId) -> Iterator[NId]:
    cidr_block = get_optional_attribute(graph, nid, "cidr_blocks") or get_optional_attribute(
        graph,
        nid,
        "ipv6_cidr_blocks",
    )
    if not cidr_block:
        return

    cidr_vals = set(cidr_block[1] if isinstance(cidr_block[1], list) else [cidr_block[1]])
    valid_cidrs = set(filter(is_cidr, cidr_vals))
    if (
        PUBLIC_CIDRS.intersection(valid_cidrs)
        and (from_port := get_optional_attribute(graph, nid, "from_port"))
        and (to_port := get_optional_attribute(graph, nid, "to_port"))
    ):
        has_all_ports_open = False
        with suppress(TypeError, ValueError):
            has_all_ports_open = int(to_port[1]) - int(from_port[1]) >= 65535

        if has_all_ports_open:
            yield from_port[2]


def tfm_ec2_has_open_all_ports_to_the_public(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_EC2_HAS_OPEN_ALL_PORTS_TO_THE_PUBLIC

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in {
                "aws_security_group",
                "aws_security_group_rule",
                "ingress",
                "egress",
            }:
                yield from _ec2_has_open_all_ports_to_the_public(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
