from collections.abc import (
    Iterator,
)
from contextlib import (
    suppress,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_argument,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _aux_ec2_has_unrestricted_ports(graph: Graph, nid: NId) -> Iterator[NId]:
    from_port = get_optional_attribute(graph, nid, "from_port")
    to_port = get_optional_attribute(graph, nid, "to_port")

    with suppress(TypeError, ValueError):
        if from_port and to_port and (int(from_port[1]) != int(to_port[1])):
            yield nid


def _ec2_has_unrestricted_ports(graph: Graph, nid: NId) -> Iterator[NId]:
    if graph.nodes[nid]["name"] == "aws_security_group":
        if ingress := get_argument(graph, nid, "ingress"):
            yield from _aux_ec2_has_unrestricted_ports(graph, ingress)
        if egress := get_argument(graph, nid, "egress"):
            yield from _aux_ec2_has_unrestricted_ports(graph, egress)
    elif graph.nodes[nid]["name"] == "aws_security_group_rule":
        yield from _aux_ec2_has_unrestricted_ports(graph, nid)


def tfm_ec2_has_unrestricted_ports(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_EC2_HAS_UNRESTRICTED_PORTS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in {
                "aws_security_group",
                "aws_security_group_rule",
            }:
                yield from _ec2_has_unrestricted_ports(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
