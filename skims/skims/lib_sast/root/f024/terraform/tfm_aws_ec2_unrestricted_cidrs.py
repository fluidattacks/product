from collections.abc import (
    Iterator,
)
from contextlib import (
    suppress,
)
from ipaddress import (
    AddressValueError,
    IPv4Network,
    IPv6Network,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.f024.constants import (
    UNRESTRICTED_IPV4,
    UNRESTRICTED_IPV6,
)
from lib_sast.root.utilities.terraform import (
    get_argument,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _ec2_unrestricted_cidrs(ip_val: str, ip_type: str, rule: str) -> bool:
    with suppress(AddressValueError, KeyError):
        if ip_type == "ipv4":
            ipv4_object = IPv4Network(ip_val, strict=False)
            if ipv4_object == UNRESTRICTED_IPV4 or (
                rule == "ingress" and ipv4_object.num_addresses > 1
            ):
                return True
        else:
            ipv6_object = IPv6Network(ip_val, strict=False)
            if ipv6_object == UNRESTRICTED_IPV6 or (
                rule == "ingress" and ipv6_object.num_addresses > 1
            ):
                return True
    return False


def _aux_ingress_unrestricted_cidrs(graph: Graph, nid: NId, rule: str) -> Iterator[NId]:
    ipv4 = get_optional_attribute(graph, nid, "cidr_blocks")
    if ipv4 and _ec2_unrestricted_cidrs(ipv4[1], "ipv4", rule):
        yield ipv4[2]
    ipv6 = get_optional_attribute(graph, nid, "ipv6_cidr_blocks")
    if ipv6 and _ec2_unrestricted_cidrs(ipv6[1], "ipv6", rule):
        yield ipv6[2]


def _aws_ec2_unrestricted_cidrs(graph: Graph, nid: NId) -> Iterator[NId]:
    if graph.nodes[nid]["name"] == "aws_security_group":
        if ingress := get_argument(graph, nid, "ingress"):
            yield from _aux_ingress_unrestricted_cidrs(graph, ingress, "ingress")
    elif graph.nodes[nid]["name"] == "aws_security_group_rule" and (
        rule_type := get_optional_attribute(graph, nid, "type")
    ):
        yield from _aux_ingress_unrestricted_cidrs(graph, nid, rule_type[1])


def tfm_aws_ec2_unrestricted_cidrs(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AWS_EC2_UNRESTRICTED_CIDRS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in {
                "aws_security_group",
                "aws_security_group_rule",
            }:
                yield from _aws_ec2_unrestricted_cidrs(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
