from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_argument,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _aux_aws_ec2_cfn_unrestricted_ip_protocols(graph: Graph, nid: NId) -> Iterator[NId]:
    protocol = get_optional_attribute(graph, nid, "protocol")
    if protocol and protocol[1] in ("-1", -1):
        yield protocol[2]


def _aws_ec2_cfn_unrestricted_ip_protocols(graph: Graph, nid: NId) -> Iterator[NId]:
    if graph.nodes[nid]["name"] == "aws_security_group":
        if ingress := get_argument(graph, nid, "ingress"):
            yield from _aux_aws_ec2_cfn_unrestricted_ip_protocols(graph, ingress)
        if egress := get_argument(graph, nid, "egress"):
            yield from _aux_aws_ec2_cfn_unrestricted_ip_protocols(graph, egress)
    elif graph.nodes[nid]["name"] == "aws_security_group_rule":
        yield from _aux_aws_ec2_cfn_unrestricted_ip_protocols(graph, nid)


def tfm_aws_ec2_cfn_unrestricted_ip_protocols(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AWS_EC2_CFN_UNRESTRICTED_IP_PROTOCOLS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in {
                "aws_security_group",
                "aws_security_group_rule",
            }:
                yield from _aws_ec2_cfn_unrestricted_ip_protocols(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
