from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_key_value,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    matching_nodes,
    pred_ast,
)
from lib_sast.utils.graph.text_nodes import (
    node_to_values,
)
from model.core import (
    MethodExecutionResult,
)


def _group_without_egress(graph: Graph, nid: NId) -> NId | None:
    prop = get_optional_attribute(graph, nid, "Properties")
    if not prop:
        return None

    val_id = graph.nodes[prop[2]]["value_id"]
    if get_optional_attribute(graph, val_id, "SecurityGroupEgress") or not get_optional_attribute(
        graph,
        val_id,
        "SecurityGroupIngress",
    ):
        return None

    group_ref = None
    if (parent := pred_ast(graph, nid)[0]) and graph.nodes[parent]["label_type"] == "Pair":
        group_ref = get_key_value(graph, parent)[0]

    if not group_ref:
        return prop[2]

    for obj_id in matching_nodes(graph, label_type="Object"):
        if (
            (resource := get_optional_attribute(graph, obj_id, "Type"))
            and resource[1] == "AWS::EC2::SecurityGroupEgress"
            and (prop_2 := get_optional_attribute(graph, obj_id, "Properties"))
        ):
            val_id = graph.nodes[prop_2[2]]["value_id"]
            if (
                group_id := get_optional_attribute(graph, val_id, "GroupId")
            ) and group_ref in node_to_values(graph, group_id[2]):
                return None
    return prop[2]


def cfn_groups_without_egress(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_GROUPS_WITHOUT_EGRESS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (
                (resource := get_optional_attribute(graph, nid, "Type"))
                and resource[1] == "AWS::EC2::SecurityGroup"
                and (report := _group_without_egress(graph, nid))
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
