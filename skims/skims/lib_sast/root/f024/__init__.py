from lib_sast.root.f024.cloudformation.cfn_allows_anyone_to_admin_ports import (
    cfn_allows_anyone_to_admin_ports as _cfn_allows_anyone_to_admin_ports,
)
from lib_sast.root.f024.cloudformation.cfn_ec2_has_open_all_ports_to_the_public import (
    cfn_ec2_has_open_all_ports_to_the_public as _cfn_ec2_has_open_all_ports,
)
from lib_sast.root.f024.cloudformation.cfn_ec2_has_security_groups_ip_ranges_in_rfc1918 import (
    cfn_ec2_has_security_groups_ip_ranges_in_rfc1918 as _cfn_ec2_sg_rfc1918,
)
from lib_sast.root.f024.cloudformation.cfn_ec2_has_unrestricted_dns_access import (
    cfn_ec2_has_unrestricted_dns_access as _cfn_ec2_unrestricted_dns_access,
)
from lib_sast.root.f024.cloudformation.cfn_ec2_has_unrestricted_ftp_access import (
    cfn_ec2_has_unrestricted_ftp_access as _cfn_ec2_unrestricted_ftp_access,
)
from lib_sast.root.f024.cloudformation.cfn_ec2_has_unrestricted_ports import (
    cfn_ec2_has_unrestricted_ports as _cfn_ec2_has_unrestricted_ports,
)
from lib_sast.root.f024.cloudformation.cfn_groups_without_egress import (
    cfn_groups_without_egress as _cfn_groups_without_egress,
)
from lib_sast.root.f024.cloudformation.cfn_instances_without_profile import (
    cfn_instances_without_profile as _cfn_instances_without_profile,
)
from lib_sast.root.f024.cloudformation.cfn_unrestricted_cidrs import (
    cfn_unrestricted_cidrs as _cfn_unrestricted_cidrs,
)
from lib_sast.root.f024.cloudformation.cfn_unrestricted_ip_protocols import (
    cfn_unrestricted_ip_protocols as _cfn_unrestricted_ip_protocols,
)
from lib_sast.root.f024.terraform.tfm_allows_anyone_to_admin_ports import (
    tfm_aws_allows_anyone_to_admin_ports as _tfm_aws_allows_admin_ports,
)
from lib_sast.root.f024.terraform.tfm_aws_ec2_cfn_unrestricted_ip_protocols import (
    tfm_aws_ec2_cfn_unrestricted_ip_protocols as _tfm_ec2_unrestricted_ip,
)
from lib_sast.root.f024.terraform.tfm_aws_ec2_unrestricted_cidrs import (
    tfm_aws_ec2_unrestricted_cidrs as _tfm_aws_ec2_unrestricted_cidrs,
)
from lib_sast.root.f024.terraform.tfm_ec2_has_open_all_ports_to_the_public import (
    tfm_ec2_has_open_all_ports_to_the_public as _tfm_ec2_open_ports_to_public,
)
from lib_sast.root.f024.terraform.tfm_ec2_has_security_groups_ip_ranges_in_rfc1918 import (
    tfm_ec2_has_security_groups_ip_ranges_in_rfc1918 as _tfm_ec2_sg_rfc1918,
)
from lib_sast.root.f024.terraform.tfm_ec2_has_unrestricted_dns_access import (
    tfm_ec2_has_unrestricted_dns_access as _tfm_ec2_unrestricted_dns_access,
)
from lib_sast.root.f024.terraform.tfm_ec2_has_unrestricted_ftp_access import (
    tfm_ec2_has_unrestricted_ftp_access as _tfm_ec2_unrestricted_ftp_access,
)
from lib_sast.root.f024.terraform.tfm_ec2_has_unrestricted_ports import (
    tfm_ec2_has_unrestricted_ports as _tfm_ec2_has_unrestricted_ports,
)
from lib_sast.root.f024.terraform.tfm_ec2_instances_without_profile import (
    tfm_ec2_instances_without_profile as _tfm_ec2_instances_without_profile,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_allows_anyone_to_admin_ports(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_allows_anyone_to_admin_ports(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_ec2_has_open_all_ports_to_the_public(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_ec2_has_open_all_ports(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_ec2_has_security_groups_ip_ranges_in_rfc1918(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_ec2_sg_rfc1918(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_ec2_has_unrestricted_dns_access(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_ec2_unrestricted_dns_access(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_ec2_has_unrestricted_ftp_access(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_ec2_unrestricted_ftp_access(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_ec2_has_unrestricted_ports(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_ec2_has_unrestricted_ports(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_groups_without_egress(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_groups_without_egress(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_instances_without_profile(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_instances_without_profile(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_unrestricted_cidrs(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_unrestricted_cidrs(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_unrestricted_ip_protocols(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_unrestricted_ip_protocols(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_aws_allows_anyone_to_admin_ports(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_aws_allows_admin_ports(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_aws_ec2_cfn_unrestricted_ip_protocols(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_ec2_unrestricted_ip(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_aws_ec2_unrestricted_cidrs(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_aws_ec2_unrestricted_cidrs(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_ec2_has_open_all_ports_to_the_public(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_ec2_open_ports_to_public(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_ec2_has_security_groups_ip_ranges_in_rfc1918(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_ec2_sg_rfc1918(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_ec2_has_unrestricted_dns_access(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_ec2_unrestricted_dns_access(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_ec2_has_unrestricted_ftp_access(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_ec2_unrestricted_ftp_access(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_ec2_has_unrestricted_ports(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_ec2_has_unrestricted_ports(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_ec2_instances_without_profile(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_ec2_instances_without_profile(shard, method_supplies)
