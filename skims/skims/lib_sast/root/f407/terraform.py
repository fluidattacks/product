from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _aws_ebs_volumes_unencrypted(graph: Graph, nid: NId) -> Iterator[NId]:
    for block_id in adj_ast(graph, nid, label_type="Object"):
        if graph.nodes[block_id].get("name") in {
            "root_block_device",
            "ebs_block_device",
        }:
            encrypted = get_optional_attribute(graph, block_id, "encrypted")
            if not encrypted:
                yield block_id
            elif encrypted[1].lower() == "false":
                yield encrypted[2]


def tfm_aws_ebs_volumes_unencrypted(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AWS_EBS_VOLUMES_UNENCRYPTED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "aws_launch_configuration":
                yield from _aws_ebs_volumes_unencrypted(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
