from lib_sast.root.f109.cloudformation import (
    cfn_rds_is_not_inside_a_db_subnet_group as _cfn_rds_is_not_inside_subnet,
)
from lib_sast.root.f109.terraform import (
    tfm_rds_not_inside_subnet as _tfm_rds_not_inside_subnet,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_rds_is_not_inside_a_db_subnet_group(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_rds_is_not_inside_subnet(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_rds_not_inside_subnet(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_rds_not_inside_subnet(shard, method_supplies)
