from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    FALSE_OPTIONS,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_attribute,
    get_optional_attribute,
    yield_statements_from_policy,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)

AWS_ROLE = "AWS::IAM::Role"
NOT_AURORA_DB_ENGINES = {
    "custom-oracle-ee",
    "custom-oracle-ee-cdb",
    "custom-sqlserver-ee",
    "custom-sqlserver-se",
    "custom-sqlserver-web",
    "db2-ae",
    "db2-se",
    "mariadb",
    "mysql",
    "oracle-ee",
    "oracle-ee-cdb",
    "oracle-se2",
    "oracle-se2-cdb",
    "postgres",
    "sqlserver-ee",
    "sqlserver-se",
    "sqlserver-ex",
    "sqlserver-web",
}


def _iam_allow_not_element_trust_policy(
    graph: Graph,
    nid: NId,
    danger_element: str,
) -> Iterator[NId]:
    if (
        (props := get_optional_attribute(graph, nid, "Properties"))
        and (val_id := graph.nodes[props[2]]["value_id"])
        and (pol_doc := get_optional_attribute(graph, val_id, "AssumeRolePolicyDocument"))
    ):
        for stmt in yield_statements_from_policy(graph, graph.nodes[pol_doc[2]]["value_id"]):
            if (
                (effect := get_optional_attribute(graph, stmt, "Effect"))
                and effect[1] == "Allow"
                and (danger_el := get_optional_attribute(graph, stmt, danger_element))
            ):
                yield danger_el[2]


def _iam_allow_not_element_perms_policies(
    graph: Graph,
    nid: NId,
    danger_element: str,
) -> Iterator[NId]:
    if (
        (props := get_optional_attribute(graph, nid, "Properties"))
        and (val_id := graph.nodes[props[2]]["value_id"])
        and (pols := get_optional_attribute(graph, val_id, "Policies"))
    ):
        for policy in adj_ast(graph, graph.nodes[pols[2]]["value_id"]):
            if not (pol_doc := get_optional_attribute(graph, policy, "PolicyDocument")):
                continue

            for stmt in yield_statements_from_policy(graph, graph.nodes[pol_doc[2]]["value_id"]):
                if (
                    (effect := get_optional_attribute(graph, stmt, "Effect"))
                    and effect[1] == "Allow"
                    and (danger_el := get_optional_attribute(graph, stmt, danger_element))
                ):
                    yield danger_el[2]


def eks_has_endpoints_publicly_accessible(graph: Graph, nid: NId) -> Iterator[NId]:
    if (
        (props := get_optional_attribute(graph, nid, "Properties"))
        and (val_id := graph.nodes[props[2]]["value_id"])
        and (vpc := get_optional_attribute(graph, val_id, "ResourcesVpcConfig"))
    ):
        object_id = graph.nodes[vpc[2]]["value_id"]
        public = get_optional_attribute(graph, object_id, "EndpointPublicAccess")
        private = get_optional_attribute(graph, object_id, "EndpointPrivateAccess")
        if public and private and public[1].lower() == "true" and private[1].lower() == "false":
            yield public[2]


def rds_not_uses_iam_authentication(graph: Graph, nid: NId) -> NId | None:
    iam_db_auth = "EnableIAMDatabaseAuthentication"
    if (
        (props := get_optional_attribute(graph, nid, "Properties"))
        and (val_id := graph.nodes[props[2]]["value_id"])
        and (engine := get_optional_attribute(graph, val_id, "Engine"))
        and engine[1].lower() in NOT_AURORA_DB_ENGINES
    ):
        iam = get_optional_attribute(graph, val_id, iam_db_auth)
        if iam and iam[1].lower() in FALSE_OPTIONS:
            return iam[2]
        return props[2]
    return None


def redshift_has_public_clusters(graph: Graph, nid: NId) -> Iterator[NId]:
    if (
        (props := get_optional_attribute(graph, nid, "Properties"))
        and (val_id := graph.nodes[props[2]]["value_id"])
        and (public_attr := get_optional_attribute(graph, val_id, "PubliclyAccessible"))
        and public_attr[1].lower() == "true"
    ):
        yield public_attr[2]


def redshift_not_requires_ssl(graph: Graph, nid: NId) -> Iterator[NId]:
    prop, _, prop_id = get_attribute(graph, nid, "Properties")
    if prop:
        val_id = graph.nodes[prop_id]["value_id"]
        parameters, _, p_id = get_attribute(graph, val_id, "Parameters")
        if not parameters:
            yield prop_id
            return
        parameter_list = graph.nodes[p_id]["value_id"]
        p_exist = False
        for c_id in adj_ast(graph, parameter_list):
            p_name = get_attribute(graph, c_id, "ParameterName")
            p_value = get_attribute(graph, c_id, "ParameterValue")
            if p_name[1] == "require_ssl":
                p_exist = True
                if p_value[1] == "false":
                    yield p_name[2]
        if not p_exist:
            yield p_id


def elasticache_uses_default_port(graph: Graph, nid: NId) -> Iterator[NId]:
    if (props := get_optional_attribute(graph, nid, "Properties")) and (
        val_id := graph.nodes[props[2]]["value_id"]
    ):
        _, engine, _ = get_attribute(graph, val_id, "Engine")
        port = get_optional_attribute(graph, val_id, "Port")
        if port and engine == "memcached" and port[1] in {"11211", "6379"}:
            yield port[2]


def elasticache_is_transit_encryption_disabled(graph: Graph, nid: NId) -> Iterator[NId]:
    if (props := get_optional_attribute(graph, nid, "Properties")) and (
        val_id := graph.nodes[props[2]]["value_id"]
    ):
        _, engine, _ = get_attribute(graph, val_id, "Engine")
        encryption = get_optional_attribute(graph, val_id, "TransitEncryptionEnabled")
        if engine == "redis" and encryption and encryption[1].lower() != "true":
            yield encryption[2]


def sns_is_server_side_encryption_disabled(graph: Graph, nid: NId) -> Iterator[NId]:
    if (props := get_optional_attribute(graph, nid, "Properties")) and (
        val_id := graph.nodes[props[2]]["value_id"]
    ):
        encryption = get_optional_attribute(graph, val_id, "KmsMasterKeyId")
        if not encryption:
            yield props[2]


def sqs_is_encryption_disabled(graph: Graph, nid: NId) -> Iterator[NId]:
    if (
        (props := get_optional_attribute(graph, nid, "Properties"))
        and (val_id := graph.nodes[props[2]]["value_id"])
        and (sse_enc := get_optional_attribute(graph, val_id, "SqsManagedSseEnabled"))
        and sse_enc[1] in FALSE_OPTIONS
        and not get_optional_attribute(graph, val_id, "KmsMasterKeyId")
    ):
        yield sse_enc[2]


def cfn_sqs_is_encryption_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_SQS_IS_ENCRYPTION_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (resource := get_optional_attribute(graph, nid, "Type")) and resource[
                1
            ] == "AWS::SQS::Queue":
                yield from sqs_is_encryption_disabled(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def cfn_sns_is_server_side_encryption_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_SNS_IS_SERVER_SIDE_ENCRYPTION_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (resource := get_optional_attribute(graph, nid, "Type")) and resource[
                1
            ] == "AWS::SNS::Topic":
                yield from sns_is_server_side_encryption_disabled(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def cfn_elasticache_is_transit_encryption_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_ELASTICACHE_IS_TRANSIT_ENCRYPTION_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (resource := get_optional_attribute(graph, nid, "Type")) and resource[
                1
            ] == "AWS::ElastiCache::CacheCluster":
                yield from elasticache_is_transit_encryption_disabled(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def cfn_elasticache_uses_default_port(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_ELASTICACHE_USES_DEFAULT_PORT

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (resource := get_optional_attribute(graph, nid, "Type")) and resource[
                1
            ] == "AWS::ElastiCache::CacheCluster":
                yield from elasticache_uses_default_port(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def cfn_redshift_not_requires_ssl(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_REDSHIFT_NOT_REQUIRES_SSL

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (resource := get_optional_attribute(graph, nid, "Type")) and resource[
                1
            ] == "AWS::Redshift::ClusterParameterGroup":
                yield from redshift_not_requires_ssl(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def cfn_redshift_has_public_clusters(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_REDSHIFT_HAS_PUBLIC_CLUSTERS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (resource := get_optional_attribute(graph, nid, "Type")) and resource[
                1
            ] == "AWS::Redshift::Cluster":
                yield from redshift_has_public_clusters(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def cfn_rds_not_uses_iam_authentication(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_RDS_NOT_USES_IAM_AUTHENTICATION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (
                (resource := get_optional_attribute(graph, nid, "Type"))
                and resource[1] == "AWS::RDS::DBInstance"
                and (report_id := rds_not_uses_iam_authentication(graph, nid))
            ):
                yield report_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def cfn_eks_has_endpoints_publicly_accessible(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_EKS_HAS_ENDPOINTS_PUBLICLY_ACCESSIBLE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (resource := get_optional_attribute(graph, nid, "Type")) and resource[
                1
            ] == "AWS::EKS::Cluster":
                yield from eks_has_endpoints_publicly_accessible(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def cfn_iam_allow_not_resource_perms_policies(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_IAM_PERMISSIONS_POLICY_ALLOW_NOT_RESOURCE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (resource := get_optional_attribute(graph, nid, "Type")) and resource[1] == AWS_ROLE:
                yield from _iam_allow_not_element_perms_policies(graph, nid, "NotResource")

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def cfn_iam_allow_not_action_perms_policies(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_IAM_PERMISSIONS_POLICY_ALLOW_NOT_ACTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (resource := get_optional_attribute(graph, nid, "Type")) and resource[1] == AWS_ROLE:
                yield from _iam_allow_not_element_perms_policies(graph, nid, "NotAction")

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def cfn_iam_allow_not_principal_trust_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_IAM_TRUST_POLICY_ALLOW_NOT_PRINCIPAL

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (resource := get_optional_attribute(graph, nid, "Type")) and resource[1] == AWS_ROLE:
                yield from _iam_allow_not_element_trust_policy(graph, nid, "NotPrincipal")

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def cfn_iam_allow_not_actions_trust_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_IAM_TRUST_POLICY_NOT_ACTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (resource := get_optional_attribute(graph, nid, "Type")) and resource[1] == AWS_ROLE:
                yield from _iam_allow_not_element_trust_policy(graph, nid, "NotAction")

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def cfn_iam_is_policy_applying_to_users(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_IAM_PERMISSIONS_POLICY_APLLY_USERS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (
                (resource := get_optional_attribute(graph, nid, "Type"))
                and resource[1] in {"AWS::IAM::ManagedPolicy", "AWS::IAM::Policy"}
                and (props := get_optional_attribute(graph, nid, "Properties"))
                and (val_id := graph.nodes[props[2]]["value_id"])
                and get_optional_attribute(graph, val_id, "Users")
            ):
                yield props[2]

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
