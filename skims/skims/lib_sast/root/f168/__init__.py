from lib_sast.root.f168.java.java_keystore_hardcoded_passwords import (
    java_keystore_hardcoded_passwords_invocation as _java_keystore_hardcoded_passwords_invocation,
)
from lib_sast.root.f168.java.java_keystore_hardcoded_passwords import (
    java_keystore_hardcoded_passwords_object as _java_keystore_hardcoded_passwords_object,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def java_keystore_hardcoded_passwords_invocation(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_keystore_hardcoded_passwords_invocation(shard, method_supplies)


@SHIELD_BLOCKING
def java_keystore_hardcoded_passwords_object(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_keystore_hardcoded_passwords_object(shard, method_supplies)
