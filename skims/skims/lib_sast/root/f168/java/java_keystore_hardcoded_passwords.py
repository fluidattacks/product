from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    has_string_definition,
    is_node_definition_unsafe,
)
from lib_sast.root.utilities.java import (
    concatenate_name,
    is_any_library_imported_prefix,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model import (
    core,
)


def _is_hardcoded_password_in_argument(graph: Graph, n_id: NId) -> bool:
    return bool(
        ((str_val := has_string_definition(graph, n_id)) and str_val != "")
        or (
            graph.nodes[n_id].get("expression", "") == "toCharArray"
            and (object_id := graph.nodes[n_id].get("object_id"))
            and ((str_val := has_string_definition(graph, object_id)) and str_val != "")
        ),
    )


def _is_keystore_instance(graph: Graph, n_id: NId) -> bool:
    return concatenate_name(graph, n_id).startswith("KeyStore")


def java_keystore_hardcoded_passwords_object(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> core.MethodExecutionResult:
    method = MethodsEnum.JAVA_KEYSTORE_HARDCODED_PASSWORDS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported_prefix(graph, ("java.security.KeyStore",)):
            return

        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("name", "") == "KeyStore.PasswordProtection"
                and (first_arg := get_n_arg(graph, n_id, 0))
                and (
                    is_node_definition_unsafe(
                        graph,
                        first_arg,
                        _is_hardcoded_password_in_argument,
                    )
                )
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def java_keystore_hardcoded_passwords_invocation(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> core.MethodExecutionResult:
    method = MethodsEnum.JAVA_KEYSTORE_HARDCODED_PASSWORDS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported_prefix(graph, ("java.security.KeyStore",)):
            return

        for n_id in method_supplies.selected_nodes:
            if (
                (graph.nodes[n_id].get("expression", "") == "load")
                and (second_arg := get_n_arg(graph, n_id, 1))
                and is_node_definition_unsafe(graph, second_arg, _is_hardcoded_password_in_argument)
                and (object_id := graph.nodes[n_id].get("object_id"))
                and is_node_definition_unsafe(graph, object_id, _is_keystore_instance)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
