from lib_sast.root.f177.cloudformation import (
    cfn_ec2_use_default_security_group as _cfn_ec2_use_default_security_group,
)
from lib_sast.root.f177.terraform import (
    tfm_ec2_use_default_security_group as _tfm_ec2_use_default_security_group,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_ec2_use_default_security_group(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_ec2_use_default_security_group(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_ec2_use_default_security_group(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_ec2_use_default_security_group(shard, method_supplies)
