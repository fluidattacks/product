from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_argument,
    get_argument_iterator,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def aws_instance_defines_security_group(graph: Graph, n_id: NId) -> bool:
    group_definition_attrs = {
        "security_groups",
        "vpc_security_group_ids",
    }
    return bool(
        get_argument(graph, n_id, "network_interface")
        or any(get_optional_attribute(graph, n_id, attr) for attr in group_definition_attrs),
    )


def aws_launch_template_defines_security_group(graph: Graph, n_id: NId) -> bool:
    return bool(
        get_optional_attribute(graph, n_id, "vpc_security_group_ids")
        or any(
            get_optional_attribute(graph, network_n_id, "security_groups")
            for network_n_id in get_argument_iterator(graph, n_id, "network_interfaces")
        ),
    )


def tfm_ec2_use_default_security_group(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.EC2_USE_DEFAULT_SECURITY_GROUP

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        resource_mapper = {
            "aws_instance": aws_instance_defines_security_group,
            "aws_launch_template": aws_launch_template_defines_security_group,
        }
        for n_id in method_supplies.selected_nodes:
            if (
                name := graph.nodes[n_id].get("name", "")
            ) in resource_mapper and not resource_mapper[name](graph, n_id):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
