from lib_sast.root.f234.c_sharp import (
    c_sharp_stacktrace_disclosure as _c_sharp_stacktrace_disclosure,
)
from lib_sast.root.f234.c_sharp import (
    c_sharp_technical_info_leak as _c_sharp_technical_info_leak,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_stacktrace_disclosure(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_stacktrace_disclosure(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_technical_info_leak(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_technical_info_leak(shard, method_supplies)
