from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def python_unsafe_temp_file(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PYTHON_UNSAFE_TEMP_FILE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            expr = f"{n_attrs['expression']}.{n_attrs['member']}"
            if expr == "tempfile.mktemp":
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
