from lib_sast.root.f044.cloudformation.cnf_http_methods_enabled import (
    cfn_api_all_http_methods_enabled as _cfn_api_all_http_methods_enabled,
)
from lib_sast.root.f044.cloudformation.cnf_http_methods_enabled import (
    cfn_has_danger_https_methods_enabled as _cfn_has_danger_https_methods,
)
from lib_sast.root.f044.terraform.tfm_api_all_http_methods_enabled import (
    tfm_api_all_http_methods_enabled as _tfm_api_all_http_methods_enabled,
)
from lib_sast.root.f044.terraform.tfm_http_methods_enabled import (
    tfm_has_danger_https_methods_enabled as _tfm_has_danger_https_methods,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_api_all_http_methods_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_api_all_http_methods_enabled(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_has_danger_https_methods_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_has_danger_https_methods(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_api_all_http_methods_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_api_all_http_methods_enabled(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_has_danger_https_methods_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_has_danger_https_methods(shard, method_supplies)
