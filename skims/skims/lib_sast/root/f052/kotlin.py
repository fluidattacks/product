from collections.abc import (
    Iterator,
)
from contextlib import suppress

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    complete_attrs_on_set,
    get_n_arg,
    has_string_definition,
)
from lib_sast.root.utilities.kotlin import (
    check_method_origin,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.evaluate import (
    evaluate,
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast,
    matching_nodes,
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def kotlin_insecure_hash(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.KOTLIN_INSECURE_HASH
    danger_methods = complete_attrs_on_set(
        {
            "org.apache.commons.codec.digest.DigestUtils.getMd2Digest",
            "org.apache.commons.codec.digest.DigestUtils.getMd5Digest",
            "org.apache.commons.codec.digest.DigestUtils.getShaDigest",
            "org.apache.commons.codec.digest.DigestUtils.getSha1Digest",
            "org.apache.commons.codec.digest.DigestUtils.md2",
            "org.apache.commons.codec.digest.DigestUtils.md2Hex",
            "org.apache.commons.codec.digest.DigestUtils.md5",
            "org.apache.commons.codec.digest.DigestUtils.md5Hex",
            "org.apache.commons.codec.digest.DigestUtils.sha",
            "org.apache.commons.codec.digest.DigestUtils.shaHex",
            "org.apache.commons.codec.digest.DigestUtils.sha1",
            "org.apache.commons.codec.digest.DigestUtils.sha1Hex",
            "com.google.common.hash.Hashing.adler32",
            "com.google.common.hash.Hashing.crc32",
            "com.google.common.hash.Hashing.crc32c",
            "com.google.common.hash.Hashing.goodFastHash",
            "com.google.common.hash.Hashing.hmacMd5",
            "com.google.common.hash.Hashing.hmacSha1",
            "com.google.common.hash.Hashing.md5",
            "com.google.common.hash.Hashing.sha1",
            "java.security.spec.MGF1ParameterSpec.SHA1",
        },
    )

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            if n_attrs["expression"] in danger_methods:
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def kotlin_insecure_hash_instance(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.KOTLIN_INSECURE_HASH
    danger_methods = {
        "java.security.MessageDigest.getInstance",
        "security.MessageDigest.getInstance",
        "MessageDigest.getInstance",
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            if (
                n_attrs["expression"] in danger_methods
                and (al_id := graph.nodes[n_id].get("arguments_id"))
                and (arg_id := match_ast(graph, al_id).get("__0__"))
                and get_node_evaluation_results(method, graph, arg_id, set())
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def kotlin_insecure_cipher(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.KOTLIN_INSECURE_CIPHER
    danger_methods = {
        "javax.crypto.KeyGenerator.getInstance",
        "crypto.KeyGenerator.getInstance",
        "KeyGenerator.getInstance",
        "javax.crypto.KeyPairGenerator.getInstance",
        "crypto.KeyPairGenerator.getInstance",
        "KeyPairGenerator.getInstance",
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            if (
                n_attrs["expression"] in danger_methods
                and (al_id := graph.nodes[n_id].get("arguments_id"))
                and (arg_id := match_ast(graph, al_id).get("__0__"))
                and get_node_evaluation_results(method, graph, arg_id, set())
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def kotlin_insecure_cipher_mode(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.KOTLIN_INSECURE_CIPHER_MODE
    danger_methods = {
        "javax.crypto.Cipher.getInstance",
        "crypto.Cipher.getInstance",
        "Cipher.getInstance",
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            if (
                n_attrs["expression"] in danger_methods
                and (al_id := graph.nodes[n_id].get("arguments_id"))
                and (arg_id := match_ast(graph, al_id).get("__0__"))
                and get_node_evaluation_results(method, graph, arg_id, set())
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def kotlin_insecure_cipher_ssl(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.KOTLIN_INSECURE_CIPHER_SSL
    danger_methods = {
        "javax.net.ssl.SSLContext.getInstance",
        "net.ssl.SSLContext.getInstance",
        "ssl.SSLContext.getInstance",
        "SSLContext.getInstance",
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            if (
                n_attrs["expression"] in danger_methods
                and (al_id := graph.nodes[n_id].get("arguments_id"))
                and (arg_id := match_ast(graph, al_id).get("__0__"))
                and get_node_evaluation_results(method, graph, arg_id, set())
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def kotlin_insecure_cipher_http(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.KOTLIN_INSECURE_CIPHER_HTTP
    danger_methods = {"tlsVersions"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            m_names = graph.nodes[n_id]["expression"].split(".")
            if (
                m_names[-1] in danger_methods
                and (al_id := graph.nodes[n_id].get("arguments_id"))
                and (arg_id := match_ast(graph, al_id).get("__0__"))
                and get_node_evaluation_results(method, graph, arg_id, set())
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def kotlin_insecure_key_rsa(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.KOTLIN_INSECURE_KEY
    danger_methods = {
        "security.spec.RSAKeyGenParameterSpec",
        "spec.RSAKeyGenParameterSpec",
        "RSAKeyGenParameterSpec",
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            if (
                n_attrs["expression"] in danger_methods
                and (al_id := graph.nodes[n_id].get("arguments_id"))
                and (arg_id := match_ast(graph, al_id).get("__0__"))
                and get_node_evaluation_results(method, graph, arg_id, set())
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def kotlin_insecure_key_ec(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.KOTLIN_INSECURE_KEY_EC
    danger_methods = {
        "security.spec.ECGenParameterSpec",
        "spec.ECGenParameterSpec",
        "ECGenParameterSpec",
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            if (
                n_attrs["expression"] in danger_methods
                and (al_id := graph.nodes[n_id].get("arguments_id"))
                and (arg_id := match_ast(graph, al_id).get("__0__"))
                and get_node_evaluation_results(method, graph, arg_id, set())
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def kotlin_insecure_init_vector(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.KT_INSECURE_INIT_VECTOR
    danger_methods = {"GCMParameterSpec"}
    lib = "javax.crypto.spec"

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            if (
                check_method_origin(graph, lib, danger_methods, n_attrs)
                and (al_id := graph.nodes[n_id].get("arguments_id"))
                and (arg_id := match_ast(graph, al_id).get("__1__"))
                and get_node_evaluation_results(method, graph, arg_id, set())
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def search_host_verifier(graph: Graph, n_id: NId) -> str | None:
    var = pred_ast(graph, n_id)[0]
    if (
        var
        and (label_type := graph.nodes[var].get("label_type"))
        and label_type == "VariableDeclaration"
    ):
        var_name = graph.nodes[var]["variable"]
        expression = matching_nodes(graph, expression=var_name)
        if expression and graph.nodes[expression[0]]["member"] == "hostnameVerifier":
            return expression[0]
    return None


def kotlin_insecure_hostname_ver(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.KT_INSECURE_HOST_VERIFICATION
    danger_methods = {"OkHttpClient.Builder"}
    lib = "okhttp3"

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            if check_method_origin(graph, lib, danger_methods, n_attrs) and (
                verifier := search_host_verifier(graph, n_id)
            ):
                yield verifier

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def kotlin_insecure_certification(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.KT_INSECURE_CERTIFICATE_VALIDATION
    danger_set = {"TrustManager"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            if (
                n_attrs["member"] == "init"
                and (parent := pred_ast(graph, n_id)[0])
                and get_node_evaluation_results(method, graph, parent, danger_set)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def has_insufficient_size(graph: Graph, n_id: NId, size: int, method: MethodsEnum) -> bool:
    instance_min_safe_size = {
        "AES": 128,
        "RSA": 2048,
    }
    danger_instance_triggers = [{"AES"}, {"RSA"}]

    for path in get_backward_paths(graph, n_id):
        if (
            evaluation := evaluate(method, graph, path, n_id)
        ) and evaluation.triggers in danger_instance_triggers:
            instance: str = next(iter(evaluation.triggers))
            return size < instance_min_safe_size[instance]

    return False


def get_size(graph: Graph, n_id: NId) -> int | None:
    size: int | None = None

    with suppress(ValueError, TypeError):
        size = int(graph.nodes[n_id].get("value"))

    return size


def kt_insecure_key_generator(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.KT_INSECURE_KEY_GEN

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            nodes = graph.nodes

            if (
                nodes[n_id].get("expression", "").endswith("init")
                and (f_arg := get_n_arg(graph, n_id, 0))
                and (size := get_size(graph, f_arg))
                and has_insufficient_size(graph, n_id, size, method)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def kt_insecure_key_pair_generator(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.KT_INSECURE_KEY_PAIR_GEN

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            parent = pred_ast(graph, n_id)
            arg_list = graph.nodes[parent[0]].get("arguments_id")

            if (
                n_attrs["member"] == "initialize"
                and (child := adj_ast(graph, n_id)[0])
                and get_node_evaluation_results(method, graph, child, set())
                and arg_list
                and get_node_evaluation_results(method, graph, arg_list, set())
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def kt_insecure_parameter_spec(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.KT_INSECURE_PARAMETER_SPEC

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            if (
                n_attrs["expression"] == "IvParameterSpec"
                and (args_id := n_attrs.get("arguments_id"))
                and get_node_evaluation_results(method, graph, args_id, set())
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def insec_sql_database(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    n_attrs = graph.nodes[n_id]
    if (
        n_attrs["expression"] == "SQLiteDatabase.openOrCreateDatabase"
        and (args_id := n_attrs.get("arguments_id"))
        and (key := adj_ast(graph, args_id))
        and get_node_evaluation_results(method, graph, key[1], set())
    ):
        return True
    if (  # noqa: SIM103
        n_attrs["expression"] == "RealmConfiguration.Builder().encryptionKey"
        and (args_id := n_attrs.get("arguments_id"))
        and (key := adj_ast(graph, args_id))
        and get_node_evaluation_results(method, graph, key[0], set())
    ):
        return True
    return False


def kt_insecure_encryption_key(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.KT_INSECURE_ENCRYPTION_KEY

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            if insec_sql_database(graph, n_id, method):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def has_byte_definition(graph: Graph, n_id: NId) -> bool:
    nodes = graph.nodes

    for path in get_backward_paths(graph, n_id):
        if (
            (symbol := nodes[n_id].get("symbol"))
            and (def_id := definition_search(graph, path, symbol))
            and (val_id := nodes[def_id].get("value_id"))
            and (exp := nodes[val_id].get("expression"))
        ):
            if (exp == "stringToByteArray") and (first_arg := get_n_arg(graph, val_id, 0)):
                return has_string_definition(graph, first_arg) is not None

            if exp.split(".")[-1] == "toByteArray":
                return has_string_definition(graph, val_id) is not None

    return False


def kotlin_hc_secret_alg_instance(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.KOTLIN_HC_SECRET_ALG_INSTANCE
    dang_invocations = complete_attrs_on_set(
        {
            "com.auth0.jwt.algorithms.Algorithm.HMAC256",
            "com.auth0.jwt.algorithms.Algorithm.HMAC384",
            "com.auth0.jwt.algorithms.Algorithm.HMAC512",
            "io.fusionauth.jwt.hmac.HMACSigner.newSHA256Signer",
            "io.fusionauth.jwt.hmac.HMACSigner.newSHA384Signer",
            "io.fusionauth.jwt.hmac.HMACSigner.newSHA512Signer",
        },
    )

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("expression") in dang_invocations
                and (first_arg := get_n_arg(graph, n_id, 0))
                and (
                    has_string_definition(graph, first_arg) is not None
                    or has_byte_definition(graph, first_arg)
                )
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
