from lib_sast.root.f052.c_sharp import (
    c_sharp_disabled_strong_crypto as _c_sharp_disabled_strong_crypto,
)
from lib_sast.root.f052.c_sharp import (
    c_sharp_hardcoded_symmetric_key as _c_sharp_hardcoded_symmetric_key,
)
from lib_sast.root.f052.c_sharp import (
    c_sharp_insecure_cipher_member_access as _c_sharp_insec_member_access,
)
from lib_sast.root.f052.c_sharp import (
    c_sharp_insecure_cipher_object_creation as _c_sharp_cipher_obj_creation,
)
from lib_sast.root.f052.c_sharp import (
    c_sharp_insecure_hash_member_access as _c_sharp_insec_hash_member_access,
)
from lib_sast.root.f052.c_sharp import (
    c_sharp_insecure_hash_object_creation as _c_sharp_insec_hash_obj_creation,
)
from lib_sast.root.f052.c_sharp import (
    c_sharp_insecure_keys as _c_sharp_insecure_keys,
)
from lib_sast.root.f052.c_sharp import (
    c_sharp_managed_secure_mode as _c_sharp_managed_secure_mode,
)
from lib_sast.root.f052.c_sharp import (
    c_sharp_obsolete_key_derivation_method_invocation as _c_sharp_okd_method,
)
from lib_sast.root.f052.c_sharp import (
    c_sharp_obsolete_key_derivation_object_creation as _c_sharp_okd_object,
)
from lib_sast.root.f052.c_sharp import (
    c_sharp_rsa_secure_mode as _c_sharp_rsa_secure_mode,
)
from lib_sast.root.f052.c_sharp import (
    c_sharp_weak_rsa_encrypt_padding as _c_sharp_weak_rsa_encrypt_padding,
)
from lib_sast.root.f052.go import (
    go_hardcoded_symmetric_key as _go_hardcoded_symmetric_key,
)
from lib_sast.root.f052.go import (
    go_insecure_cipher as _go_insecure_cipher,
)
from lib_sast.root.f052.go import (
    go_insecure_hash as _go_insecure_hash,
)
from lib_sast.root.f052.java import (
    java_insec_sign_algorithm as _java_insec_sign_algorithm,
)
from lib_sast.root.f052.java import (
    java_insecure_cipher as _java_insecure_cipher,
)
from lib_sast.root.f052.java import (
    java_insecure_cipher_jmqi as _java_insecure_cipher_jmqi,
)
from lib_sast.root.f052.java import (
    java_insecure_cipher_mode as _java_insecure_cipher_mode,
)
from lib_sast.root.f052.java import (
    java_insecure_cipher_ssl as _java_insecure_cipher_ssl,
)
from lib_sast.root.f052.java import (
    java_insecure_connection as _java_insecure_connection,
)
from lib_sast.root.f052.java import (
    java_insecure_engine_cipher_ssl,
)
from lib_sast.root.f052.java import (
    java_insecure_hash as _java_insecure_hash,
)
from lib_sast.root.f052.java import (
    java_insecure_hash_argument as _java_insecure_hash_argument,
)
from lib_sast.root.f052.java import (
    java_insecure_key_ec as _java_insecure_key_ec,
)
from lib_sast.root.f052.java import (
    java_insecure_key_rsa as _java_insecure_key_rsa,
)
from lib_sast.root.f052.java import (
    java_insecure_key_secret as _java_insecure_key_secret,
)
from lib_sast.root.f052.java import (
    java_insecure_pass as _java_insecure_pass,
)
from lib_sast.root.f052.java import (
    java_jwt_unsafe_decode as _java_jwt_unsafe_decode,
)
from lib_sast.root.f052.java import (
    java_jwt_without_proper_sign as _java_jwt_without_proper_sign,
)
from lib_sast.root.f052.java import (
    java_weak_rsa_key as _java_weak_rsa_key,
)
from lib_sast.root.f052.javascript import (
    javascript_insecure_create_cipher as _javascript_insecure_create_cipher,
)
from lib_sast.root.f052.javascript import (
    javascript_insecure_ec_keypair as _javascript_insecure_ec_keypair,
)
from lib_sast.root.f052.javascript import (
    javascript_insecure_ecdh_key as _javascript_insecure_ecdh_key,
)
from lib_sast.root.f052.javascript import (
    javascript_insecure_encrypt as _javascript_insecure_encrypt,
)
from lib_sast.root.f052.javascript import (
    javascript_insecure_hash as _javascript_insecure_hash,
)
from lib_sast.root.f052.javascript import (
    javascript_insecure_hash_library as _javascript_insecure_hash_library,
)
from lib_sast.root.f052.javascript import (
    javascript_insecure_rsa_keypair as _javascript_insecure_rsa_keypair,
)
from lib_sast.root.f052.kotlin import (
    kotlin_hc_secret_alg_instance as _kotlin_hc_secret_alg_instance,
)
from lib_sast.root.f052.kotlin import (
    kotlin_insecure_certification as _kotlin_insecure_certification,
)
from lib_sast.root.f052.kotlin import (
    kotlin_insecure_cipher as _kotlin_insecure_cipher,
)
from lib_sast.root.f052.kotlin import (
    kotlin_insecure_cipher_http as _kotlin_insecure_cipher_http,
)
from lib_sast.root.f052.kotlin import (
    kotlin_insecure_cipher_mode as _kotlin_insecure_cipher_mode,
)
from lib_sast.root.f052.kotlin import (
    kotlin_insecure_cipher_ssl as _kotlin_insecure_cipher_ssl,
)
from lib_sast.root.f052.kotlin import (
    kotlin_insecure_hash as _kotlin_insecure_hash,
)
from lib_sast.root.f052.kotlin import (
    kotlin_insecure_hash_instance as _kotlin_insecure_hash_instance,
)
from lib_sast.root.f052.kotlin import (
    kotlin_insecure_hostname_ver as _kotlin_insecure_hostname_ver,
)
from lib_sast.root.f052.kotlin import (
    kotlin_insecure_init_vector as _kotlin_insecure_init_vector,
)
from lib_sast.root.f052.kotlin import (
    kotlin_insecure_key_ec as _kotlin_insecure_key_ec,
)
from lib_sast.root.f052.kotlin import (
    kotlin_insecure_key_rsa as _kotlin_insecure_key_rsa,
)
from lib_sast.root.f052.kotlin import (
    kt_insecure_encryption_key as _kt_insecure_encryption_key,
)
from lib_sast.root.f052.kotlin import (
    kt_insecure_key_generator as _kt_insecure_key_generator,
)
from lib_sast.root.f052.kotlin import (
    kt_insecure_key_pair_generator as _kt_insecure_key_pair_generator,
)
from lib_sast.root.f052.kotlin import (
    kt_insecure_parameter_spec as _kt_insecure_parameter_spec,
)
from lib_sast.root.f052.php import (
    php_insecure_mcrypt as _php_insecure_mcrypt,
)
from lib_sast.root.f052.php import (
    php_insecure_openssl as _php_insecure_openssl,
)
from lib_sast.root.f052.python import (
    python_insec_hash_library as _python_insec_hash_library,
)
from lib_sast.root.f052.python import (
    python_insecure_cipher as _python_insecure_cipher,
)
from lib_sast.root.f052.python import (
    python_insecure_cipher_mode as _python_insecure_cipher_mode,
)
from lib_sast.root.f052.python import (
    python_insecure_jwt_key as _python_insecure_jwt_key,
)
from lib_sast.root.f052.swift import (
    swift_hc_secret_jwt as _swift_hc_secret_jwt,
)
from lib_sast.root.f052.swift import (
    swift_insecure_cipher as _swift_insecure_cipher,
)
from lib_sast.root.f052.swift import (
    swift_insecure_cryptalgo as _swift_insecure_cryptalgo,
)
from lib_sast.root.f052.swift import (
    swift_insecure_crypto as _swift_insecure_crypto,
)
from lib_sast.root.f052.typescript import (
    typescript_insecure_create_cipher as _typescript_insecure_create_cipher,
)
from lib_sast.root.f052.typescript import (
    typescript_insecure_ec_keypair as _typescript_insecure_ec_keypair,
)
from lib_sast.root.f052.typescript import (
    typescript_insecure_ecdh_key as _typescript_insecure_ecdh_key,
)
from lib_sast.root.f052.typescript import (
    typescript_insecure_encrypt as _typescript_insecure_encrypt,
)
from lib_sast.root.f052.typescript import (
    typescript_insecure_hash as _typescript_insecure_hash,
)
from lib_sast.root.f052.typescript import (
    typescript_insecure_hash_library as _typescript_insecure_hash_library,
)
from lib_sast.root.f052.typescript import (
    typescript_insecure_rsa_keypair as _typescript_insecure_rsa_keypair,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_disabled_strong_crypto(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_disabled_strong_crypto(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_hardcoded_symmetric_key(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_hardcoded_symmetric_key(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_insecure_cipher_member_access(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_insec_member_access(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_insecure_cipher_object_creation(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_cipher_obj_creation(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_insecure_hash_member_access(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_insec_hash_member_access(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_insecure_hash_object_creation(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_insec_hash_obj_creation(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_insecure_keys(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_insecure_keys(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_managed_secure_mode(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_managed_secure_mode(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_obsolete_key_derivation_method_invocation(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_okd_method(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_obsolete_key_derivation_object_creation(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_okd_object(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_rsa_secure_mode(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_rsa_secure_mode(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_weak_rsa_encrypt_padding(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_weak_rsa_encrypt_padding(shard, method_supplies)


@SHIELD_BLOCKING
def go_hardcoded_symmetric_key(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _go_hardcoded_symmetric_key(shard, method_supplies)


@SHIELD_BLOCKING
def go_insecure_cipher(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _go_insecure_cipher(shard, method_supplies)


@SHIELD_BLOCKING
def go_insecure_hash(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _go_insecure_hash(shard, method_supplies)


@SHIELD_BLOCKING
def java_insec_sign_algorithm(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_insec_sign_algorithm(shard, method_supplies)


@SHIELD_BLOCKING
def java_insecure_cipher(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_insecure_cipher(shard, method_supplies)


@SHIELD_BLOCKING
def java_insecure_cipher_jmqi(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_insecure_cipher_jmqi(shard, method_supplies)


@SHIELD_BLOCKING
def java_insecure_cipher_mode(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_insecure_cipher_mode(shard, method_supplies)


@SHIELD_BLOCKING
def java_insecure_cipher_ssl(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_insecure_cipher_ssl(shard, method_supplies)


@SHIELD_BLOCKING
def java_insecure_connection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_insecure_connection(shard, method_supplies)


@SHIELD_BLOCKING
def java_insecure_hash(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _java_insecure_hash(shard, method_supplies)


@SHIELD_BLOCKING
def run_java_insecure_engine_cipher_ssl(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return java_insecure_engine_cipher_ssl(shard, method_supplies)


@SHIELD_BLOCKING
def java_insecure_hash_argument(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_insecure_hash_argument(shard, method_supplies)


@SHIELD_BLOCKING
def java_insecure_key_ec(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_insecure_key_ec(shard, method_supplies)


@SHIELD_BLOCKING
def java_insecure_key_rsa(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_insecure_key_rsa(shard, method_supplies)


@SHIELD_BLOCKING
def java_insecure_key_secret(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_insecure_key_secret(shard, method_supplies)


@SHIELD_BLOCKING
def java_insecure_pass(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _java_insecure_pass(shard, method_supplies)


@SHIELD_BLOCKING
def java_jwt_unsafe_decode(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_jwt_unsafe_decode(shard, method_supplies)


@SHIELD_BLOCKING
def java_jwt_without_proper_sign(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_jwt_without_proper_sign(shard, method_supplies)


@SHIELD_BLOCKING
def java_weak_rsa_key(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _java_weak_rsa_key(shard, method_supplies)


@SHIELD_BLOCKING
def js_insecure_create_cipher(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _javascript_insecure_create_cipher(shard, method_supplies)


@SHIELD_BLOCKING
def js_insecure_ec_keypair(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _javascript_insecure_ec_keypair(shard, method_supplies)


@SHIELD_BLOCKING
def js_insecure_ecdh_key(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _javascript_insecure_ecdh_key(shard, method_supplies)


@SHIELD_BLOCKING
def js_insecure_encrypt(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _javascript_insecure_encrypt(shard, method_supplies)


@SHIELD_BLOCKING
def js_insecure_hash(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _javascript_insecure_hash(shard, method_supplies)


@SHIELD_BLOCKING
def js_insecure_hash_library(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _javascript_insecure_hash_library(shard, method_supplies)


@SHIELD_BLOCKING
def js_insecure_rsa_keypair(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _javascript_insecure_rsa_keypair(shard, method_supplies)


@SHIELD_BLOCKING
def kotlin_insecure_certification(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _kotlin_insecure_certification(shard, method_supplies)


@SHIELD_BLOCKING
def kotlin_insecure_cipher(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _kotlin_insecure_cipher(shard, method_supplies)


@SHIELD_BLOCKING
def kotlin_insecure_cipher_mode(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _kotlin_insecure_cipher_mode(shard, method_supplies)


@SHIELD_BLOCKING
def kotlin_insecure_cipher_http(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _kotlin_insecure_cipher_http(shard, method_supplies)


@SHIELD_BLOCKING
def kotlin_insecure_cipher_ssl(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _kotlin_insecure_cipher_ssl(shard, method_supplies)


@SHIELD_BLOCKING
def kotlin_insecure_hash(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _kotlin_insecure_hash(shard, method_supplies)


@SHIELD_BLOCKING
def kotlin_insecure_hash_instance(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _kotlin_insecure_hash_instance(shard, method_supplies)


@SHIELD_BLOCKING
def kotlin_insecure_hostname_ver(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _kotlin_insecure_hostname_ver(shard, method_supplies)


@SHIELD_BLOCKING
def kotlin_insecure_init_vector(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _kotlin_insecure_init_vector(shard, method_supplies)


@SHIELD_BLOCKING
def kotlin_insecure_key_ec(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _kotlin_insecure_key_ec(shard, method_supplies)


@SHIELD_BLOCKING
def kotlin_insecure_key_rsa(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _kotlin_insecure_key_rsa(shard, method_supplies)


@SHIELD_BLOCKING
def kotlin_hc_secret_alg_instance(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _kotlin_hc_secret_alg_instance(shard, method_supplies)


@SHIELD_BLOCKING
def kt_insecure_encryption_key(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _kt_insecure_encryption_key(shard, method_supplies)


@SHIELD_BLOCKING
def kt_insecure_key_generator(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _kt_insecure_key_generator(shard, method_supplies)


@SHIELD_BLOCKING
def kt_insecure_key_pair_generator(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _kt_insecure_key_pair_generator(shard, method_supplies)


@SHIELD_BLOCKING
def kt_insecure_parameter_spec(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _kt_insecure_parameter_spec(shard, method_supplies)


@SHIELD_BLOCKING
def php_insecure_mcrypt(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _php_insecure_mcrypt(shard, method_supplies)


@SHIELD_BLOCKING
def php_insecure_openssl(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _php_insecure_openssl(shard, method_supplies)


@SHIELD_BLOCKING
def python_insec_hash_library(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_insec_hash_library(shard, method_supplies)


@SHIELD_BLOCKING
def python_insecure_cipher(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_insecure_cipher(shard, method_supplies)


@SHIELD_BLOCKING
def python_insecure_jwt_key(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_insecure_jwt_key(shard, method_supplies)


@SHIELD_BLOCKING
def python_insecure_cipher_mode(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_insecure_cipher_mode(shard, method_supplies)


@SHIELD_BLOCKING
def swift_hc_secret_jwt(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _swift_hc_secret_jwt(shard, method_supplies)


@SHIELD_BLOCKING
def swift_insecure_cipher(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _swift_insecure_cipher(shard, method_supplies)


@SHIELD_BLOCKING
def swift_insecure_cryptalgo(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _swift_insecure_cryptalgo(shard, method_supplies)


@SHIELD_BLOCKING
def swift_insecure_crypto(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _swift_insecure_crypto(shard, method_supplies)


@SHIELD_BLOCKING
def ts_insecure_create_cipher(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _typescript_insecure_create_cipher(shard, method_supplies)


@SHIELD_BLOCKING
def ts_insecure_ec_keypair(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _typescript_insecure_ec_keypair(shard, method_supplies)


@SHIELD_BLOCKING
def ts_insecure_ecdh_key(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _typescript_insecure_ecdh_key(shard, method_supplies)


@SHIELD_BLOCKING
def ts_insecure_encrypt(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _typescript_insecure_encrypt(shard, method_supplies)


@SHIELD_BLOCKING
def ts_insecure_hash(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _typescript_insecure_hash(shard, method_supplies)


@SHIELD_BLOCKING
def ts_insecure_hash_library(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _typescript_insecure_hash_library(shard, method_supplies)


@SHIELD_BLOCKING
def ts_insecure_rsa_keypair(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _typescript_insecure_rsa_keypair(shard, method_supplies)
