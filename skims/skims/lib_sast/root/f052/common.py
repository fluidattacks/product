from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.utilities.javascript import (
    get_default_alias,
    split_function_name,
)
from lib_sast.sast_model import (
    Graph,
    MethodSupplies,
    NId,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast,
)

C_SHARP_INSECURE_CIPHERS = {
    "AesFastEngine",
    "DES",
    "DESCryptoServiceProvider",
    "DesEdeEngine",
    "DSACryptoServiceProvider",
    "RC2",
    "RC2CryptoServiceProvider",
    "RijndaelManaged",
    "TripleDES",
    "TripleDESCng",
    "TripleDESCryptoServiceProvider",
    "Blowfish",
}

C_SHARP_INSECURE_HASH = {
    "HMACMD5",
    "HMACRIPEMD160",
    "HMACSHA1",
    "MACTripleDES",
    "MD5",
    "MD5Cng",
    "MD5CryptoServiceProvider",
    "MD5Managed",
    "RIPEMD160",
    "RIPEMD160Managed",
    "SHA1",
    "SHA1Cng",
    "SHA1CryptoServiceProvider",
    "SHA1Managed",
}


def is_insecure_encrypt(graph: Graph, al_id: NId, algo: str, method: MethodsEnum) -> bool:
    if algo in {"DES", "RC4"}:
        return True
    if (
        algo in {"AES", "Rabbit", "TripleDES", "RC4Drop"}
        and (args := adj_ast(graph, al_id))
        and len(args) > 2
    ):
        return get_node_evaluation_results(method, graph, args[-1], set())
    return False


def insecure_create_cipher(
    graph: Graph,
    method: MethodsEnum,
    method_supplies: MethodSupplies,
) -> list[NId]:
    vuln_nodes: list[NId] = []
    ciphers_methods = {
        "createdecipher",
        "createcipher",
        "createdecipheriv",
        "createcipheriv",
    }
    for n_id in method_supplies.selected_nodes:
        f_name = graph.nodes[n_id]["expression"]
        _, crypt = split_function_name(f_name)
        if (
            crypt in ciphers_methods
            and (al_id := graph.nodes[n_id].get("arguments_id"))
            and (args := adj_ast(graph, al_id))
            and len(args) > 0
            and get_node_evaluation_results(method, graph, args[0], set())
        ):
            vuln_nodes.append(n_id)

    return vuln_nodes


def insecure_hash(
    graph: Graph,
    method: MethodsEnum,
    method_supplies: MethodSupplies,
) -> list[NId]:
    return [
        n_id
        for n_id in method_supplies.selected_nodes
        if (
            graph.nodes[n_id]["expression"].split(".")[-1] == "createHash"
            and (al_id := graph.nodes[n_id].get("arguments_id"))
            and (test_node := match_ast(graph, al_id).get("__0__"))
            and get_node_evaluation_results(method, graph, test_node, set())
        )
    ]


def insecure_encrypt(
    graph: Graph,
    method: MethodsEnum,
    method_supplies: MethodSupplies,
) -> list[NId]:
    vuln_nodes: list[NId] = []
    crypto_methods = {"encrypt", "decrypt"}

    for n_id in method_supplies.selected_nodes:
        f_name = graph.nodes[n_id]["expression"]
        function_items = f_name.split(".")
        if (
            len(function_items) >= 2
            and function_items[-1] in crypto_methods
            and (al_id := graph.nodes[n_id].get("arguments_id"))
            and is_insecure_encrypt(graph, al_id, function_items[-2], method)
        ):
            vuln_nodes.append(n_id)
    return vuln_nodes


def insecure_ecdh_key(
    graph: Graph,
    method: MethodsEnum,
    method_supplies: MethodSupplies,
) -> list[NId]:
    vuln_nodes: list[NId] = []
    danger_f = {"createecdh"}

    for n_id in method_supplies.selected_nodes:
        f_name = graph.nodes[n_id]["expression"]
        _, key = split_function_name(f_name)
        if (
            key in danger_f
            and (al_id := graph.nodes[n_id].get("arguments_id"))
            and (args := adj_ast(graph, al_id))
            and len(args) > 0
            and get_node_evaluation_results(method, graph, args[0], set())
        ):
            vuln_nodes.append(n_id)
    return vuln_nodes


def insecure_rsa_keypair(
    graph: Graph,
    method: MethodsEnum,
    method_supplies: MethodSupplies,
) -> list[NId]:
    vuln_nodes: list[NId] = []
    danger_f = {"generatekeypair"}
    rules = {"rsa", "unsafemodulus"}

    for n_id in method_supplies.selected_nodes:
        f_name = graph.nodes[n_id]["expression"]
        _, key = split_function_name(f_name)
        if (
            key in danger_f
            and (al_id := graph.nodes[n_id].get("arguments_id"))
            and (args := adj_ast(graph, al_id))
            and len(args) > 1
            and get_node_evaluation_results(method, graph, al_id, rules)
        ):
            vuln_nodes.append(n_id)
    return vuln_nodes


def insecure_ec_keypair(
    graph: Graph,
    method: MethodsEnum,
    method_supplies: MethodSupplies,
) -> list[NId]:
    vuln_nodes: list[NId] = []
    danger_f = {"generatekeypair"}
    rules = {"ec", "unsafecurve"}

    for n_id in method_supplies.selected_nodes:
        f_name = graph.nodes[n_id]["expression"]
        _, key = split_function_name(f_name)
        if (
            key in danger_f
            and (al_id := graph.nodes[n_id].get("arguments_id"))
            and (args := adj_ast(graph, al_id))
            and len(args) > 1
            and get_node_evaluation_results(method, graph, al_id, rules)
        ):
            vuln_nodes.append(n_id)
    return vuln_nodes


def insecure_hash_library(
    graph: Graph,
    method_supplies: MethodSupplies,
) -> list[NId]:
    vuln_nodes: list[NId] = []
    if dangerous_name := get_default_alias(graph, "js-sha1"):
        for n_id in method_supplies.selected_nodes:
            method_expression = graph.nodes[n_id]["expression"]
            if method_expression.split(".")[0] == dangerous_name:
                vuln_nodes.append(n_id)
    return vuln_nodes
