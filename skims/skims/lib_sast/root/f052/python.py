from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    has_string_definition,
)
from lib_sast.root.utilities.python import (
    get_danger_imported_names,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils import (
    graph as g,
)
from model.core import (
    MethodExecutionResult,
)


def python_insecure_cipher(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PYTHON_UNSAFE_CIPHER

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            if (
                n_attrs["expression"] == "Cipher"
                and (al_id := n_attrs.get("arguments_id"))
                and (args_ids := g.adj_ast(graph, al_id))
                and len(args_ids) > 2
                and get_node_evaluation_results(method, graph, al_id, {"unsafemode"})
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def get_vuln_nodes(
    graph: Graph,
    method_supplies: MethodSupplies,
) -> tuple[NId, ...]:
    def predicate_matcher(node: dict[str, str]) -> bool:
        return bool(node.get("expression") in danger_libraries)

    danger_libraries = get_danger_imported_names(graph, {"hashlib.sha1"})

    return g.filter_nodes(graph, method_supplies.selected_nodes, predicate_matcher)


def python_insec_hash_library(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PYTHON_INSEC_HASH_LIBRARY

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        yield from get_vuln_nodes(graph, method_supplies)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def python_insecure_jwt_key(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PYTHON_INSECURE_JWT_KEY

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        dang_exps = get_danger_imported_names(graph, {"jwt.encode"})
        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]

            if (
                (n_attrs.get("expression") in dang_exps)
                and (key_arg_id := get_n_arg(graph, n_id, 1))
                and (has_string_definition(graph, key_arg_id) is not None)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def python_insecure_cipher_mode(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PYTHON_INSECURE_CIPHER_MODE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id]["expression"] == "AES.new"
                and (args_id := graph.nodes[n_id].get("arguments_id"))
                and get_node_evaluation_results(method, graph, args_id, set())
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
