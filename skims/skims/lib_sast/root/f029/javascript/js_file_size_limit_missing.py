from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.f029.common import (
    file_size_limit_missing,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model import (
    core,
)


def js_file_size_limit_missing(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> core.MethodExecutionResult:
    method = MethodsEnum.JS_FILE_SIZE_LIMIT_MISSING

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        yield from file_size_limit_missing(graph, method_supplies.selected_nodes)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
