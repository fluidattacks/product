from lib_sast.root.utilities.javascript import (
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def file_size_limit_missing(graph: Graph, filtered_nodes: list[NId]) -> list[NId]:
    vuln_nodes: list[NId] = []

    for n_id in filtered_nodes:
        if (
            graph.nodes[n_id]["expression"] == "multer"
            and (al_id := graph.nodes[n_id].get("arguments_id"))
            and (args_ids := adj_ast(graph, al_id))
            and len(args_ids) == 1
            and graph.nodes[args_ids[0]]["label_type"] == "Object"
        ):
            limits = get_optional_attribute(graph, args_ids[0], "limits")
            if not limits:
                vuln_nodes.append(n_id)

    return vuln_nodes
