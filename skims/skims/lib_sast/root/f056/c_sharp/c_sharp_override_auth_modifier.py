from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _check_auth_modifier(graph: Graph, n_id: NId) -> bool:
    return any(
        (
            (attr_id := graph.nodes[dec_n_id].get("attributes_id"))
            and adj_ast(graph, attr_id, label_type="Attribute", name="Authorize")
        )
        for dec_n_id in adj_ast(graph, n_id, -1, label_type="MethodDeclaration")
    )


def c_sharp_override_auth_modifier(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_OVERRIDE_AUTH_MODIFIER

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for node in method_supplies.selected_nodes:
            if (
                (p_id := next(iter(pred_ast(graph, node)), None))
                and (graph.nodes[p_id].get("label_type") == "Class")
                and adj_ast(
                    graph,
                    node,
                    label_type="Attribute",
                    name="AllowAnonymous",
                )
                and _check_auth_modifier(graph, p_id)
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
