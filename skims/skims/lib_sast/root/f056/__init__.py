from lib_sast.root.f056.c_sharp.c_sharp_override_auth_modifier import (
    c_sharp_override_auth_modifier as _c_sharp_override_auth_modifier,
)
from lib_sast.root.f056.conf_files.json_anon_connection_config import (
    anon_connection_config as _json_anon_connection_config,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def json_anon_connection_config(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _json_anon_connection_config(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_override_auth_modifier(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_override_auth_modifier(shard, method_supplies)
