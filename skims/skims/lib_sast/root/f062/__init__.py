from lib_sast.root.f062.java.java_spring_concurrent_sessions import (
    java_spring_concurrent_sessions as _java_spring_concurrent_sessions,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def java_spring_concurrent_sessions(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_spring_concurrent_sessions(shard, method_supplies)
