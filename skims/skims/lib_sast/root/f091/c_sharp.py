from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from model.core import (
    MethodExecutionResult,
)


def _is_stream_writer(graph: Graph, n_id: NId) -> bool:
    return (
        graph.nodes[n_id]["label_type"] == "ObjectCreation"
        and graph.nodes[n_id].get("name") == "StreamWriter"
    )


def _is_writing_to_stream(graph: Graph, n_id: NId) -> bool:
    member = graph.nodes[n_id].get("expression_id")
    if not member:
        return False
    if graph.nodes[member]["label_type"] == "SymbolLookup":
        symbol = graph.nodes[member].get("symbol")
        for path in get_backward_paths(graph, member):
            if (
                (def_id := definition_search(graph, path, symbol))
                and (val_id := graph.nodes[def_id].get("value_id"))
                and _is_stream_writer(graph, val_id)
            ):
                return True
    return False


def c_sharp_log_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_LOG_INJECTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            expression = graph.nodes[node]["expression"].split(".")[-1]
            if (
                expression == "WriteLine"
                and (expr_id := graph.nodes[node].get("expression_id"))
                and _is_writing_to_stream(graph, expr_id)
                and (args_id := graph.nodes[node].get("arguments_id"))
                and get_node_evaluation_results(method, graph, args_id, {"user_connection"})
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
