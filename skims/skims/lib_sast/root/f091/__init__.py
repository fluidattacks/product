from lib_sast.root.f091.c_sharp import (
    c_sharp_log_injection as _c_sharp_log_injection,
)
from lib_sast.root.f091.dart import (
    dart_insecure_logging as _dart_insecure_logging,
)
from lib_sast.root.f091.python import (
    python_flask_log_injection as _python_flask_log_injection,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_log_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_log_injection(shard, method_supplies)


@SHIELD_BLOCKING
def dart_insecure_logging(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _dart_insecure_logging(shard, method_supplies)


@SHIELD_BLOCKING
def python_flask_log_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_flask_log_injection(shard, method_supplies)
