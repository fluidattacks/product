from collections.abc import (
    Iterator,
)
from itertools import chain

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_argument_iterator,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def tfm_azure_api_mgmt_front_minimum_tls_version(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AZURE_API_MGMT_FRONT_MINIMUM_TLS_VERSION

    def n_ids() -> Iterator[NId]:
        def find_insecure_attributes(attribute_name: str) -> list[NId]:
            return [
                attr[2]
                for nid in security
                if (attr := get_optional_attribute(graph, nid, attribute_name))
                and attr[1] == "true"
            ]

        graph = shard.syntax_graph
        resource_nids = list(
            filter(
                lambda nid: graph.nodes[nid].get("name") == "azurerm_api_management",
                method_supplies.selected_nodes,
            ),
        )
        security = list(
            chain.from_iterable(
                get_argument_iterator(graph, nid, "security") for nid in resource_nids
            ),
        )
        attributes = ["enable_frontend_ssl30", "enable_frontend_tls10", "enable_frontend_tls11"]
        insecure_nodes = map(find_insecure_attributes, attributes)
        flattened_insecure_nodes = list(chain.from_iterable(insecure_nodes))

        yield from flattened_insecure_nodes

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
