from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _az_api_insecure_protocols(graph: Graph, nid: NId) -> NId | None:
    if (
        (properties := get_optional_attribute(graph, nid, "properties"))
        and (
            site_config := get_optional_attribute(
                graph,
                graph.nodes[properties[2]]["value_id"],
                "siteConfig",
            )
        )
        and (
            tls_version := get_optional_attribute(
                graph,
                graph.nodes[site_config[2]]["value_id"],
                "minTlsVersion",
            )
        )
        and tls_version[1] in {"1.0", "1.1"}
    ):
        return tls_version[2]
    return None


def tfm_azure_api_insecure_protocols(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AZURE_SERVES_CONTENT_OVER_INSECURE_PROTOCOLS

    def n_ids() -> Iterator[NId]:
        """Report API insecure protocols.

        Sources:
        https://docs.fugue.co/FG_R00347.html
        https://docs.bridgecrew.io/docs/bc_azr_networking_6
        """
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if (
                (name := graph.nodes[node].get("name"))
                and name == "azapi_resource"
                and (tls_vuln := _az_api_insecure_protocols(graph, node))
            ):
                yield tls_vuln

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def _azure_serves_content_over_insecure_protocols(graph: Graph, nid: NId) -> NId | None:
    tls_ver = get_optional_attribute(graph, nid, "min_tls_version")
    if not tls_ver:
        return nid
    if tls_ver[1] in ("TLS1_0", "TLS1_1"):
        return tls_ver[2]
    return None


def tfm_azure_serves_content_over_insecure_protocols(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AZURE_SERVES_CONTENT_OVER_INSECURE_PROTOCOLS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if (
                (name := graph.nodes[node].get("name"))
                and name == "azurerm_storage_account"
                and (report := _azure_serves_content_over_insecure_protocols(graph, node))
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
