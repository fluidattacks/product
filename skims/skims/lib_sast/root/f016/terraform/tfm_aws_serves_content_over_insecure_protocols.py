from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_argument,
    get_optional_attribute,
    list_has_string,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)

VULNERABLE_ORIGIN_SSL_PROTOCOLS = ["SSLv3", "TLSv1", "TLSv1.1"]
VULNERABLE_MIN_PROT_VERSIONS = [
    "SSLv3",
    "TLSv1",
    "TLSv1_2016",
    "TLSv1.1_2016",
]


def _has_vuln_ssl(graph: Graph, nid: NId) -> bool:
    array_id = graph.nodes[nid]["value_id"]
    return any(list_has_string(graph, array_id, prot) for prot in VULNERABLE_ORIGIN_SSL_PROTOCOLS)


def _aws_serves_content_over_insecure_protocols(graph: Graph, nid: NId) -> Iterator[NId]:
    if (
        (v_cert := get_argument(graph, nid, "viewer_certificate"))
        and (min_prot := get_optional_attribute(graph, v_cert, "minimum_protocol_version"))
        and any(True for protocol in VULNERABLE_MIN_PROT_VERSIONS if protocol == min_prot[1])
    ):
        yield min_prot[2]
    if (
        (origin := get_argument(graph, nid, "origin"))
        and (argument := get_argument(graph, origin, "custom_origin_config"))
        and (ssl_prot := get_optional_attribute(graph, argument, "origin_ssl_protocols"))
        and _has_vuln_ssl(graph, ssl_prot[2])
    ):
        yield ssl_prot[2]


def tfm_aws_serves_content_over_insecure_protocols(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AWS_SERVES_CONTENT_OVER_INSECURE_PROTOCOLS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if (name := graph.nodes[node].get("name")) and name == "aws_cloudfront_distribution":
                yield from _aws_serves_content_over_insecure_protocols(graph, node)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
