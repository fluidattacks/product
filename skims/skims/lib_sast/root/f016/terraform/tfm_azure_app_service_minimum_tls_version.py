from collections.abc import (
    Iterator,
)
from itertools import chain

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_argument_iterator,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)

AZURE_APP_SERVICES = {
    "azurerm_app_service",
    "azurerm_windows_web_app",
    "azurerm_linux_web_app",
}


def tfm_azure_app_service_minimum_tls_version(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AZURE_APP_SERVICE_MINIMUM_TLS_VERSION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        resource_nids = list(
            filter(
                lambda nid: graph.nodes[nid].get("name") in AZURE_APP_SERVICES,
                method_supplies.selected_nodes,
            ),
        )
        site_config = list(
            chain.from_iterable(
                get_argument_iterator(graph, nid, "site_config") for nid in resource_nids
            ),
        )
        minimum_tls_version: list[NId] = [
            attr[2]
            for nid in site_config
            if (attr := get_optional_attribute(graph, nid, "minimum_tls_version"))
            and attr[1] in {"1.0", "1.1"}
        ]
        yield from minimum_tls_version

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
