from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_argument,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)
from utils.cipher_suites import (
    PREDEFINED_SSL_POLICY_VALUES,
    SAFE_SSL_POLICY_VALUES,
)


def _aws_elb_without_sslpolicy(graph: Graph, nid: NId) -> NId | None:
    if ssl_pol := get_optional_attribute(graph, nid, "ssl_policy"):
        ssl_policy_val = ssl_pol[1]
        if (
            ssl_policy_val in PREDEFINED_SSL_POLICY_VALUES
            and ssl_policy_val not in SAFE_SSL_POLICY_VALUES
        ):
            return ssl_pol[2]
        return None

    for block_id in adj_ast(graph, nid, label_type="Object"):
        if graph.nodes[block_id].get("name") == "default_action" and get_argument(
            graph,
            block_id,
            "redirect",
        ):
            return None

    return nid


def tfm_aws_elb_without_sslpolicy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_AWS_ELB_WITHOUT_SSLPOLICY

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if (
                (name := graph.nodes[node].get("name"))
                and name == "aws_lb_listener"
                and (report := _aws_elb_without_sslpolicy(graph, node))
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
