from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _symbol_lookup_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    weak_ssl_tls = {"STREAM_CRYPTO_METHOD_TLSv1_1_CLIENT"}

    nodes = args.graph.nodes
    if (value := nodes[args.n_id].get("symbol")) and value in weak_ssl_tls:
        args.triggers.add("encrypt")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "symbol_lookup": _symbol_lookup_evaluator,
}


def _is_danger_stream_crypto(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    third_arg_id = get_n_arg(graph, n_id, 2)
    if third_arg_id is None:
        return False

    return get_node_evaluation_results(
        method,
        graph,
        third_arg_id,
        {"encrypt"},
        danger_goal=False,
        method_evaluators=METHOD_EVALUATORS,
    )


def _get_key_value_id(graph: Graph, n_id: NId) -> tuple[NId, NId]:
    return graph.nodes[n_id]["key_id"], graph.nodes[n_id]["value_id"]


def _is_danger_stream(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    al_id = graph.nodes[n_id].get("arguments_id")
    if al_id is None:
        return False

    for adj_arg_id in adj_ast(graph, al_id):
        for init_arg in adj_ast(graph, adj_arg_id, label_type="Pair"):
            init_key_id, init_value_id = _get_key_value_id(graph, init_arg)
            if graph.nodes[init_key_id].get("value") == "ssl":
                inner_args = [
                    _get_key_value_id(graph, inner_arg)
                    for inner_arg in adj_ast(graph, init_value_id, label_type="Pair")
                ]
                crypto_methods = [
                    value
                    for key, value in inner_args
                    if graph.nodes[key].get("value") == "crypto_method"
                ]
                for value in crypto_methods:
                    return get_node_evaluation_results(
                        method,
                        graph,
                        value,
                        {"encrypt"},
                        danger_goal=False,
                        method_evaluators=METHOD_EVALUATORS,
                    )

    return False


def php_insecure_ssl_tls_stream(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PHP_INSECURE_SSL_TLS_STREAM

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                (exp := graph.nodes[n_id].get("expression"))
                and (exp == "stream_context_create" and _is_danger_stream(graph, n_id, method))
            ) or (
                exp == "stream_socket_enable_crypto"
                and _is_danger_stream_crypto(graph, n_id, method)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
