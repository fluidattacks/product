from lib_sast.root.f016.c_sharp.c_sharp_httpclient_no_revocation_list import (
    c_sharp_httpclient_no_revocation_list as httpclient_no_revocation_list,
)
from lib_sast.root.f016.c_sharp.c_sharp_insecure_shared_access_protocol import (
    c_sharp_insecure_shared_access_protocol as insecure_access_protocol,
)
from lib_sast.root.f016.c_sharp.c_sharp_service_point_manager_disabled import (
    c_sharp_service_point_manager_disabled as service_point_manager_disabled,
)
from lib_sast.root.f016.c_sharp.c_sharp_weak_protocol import (
    c_sharp_weak_protocol as _c_sharp_weak_protocol,
)
from lib_sast.root.f016.cloudformation.cfn_elb_without_sslpolicy import (
    cfn_elb_without_sslpolicy as _cfn_elb_without_sslpolicy,
)
from lib_sast.root.f016.cloudformation.cfn_serves_content_over_insecure_protocols import (
    cfn_serves_content_over_insecure_protocols as c_serves_insecure_protocols,
)
from lib_sast.root.f016.java.java_unsafe_default_http_client import (
    java_unsafe_default_http_client as _java_unsafe_default_http_client,
)
from lib_sast.root.f016.java.java_unsafe_ssl_tls_protocol import (
    java_unsafe_ssl_tls_protocol as _java_unsafe_ssl_tls_protocol,
)
from lib_sast.root.f016.javascript.js_weak_ssl_tls_protocol import (
    js_weak_ssl_tls_protocol as _js_weak_ssl_tls_protocol,
)
from lib_sast.root.f016.kotlin.kt_default_http_client_deprecated import (
    kt_default_http_client_deprecated as _kt_default_http_client_deprecated,
)
from lib_sast.root.f016.php.php_insecure_ssl_tls_stream import (
    php_insecure_ssl_tls_stream as _php_insecure_ssl_tls_stream,
)
from lib_sast.root.f016.terraform.tfm_aws_elb_without_sslpolicy import (
    tfm_aws_elb_without_sslpolicy as _tfm_aws_elb_without_sslpolicy,
)
from lib_sast.root.f016.terraform.tfm_aws_serves_content_over_insecure_protocols import (
    tfm_aws_serves_content_over_insecure_protocols as t_aws_serves_insec_prot,
)
from lib_sast.root.f016.terraform.tfm_azure_api_mgmt_back_minimum_tls_version import (
    tfm_azure_api_mgmt_back_minimum_tls_version as _tfm_azure_api_mgmt_back_minimum_tls_version,
)
from lib_sast.root.f016.terraform.tfm_azure_api_mgmt_front_minimum_tls_version import (
    tfm_azure_api_mgmt_front_minimum_tls_version as _tfm_azure_api_mgmt_front_minimum_tls_version,
)
from lib_sast.root.f016.terraform.tfm_azure_app_service_minimum_tls_version import (
    tfm_azure_app_service_minimum_tls_version as _tfm_azure_app_service_minimum_tls_version,
)
from lib_sast.root.f016.terraform.tfm_azure_db_mysql_ssl_disabled import (
    tfm_azure_db_mysql_ssl_disabled as _tfm_azure_db_mysql_ssl_disabled,
)
from lib_sast.root.f016.terraform.tfm_azure_db_postgresql_ssl_disabled import (
    tfm_azure_db_postgresql_ssl_disabled as _tfm_azure_db_postgresql_ssl_disabled,
)
from lib_sast.root.f016.terraform.tfm_azure_postgresql_minimum_tls_version import (
    tfm_azure_postgresql_minimum_tls_version as _tfm_azure_postgresql_minimum_tls_version,
)
from lib_sast.root.f016.terraform.tfm_azure_redis_front_insecure_port_ssl import (
    tfm_azure_redis_front_insecure_port_ssl as _tfm_azure_redis_front_insecure_port_ssl,
)
from lib_sast.root.f016.terraform.tfm_azure_redis_minimum_tls_version import (
    tfm_azure_redis_minimum_tls_version as _tfm_azure_redis_minimum_tls_version,
)
from lib_sast.root.f016.terraform.tfm_azure_serves_content_over_insecure_protocols import (
    tfm_azure_api_insecure_protocols as _tfm_azure_api_insecure_protocols,
)
from lib_sast.root.f016.terraform.tfm_azure_serves_content_over_insecure_protocols import (
    tfm_azure_serves_content_over_insecure_protocols as _ta_serves_insec_prot,
)
from lib_sast.root.f016.typescript.ts_weak_ssl_tls_protocol import (
    ts_weak_ssl_tls_protocol as _ts_weak_ssl_tls_protocol,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_httpclient_no_revocation_list(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return httpclient_no_revocation_list(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_insecure_shared_access_protocol(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return insecure_access_protocol(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_service_point_manager_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return service_point_manager_disabled(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_weak_protocol(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_weak_protocol(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_elb_without_sslpolicy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_elb_without_sslpolicy(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_serves_content_over_insecure_protocols(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return c_serves_insecure_protocols(shard, method_supplies)


@SHIELD_BLOCKING
def java_unsafe_default_http_client(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_unsafe_default_http_client(shard, method_supplies)


@SHIELD_BLOCKING
def java_unsafe_ssl_tls_protocol(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_unsafe_ssl_tls_protocol(shard, method_supplies)


@SHIELD_BLOCKING
def js_weak_ssl_tls_protocol(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_weak_ssl_tls_protocol(shard, method_supplies)


@SHIELD_BLOCKING
def kt_default_http_client_deprecated(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _kt_default_http_client_deprecated(shard, method_supplies)


@SHIELD_BLOCKING
def php_insecure_ssl_tls_stream(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _php_insecure_ssl_tls_stream(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_aws_elb_without_sslpolicy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_aws_elb_without_sslpolicy(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_aws_serves_content_over_insecure_protocols(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return t_aws_serves_insec_prot(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_azure_api_insecure_protocols(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_azure_api_insecure_protocols(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_azure_serves_content_over_insecure_protocols(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ta_serves_insec_prot(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_azure_db_mysql_ssl_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_azure_db_mysql_ssl_disabled(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_azure_db_postgresql_ssl_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_azure_db_postgresql_ssl_disabled(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_azure_redis_minimum_tls_version(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_azure_redis_minimum_tls_version(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_azure_app_service_minimum_tls_version(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_azure_app_service_minimum_tls_version(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_azure_postgresql_minimum_tls_version(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_azure_postgresql_minimum_tls_version(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_azure_api_mgmt_back_minimum_tls_version(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_azure_api_mgmt_back_minimum_tls_version(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_azure_api_mgmt_front_minimum_tls_version(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_azure_api_mgmt_front_minimum_tls_version(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_azure_redis_front_insecure_port_ssl(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_azure_redis_front_insecure_port_ssl(shard, method_supplies)


@SHIELD_BLOCKING
def ts_weak_ssl_tls_protocol(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_weak_ssl_tls_protocol(shard, method_supplies)
