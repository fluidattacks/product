from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _object_creation_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id].get("name") == "WinHttpHandler":
        args.evaluation[args.n_id] = True
        args.triggers.add("http_win_client")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def _literal_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id]["value"] == "false":
        args.evaluation[args.n_id] = True
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "object_creation": _object_creation_evaluator,
    "literal": _literal_evaluator,
}


def _is_certificate_disabled(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    if (  # noqa: SIM103
        (parent_id := pred_ast(graph, n_id)[0])
        and graph.nodes[parent_id].get("label_type") == "Assignment"
        and (value_id := graph.nodes[parent_id].get("value_id"))
        and get_node_evaluation_results(
            method,
            graph,
            value_id,
            set(),
            method_evaluators=METHOD_EVALUATORS,
        )
    ):
        return True

    return False


def c_sharp_httpclient_no_revocation_list(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_HTTPCLIENT_NO_REVOCATION_LIST

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("member") == "CheckCertificateRevocationList"
                and (expr_id := graph.nodes[n_id].get("expression_id"))
                and get_node_evaluation_results(
                    method,
                    graph,
                    expr_id,
                    {"http_win_client"},
                    method_evaluators=METHOD_EVALUATORS,
                )
                and _is_certificate_disabled(graph, n_id, method)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
