from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_key_value,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    match_ast_group_d,
)
from model.core import (
    MethodExecutionResult,
)
from utils.cipher_suites import (
    PREDEFINED_SSL_POLICY_VALUES,
    SAFE_SSL_POLICY_VALUES,
)


def _elb_without_sslpolicy(graph: Graph, nid: NId) -> NId | None:
    if not (prop := get_optional_attribute(graph, nid, "Properties")):
        return None
    val_id = graph.nodes[prop[2]]["value_id"]

    if (protocol := get_optional_attribute(graph, val_id, "Protocol")) and protocol[
        1
    ].lower() == "tcp":
        return None

    if ssl_pol := get_optional_attribute(graph, val_id, "SslPolicy"):
        ssl_policy_val = ssl_pol[1]
        if (
            ssl_policy_val in PREDEFINED_SSL_POLICY_VALUES
            and ssl_policy_val not in SAFE_SSL_POLICY_VALUES
        ):
            return ssl_pol[2]
        return None

    if def_act := get_optional_attribute(graph, val_id, "DefaultActions"):
        for pair in match_ast_group_d(graph, def_act[2], "Pair", depth=-1):
            if get_key_value(graph, pair) == ("Type", "redirect"):
                return None
    return prop[2]


def cfn_elb_without_sslpolicy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_ELB_WITHOUT_SSLPOLICY

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if (
                (resource := get_optional_attribute(graph, nid, "Type"))
                and resource[1] in {"AWS::ElasticLoadBalancingV2::Listener"}
                and (report := _elb_without_sslpolicy(graph, nid))
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
