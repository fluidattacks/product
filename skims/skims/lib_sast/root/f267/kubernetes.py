from __future__ import (
    annotations,
)

import functools
import operator
from typing import (
    TYPE_CHECKING,
    TypeVar,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.path.common import (
    FALSE_OPTIONS,
    TRUE_OPTIONS,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.kubernetes import (
    CONTAINERS_TYPE,
    Natural,
    check_template_integrity,
    get_container_objects,
    get_container_spec,
    get_key_value,
    get_list_from_node,
    get_nodes_with_key,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    match_ast_group_d,
)

if TYPE_CHECKING:
    from collections.abc import (
        Callable,
        Iterator,
    )

    from model.core import (
        MethodExecutionResult,
    )

_A = TypeVar("_A")
_B = TypeVar("_B")


def _map(function: Callable[[_A], _B], items: list[_A]) -> list[_B]:
    return list(map(function, items))


def _filter(predicate: Callable[[_A], bool], items: list[_A]) -> list[_A]:
    return list(filter(predicate, items))


def _has_container(graph: Graph, nid: NId) -> bool:
    def check_pair(p_id: NId) -> bool:
        return get_key_value(graph, p_id)[0] in CONTAINERS_TYPE

    return any(map(check_pair, match_ast_group_d(graph, nid, "Pair", depth=2)))


def _get_pod_level_safe(graph: Graph, spec_ids: list[NId]) -> bool:
    container_exists = _filter(lambda s_id: _has_container(graph, s_id), spec_ids)
    pod_level_root = get_nodes_with_key(
        graph,
        container_exists,
        "runAsNonRoot",
        Natural.assert_natural(4),
    )
    pod_level_safe_list = _map(
        lambda p_id: get_key_value(graph, p_id)[1].lower() == "true",
        pod_level_root,
    )
    return any(pod_level_safe_list) or not pod_level_root


def _get_pod_seccomp_safe(graph: Graph, spec_ids: list[NId]) -> bool:
    container_exists = _filter(lambda s_id: _has_container(graph, s_id), spec_ids)
    pod_level_root = get_nodes_with_key(graph, container_exists, "type", Natural.assert_natural(6))
    pod_level_safe_list = _map(
        lambda p_id: get_key_value(graph, p_id)[1].lower() != "unconfined",
        pod_level_root,
    )
    return any(pod_level_safe_list) or not pod_level_root


def _get_pod_run_user_safe(graph: Graph, spec_ids: list[NId]) -> bool:
    container_exists = _filter(lambda s_id: _has_container(graph, s_id), spec_ids)
    pod_level_root = get_nodes_with_key(
        graph,
        container_exists,
        "runAsUser",
        Natural.assert_natural(4),
    )
    pod_level_safe_list = _map(
        lambda p_id: not _is_unsafe(graph, p_id),
        pod_level_root,
    )
    return any(pod_level_safe_list) or not pod_level_root


def _get_pod_sec_context_safe(graph: Graph, spec_ids: list[NId]) -> bool:
    container_exists = _filter(lambda s_id: _has_container(graph, s_id), spec_ids)
    pod_level_sec_context = get_nodes_with_key(
        graph,
        container_exists,
        "securityContext",
        Natural.assert_natural(2),
    )
    return any(pod_level_sec_context)


def _is_unsafe(graph: Graph, r_id: NId) -> bool:
    value = get_key_value(graph, r_id)[1]
    match value:
        case int(integer_value):
            return integer_value < 1000
        case str(string_value) if string_value.isdigit():
            return int(string_value) < 1000
        case _:
            return False


def find_ids_missing_add_and_drop(graph: Graph, sec_ids: list[NId]) -> set[NId]:
    return set(
        filter(
            lambda n_id: not get_nodes_with_key(graph, [n_id], "add", Natural.assert_natural(4))
            and not get_nodes_with_key(graph, [n_id], "drop", Natural.assert_natural(4)),
            sec_ids,
        ),
    )


def k8s_root_container(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    method = MethodsEnum.K8S_ROOT_CONTAINER

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        spec_ids = get_container_spec(graph, method_supplies.selected_nodes)
        pod_level_safe = _get_pod_level_safe(graph, spec_ids)
        cont_objs = get_container_objects(graph, spec_ids)
        cont_whitout_sec = _filter(
            lambda n_id: not get_nodes_with_key(
                graph,
                [n_id],
                "securityContext",
                Natural.assert_natural(2),
            ),
            cont_objs,
        )
        unsafe_without_sec = cont_whitout_sec if not pod_level_safe else []
        cont_with_sec = list(set(cont_objs) - set(cont_whitout_sec))
        sec_ids = get_nodes_with_key(
            graph,
            cont_with_sec,
            "securityContext",
            Natural.assert_natural(2),
        )
        sec_whitout_non_root = _filter(
            lambda n_id: not get_nodes_with_key(
                graph,
                [n_id],
                "runAsNonRoot",
                Natural.assert_natural(2),
            ),
            sec_ids,
        )
        unsafe_without_non_root = sec_whitout_non_root if not pod_level_safe else []
        sec_with_root = list(set(sec_ids) - set(sec_whitout_non_root))
        non_root_id = get_nodes_with_key(
            graph,
            sec_with_root,
            "runAsNonRoot",
            Natural.assert_natural(2),
        )
        unsafe_whit_root = _filter(
            lambda r_id: get_key_value(graph, r_id)[1].lower() == "false",
            non_root_id,
        )
        yield from (unsafe_without_sec + unsafe_without_non_root + unsafe_whit_root)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def k8s_allow_privilege_escalation_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.K8S_ALLOW_PRIVILEGE_ESCALATION_ENABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        spec_ids = get_container_spec(graph, method_supplies.selected_nodes)
        cont_objs = get_container_objects(graph, spec_ids)
        sec_ids = get_nodes_with_key(
            graph,
            cont_objs,
            "securityContext",
            Natural.assert_natural(2),
        )
        sec_without_escalation = _filter(
            lambda n_id: not get_nodes_with_key(
                graph,
                [n_id],
                "allowPrivilegeEscalation",
                Natural.assert_natural(2),
            ),
            sec_ids,
        )
        escalation_ids = get_nodes_with_key(
            graph,
            sec_ids,
            "allowPrivilegeEscalation",
            Natural.assert_natural(2),
        )
        unsafe_with_escalation = _filter(
            lambda r_id: get_key_value(graph, r_id)[1] in TRUE_OPTIONS,
            escalation_ids,
        )
        yield from (sec_without_escalation + unsafe_with_escalation)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def k8s_root_filesystem_read_only(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.K8S_ROOT_FILESYSTEM_READ_ONLY

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        spec_ids = get_container_spec(graph, method_supplies.selected_nodes)
        cont_objs = get_container_objects(graph, spec_ids)
        sec_ids = get_nodes_with_key(
            graph,
            cont_objs,
            "securityContext",
            Natural.assert_natural(2),
        )
        sec_without_escalation = _filter(
            lambda n_id: not get_nodes_with_key(
                graph,
                [n_id],
                "readOnlyRootFilesystem",
                Natural.assert_natural(2),
            ),
            sec_ids,
        )
        escalation_ids = get_nodes_with_key(
            graph,
            sec_ids,
            "readOnlyRootFilesystem",
            Natural.assert_natural(2),
        )
        unsafe_with_escalation = _filter(
            lambda r_id: get_key_value(graph, r_id)[1] in FALSE_OPTIONS,
            escalation_ids,
        )
        yield from (sec_without_escalation + unsafe_with_escalation)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def k8s_check_seccomp_profile(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.K8S_CHECK_SECCOMP_PROFILE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        spec_ids = get_container_spec(graph, method_supplies.selected_nodes)
        pod_level_safe = _get_pod_seccomp_safe(graph, spec_ids)
        cont_objs = get_container_objects(graph, spec_ids)
        cont_whitout_sec = _filter(
            lambda n_id: not get_nodes_with_key(
                graph,
                [n_id],
                "securityContext",
                Natural.assert_natural(2),
            ),
            cont_objs,
        )
        unsafe_without_sec = cont_whitout_sec if not pod_level_safe else []
        cont_with_sec = list(set(cont_objs) - set(cont_whitout_sec))
        sec_ids = get_nodes_with_key(
            graph,
            cont_with_sec,
            "securityContext",
            Natural.assert_natural(2),
        )
        sec_whitout_type = _filter(
            lambda n_id: not get_nodes_with_key(graph, [n_id], "type", Natural.assert_natural(4)),
            sec_ids,
        )
        unsafe_without_seccomp = sec_whitout_type if not pod_level_safe else []
        sec_with_type = list(set(sec_ids) - set(sec_whitout_type))
        type_id = get_nodes_with_key(graph, sec_with_type, "type", Natural.assert_natural(4))
        unsafe_whit_type = _filter(
            lambda r_id: get_key_value(graph, r_id)[1].lower() == "unconfined",
            type_id,
        )
        yield from (unsafe_without_sec + unsafe_without_seccomp + unsafe_whit_type)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def k8s_check_run_as_user(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.K8S_CHECK_RUN_AS_USER

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        spec_ids = get_container_spec(graph, method_supplies.selected_nodes)
        pod_level_safe = _get_pod_run_user_safe(graph, spec_ids)
        cont_objs = get_container_objects(graph, spec_ids)
        cont_whitout_sec = _filter(
            lambda n_id: not get_nodes_with_key(
                graph,
                [n_id],
                "securityContext",
                Natural.assert_natural(2),
            ),
            cont_objs,
        )
        unsafe_without_sec = cont_whitout_sec if not pod_level_safe else []
        cont_with_sec = list(set(cont_objs) - set(cont_whitout_sec))
        sec_ids = get_nodes_with_key(
            graph,
            cont_with_sec,
            "securityContext",
            Natural.assert_natural(2),
        )
        sec_whitout_run_user = _filter(
            lambda n_id: not get_nodes_with_key(
                graph,
                [n_id],
                "runAsUser",
                Natural.assert_natural(4),
            ),
            sec_ids,
        )
        unsafe_without_run_user = sec_whitout_run_user if not pod_level_safe else []
        sec_with_run_user = list(set(sec_ids) - set(sec_whitout_run_user))
        run_user_id = get_nodes_with_key(
            graph,
            sec_with_run_user,
            "runAsUser",
            Natural.assert_natural(4),
        )
        unsafe_with_run_user = _filter(lambda nid: _is_unsafe(graph, nid), run_user_id)
        yield from (unsafe_without_sec + unsafe_without_run_user + unsafe_with_run_user)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def k8s_check_privileged_used(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.K8S_CHECK_PRIVILEGED_USED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        spec_ids = get_container_spec(graph, method_supplies.selected_nodes)
        cont_objs = get_container_objects(graph, spec_ids)
        sec_ids = get_nodes_with_key(
            graph,
            cont_objs,
            "securityContext",
            Natural.assert_natural(2),
        )
        privileged_id = get_nodes_with_key(graph, sec_ids, "privileged", Natural.assert_natural(2))
        unsafe_with_privileged = _filter(
            lambda r_id: get_key_value(graph, r_id)[1] in TRUE_OPTIONS,
            privileged_id,
        )
        yield from unsafe_with_privileged

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def k8s_check_drop_capability(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.K8S_CHECK_DROP_CAPABILITY

    def n_ids() -> Iterator[NId]:
        def check_drop_presence(sublist: list[str]) -> bool:
            return all(
                add_cap.lower()
                not in {
                    "all",
                }
                for add_cap in sublist
            )

        graph = shard.syntax_graph
        spec_ids = get_container_spec(graph, method_supplies.selected_nodes)
        containers_ids = get_nodes_with_key(
            graph,
            spec_ids,
            "containers",
            Natural.assert_natural(2),
        )
        drop_ids = get_nodes_with_key(graph, containers_ids, "drop", Natural.assert_natural(8))
        drop_cap = _map(
            lambda n_id: get_list_from_node(graph, n_id),
            drop_ids,
        )
        id_cap_pairs = list(zip(drop_ids, drop_cap, strict=False))
        unsafe_add_ids = _filter(
            lambda pair: check_drop_presence(pair[1]),
            id_cap_pairs,
        )

        yield from (add[0] for add in unsafe_add_ids)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def k8s_check_if_capability_exists(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.K8S_CHECK_IF_CAPABILITY_EXISTS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        spec_ids = get_container_spec(graph, method_supplies.selected_nodes)
        cont_objs = get_container_objects(graph, spec_ids)
        sec_ids = get_nodes_with_key(
            graph,
            cont_objs,
            "securityContext",
            Natural.assert_natural(2),
        )
        final_ids = find_ids_missing_add_and_drop(graph, sec_ids)
        yield from final_ids

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def k8s_check_if_sys_admin_exists(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.K8S_CHECK_IF_SYS_ADMIN_EXISTS

    def n_ids() -> Iterator[NId]:
        def check_sys_admin_presence(sublist: list[str]) -> bool:
            return any(
                add_cap.lower()
                in {
                    "sys_admin",
                }
                for add_cap in sublist
            )

        graph = shard.syntax_graph
        spec_ids = get_container_spec(graph, method_supplies.selected_nodes)
        containers_ids = get_nodes_with_key(
            graph,
            spec_ids,
            "containers",
            Natural.assert_natural(2),
        )
        add_ids = get_nodes_with_key(graph, containers_ids, "add", Natural.assert_natural(8))
        add_cap = _map(
            lambda n_id: get_list_from_node(graph, n_id),
            add_ids,
        )
        id_cap_pairs = list(zip(add_ids, add_cap, strict=False))
        unsafe_add_ids = _filter(
            lambda pair: check_sys_admin_presence(pair[1]),
            id_cap_pairs,
        )
        yield from (add[0] for add in unsafe_add_ids)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def k8s_container_without_securitycontext(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.K8S_CONTAINER_WITHOUT_SECURITYCONTEXT

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        spec_ids = get_container_spec(graph, method_supplies.selected_nodes)
        pod_level_safe = _get_pod_sec_context_safe(graph, spec_ids)
        cont_objs = get_container_objects(graph, spec_ids)
        cont_without_sec = _filter(
            lambda n_id: not get_nodes_with_key(
                graph,
                [n_id],
                "securityContext",
                Natural.assert_natural(2),
            ),
            cont_objs,
        )
        if not pod_level_safe:
            yield from cont_without_sec

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def k8s_host_process_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        valid_nodes = _filter(
            lambda nid: check_template_integrity(graph, nid),
            method_supplies.selected_nodes,
        )

        host_process_ids: list[NId] = functools.reduce(
            operator.iadd,
            (
                _filter(
                    lambda p_id: get_key_value(graph, p_id)[0] == "hostProcess",
                    match_ast_group_d(graph, nid, "Pair", depth=-1),
                )
                for nid in valid_nodes
            ),
            [],
        )

        host_process_ids_in_true = _filter(
            lambda p_id: get_key_value(graph, p_id)[1] == "true",
            host_process_ids,
        )

        yield from host_process_ids_in_true

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=MethodsEnum.K8S_HOST_PROCESS_ENABLED,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def k8s_host_path_volumes(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        valid_nodes = _filter(
            lambda nid: check_template_integrity(graph, nid),
            method_supplies.selected_nodes,
        )

        spec_ids: list[NId] = functools.reduce(
            operator.iadd,
            (
                _filter(
                    lambda p_id: get_key_value(graph, p_id)[0] == "spec",
                    match_ast_group_d(graph, nid, "Pair", depth=-1),
                )
                for nid in valid_nodes
            ),
            [],
        )

        volumes_ids: list[NId] = functools.reduce(
            operator.iadd,
            (
                _filter(
                    lambda p_id: get_key_value(graph, p_id)[0] == "volumes",
                    match_ast_group_d(graph, nid, "Pair", depth=2),
                )
                for nid in spec_ids
            ),
            [],
        )

        volumes_objs: list[NId] = functools.reduce(
            operator.iadd,
            (match_ast_group_d(graph, nid, "Object", depth=2) for nid in volumes_ids),
            [],
        )

        host_path_ids: list[NId] = functools.reduce(
            operator.iadd,
            (
                _filter(
                    lambda p_id: get_key_value(graph, p_id)[0] == "hostPath",
                    match_ast_group_d(graph, nid, "Pair", depth=1),
                )
                for nid in volumes_objs
            ),
            [],
        )

        yield from host_path_ids

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=MethodsEnum.K8S_HOST_PATH_VOLUMES,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def k8s_sa_token_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        valid_nodes = _filter(
            lambda nid: check_template_integrity(graph, nid),
            method_supplies.selected_nodes,
        )
        spec_ids = get_nodes_with_key(graph, valid_nodes, "spec", None)
        container_exists = _filter(lambda s_id: _has_container(graph, s_id), spec_ids)
        sa_token_ids = get_nodes_with_key(
            graph,
            spec_ids,
            "automountServiceAccountToken",
            Natural.assert_natural(2),
        )
        sa_token_ids_in_true = _filter(
            lambda p_id: get_key_value(graph, p_id)[1] == "true",
            sa_token_ids,
        )
        yield from (sa_token_ids_in_true if sa_token_ids else container_exists)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=MethodsEnum.K8S_SA_TOKEN_ENABLED,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
