from lib_sast.root.f267.kubernetes import (
    k8s_allow_privilege_escalation_enabled as _k8s_allow_priv_esc_enabled,
)
from lib_sast.root.f267.kubernetes import (
    k8s_check_drop_capability as _k8s_check_drop_capability,
)
from lib_sast.root.f267.kubernetes import (
    k8s_check_if_capability_exists as _k8s_check_if_capability_exists,
)
from lib_sast.root.f267.kubernetes import (
    k8s_check_if_sys_admin_exists as _k8s_check_if_sys_admin_exists,
)
from lib_sast.root.f267.kubernetes import (
    k8s_check_privileged_used as _k8s_check_privileged_used,
)
from lib_sast.root.f267.kubernetes import (
    k8s_check_run_as_user as _k8s_check_run_as_user,
)
from lib_sast.root.f267.kubernetes import (
    k8s_check_seccomp_profile as _k8s_check_seccomp_profile,
)
from lib_sast.root.f267.kubernetes import (
    k8s_container_without_securitycontext as _k8s_container_without_sc,
)
from lib_sast.root.f267.kubernetes import (
    k8s_host_path_volumes as _k8s_host_path_volumes,
)
from lib_sast.root.f267.kubernetes import (
    k8s_host_process_enabled as _k8s_host_process_enabled,
)
from lib_sast.root.f267.kubernetes import (
    k8s_root_container as _k8s_root_container,
)
from lib_sast.root.f267.kubernetes import (
    k8s_root_filesystem_read_only as _k8s_root_filesystem_read_only,
)
from lib_sast.root.f267.kubernetes import (
    k8s_sa_token_enabled as _k8s_sa_token_enabled,
)
from lib_sast.root.f267.terraform import (
    tfm_k8s_allow_privilege_escalation_enabled as _tfm_k8s_all_priv_esc_en,
)
from lib_sast.root.f267.terraform import (
    tfm_k8s_check_drop_capability as _tfm_k8s_check_drop_capability,
)
from lib_sast.root.f267.terraform import (
    tfm_k8s_check_if_capability_exists as _tfm_k8s_check_if_capability_exists,
)
from lib_sast.root.f267.terraform import (
    tfm_k8s_check_if_sys_admin_exists as _tfm_k8s_check_if_sys_admin_exists,
)
from lib_sast.root.f267.terraform import (
    tfm_k8s_check_privileged_used as _tfm_k8s_check_privileged_used,
)
from lib_sast.root.f267.terraform import (
    tfm_k8s_check_run_as_user as _tfm_k8s_check_run_as_user,
)
from lib_sast.root.f267.terraform import (
    tfm_k8s_check_seccomp_profile as _tfm_k8s_check_seccomp_profile,
)
from lib_sast.root.f267.terraform import (
    tfm_k8s_container_without_securitycontext as _tfm_k8s_cont_without_sc,
)
from lib_sast.root.f267.terraform import (
    tfm_k8s_host_path_volumes as _tfm_k8s_host_path_volumes,
)
from lib_sast.root.f267.terraform import (
    tfm_k8s_host_process_enabled as _tfm_k8s_host_process_enabled,
)
from lib_sast.root.f267.terraform import (
    tfm_k8s_root_container as _tfm_k8s_root_container,
)
from lib_sast.root.f267.terraform import (
    tfm_k8s_root_filesystem_read_only as _tfm_k8s_root_filesystem_read_only,
)
from lib_sast.root.f267.terraform import (
    tfm_k8s_sa_token_enabled as _tfm_k8s_sa_token_enabled,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def k8s_allow_privilege_escalation_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _k8s_allow_priv_esc_enabled(shard, method_supplies)


@SHIELD_BLOCKING
def k8s_check_drop_capability(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _k8s_check_drop_capability(shard, method_supplies)


@SHIELD_BLOCKING
def k8s_check_if_capability_exists(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _k8s_check_if_capability_exists(shard, method_supplies)


@SHIELD_BLOCKING
def k8s_check_if_sys_admin_exists(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _k8s_check_if_sys_admin_exists(shard, method_supplies)


@SHIELD_BLOCKING
def k8s_check_privileged_used(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _k8s_check_privileged_used(shard, method_supplies)


@SHIELD_BLOCKING
def k8s_check_run_as_user(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _k8s_check_run_as_user(shard, method_supplies)


@SHIELD_BLOCKING
def k8s_check_seccomp_profile(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _k8s_check_seccomp_profile(shard, method_supplies)


@SHIELD_BLOCKING
def k8s_container_without_securitycontext(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _k8s_container_without_sc(shard, method_supplies)


@SHIELD_BLOCKING
def k8s_root_container(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _k8s_root_container(shard, method_supplies)


@SHIELD_BLOCKING
def k8s_root_filesystem_read_only(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _k8s_root_filesystem_read_only(shard, method_supplies)


@SHIELD_BLOCKING
def k8s_host_process_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _k8s_host_process_enabled(shard, method_supplies)


@SHIELD_BLOCKING
def k8s_host_path_volumes(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _k8s_host_path_volumes(shard, method_supplies)


@SHIELD_BLOCKING
def k8s_sa_token_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _k8s_sa_token_enabled(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_k8s_root_container(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_k8s_root_container(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_k8s_allow_privilege_escalation_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_k8s_all_priv_esc_en(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_k8s_root_filesystem_read_only(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_k8s_root_filesystem_read_only(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_k8s_check_seccomp_profile(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_k8s_check_seccomp_profile(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_k8s_check_drop_capability(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_k8s_check_drop_capability(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_k8s_check_if_capability_exists(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_k8s_check_if_capability_exists(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_k8s_check_run_as_user(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_k8s_check_run_as_user(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_k8s_check_privileged_used(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_k8s_check_privileged_used(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_k8s_check_if_sys_admin_exists(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_k8s_check_if_sys_admin_exists(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_k8s_container_without_securitycontext(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_k8s_cont_without_sc(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_k8s_host_process_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_k8s_host_process_enabled(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_k8s_host_path_volumes(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_k8s_host_path_volumes(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_k8s_sa_token_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_k8s_sa_token_enabled(shard, method_supplies)
