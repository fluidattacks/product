from lib_sast.root.f343.javascript import (
    js_insecure_compression as _js_insecure_compression,
)
from lib_sast.root.f343.typescript import (
    ts_insecure_compression as _ts_insecure_compression,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def js_insecure_compression(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_insecure_compression(shard, method_supplies)


@SHIELD_BLOCKING
def ts_insecure_compression(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_insecure_compression(shard, method_supplies)
