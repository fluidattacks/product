from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils.graph import (
    adj_ast,
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def is_danger_expression(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    n_attrs = graph.nodes[n_id]
    member = n_attrs["member"]
    parent_id = pred_ast(graph, n_id)[0]
    if (
        member != "search_s"
        or graph.nodes[parent_id]["label_type"] != "MethodInvocation"
        or not get_node_evaluation_results(method, graph, n_attrs["expression_id"], {"ldapconnect"})
    ):
        return False

    m_attrs = graph.nodes[parent_id]
    al_id = m_attrs.get("arguments_id")
    if not al_id:
        return False
    args_ids = adj_ast(graph, al_id)

    return (
        len(args_ids) > 2
        and get_node_evaluation_results(method, graph, args_ids[0], {"userparams"})
        and get_node_evaluation_results(method, graph, args_ids[2], {"userparams"})
    )


def python_ldap_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PYTHON_LDAP_INJECTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if is_danger_expression(graph, n_id, method):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
