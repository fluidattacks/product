from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    K8S_CONTAINER_RESOURCE_DEPTHS,
    get_argument_iterator_d,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _check_spec_attr_enabled(attr: str, graph: Graph, nid: NId) -> Iterator[NId]:
    cont_childs_nids = adj_ast(graph, nid, label_type="Object")
    names = [graph.nodes[cid].get("name") for cid in cont_childs_nids]
    has_container = any(name == "container" for name in names)
    host_pid_attr = get_optional_attribute(graph, nid, attr)
    if has_container and host_pid_attr and host_pid_attr[1] == "true":
        yield host_pid_attr[2]


def tfm_k8s_hostpid_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_K8S_HOSTPID_ENABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in K8S_CONTAINER_RESOURCE_DEPTHS:
                resource_name = graph.nodes[nid].get("name")
                for spec_nid in get_argument_iterator_d(
                    graph,
                    nid,
                    "spec",
                    depth=K8S_CONTAINER_RESOURCE_DEPTHS[resource_name],
                ):
                    yield from _check_spec_attr_enabled("host_pid", graph, spec_nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
