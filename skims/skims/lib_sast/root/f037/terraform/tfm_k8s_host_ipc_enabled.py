from collections.abc import (
    Iterator,
)
from itertools import (
    chain,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    K8S_CONTAINER_RESOURCE_DEPTHS,
    get_argument_iterator_d,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _check_spec_attr_enabled(attr: str, graph: Graph, nid: NId) -> Iterator[NId]:
    cont_childs_nids = adj_ast(graph, nid, label_type="Object")
    names = [graph.nodes[cid].get("name") for cid in cont_childs_nids]
    has_container = any(name == "container" for name in names)
    host_pid_attr = get_optional_attribute(graph, nid, attr)
    if has_container and host_pid_attr and host_pid_attr[1] == "true":
        yield host_pid_attr[2]


def _get_spec_nids(graph: Graph, nid: NId) -> Iterator[NId]:
    resource_name = graph.nodes[nid].get("name")
    return get_argument_iterator_d(
        graph,
        nid,
        "spec",
        depth=K8S_CONTAINER_RESOURCE_DEPTHS[resource_name],
    )


def tfm_k8s_host_ipc_enabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_K8S_HOST_IPC_ENABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        selected_nids = filter(
            lambda nid: graph.nodes[nid].get("name") in K8S_CONTAINER_RESOURCE_DEPTHS,
            method_supplies.selected_nodes,
        )
        spec_nids = chain.from_iterable(_get_spec_nids(graph, nid) for nid in selected_nids)
        ipc_spec_nids = chain.from_iterable(
            _check_spec_attr_enabled("host_ipc", graph, spec_nid) for spec_nid in spec_nids
        )

        yield from ipc_spec_nids

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
