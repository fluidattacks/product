import re
from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.docker import (
    get_key_value,
    validate_path,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def _compose_image_has_digest(value: str, nid: NId) -> Iterator[NId]:
    env_var_re: re.Pattern = re.compile(r"\{.+\}")
    digest_re: re.Pattern = re.compile(".*@sha256:[a-fA-F0-9]{64}")
    if not (env_var_re.search(value) or digest_re.search(value)):
        yield nid


def docker_compose_image_has_digest(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.DOCKER_COMPOSE_IMAGE_HAS_DIGEST

    def n_ids() -> Iterator[NId]:
        if not validate_path(shard.path):
            return
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            key, value = get_key_value(graph, nid)
            if key == "image":
                yield from _compose_image_has_digest(value, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
