from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.c_sharp import (
    yield_syntax_graph_member_access,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils import (
    graph as g,
)
from model.core import (
    MethodExecutionResult,
)


def is_insecure_cookie_object(
    method: MethodsEnum,
    graph: Graph,
    object_nid: str,
    method_supplies: MethodSupplies,
) -> NId | None:
    security_props = {"HttpOnly", "Secure"}
    pred = g.pred(graph, object_nid)[0]
    var_name = {graph.nodes[pred].get("variable")}

    sec_access = []
    for nid in yield_syntax_graph_member_access(graph, var_name):
        if graph.nodes[nid].get("member") not in security_props:
            continue
        parent_id = g.pred(graph, nid)[0]
        test_nid = graph.nodes[parent_id].get("value_id")
        sec_access.append(nid)
        if get_node_evaluation_results(
            method,
            graph,
            test_nid,
            set(),
            graph_db=method_supplies.graph_db,
        ):
            return pred

    if len(sec_access) < 2:
        return pred
    return None


def c_sharp_insecurely_generated_cookies(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CSHARP_INSECURELY_GENERATED_COOKIES
    object_name = {"HttpCookie"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            if graph.nodes[node].get("name") in object_name and (
                pred := is_insecure_cookie_object(method, graph, node, method_supplies)
            ):
                yield pred

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
