from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.f042.common import (
    cookie_service_sensitive_cookies,
    is_insecure_cookie,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def insecurely_generated_cookies(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TYPESCRIPT_INSECURELY_GENERATED_COOKIES

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        yield from is_insecure_cookie(graph, method, method_supplies)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def ts_cookie_service_sensitive_info(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TYPESCRIPT_COOKIE_SERVICE_SENSITIVE_INFO

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        yield from cookie_service_sensitive_cookies(method_supplies.selected_nodes, graph)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
