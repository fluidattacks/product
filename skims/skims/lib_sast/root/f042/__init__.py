from lib_sast.root.f042.c_sharp import (
    c_sharp_insecurely_generated_cookies as _c_insecurely_generated_cookies,
)
from lib_sast.root.f042.javascript import (
    insecurely_generated_cookies as _js_insecurely_generated_cookies,
)
from lib_sast.root.f042.javascript import (
    js_cookie_service_sensitive_info as _js_cookie_service_sensitive_info,
)
from lib_sast.root.f042.typescript import (
    insecurely_generated_cookies as _ts_insecurely_generated_cookies,
)
from lib_sast.root.f042.typescript import (
    ts_cookie_service_sensitive_info as _ts_cookie_service_sensitive_info,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_insecurely_generated_cookies(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_insecurely_generated_cookies(shard, method_supplies)


@SHIELD_BLOCKING
def js_insecurely_generated_cookies(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_insecurely_generated_cookies(shard, method_supplies)


@SHIELD_BLOCKING
def ts_insecurely_generated_cookies(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_insecurely_generated_cookies(shard, method_supplies)


@SHIELD_BLOCKING
def js_cookie_service_sensitive_info(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_cookie_service_sensitive_info(shard, method_supplies)


@SHIELD_BLOCKING
def ts_cookie_service_sensitive_info(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_cookie_service_sensitive_info(shard, method_supplies)
