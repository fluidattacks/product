from lib_sast.root.f128.c_sharp import (
    c_sharp_http_only_cookie as _c_sharp_http_only_cookie,
)
from lib_sast.root.f128.java import (
    java_cookie_http_only as _java_cookie_http_only,
)
from lib_sast.root.f128.javascript import (
    js_express_insec_httponly as _js_express_insec_httponly,
)
from lib_sast.root.f128.kotlin import (
    kt_cookie_http_only as _kt_cookie_http_only,
)
from lib_sast.root.f128.python import (
    python_http_only_cookie as _python_http_only_cookie,
)
from lib_sast.root.f128.typescript import (
    ts_express_insec_httponly as _ts_express_insec_httponly,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_http_only_cookie(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_http_only_cookie(shard, method_supplies)


@SHIELD_BLOCKING
def java_cookie_http_only(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_cookie_http_only(shard, method_supplies)


@SHIELD_BLOCKING
def kt_cookie_http_only(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _kt_cookie_http_only(shard, method_supplies)


@SHIELD_BLOCKING
def python_http_only_cookie(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_http_only_cookie(shard, method_supplies)


@SHIELD_BLOCKING
def js_express_insec_httponly(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_express_insec_httponly(shard, method_supplies)


@SHIELD_BLOCKING
def ts_express_insec_httponly(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_express_insec_httponly(shard, method_supplies)
