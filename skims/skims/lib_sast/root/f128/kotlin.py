from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils import (
    graph as g,
)
from model.core import (
    MethodExecutionResult,
)


def has_http_only_attr(method: MethodsEnum, graph: Graph, n_id: NId, cookie_name: str) -> bool:
    for path in get_backward_paths(graph, n_id):
        for node in path:
            n_attrs = graph.nodes[node]
            if (
                n_attrs.get("label_type") == "MethodInvocation"
                and n_attrs.get("expression") == cookie_name + "." + "setHttpOnly"
                and n_attrs.get("label_type") == "MethodInvocation"
                and n_attrs.get("arguments_id")
                and graph.nodes.get(n_attrs.get("expression_id")).get("expression") == cookie_name
            ):
                return get_node_evaluation_results(method, graph, n_attrs["arguments_id"], set())
    return True


def analyze_insecure_cookie(method: MethodsEnum, graph: Graph, expr_id: NId, al_id: NId) -> bool:
    args_ids = g.adj_ast(graph, al_id)
    if (
        len(args_ids) == 1
        and get_node_evaluation_results(method, graph, expr_id, {"user_response"})
        and (cookie_name := graph.nodes[args_ids[0]].get("symbol"))
        and get_node_evaluation_results(method, graph, args_ids[0], {"isCookieObject"})
    ):
        return has_http_only_attr(method, graph, args_ids[0], cookie_name)
    return False


def kt_cookie_http_only(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.KOTLIN_HTTP_ONLY_COOKIE
    danger_methods = {"addCookie"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            expr = n_attrs["expression"].split(".")
            if (
                expr[-1] in danger_methods
                and (obj_id := n_attrs.get("expression_id"))
                and (symbol_id := graph.nodes[obj_id].get("expression_id"))
                and (al_id := graph.nodes[n_id].get("arguments_id"))
                and analyze_insecure_cookie(method, graph, symbol_id, al_id)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
