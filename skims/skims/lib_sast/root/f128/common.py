from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils import (
    graph as g,
)


def get_attr_n_id(graph: Graph, obj_n_id: NId, attr: str) -> NId | None:
    nodes = graph.nodes

    for pair_n_id in g.match_ast_group_d(graph, obj_n_id, "Pair"):
        if nodes[nodes[pair_n_id].get("key_id")].get("symbol") == attr:
            return nodes[pair_n_id].get("value_id")
    return None


def object_has_vuln_cookie_config(graph: Graph, n_id: NId) -> bool:
    nodes = graph.nodes
    return bool(
        (nodes[n_id].get("label_type") == "Object")
        and (cookie_n_id := get_attr_n_id(graph, n_id, "cookie"))
        and (nodes[cookie_n_id].get("label_type") == "Object")
        and (danger_val_n_id := get_attr_n_id(graph, cookie_n_id, "httpOnly"))
        and (nodes[danger_val_n_id].get("value") == "false"),
    )


def has_vuln_cookie_config(graph: Graph, n_id: NId) -> bool:
    if (label_type := graph.nodes[n_id].get("label_type")) == "Object":
        return object_has_vuln_cookie_config(graph, n_id)
    if label_type == "SymbolLookup":
        symbol = graph.nodes[n_id].get("symbol")

        for path in get_backward_paths(graph, n_id):
            if (def_id := definition_search(graph, path, symbol)) and (
                val_id := graph.nodes[def_id].get("value_id")
            ):
                return object_has_vuln_cookie_config(graph, val_id)

    return False
