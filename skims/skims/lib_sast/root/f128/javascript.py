from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.f128.common import (
    has_vuln_cookie_config,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.root.utilities.javascript import (
    get_default_alias,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def js_express_insec_httponly(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JS_EXPRESS_INSEC_HTTPONLY

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not (session_name := get_default_alias(graph, "express-session")):
            return
        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("expression") == session_name
                and (f_arg := get_n_arg(graph, n_id, 0))
                and has_vuln_cookie_config(graph, f_arg)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
