from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _is_dangerous_cookie(graph: Graph, n_id: NId) -> bool:
    if (  # noqa: SIM103
        graph.nodes[n_id]["label_type"] == "Literal"
        and (value := graph.nodes[n_id].get("value"))
        and value.lower() == "false"
    ):
        return True
    return False


def eval_danger_cookie(graph: Graph, n_id: NId) -> bool:
    parent = pred_ast(graph, n_id)[0]
    if graph.nodes[parent]["label_type"] != "Assignment":
        return False

    assignment_val = graph.nodes[parent]["value_id"]
    if _is_dangerous_cookie(graph, assignment_val):
        return True
    if graph.nodes[assignment_val]["label_type"] == "SymbolLookup":
        symbol = graph.nodes[assignment_val].get("symbol")
        for path in get_backward_paths(graph, assignment_val):
            if (
                (def_id := definition_search(graph, path, symbol))
                and (val_id := graph.nodes[def_id].get("value_id"))
                and _is_dangerous_cookie(graph, val_id)
            ):
                return True
    return False


def c_sharp_http_only_cookie(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_HTTP_ONLY_COOKIE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                (member := graph.nodes[n_id].get("member"))
                and member == "HttpOnly"
                and eval_danger_cookie(graph, n_id)
                and (expr_id := graph.nodes[n_id]["expression_id"])
                and get_node_evaluation_results(method, graph, expr_id, set())
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
