from lib_sast.root.f353.c_sharp import (
    c_sharp_token_validation_checks as _c_sharp_token_validation_checks,
)
from lib_sast.root.f353.javascript import (
    decode_insecure_jwt_token as _js_decode_insecure_jwt_token,
)
from lib_sast.root.f353.typescript import (
    decode_insecure_jwt_token as _ts_decode_insecure_jwt_token,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_token_validation_checks(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_token_validation_checks(shard, method_supplies)


@SHIELD_BLOCKING
def js_decode_insecure_jwt_token(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _js_decode_insecure_jwt_token(shard, method_supplies)


@SHIELD_BLOCKING
def ts_decode_insecure_jwt_token(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _ts_decode_insecure_jwt_token(shard, method_supplies)
