import re

from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def could_be_boolean(key: str) -> bool:
    prefixes = {"is", "has", "es"}
    match = re.search("[a-z]", key, re.IGNORECASE)
    if match:
        _key = key[match.start() :]
        return any(_key.startswith(prefix) for prefix in prefixes)
    return False


def is_smell_dangerous(value: str) -> bool:
    dangerous_smells = {
        "auth",
        "authenticationtoken",
        "credential",
        "documentousuario",
        "jwttoken",
        "mailuser",
        "nameuser",
        "nombreusuario",
        "password",
        "sesiondata",
        "sesionid",
        "sessiondata",
        "sessionid",
        "sessiontoken",
        "tmptoken",
        "tokenaccess",
        "tokenapp",
        "tokenid",
        "totptmptoken",
        "totptoken",
    }
    value = re.sub(r"[-_.]", "", value)
    if could_be_boolean(value):
        return False

    return any(smell in value for smell in dangerous_smells)


def get_smell_value(graph: Graph, nid: NId) -> str:
    value = ""
    if graph.nodes[nid]["label_type"] == "SymbolLookup":
        value = graph.nodes[nid]["symbol"]
    elif graph.nodes[nid]["label_type"] == "Literal" and graph.nodes[nid]["value_type"] == "string":
        value = graph.nodes[nid]["value"]
    elif graph.nodes[nid]["label_type"] == "MemberAccess":
        value = f"{graph.nodes[nid]['expression']}.{graph.nodes[nid]['member']}"

    return value.lower()


def is_insecure_storage(graph: Graph, nid: NId, al_id: NId) -> bool:
    expr = graph.nodes[nid]["expression"]
    opc_nid = adj_ast(graph, al_id)

    if "setItem" in expr and len(opc_nid) > 1:
        key_smell = get_smell_value(graph, opc_nid[0])
        value_smell = get_smell_value(graph, opc_nid[1])
        return (is_smell_dangerous(key_smell) or key_smell == "token") and is_smell_dangerous(
            value_smell,
        )

    return False


def client_storage(graph: Graph, n_id: NId) -> bool:
    danger_names = {
        "localStorage.setItem",
        "sessionStorage.setItem",
    }
    f_name = graph.nodes[n_id].get("expression")
    if (  # noqa: SIM103
        f_name in danger_names
        and (al_id := graph.nodes[n_id].get("arguments_id"))
        and is_insecure_storage(graph, n_id, al_id)
    ):
        return True
    return False
