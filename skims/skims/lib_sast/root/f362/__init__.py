from lib_sast.root.f362.php import (
    php_technical_info_leak as _php_technical_info_leak,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def php_technical_info_leak(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _php_technical_info_leak(shard, method_supplies)
