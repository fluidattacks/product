from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _is_dangerous_id(graph: Graph, n_id: NId) -> bool:
    if (  # noqa: SIM103
        graph.nodes[n_id].get("label_type") == "MethodInvocation"
        and (value := graph.nodes[n_id].get("expression"))
        and value in {"mysqli_insert_id", "mysql_insert_id"}
    ):
        return True
    return False


def _eval_danger_cookie(graph: Graph, n_id: NId) -> bool:
    if _is_dangerous_id(graph, n_id):
        return True

    if graph.nodes[n_id]["label_type"] == "SymbolLookup":
        symbol = graph.nodes[n_id].get("symbol")
        for path in get_backward_paths(graph, n_id):
            if (
                (def_id := definition_search(graph, path, symbol))
                and (val_id := graph.nodes[def_id].get("value_id"))
                and _is_dangerous_id(graph, val_id)
            ):
                return True
    return False


def is_smell(graph: Graph, n_id: NId) -> NId | None:
    dangerous_cookies = {"session", "sessiontoken"}
    if (
        (al_id := graph.nodes[n_id].get("arguments_id"))
        and (args_id := adj_ast(graph, al_id))
        and len(args_id) > 1
        and (graph.nodes[args_id[0]]["label_type"] == "Literal")
        and graph.nodes[args_id[0]].get("value") in dangerous_cookies
    ):
        return args_id[1]
    return None


def php_generates_insecure_token(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PHP_GENERATES_INSECURE_TOKEN

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("expression") == "setcookie"
                and (danger_value := is_smell(graph, n_id))
                and _eval_danger_cookie(graph, danger_value)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
