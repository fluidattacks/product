from lib_sast.root.f262.php import (
    php_uses_sha1_in_query as _php_uses_sha1_in_query,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def php_uses_sha1_in_query(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _php_uses_sha1_in_query(shard, method_supplies)
