import re
from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from model.core import (
    MethodExecutionResult,
)


def _uses_sha1_for_password(graph: Graph, n_id: NId) -> bool:
    pattern = r"[`|\"]password[`|\"]\s*=\s*SHA1\s*\("
    if (  # noqa: SIM103
        graph.nodes[n_id]["label_type"] == "Literal"
        and (val := graph.nodes[n_id].get("value"))
        and re.search(pattern, val)
    ):
        return True
    return False


def _is_dangerous_query(graph: Graph, n_id: NId) -> bool:
    if _uses_sha1_for_password(graph, n_id):
        return True

    if (  # noqa: SIM103
        graph.nodes[n_id]["label_type"] == "MethodInvocation"
        and (expr := graph.nodes[n_id].get("expression"))
        and expr == "sprintf"
        and (first_arg := get_n_arg(graph, n_id, 0))
        and _uses_sha1_for_password(graph, first_arg)
    ):
        return True

    return False


def _query_uses_sha1(graph: Graph, n_id: NId) -> bool:
    if _is_dangerous_query(graph, n_id):
        return True

    if graph.nodes[n_id]["label_type"] == "SymbolLookup":
        symbol = graph.nodes[n_id]["symbol"]
        for path in get_backward_paths(graph, n_id):
            if (
                (def_id := definition_search(graph, path, symbol))
                and (val_id := graph.nodes[def_id].get("value_id"))
                and _is_dangerous_query(graph, val_id)
            ):
                return True
    return False


def php_uses_sha1_in_query(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PHP_USES_SHA1_IN_QUERY

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("expression") == "mysql_query"
                and (first_arg := get_n_arg(graph, n_id, 0))
                and _query_uses_sha1(graph, first_arg)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
