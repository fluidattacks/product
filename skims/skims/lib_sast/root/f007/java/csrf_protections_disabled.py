from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils import (
    graph as g,
)
from model.core import (
    MethodExecutionResult,
)


def java_csrf_protections_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CSRF_PROTECTIONS_DISABLED
    csrf_methods = {"disable", "ignoringAntMatchers"}

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            expr_id = graph.nodes[node]["expression_id"]
            if (
                graph.nodes[expr_id].get("symbol") == "csrf"
                and (parent_id := g.pred_ast(graph, node)[0])
                and (expr_id := graph.nodes[parent_id].get("expression_id"))
                and graph.nodes[expr_id].get("symbol") in csrf_methods
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
