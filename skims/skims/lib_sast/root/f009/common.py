from lib_sast.path.f009.utils import (
    AWS_CREDENTIALS_PATTERN,
    JWT_TOKEN_PATTERN,
    SONAR_TOKEN_PATTERN,
    validate_jwt,
)
from lib_sast.sast_model import (
    Graph,
    MethodSupplies,
    NId,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def crypto_credentials(
    graph: Graph,
    method_supplies: MethodSupplies,
) -> list[NId]:
    vuln_nodes: list[NId] = []
    danger_methods = {
        "cryptojs.enc.base64.parse",
        "cryptojs.enc.utf16.parse",
        "cryptojs.enc.utf16le.parse",
        "cryptojs.enc.hex.parse",
        "cryptojs.enc.latin1.parse",
        "cryptojs.enc.utf8.parse",
        "enc.base64.parse",
        "enc.utf16.parse",
        "enc.utf16le.parse",
        "enc.hex.parse",
        "enc.latin1.parse",
        "enc.utf8.parse",
    }

    for n_id in method_supplies.selected_nodes:
        n_attrs = graph.nodes[n_id]
        m_name = n_attrs.get("expression")
        al_id = n_attrs.get("arguments_id")

        if not (m_name and al_id):
            continue

        arg_childs = adj_ast(graph, al_id)
        if (
            m_name.lower() in danger_methods
            and len(arg_childs) == 1
            and graph.nodes[arg_childs[0]].get("value_type") == "string"
            and graph.nodes[arg_childs[0]].get("value") not in {'""', "''"}
        ):
            vuln_nodes.append(n_id)

    return vuln_nodes


def node_contains_credentials(graph: Graph, n_id: NId, *, add_sonar_check: bool = False) -> bool:
    if graph.nodes[n_id].get("value_type") != "string":
        return False

    value = graph.nodes[n_id]["value"]
    if len(value) > 1000:
        return False
    if AWS_CREDENTIALS_PATTERN.search(value) or (
        (matches := JWT_TOKEN_PATTERN.findall(value)) and any(validate_jwt(jwt) for jwt in matches)
    ):
        return True

    return bool(add_sonar_check and SONAR_TOKEN_PATTERN.search(value))
