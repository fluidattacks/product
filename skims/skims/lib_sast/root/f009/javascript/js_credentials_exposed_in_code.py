from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
    is_test_file,
)
from lib_sast.root.f009.common import (
    node_contains_credentials,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)


def js_credentials_exposed_in_code(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.JS_CREDENTIALS_EXPOSED_IN_CODE

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_test_file(shard.path):
            for n_id in method_supplies.selected_nodes:
                if node_contains_credentials(graph, n_id):
                    yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
