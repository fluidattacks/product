from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    has_string_definition,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model import (
    core,
)


def _has_harcoded_password_in_arguments(graph: Graph, args_list_id: NId) -> bool:
    return bool(
        (
            args_n_ids := [
                arg_id
                for arg_id in adj_ast(graph, args_list_id)
                if graph.nodes[arg_id].get("label_type") != "Comment"
            ]
        )
        and len(args_n_ids) > 0
        and (has_string_definition(graph, args_n_ids[-1]))  # noqa: COM812
    )


def java_csrf_handler_hardcoded_password(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> core.MethodExecutionResult:
    method = MethodsEnum.JAVA_CSRF_HANDLER_HARDCODED_PASSWORD

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("expression", "") == "create"
                and (obj_id := graph.nodes[n_id].get("object_id"))
                and graph.nodes[obj_id].get("symbol", "") == "CSRFHandler"
                and (args_list_id := graph.nodes[n_id].get("arguments_id"))
                and _has_harcoded_password_in_arguments(graph, args_list_id)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
