import math
from collections import (
    Counter,
)
from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.common import (
    PYTHON_REQUESTS_LIBRARIES,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)

DANGER_METHODS = {
    "ClientSession",
    "Request",
    "add_header",
    "add_unredirected_header",
    "urlopen",
    "make_headers",
    "delete",
    "get",
    "head",
    "patch",
    "post",
    "put",
    "request",
}


def _is_str_high_shannon_entropy(value: str) -> bool:
    counts = Counter(value)
    frequencies = ((i / len(value)) for i in counts.values())
    entropy = -sum(f * math.log2(f) for f in frequencies)
    return entropy > 4.2


def _literal_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    value = str(args.graph.nodes[args.n_id].get("value"))
    clean_values = value.split()
    if (
        len(clean_values) == 2
        and len(adj_ast(args.graph, args.n_id)) == 0
        and _is_str_high_shannon_entropy(clean_values[1])
    ):
        args.evaluation[args.n_id] = True
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def _import_node_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    n_attrs = args.graph.nodes[args.n_id]
    if (expression := n_attrs.get("expression")) and (
        expression.split(".")[0] in PYTHON_REQUESTS_LIBRARIES
    ):
        args.triggers.add("client_connection")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def _pair_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    n_attrs = args.graph.nodes[args.n_id]
    if (
        str(args.graph.nodes[n_attrs["key_id"]].get("value")).lower() == "authorization"
        and args.generic(args.fork(n_id=n_attrs["value_id"])).danger
    ):
        args.triggers.add("exposed_token")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "import_statement": _import_node_evaluator,
    "literal": _literal_evaluator,
    "pair": _pair_evaluator,
}


def _is_danger_headers(
    method: MethodsEnum,
    graph: Graph,
    al_id: NId,
) -> bool:
    for _id in adj_ast(graph, al_id):
        if graph.nodes[_id].get("argument_name") != "headers":
            continue
        val_id = graph.nodes[_id]["value_id"]
        return get_node_evaluation_results(
            method,
            graph,
            val_id,
            {"exposed_token"},
            danger_goal=False,
            method_evaluators=METHOD_EVALUATORS,
        )
    return False


def python_exposed_token(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.EXPOSED_AUTHORIZATION_TOKEN

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            m_attrs = graph.nodes[n_id]
            expr = str(m_attrs.get("expression"))
            if (
                expr.rsplit(".", maxsplit=1)[-1] in DANGER_METHODS
                and (al_id := m_attrs.get("arguments_id"))
                and _is_danger_headers(method, graph, al_id)
                and (expr_id := m_attrs.get("expression_id"))
                and get_node_evaluation_results(
                    method,
                    graph,
                    expr_id,
                    {"client_connection"},
                    danger_goal=False,
                    method_evaluators=METHOD_EVALUATORS,
                )
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
