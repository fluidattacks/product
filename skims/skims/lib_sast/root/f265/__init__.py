from lib_sast.root.f265.php import (
    php_insecure_encrypt_aes as _php_insecure_encrypt_aes,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def php_insecure_encrypt_aes(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _php_insecure_encrypt_aes(shard, method_supplies)
