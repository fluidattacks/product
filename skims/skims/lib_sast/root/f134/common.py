from collections.abc import (
    Callable,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    is_node_definition_unsafe,
)
from lib_sast.root.utilities.javascript import (
    get_default_alias,
)
from lib_sast.sast_model import (
    Graph,
    MethodSupplies,
    NId,
)
from lib_sast.symbolic_eval.evaluate import (
    evaluate,
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    adj_ast,
    get_node_by_path,
    get_nodes_by_path,
    match_ast_group_d,
)


def insecure_options(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    has_origin = False
    origin_has_wildcard = False

    for pair_id in match_ast_group_d(args.graph, args.n_id, "Pair"):
        pair_node = args.graph.nodes[pair_id]
        key = args.graph.nodes[pair_node["key_id"]].get("symbol", "")
        if key.lower() == "origin":
            has_origin = True
            value = args.graph.nodes[pair_node["value_id"]].get("value", "")
            if value == "*":
                origin_has_wildcard = True

    if not has_origin or origin_has_wildcard:
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def express_instance(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    nodes = args.graph.nodes
    if (
        (val_id := nodes[args.n_id].get("value_id"))
        and (nodes[val_id].get("label_type") == "MethodInvocation")
        and (exp := nodes[val_id].get("expression"))
    ):
        args.evaluation[args.n_id] = True
        args.triggers.add(exp)

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS_1: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "object_node": insecure_options,
}

METHOD_EVALUATORS_2: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "variable_declaration": express_instance,
}


DANGER_INVOCATIONS = {
    "use",
    "get",
    "post",
    "put",
    "patch",
    "delete",
    "options",
    "head",
    "all",
}


def comes_from_express(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    var_express = get_default_alias(graph, "express")
    for path in get_backward_paths(graph, n_id):
        evaluation = evaluate(method, graph, path, n_id, method_evaluators=METHOD_EVALUATORS_2)
        if evaluation and (var_express in evaluation.triggers):
            return True
    return False


def has_insecure_cors_origin(
    graph: Graph,
    n_id: NId,
    method: MethodsEnum,
    cors_var_name: str,
    invocation_name: str,
) -> bool:
    required_arg_idx = 0 if invocation_name == "use" else 1

    return bool(
        (danger_arg := get_n_arg(graph, n_id, required_arg_idx))
        and graph.nodes[danger_arg].get("expression", "") == cors_var_name
        and (
            (not (cors_arg_n_id := get_n_arg(graph, danger_arg, 0)))
            or (
                get_node_evaluation_results(
                    method,
                    graph,
                    cors_arg_n_id,
                    set(),
                    method_evaluators=METHOD_EVALUATORS_1,
                )
            )
        ),
    )


def insecure_cors_origin(
    graph: Graph,
    method_supplies: MethodSupplies,
    method: MethodsEnum,
) -> list[NId]:
    if var_cors := get_default_alias(graph, "cors"):
        return [
            n_id
            for n_id in method_supplies.selected_nodes
            if (
                (exp := graph.nodes[n_id].get("expression"))
                and len(split_exp := exp.split(".")) == 2
                and (invocation_name := split_exp[1]) in DANGER_INVOCATIONS
                and has_insecure_cors_origin(graph, n_id, method, var_cors, invocation_name)
                and comes_from_express(graph, n_id, method)
            )
        ]
    return []


def is_wildcard_or_true_bool(graph: Graph, n_id: NId) -> bool:
    value = graph.nodes[n_id].get("value", "")
    value_type = graph.nodes[n_id].get("value_type", "")
    return value == "*" or (value == "true" and value_type == "bool")


def is_array_with_wildcard_value(graph: Graph, n_id: NId) -> bool:
    return graph.nodes[n_id]["label_type"] == "ArrayInitializer" and any(
        graph.nodes[child].get("value", "") == "*"
        for child in adj_ast(graph, n_id, label_type="Literal")
    )


def has_insecure_origin_value(graph: Graph, val_id: NId) -> bool:
    return is_wildcard_or_true_bool(graph, val_id) or is_array_with_wildcard_value(graph, val_id)


def is_origin_pair_insecure(graph: Graph, pair_id: NId) -> bool:
    return bool(
        (key_id_child := graph.nodes[pair_id].get("key_id"))
        and graph.nodes[key_id_child].get("symbol", "") == "origin"
        and (val_id_child := graph.nodes[pair_id].get("value_id"))
        and is_node_definition_unsafe(graph, val_id_child, has_insecure_origin_value),
    )


def is_cors_configuration_insecure(graph: Graph, pair_arg: NId) -> bool:
    if (
        (key_id := graph.nodes[pair_arg].get("key_id"))
        and graph.nodes[key_id].get("symbol", "") == "cors"
        and (val_id := graph.nodes[pair_arg].get("value_id"))
    ):
        if is_node_definition_unsafe(graph, val_id, is_wildcard_or_true_bool):
            return True

        for child_pair in adj_ast(graph, val_id, label_type="Pair", depth=-1):
            if is_origin_pair_insecure(graph, child_pair):
                return True

    return False


def has_insecure_cors_argument(graph: Graph, n_id: NId) -> bool:
    return any(
        is_cors_configuration_insecure(graph, pair_arg)
        for pair_arg in get_nodes_by_path(graph, n_id, (), "ArgumentList", "Object", "Pair")
    )


def has_insecure_cors_in_first_argument(graph: Graph, n_id: NId) -> bool:
    first_arg = get_n_arg(graph, n_id, 0)
    return first_arg is None or any(
        is_origin_pair_insecure(graph, child_pair)
        for child_pair in adj_ast(graph, first_arg, label_type="Pair", depth=-1)
    )


def is_nestjs_factory_instance(graph: Graph, n_id: NId) -> bool:
    return any(
        graph.nodes[child].get("expression", "") == "NestFactory"
        for child in adj_ast(graph, n_id, label_type="MemberAccess", depth=-1)
    )


def nestjs_insecure_cors_origin(
    graph: Graph,
    method_supplies: MethodSupplies,
) -> list[NId]:
    return [
        n_id
        for n_id in method_supplies.selected_nodes
        if (
            (
                graph.nodes[n_id].get("expression", "") == "NestFactory.create"
                and has_insecure_cors_argument(graph, n_id)
            )
            or (
                graph.nodes[n_id].get("expression", "").endswith(".enableCors")
                and has_insecure_cors_in_first_argument(graph, n_id)
                and (symbol_n_id := get_node_by_path(graph, n_id, "MemberAccess", "SymbolLookup"))
                and is_node_definition_unsafe(graph, symbol_n_id, is_nestjs_factory_instance)
            )
        )
    ]
