from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    has_string_definition,
    is_in_conditional,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.context.search import (
    definition_search,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.utils import (
    get_backward_paths,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast,
)
from model.core import (
    MethodExecutionResult,
)


def eval_key_danger(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    n_attrs = graph.nodes[n_id]
    if n_attrs.get("label_type") == "Literal":
        return bool((value := n_attrs.get("value")) and value.lower() == "display_errors")
    return bool(
        has_string_definition(graph, n_id) is not None
        and get_node_evaluation_results(method, graph, n_id, {"dang_key"}, danger_goal=False),
    )


def eval_value_danger(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    n_attrs = graph.nodes[n_id]
    danger_vals = {"on", "1"}

    if n_attrs.get("label_type") == "Literal":
        return bool((value := n_attrs.get("value")) and value.lower() in danger_vals)

    return bool(
        has_string_definition(graph, n_id) is not None
        and get_node_evaluation_results(method, graph, n_id, {"dang_val"}, danger_goal=False),
    )


def eval_danger_invocation(graph: Graph, key_n_id: NId, val_n_id: NId, method: MethodsEnum) -> bool:
    return bool(
        eval_key_danger(graph, key_n_id, method) and eval_value_danger(graph, val_n_id, method),
    )


def _is_sql_error_leak(graph: Graph, n_id: NId) -> bool:
    if (  # noqa: SIM103
        graph.nodes[n_id]["label_type"] == "MethodInvocation"
        and (expr := graph.nodes[n_id].get("expression"))
        and expr in {"mysqli_error", "mysql_error"}
    ):
        return True
    return False


def _exit_leaks_sql_error(graph: Graph, al_id: NId) -> bool:
    arg_id = (
        al_id
        if graph.nodes[al_id]["label_type"] == "MethodInvocation"
        else match_ast(graph, al_id).get("__0__")
    )

    if not arg_id:
        return False

    if _is_sql_error_leak(graph, arg_id):
        return True

    if graph.nodes[arg_id]["label_type"] == "SymbolLookup":
        symbol = graph.nodes[arg_id]["symbol"]
        for path in get_backward_paths(graph, arg_id):
            if (
                (def_id := definition_search(graph, path, symbol))
                and (val_id := graph.nodes[def_id].get("value_id"))
                and _is_sql_error_leak(graph, val_id)
            ):
                return True
    return False


def php_info_leak_errors(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PHP_INFO_LEAK_ERRORS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            if (
                (n_attrs.get("expression") == "ini_set")
                and not is_in_conditional(graph, n_id)
                and (args_n_id := n_attrs.get("arguments_id"))
                and (args := adj_ast(graph, args_n_id))
                and eval_danger_invocation(graph, args[0], args[1], method)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )


def php_sql_leak_errors(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PHP_SQL_LEAK_ERRORS

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for n_id in method_supplies.selected_nodes:
            n_attrs = graph.nodes[n_id]
            if (
                n_attrs.get("expression") in {"die", "exit"}
                and (al_id := n_attrs.get("arguments_id"))
                and _exit_leaks_sql_error(graph, al_id)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
