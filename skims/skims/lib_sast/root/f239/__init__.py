from lib_sast.root.f239.c_sharp import (
    c_sharp_info_leak_errors as _c_sharp_info_leak_errors,
)
from lib_sast.root.f239.php import (
    php_info_leak_errors as _php_info_leak_errors,
)
from lib_sast.root.f239.php import (
    php_sql_leak_errors as _php_sql_leak_errors,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_info_leak_errors(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_info_leak_errors(shard, method_supplies)


@SHIELD_BLOCKING
def php_info_leak_errors(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _php_info_leak_errors(shard, method_supplies)


@SHIELD_BLOCKING
def php_sql_leak_errors(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _php_sql_leak_errors(shard, method_supplies)
