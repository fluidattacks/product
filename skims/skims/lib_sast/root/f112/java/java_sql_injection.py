from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.common import (
    owasp_user_connection,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    match_ast_d,
)
from model.core import (
    MethodExecutionResult,
)

DANGER_CLASSES = {
    "JDBCtemplate",
}

SQL_CONNECTIONS = {"getSqlStatement", "getSqlConnection"}


def parameter_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id].get("variable_type") == "HttpServletRequest":
        args.triggers.add("userconnection")

    if (
        (mod_id := match_ast_d(args.graph, args.n_id, "Modifiers"))
        and (annot_id := match_ast_d(args.graph, mod_id, "Annotation"))
        and args.graph.nodes[annot_id].get("name") == "RequestParam"
    ):
        args.triggers.add("userconnection")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def method_invocation_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    ma_attr = args.graph.nodes[args.n_id]
    if owasp_user_connection(args):
        args.triggers.add("userparameters")

    if ma_attr["expression"] in SQL_CONNECTIONS:
        args.evaluation[args.n_id] = True

    if ma_attr["expression"] == "get" and (obj_id := ma_attr.get("object_id")):
        expr_eval = args.generic(args.fork(n_id=obj_id))
        if expr_eval and expr_eval.triggers == {"userconnection"}:
            args.evaluation[args.n_id] = True
            args.triggers.add("userparameters")

    if (
        (obj_id := ma_attr.get("object_id"))
        and (symb := args.graph.nodes[obj_id].get("symbol"))
        and (symb.split(".")[-1] in DANGER_CLASSES)
    ):
        args.evaluation[args.n_id] = True

    if ma_attr["expression"] == "replaceAll":
        args.triggers.add("sanitize")
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "method_invocation": method_invocation_evaluator,
    "parameter": parameter_evaluator,
}


def java_sql_injection(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_SQL_INJECTION
    danger_methods = {
        "addBatch",
        "batchUpdate",
        "executeQuery",
        "executeUpdate",
        "execute",
        "queryForList",
        "queryForMap",
        "queryForObject",
        "queryForRowSet",
    }

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for node in method_supplies.selected_nodes:
            n_attrs = graph.nodes[node]
            expr = n_attrs["expression"].split(".")

            if (
                expr[-1] in danger_methods
                or (
                    n_attrs["expression"] == "query"
                    and (obj_id := n_attrs.get("object_id"))
                    and "jdbctemplate" in str(graph.nodes[obj_id].get("symbol")).lower()
                )
            ) and get_node_evaluation_results(
                method,
                graph,
                node,
                {"userparameters", "userconnection"},
                graph_db=method_supplies.graph_db,
                method_evaluators=METHOD_EVALUATORS,
            ):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
