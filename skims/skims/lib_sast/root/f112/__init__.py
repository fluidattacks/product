from lib_sast.root.f112.java.java_sql_injection import (
    java_sql_injection as _java_sql_injection,
)
from lib_sast.root.f112.javascript import (
    unsafe_sql_injection as _js_sql_injection,
)
from lib_sast.root.f112.typescript import (
    unsafe_sql_injection as _ts_sql_injection,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def java_sql_injection(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _java_sql_injection(shard, method_supplies)


@SHIELD_BLOCKING
def js_sql_injection(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _js_sql_injection(shard, method_supplies)


@SHIELD_BLOCKING
def ts_sql_injection(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _ts_sql_injection(shard, method_supplies)
