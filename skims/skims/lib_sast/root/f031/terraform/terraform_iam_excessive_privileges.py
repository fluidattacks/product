from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)

ELEVATED_POLICIES = {
    "PowerUserAccess",
    "IAMFullAccess",
    "AdministratorAccess",
}


def _iam_excessive_privileges(graph: Graph, nid: NId) -> NId | None:
    if (
        (pol_arns := get_optional_attribute(graph, nid, "managed_policy_arns"))
        and (value_id := graph.nodes[pol_arns[2]]["value_id"])
        and graph.nodes[value_id]["label_type"] == "ArrayInitializer"
    ):
        for array_elem in adj_ast(graph, value_id):
            value = graph.nodes[array_elem]["value"]
            if value.split("/")[-1] in ELEVATED_POLICIES:
                return array_elem
    return None


def tfm_iam_excessive_privileges(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TERRAFORM_IAM_EXCESSIVE_PRIVILEGES

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "aws_iam_role" and (
                report := _iam_excessive_privileges(graph, nid)
            ):
                yield report

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
