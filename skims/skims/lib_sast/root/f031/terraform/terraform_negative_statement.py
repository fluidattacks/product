from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_dict_from_attr,
    get_dict_values,
    get_list_from_node,
    get_optional_attribute,
    iter_statements_from_policy_document,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)
from utils.aws.iam import (
    is_action_permissive,
    is_resource_permissive,
)


def _negative_statement_in_jsonencode(graph: Graph, nid: NId) -> Iterator[NId]:
    child_id = adj_ast(graph, graph.nodes[nid]["arguments_id"])[0]
    statements = get_optional_attribute(graph, child_id, "Statement")
    if not statements:
        return
    value_id = graph.nodes[statements[2]]["value_id"]
    for c_id in adj_ast(graph, value_id, label_type="Object"):
        effect = get_optional_attribute(graph, c_id, "Effect")
        if effect and effect[1] == "Allow":
            actions = get_optional_attribute(graph, c_id, "NotAction")
            if (
                actions
                and (action_list := get_list_from_node(graph, actions[2]))
                and not any(map(is_action_permissive, action_list))
            ):
                yield c_id
                continue

            resources = get_optional_attribute(graph, c_id, "NotResource")
            if (
                resources
                and (res_list := get_list_from_node(graph, resources[2]))
                and not any(map(is_resource_permissive, res_list))
            ):
                yield c_id


def _aux_negative_statement(attr_val: str, attr_id: NId) -> Iterator[NId]:
    dict_value = get_dict_from_attr(attr_val)
    statements = get_dict_values(dict_value, "Statement")
    for stmt in statements if isinstance(statements, list) else []:
        effect = stmt.get("Effect")
        if effect == "Allow":
            actions = stmt.get("NotAction")
            resource = stmt.get("NotResource")
            if (actions and not any(map(is_action_permissive, actions))) or (
                resource and not any(map(is_resource_permissive, resource))
            ):
                yield attr_id


def _negative_statement_policy_resource(graph: Graph, nid: NId) -> Iterator[NId]:
    for stmt in iter_statements_from_policy_document(graph, nid):
        effect = get_optional_attribute(graph, stmt, "effect")
        if not effect or effect[1] == "Allow":
            actions = get_optional_attribute(graph, stmt, "not_actions")
            if (
                actions
                and (action_list := get_list_from_node(graph, actions[2]))
                and not any(map(is_action_permissive, action_list))
            ):
                yield stmt
                continue

            resources = get_optional_attribute(graph, stmt, "not_resources")
            if (
                resources
                and (res_list := get_list_from_node(graph, resources[2]))
                and not any(map(is_resource_permissive, res_list))
            ):
                yield stmt


def _negative_statement(graph: Graph, nid: NId) -> Iterator[NId]:
    if graph.nodes[nid]["name"] == "aws_iam_policy_document":
        yield from _negative_statement_policy_resource(graph, nid)
    else:
        policy_attr = get_optional_attribute(graph, nid, "policy")
        if not policy_attr:
            return
        value_id = graph.nodes[policy_attr[2]]["value_id"]
        if graph.nodes[value_id]["label_type"] == "Literal":
            yield from _aux_negative_statement(policy_attr[1], policy_attr[2])
        elif (
            graph.nodes[value_id]["label_type"] == "MethodInvocation"
            and graph.nodes[value_id]["expression"] == "jsonencode"
        ):
            yield from _negative_statement_in_jsonencode(graph, value_id)


def tfm_negative_statement(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TERRAFORM_NEGATIVE_STATEMENT

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in {
                "aws_iam_group_policy",
                "aws_iam_policy",
                "aws_iam_role_policy",
                "aws_iam_user_policy",
                "aws_iam_policy_document",
            }:
                yield from _negative_statement(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
