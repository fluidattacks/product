import re
from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_dict_from_attr,
    get_dict_values,
    get_key_value,
    get_list_from_node,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_group_d,
    matching_nodes,
)
from model.core import (
    MethodExecutionResult,
)


def _get_managed_policy_arns(graph: Graph) -> list[str]:
    policy_arns = []
    for nid in matching_nodes(graph, label_type="Object"):
        if (name := graph.nodes[nid].get("name")) and name in {
            "aws_iam_group_policy_attachment",
            "aws_iam_policy_attachment",
            "aws_iam_role_policy_attachment",
            "aws_iam_user_policy_attachment",
        }:
            for attr_id in adj_ast(graph, nid, label_type="Pair"):
                _, value = get_key_value(graph, attr_id)
                if value.startswith("aws_iam_role."):
                    policy_arns.append(value.split(".")[1])
    return policy_arns


def _get_iam_role_names(graph: Graph) -> list[str]:
    policy_arns = _get_managed_policy_arns(graph)

    return [
        name_attr[1]
        for nid in matching_nodes(graph, label_type="Object")
        if (
            graph.nodes[nid].get("name") == "aws_iam_role"
            and (res_name := graph.nodes[nid]["tf_reference"].split(".")[1])
            and res_name in policy_arns
            and (name_attr := get_optional_attribute(graph, nid, "name"))
        )
    ] + ["*"]


def _action_has_attach_role(
    actions: str | list,
    resources: str | list,
    iam_role_names: list[str],
) -> bool:
    actions_list = actions if isinstance(actions, list) else [actions]
    resource_list = resources if isinstance(resources, list) else [resources]
    for action in actions_list:
        if action == "iam:Attach*" and any(
            res.startswith("arn:aws:iam")
            and ":role/" in res
            and re.split(":role/", res)[1] in iam_role_names
            for res in resource_list
        ):
            return True
    return False


def _iam_excessive_role_policy_in_jsonencode(
    graph: Graph,
    nid: NId,
    iam_role_names: list[str],
) -> Iterator[NId]:
    child_id = adj_ast(graph, graph.nodes[nid]["arguments_id"])[0]
    if statements := get_optional_attribute(graph, child_id, "Statement"):
        value_id = graph.nodes[statements[2]]["value_id"]
        for c_id in adj_ast(graph, value_id, label_type="Object"):
            if (
                (effect := get_optional_attribute(graph, c_id, "Effect"))
                and effect[1] == "Allow"
                and (action := get_optional_attribute(graph, c_id, "Action"))
                and (resources := get_optional_attribute(graph, c_id, "Resource"))
            ):
                action_list = get_list_from_node(graph, action[2])
                resources_list = get_list_from_node(graph, resources[2])
                if _action_has_attach_role(action_list, resources_list, iam_role_names):
                    yield c_id


def _iam_excessive_role_policy_in_literal(
    attr_val: str,
    attr_id: NId,
    iam_role_names: list[str],
) -> Iterator[NId]:
    dict_value = get_dict_from_attr(attr_val)
    statements = get_dict_values(dict_value, "Statement")
    if not isinstance(statements, list):
        return

    for stmt in statements:
        if (
            stmt.get("Effect") == "Allow"
            and (actions := stmt.get("Action"))
            and (resource := stmt.get("Resource"))
            and _action_has_attach_role(actions, resource, iam_role_names)
        ):
            yield attr_id


def _iam_excessive_role_policy_document(
    graph: Graph,
    nid: NId,
    iam_role_names: list[str],
) -> Iterator[NId]:
    for stmt in match_ast_group_d(graph, nid, "Object"):
        if (
            graph.nodes[stmt].get("name") == "statement"
            and (
                not (effect := get_optional_attribute(graph, stmt, "effect"))
                or effect[1] == "Allow"
            )
            and (action := get_optional_attribute(graph, stmt, "actions"))
            and (resources := get_optional_attribute(graph, stmt, "resources"))
        ):
            action_list = get_list_from_node(graph, action[2])
            resources_list = get_list_from_node(graph, resources[2])
            if _action_has_attach_role(action_list, resources_list, iam_role_names):
                yield stmt


def _iam_excessive_role_policy(
    graph: Graph,
    nid: NId,
    iam_role_names: list[str],
) -> Iterator[NId]:
    if graph.nodes[nid]["name"] == "aws_iam_policy_document":
        yield from _iam_excessive_role_policy_document(graph, nid, iam_role_names)
    elif attr := get_optional_attribute(graph, nid, "policy"):
        value_id = graph.nodes[attr[2]]["value_id"]
        if graph.nodes[value_id]["label_type"] == "Literal":
            yield from _iam_excessive_role_policy_in_literal(attr[1], attr[2], iam_role_names)
        elif (
            graph.nodes[value_id]["label_type"] == "MethodInvocation"
            and graph.nodes[value_id]["expression"] == "jsonencode"
        ):
            yield from _iam_excessive_role_policy_in_jsonencode(graph, value_id, iam_role_names)


def tfm_iam_excessive_role_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_IAM_EXCESSIVE_ROLE_POLICY

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in {
                "aws_iam_group_policy",
                "aws_iam_policy",
                "aws_iam_role_policy",
                "aws_iam_user_policy",
                "aws_iam_policy_document",
            }:
                yield from _iam_excessive_role_policy(
                    graph,
                    nid,
                    _get_iam_role_names(graph),
                )

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
