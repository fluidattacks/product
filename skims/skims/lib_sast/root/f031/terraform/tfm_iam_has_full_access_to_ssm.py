from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.f031.utils import (
    action_has_full_access_to_ssm,
)
from lib_sast.root.utilities.terraform import (
    get_dict_from_attr,
    get_dict_values,
    get_list_from_node,
    get_optional_attribute,
    iter_statements_from_policy_document,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _iam_has_full_access_to_ssm_in_jsonencode(graph: Graph, nid: NId) -> Iterator[NId]:
    child_id = adj_ast(graph, graph.nodes[nid]["arguments_id"])[0]
    statements = get_optional_attribute(graph, child_id, "Statement")
    if not statements:
        return
    value_id = graph.nodes[statements[2]]["value_id"]
    for c_id in adj_ast(graph, value_id, label_type="Object"):
        if (
            (effect := get_optional_attribute(graph, c_id, "Effect"))
            and effect[1] == "Allow"
            and (actions := get_optional_attribute(graph, c_id, "Action"))
            and (act_list := get_list_from_node(graph, actions[2]))
            and action_has_full_access_to_ssm(act_list)
        ):
            yield c_id


def _aux_iam_has_full_access_to_ssm(attr_val: str, attr_id: NId) -> Iterator[NId]:
    dict_value = get_dict_from_attr(attr_val)
    statements = get_dict_values(dict_value, "Statement")
    for stmt in statements if isinstance(statements, list) else []:
        effect = stmt.get("Effect")
        actions = stmt.get("Action", [])
        if effect == "Allow" and action_has_full_access_to_ssm(actions):
            yield attr_id


def _iam_has_full_access_to_ssm_policy_resource(graph: Graph, nid: NId) -> Iterator[NId]:
    for stmt in iter_statements_from_policy_document(graph, nid):
        effect = get_optional_attribute(graph, stmt, "effect")
        if not effect or effect[1] != "Allow":
            continue
        action = get_optional_attribute(graph, stmt, "actions")
        if (
            action
            and (action_list := get_list_from_node(graph, action[2]))
            and action_has_full_access_to_ssm(action_list)
        ):
            yield stmt


def _iam_has_full_access_to_ssm(graph: Graph, nid: NId) -> Iterator[NId]:
    if graph.nodes[nid]["name"] == "aws_iam_policy_document":
        yield from _iam_has_full_access_to_ssm_policy_resource(graph, nid)
    elif attr := get_optional_attribute(graph, nid, "policy"):
        value_id = graph.nodes[attr[2]]["value_id"]
        if graph.nodes[value_id]["label_type"] == "Literal":
            yield from _aux_iam_has_full_access_to_ssm(attr[1], attr[2])
        elif (
            graph.nodes[value_id]["label_type"] == "MethodInvocation"
            and graph.nodes[value_id]["expression"] == "jsonencode"
        ):
            yield from _iam_has_full_access_to_ssm_in_jsonencode(graph, value_id)


def tfm_iam_has_full_access_to_ssm(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_IAM_HAS_FULL_ACCESS_TO_SSM

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") in {
                "aws_iam_group_policy",
                "aws_iam_policy",
                "aws_iam_role_policy",
                "aws_iam_user_policy",
                "aws_iam_policy_document",
            }:
                yield from _iam_has_full_access_to_ssm(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
