from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.cloudformation import (
    get_attribute,
    get_list_from_node,
    iterate_iam_policy_resources,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from model.core import (
    MethodExecutionResult,
)
from utils.aws.iam import (
    is_action_permissive,
    is_resource_permissive,
)


def _negative_statement(graph: Graph, nid: NId) -> Iterator[NId]:
    effect, effect_val, _ = get_attribute(graph, nid, "Effect")
    action, _, action_id = get_attribute(graph, nid, "NotAction")
    resource, _, res_id = get_attribute(graph, nid, "NotResource")
    action_list = get_list_from_node(graph, action_id)
    res_list = get_list_from_node(graph, res_id)
    if effect and effect_val == "Allow":
        if action:
            yield from (action_id for act in action_list if not is_action_permissive(act))

        if resource:
            yield from (res_id for res in res_list if not is_resource_permissive(res))


def cfn_negative_statement(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.CFN_NEGATIVE_STATEMENT

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph

        for nid in iterate_iam_policy_resources(graph, method_supplies):
            yield from _negative_statement(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
