from lib_sast.root.f031.cloudformation.cfn_admin_policy_attached import (
    cfn_admin_policy_attached as _cfn_admin_policy_attached,
)
from lib_sast.root.f031.cloudformation.cfn_iam_excessive_role_policy import (
    cfn_iam_excessive_role_policy as _cfn_iam_excessive_role_policy,
)
from lib_sast.root.f031.cloudformation.cfn_iam_has_full_access_to_ssm import (
    cfn_iam_has_full_access_to_ssm as _cfn_iam_has_full_access_to_ssm,
)
from lib_sast.root.f031.cloudformation.cfn_iam_user_missing_role_based_security import (
    cfn_iam_user_missing_role_based_security as _cfn_iam_user_missing_role,
)
from lib_sast.root.f031.cloudformation.cfn_negative_statement import (
    cfn_negative_statement as _cfn_negative_statement,
)
from lib_sast.root.f031.terraform.terraform_admin_policy_attached import (
    tfm_admin_policy_attached as _tfm_admin_policy_attached,
)
from lib_sast.root.f031.terraform.terraform_iam_excessive_privileges import (
    tfm_iam_excessive_privileges as _tfm_iam_excessive_privileges,
)
from lib_sast.root.f031.terraform.terraform_negative_statement import (
    tfm_negative_statement as _tfm_negative_statement,
)
from lib_sast.root.f031.terraform.tfm_iam_excessive_role_policy import (
    tfm_iam_excessive_role_policy as _tfm_iam_excessive_role_policy,
)
from lib_sast.root.f031.terraform.tfm_iam_has_full_access_to_ssm import (
    tfm_iam_has_full_access_to_ssm as _tfm_iam_has_full_access_to_ssm,
)
from lib_sast.root.f031.terraform.tfm_iam_user_missing_role_based_security import (
    tfm_iam_user_missing_role_based_security as _tfm_iam_user_missing_role,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_admin_policy_attached(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_admin_policy_attached(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_iam_excessive_role_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_iam_excessive_role_policy(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_iam_has_full_access_to_ssm(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_iam_has_full_access_to_ssm(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_iam_user_missing_role_based_security(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_iam_user_missing_role(shard, method_supplies)


@SHIELD_BLOCKING
def cfn_negative_statement(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_negative_statement(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_admin_policy_attached(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_admin_policy_attached(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_iam_excessive_privileges(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_iam_excessive_privileges(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_iam_excessive_role_policy(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_iam_excessive_role_policy(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_iam_has_full_access_to_ssm(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_iam_has_full_access_to_ssm(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_iam_user_missing_role_based_security(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_iam_user_missing_role(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_negative_statement(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_negative_statement(shard, method_supplies)
