from utils.aws.iam import (
    match_pattern,
)


def match_iam_passrole(action: str) -> bool:
    return match_pattern(action, "iam:PassRole")


def action_has_full_access_to_ssm(actions: str | list) -> bool:
    actions_list = actions if isinstance(actions, list) else [actions]
    return any(action == "ssm:*" for action in actions_list)
