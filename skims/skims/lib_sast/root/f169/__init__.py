from lib_sast.root.f169.c_sharp import (
    c_sharp_plain_text_keys as _c_sharp_plain_text_keys,
)
from lib_sast.root.f169.python import (
    python_hc_aes_key as _python_hc_aes_key,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_plain_text_keys(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_plain_text_keys(shard, method_supplies)


@SHIELD_BLOCKING
def python_hc_aes_key(shard: GraphShard, method_supplies: MethodSupplies) -> MethodExecutionResult:
    return _python_hc_aes_key(shard, method_supplies)
