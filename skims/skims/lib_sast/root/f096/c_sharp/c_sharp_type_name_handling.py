from collections.abc import (
    Callable,
    Iterable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    is_any_library_imported,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.symbolic_eval.utils import (
    get_object_identifiers,
)
from lib_sast.utils.graph import (
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)

DANGER_HANDLING_MEMBERS = {
    "TypeNameHandling.All",
    "TypeNameHandling.Auto",
    "TypeNameHandling.Objects",
    "TypeNameHandling.Arrays",
}


def _member_access_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    ma_attr = args.graph.nodes[args.n_id]
    member_access = f"{ma_attr['expression']}.{ma_attr['member']}"
    args.evaluation[args.n_id] = member_access in DANGER_HANDLING_MEMBERS
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "member_access": _member_access_evaluator,
}


def _has_danger_handling_member(method: MethodsEnum, graph: Graph, member: NId) -> bool:
    if not (pred := pred_ast(graph, member)[0]):
        return False

    return (val_id := graph.nodes[pred].get("value_id")) and get_node_evaluation_results(
        method,
        graph,
        val_id,
        set(),
        method_evaluators=METHOD_EVALUATORS,
    )


def _is_type_handle_dangerous(
    method: MethodsEnum,
    graph: Graph,
    member: NId,
    obj_names: Iterable[str],
) -> bool:
    if (
        graph.nodes[member].get("label_type") == "MemberAccess"
        and graph.nodes[member].get("member") == "TypeNameHandling"
        and graph.nodes[member].get("expression") in obj_names
    ) or (
        graph.nodes[member].get("label_type") == "SymbolLookup"
        and graph.nodes[member].get("symbol") == "TypeNameHandling"
    ):
        return _has_danger_handling_member(method, graph, member)
    return False


def c_sharp_type_name_handling(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_TYPE_NAME_HANDLING

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported(graph, {"Newtonsoft.Json"}):
            return

        serial_objs = get_object_identifiers(graph, {"JsonSerializerSettings"})
        if not serial_objs:
            return

        for node in method_supplies.selected_nodes:
            if _is_type_handle_dangerous(method, graph, node, serial_objs):
                yield node

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
