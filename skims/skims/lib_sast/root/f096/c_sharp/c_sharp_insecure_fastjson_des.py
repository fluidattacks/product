from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    is_any_library_imported,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _is_bad_list_type_checking_false(graph: Graph, n_id: NId) -> bool:
    for assign_id in adj_ast(graph, n_id, label_type="Assignment"):
        if (
            (var_id := graph.nodes[assign_id].get("variable_id"))
            and graph.nodes[var_id].get("symbol", "") == "BadListTypeChecking"
            and (val_id := graph.nodes[assign_id].get("value_id"))
            and graph.nodes[val_id].get("value_type", "") == "bool"
            and graph.nodes[val_id].get("value", "") == "false"
        ):
            return True

    return False


def _object_creation_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    if (
        args.graph.nodes[args.n_id].get("name", "") == "JSONParameters"
        and (init_id := args.graph.nodes[args.n_id].get("initializer_id"))
        and _is_bad_list_type_checking_false(args.graph, init_id)
    ):
        args.triggers.add("insecure_fastjson_des")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "object_creation": _object_creation_evaluator,
}


def c_sharp_insecure_fastjson_des(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_INSECURE_FASTJSON_DES

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported(graph, {"fastJSON"}):
            return

        for n_id in method_supplies.selected_nodes:
            if (
                graph.nodes[n_id].get("expression", "") == "JSON.ToObject"
                and (second_arg := get_n_arg(graph, n_id, 1))
                and get_node_evaluation_results(
                    method,
                    graph,
                    second_arg,
                    {"insecure_fastjson_des"},
                    danger_goal=False,
                    method_evaluators=METHOD_EVALUATORS,
                )
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
