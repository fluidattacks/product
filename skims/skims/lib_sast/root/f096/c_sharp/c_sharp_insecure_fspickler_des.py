from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    is_any_library_imported,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    match_ast_group_d,
    pred_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _parameter_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id].get("variable_type") == "HttpRequest":
        args.evaluation[args.n_id] = True
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "parameter": _parameter_evaluator,
}


def _user_input_deserialize(
    graph: Graph,
    des_n_ids: set[NId],
    method: MethodsEnum,
) -> Iterator[NId]:
    for n_id in des_n_ids:
        if (f_arg := get_n_arg(graph, n_id, 0)) and get_node_evaluation_results(
            method,
            graph,
            f_arg,
            set(),
            method_evaluators=METHOD_EVALUATORS,
        ):
            yield f_arg


def _get_danger_deserialize(graph: Graph, n_id: NId) -> set[NId]:
    nodes = graph.nodes
    var_name = ""
    block_n_id: NId | None = None
    for parent_n_id in pred_ast(graph, n_id, -1):
        if nodes[parent_n_id].get("label_type") == "VariableDeclaration":
            var_name = nodes[parent_n_id].get("variable")
        if nodes[parent_n_id].get("label_type") == "ExecutionBlock":
            block_n_id = parent_n_id
            break
    if not (var_name and block_n_id):
        return set()
    return {
        n_id
        for n_id in match_ast_group_d(graph, block_n_id, "MethodInvocation", -1)
        if nodes[n_id].get("expression", "").startswith(f"{var_name}.Deserialize")
    }


def c_sharp_insecure_fspickler_des(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_INSECURE_FSPICKLER_DES

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        if not is_any_library_imported(graph, {"MBrace.FsPickler.Json"}):
            return

        for n_id in method_supplies.selected_nodes:
            if graph.nodes[n_id].get("expression", "") == "FsPickler.CreateJsonSerializer":
                deserialize_n_ids = _get_danger_deserialize(graph, n_id)
                yield from _user_input_deserialize(graph, deserialize_n_ids, method)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
