from lib_sast.root.f096.c_sharp.c_sharp_check_xml_serializer import (
    c_sharp_check_xml_serializer as _c_sharp_check_xml_serializer,
)
from lib_sast.root.f096.c_sharp.c_sharp_insecure_deserialization import (
    c_sharp_insecure_deserialization as _c_sharp_insecure_deserialization,
)
from lib_sast.root.f096.c_sharp.c_sharp_insecure_fastjson_des import (
    c_sharp_insecure_fastjson_des as _c_sharp_insecure_fastjson_des,
)
from lib_sast.root.f096.c_sharp.c_sharp_insecure_fspickler_des import (
    c_sharp_insecure_fspickler_des as _c_sharp_insecure_fspickler_des,
)
from lib_sast.root.f096.c_sharp.c_sharp_js_deserialization import (
    c_sharp_js_deserialization as _c_sharp_js_deserialization,
)
from lib_sast.root.f096.c_sharp.c_sharp_type_name_handling import (
    c_sharp_type_name_handling as _c_sharp_type_name_handling,
)
from lib_sast.root.f096.php.php_insecure_deserialization import (
    php_insecure_deserialization as _php_insecure_deserialization,
)
from lib_sast.root.f096.python.python_deserialization_injection import (
    python_deserialization_injection as _python_deserialization_injection,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def c_sharp_check_xml_serializer(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_check_xml_serializer(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_insecure_deserialization(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_insecure_deserialization(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_insecure_fastjson_des(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_insecure_fastjson_des(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_insecure_fspickler_des(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_insecure_fspickler_des(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_js_deserialization(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_js_deserialization(shard, method_supplies)


@SHIELD_BLOCKING
def c_sharp_type_name_handling(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _c_sharp_type_name_handling(shard, method_supplies)


@SHIELD_BLOCKING
def python_deserialization_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _python_deserialization_injection(shard, method_supplies)


@SHIELD_BLOCKING
def php_insecure_deserialization(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _php_insecure_deserialization(shard, method_supplies)
