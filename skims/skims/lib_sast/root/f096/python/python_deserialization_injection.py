from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.common import (
    PYTHON_INPUTS,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils import (
    graph as g,
)
from model.core import (
    MethodExecutionResult,
)


def _member_access_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    n_attrs = args.graph.nodes[args.n_id]
    expr = f"{n_attrs['expression']}.{n_attrs['member']}"

    if expr in PYTHON_INPUTS:
        args.evaluation[args.n_id] = True
        args.triggers.add("userparams")
    elif expr == "yaml.Loader":
        args.evaluation[args.n_id] = True
        args.triggers.add("dangerloader")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "member_access": _member_access_evaluator,
}


def _is_danger_yaml_loader(graph: Graph, n_ids: list[NId], method: MethodsEnum) -> bool:
    for _id in n_ids:
        n_attrs = graph.nodes[_id]
        if n_attrs["label_type"] != "NamedArgument" or n_attrs["argument_name"] != "Loader":
            continue
        val_id = n_attrs["value_id"]
        return get_node_evaluation_results(
            method,
            graph,
            val_id,
            {"dangerloader"},
            method_evaluators=METHOD_EVALUATORS,
        )

    return False


def _is_danger_expression(graph: Graph, n_id: NId, method: MethodsEnum) -> bool:
    n_attrs = graph.nodes[n_id]
    expr = n_attrs["expression"]
    al_id = n_attrs.get("arguments_id")
    if not al_id:
        return False

    args_ids = list(g.adj_ast(graph, al_id))
    if len(args_ids) < 1:
        return False

    return (
        expr == "pickle.load"
        or (
            expr == "yaml.load"
            and len(args_ids) > 1
            and _is_danger_yaml_loader(graph, args_ids[1:], method)
        )
    ) and get_node_evaluation_results(
        method,
        graph,
        args_ids[0],
        {"userparams"},
        method_evaluators=METHOD_EVALUATORS,
    )


def python_deserialization_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PYTHON_DESERIALIZATION_INJECTION

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if _is_danger_expression(graph, n_id, method):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
