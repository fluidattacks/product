from collections.abc import (
    Callable,
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
    is_sanitized,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.common import (
    PHP_INPUTS,
)
from lib_sast.symbolic_eval.evaluate import (
    evaluate,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.symbolic_eval.utils import get_backward_paths
from model.core import (
    MethodExecutionResult,
)


def _element_access_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    nodes = args.graph.nodes
    if (exp_n_id := nodes[args.n_id].get("expression_id")) and (
        nodes[exp_n_id].get("symbol") in PHP_INPUTS
    ):
        args.evaluation[args.n_id] = True
        args.triggers.add(args.n_id)

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "element_access": _element_access_evaluator,
}


def _is_vuln(graph: Graph, method: MethodsEnum, n_id: NId) -> bool:
    for path in get_backward_paths(graph, n_id):
        if (
            evaluation := evaluate(
                method,
                graph,
                path,
                n_id,
                None,
                METHOD_EVALUATORS,
            )
        ) and evaluation.danger:
            danger_source = next(iter(evaluation.triggers), None)
            return not is_sanitized(graph, n_id, danger_source)
    return False


def php_insecure_deserialization(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.PHP_INSECURE_DESERIALIZATION
    danger_expression = "unserialize"

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for n_id in method_supplies.selected_nodes:
            if (
                (graph.nodes[n_id].get("expression") == danger_expression)
                and (first_arg := get_n_arg(graph, n_id, 0))
                and _is_vuln(graph, method, first_arg)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
