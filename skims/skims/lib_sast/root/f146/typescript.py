from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids_metadata,
)
from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.root.utilities.javascript import (
    file_imports_module,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.utils import (
    graph as g,
)
from model import (
    core,
)


def analyze_options_arg(graph: Graph, options_arg_n_id: NId | None) -> bool:
    nodes = graph.nodes

    if not options_arg_n_id:
        return False

    has_replacements_or_bind = False

    for pair_n_id in g.match_ast_group_d(graph, options_arg_n_id, "Pair", -1):
        key_n_id = nodes[pair_n_id].get("key_id")
        if nodes[key_n_id].get("symbol") in {"replacements", "bind"}:
            has_replacements_or_bind = True
            break

    return has_replacements_or_bind


def get_first_level_n_ids(
    graph: Graph,
    current_n_id: NId,
    matches: set[NId],
    label_type: str,
) -> set[NId]:
    nodes = graph.nodes
    for n_id in g.adj_ast(graph, current_n_id):
        if nodes[n_id].get("label_type") == label_type:
            matches.add(n_id)
        else:
            matches.update(get_first_level_n_ids(graph, n_id, matches, label_type))
    return matches


def is_directly_sanitized(graph: Graph, child_n_id: NId, parent_n_id: NId) -> bool:
    nodes = graph.nodes

    not_sanitizing_invocations = {"hash"}

    sanitized = False
    for n_id in g.pred_ast(graph, child_n_id, -1):
        if n_id == parent_n_id:
            break
        if (
            nodes[n_id].get("label_type") == "MethodInvocation"
            and (expression := nodes[n_id].get("expression"))
            and expression.split(".", maxsplit=1)[-1] not in not_sanitizing_invocations
        ):
            sanitized = True
            break

    return sanitized


def get_vuln_nodes(
    graph: Graph,
    sql_n_id: NId,
    method: MethodsEnum,
    *,
    has_replacements: bool,
) -> set[NId]:
    vuln_nodes: set[NId] = set()

    if has_replacements:
        return vuln_nodes

    for access_n_id in get_first_level_n_ids(graph, sql_n_id, set(), "MemberAccess"):
        if not is_directly_sanitized(graph, access_n_id, sql_n_id) and get_node_evaluation_results(
            method,
            graph,
            access_n_id,
            set(),
        ):
            vuln_nodes.add(access_n_id)
    return vuln_nodes


def ts_sequelize_injection(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> core.MethodExecutionResult:
    method = MethodsEnum.TS_SEQUELIZE_INJECTION

    def n_ids() -> Iterator[tuple[NId, dict]]:
        graph = shard.syntax_graph

        if not file_imports_module(graph, "express"):
            return

        vuln_n_ids: set[NId] = set()
        for n_id in method_supplies.selected_nodes:
            if (graph.nodes[n_id].get("expression", "").endswith("sequelize.query")) and (
                sql_arg_n_id := get_n_arg(graph, n_id, 0)
            ):
                options_arg_n_id = get_n_arg(graph, n_id, 1)

                has_replacements = analyze_options_arg(graph, options_arg_n_id)
                vuln_n_ids.update(
                    get_vuln_nodes(graph, sql_arg_n_id, method, has_replacements=has_replacements),
                )
        for vuln_n_id in vuln_n_ids:
            n_attrs = graph.nodes[vuln_n_id]
            injection_var_name = f"{n_attrs['expression']}.{n_attrs['member']}"
            yield vuln_n_id, {"var_name": injection_var_name}

    return get_vulnerabilities_from_n_ids_metadata(
        n_ids_metadata=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
