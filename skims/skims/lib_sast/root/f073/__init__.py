from lib_sast.root.f073.cloudformation.cfn_rds_is_publicly_accessible import (
    cfn_rds_is_publicly_accessible as _cfn_rds_is_publicly_accessible,
)
from lib_sast.root.f073.terraform.tfm_db_instance_publicly_accessible import (
    tfm_rds_publicly_accessible as _tfm_rds_publicly_accessible,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_rds_is_publicly_accessible(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_rds_is_publicly_accessible(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_rds_publicly_accessible(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_rds_publicly_accessible(shard, method_supplies)
