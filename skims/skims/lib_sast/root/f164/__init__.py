from lib_sast.root.f164.conf_files.json_ssl_port_missing import (
    ssl_port_missing as _json_ssl_port_missing,
)
from lib_sast.root.f164.java.java_rpc_enabled_extensions import (
    java_rpc_enabled_extensions as _java_rpc_enabled_extensions,
)
from lib_sast.root.f164.java.java_saml_ignore_comments import (
    java_saml_ignore_comments as _java_saml_ignore_comments,
)
from lib_sast.root.f164.java.java_wicket_string_escaping_disabled import (
    java_wicket_string_escaping_disabled as _java_wicket_string_escaping_disabled,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def json_ssl_port_missing(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _json_ssl_port_missing(shard, method_supplies)


@SHIELD_BLOCKING
def java_rpc_enabled_extensions(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_rpc_enabled_extensions(shard, method_supplies)


@SHIELD_BLOCKING
def java_wicket_string_escaping_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_wicket_string_escaping_disabled(shard, method_supplies)


@SHIELD_BLOCKING
def java_saml_ignore_comments(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _java_saml_ignore_comments(shard, method_supplies)
