from lib_sast.root.f099.cloudformation import (
    cfn_bucket_server_side_encryption_disabled as _cfn_bucket_sse_disabled,
)
from lib_sast.root.f099.terraform import (
    tfm_bucket_server_side_encryption_disabled as _tfm_bucket_sse_disabled,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from lib_sast.sast_model import (
    GraphShard,
    MethodSupplies,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def cfn_bucket_server_side_encryption_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _cfn_bucket_sse_disabled(shard, method_supplies)


@SHIELD_BLOCKING
def tfm_bucket_server_side_encryption_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    return _tfm_bucket_sse_disabled(shard, method_supplies)
