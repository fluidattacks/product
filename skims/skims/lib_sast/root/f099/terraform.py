from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.root.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.root.utilities.terraform import (
    get_dict_from_attr,
    get_dict_values,
    get_optional_attribute,
)
from lib_sast.sast_model import (
    Graph,
    GraphShard,
    MethodSupplies,
    NId,
    QuerySupplies,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def _bucket_policy_in_jsonencode(graph: Graph, nid: NId) -> Iterator[NId]:
    child_id = adj_ast(graph, graph.nodes[nid]["arguments_id"])[0]
    if stmts := get_optional_attribute(graph, child_id, "Statement"):
        value_id = graph.nodes[stmts[2]]["value_id"]
        for c_id in adj_ast(graph, value_id, label_type="Object"):
            effect = get_optional_attribute(graph, c_id, "Effect")
            if (
                not effect
                or effect[1] != "Allow"
                or not (condition := get_optional_attribute(graph, c_id, "Condition"))
            ):
                continue

            c_val_id = graph.nodes[condition[2]]["value_id"]
            null = get_optional_attribute(graph, c_val_id, "Null")
            if (
                null
                and (n_val_id := graph.nodes[null[2]]["value_id"])
                and (
                    ss_enc := get_optional_attribute(
                        graph,
                        n_val_id,
                        "s3:x-amz-server-side-encryption",
                    )
                )
                and ss_enc[1] == "false"
            ):
                yield ss_enc[2]


def _bucket_policy_in_literal(attr_val: str, attr_id: NId) -> Iterator[NId]:
    dict_value = get_dict_from_attr(attr_val)
    statements = get_dict_values(dict_value, "Statement")
    if not isinstance(statements, list):
        return

    for stmt in statements:
        effect = stmt.get("Effect")
        if effect != "Allow":
            continue

        if (
            (condition := stmt.get("Condition"))
            and (null := condition.get("Null"))
            and not null.get("s3:x-amz-server-side-encryption", True)
        ):
            yield attr_id


def _bucket_policy_sse_disabled(graph: Graph, nid: NId) -> Iterator[NId]:
    if policy := get_optional_attribute(graph, nid, "policy"):
        value_id = graph.nodes[policy[2]]["value_id"]
        if graph.nodes[value_id]["label_type"] == "Literal":
            yield from _bucket_policy_in_literal(policy[1], policy[2])
        elif (
            graph.nodes[value_id]["label_type"] == "MethodInvocation"
            and graph.nodes[value_id]["expression"] == "jsonencode"
        ):
            yield from _bucket_policy_in_jsonencode(graph, value_id)


def tfm_bucket_server_side_encryption_disabled(
    shard: GraphShard,
    method_supplies: MethodSupplies,
) -> MethodExecutionResult:
    method = MethodsEnum.TFM_BUCKET_POLICY_HAS_SERVER_SIDE_ENCRYPTION_DISABLED

    def n_ids() -> Iterator[NId]:
        graph = shard.syntax_graph
        for nid in method_supplies.selected_nodes:
            if graph.nodes[nid].get("name") == "aws_s3_bucket_policy":
                yield from _bucket_policy_sse_disabled(graph, nid)

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        query_supplies=QuerySupplies(
            method=method,
            method_calls=len(method_supplies.selected_nodes),
            graph_shard=shard,
        ),
    )
