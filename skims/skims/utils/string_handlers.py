import os
import re
import time
from datetime import datetime, timedelta, timezone

from bs4 import (
    NavigableString,
    Tag,
)
from ctx import (
    STATE_FOLDER_DEBUG,
)
from fluidattacks_core.serializers.snippet import (
    Snippet,
    SnippetViewport,
    make_snippet,
    make_snippet_function,
)
from utils.logs import (
    log_blocking,
)


def get_debug_path(path: str) -> str:
    output = os.path.join(  # noqa: PTH118
        STATE_FOLDER_DEBUG,
        os.path.relpath(path).replace("/", "__").replace(".", "_"),
    )
    log_blocking("info", "An output will be generated at %s*", output)
    return output


def get_datetime_with_offset(timestamp: float) -> str:
    local_time = time.localtime(timestamp)
    offset_sec = -(time.altzone if local_time.tm_isdst > 0 and time.daylight else time.timezone)
    tz = timezone(timedelta(seconds=offset_sec))
    dt = datetime.fromtimestamp(timestamp, tz)
    offset_str = dt.strftime("%z")
    formatted_offset = f"UTC{offset_str[:3]}:{offset_str[3:]}"

    return dt.strftime("%Y-%m-%d %H:%M:%S") + f" ({formatted_offset})"


def get_duration(init_time: float, end_time: float) -> str:
    total_seconds = int(end_time - init_time)
    hours, remainder = divmod(total_seconds, 3600)
    minutes, seconds = divmod(remainder, 60)

    message = ""
    if hours:
        message += f"{hours:02} h "
    if minutes:
        message += f"{minutes:02} min"

    return message + f" {seconds:02} sec"


def split_on_last_dot(string: str) -> tuple[str, str]:
    portions = string.rsplit(".", maxsplit=1)
    if len(portions) == 2:
        return portions[0], portions[1]
    return portions[0], ""


def is_exclusion(content: str, line: int) -> bool:
    is_suppression = False
    comment: str = ""
    lines = content.split("\n")
    if line > 1 and lines and "NOFLUID" in lines[line - 2]:
        parts = lines[line - 2].split("NOFLUID", 1)
        comment = parts[1].strip()
        if len(comment) > 2:
            is_suppression = True
    return is_suppression


def filter_valid_urls(urls: tuple[str, ...]) -> list[str]:
    url_pattern = re.compile(
        r"^(https?://|ftps://|sftp://|ldaps://|wss://)?"
        r"(localhost|[\w\-\.]+(\.[a-zA-Z]{2,})?)(:[0-9]+)?(/[^\s]*)?$",
    )
    return [url for url in urls if url_pattern.match(url)]


def get_numbers(tag: Tag | NavigableString) -> tuple[int, int]:
    line_no: int = tag.sourceline if tag.sourceline else 1
    col_no: int = tag.sourcepos if tag.sourcepos else 0
    return line_no, col_no


def make_snippet_func(
    *,
    file_content: str,
    line: int,
    column: int | None,
    path: str,
) -> Snippet:
    snippet = make_snippet_function(
        file_content=file_content,
        file_path=path,
        viewport=SnippetViewport(int(line), column),
    )
    if not snippet:
        return make_snippet(
            content=file_content,
            viewport=SnippetViewport(int(line), column, line_context=8),
        )

    return snippet
