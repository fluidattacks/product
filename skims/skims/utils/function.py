import functools
import sys
import traceback
from asyncio import (
    sleep,
)
from collections.abc import (
    Callable,
)
from time import (
    sleep as sleep_blocking,
)
from typing import (
    Any,
    TypeVar,
    cast,
)

from utils.logs import (
    log,
    log_blocking,
    log_to_remote,
    log_to_remote_blocking,
)

Tfun = TypeVar("Tfun", bound=Callable[..., Any])


def get_id(function: Tfun) -> str:
    if isinstance(function, functools.partial):  # noqa: SIM108
        function_attributes = function.func
    else:
        function_attributes = function

    return f"{function_attributes.__module__}.{function_attributes.__name__}"


def get_path(
    args: Any,  # noqa: ANN401
    kwargs: Any,  # noqa: ANN401
    get_path_from_attrs: Callable[..., str] | None = None,
) -> str:
    path = ""
    if get_path_from_attrs:
        if kwargs == {}:  # noqa: SIM108
            path = get_path_from_attrs(args)
        else:
            path = get_path_from_attrs(args, kwargs)
    return path


def shield(
    *,
    on_error_return: object | None,
    on_exceptions: tuple[type[BaseException], ...] = (BaseException,),
    attempts: int = 1,
    sleep_between_retries: int = 0,
    get_path_from_attrs: Callable[..., str] | None = None,
) -> Callable[[Tfun], Tfun]:
    """Decorate a function to catch all errors.

    A customizable decorator that prevents exceptions raised in a function
    from stopping the execution of the entire program.
    Same as `shield_blocking` decorator but for asynchronous functions
    """

    def decorator(function: Tfun) -> Tfun:
        @functools.wraps(function)
        async def wrapper(*args: Any, **kwargs: Any) -> Any:  # noqa: ANN401
            for try_attempt in range(1, attempts + 1):  # noqa: RET503
                try:
                    return await function(*args, **kwargs)
                except on_exceptions:
                    function_id = get_id(function)
                    path = get_path(args, kwargs, get_path_from_attrs)
                    exc_type, exc_value, exc_traceback = sys.exc_info()

                    await log(
                        "warning",
                        "Function %s failed analyzing %s with error %s\n%s",
                        function_id,
                        path,
                        exc_value,
                        traceback.format_exc(),
                    )

                    if try_attempt == attempts:
                        await log_to_remote(
                            msg=(exc_type, exc_value, exc_traceback),
                            severity="error",
                            path=path,
                            function_id=function_id,
                        )
                        return on_error_return

                    await log("info", "retry #%s: %s", try_attempt, function_id)
                    await sleep(sleep_between_retries)

        return cast(Tfun, wrapper)

    return decorator


def shield_blocking(
    *,
    on_error_return: object | None,
    on_exceptions: tuple[type[BaseException], ...] = (BaseException,),
    attempts: int = 1,
    sleep_between_retries: int = 0,
    get_path_from_attrs: Callable[..., str] | None = None,
) -> Callable[[Tfun], Tfun]:
    """Decorate a sync function to catch all errors.

    A customizable decorator that prevents exceptions raised in a function
    from stopping the execution of the entire program.

    Args:
        on_error_return: Default value to return when
        wrapped function raises an exception
        on_exceptions: Exceptions types to catch.
        attempts: Number of attempts to try and execute the function
        sleep_between_retries: Seconds to wait between retries
        get_path_from_attrs: Customizable function to get the path of
        the function being executed (For logging purposes)

    Returns:
        The decorator to wrap around any function

    """

    def decorator(function: Tfun) -> Tfun:
        @functools.wraps(function)
        def wrapper(
            *args: Any,  # noqa: ANN401
            **kwargs: Any,  # noqa: ANN401
        ) -> Any:  # noqa: ANN401
            for try_attempt in range(1, attempts + 1):  # noqa: RET503
                try:
                    return function(*args, **kwargs)
                except on_exceptions:
                    function_id = get_id(function)
                    path = get_path(args, kwargs, get_path_from_attrs)
                    exc_type, exc_value, exc_traceback = sys.exc_info()
                    log_blocking(
                        "warning",
                        "Function %s failed analyzing %s with error %s\n%s",
                        function_id,
                        path,
                        exc_value,
                        traceback.format_exc(),
                    )

                    if try_attempt == attempts:
                        log_to_remote_blocking(
                            msg=(exc_type, exc_value, exc_traceback),
                            severity="error",
                            path=path,
                            function_id=function_id,
                        )
                        return on_error_return

                    log_blocking("info", "retry #%s: %s", try_attempt, function_id)
                    sleep_blocking(sleep_between_retries)

        return cast(Tfun, wrapper)

    return decorator
