import json
from enum import (
    Enum,
)
from typing import (
    Any,
)


def simplify(obj: Any) -> Any:  # noqa: ANN401
    simplified_obj: Any
    if hasattr(obj, "_fields"):
        # NamedTuple
        simplified_obj = dict(zip(simplify(obj._fields), simplify(tuple(obj)), strict=False))
    elif isinstance(obj, Enum):
        simplified_obj = obj.value
    elif isinstance(obj, dict):
        simplified_obj = dict(
            zip(simplify(tuple(obj.keys())), simplify(tuple(obj.values())), strict=False),
        )
    elif isinstance(obj, (list | tuple | set)):
        simplified_obj = tuple(map(simplify, obj))
    else:
        simplified_obj = obj

    return simplified_obj


def json_dumps(element: object, *args: Any, **kwargs: Any) -> str:  # noqa: ANN401
    return json.dumps(simplify(element), *args, **kwargs)
