import logging
import sys
from os import (
    environ,
)
from types import (
    TracebackType,
)
from typing import (
    Any,
)

import bugsnag
import ctx
from aioextensions import (
    in_thread,
)
from coralogix.handlers import (
    CoralogixLogger,
)
from utils.bugs import (
    META as BUGS_META,
)
from utils.env import (
    is_fluid_batch_env,
)

MAX_DESC_LENGTH = 20

# Private constants
_FORMAT: str = "[%(levelname)s] %(message)s"

# There is no problem in making this key public
# it's intentional so we can monitor Skims stability in remote users
_CORALOGIX_PRIVATE_KEY = "cxtp_hRa3KPs2UNKZLDuo6Oz1KEzY6CN3zH"
_APP_NAME = "skims-prod" if is_fluid_batch_env() else "skims-dev"
_SUB_SYSTEM = "skims-cli-prod" if is_fluid_batch_env() else "skims-cli-dev"


_LOGGER_FORMATTER: logging.Formatter = logging.Formatter(_FORMAT)

_LOGGER_HANDLER: logging.StreamHandler = logging.StreamHandler()
_LOGGER: logging.Logger = logging.getLogger("Skims")

_LOGGER_REMOTE_HANDLER = bugsnag.handlers.BugsnagHandler()
_LOGGER_REMOTE: logging.Logger = logging.getLogger("Skims.stability")

_LOGGER_CORALOGIX_HANDLER = CoralogixLogger(
    _CORALOGIX_PRIVATE_KEY,
    _APP_NAME,
    "skims-cli-ci" if environ.get("CI") and not is_fluid_batch_env() else _SUB_SYSTEM,
)


def configure() -> None:
    _LOGGER_HANDLER.setStream(sys.stdout)
    _LOGGER_HANDLER.setLevel(logging.INFO)
    _LOGGER_HANDLER.setFormatter(_LOGGER_FORMATTER)

    _LOGGER.setLevel(logging.INFO)
    _LOGGER.addHandler(_LOGGER_HANDLER)

    _LOGGER_REMOTE_HANDLER.setLevel(logging.ERROR)

    _LOGGER_REMOTE.setLevel(logging.ERROR)
    _LOGGER_REMOTE.addHandler(_LOGGER_REMOTE_HANDLER)


def set_tracing_handler() -> None:
    _LOGGER.addHandler(_LOGGER_CORALOGIX_HANDLER)


def set_level(level: int) -> None:
    _LOGGER.setLevel(level)
    _LOGGER_HANDLER.setLevel(level)


def log_blocking(level: str, msg: str, *args: Any, **kwargs: Any) -> None:  # noqa: ANN401
    getattr(_LOGGER, level)(msg, *args, **kwargs)


async def log(level: str, msg: str, *args: Any, **kwargs: Any) -> None:  # noqa: ANN401
    await in_thread(log_blocking, level, msg, *args, **kwargs)


def log_to_remote_blocking(
    *,
    msg: (
        str
        | Exception
        | tuple[
            type[BaseException] | None,
            BaseException | None,
            TracebackType | None,
        ]
    ),
    severity: str,  # info, error, warning
    **meta_data: str,
) -> None:
    if environ.get("CI_COMMIT_REF_NAME", "trunk") == "trunk" and (
        hasattr(ctx, "SKIMS_CONFIG") and not ctx.SKIMS_CONFIG.tracing_opt_out
    ):
        meta_data.update(BUGS_META)
        bugsnag.notify(
            Exception(msg) if isinstance(msg, str) else msg,  # type: ignore[arg-type]
            meta_data=dict(meta_data),
            severity=severity,
            unhandled=is_fluid_batch_env(),
        )


def log_to_remote_handled(
    *,
    msg: str,
    severity: str,
) -> None:
    if is_fluid_batch_env():
        bugsnag.notify(
            Exception(msg),
            meta_data=BUGS_META,
            severity=severity,
            unhandled=False,
        )
    elif environ.get("CI_COMMIT_REF_NAME", "trunk") != "trunk":
        log_blocking("error", msg)


async def log_to_remote(
    *,
    msg: (
        str
        | Exception
        | tuple[
            type[BaseException] | None,
            BaseException | None,
            TracebackType | None,
        ]
    ),
    severity: str,  # info, error, warning
    **meta_data: str,
) -> None:
    await in_thread(
        log_to_remote_blocking,
        msg=msg,
        severity=severity,
        **meta_data,
    )
