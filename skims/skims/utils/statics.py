from pathlib import (
    Path,
)

from utils.env import (
    is_fluid_batch_env,
)


def calculate_time_coefficient(elapsed_time: float, path: str, method_calls: int) -> float | None:
    if is_fluid_batch_env():
        return ((float(Path(path).stat().st_size) * elapsed_time) / method_calls) * 1000
    return None
