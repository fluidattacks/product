class CustomBaseError(Exception):
    pass


class _SingleMessageError(CustomBaseError):
    msg: str

    @classmethod
    def new(cls) -> "_SingleMessageError":
        return cls(cls.msg)


class UnavailabilityError(_SingleMessageError):
    msg = "AWS service unavailable, please retry"
