import json
from contextlib import (
    AsyncExitStack,
)
from tempfile import (
    NamedTemporaryFile,
)
from typing import (
    Any,
)

import aioboto3
from aiobotocore.config import (
    AioConfig,
)
from botocore import (
    UNSIGNED,
)
from botocore.exceptions import (
    ClientError,
)
from ctx import (
    AWS_REGION_NAME,
)
from utils.custom_exceptions import (
    UnavailabilityError,
)
from utils.logs import (
    log_blocking,
)

RESOURCE_OPTIONS = {
    "config": AioConfig(
        connect_timeout=15,
        max_pool_connections=2000,
        read_timeout=30,
        retries={"max_attempts": 10, "mode": "standard"},
        signature_version="s3v4",
    ),
    "region_name": AWS_REGION_NAME,
    "service_name": "s3",
    "use_ssl": True,
    "verify": True,
}
PUBLIC_RESOURCE_OPTIONS = {
    "config": AioConfig(
        connect_timeout=15,
        max_pool_connections=2000,
        read_timeout=30,
        retries={"max_attempts": 10, "mode": "standard"},
        signature_version=UNSIGNED,
    ),
    "region_name": AWS_REGION_NAME,
    "service_name": "s3",
    "use_ssl": True,
    "verify": True,
}


class Config:
    session: aioboto3.Session = aioboto3.Session()
    context_stack: AsyncExitStack | None = None
    resource: Any = None


async def s3_start_resource(*, is_public: bool = False) -> None:
    Config.context_stack = AsyncExitStack()

    Config.resource = await Config.context_stack.enter_async_context(
        Config.session.client(**(PUBLIC_RESOURCE_OPTIONS if is_public else RESOURCE_OPTIONS)),
    )


async def s3_shutdown() -> None:
    if Config.context_stack:
        await Config.context_stack.aclose()


async def get_s3_resource() -> Any:  # noqa: ANN401
    if Config.resource is None:
        await s3_start_resource()

    return Config.resource


async def upload_object(file_name: str, dict_object: dict[str, Any], bucket: str) -> None:
    try:
        client = await get_s3_resource()
        await client.put_object(
            Body=json.dumps(dict_object, indent=2, sort_keys=True),
            Bucket=bucket,
            Key=file_name,
        )
        log_blocking("info", "Added file: %s", file_name)
    except ClientError as exc:
        raise UnavailabilityError from exc


async def download_json_fileobj(
    bucket: str,
    file_name: str,
) -> dict[str, dict[str, dict[str, Any]]]:
    return_value: dict[str, dict[str, dict[str, Any]]] = {}
    with NamedTemporaryFile() as temp:
        try:
            client = await get_s3_resource()
            await client.download_fileobj(
                bucket,
                file_name,
                temp,
            )
            temp.seek(0)
            return_value = json.loads(temp.read().decode(encoding="utf-8"))
        except ValueError as exc:
            log_blocking("error", "%s", exc)
        except ClientError:
            return {}
        return return_value
