import os
import re
from fnmatch import (
    fnmatch,
    translate,
)
from pathlib import (
    Path,
)
from typing import (
    NamedTuple,
)

import ctx
from utils.cloudwatch import (
    send_metrics_to_cloudwatch,
)
from utils.env import (
    is_fluid_batch_env,
)
from utils.logs import (
    log_blocking,
)

MAX_FILE_SIZE: int = 1024 * 500


class FileTooLargeError(Exception):
    pass


class Paths(NamedTuple):
    ok_paths: tuple[str, ...]
    nu_paths: tuple[str, ...]
    nv_paths: tuple[str, ...]


def get_file_content_block(path: str, encoding: str = "utf-8-sig", size: int = -1) -> str:
    with Path(path).open(encoding=encoding, errors="ignore") as file_handle:
        file_contents: str = file_handle.read(size)
        return file_contents


def get_file_raw_content_blocking(path: str, size: int = -1) -> bytes:
    with Path(path).open(mode="rb") as file_handle:
        file_contents: bytes = file_handle.read(size)
        return file_contents


def sync_get_file_raw_content(path: str, size: int = MAX_FILE_SIZE) -> tuple[bytes, str]:
    if ctx.SKIMS_CONFIG.file_size_limit and os.stat(path).st_size > MAX_FILE_SIZE:  # noqa: PTH116
        raise FileTooLargeError(path)

    with Path(path).open("rb") as handle:
        content = handle.read(size)
        content_str = content.decode("utf-8", errors="ignore")

    return content, content_str


def safe_sync_get_file_raw_content(
    path: str,
    size: int = MAX_FILE_SIZE,
) -> tuple[bytes, str] | None:
    try:
        return sync_get_file_raw_content(path, size)
    except FileTooLargeError:
        log_blocking("warning", "File too large: %s, ignoring", path)
        return None


def check_dependency_code(path: str) -> bool:
    if not path.endswith((".js", ".jsx")):
        return False

    regex_patterns = [
        re.compile(r"jQuery.*Copyright.*License", re.IGNORECASE),
        re.compile(r"Copyright.*License.*jQuery", re.IGNORECASE),
        re.compile(r"AngularJS.*Google.*License", re.IGNORECASE),
    ]

    content = get_file_content_block(path=path, encoding="latin-1", size=200).replace("\n", " ")

    return any(pattern.search(content) for pattern in regex_patterns)


def get_non_upgradable_paths(paths: set[str]) -> set[str]:
    nu_paths: set[str] = set()

    intellisense_refs = {
        os.path.dirname(path)  # noqa: PTH120
        for path in paths
        if path.endswith("Scripts/_references.js")
    }

    for path in paths:
        if (
            any(path.startswith(intellisense_ref) for intellisense_ref in intellisense_refs)
            or any(
                fnmatch(f"/{path}", glob)
                for glob in (
                    "*/Assets*/vendor/*",
                    "*/Assets*/lib/*",
                    "*/Assets*/js/*",
                    "*/Content*/jquery*",
                    "*/GoogleMapping*.js",
                    "*/Scripts*/bootstrap*",
                    "*/Scripts*/modernizr*",
                    "*/Scripts*/jquery*",
                    "*/Scripts*/popper*",
                    "*/Scripts*/vue*",
                    "*/wwwroot/lib*",
                )
            )
            or check_dependency_code(path)
        ):
            nu_paths.add(path)

    return nu_paths


def get_non_verifiable_paths(paths: set[str]) -> set[str]:
    nv_paths: set[str] = set()
    binary_extensions = {
        "aar",
        "apk",
        "bin",
        "class",
        "dll",
        "DS_Store",
        "exe",
        "exec",
        "hprof",
        "jar",
        "jasper",
        "pdb",
        "pyc",
        "wasm",
    }

    for path in paths:
        file_info = os.path.basename(path)  # noqa: PTH119
        _, file_extension = os.path.splitext(file_info)  # noqa: PTH122
        file_extension = file_extension[1:]

        if file_extension in binary_extensions or path.endswith(".min.js"):
            nv_paths.add(path)

    return nv_paths


def mkdir(*, name: str, mode: int = 0o777, exist_ok: bool = False) -> None:
    return os.makedirs(name, mode=mode, exist_ok=exist_ok)  # noqa: PTH103


def _normalize_rules(rules: tuple[str, ...]) -> tuple[str, ...]:
    normalized_rules: list[str] = []
    for rule in rules:
        normalized_rule = rule
        if normalized_rule == ".":
            normalized_rule = "glob(**)"
        elif normalized_rule.endswith("/"):
            normalized_rule += "**"

        if not normalized_rule.startswith("glob"):
            normalized_rule = f"glob({normalized_rule})"

        normalized_rules.append(normalized_rule)

    return tuple(normalized_rules)


def list_paths(
    *,
    working_dir: str,
    include: tuple[str, ...],
    exclude: tuple[str, ...],
    relative: bool = True,
) -> set[str]:
    excluded_dirs = [".git", "node_modules"]

    include_regex = [
        translate(os.path.join(working_dir, glob[5:-1]))  # noqa: PTH118
        for glob in _normalize_rules(include)
    ]
    exclude_regex = [
        translate(os.path.join(working_dir, glob[5:-1]))  # noqa: PTH118
        for glob in _normalize_rules(exclude)
    ]

    paths = set()
    for dirpath, _, files in os.walk(working_dir):
        if any(
            (
                dirpath.endswith(excluded_dir)
                or f"{os.path.sep}{excluded_dir}{os.path.sep}" in dirpath
            )
            for excluded_dir in excluded_dirs
        ):
            continue

        for file in files:
            filepath = os.path.join(dirpath, file)  # noqa: PTH118
            if any(re.match(regex, filepath) for regex in include_regex) and not any(
                re.match(regex, filepath) for regex in exclude_regex
            ):
                if relative:
                    filepath = filepath.split(working_dir + os.path.sep)[1]
                paths.add(filepath)

    return paths


def split_by_upgradable(paths: set[str]) -> tuple[set[str], set[str]]:
    try:
        nu_paths = get_non_upgradable_paths(paths)
        return nu_paths, paths - nu_paths
    except FileNotFoundError as exc:
        exc_log = f"File does not exist: {exc.filename}"
        raise SystemExit(exc_log) from exc


def split_by_verifiable(paths: set[str]) -> tuple[set[str], set[str]]:
    try:
        nv_paths = get_non_verifiable_paths(paths)
        return nv_paths, paths - nv_paths
    except FileNotFoundError as exc:
        exc_log = f"File does not exist: {exc.filename}"
        raise SystemExit(exc_log) from exc


def split_by_upgradable_and_verifiable(
    paths: set[str],
) -> tuple[tuple[str, ...], tuple[str, ...], tuple[str, ...]]:
    nu_paths, up_paths = split_by_upgradable(paths)
    nv_paths, ok_paths = split_by_verifiable(up_paths)
    return tuple(ok_paths), tuple(nu_paths), tuple(nv_paths)


def resolve_paths(
    working_dir: str,
    include: tuple[str, ...],
    exclude: tuple[str, ...],
    *,
    send_data: bool = True,
) -> Paths:
    selected_paths = list_paths(working_dir=working_dir, include=include, exclude=exclude)
    ok_paths, nu_paths, nv_paths = split_by_upgradable_and_verifiable(selected_paths)
    if send_data and is_fluid_batch_env():
        send_metrics_to_cloudwatch(
            {
                "SkimsFilesToAnalyze": {
                    "total_files": len(selected_paths),
                    "valid_files": len(ok_paths),
                },
            },
        )

    return Paths(ok_paths=ok_paths, nu_paths=nu_paths, nv_paths=nv_paths)
