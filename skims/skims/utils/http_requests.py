import asyncio
from contextlib import (
    asynccontextmanager,
    suppress,
)
from typing import (
    Any,
)

import aiohttp
from utils.function import (
    shield,
)

RETRY = shield(
    on_error_return=None,
    attempts=3,
    sleep_between_retries=3,
)


@asynccontextmanager  # type: ignore[arg-type]
async def create_session() -> aiohttp.ClientSession:  # type: ignore[misc]
    async with aiohttp.ClientSession(
        connector=aiohttp.TCPConnector(
            # The server might be timing out the connection
            # since it's being used for multiple requests
            force_close=True,
            ssl=False,
        ),
        timeout=aiohttp.ClientTimeout(
            total=120,
            connect=None,
            sock_read=None,
            sock_connect=None,
        ),
        trust_env=True,
    ) as session:
        yield session


@RETRY
async def request(
    session: aiohttp.ClientSession,
    method: str,
    url: str,
    *args: Any,  # noqa: ANN401
    **kwargs: Any,  # noqa: ANN401
) -> aiohttp.ClientResponse | None:
    with suppress(
        aiohttp.client_exceptions.ClientConnectorError,
        asyncio.TimeoutError,
        aiohttp.client_exceptions.ServerDisconnectedError,
        aiohttp.client_exceptions.ClientOSError,
        aiohttp.client_exceptions.TooManyRedirects,
    ):
        return await session.request(method, url, *args, **kwargs)
    return None
