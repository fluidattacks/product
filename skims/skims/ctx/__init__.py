import os
from os import (
    environ,
    makedirs,
)
from os.path import (
    expanduser,
)

from model.core import (
    SkimsConfig,
)

# Constants
CPU_CORES = os.cpu_count() or 1
SKIMS_CONFIG: SkimsConfig
STATE_FOLDER: str = expanduser("~/.skims")  # noqa: PTH111
STATE_FOLDER_DEBUG: str = os.path.join(STATE_FOLDER, "debug")  # noqa: PTH118
STATE_FOLDER_SCA: str = os.path.join(STATE_FOLDER, "sca")  # noqa: PTH118
NAMESPACES_FOLDER: str = os.path.join(STATE_FOLDER, "namespaces")  # noqa: PTH118


def _get_artifact(env_var: str) -> str:
    if value := environ.get(env_var):
        return value
    exc_log = f"Expected environment variable: {env_var}"
    raise ValueError(exc_log)


# Side effects
CRITERIA_REQUIREMENTS: str = _get_artifact("SKIMS_CRITERIA_REQUIREMENTS")
CRITERIA_VULNERABILITIES: str = _get_artifact("SKIMS_CRITERIA_VULNERABILITIES")
PROJECT_PATH = _get_artifact("SKIMS_PROJECT_PATH")
TREE_SITTER_PARSERS = _get_artifact("TREE_SITTER_PARSERS_DIR")

# not secrets but must be environment vars
AWS_REGION_NAME = "us-east-1"


makedirs(STATE_FOLDER, mode=0o700, exist_ok=True)  # noqa: PTH103
makedirs(STATE_FOLDER_DEBUG, mode=0o700, exist_ok=True)  # noqa: PTH103
makedirs(STATE_FOLDER_SCA, mode=0o700, exist_ok=True)  # noqa: PTH103
makedirs(NAMESPACES_FOLDER, mode=0o700, exist_ok=True)  # noqa: PTH103
