{ inputs, makeScript, outputs, ... }: {
  jobs."/skims/sca/scheduler" = makeScript {
    name = "skims-scheduler";
    searchPaths = {
      bin = [
        inputs.nixpkgs.zstd
        inputs.nixpkgs.python311
        outputs."/skims"
        outputs."/skims/skims-ai"
      ];
      source = [
        outputs."/common/utils/aws"
        outputs."/common/utils/sops"
        outputs."/skims/config/runtime"
      ];
    };
    entrypoint = ./entrypoint.sh;
  };
}
