# shellcheck shell=bash

function main {

  aws_login "prod_skims" "3600"
  python3 'skims/skims/lib_sca/schedulers/refresh_advisories.py'
  skims-ai classify_cve --db_path '../../skims_sca_advisories.db'
  zstd ./skims_sca_advisories.db
  aws s3 cp ./skims_sca_advisories.db.zst s3://skims.sca/
}

main "${@}"
