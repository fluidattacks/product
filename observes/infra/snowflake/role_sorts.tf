resource "snowflake_account_role" "sorts" {
  name = "SORTS"
}

# Grants
resource "snowflake_grant_privileges_to_account_role" "sorts_etl_warehouse_usage" {
  account_role_name = snowflake_account_role.sorts.name
  privileges        = ["USAGE"]
  on_account_object {
    object_type = "WAREHOUSE"
    object_name = snowflake_warehouse.etl_compute.name
  }
}
resource "snowflake_grant_privileges_to_account_role" "sorts_generic_warehouse_usage" {
  account_role_name = snowflake_account_role.sorts.name
  privileges        = ["USAGE"]
  on_account_object {
    object_type = "WAREHOUSE"
    object_name = snowflake_warehouse.generic_compute.name
  }
}

# Assign roles
resource "snowflake_grant_database_role" "sorts_role_setup" {
  database_role_name = snowflake_database_role.sorts.fully_qualified_name
  parent_role_name   = snowflake_account_role.sorts.name
}
resource "snowflake_grant_account_role" "admin_grant_sorts_role" {
  role_name        = snowflake_account_role.sorts.name
  parent_role_name = "SYSADMIN"
}
