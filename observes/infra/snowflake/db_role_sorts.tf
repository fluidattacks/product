resource "snowflake_database_role" "sorts" {
  database = snowflake_database.observes.name
  name     = "SORTS"
  comment  = "Role for sorts processes"
}

# Grants
resource "snowflake_grant_privileges_to_database_role" "grant_sorts_usage" {
  privileges         = ["USAGE"]
  database_role_name = snowflake_database_role.sorts.fully_qualified_name
  on_database        = snowflake_database_role.sorts.database
}
resource "snowflake_grant_privileges_to_database_role" "grant_sorts_create" {
  privileges         = ["CREATE TABLE", "USAGE"]
  database_role_name = snowflake_database_role.sorts.fully_qualified_name
  on_schema {
    schema_name = snowflake_schema.sorts.fully_qualified_name
  }
}
resource "snowflake_grant_privileges_to_database_role" "grant_sorts_usage_dynamo" {
  privileges         = ["USAGE"]
  database_role_name = snowflake_database_role.sorts.fully_qualified_name
  on_schema {
    schema_name = snowflake_schema.dynamodb.fully_qualified_name
  }
}
resource "snowflake_grant_privileges_to_database_role" "grant_sorts_read_dynamo" {
  privileges         = ["SELECT"]
  database_role_name = snowflake_database_role.sorts.fully_qualified_name
  on_schema_object {
    all {
      object_type_plural = "TABLES"
      in_schema          = snowflake_schema.dynamodb.fully_qualified_name
    }
  }
}
resource "snowflake_grant_privileges_to_database_role" "grant_sorts_read_future_dynamo" {
  privileges         = ["SELECT"]
  database_role_name = snowflake_database_role.sorts.fully_qualified_name
  on_schema_object {
    future {
      object_type_plural = "TABLES"
      in_schema          = snowflake_schema.dynamodb.fully_qualified_name
    }
  }
}

# Assign roles
resource "snowflake_grant_database_role" "sorts_indicators_writer" {
  database_role_name        = snowflake_database_role.indicators_writer.fully_qualified_name
  parent_database_role_name = snowflake_database_role.sorts.fully_qualified_name
}
