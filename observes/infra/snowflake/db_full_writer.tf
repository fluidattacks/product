resource "snowflake_database_role" "full_writer" {
  database = snowflake_database.observes.name
  name     = "FULL_WRITER"
  comment  = "Role writing over all ETLs data"
}

# Grants
resource "snowflake_grant_privileges_to_database_role" "grant_full_writer_create_schemas" {
  privileges         = ["CREATE SCHEMA", "USAGE"]
  database_role_name = snowflake_database_role.full_writer.fully_qualified_name
  on_database        = snowflake_database_role.full_writer.database
}
resource "snowflake_grant_privileges_to_database_role" "grant_full_writer_create_tables" {
  for_each           = setunion(local.persistent_schemas, local.archived_schemas, local.analytics_schemas)
  privileges         = ["CREATE TABLE", "CREATE VIEW", "CREATE MATERIALIZED VIEW", "USAGE"]
  database_role_name = snowflake_database_role.full_writer.fully_qualified_name
  on_schema {
    schema_name = each.key
  }
}
resource "snowflake_grant_privileges_to_database_role" "grant_full_writer_insert_tables" {
  for_each           = setunion(local.persistent_schemas, local.archived_schemas, local.analytics_schemas)
  privileges         = ["INSERT"]
  database_role_name = snowflake_database_role.full_writer.fully_qualified_name
  on_schema_object {
    all {
      object_type_plural = "TABLES"
      in_schema          = each.key
    }
  }
}

# Future schemas & tables
resource "snowflake_grant_privileges_to_database_role" "full_writer_future_schemas" {
  privileges         = ["CREATE TABLE", "CREATE VIEW", "CREATE MATERIALIZED VIEW", "USAGE"]
  database_role_name = snowflake_database_role.full_writer.fully_qualified_name
  on_schema {
    future_schemas_in_database = snowflake_database_role.full_writer.database
  }
}
resource "snowflake_grant_privileges_to_database_role" "full_writer_future_tables" {
  privileges         = ["INSERT"]
  database_role_name = snowflake_database_role.full_writer.fully_qualified_name
  on_schema_object {
    future {
      object_type_plural = "TABLES"
      in_database        = snowflake_database_role.full_writer.database
    }
  }
}
resource "snowflake_grant_privileges_to_database_role" "grant_full_writer_write_future_dynamo" {
  privileges         = ["INSERT"]
  database_role_name = snowflake_database_role.full_writer.fully_qualified_name
  on_schema_object {
    future {
      object_type_plural = "TABLES"
      in_schema          = snowflake_schema.dynamodb.fully_qualified_name
    }
  }
}
resource "snowflake_grant_privileges_to_database_role" "grant_full_writer_write_future_analytics" {
  privileges         = ["INSERT"]
  database_role_name = snowflake_database_role.full_writer.fully_qualified_name
  on_schema_object {
    future {
      object_type_plural = "TABLES"
      in_schema          = snowflake_schema.analytics_production.fully_qualified_name
    }
  }
}
