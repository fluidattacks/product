# [WARNING] remember to set schema names in UPPERCASE.
# If set in lower-case the schema cannot be found after creation

# Achived schemas that holds legacy data
resource "snowflake_schema" "activecampaign" {
  database = snowflake_database.observes.name
  name     = "ACTIVECAMPAIGN"
}
resource "snowflake_schema" "adwords_fluid_attacks" {
  database = snowflake_database.observes.name
  name     = "ADWORDS_FLUID_ATTACKS"
}
resource "snowflake_schema" "checkly_old" {
  database = snowflake_database.observes.name
  name     = "CHECKLY_OLD"
}
resource "snowflake_schema" "checkly_old_2" {
  database = snowflake_database.observes.name
  name     = "CHECKLY_OLD_2"
}
resource "snowflake_schema" "checkly_rolled_results" {
  database = snowflake_database.observes.name
  name     = "CHECKLY_ROLLED_RESULTS"
}
resource "snowflake_schema" "checkly_rolled_results_backup" {
  database = snowflake_database.observes.name
  name     = "CHECKLY_ROLLED_RESULTS_BACKUP"
}
resource "snowflake_schema" "customer_success" {
  database = snowflake_database.observes.name
  name     = "CUSTOMER_SUCCESS"
}
resource "snowflake_schema" "decripted_tables" {
  database = snowflake_database.observes.name
  name     = "DECRIPTED_TABLES"
}
resource "snowflake_schema" "dynamodb_fi_forces_backup" {
  database = snowflake_database.observes.name
  name     = "DYNAMODB_FI_FORCES_BACKUP"
}
resource "snowflake_schema" "dynamodb_fi_project_access_backup" {
  database = snowflake_database.observes.name
  name     = "DYNAMODB_FI_PROJECT_ACCESS_BACKUP"
}
resource "snowflake_schema" "formstack" {
  database = snowflake_database.observes.name
  name     = "FORMSTACK"
}
resource "snowflake_schema" "gsheets" {
  database = snowflake_database.observes.name
  name     = "GSHEETS"
}
resource "snowflake_schema" "help" {
  database = snowflake_database.observes.name
  name     = "HELP"
}
resource "snowflake_schema" "historic_state" {
  database = snowflake_database.observes.name
  name     = "HISTORIC_STATE"
}
resource "snowflake_schema" "logrocket" {
  database = snowflake_database.observes.name
  name     = "LOGROCKET"
}
resource "snowflake_schema" "looker_scratch" {
  database = snowflake_database.observes.name
  name     = "LOOKER_SCRATCH"
}
resource "snowflake_schema" "mailchimp" {
  database = snowflake_database.observes.name
  name     = "MAILCHIMP"
}
resource "snowflake_schema" "mailchimp_def" {
  database = snowflake_database.observes.name
  name     = "MAILCHIMP_DEF"
}
resource "snowflake_schema" "mailchimp_def_mailchimp" {
  database = snowflake_database.observes.name
  name     = "MAILCHIMP_DEF_MAILCHIMP"
}
resource "snowflake_schema" "mailchimp_def_stg_mailchimp" {
  database = snowflake_database.observes.name
  name     = "MAILCHIMP_DEF_STG_MAILCHIMP"
}
resource "snowflake_schema" "mandrill" {
  database = snowflake_database.observes.name
  name     = "MANDRILL"
}
resource "snowflake_schema" "matomo" {
  database = snowflake_database.observes.name
  name     = "MATOMO"
}
resource "snowflake_schema" "meta_comercial" {
  database = snowflake_database.observes.name
  name     = "META_COMERCIAL"
}
resource "snowflake_schema" "moodle" {
  database = snowflake_database.observes.name
  name     = "MOODLE"
}
resource "snowflake_schema" "new_checkly_backup" {
  database = snowflake_database.observes.name
  name     = "NEW_CHECKLY_BACKUP"
}
resource "snowflake_schema" "sendgrid" {
  database = snowflake_database.observes.name
  name     = "SENDGRID"
}
resource "snowflake_schema" "timedoctor_old" {
  database = snowflake_database.observes.name
  name     = "TIMEDOCTOR_OLD"
}
resource "snowflake_schema" "vault" {
  database = snowflake_database.observes.name
  name     = "VAULT"
}
resource "snowflake_schema" "zendesk" {
  database = snowflake_database.observes.name
  name     = "ZENDESK"
}
