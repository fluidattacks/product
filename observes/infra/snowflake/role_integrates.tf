resource "snowflake_account_role" "integrates" {
  name = "INTEGRATES"
}

# Grants
resource "snowflake_grant_privileges_to_account_role" "integrates_engineering_warehouse_usage" {
  account_role_name = snowflake_account_role.integrates.name
  privileges        = ["USAGE"]
  on_account_object {
    object_type = "WAREHOUSE"
    object_name = snowflake_warehouse.engineering_compute.name
  }
}
resource "snowflake_grant_privileges_to_account_role" "integrates_generic_warehouse_usage" {
  account_role_name = snowflake_account_role.integrates.name
  privileges        = ["USAGE"]
  on_account_object {
    object_type = "WAREHOUSE"
    object_name = snowflake_warehouse.generic_compute.name
  }
}

# Assign roles
resource "snowflake_grant_database_role" "integrates_role_setup" {
  database_role_name = snowflake_database_role.integrates.fully_qualified_name
  parent_role_name   = snowflake_account_role.integrates.name
}
resource "snowflake_grant_account_role" "admin_grant_integrates_role" {
  role_name        = snowflake_account_role.integrates.name
  parent_role_name = "SYSADMIN"
}
