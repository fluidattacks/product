resource "snowflake_account_role" "analytics_reader" {
  name = "ANALYTICS_READER"
}

# Grants
resource "snowflake_grant_privileges_to_account_role" "analytics_reader_dbt_warehouse_usage" {
  account_role_name = snowflake_account_role.analytics_reader.name
  privileges        = ["USAGE"]
  on_account_object {
    object_type = "WAREHOUSE"
    object_name = snowflake_warehouse.dbt_compute.name
  }
}
resource "snowflake_grant_privileges_to_account_role" "analytics_reader_generic_warehouse_usage" {
  account_role_name = snowflake_account_role.analytics_reader.name
  privileges        = ["USAGE"]
  on_account_object {
    object_type = "WAREHOUSE"
    object_name = snowflake_warehouse.generic_compute.name
  }
}
# Assign roles
resource "snowflake_grant_database_role" "analytics_reader_role_setup" {
  database_role_name = snowflake_database_role.analytics_reader.fully_qualified_name
  parent_role_name   = snowflake_account_role.analytics_reader.name
}
resource "snowflake_grant_account_role" "admin_grant_analytics_reader_role" {
  role_name        = snowflake_account_role.analytics_reader.name
  parent_role_name = "SYSADMIN"
}
