resource "snowflake_database_role" "code_reader" {
  database = snowflake_database.observes.name
  name     = "CODE_READER"
  comment  = "Role for monitoring code ETL data"
}

# Grants
resource "snowflake_grant_privileges_to_database_role" "code_reader_db" {
  privileges         = ["USAGE"]
  database_role_name = snowflake_database_role.code_reader.fully_qualified_name
  on_database        = snowflake_database_role.code_reader.database
}

resource "snowflake_grant_privileges_to_database_role" "code_reader_code_schema" {
  privileges         = ["USAGE"]
  database_role_name = snowflake_database_role.code_reader.fully_qualified_name
  on_schema {
    schema_name = snowflake_schema.code.fully_qualified_name
  }
}

resource "snowflake_grant_privileges_to_database_role" "code_reader_select_code_tables" {
  privileges         = ["SELECT"]
  database_role_name = snowflake_database_role.code_reader.fully_qualified_name
  on_schema_object {
    all {
      object_type_plural = "TABLES"
      in_schema          = snowflake_schema.code.fully_qualified_name
    }
  }
}
