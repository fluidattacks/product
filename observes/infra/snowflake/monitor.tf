resource "snowflake_resource_monitor" "etl_compute" {
  name            = "ETL_COMPUTE_ALERT"
  credit_quota    = 10
  frequency       = "DAILY"
  start_timestamp = "2024-11-07 05:00"
  suspend_trigger = 1
  notify_triggers = [100]
  notify_users = [
    "DMURCIA",
    "MRIVERA",
  ]
}

resource "snowflake_resource_monitor" "engineering_compute" {
  name            = "ENGINEERING_COMPUTE_ALERT"
  credit_quota    = 30
  frequency       = "DAILY"
  start_timestamp = "2024-11-07 05:00"
  suspend_trigger = 1
  notify_triggers = [100]
  notify_users = [
    "DMURCIA",
    "MRIVERA",
    "DBETANCUR",
    "JRESTREPO",
  ]
}
resource "snowflake_resource_monitor" "generic_compute" {
  name            = "GENERIC_COMPUTE_ALERT"
  credit_quota    = 23
  frequency       = "DAILY"
  start_timestamp = "2025-02-15 05:00"
  notify_triggers = [100]
  notify_users = [
    "DMURCIA",
    "MRIVERA",
    "DBETANCUR",
    "JRESTREPO",
  ]
}

resource "snowflake_resource_monitor" "dbt_compute" {
  name            = "DBT_COMPUTE_ALERT"
  credit_quota    = 10
  frequency       = "DAILY"
  start_timestamp = "2024-11-07 05:00"
  suspend_trigger = 1
  notify_triggers = [100]
  notify_users = [
    "DMURCIA",
    "MRIVERA",
  ]
}
