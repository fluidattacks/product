resource "snowflake_warehouse" "dbt_compute" {
  name                = "DBT_COMPUTE"
  auto_resume         = true
  auto_suspend        = 60
  initially_suspended = true
  max_cluster_count   = 1
  comment             = "compute resources for dbt analytics"
  warehouse_size      = "xsmall"
  warehouse_type      = "standard"
  resource_monitor    = snowflake_resource_monitor.dbt_compute.name
}

resource "snowflake_warehouse" "etl_compute" {
  name                = "ETL_COMPUTE"
  auto_resume         = true
  auto_suspend        = 60
  initially_suspended = true
  max_cluster_count   = 1
  comment             = "compute resources for ETL processes"
  warehouse_size      = "xsmall"
  warehouse_type      = "standard"
  resource_monitor    = snowflake_resource_monitor.etl_compute.name
}

resource "snowflake_warehouse" "engineering_compute" {
  name                = "ENGINEERING_COMPUTE"
  auto_resume         = true
  auto_suspend        = 60
  initially_suspended = true
  max_cluster_count   = 2
  min_cluster_count   = 1
  comment             = "compute resources for engineering processes"
  warehouse_size      = "xsmall"
  warehouse_type      = "standard"
  resource_monitor    = snowflake_resource_monitor.engineering_compute.name
}

resource "snowflake_warehouse" "generic_compute" {
  name                = "GENERIC_COMPUTE"
  auto_resume         = true
  auto_suspend        = 60
  initially_suspended = true
  max_cluster_count   = 2
  min_cluster_count   = 1
  comment             = "general compute resources"
  warehouse_size      = "xsmall"
  warehouse_type      = "standard"
  resource_monitor    = snowflake_resource_monitor.generic_compute.name
}
