resource "snowflake_account_role" "full_writer" {
  name = "FULL_WRITER"
}

# Grants
resource "snowflake_grant_privileges_to_account_role" "full_writer_warehouse_usage" {
  account_role_name = snowflake_account_role.full_writer.name
  privileges        = ["USAGE"]
  on_account_object {
    object_type = "WAREHOUSE"
    object_name = snowflake_warehouse.dbt_compute.name
  }
}
resource "snowflake_grant_privileges_to_account_role" "full_writer_generic_warehouse_usage" {
  account_role_name = snowflake_account_role.full_writer.name
  privileges        = ["USAGE"]
  on_account_object {
    object_type = "WAREHOUSE"
    object_name = snowflake_warehouse.generic_compute.name
  }
}

# Assign roles
resource "snowflake_grant_database_role" "full_writer_role_setup" {
  database_role_name = snowflake_database_role.full_writer.fully_qualified_name
  parent_role_name   = snowflake_account_role.full_writer.name
}
resource "snowflake_grant_account_role" "admin_grant_full_writer_role" {
  role_name        = snowflake_account_role.full_writer.name
  parent_role_name = "SYSADMIN"
}
