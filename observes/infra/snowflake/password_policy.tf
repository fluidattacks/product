resource "snowflake_password_policy" "machine_users" {
  database             = snowflake_schema.public.database
  schema               = snowflake_schema.public.name
  name                 = "MACHINE_USERS_PASSWORD_POLICY"
  history              = 12
  max_age_days         = 30
  min_length           = 64
  min_lower_case_chars = 0
  min_numeric_chars    = 0
  min_special_chars    = 0
  min_upper_case_chars = 0
}
resource "snowflake_user_password_policy_attachment" "quicksight_policy_attachment" {
  password_policy_name = snowflake_password_policy.machine_users.fully_qualified_name
  user_name            = snowflake_legacy_service_user.quicksight.name
}
resource "snowflake_user_password_policy_attachment" "zoho_dataprep_policy_attachment" {
  password_policy_name = snowflake_password_policy.machine_users.fully_qualified_name
  user_name            = snowflake_legacy_service_user.zoho_dataprep.name
}
