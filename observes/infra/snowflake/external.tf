variable "snowflakeOrgName" {
  type = string
}
variable "snowflakeAccountName" {
  type = string
}
variable "snowflakeAuthMethod" {
  type = string
}
variable "snowflakeUser" {
  type = string
}
variable "snowflakePrivateKey" {
  type = string
}
variable "zohoDataPrepPassword" {
  type = string
}
variable "quicksightPassword" {
  type = string
}
