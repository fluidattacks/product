terraform {
  required_version = "~> 1.0"

  required_providers {
    snowflake = {
      source  = "Snowflake-Labs/snowflake"
      version = "~> 1.0.1"
    }
  }

  backend "s3" {
    bucket         = "fluidattacks-terraform-states-prod"
    key            = "observes-snowflake-infra.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "terraform_state_lock"
  }

}

provider "snowflake" {
  role              = "ACCOUNTADMIN"
  organization_name = var.snowflakeOrgName
  account_name      = var.snowflakeAccountName
  user              = var.snowflakeUser
  authenticator     = var.snowflakeAuthMethod
  private_key       = var.snowflakePrivateKey
  warehouse         = "GENERIC_COMPUTE"
  preview_features_enabled = toset([
    "snowflake_file_format_resource",
    "snowflake_password_policy_resource",
    "snowflake_stage_resource",
    "snowflake_storage_integration_resource",
    "snowflake_user_password_policy_attachment_resource",
  ])
}
