# [WARNING] remember to set schema names in UPPERCASE.
# If set in lower-case the schema cannot be found after creation

resource "snowflake_schema" "public" {
  database = snowflake_database.observes.name
  name     = "PUBLIC"
}

resource "snowflake_schema" "aws_costs" {
  database = snowflake_database.observes.name
  name     = "AWS_COSTS"
}
resource "snowflake_schema" "dynamodb" {
  database = snowflake_database.observes.name
  name     = "DYNAMODB"
}
resource "snowflake_schema" "machine_executions" {
  database = snowflake_database.observes.name
  name     = "MACHINE-EXECUTIONS"
}
resource "snowflake_schema" "checkly" {
  database = snowflake_database.observes.name
  name     = "CHECKLY"
}
resource "snowflake_schema" "code" {
  database = snowflake_database.observes.name
  name     = "CODE"
}
resource "snowflake_schema" "gitlab_ci" {
  database = snowflake_database.observes.name
  name     = "GITLAB-CI"
}
resource "snowflake_schema" "integrates" {
  database = snowflake_database.observes.name
  name     = "INTEGRATES"
}
resource "snowflake_schema" "last_update" {
  database = snowflake_database.observes.name
  name     = "LAST_UPDATE"
}
resource "snowflake_schema" "timedoctor" {
  database = snowflake_database.observes.name
  name     = "TIMEDOCTOR"
}

resource "snowflake_schema" "sorts" {
  database = snowflake_database.observes.name
  name     = "SORTS"
}
resource "snowflake_schema" "migration" {
  database = snowflake_database.observes.name
  name     = "MIGRATION"
}
resource "snowflake_schema" "integrates_usage" {
  database = snowflake_database.observes.name
  name     = "INTEGRATES_USAGE"
}

# Analytics

resource "snowflake_schema" "analytics_production" {
  database = snowflake_database.observes.name
  name     = "ANALYTICS_PRODUCTION"
}

resource "snowflake_schema" "dbt_cosorio" {
  database = snowflake_database.observes.name
  name     = "DBT_COSORIO"
}
resource "snowflake_schema" "dbt_mrivera" {
  database = snowflake_database.observes.name
  name     = "DBT_MRIVERA"
}
resource "snowflake_schema" "dbt_lruiz" {
  database = snowflake_database.observes.name
  name     = "DBT_LRUIZ"
}
resource "snowflake_schema" "dbt_dhernandez" {
  database = snowflake_database.observes.name
  name     = "DBT_DHERNANDEZ"
}
resource "snowflake_schema" "airbyte_schema" {
  database = snowflake_database.observes.name
  name     = "AIRBYTE_SCHEMA"
}
resource "snowflake_schema" "qs_obs" {
  database = snowflake_database.observes.name
  name     = "QS_OBS"
}
