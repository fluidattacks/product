{ outputs, makeSearchPaths, makeTemplate, ... }:
let
  _set_env = env: value:
    makeTemplate {
      name = "setup_env";
      template = builtins.toFile "set_env_${env}" ''
        export ${env}="${value}"
      '';
    };
  default_region_setup = _set_env "AWS_DEFAULT_REGION" "us-east-1";
in {
  deployTerraform = {
    modules = {
      observes = {
        setup = [ outputs."/secretsForAwsFromGitlab/prodCommon" ];
        src = "/observes/infra/src";
        version = "1.0";
      };
      observesSnowflake = {
        # this test needs a DB super-user
        setup = [
          default_region_setup
          outputs."/secretsForAwsFromGitlab/prodCommon"
          outputs."/secretsForEnvFromSops/observesProdSnowflake"
          outputs."/secretsForTerraformFromEnv/observesProdSnowflake"
        ];
        src = "/observes/infra/snowflake";
        version = "1.0";
      };
    };
  };
  lintTerraform = {
    modules = {
      observes = {
        setup = [ outputs."/secretsForAwsFromGitlab/dev" ];
        src = "/observes/infra/src";
        version = "1.0";
      };
      observesSnowflake = {
        setup = [ default_region_setup outputs."/secretsForAwsFromGitlab/dev" ];
        src = "/observes/infra/snowflake";
        version = "1.0";
      };
    };
  };
  secretsForEnvFromSops = {
    observesProdSnowflake = {
      vars = [
        "SNOWFLAKE_ORG_NAME"
        "SNOWFLAKE_ACCOUNT_NAME"
        "SNOWFLAKE_AUTHENTICATOR"
        "SNOWFLAKE_USER"
        "SNOWFLAKE_PRIVATE_KEY"
        "SNOWFLAKE_ZOHO_DATA_PASSWORD"
        "SNOWFLAKE_QUICKSIGHT_PASSWORD"
      ];
      manifest = "/observes/secrets/prod.yaml";
    };
  };
  secretsForTerraformFromEnv = {
    observesProdSnowflake = {
      snowflakeOrgName = "SNOWFLAKE_ORG_NAME";
      snowflakeAccountName = "SNOWFLAKE_ACCOUNT_NAME";
      snowflakeAuthMethod = "SNOWFLAKE_AUTHENTICATOR";
      snowflakeUser = "SNOWFLAKE_USER";
      snowflakePrivateKey = "SNOWFLAKE_PRIVATE_KEY";
      zohoDataPrepPassword = "SNOWFLAKE_ZOHO_DATA_PASSWORD";
      quicksightPassword = "SNOWFLAKE_QUICKSIGHT_PASSWORD";
    };
  };
  testTerraform = {
    modules = {
      observes = {
        setup = [ outputs."/secretsForAwsFromGitlab/dev" ];
        src = "/observes/infra/src";
        version = "1.0";
      };
      observesSnowflake = {
        # this test needs a DB super-user
        setup = [
          default_region_setup
          outputs."/secretsForAwsFromGitlab/prodObserves"
          outputs."/secretsForEnvFromSops/observesProdSnowflake"
          outputs."/secretsForTerraformFromEnv/observesProdSnowflake"
        ];
        src = "/observes/infra/snowflake";
        version = "1.0";
      };
    };
  };
}
