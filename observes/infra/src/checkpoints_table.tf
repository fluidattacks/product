resource "aws_dynamodb_table" "observes_repos_checkpoints_tables" {
  for_each = toset(["observes_repos_checkpoints", "observes_repos_start_progress", "observes_repos_end_progress"])

  name                        = each.key
  billing_mode                = "PAY_PER_REQUEST"
  hash_key                    = "group"
  range_key                   = "repository"
  deletion_protection_enabled = true

  attribute {
    name = "group"
    type = "S"
  }

  attribute {
    name = "repository"
    type = "S"
  }

  server_side_encryption {
    enabled = true
  }

  tags = {
    "Name"              = "observes-repos-checkpoints"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "etl"
    "NOFLUID"           = "f259_non_critical_data"
  }
}
