resource "aws_ecr_repository" "observes" {
  name                 = "observes"
  image_tag_mutability = "IMMUTABLE"

  tags = {
    "Name"              = "observes.ecr"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "etl"
    "Access"            = "public"
  }
}

data "aws_ecr_lifecycle_policy_document" "observes" {
  rule {
    priority    = 1
    description = "Keep last 5 images"

    selection {
      tag_status       = "tagged"
      tag_pattern_list = ["*"]
      count_type       = "imageCountMoreThan"
      count_number     = 5
    }
    action {
      type = "expire"
    }
  }
}
resource "aws_ecr_lifecycle_policy" "observes" {
  repository = aws_ecr_repository.observes.name
  policy     = data.aws_ecr_lifecycle_policy_document.observes.json
}
