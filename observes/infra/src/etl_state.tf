resource "aws_s3_bucket" "observes_state" {
  bucket = "observes.state"

  tags = {
    "Name"              = "observes.state"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "etl"
    "Access"            = "private"
  }
}

# Bucket logging
resource "aws_s3_bucket_logging" "observes_state_logs" {
  bucket = aws_s3_bucket.observes_state.id

  target_bucket = "common.logging"
  target_prefix = "log/observes.state"
}

resource "aws_s3_bucket_acl" "observes_state" {
  bucket = aws_s3_bucket.observes_state.id

  acl = "private"
}

resource "aws_s3_bucket_server_side_encryption_configuration" "observes_state" {
  bucket = aws_s3_bucket.observes_state.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_versioning" "observes_state" {
  bucket = aws_s3_bucket.observes_state.id

  versioning_configuration {
    status     = "Enabled"
    mfa_delete = "Disabled"
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "observes_state_lifecycle" {
  depends_on = [aws_s3_bucket_versioning.observes_state]
  bucket     = aws_s3_bucket.observes_state.id
  rule {
    id = "remove_old_versions"
    noncurrent_version_expiration {
      noncurrent_days = 30
    }
    filter {}

    status = "Enabled"
  }
}
#Bucket public access
resource "aws_s3_bucket_public_access_block" "observes_state" {
  bucket = aws_s3_bucket.observes_state.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
