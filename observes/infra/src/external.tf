data "aws_caller_identity" "current" {}

data "aws_vpc" "main" {
  filter {
    name   = "tag:Name"
    values = ["fluid-vpc"]
  }
}

variable "region" {
  default = "us-east-1"
  type    = string
}

variable "accountId" {
  default = "205810638802"
  type    = string
}

variable "handlerId" {
  default = "Records"
  type    = string
}

variable "profile" {
  default = "prod_observes"
  type    = string
}
