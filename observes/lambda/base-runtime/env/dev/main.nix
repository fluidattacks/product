{ inputs, makePythonVscodeSettings, projectPath, ... }@makes_inputs:
let
  root = projectPath inputs.observesIndex.lambda.base_runtime.root;
  bundle = import "${root}/entrypoint.nix" makes_inputs;
in makePythonVscodeSettings {
  env = bundle.env.dev;
  bins = [ ];
  name = "observes-lambda-base-runtime-env-dev";
}
