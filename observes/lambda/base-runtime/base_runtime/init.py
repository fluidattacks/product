from ._utils import (
    cast_exception,
)
from base_runtime import (
    _utils,
)
from base_runtime.api import (
    new_client,
)
from base_runtime.api.core import (
    ApiClient,
    ApiEndpoint,
    Context,
    Event,
    HandlerId,
    LambdaError,
    PartialContext,
)
from dataclasses import (
    dataclass,
)
from fa_purity import (
    Cmd,
    CmdUnwrapper,
    FrozenDict,
    Maybe,
    Result,
    ResultE,
)
from fa_purity.json import (
    JsonObj,
)
import logging
from os import (
    environ,
)
from typing import (
    Callable,
)

LOG = logging.getLogger(__name__)
HandlerFunction = Callable[[Event, Context], Cmd[Result[JsonObj, LambdaError]]]
HandlerMap = FrozenDict[HandlerId, HandlerFunction]


def _get_handler_map(
    handler_map: Callable[[], HandlerMap]
) -> ResultE[HandlerMap]:
    try:
        return Result.success(handler_map())
    except Exception as error:  # pylint: disable=broad-exception-caught
        # Lambda runtime needs to catch any exception
        # for reporting it to the aws lambda service.
        return Result.failure(error)


def _require_env_var(key: str) -> Cmd[ResultE[str]]:
    return Cmd.wrap_impure(
        lambda: Maybe.from_optional(environ.get(key))
        .to_result()
        .alt(lambda _: KeyError(key))
        .alt(cast_exception)
    )


def _get_env_var(key: str) -> Cmd[Maybe[str]]:
    return Cmd.wrap_impure(lambda: Maybe.from_optional(environ.get(key)))


def _get_context() -> Cmd[ResultE[PartialContext]]:
    def _action(unwrapper: CmdUnwrapper) -> ResultE[PartialContext]:
        name = unwrapper.act(_require_env_var("AWS_LAMBDA_FUNCTION_NAME"))
        version = unwrapper.act(
            _require_env_var("AWS_LAMBDA_FUNCTION_VERSION")
        )
        memory = unwrapper.act(
            _require_env_var("AWS_LAMBDA_FUNCTION_MEMORY_SIZE")
        )
        log_group = unwrapper.act(_get_env_var("AWS_LAMBDA_LOG_GROUP_NAME"))
        log_stream = unwrapper.act(_get_env_var("AWS_LAMBDA_LOG_STREAM_NAME"))
        return name.bind(
            lambda n: version.bind(
                lambda v: memory.map(
                    lambda m: PartialContext(n, v, m, log_group, log_stream)
                )
            )
        )

    return Cmd.new_cmd(_action)


def _get_endpoint() -> Cmd[ResultE[ApiEndpoint]]:
    def _print(item: str) -> str:
        LOG.info("ApiEndpoint = %s", item)
        return item

    return _require_env_var("AWS_LAMBDA_RUNTIME_API").map(
        lambda r: r.map(_print).map(ApiEndpoint)
    )


def _get_handler() -> Cmd[ResultE[HandlerId]]:
    return _require_env_var("HANDLER_ID").map(lambda r: r.map(HandlerId))


@dataclass(frozen=True)
class InitContext:
    client: ApiClient
    partial_context: PartialContext
    handler: HandlerFunction


def get_api_client() -> Cmd[ResultE[ApiClient]]:
    return _get_endpoint().map(lambda e: e.map(new_client))


def initialize(_map: Callable[[], HandlerMap]) -> Cmd[ResultE[InitContext]]:
    def _action(unwrapper: CmdUnwrapper) -> ResultE[InitContext]:
        LOG.info("Initializing...")
        _client = unwrapper.act(get_api_client())
        _partial_context = unwrapper.act(_get_context())
        LOG.info("partial context = %s", _partial_context)
        _handler = unwrapper.act(_get_handler())
        LOG.info("Handler id = %s", _handler)
        _handler_function = _get_handler_map(_map).bind(
            lambda _map: _handler.bind(lambda h: _utils.require_key(_map, h))
        )
        return _client.bind(
            lambda client: _partial_context.bind(
                lambda context: _handler_function.map(
                    lambda fx: InitContext(client, context, fx)
                )
            )
        )

    return Cmd.new_cmd(_action)
