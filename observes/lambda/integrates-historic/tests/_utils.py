from fa_purity import (
    FrozenList,
)
import re
from redshift_client.sql_client import (
    QueryValues,
)
from snowflake_client import (
    SnowflakeQuery,
)
from typing import (
    List,
    NoReturn,
)


def check_query_replacement(
    query: SnowflakeQuery, values: QueryValues | None
) -> None | NoReturn:
    template = str(query.statement)
    _placeholders: List[str] = re.findall(r"%\((\w+)\)s", template)
    placeholders = frozenset(_placeholders)
    for placeholder in placeholders:
        if values and placeholder not in values.values:
            raise KeyError(f"value placeholder `%({placeholder})s` not mapped")
    return None


def check_query_replacements(
    query: SnowflakeQuery, values_list: FrozenList[QueryValues]
) -> None | NoReturn:
    for values in values_list:
        check_query_replacement(query, values)
    return None
