{ outputs, inputs, makeScript, projectPath, ... }@makes_inputs:
let
  root = projectPath inputs.observesIndex.lambda.integrates_historic.root;
  bundle = import "${root}/entrypoint.nix" makes_inputs;
  inherit (bundle) image;
  inherit (bundle.pkg) version;
  env = let
    root = projectPath inputs.observesIndex.common.deploy_image.root;
    bundle = import "${root}/entrypoint.nix" makes_inputs;
  in bundle.env.runtime;
in makeScript {
  name = "deploy-image";
  searchPaths = {
    bin = [ env ];
    source = [ outputs."/secretsForAwsFromGitlab/prodCommon" ];
  };
  entrypoint = ''
    deploy-image deploy \
    --registry "205810638802.dkr.ecr.us-east-1.amazonaws.com/observes" \
    --region "us-east-1" \
    --image-tar "${image}" \
    --version "${version}"
  '';
}
