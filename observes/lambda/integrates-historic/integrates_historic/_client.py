from dataclasses import (
    dataclass,
    field,
    fields as dataclass_fields,
)
from etl_utils.bug import (
    Bug,
)
from etl_utils.retry import (
    cmd_if_fail,
    retry_cmd,
    sleep_cmd,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    FrozenList,
    FrozenTools,
    Maybe,
    Result,
    ResultE,
)
from fa_purity.json import (
    JsonObj,
    JsonPrimitiveUnfolder,
    JsonUnfolder,
    JsonValueFactory,
    Unfolder,
)
import inspect
from integrates_historic.utils import (
    adapt,
    require_index,
    swap_maybe_result,
)
import logging
from redshift_client.core.id_objs import (
    Identifier,
    SchemaId,
    TableId,
)
from redshift_client.sql_client import (
    DbPrimitive,
    DbPrimitiveFactory,
    QueryValues,
    RowData as SqlClientRowData,
)
from snowflake_client import (
    SnowflakeCursor,
    SnowflakeQuery,
)
from typing import (
    Tuple,
)

LOG = logging.getLogger(__name__)
MAX_RETRIES = 10


@dataclass(frozen=True)
class HistoricDataId:
    pk: str
    sk: str


@dataclass(frozen=True)
class HistoricData:
    pk: str
    sk: str
    item: JsonObj

    @staticmethod
    def fields() -> FrozenList[str]:
        return tuple(  # type: ignore[misc]
            f.name for f in dataclass_fields(HistoricData)  # type: ignore[misc]
        )


def _decode_str(primitive: DbPrimitive) -> ResultE[str]:
    return primitive.map(
        JsonPrimitiveUnfolder.to_str,
        lambda _: Result.failure(
            Exception(TypeError("datetime not expected"))
        ),
    )


def _to_keys_dict(row: HistoricDataId) -> QueryValues:
    return QueryValues(
        DbPrimitiveFactory.from_raw_prim_dict(
            FrozenTools.freeze(
                {
                    "pk": row.pk,
                    "sk": row.sk,
                }
            )
        )
    )


def _to_raw_dict(row: HistoricData) -> QueryValues:
    return QueryValues(
        DbPrimitiveFactory.from_raw_prim_dict(
            FrozenTools.freeze(
                {
                    "pk": row.pk,
                    "sk": row.sk,
                    "item": JsonUnfolder.dumps(row.item),
                }
            )
        )
    )


def _format_historic_row_data(
    row_data: SqlClientRowData,
) -> ResultE[HistoricData]:
    pk_result = require_index(row_data.data, 0).bind(_decode_str)
    sk_result = require_index(row_data.data, 1).bind(_decode_str)
    item_result = (
        require_index(row_data.data, 2)
        .bind(_decode_str)
        .bind(JsonValueFactory.loads)
        .bind(Unfolder.to_json)
    )
    return pk_result.bind(
        lambda pk: sk_result.bind(
            lambda sk: item_result.bind(
                lambda item: Result.success(
                    HistoricData(pk, sk, item),
                    Exception,
                )
            )
        )
    )


def assert_single(
    items: FrozenList[SqlClientRowData],
) -> ResultE[SqlClientRowData]:
    if len(items) > 1:
        return Result.failure(ValueError("Expected one element"))
    return Result.success(items[0])


def format_historic(
    result: Maybe[ResultE[SqlClientRowData]],
) -> ResultE[Maybe[HistoricData]]:
    return swap_maybe_result(result).bind(
        lambda m: m.map(
            lambda row: _format_historic_row_data(row).map(
                lambda historic: Maybe.some(historic)
            )
        ).value_or(Result.success(Maybe.empty()))
    )


@dataclass(frozen=True)
class HistoricClient:
    _sql_client: SnowflakeCursor
    _schema: SchemaId = field(default=SchemaId(Identifier.new("integrates")))
    _table_id: TableId = field(default=TableId(Identifier.new("historics")))

    def _get_identifiers(self) -> FrozenDict[str, str]:
        return FrozenTools.freeze(
            {
                "schema": self._schema.name.to_str(),
                "table": self._table_id.name.to_str(),
            }
        )

    def _delete_query(
        self, row: HistoricDataId
    ) -> Tuple[SnowflakeQuery, QueryValues]:
        condition = " and ".join(
            tuple(f"{f} = %({f})s" for f in FrozenTools.freeze(["pk", "sk"]))
        )
        query = Bug.assume_success(
            "delete query",
            inspect.currentframe(),
            tuple(),
            SnowflakeQuery.dynamic_query(
                f"DELETE FROM {{schema}}.{{table}} WHERE {condition}",
                self._get_identifiers(),
            ),
        )
        data = _to_keys_dict(row)
        return (query, data)

    def delete(self, row: HistoricDataId | None) -> Cmd[None]:
        if not row:
            return Cmd.wrap_impure(lambda: LOG.debug("No rows to delete"))
        msg = Cmd.wrap_impure(lambda: LOG.debug("deleting %s row", row))
        query, data = self._delete_query(row)
        insert_cmd = msg + self._sql_client.execute(query, data)
        return retry_cmd(
            insert_cmd,
            lambda i, r: cmd_if_fail(r, sleep_cmd(i**2)),
            MAX_RETRIES,
        ).map(
            lambda r: Bug.assume_success(
                "delete_historic",
                inspect.currentframe(),
                tuple([]),
                r,
            )
        )

    def _insert_query(
        self, row: HistoricData
    ) -> Tuple[SnowflakeQuery, QueryValues]:
        fields = ",".join(HistoricData.fields())
        values = ",".join(
            tuple(
                f"PARSE_JSON(%({f})s)" if f == "item" else f"%({f})s"
                for f in HistoricData.fields()
            )
        )
        query = Bug.assume_success(
            "insert query",
            inspect.currentframe(),
            tuple(),
            SnowflakeQuery.dynamic_query(
                f"INSERT INTO {{schema}}.{{table}} ({fields}) SELECT {values}",
                self._get_identifiers(),
            ),
        )
        data = _to_raw_dict(row)
        return (query, data)

    def insert(self, row: HistoricData | None) -> Cmd[None]:
        if not row:
            return Cmd.wrap_impure(lambda: LOG.debug("No rows to insert"))
        msg = Cmd.wrap_impure(lambda: LOG.debug("inserting %s row", row))
        query, data = self._insert_query(row)
        insert_cmd = msg + self._sql_client.execute(query, data)
        return retry_cmd(
            insert_cmd,
            lambda i, r: cmd_if_fail(r, sleep_cmd(i**2)),
            MAX_RETRIES,
        ).map(
            lambda r: Bug.assume_success(
                "insert_historic",
                inspect.currentframe(),
                tuple([]),
                r,
            )
        )

    def _select_query(
        self, row: HistoricDataId
    ) -> Tuple[SnowflakeQuery, QueryValues]:
        fields = ",".join(HistoricData.fields())
        condition = " and ".join(
            tuple(f"{f}=%({f})s" for f in FrozenTools.freeze(["pk", "sk"]))
        )
        stm = f"SELECT {fields} FROM {{schema}}.{{table}} WHERE {condition}"
        query = Bug.assume_success(
            "select query",
            inspect.currentframe(),
            tuple(),
            SnowflakeQuery.dynamic_query(stm, self._get_identifiers()),
        )
        data = _to_keys_dict(row)
        return (query, data)

    def select(self, row: HistoricDataId) -> Cmd[ResultE[Maybe[HistoricData]]]:
        msg = Cmd.wrap_impure(lambda: LOG.debug("selected %s row", row))
        query, data = self._select_query(row)
        return (
            msg
            + self._sql_client.execute(query, data).map(
                lambda r: Bug.assume_success(
                    "select_historic",
                    inspect.currentframe(),
                    tuple([]),
                    r,
                )
            )
            + self._sql_client.fetch_all.map(
                lambda r: Bug.assume_success(
                    "select_historic_fetch_all",
                    inspect.currentframe(),
                    tuple([]),
                    r,
                )
            )
            .map(lambda result: adapt(result))
            .map(lambda result: result.map(assert_single))
            .map(lambda result: format_historic(result))
        )

    def _update_query(
        self, row: HistoricData
    ) -> Tuple[SnowflakeQuery, QueryValues]:
        condition = " and ".join(
            tuple(f"{f}=%({f})s" for f in FrozenTools.freeze(["pk", "sk"]))
        )
        column = "item=PARSE_JSON(%(item)s)"
        query = Bug.assume_success(
            "update query",
            inspect.currentframe(),
            tuple(),
            SnowflakeQuery.dynamic_query(
                f"UPDATE {{schema}}.{{table}} SET {column} WHERE {condition}",
                self._get_identifiers(),
            ),
        )
        data = _to_raw_dict(row)
        return (query, data)

    def update(self, row: HistoricData | None) -> Cmd[None]:
        if not row:
            return Cmd.wrap_impure(lambda: LOG.debug("No rows to update"))
        msg = Cmd.wrap_impure(lambda: LOG.debug("updating %s row", row))
        query, data = self._update_query(row)
        insert_cmd = msg + self._sql_client.execute(query, data)
        return retry_cmd(
            insert_cmd,
            lambda i, r: cmd_if_fail(r, sleep_cmd(i**2)),
            MAX_RETRIES,
        ).map(
            lambda r: Bug.assume_success(
                "update_historic",
                inspect.currentframe(),
                tuple([]),
                r,
            )
        )

    def init_table(self) -> Cmd[None]:
        statement = """
        CREATE TABLE IF NOT EXISTS {schema}.{table} (
            pk VARCHAR(4096),
            sk VARCHAR(4096),
            item VARIANT,

            PRIMARY KEY (
                pk,
                sk
            )
        )
        """
        query = Bug.assume_success(
            "init query",
            inspect.currentframe(),
            tuple(),
            SnowflakeQuery.dynamic_query(statement, self._get_identifiers()),
        )
        return self._sql_client.execute(query, None).map(
            lambda r: Bug.assume_success(
                "init_registration_table_execution",
                inspect.currentframe(),
                tuple([]),
                r,
            )
        )
