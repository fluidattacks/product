from ._logger import (
    setup_log,
)
import logging

LOG = logging.getLogger(__name__)
setup_log(LOG)
__version__ = "0.36.2"
