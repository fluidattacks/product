{ inputs, makePythonVscodeSettings, projectPath, ... }@makes_inputs:
let
  root = projectPath inputs.observesIndex.lambda.integrates_historic.root;
  bundle = import "${root}/entrypoint.nix" makes_inputs;
in makePythonVscodeSettings {
  env = bundle.env.runtime;
  bins = [ ];
  name = "observes-lambda-integrates-historic-env-runtime";
}
