from __future__ import annotations

from dataclasses import dataclass

from etl_utils.natural import Natural


@dataclass(frozen=True)
class ProjectId:
    """Represents a global project id."""

    project_id: Natural


@dataclass(frozen=True)
class UserId:
    """Represents an user id."""

    user_id: Natural


@dataclass(frozen=True)
class MilestoneGlobalId:
    """Represents an milestone global id."""

    global_id: Natural


@dataclass(frozen=True)
class MilestoneInternalId:
    """Represents an milestone internal id."""

    internal: Natural


@dataclass(frozen=True)
class MrGlobalId:
    """Represents an MR global id."""

    global_id: Natural


@dataclass(frozen=True)
class MrInternalId:
    """Represents an MR internal id."""

    internal: Natural
