import pytest
from etl_utils.natural import NaturalOperations
from fa_purity import Cmd, Unsafe

from gitlab_sdk.ids import MrGlobalId, MrInternalId
from gitlab_sdk.merge_requests import MrClientFactory
from gitlab_sdk.merge_requests.core import MergeRequest
from tests_fx._utils import get_creds_from_env, get_project_from_env


def _check_data(data: tuple[MrGlobalId, MergeRequest]) -> None:
    assert data


def test_mr() -> None:
    get_creds = get_creds_from_env().map(lambda r: r.alt(Unsafe.raise_exception).to_union())
    get_project = get_project_from_env().map(lambda r: r.alt(Unsafe.raise_exception).to_union())
    action: Cmd[None] = (
        get_creds.map(MrClientFactory.new)
        .bind(
            lambda c: get_project.bind(
                lambda project: c.get_mr(project, MrInternalId(NaturalOperations.absolute(3))),
            ),
        )
        .map(lambda r: r.alt(Unsafe.raise_exception).to_union())
        .map(_check_data)
    )

    with pytest.raises(SystemExit):
        action.compute()
