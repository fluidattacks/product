{ makes_inputs, nixpkgs, pynix, python_pkgs, }:
let
  raw_src = makes_inputs.projectPath
    makes_inputs.inputs.observesIndex.common.etl_utils.root;
  src = import "${raw_src}/build/filter.nix" nixpkgs.nix-filter raw_src;
  bundle =
    import "${raw_src}/build" { inherit makes_inputs nixpkgs pynix src; };
in bundle.buildBundle {
  pkgDeps = bundle.requirements { inherit nixpkgs python_pkgs; };
}

