path_filter: src:
path_filter {
  root = src;
  include = [ "gitlab_sdk" "tests" "pyproject.toml" "mypy.ini" "ruff.toml" ];
}
