from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    FrozenTools,
    ResultE,
    Unsafe,
)
from fa_purity.json import (
    Primitive,
)
import logging
from redshift_client.client import (
    AwsRole,
    S3Prefix,
)
from redshift_client.core.id_objs import (
    DbTableId,
    SchemaId,
    TableId,
)
from redshift_client.core.table import (
    Table,
)
from redshift_client.sql_client import (
    DbPrimitiveFactory,
    Query,
    QueryValues,
    SqlCursor,
)
from snowflake_client import (
    SnowflakeQuery,
    TableClient,
)
from snowflake_client._core import (
    SnowflakeCursor,
)
from typing import (
    Dict,
)

LOG = logging.getLogger(__name__)


def _to_raw_schema(schema: SchemaId) -> str:
    return schema.name.to_str().upper()


def _to_raw_table(table: TableId) -> str:
    return table.name.to_str().upper()


@dataclass(frozen=True)
class TableObj:
    @dataclass(frozen=True)
    class _Private:
        pass

    _private: TableObj._Private
    table_id: DbTableId
    table: Table

    @staticmethod
    def get(
        client: TableClient, table_id: DbTableId
    ) -> Cmd[ResultE[TableObj]]:
        return client.get(table_id).map(
            lambda r: r.map(
                lambda t: TableObj(TableObj._Private(), table_id, t)
            )
        )


def redshift_unload_no_manifest(
    client: SqlCursor, table: DbTableId, prefix: S3Prefix, role: AwsRole
) -> Cmd[ResultE[None]]:
    """
    prefix: a s3 uri prefix
    role: an aws role id-arn
    """
    stm = """
        UNLOAD ('SELECT * FROM {schema}.{table}')
        TO %(prefix)s iam_role %(role)s FORMAT JSON MAXFILESIZE 100 MB
    """
    args: Dict[str, Primitive] = {
        "prefix": prefix.prefix,
        "role": role.role,
    }
    return client.execute(
        Query.dynamic_query(
            stm,
            FrozenDict(
                {
                    "schema": _to_raw_schema(table.schema),
                    "table": _to_raw_table(table.table),
                }
            ),
        ),
        QueryValues(DbPrimitiveFactory.from_raw_prim_dict(FrozenDict(args))),
    )


def snowflake_load_prefix(
    cursor: SnowflakeCursor,
    table_id: DbTableId,
    prefix: S3Prefix,
) -> Cmd[ResultE[None]]:
    statement = (
        "COPY INTO {schema}.{table} "
        "FROM @migration.migration_stage "
        "PATTERN=%(pattern)s MATCH_BY_COLUMN_NAME = CASE_INSENSITIVE FORCE = TRUE"
    )
    identifiers: Dict[str, str] = {
        "schema": table_id.schema.name.to_str(),
        "table": table_id.table.name.to_str(),
    }
    pattern = (
        prefix.prefix.removeprefix("s3://observes.etl-data/migration/").rstrip(
            "/"
        )
        + "/.*"
    )
    args: Dict[str, Primitive] = {"pattern": pattern}
    msg = Cmd.wrap_impure(
        lambda: LOG.info(
            "Copying %s into %s.%s",
            pattern,
            table_id.schema.name.to_str(),
            table_id.table.name.to_str(),
        )
    )
    return msg + cursor.execute(
        SnowflakeQuery.dynamic_query(
            statement, FrozenTools.freeze(identifiers)
        )
        .alt(Unsafe.raise_exception)
        .to_union(),
        QueryValues(
            DbPrimitiveFactory.from_raw_prim_dict(FrozenTools.freeze(args))
        ),
    )
