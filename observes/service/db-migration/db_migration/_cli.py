from . import (
    archive,
)
import click
from db_migration import (
    core,
)
from fa_purity import (
    Cmd,
)
import logging
from redshift_client.core.id_objs import (
    DbTableId,
    Identifier,
    SchemaId,
    TableId,
)
from typing import (
    NoReturn,
)

LOG = logging.getLogger(__name__)


@click.command()
@click.argument("schema", type=str)
def migrate_schema(
    schema: str,
) -> NoReturn:
    info = Cmd.wrap_impure(
        lambda: LOG.info("Starting migration for schema `%s`", schema)
    )
    cmd: Cmd[None] = info + core.migrate(
        frozenset([SchemaId(Identifier.new(schema))])
    )
    cmd.compute()


@click.command()
def migrate_archive() -> NoReturn:
    info = Cmd.wrap_impure(
        lambda: LOG.info("Starting migration for data archive")
    )
    cmd: Cmd[None] = info + archive.migrate_archive()
    cmd.compute()


@click.command()
@click.option("--source-schema", type=str)
@click.option("--source-table", type=str)
@click.option("--target-schema", type=str)
@click.option("--target-table", type=str)
def migrate_table(
    source_schema: str,
    source_table: str,
    target_schema: str,
    target_table: str,
) -> NoReturn:
    _source = DbTableId(
        SchemaId(Identifier.new(source_schema)),
        TableId(Identifier.new(source_table)),
    )
    _target = DbTableId(
        SchemaId(Identifier.new(target_schema)),
        TableId(Identifier.new(target_table)),
    )

    info = Cmd.wrap_impure(
        lambda: LOG.info(
            "Starting migration from `%s` -> `%s`", _source, _target
        )
    )
    cmd: Cmd[None] = info + core.migrate_table(_source, _target)
    cmd.compute()


@click.group()
def main() -> None:
    # cli main entrypoint
    pass


main.add_command(migrate_schema)
main.add_command(migrate_archive)
main.add_command(migrate_table)
