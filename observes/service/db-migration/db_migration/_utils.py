from collections.abc import (
    Callable,
)
from fa_purity import (
    Cmd,
    CmdUnwrapper,
    PureIter,
    Result,
    ResultFactory,
)
from typing import (
    TypeVar,
)

_S = TypeVar("_S")
_F = TypeVar("_F")
_A = TypeVar("_A")


def chain_cmd_result(
    cmd_1: Cmd[Result[_S, _F]], cmd_2: Callable[[_S], Cmd[Result[_A, _F]]]
) -> Cmd[Result[_A, _F]]:
    factory: ResultFactory[_A, _F] = ResultFactory()
    return cmd_1.bind(
        lambda r: r.map(cmd_2)
        .alt(lambda e: Cmd.wrap_value(factory.failure(e)))
        .to_union()
    )


def consume_results(
    commands: PureIter[Cmd[Result[None, _F]]]
) -> Cmd[Result[None, _F]]:
    def _action(unwrapper: CmdUnwrapper) -> Result[None, _F]:
        for cmd in commands:
            result = unwrapper.act(cmd)
            success = result.map(lambda _: True).value_or(False)
            if not success:
                return result
        return Result.success(None)

    return Cmd.new_cmd(_action)
