{ makes_inputs, nixpkgs, pynix, python_version, src, }:
let
  deps = import ./deps { inherit makes_inputs nixpkgs python_version; };
  pkgDeps = {
    runtime_deps = with deps.python_pkgs; [
      boto3
      connection-manager
      click
      fa-purity
      etl-utils
      utils-logger
    ];
    build_deps = with deps.python_pkgs; [ flit-core ];
    test_deps = with deps.python_pkgs; [
      arch-lint
      mypy
      pytest
      pytest-cov
      ruff
    ];
  };
  bundle = pynix.stdBundle { inherit pkgDeps src; };
  vs_settings = pynix.vscodeSettingsShell { pythonEnv = bundle.env.dev; };
  coverage = pynix.coverageReport {
    inherit src;
    parentModulePath =
      makes_inputs.inputs.observesIndex.service.success_indicators.root;
    pythonDevEnv = bundle.env.dev;
    module =
      makes_inputs.inputs.observesIndex.service.success_indicators.pkg_name;
    testsFolder = "tests";
  };
in bundle // {
  inherit coverage;
  dev_hook = vs_settings.hook.text;
}

