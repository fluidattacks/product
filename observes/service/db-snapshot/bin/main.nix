{ inputs, makeScript, projectPath, ... }@makes_inputs:
let
  root = projectPath inputs.observesIndex.service.db_snapshot.root;
  bundle = import "${root}/entrypoint.nix" makes_inputs;
  env = bundle.env.runtime;
in makeScript {
  name = "db-snapshot";
  searchPaths = { bin = [ env ]; };
  entrypoint = ./entrypoint.sh;
}
