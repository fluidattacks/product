{ inputs, ... }@makes_inputs:
let
  python_version = "python311";
  nixpkgs = inputs.nixpkgs-observes // { inherit (inputs) nix-filter; };
  out = import ./build {
    inherit makes_inputs nixpkgs python_version;
    src = nixpkgs.nix-filter {
      root = ./.;
      include =
        [ "db_snapshot" "tests" "pyproject.toml" "mypy.ini" ".pylintrc" ];
    };
  };
in out
