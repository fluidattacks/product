let
  commonPath = "/observes/common";
  lambdasPath = "/observes/lambda";
  pipelinePath = "/observes/pipeline";
  servicePath = "/observes/service";
  sdkPath = "/observes/sdk";
  singerPath = "/observes/singer";
  etlsPath = "/observes/etl";

  compose = let
    # definition from nixpkgs.lib.reverseList
    reverseList = xs:
      let l = builtins.length xs;
      in builtins.genList (n: builtins.elemAt xs (l - n - 1)) l;
  in functions: val: builtins.foldl' (x: f: f x) val (reverseList functions);

  underscore_pkg = root:
    builtins.replaceStrings [ "-" ] [ "_" ] (baseNameOf root);
  standard_pkgs = { root, name, pkg_name, }: {
    inherit pkg_name;
    inherit name;
    inherit root;
    src = "${root}/${pkg_name}";
    check = {
      tests = "${root}/check/tests";
      types = "${root}/check/types";
    };
    bin = "${root}/bin";
    env = {
      dev = "${root}/env/dev";
      runtime = "${root}/env/runtime";
    };
  };
  default_nomenclature = root: {
    inherit root;
    name = baseNameOf root;
    pkg_name = underscore_pkg root;
  };
  with_default_nomenclature = root: standard_pkgs (default_nomenclature root);

  no_bin = x: builtins.removeAttrs x [ "bin" ];
  no_checks = x: builtins.removeAttrs x [ "check" ];
  no_bin_standard = compose [ no_bin with_default_nomenclature ];
  with_coverage = x: x // { upload_coverage = "${x.root}/upload/coverage"; };
  no_bin_standard_2 =
    compose [ with_coverage no_checks no_bin with_default_nomenclature ];
  bin_standard_2 =
    compose [ with_coverage no_checks with_default_nomenclature ];
  standard_pkgs_2 = compose [ with_coverage no_checks standard_pkgs ];
in {
  inherit commonPath pipelinePath servicePath singerPath etlsPath;
  lambda = {
    base_runtime = no_bin_standard "${lambdasPath}/base-runtime";
    integrates_historic = let
      root = "${lambdasPath}/integrates-historic";
      self = no_bin_standard root;
    in self // { check = self.check // { image = "${root}/check/image"; }; };
  };
  service = {
    db_migration = with_default_nomenclature "${servicePath}/db-migration";
    db_snapshot = with_default_nomenclature "${servicePath}/db-snapshot";
    success_indicators = no_bin_standard_2 "${servicePath}/success-indicators";
  };
  etl = {
    dynamo = compose [ no_bin standard_pkgs ] {
      root = "${etlsPath}/dynamo";
      name = "dynamo-etl-conf";
      pkg_name = "dynamo_etl_conf";
    };
    code = standard_pkgs {
      root = "${etlsPath}/code";
      name = "code-etl";
      pkg_name = "code_etl";
    };
    gitlab = standard_pkgs_2 {
      root = "${etlsPath}/gitlab";
      name = "gitlab-etl";
      pkg_name = "gitlab_etl";
    };
    google_sheets = standard_pkgs {
      root = "${etlsPath}/google-sheets";
      name = "google-sheets-etl";
      pkg_name = "google_sheets_etl";
    };
    integrates_usage = standard_pkgs_2 {
      root = "${etlsPath}/integrates-usage";
      name = "integrates-usage-etl";
      pkg_name = "integrates_usage_etl";
    };
    timedoctor = standard_pkgs {
      root = "${etlsPath}/timedoctor";
      name = "timedoctor-etl";
      pkg_name = "timedoctor_etl";
    };
    zoho_crm_leads = bin_standard_2 "${etlsPath}/zoho-crm-leads";
  };
  common = {
    deploy_image = no_bin_standard_2 "${commonPath}/deploy-image";
    python_pkgs = "${commonPath}/python-pkgs";
    integrates_dal = no_bin_standard "${commonPath}/integrates-dal";
    connection_manager = no_bin_standard "${commonPath}/connection-manager";
    etl_utils = no_bin_standard_2 "${commonPath}/etl-utils";
    utils_logger = no_bin_standard "${commonPath}/utils-logger";
    utils_logger_2 = compose [ no_bin standard_pkgs ] {
      root = "${commonPath}/utils-logger-2";
      name = "utils-logger-2";
      pkg_name = "utils_logger";
    };
  };
  sdk = {
    gitlab = compose [ with_coverage no_checks no_bin standard_pkgs ] {
      root = "${sdkPath}/gitlab";
      name = "gitlab-sdk";
      pkg_name = "gitlab_sdk";
    };
    timedoctor = compose [ with_coverage no_checks no_bin standard_pkgs ] {
      root = "${sdkPath}/timedoctor";
      name = "timedoctor-sdk";
      pkg_name = "timedoctor_sdk";
    };
  };
  tap = {
    batch = no_bin_standard "${singerPath}/tap-batch";
    bugsnag = no_bin_standard_2 "${singerPath}/tap-bugsnag";
    ce = bin_standard_2 "${singerPath}/tap-ce";
    checkly = no_bin_standard "${singerPath}/tap-checkly";
    cloudwatch = no_bin_standard "${singerPath}/tap-cloudwatch";
    csv = no_bin_standard "${singerPath}/tap-csv";
    delighted = no_bin_standard "${singerPath}/tap-delighted";
    dynamo = no_bin_standard "${singerPath}/tap-dynamo";
    flow = no_bin_standard "${singerPath}/tap-flow";
    gitlab = no_bin_standard "${singerPath}/tap-gitlab";
    gitlab_dora = no_bin_standard "${singerPath}/tap-gitlab-dora";
    google_sheets = let root = "${singerPath}/tap-google-sheets";
    in {
      name = "tap-google-sheets";
      inherit root;
      env.dev = "${root}/env/dev";
    };
    json = no_bin_standard "${singerPath}/tap-json";
    mixpanel = no_bin_standard "${singerPath}/tap-mixpanel";
    timedoctor = no_bin_standard "${singerPath}/tap-timedoctor";
    zoho_crm = no_bin_standard "${singerPath}/tap-zoho-crm";
    zoho_people = no_bin_standard "${singerPath}/tap-zoho-people";
  };
  target = {
    s3 = no_bin_standard "${singerPath}/target-s3";
    warehouse = no_bin_standard "${singerPath}/target-warehouse";
  };
}
