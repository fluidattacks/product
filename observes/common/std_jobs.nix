{ makes_inputs, pkg_index }:
let
  root = makes_inputs.projectPath pkg_index.root;
  job_prefix = pkg_index.root;
  bundle = import "${root}/entrypoint.nix" makes_inputs;
  name = builtins.replaceStrings [ "/" ] [ "-" ] job_prefix;
in {
  "${job_prefix}/env/dev" = bundle.env.dev;
  "${job_prefix}/env/runtime" = bundle.env.runtime;
  "${job_prefix}/shell/dev" = let
    run-lint = makes_inputs.inputs.nixpkgs.writeShellApplication {
      name = "run-lint";
      runtimeInputs = [ ];
      text = ''
        ruff format
        ruff check . --fix
        ruff format
        mypy . && pytest ./tests
      '';
    };
  in makes_inputs.makeTemplate {
    name = "${name}-makes-dev-shell";
    searchPaths = { bin = [ run-lint bundle.env.dev ]; };
    template = bundle.dev_hook;
  };
  "${job_prefix}/upload/coverage" = makes_inputs.inputs.uploadCoverage {
    inherit makes_inputs;
    dev_env = bundle.env.dev;
    coverage_report = bundle.coverage;
    project_path =
      makes_inputs.inputs.nixpkgs.lib.removePrefix "/" pkg_index.root;
  };
}
