{ makes_inputs, nixpkgs, pynix, python_version, src, }:
let
  deps = import ./deps { inherit makes_inputs nixpkgs pynix python_version; };
  bin_deps = with nixpkgs; [ skopeo ];
  pkgDeps = {
    runtime_deps = bin_deps ++ (with deps.python_pkgs; [
      boto3
      click
      fa-purity
      mypy-boto3-ecr
      types-boto3
    ]);
    build_deps = with deps.python_pkgs; [ flit-core ];
    test_deps = with deps.python_pkgs; [
      arch-lint
      mypy
      pytest
      pytest-cov
      ruff
    ];
  };
  bundle = pynix.stdBundle { inherit pkgDeps src; };
  vs_settings = pynix.vscodeSettingsShell { pythonEnv = bundle.env.dev; };
  coverage = pynix.coverageReport {
    inherit src;
    parentModulePath =
      makes_inputs.inputs.observesIndex.common.deploy_image.root;
    pythonDevEnv = bundle.env.dev;
    module = makes_inputs.inputs.observesIndex.common.deploy_image.pkg_name;
    testsFolder = "tests";
  };
in bundle // {
  inherit coverage;
  dev_hook = vs_settings.hook.text;
}
