from pathlib import Path
from typing import (
    NoReturn,
)

import click
from fa_purity import Cmd, Unsafe

from .deploy import EcrRepo, Image
from .main import login_and_deploy


@click.group()
def main() -> None:
    # main cli entrypoint
    pass


@click.command()
@click.option(
    "--registry",
    type=str,
    required=True,
    help="The image registry URI",
)
@click.option(
    "--region",
    type=str,
    required=True,
    help="The aws region",
)
@click.option(
    "--image-tar",
    type=str,
    required=True,
    help="The image tar to deploy",
)
@click.option(
    "--version",
    type=str,
    required=True,
    help="The image version tag",
)
def deploy(registry: str, region: str, image_tar: str, version: str) -> NoReturn:
    cmd: Cmd[None] = login_and_deploy(
        region,
        EcrRepo(registry),
        Image(Path(image_tar), version),
    ).map(lambda r: r.alt(Unsafe.raise_exception).to_union())
    cmd.compute()


main.add_command(deploy)
