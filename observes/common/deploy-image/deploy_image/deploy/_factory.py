import logging
import os
import sys
from dataclasses import dataclass

import boto3
from botocore.client import ClientError
from fa_purity import (
    Cmd,
    FrozenDict,
    Result,
    ResultE,
)
from mypy_boto3_ecr.client import ECRClient

from . import _token
from ._core import Credentials, DeployClient, EcrRepo, Image
from ._process import (
    RunningSubprocess,
    Subprocess,
)

LOG = logging.getLogger(__name__)


def _delete_tag(creds: Credentials, repo: EcrRepo, tag: str) -> Cmd[ResultE[None]]:
    def _delete() -> Subprocess[str]:
        args = (
            "skopeo",
            "--insecure-policy",
            "delete",
            "--creds",
            creds.user + ":" + creds.password,
            "docker://" + repo.uri + ":" + tag,
        )
        LOG.info("Deleting latest tag from %s", repo.uri)
        external: dict[str, str] = dict(os.environ)
        return Subprocess(args, None, None, sys.stderr, FrozenDict(external))

    return RunningSubprocess.run_universal_newlines(_delete()).bind(lambda p: p.wait_result(None))


def _exist_tag(client: ECRClient, repo: EcrRepo, tag: str) -> Cmd[ResultE[bool]]:
    def _action() -> ResultE[bool]:
        try:
            client.describe_images(
                repositoryName=repo.uri.split("/")[1],
                imageIds=[{"imageTag": tag}],
            )
            return Result.success(True)
        except ClientError as e:  # type: ignore[misc]
            if e.response["Error"]["Code"] == "ImageNotFoundException":  # type: ignore[misc]
                return Result.success(False)
            return Result.failure(e)

    return Cmd.wrap_impure(_action)


def _copy_to_registry(
    creds: Credentials,
    repo: EcrRepo,
    image: Image,
) -> Cmd[ResultE[None]]:
    def _copy() -> Subprocess[str]:
        args = (
            "skopeo",
            "--insecure-policy",
            "copy",
            "--dest-creds",
            creds.user + ":" + creds.password,
            "docker-archive:" + image.tar_file.as_posix(),
            "docker://" + repo.uri + ":" + image.tag,
        )
        LOG.info("skopeo args: %s", str(args))
        external: dict[str, str] = dict(os.environ)
        return Subprocess(args, None, None, sys.stderr, FrozenDict(external))

    return RunningSubprocess.run_universal_newlines(_copy()).bind(lambda p: p.wait_result(None))


def _new_ecr_client(aws_region: str) -> Cmd[ResultE[ECRClient]]:
    def _action() -> ResultE[ECRClient]:
        try:
            return Result.success(boto3.client("ecr", region_name=aws_region))
        except Exception as err:  # noqa: BLE001 it is safe since the error is returned
            return Result.failure(err)

    return Cmd.wrap_impure(_action)


def _new_deploy_client(client: ECRClient, creds: Credentials) -> DeployClient:
    return DeployClient(
        lambda r, i: _copy_to_registry(creds, r, i),
        lambda r, t: _delete_tag(creds, r, t),
        lambda r, t: _exist_tag(client, r, t),
    )


@dataclass(frozen=True)
class DeployClientFactory:
    @staticmethod
    def new(aws_region: str) -> Cmd[ResultE[DeployClient]]:
        return _new_ecr_client(aws_region).bind(
            lambda r: r.to_coproduct().map(
                lambda c: _token.get_ecr_creds(c).map(
                    lambda r: r.map(lambda creds: _new_deploy_client(c, creds)),
                ),
                lambda e: Cmd.wrap_value(Result.failure(e)),
            ),
        )
