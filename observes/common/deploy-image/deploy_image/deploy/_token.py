import base64
import logging
import sys
from dataclasses import (
    dataclass,
)

from botocore.exceptions import (
    NoCredentialsError,
)
from fa_purity import (
    Cmd,
    Maybe,
    ResultE,
    ResultFactory,
    cast_exception,
)
from mypy_boto3_ecr.client import ECRClient

from deploy_image import (
    _utils,
)

from ._core import (
    Credentials,
)

LOG = logging.getLogger(__name__)
stderr_handler = logging.StreamHandler(sys.stderr)
LOG.addHandler(stderr_handler)


def _split(raw: str) -> ResultE[Credentials]:
    splitted = tuple(base64.b64decode(raw).decode("utf-8").split(":"))
    return _utils.get_index(splitted, 0).bind(
        lambda user: _utils.get_index(splitted, 1).map(lambda p: Credentials(user, p)),
    )


@dataclass
class MissingCredsError(Exception):
    parent: Exception
    msg: str


@dataclass
class ClientError(Exception):
    parent: Exception
    msg: str


def get_ecr_creds(ecr_client: ECRClient) -> Cmd[ResultE[Credentials]]:
    def _action() -> ResultE[Credentials]:
        factory: ResultFactory[Credentials, Exception] = ResultFactory()
        try:
            auth = ecr_client.get_authorization_token()
            token = Maybe.from_result(
                _utils.get_index(tuple(auth["authorizationData"]), 0).alt(lambda _: None),
            ).bind_optional(lambda a: a.get("authorizationToken"))
            return (
                token.to_result()
                .alt(lambda _: ValueError("Missing creds"))
                .alt(cast_exception)
                .bind(_split)
            )
        except NoCredentialsError as err:  # type: ignore[misc]
            return factory.failure(
                MissingCredsError(err, "AWS creds not found, try to login again with okta."),
            ).alt(cast_exception)
        except ClientError as err:
            return factory.failure(ClientError(err, "")).alt(cast_exception)

    return Cmd.wrap_impure(_action)
