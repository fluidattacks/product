from __future__ import annotations

from dataclasses import dataclass

from fa_purity import Result, ResultE, Unsafe


@dataclass(frozen=True)
class Natural:
    """Defines a natural number i.e. integers >= 0."""

    @dataclass(frozen=True)
    class _Private:
        pass

    _private: Natural._Private
    value: int

    @staticmethod
    def from_int(value: int) -> ResultE[Natural]:
        if value >= 0:
            return Result.success(Natural(Natural._Private(), value))
        err = "`Natural` must be >= 0"
        return Result.failure(ValueError(err))

    @classmethod
    def zero(cls) -> Natural:
        return cls.from_int(0).alt(Unsafe.raise_exception).to_union()

    @classmethod
    def succ(cls, number: Natural) -> Natural:
        return cls.from_int(number.value + 1).alt(Unsafe.raise_exception).to_union()


@dataclass(frozen=True)
class NaturalOperations:
    @staticmethod
    def add(number: Natural, number_2: Natural) -> Natural:
        return (
            Natural.from_int(number.value + number_2.value).alt(Unsafe.raise_exception).to_union()
        )

    @staticmethod
    def absolute(number: int) -> Natural:
        return Natural.from_int(abs(number)).alt(Unsafe.raise_exception).to_union()
