{ inputs, ... }@makes_inputs:
let
  nixpkgs = inputs.nixpkgs-observes-2 // { inherit (inputs) nix-filter; };
  pynix = inputs.buildPynix {
    inherit nixpkgs;
    pythonVersion = "python311";
  };
  out = import ./build {
    inherit makes_inputs nixpkgs pynix;
    src = import ./build/filter.nix nixpkgs.nix-filter ./.;
  };
in out
