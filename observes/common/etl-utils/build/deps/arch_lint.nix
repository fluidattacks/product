{ lib, nixpkgs, makes_inputs, python_pkgs, python_version }@args:
let
  bundles = import (makes_inputs.projectPath
    "${makes_inputs.inputs.observesIndex.common.python_pkgs}/arch_lint") args;
in bundles."v4.0.0".build_bundle (default: required_deps: builder:
  builder lib
  (required_deps (python_pkgs // { inherit (default.python_pkgs) grimp; })))
