{ nixpkgs, makes_inputs, pkg_index_item
, # compatible with pkg items at standard 4
}:
let
  root = makes_inputs.projectPath pkg_index_item.root;
  bundle = import "${root}/entrypoint.nix" makes_inputs;
  inherit (bundle.check) types;
in makes_inputs.makeDerivation {
  searchPaths = { bin = [ types ]; };
  name = "observes-${pkg_index_item.name}-check-types";
  builder = "touch $out";
}
