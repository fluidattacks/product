lib: python_pkgs:
lib.buildPythonPackage rec {
  pname = "coralogix_logger";
  version = "2.0.6";
  src = lib.fetchPypi {
    inherit pname version;
    sha256 = "sha256-e6zLEFSigraB+CHkh+at/B/BcbO11tmHwcQe2uAEA84=";
  };
  doCheck = false;
  propagatedBuildInputs = with python_pkgs; [ requests setuptools ];
  format = "pyproject";
}
