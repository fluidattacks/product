{ makes_inputs, nixpkgs, python_version, }:
let
  lib = {
    buildEnv = nixpkgs."${python_version}".buildEnv.override;
    inherit (nixpkgs."${python_version}".pkgs) buildPythonPackage;
    inherit (nixpkgs.python3Packages) fetchPypi;
  };

  utils = makes_inputs.pythonOverrideUtils;
  override_utils =
    import (makes_inputs.projectPath "/observes/common/override_utils.nix") {
      inherit (makes_inputs) pythonOverrideUtils;
    };
  layer_1 = python_pkgs:
    python_pkgs // {
      arch-lint = let
        result = import ./arch_lint.nix {
          inherit lib makes_inputs nixpkgs python_pkgs python_version;
        };
      in result.pkg;
    };
  layer_2 = python_pkgs:
    python_pkgs // {
      fa-purity = let
        result = import ./fa_purity.nix {
          inherit lib makes_inputs nixpkgs python_pkgs python_version;
        };
      in result.pkg;
    };
  layer_3 = python_pkgs:
    python_pkgs // {
      etl-utils = let
        result = import ./etl_utils.nix {
          inherit lib makes_inputs nixpkgs python_pkgs python_version;
        };
      in result.pkg;
      utils-logger = let
        result = import ./utils_logger.nix {
          inherit lib makes_inputs nixpkgs python_pkgs python_version;
        };
      in result.pkg;
    };
  layer_4 = python_pkgs:
    python_pkgs // {
      snowflake-client = let
        result = import ./snowflake_client.nix {
          inherit lib makes_inputs nixpkgs python_pkgs python_version;
        };
      in override_utils.deep_pkg_override python_pkgs.fa-purity result.pkg;
    };

  python_pkgs = utils.compose [ layer_4 layer_3 layer_2 layer_1 ]
    nixpkgs."${python_version}Packages";
in { inherit lib python_pkgs; }
