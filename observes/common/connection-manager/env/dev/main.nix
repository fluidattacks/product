{ inputs, makePythonVscodeSettings, projectPath, ... }@makes_inputs:
let
  root = projectPath inputs.observesIndex.common.connection_manager.root;
  bundle = import "${root}/entrypoint.nix" makes_inputs;
in makePythonVscodeSettings {
  env = bundle.env.dev;
  bins = [ ];
  name = "observes-common-connection-manager-env-dev";
}
