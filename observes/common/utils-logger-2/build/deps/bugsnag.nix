lib: old_bugsnag:
old_bugsnag.overridePythonAttrs (_: rec {
  version = "4.7.0";
  src = lib.fetchPypi {
    pname = "bugsnag";
    inherit version;
    hash = "sha256-+w0lI2goXYnfX2KB+DNFPXl6UCOhg5o17zggalyYXr8=";
  };
})
