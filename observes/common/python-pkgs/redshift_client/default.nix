{ lib, makes_inputs, nixpkgs, python_pkgs, python_version, }:
let
  make_bundle = commit: sha256:
    let
      raw_src = builtins.fetchTarball {
        inherit sha256;
        url =
          "https://gitlab.com/dmurciaatfluid/redshift_client/-/archive/${commit}/redshift_client-${commit}.tar";
      };
      src = import "${raw_src}/build/filter.nix" nixpkgs.nix-filter raw_src;
    in import "${raw_src}/build" {
      makesLib = makes_inputs;
      inherit nixpkgs python_version src;
    };
  make_bundle_2 = commit: sha256:
    let
      raw_src = builtins.fetchTarball {
        inherit sha256;
        url =
          "https://gitlab.com/dmurciaatfluid/redshift_client/-/archive/${commit}/redshift_client-${commit}.tar";
      };
      src = import "${raw_src}/build/filter.nix" nixpkgs.nix-filter raw_src;
    in import "${raw_src}/build" {
      inherit makes_inputs nixpkgs python_version src;
    };
in {
  "v3.0.0+1" = make_bundle "0c87f67a4a028696ff65e56bb45a479123bb598d"
    "19m4g2kbpl3mxvlh2h1fc2ib5685pzkwzrjb4dvvs6yac9ddifxc";
  "v4.0.2" = make_bundle "c0da0fa5705c6a6c3b54bebb8677e7d22ef229c7"
    "18mh22pfc212lx7s4wc5i5plajpmzj4pq75swm62fya8gc9cdn6q";
  "v5.1.2" = make_bundle "76a448306e18c5de6a2cee5372889b631c55a1e2"
    "1ss1w6vqb1xwchyhjzzs9q4jn2za8nshjm17hll2gmw1gvq6i4wz";
  "v6.0.0" = make_bundle "f1c3252cfd18dd79c05f0d4e5361ecf3169ed006"
    "0gh3bd8kyinj1kp0kgs0baq200n3prpw4pxddfz6xn30djxvf5jh";
  "v7.0.0" = make_bundle_2 "27b3603f00a926502996b44c283db5df05943b38"
    "0p0gca0qxnlmql241v3m4fvs9sxqaw4fb2d1b5xx4czjm9a2p29h";
  "v8.0.1" = make_bundle_2 "2db2133313edc79b21643b431bb01614dbfcbf05"
    "060gqc3lqxh6nfa0sksn5qxj11b71qn6dpjjvf7bj5vfpyhxwijp";
}
