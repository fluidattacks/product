{ lib, makes_inputs, nixpkgs, python_pkgs, python_version, }:
let
  commit = "886926021cd0bbe12ad3333fd6925e66306d7d7a"; # 1.7.0+1
  raw_src = builtins.fetchTarball {
    sha256 = "sha256:0ymmmhkzvsjlgsr7bym0s2k05pfhiig3h2d1ivv37hpgakkf5bgs";
    url =
      "https://gitlab.com/dmurciaatfluid/singer_io/-/archive/${commit}/singer_io-${commit}.tar";
  };
  src = import "${raw_src}/build/filter.nix" nixpkgs.nix-filter raw_src;
  bundle = import "${raw_src}/build" {
    makesLib = makes_inputs;
    inherit nixpkgs python_version src;
  };
in bundle
