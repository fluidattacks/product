{ lib, makes_inputs, nixpkgs, python_pkgs, python_version, }:
let
  make_bundle = commit: sha256:
    let
      raw_src = builtins.fetchTarball {
        inherit sha256;
        url =
          "https://gitlab.com/dmurciaatfluid/pure_requests/-/archive/${commit}/pure_requests-${commit}.tar";
      };
      src = import "${raw_src}/build/filter.nix" nixpkgs.nix-filter raw_src;
    in import "${raw_src}/build" {
      makesLib = makes_inputs;
      inherit nixpkgs python_version src;
    };
  make_bundle_2 = commit: sha256:
    let
      raw_src = builtins.fetchTarball {
        inherit sha256;
        url =
          "https://gitlab.com/dmurciaatfluid/pure_requests/-/archive/${commit}/pure_requests-${commit}.tar";
      };
      src = import "${raw_src}/build/filter.nix" nixpkgs.nix-filter raw_src;
    in import "${raw_src}/build" {
      inherit makes_inputs nixpkgs python_version src;
    };
in {
  "v1.0.0" = make_bundle "fdf9daae9d6e5ee337e647aeb625b03932977483"
    "16rlsdqb953kbb9k9v1sjh5sd0xp06rih4rqh35y5k0mjgyxq7r8";
  "v2.0.0" = make_bundle_2 "18ad8b3f17bb77fd6da9f6f5c59cf4556b1250ab"
    "09bmj8byg1kxvk4y0k33bsqddfsg0m69hqagx6xq13vvslnx1c1g";
}
