{ lib, makes_inputs, nixpkgs, python_pkgs, python_version, }:
let
  pynix = makes_inputs.inputs.buildPynix {
    inherit nixpkgs;
    pythonVersion = python_version;
  };
  raw_src = makes_inputs.projectPath
    makes_inputs.inputs.observesIndex.sdk.timedoctor.root;
  src = import "${raw_src}/build/filter.nix" nixpkgs.nix-filter raw_src;
  bundle = import "${raw_src}/build" {
    inherit makes_inputs nixpkgs pynix python_version src;
  };
in { "v0.1.0" = bundle; }
