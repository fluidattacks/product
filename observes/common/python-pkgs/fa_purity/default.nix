{ lib, makes_inputs, nixpkgs, python_pkgs, python_version, }:
let
  make_bundle = commit: sha256:
    let
      raw_src = builtins.fetchTarball {
        inherit sha256;
        url =
          "https://gitlab.com/dmurciaatfluid/purity/-/archive/${commit}/purity-${commit}.tar";
      };
      src = import "${raw_src}/build/filter.nix" nixpkgs.nix-filter raw_src;
    in import "${raw_src}/build" {
      makesLib = makes_inputs;
      inherit nixpkgs python_version src;
    };
in {
  "v1.42.0" = make_bundle "7b13ef0b00cb0985561c625534894275f120b4b7"
    "18yrlz1j797vd4v55biphvlxviblmk1ah15w38kdalkckpmfzbfj";
  "v2.0.0" = make_bundle "bc2621cb8b330474edc2d36407fc5a7e0b0db09c"
    "1cjrjhbypby2s0q456dja3ji3b1f3rfijmbrymk13blxsxavq183";
  "v2.1.0" = make_bundle "4888d4773ccf685be5638c2188b7feedf42866a7"
    "0nwnas89fm3q48mrkpm9078jiy6wap9nrl89x4q09a7cialjbs30";
}
