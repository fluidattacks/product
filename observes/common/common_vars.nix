{ projectPath }: [
  [
    "CORALOGIX_LOG_URL"
    "https://ingress.cx498-aws-us-west-2.coralogix.com:443/api/v1/logs"
    ""
  ]
  [ "SECRETS_FILE" (projectPath "/observes/secrets/prod.yaml") "" ]
  [ "AWS_DEFAULT_REGION" "us-east-1" "" ]
]
