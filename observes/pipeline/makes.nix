{ inputs, lib, ... }:
let
  arch = let
    commit = "3c8778c732597a2fdee42d7c20d1ab5d948e13d4";
    sha256 = "sha256:0ii4qam4ykq9gbi2lmkk9p9gixmrypmz54jgcji9hpypk4azxy3w";
    url =
      "https://gitlab.com/fluidattacks/universe/-/raw/${commit}/common/ci/arch.nix";
    src = builtins.fetchurl { inherit sha256 url; };
  in import src;

  rules = {
    all = let products = [ "all" "observes" "observes-.*" ];
    in {
      default = arch.core.rules.titleRule {
        inherit products;
        types = [ ];
      };
      noRotate = arch.core.rules.titleRule {
        inherit products;
        types = [ "feat" "fix" "refac" ];
      };
    };
    common = let products = [ "all" "observes" "observes-common" ];
    in {
      noRotate = arch.core.rules.titleRule {
        inherit products;
        types = [ "feat" "fix" "refac" ];
      };
    };
    infra = let products = [ "all" "observes" "observes-infra" ];
    in {
      default = arch.core.rules.titleRule {
        inherit products;
        types = [ ];
      };
      noRotate = arch.core.rules.titleRule {
        inherit products;
        types = [ "feat" "fix" "refac" ];
      };
    };
  };

  _to_product_name = product: "observes-${product}";
  index = inputs.observesIndex;
  std_pkgs = with index; [
    common.connection_manager
    common.deploy_image
    common.integrates_dal
    common.etl_utils
    common.utils_logger
    common.utils_logger_2
    etl.code
    etl.dynamo
    etl.gitlab
    etl.google_sheets
    etl.integrates_usage
    etl.timedoctor
    etl.zoho_crm_leads
    lambda.base_runtime
    lambda.integrates_historic
    service.db_migration
    service.db_snapshot
    service.success_indicators
    sdk.timedoctor
    sdk.gitlab
    tap.batch
    tap.bugsnag
    tap.ce
    tap.cloudwatch
    tap.csv
    tap.checkly
    tap.delighted
    tap.dynamo
    tap.flow
    tap.gitlab
    tap.gitlab_dora
    tap.json
    tap.mixpanel
    tap.timedoctor
    tap.zoho_crm
    tap.zoho_people
    target.s3
    target.warehouse
  ];
  _if_exists = attrs: key: function: default:
    if builtins.hasAttr key attrs then function attrs."${key}" else default;

  per_package_deps = pkg:
    let
      dependency_map = {
        "${index.etl.timedoctor.root}" =
          [ index.etl.timedoctor.name index.sdk.timedoctor.name ];
        "${index.etl.gitlab.root}" =
          [ index.etl.gitlab.name index.sdk.gitlab.name ];
        "${index.sdk.gitlab.root}" =
          [ index.sdk.gitlab.name index.etl.gitlab.name ];
        "${index.etl.dynamo.root}" = [
          index.etl.dynamo.name
          index.tap.dynamo.name
          index.target.warehouse.name
        ];
        "${index.etl.google_sheets.root}" = [
          index.etl.google_sheets.name
          index.tap.google_sheets.name
          index.target.warehouse.name
        ];
      };
      triggers = _if_exists dependency_map pkg.root (x: x) [ pkg.name ];
    in triggers;

  _gen_job = gitlabExtraChanges: output:
    let
      _retryOnError = {
        retry = {
          max = 2;
          when = "runner_system_failure";
        };
      };
    in {
      inherit output;
      gitlabExtra = gitlabExtraChanges // _retryOnError;
    };
  # jobs
  gen_pkg_jobs = pkg:
    let
      all_pkg_triggers = per_package_deps pkg;

      _lint = output: [{
        inherit output;
        gitlabExtra = arch.extras.default // {
          rules = arch.rules.dev ++ [
            (arch.core.rules.titleRule {
              products = [ "all" "observes" ]
                ++ (map _to_product_name all_pkg_triggers);
              types = [ "feat" "fix" "refac" ];
            })
          ];
          stage = arch.stages.test;
          tags = [ arch.tags.observes ];
        };
      }];
      _test = output: [{
        inherit output;
        gitlabExtra = arch.extras.default // {
          rules = arch.rules.dev ++ [ rules.all.noRotate ]
            ++ (map _to_product_name all_pkg_triggers);
          stage = arch.stages.test;
          tags = [ arch.tags.observes ];
          types = [ "feat" "fix" "refac" ];
        };
      }];
      _build = output: [{
        inherit output;
        gitlabExtra = arch.extras.default // {
          rules = arch.rules.dev ++ [
            (arch.core.rules.titleRule {
              products = [ "all" "observes" ]
                ++ (map _to_product_name all_pkg_triggers);
              types = [ "feat" "fix" "refac" ];
            })
          ];
          stage = arch.stages.test;
          tags = [ arch.tags.observes ];
        };
      }];
      _deploy_dev = output: [{
        inherit output;
        gitlabExtra = arch.extras.default // {
          resource_group = "$CI_JOB_NAME";
          rules = arch.rules.dev ++ [
            (arch.core.rules.titleRule {
              products = [ "all" "observes" ]
                ++ (map _to_product_name all_pkg_triggers);
              types = [ "feat" "fix" "refac" ];
            })
          ];
          stage = arch.stages.deploy;
          tags = [ arch.tags.observes ];
          variables.GIT_DEPTH =
            if lib.strings.hasSuffix "/upload/coverage" output then 1000 else 3;
        };
      }];
      _deploy = output: [{
        inherit output;
        gitlabExtra = arch.extras.default // {
          resource_group = "$CI_JOB_NAME";
          rules = arch.rules.prod ++ [
            (arch.core.rules.titleRule {
              products = [ "all" "observes" ]
                ++ (map _to_product_name all_pkg_triggers);
              types = [ "feat" "fix" "refac" ];
            })
          ];
          stage = arch.stages.deploy;
          tags = [ arch.tags.observes ];
        };
      }];

      arch_check =
        _if_exists pkg "check" (c: _if_exists c "arch" _lint [ ]) [ ];
      types_check =
        _if_exists pkg "check" (c: _if_exists c "types" _lint [ ]) [ ];
      tests_check =
        _if_exists pkg "check" (c: _if_exists c "tests" _lint [ ]) [ ];
      run_check =
        _if_exists pkg "check" (c: _if_exists c "runtime" _lint [ ]) [ ];
      image_check =
        _if_exists pkg "check" (c: _if_exists c "image" _lint [ ]) [ ];
      env_dev = _if_exists pkg.env "dev" _build [ ];
      env_runtime = _if_exists pkg.env "runtime" _build [ ];
      bin_test = _if_exists pkg "bin" (v: _build "${v} --help") [ ];
      deploy = let
        dependency_map = {
          "${index.lambda.integrates_historic.root}" =
            "${index.lambda.integrates_historic.root}/deploy";
        };
      in _if_exists dependency_map pkg.root _deploy [ ];
      upload_coverage = _if_exists pkg "upload_coverage" _deploy_dev [ ];
    in builtins.concatLists [
      arch_check
      types_check
      tests_check
      run_check
      env_dev
      image_check
      env_runtime
      bin_test
      upload_coverage
      deploy
    ];
  pkgs_jobs = builtins.concatLists (map gen_pkg_jobs std_pkgs);
  manual_jobs = [
    {
      output = "/deployTerraform/observes";
      gitlabExtra = arch.extras.default // {
        needs = [{
          job = "${index.lambda.integrates_historic.root}/deploy";
          optional = true;
        }];
        resource_group = "$CI_JOB_NAME";
        rules = arch.rules.prod ++ [ rules.infra.default ];
        stage = arch.stages.deploy;
        tags = [ arch.tags.observes ];
      };
    }
    {
      output = "/deployTerraform/observesSnowflake";
      gitlabExtra = arch.extras.default // {
        needs = [ "/testTerraform/observesSnowflake" ];
        resource_group = "$CI_JOB_NAME";
        rules = arch.rules.prod ++ [ rules.infra.default ];
        stage = arch.stages.deploy;
        tags = [ arch.tags.observes ];
      };
    }
    {
      output = "/lintTerraform/observes";
      gitlabExtra = arch.extras.default // {
        rules = arch.rules.dev ++ [ rules.infra.noRotate ];
        stage = arch.stages.test;
        tags = [ arch.tags.observes ];
      };
    }
    {
      output = "/lintTerraform/observesSnowflake";
      gitlabExtra = arch.extras.default // {
        rules = arch.rules.dev ++ [ rules.infra.noRotate ];
        stage = arch.stages.test;
        tags = [ arch.tags.observes ];
      };
    }
    {
      output = "/pipelineOnGitlab/observes";
      gitlabExtra = arch.extras.default // {
        rules = arch.rules.dev ++ [ rules.common.noRotate ];
        stage = arch.stages.test;
        tags = [ arch.tags.observes ];
      };
    }
    {
      output = "/observes/job/one-mypy-conf";
      gitlabExtra = arch.extras.default // {
        rules = arch.rules.dev ++ [ rules.all.noRotate ];
        stage = arch.stages.test;
        tags = [ arch.tags.observes ];
      };
    }
    {
      output = "/testTerraform/observes";
      gitlabExtra = arch.extras.default // {
        rules = arch.rules.dev ++ [ rules.infra.default ];
        stage = arch.stages.test;
        tags = [ arch.tags.observes ];
      };
    }
    {
      output = "/testTerraform/observesSnowflake";
      gitlabExtra = arch.extras.default // {
        rules = arch.rules.prod ++ [ rules.infra.default ];
        stage = arch.stages.test;
        tags = [ arch.tags.observes ];
      };
    }
    {
      output = "/observes/machine/sca";
      gitlabExtra = arch.extras.default // {
        rules = arch.rules.dev ++ [ rules.all.default ];
        script = [ "m . /skims scan $PWD/observes/sca_config.yaml" ];
        stage = arch.stages.test;
        tags = [ arch.tags.observes ];
      };
    }
  ];
in {
  pipelines = {
    observes = {
      gitlabPath = "/observes/pipeline/default.yaml";
      jobs = pkgs_jobs ++ manual_jobs;
    };
  };
}
