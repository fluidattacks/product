{ inputs, makeScript, outputs, projectPath, ... }@makes_inputs:
let
  deps = with inputs.observesIndex; [ tap.gitlab.root ];
  env = builtins.map inputs.getRuntime deps;
  common_vars = import (projectPath "/observes/common/common_vars.nix") {
    inherit projectPath;
  };
in makeScript {
  searchPaths = {
    bin = env;
    export = common_vars;
    source = [ outputs."/common/utils/aws" outputs."/common/utils/sops" ];
  };
  name = "observes-job-cancel-ci-jobs";
  entrypoint = ./entrypoint.sh;
}
