import filecmp
import glob
import logging
import os
from pathlib import (
    Path,
)
import sys

LOG = logging.getLogger(__name__)
LOG.setLevel(logging.INFO)
LOG.addHandler(logging.StreamHandler(sys.stderr))

core = Path("./observes/common/mypy.ini")
observes_root = (Path(".") / "observes").resolve().as_posix()
LOG.info("Checking mypy configurations at %s", observes_root)

for root, _, files in os.walk(observes_root):
    for file in files:
        if file == "mypy.ini":
            _file = os.path.join(root, file)
            if not filecmp.cmp(_file, core.resolve(), shallow=False):
                raise ValueError(
                    f"A config file diverges from the standard config i.e. {_file}"
                )
            else:
                LOG.info("%s is compliant!", _file)
