{ makes_inputs, dev_env, coverage_report, project_path }:
let inherit (makes_inputs.inputs) nixpkgs;
in makes_inputs.makeScript {
  replace = {
    __argSecretsDev__ = makes_inputs.projectPath "/common/secrets/dev.yaml";
    __argCovReports__ = coverage_report;
    __argRelativeProjectPath__ = project_path;
  };
  name = "observes-coverage";
  searchPaths = {
    bin = [ dev_env nixpkgs.git makes_inputs.inputs.codecov-cli ];
    source = [
      makes_inputs.outputs."/common/utils/aws"
      makes_inputs.outputs."/common/utils/sops"
    ];
  };
  entrypoint = makes_inputs.projectPath "/observes/job/coverage/upload.sh";
}
