# shellcheck shell=bash

cp -r __argCovReports__/coverage.xml ./__argRelativeProjectPath__
info "Uploading codecov"
aws_login "dev" "3600"
sops_export_vars __argSecretsDev__ CODECOV_TOKEN
codecov-cli upload-process --flag observes --dir __argRelativeProjectPath__
