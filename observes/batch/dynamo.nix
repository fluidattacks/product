{ compose, add_name, # Str -> Attrs -> Attrs
chain, # Attrs -> NextJob -> Attrs
parallel_job, # int -> Attrs -> Attrs
observes_job, # Attrs -> Attrs
}:
let
  segmentation = 500;
  no_name = job: removeAttrs job [ "name" ];
  phase_1 = let name = "dynamo_etl_pipeline_phase_1";
  in compose [ (add_name name) (parallel_job segmentation) observes_job ] {
    inherit name;
    size = "observes";
    attempts = 5;
    timeout = 24 * 3600;
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/observes/etl/dynamo/bin"
      "local"
      "phase-1"
      (toString segmentation)
      "auto"
    ];
  };
  base_cmd =
    [ "m" "gitlab:fluidattacks/universe@trunk" "/observes/etl/dynamo/bin" ];
  local_cmd = base_cmd ++ [ "local" ];
  phase_2_snowflake = let name = "dynamo_etl_pipeline_phase_2";
  in compose [ (add_name name) observes_job ] {
    inherit name;
    size = "observes";
    attempts = 5;
    timeout = 1 * 3600;
    command = local_cmd ++ [ "phase-2" ];
  };
  phase_3_snowflake = let name = "dynamo_etl_pipeline_phase_3";
  in compose [ (add_name name) observes_job ] {
    inherit name;
    size = "observes";
    attempts = 5;
    timeout = 24 * 3600;
    command = local_cmd ++ [ "phase-3" ];
  };
  phase_4_snowflake = let name = "dynamo_etl_pipeline_phase_4";
  in compose [ (add_name name) observes_job ] {
    inherit name;
    size = "observes";
    attempts = 10;
    timeout = 2 * 3600;
    command = local_cmd ++ [ "phase-4" ];
  };
in {
  observesDynamoSchema = observes_job {
    name = "dynamo_etl_determine_schema";
    size = "observes_large";
    attempts = 3;
    timeout = 1 * 3600;
    command = base_cmd ++ [ "determine-schema" ];
  };
  observesDynamoPhase1 = observes_job {
    name = "dynamo_etl_phase1_segment";
    size = "observes";
    attempts = 5;
    timeout = 24 * 3600;
    command = local_cmd ++ [ "phase-1" ];
  };
  observesDynamoPhase2 = no_name phase_2_snowflake;
  observesDynamoPhase3 = no_name phase_3_snowflake;
  observesDynamoPhase4 = no_name phase_4_snowflake;
  observesDynamoPipelineSnowflake = let
    continue_3 = chain phase_3_snowflake phase_4_snowflake;
    continue_2 = chain phase_2_snowflake continue_3;
    main = chain phase_1 continue_2;
  in no_name main;
}
