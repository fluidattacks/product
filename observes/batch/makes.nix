{ fromYaml, projectPath, pythonOverrideUtils, inputs, outputs, ... }:
let
  inherit (pythonOverrideUtils) compose;
  sizes_conf = fromYaml
    (builtins.readFile (projectPath "/common/compute/arch/sizes/data.yaml"));
  compute_resources = size: {
    vcpus = sizes_conf."${size}".cpu;
    inherit (sizes_conf."${size}") memory;
    inherit (sizes_conf."${size}") queue;
  };
  observes_job = { name, attempts, timeout, size, command, nextJob ? { }, }:
    {
      inherit attempts command nextJob;
      includePositionalArgsInName = false;
      attemptDurationSeconds = timeout;
      definition = "prod_observes";
      environment = [ "CACHIX_AUTH_TOKEN" ];
      setup = [ outputs."/secretsForAwsFromGitlab/prodObserves" ];
      tags = {
        "Name" = name;
        "fluidattacks:line" = "cost";
        "fluidattacks:comp" = "etl";
      };
    } // compute_resources size;

  # Modifiers
  add_name = name: item: item // { inherit name; };
  chain = item: nextJob: item // { inherit nextJob; };
  include_args = item: item // { includePositionalArgsInName = true; };
  parallel_job = parallel:
    let parallel_conf = if parallel >= 2 then { inherit parallel; } else { };
    in job: job // parallel_conf;

  dynamo_jobs = import ./dynamo.nix {
    inherit compose add_name chain parallel_job observes_job;
  };
in {
  computeOnAwsBatch = {
    observesCodeEtlAmend = compose [ include_args observes_job ] {
      name = "dynamo_etl_amend";
      size = "observes_code_etl_new";
      attempts = 1;
      timeout = 24 * 3600;
      command = [
        "m"
        "gitlab:fluidattacks/universe@trunk"
        "/observes/etl/code/jobs/amend"
      ];
    };
    observesMigration = compose [ include_args observes_job ] {
      name = "data_migration";
      size = "observes";
      attempts = 1;
      timeout = 2 * 24 * 3600;
      command = [
        "m"
        "gitlab:fluidattacks/universe@trunk"
        "/observes/service/db-migration/bin"
      ];
    };
    observesCodeEtlFixBills = compose [ include_args observes_job ] {
      name = "fix_bills";
      size = "observes_code_etl_new";
      attempts = 3;
      timeout = 8 * 3600;
      command = [
        "m"
        "gitlab:fluidattacks/universe@trunk"
        "/observes/etl/code/jobs/compute-bills-temp"
      ];
    };
    observesCodeEtlNewUploader = compose [ include_args observes_job ] {
      name = "code_upload";
      size = "observes_code_etl_on_demand_new";
      attempts = 10;
      timeout = 16 * 3600;
      command = [
        "m"
        "gitlab:fluidattacks/universe@trunk"
        "/observes/etl/code/jobs/upload-group-new-uploader"
      ];
    };
    observesCodeEtlUploadGroupSnowflake =
      compose [ include_args observes_job ] {
        name = "code_upload";
        size = "observes_code_etl_on_demand_new";
        attempts = 10;
        timeout = 16 * 3600;
        command = [
          "m"
          "gitlab:fluidattacks/universe@trunk"
          "/observes/etl/code/jobs/upload-group-snowflake"
        ];
      };
    observesCodeEtlUploadPerRootSnowflake =
      compose [ include_args observes_job ] {
        name = "code_upload_per_root";
        size = "observes_code_etl_on_demand_new";
        attempts = 10;
        timeout = 16 * 3600;
        command = [
          "m"
          "gitlab:fluidattacks/universe@trunk"
          "/observes/etl/code/jobs/upload-root-snowflake"
        ];
      };
    observesBugsnagSnowflake = compose [ include_args observes_job ] {
      name = "bugsnag_snowflake";
      size = "observes";
      attempts = 3;
      timeout = 8 * 3600;
      command = [
        "m"
        "gitlab:fluidattacks/universe@trunk"
        "/observes/etl/bugsnag/snowflake"
      ];
    };
    observesCloudwatchSnowflake = compose [ include_args observes_job ] {
      name = "cloudwatch_snowflake";
      size = "observes";
      attempts = 3;
      timeout = 8 * 3600;
      command = [
        "m"
        "gitlab:fluidattacks/universe@trunk"
        "/observes/etl/cloudwatch/snowflake"
      ];
    };
    observesDelightedSnowflake = compose [ include_args observes_job ] {
      name = "delighted_snowflake";
      size = "admin_etl";
      attempts = 3;
      timeout = 8 * 3600;
      command = [
        "m"
        "gitlab:fluidattacks/universe@trunk"
        "/observes/etl/delighted/snowflake"
      ];
    };
    observesFlowSnowflake = compose [ include_args observes_job ] {
      name = "flow_snowflake";
      size = "research_etl";
      attempts = 3;
      timeout = 8 * 3600;
      command = [
        "m"
        "gitlab:fluidattacks/universe@trunk"
        "/observes/etl/flow/snowflake"
      ];
    };
    observesMixpanelSnowflake = compose [ include_args observes_job ] {
      name = "mixpanel_snowflake";
      size = "admin_etl";
      attempts = 3;
      timeout = 8 * 3600;
      command = [
        "m"
        "gitlab:fluidattacks/universe@trunk"
        "/observes/etl/mixpanel/snowflake"
      ];
    };
    observesTimedoctorSnowflake = compose [ include_args observes_job ] {
      name = "timedoctor_snowflake";
      size = "observes";
      attempts = 3;
      timeout = 8 * 3600;
      command = [
        "m"
        "gitlab:fluidattacks/universe@trunk"
        "/observes/etl/timedoctor/snowflake"
      ];
    };
  } // dynamo_jobs;
}
