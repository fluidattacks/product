from ._core import (
    SingerLoader,
)
from ._handlers import (
    CommonSingerHandler,
    MutableMap,
    MutableTableMap,
    SingerHandlerOptions,
)
from connection_manager import (
    CommonTableClient,
)
from dataclasses import (
    dataclass,
)
from etl_utils.parallel import (
    ThreadPool,
)
from fa_purity import (
    Cmd,
    Maybe,
)
from target_warehouse._s3 import (
    S3URI,
)
from target_warehouse.data_schema.duplicates import (
    SingerToColumnMap,
)


@dataclass(frozen=True)
class Loaders:
    @staticmethod
    def common_loader(
        thread_pool: ThreadPool,
        client: CommonTableClient,
        options: SingerHandlerOptions,
        s3_state: Maybe[S3URI],
        column_map: SingerToColumnMap,
    ) -> Cmd[SingerLoader]:
        """
        - upload singer records into the warehouse
        - transforms singer schemas into the warehouse tables
        - saves singer states into a s3 file
        """
        state: Cmd[MutableTableMap] = MutableMap.new()
        return state.map(
            lambda m: SingerLoader(
                lambda s, p: CommonSingerHandler(
                    s, client, options, s3_state, thread_pool, column_map
                ).handle(m, p)
            )
        )
