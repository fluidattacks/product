from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
    field,
)
from fa_purity import (
    PureIter,
)
from typing import (
    Callable,
    FrozenSet,
    Generic,
    TypeVar,
)

_T = TypeVar("_T")


@dataclass(frozen=True)
class _Private:
    pass


def _append_set(items: FrozenSet[_T], item: _T) -> FrozenSet[_T]:
    return frozenset(items | {item})


@dataclass(frozen=True)
class DuplicateClassifier(Generic[_T]):
    _private: _Private = field(repr=False, hash=False, compare=False)
    unique: FrozenSet[_T]
    duplicates: FrozenSet[_T]
    is_present: Callable[[_T, FrozenSet[_T]], bool]

    def append(self, item: _T) -> DuplicateClassifier[_T]:
        if self.is_present(item, self.unique):
            return DuplicateClassifier(
                _Private(),
                self.unique,
                _append_set(self.duplicates, item),
                self.is_present,
            )
        return DuplicateClassifier(
            _Private(),
            _append_set(self.unique, item),
            self.duplicates,
            self.is_present,
        )

    @staticmethod
    def empty(
        is_present: Callable[[_T, FrozenSet[_T]], bool]
    ) -> DuplicateClassifier[_T]:
        empty: FrozenSet[_T] = frozenset({})
        return DuplicateClassifier(_Private(), empty, empty, is_present)

    @classmethod
    def classify(
        cls,
        is_present: Callable[[_T, FrozenSet[_T]], bool],
        items: PureIter[_T],
    ) -> DuplicateClassifier[_T]:
        return items.reduce(lambda s, i: s.append(i), cls.empty(is_present))
