"""
Core procedures configured and called in the cli interface
"""
from ._generic import (
    GenericExecutor,
)

__all__ = [
    "GenericExecutor",
]
