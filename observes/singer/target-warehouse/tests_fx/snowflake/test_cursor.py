from ._utils import (
    common_cursor,
    connection_setup,
)
from fa_purity import (
    Cmd,
    Unsafe,
)
import logging
import pytest
from snowflake_client._core import (
    SnowflakeQuery,
)

LOG = logging.getLogger(__name__)


def test_cursor() -> None:
    with pytest.raises(SystemExit):
        cmd: Cmd[None] = (
            connection_setup()
            .bind(lambda c: common_cursor(c, LOG))
            .bind(
                lambda c: c.execute(
                    SnowflakeQuery.new_query("CREATE SCHEMA test"), None
                ).map(lambda r: r.alt(Unsafe.raise_exception).to_union())
            )
        )
        cmd.compute()
