{ makes_inputs, nixpkgs, python_version, src, }:
let
  deps = import ./deps { inherit makes_inputs nixpkgs python_version; };
  pkgDeps = {
    runtime_deps = with deps.python_pkgs; [
      boto3
      click
      connection-manager
      fa-singer-io
      mypy-boto3-s3
      pathos
      utils-logger
    ];
    build_deps = with deps.python_pkgs; [ flit-core ];
    test_deps = with deps.python_pkgs; [
      arch-lint
      mypy
      pylint
      pytest
      pytest-cov
    ];
  };
  bundle = makes_inputs.makePythonPyprojectPackage {
    inherit (deps.lib) buildEnv buildPythonPackage;
    inherit pkgDeps src;
  };
in import (makes_inputs.projectPath "/observes/common/bundle_patch.nix") {
  inherit (deps.lib) buildEnv;
  inherit bundle;
}
