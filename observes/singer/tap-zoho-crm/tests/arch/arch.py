from arch_lint.dag import (
    DagMap,
)
from arch_lint.graph import (
    FullPathModule,
)
from fa_purity import (
    FrozenList,
)
from typing import (
    Dict,
    FrozenSet,
    TypeVar,
)

_dag: Dict[str, FrozenList[FrozenList[str] | str]] = {
    "tap_zoho_crm": (
        "_cli",
        "streams",
        "extractor",
        "api",
        ("_logger", "_decode"),
    ),
    "tap_zoho_crm.api": (
        ("bulk", "users"),
        "auth",
        "common",
    ),
    "tap_zoho_crm.api.users": (
        "_crud",
        "_get",
        "_objs",
    ),
    "tap_zoho_crm.api.bulk": (
        "_crud",
        ("_create", "_download", "_get"),
        "_decode",
        "_objs",
    ),
    "tap_zoho_crm.api.auth": (
        ("_access", "_refresh", "_revoke"),
        "_core",
    ),
    "tap_zoho_crm._cli": (
        "_stream",
        "_decode",
    ),
}
_T = TypeVar("_T")


def raise_or_return(item: Exception | _T) -> _T:
    if isinstance(item, Exception):
        raise item
    return item


def project_dag() -> DagMap:
    return raise_or_return(DagMap.new(_dag))


def forbidden_allowlist() -> Dict[FullPathModule, FrozenSet[FullPathModule]]:
    _raw: Dict[str, FrozenSet[str]] = {}
    return {
        raise_or_return(FullPathModule.from_raw(k)): frozenset(
            raise_or_return(FullPathModule.from_raw(i)) for i in v
        )
        for k, v in _raw.items()
    }
