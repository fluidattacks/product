from ._objs import (
    UsersDataPage,
    UserType,
)
from fa_purity import (
    cast_exception,
    Cmd,
    ResultE,
)
from fa_purity.json import (
    JsonObj,
    JsonPrimitiveUnfolder,
    JsonUnfolder,
    JsonValue,
    Primitive,
    UnfoldedFactory,
    Unfolder,
)
import inspect
import logging
from pure_requests.basic import (
    Endpoint,
    HttpClientFactory,
    Params,
)
from pure_requests.rate_limit import (
    RateLimiter,
)
from tap_zoho_crm import (
    _decode,
)
from tap_zoho_crm.api.common import (
    API_URL,
    ApiBug,
    DataPageInfo,
    PageIndex,
    Token,
)
from typing import (
    Dict,
)

API_ENDPOINT = API_URL + "/crm/v2/users"
LOG = logging.getLogger(__name__)
require = JsonUnfolder.require


def _to_int(value: JsonValue) -> ResultE[int]:
    return Unfolder.to_primitive(value).bind(JsonPrimitiveUnfolder.to_int)


def _to_bool(value: JsonValue) -> ResultE[bool]:
    return Unfolder.to_primitive(value).bind(JsonPrimitiveUnfolder.to_bool)


def _decode_info(raw: JsonObj) -> ResultE[DataPageInfo]:
    page_index_result = require(raw, "page", _to_int).bind(
        lambda page: require(raw, "per_page", _to_int).map(
            lambda per_page: PageIndex(page, per_page)
        )
    )
    return require(raw, "count", _to_int).bind(
        lambda count: require(raw, "more_records", _to_bool).bind(
            lambda more_records: page_index_result.map(
                lambda p: DataPageInfo(p, count, more_records)
            )
        )
    )


def _decode_users(raw: JsonObj) -> ResultE[UsersDataPage]:
    users_result = require(
        raw, "users", lambda u: Unfolder.to_list_of(u, Unfolder.to_json)
    )
    info = require(
        raw, "info", lambda r: Unfolder.to_json(r).bind(_decode_info)
    )
    return users_result.bind(
        lambda users: info.map(lambda info: UsersDataPage(users, info))
    )


def get_users(
    limiter: RateLimiter, token: Token, user_type: UserType, page_i: PageIndex
) -> Cmd[UsersDataPage]:
    msg = Cmd.wrap_impure(lambda: LOG.info("API: Get users (%s)", user_type))
    endpoint = Endpoint(API_ENDPOINT)
    headers: Dict[str, Primitive] = {
        "Authorization": "Zoho-oauthtoken " + token.raw_token
    }
    _params: Dict[str, Primitive] = {
        "type": user_type.value,
        "page": page_i.page,
        "per_page": page_i.per_page,
    }
    client = HttpClientFactory.new_client(
        None, UnfoldedFactory.from_dict(headers), None
    )
    params = Params(UnfoldedFactory.from_dict(_params))
    cmd: Cmd[UsersDataPage] = msg + client.get(endpoint, params).map(
        lambda r: ApiBug.assume_success(
            "UsersError.response",
            inspect.currentframe(),
            (str(client), endpoint.raw, str(params)),
            r.alt(cast_exception),
        )
    ).map(
        lambda r: ApiBug.assume_success(
            "UsersError.decode_json",
            inspect.currentframe(),
            (str(client), endpoint.raw, str(params), str(r)),
            _decode.decode_json(r).alt(
                lambda c: c.map(
                    cast_exception,
                    lambda x: x.map(cast_exception, cast_exception),
                )
            ),
        )
    ).map(
        lambda j: ApiBug.assume_success(
            "UsersError.decode_json",
            inspect.currentframe(),
            (str(client), endpoint.raw, str(params), JsonUnfolder.dumps(j)),
            _decode_users(j),
        )
    )
    return limiter.call_or_wait(cmd)
