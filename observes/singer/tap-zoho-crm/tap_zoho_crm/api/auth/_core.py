from dataclasses import (
    dataclass,
)
import hashlib
from tap_zoho_crm.api.common import (
    Token,
)
from typing import (
    FrozenSet,
)

ACCOUNTS_URL = "https://accounts.zoho.com"  # for US region


@dataclass(frozen=True)
class RefreshToken:
    raw_token: str

    def __repr__(self) -> str:
        signature = hashlib.sha256(self.raw_token.encode("utf-8")).hexdigest()[
            :128
        ]
        return f"[masked] signature={signature}"


@dataclass(frozen=True)
class Credentials:
    client_id: str
    client_secret: str
    refresh_token: str
    scopes: FrozenSet[str]

    def __repr__(self) -> str:
        return f"Creds(client_id={self.client_id}, scopes={self.scopes})"


__all__ = [
    "Token",
]
