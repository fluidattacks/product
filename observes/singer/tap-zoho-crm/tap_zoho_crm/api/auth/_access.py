from ._core import (
    Credentials,
    Token,
)
from fa_purity import (
    cast_exception,
    Cmd,
    FrozenDict,
    ResultE,
)
from fa_purity.json import (
    JsonObj,
    JsonPrimitiveUnfolder,
    JsonUnfolder,
    Primitive,
    UnfoldedFactory,
    Unfolder,
)
import inspect
import logging
from pure_requests.basic import (
    Data,
    Endpoint,
    HttpClientFactory,
    Params,
)
from tap_zoho_crm._decode import (
    decode_json,
)
from tap_zoho_crm.api.common import (
    ApiBug,
)
from typing import (
    Dict,
)

ACCOUNTS_URL = "https://accounts.zoho.com"  # for US region
LOG = logging.getLogger(__name__)


def _decode(raw: JsonObj) -> ResultE[Token]:
    return JsonUnfolder.require(
        raw,
        "access_token",
        lambda v: Unfolder.to_primitive(v).bind(JsonPrimitiveUnfolder.to_str),
    ).map(Token)


def new_access_token(credentials: Credentials) -> Cmd[Token]:
    LOG.info("Generating access token")
    endpoint = Endpoint(f"{ACCOUNTS_URL}/oauth/v2/token")
    params: Dict[str, Primitive] = {
        "refresh_token": credentials.refresh_token,
        "client_id": credentials.client_id,
        "client_secret": credentials.client_secret,
        "grant_type": "refresh_token",
    }
    empty: JsonObj = FrozenDict({})
    client = HttpClientFactory.new_client(None, None, None)
    response = client.post(
        endpoint, Params(UnfoldedFactory.from_dict(params)), Data(empty)
    ).map(
        lambda r: ApiBug.assume_success(
            "NewTokenError.response",
            inspect.currentframe(),
            (str(client), endpoint.raw, str(params)),
            r.alt(cast_exception),
        )
    )
    data = response.map(
        lambda r: ApiBug.assume_success(
            "NewTokenError.decode_json",
            inspect.currentframe(),
            (str(client), endpoint.raw, str(params)),
            decode_json(r).alt(
                lambda c: c.map(
                    cast_exception,
                    lambda v: v.map(cast_exception, cast_exception),
                )
            ),
        )
    )

    return data.map(
        lambda j: ApiBug.assume_success(
            "NewTokenError.decode_json",
            inspect.currentframe(),
            (JsonUnfolder.dumps(j),),
            _decode(j),
        )
    )
