from ._decode import (
    decode_bulk_job,
)
from ._objs import (
    BulkJob,
    BulkJobId,
    BulkJobResult,
    ExpiredBulkJob,
    ModuleName,
)
from fa_purity import (
    cast_exception,
    Cmd,
    Coproduct,
    CoproductFactory,
    FrozenDict,
    Result,
    ResultE,
)
from fa_purity.json import (
    JsonObj,
    JsonPrimitiveUnfolder,
    JsonUnfolder,
    JsonValue,
    Primitive,
    UnfoldedFactory,
    Unfolder,
)
import inspect
import logging
from pure_requests.basic import (
    Endpoint,
    HttpClientFactory,
    Params,
)
from requests import (
    Response,
)
from tap_zoho_crm import (
    _decode,
)
from tap_zoho_crm.api.common import (
    API_URL,
    ApiBug,
    Token,
)
from typing import (
    Dict,
)

API_ENDPOINT = API_URL + "/crm/bulk/v2/read"
LOG = logging.getLogger(__name__)


def _require_first_item(raw: JsonObj) -> ResultE[JsonObj]:
    return JsonUnfolder.require(raw, "data", Unfolder.to_list).bind(
        lambda items: _decode.require_index(items, 0).bind(Unfolder.to_json)
    )


def _to_int(value: JsonValue) -> ResultE[int]:
    return Unfolder.to_primitive(value).bind(JsonPrimitiveUnfolder.to_int)


def _to_str(value: JsonValue) -> ResultE[str]:
    return Unfolder.to_primitive(value).bind(JsonPrimitiveUnfolder.to_str)


def _to_bool(value: JsonValue) -> ResultE[bool]:
    return Unfolder.to_primitive(value).bind(JsonPrimitiveUnfolder.to_bool)


def _decode_job_result(raw: JsonObj) -> ResultE[BulkJobResult]:
    return JsonUnfolder.require(raw, "page", _to_int).bind(
        lambda page: JsonUnfolder.require(raw, "count", _to_int).bind(
            lambda count: JsonUnfolder.require(
                raw, "download_url", _to_str
            ).bind(
                lambda download_url: JsonUnfolder.require(
                    raw, "more_records", _to_bool
                ).map(
                    lambda more_records: BulkJobResult(
                        page, count, download_url, more_records
                    )
                )
            )
        )
    )


def _decode_bulk_job(raw: JsonObj) -> ResultE[BulkJob]:
    module_page_result = JsonUnfolder.require(
        raw,
        "query",
        lambda j: Unfolder.to_json(j).bind(
            lambda v: JsonUnfolder.require(
                v,
                "module",
                lambda c: Unfolder.to_primitive(c)
                .bind(JsonPrimitiveUnfolder.to_str)
                .bind(ModuleName.from_raw),
            ).bind(
                lambda _module: JsonUnfolder.require(
                    v,
                    "page",
                    lambda x: Unfolder.to_primitive(x)
                    .bind(JsonPrimitiveUnfolder.to_int)
                    .map(lambda page: (_module, page)),
                ),
            )
        ),
    )
    bulk_result = JsonUnfolder.optional(
        raw, "result", lambda v: Unfolder.to_json(v).bind(_decode_job_result)
    )
    return bulk_result.bind(
        lambda r: module_page_result.bind(
            lambda t: decode_bulk_job(raw, t[0], t[1], r.value_or(None))
        )
    )


def _handle_expired(
    job_id: BulkJobId, response: Response
) -> ResultE[ExpiredBulkJob]:
    if response.status_code == 404:
        return Result.success(ExpiredBulkJob(job_id))
    return Result.failure(
        ValueError("Not an `ExpiredBulkJob`"), ExpiredBulkJob
    ).alt(cast_exception)


def _get_bulk_job(response: Response) -> ResultE[BulkJob]:
    json_data = _decode.decode_json(response).alt(
        lambda c: c.map(
            cast_exception,
            lambda x: x.map(cast_exception, cast_exception),
        )
    )
    return json_data.bind(_require_first_item).bind(_decode_bulk_job)


def get_bulk_job(
    token: Token, job_id: BulkJobId
) -> Cmd[Coproduct[BulkJob, ExpiredBulkJob]]:
    msg = Cmd.wrap_impure(lambda: LOG.info("API: Get bulk job #%s", job_id))
    endpoint = Endpoint(f"{API_ENDPOINT}/" + job_id.job_id)
    headers: Dict[str, Primitive] = {
        "Authorization": "Zoho-oauthtoken " + token.raw_token
    }
    empty: JsonObj = FrozenDict({})
    client = HttpClientFactory.new_client(
        None, UnfoldedFactory.from_dict(headers), None
    )
    factory: CoproductFactory[BulkJob, ExpiredBulkJob] = CoproductFactory()
    response = (
        client.get(endpoint, Params(empty))
        .map(
            lambda r: ApiBug.assume_success(
                "BulkGetError.response",
                inspect.currentframe(),
                (endpoint.raw,),
                r.alt(cast_exception),
            )
        )
        .map(
            lambda r: _handle_expired(job_id, r)
            .map(factory.inr)
            .lash(lambda _: _get_bulk_job(r).map(factory.inl))
        )
    )
    return msg + response.map(
        lambda r: ApiBug.assume_success(
            "BulkGetError.transform",
            inspect.currentframe(),
            (endpoint.raw,),
            r,
        )
    )
