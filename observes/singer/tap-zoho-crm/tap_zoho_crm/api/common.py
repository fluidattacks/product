from dataclasses import (
    dataclass,
    field,
)
from fa_purity import (
    FrozenList,
    ResultE,
)
import inspect
import sys
from types import (
    FrameType,
)
from typing import (
    Generic,
    NoReturn,
    TypeVar,
)

API_URL = "https://www.zohoapis.com"
_T = TypeVar("_T")


@dataclass(frozen=True)
class Token:
    raw_token: str

    def __repr__(self) -> str:
        return "[masked]"


@dataclass
class _Private:
    pass


@dataclass
class ApiBug(Exception, Generic[_T]):
    """
    Equivalent to `NoReturn`, therefore no instance
    can be created without terminating the program
    """

    _private: _Private = field(repr=False, hash=False, compare=False)
    obj_id: str
    location: str
    parent_error: Exception | None
    context: FrozenList[str]

    @staticmethod
    def new(
        obj_id: str,
        location: str,
        parent_error: Exception | None,
        context: FrozenList[str],
    ) -> NoReturn:
        sys.tracebacklimit = 0
        raise ApiBug(
            _Private(),
            obj_id,
            location,
            parent_error,
            context,
        )

    @classmethod
    def assume_success(
        cls,
        name: str,
        location: FrameType | None,
        context: FrozenList[str],
        result: ResultE[_T],
    ) -> _T | NoReturn:
        return result.alt(
            lambda e: cls.new(
                name,
                str(inspect.getframeinfo(location))
                if location is not None
                else "?? Unknown ??",
                e,
                context,
            )
        ).to_union()

    def __str__(self) -> str:
        return (
            "\n"
            + "-" * 30
            + "\n [Id] "
            + self.obj_id
            + "\n [Location] "
            + self.location
            + "\n [Parent] "
            + str(self.parent_error)
            + "\n [Context] "
            + str(self.context)
        )


@dataclass(frozen=True)
class PageIndex:
    page: int
    per_page: int


@dataclass(frozen=True)
class DataPageInfo:
    page: PageIndex
    n_items: int
    more_records: bool


__all__ = ["Token"]
