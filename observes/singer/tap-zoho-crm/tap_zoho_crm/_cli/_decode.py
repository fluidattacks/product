from fa_purity import (
    ResultE,
)
from fa_purity.json import (
    JsonObj,
    JsonPrimitiveUnfolder,
    JsonUnfolder,
    UnfoldedFactory,
    Unfolder,
)
from tap_zoho_crm.api.auth import (
    Credentials,
)
from typing import (
    IO,
)


def _decode_zoho_creds(raw: JsonObj) -> ResultE[Credentials]:
    client_id = JsonUnfolder.require(
        raw, "client_id", Unfolder.to_primitive
    ).bind(JsonPrimitiveUnfolder.to_str)
    client_secret = JsonUnfolder.require(
        raw, "client_secret", Unfolder.to_primitive
    ).bind(JsonPrimitiveUnfolder.to_str)
    refresh_token = JsonUnfolder.require(
        raw, "refresh_token", Unfolder.to_primitive
    ).bind(JsonPrimitiveUnfolder.to_str)
    scopes_result = JsonUnfolder.require(
        raw,
        "scopes",
        lambda i: Unfolder.to_list_of(
            i,
            lambda x: Unfolder.to_primitive(x).bind(
                JsonPrimitiveUnfolder.to_str
            ),
        ),
    )
    return client_id.bind(
        lambda cid: client_secret.bind(
            lambda secret: refresh_token.bind(
                lambda token: scopes_result.map(
                    lambda scopes: Credentials(
                        cid, secret, token, frozenset(scopes)
                    )
                )
            )
        )
    )


def decode_zoho_creds(auth_file: IO[str]) -> ResultE[Credentials]:
    return UnfoldedFactory.load(auth_file).bind(_decode_zoho_creds)
