from . import (
    _decode,
)
import click
from fa_purity import (
    Cmd,
    Unsafe,
)
from tap_zoho_crm import (
    streams,
)
from tap_zoho_crm.api.bulk import (
    ModuleName,
)
from typing import (
    IO,
    NoReturn,
)
from utils_logger import (
    start_session,
)


@click.command()
@click.argument("crm_auth_file", type=click.File("r", "utf-8"))
def stream_modules(
    crm_auth_file: IO[str],
) -> NoReturn:
    crm_creds = (
        _decode.decode_zoho_creds(crm_auth_file)
        .alt(Unsafe.raise_exception)
        .to_union()
    )
    target_modules = frozenset(ModuleName) - frozenset(
        [
            ModuleName.VENDORS,
            ModuleName.SOLUTIONS,
            ModuleName.CASES,
        ]
    )
    cmd: Cmd[None] = start_session() + streams.stream_data(
        crm_creds,
        target_modules,
    )
    cmd.compute()
