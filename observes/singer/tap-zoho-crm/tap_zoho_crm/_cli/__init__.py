from . import (
    _decode,
    _stream,
)
import click
from fa_purity import (
    Cmd,
    Unsafe,
)
from tap_zoho_crm.api.auth import (
    AuthApiFactory,
    Credentials,
)
from typing import (
    IO,
    NoReturn,
)
from utils_logger import (
    start_session,
)


@click.command()
@click.argument("crm_auth_file", type=click.File("r", "utf-8"))
def gen_refresh_token(crm_auth_file: IO[str]) -> NoReturn:
    # Manual refresh token generation, see:
    # https://www.zoho.com/crm/developer/docs/api/v2/auth-request.html
    creds = (
        _decode.decode_zoho_creds(crm_auth_file)
        .alt(Unsafe.raise_exception)
        .to_union()
    )
    cmd: Cmd[None] = start_session() + AuthApiFactory.auth_api(
        creds
    ).manual_new_refresh_token.map(
        lambda x: print("The token: " + x.raw_token)
    )
    cmd.compute()


@click.command()
def revoke_token() -> NoReturn:
    # Manual refresh token revoke
    fake_creds = Credentials("", "", "", frozenset())
    cmd: Cmd[None] = start_session() + AuthApiFactory.auth_api(
        fake_creds
    ).manual_revoke_token.map(print)
    cmd.compute()


@click.group()
def main() -> None:  # decorator make it to return `NoReturn`
    # cli group entrypoint
    pass


main.add_command(gen_refresh_token)
main.add_command(revoke_token)
main.add_command(_stream.stream_modules)
