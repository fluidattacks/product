from etl_utils.retry import (
    cmd_if_fail,
    MaxRetriesReached,
    retry_cmd,
    sleep_cmd,
)
from fa_purity import (
    Cmd,
    Coproduct,
    Result,
    ResultFactory,
)
import logging
from tap_zoho_crm.api.bulk import (
    BulkData,
    BulkJobApi,
    BulkJobId,
    BulkJobObj,
    ExpiredBulkJob,
    ModuleName,
)

LOG = logging.getLogger(__name__)


def _job_completed(
    client: BulkJobApi, job: BulkJobId
) -> Cmd[Result[bool, ExpiredBulkJob]]:
    "Procedure to check if a bulk job is ready to download"
    return client.get(job).map(
        lambda c: c.map(
            lambda j: Result.success(
                j.state.upper() == "COMPLETED", ExpiredBulkJob
            ),
            lambda e: Result.failure(e, bool),
        )
    )


def _retry_result(
    result: Result[bool, ExpiredBulkJob]
) -> Result[Coproduct[None, ExpiredBulkJob], None]:
    """
    - transform bool: true -> success, false -> failure
    - transform ExpiredBulkJob into success (since it will be not retried)
    """
    _factory: ResultFactory[
        Coproduct[None, ExpiredBulkJob], None
    ] = ResultFactory()
    return result.to_coproduct().map(
        lambda b: _factory.success(Coproduct.inl(None))
        if b
        else _factory.failure(None),
        lambda e: _factory.success(Coproduct.inr(e)),
    )


def _top_truncation(value: float, limit: float) -> float:
    if value > limit:
        return limit
    return value


def _waiting_msg(job: BulkJobId, time: float) -> Cmd[None]:
    return Cmd.wrap_impure(
        lambda: LOG.info(
            "Waiting bulk job %s to be ready (%ss)", job.job_id, int(time)
        )
    )


def _wait_job(retry: int, job: BulkJobId) -> Cmd[None]:
    wait_time = _top_truncation(60 * retry, 5 * 50)
    return _waiting_msg(job, wait_time) + sleep_cmd(wait_time)


def download_job_data(
    client: BulkJobApi, job: BulkJobId
) -> Cmd[Result[BulkData, Coproduct[ExpiredBulkJob, MaxRetriesReached]]]:
    _factory: ResultFactory[
        BulkData, Coproduct[ExpiredBulkJob, MaxRetriesReached]
    ] = ResultFactory()
    return retry_cmd(
        _job_completed(client, job).map(_retry_result),
        lambda i, r: cmd_if_fail(r, _wait_job(i, job)),
        10,
    ).bind(
        lambda r: r.to_coproduct().map(
            lambda c: c.map(
                lambda _: client.download(job).map(
                    _factory.success
                ),  # bulk job is ready to download
                lambda e: Cmd.wrap_value(
                    _factory.failure(Coproduct.inl(e))
                ),  # bulk job expired
            ),
            lambda m: Cmd.wrap_value(
                _factory.failure(Coproduct.inr(m))
            ),  # max retries reached
        )
    )


def get_module_data(
    client: BulkJobApi, module: ModuleName
) -> Cmd[Result[BulkData, Coproduct[ExpiredBulkJob, MaxRetriesReached]]]:
    msg = Cmd.wrap_impure(
        lambda: LOG.info("Creating bulk job for module `%s`", module)
    )

    def created_msg(job: BulkJobObj) -> Cmd[None]:
        return Cmd.wrap_impure(lambda: LOG.info("Bulk job created!\n%s", job))

    return msg + client.new(module, 1).bind(
        lambda j: created_msg(j) + download_job_data(client, j.job_id)
    )
