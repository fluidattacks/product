{ inputs, makeScript, projectPath, ... }@makes_inputs:
let
  root = projectPath inputs.observesIndex.tap.zoho_crm.root;
  bundle = import "${root}/entrypoint.nix" makes_inputs;
  env = bundle.env.runtime;
  common_vars = import (projectPath "/observes/common/common_vars.nix") {
    inherit projectPath;
  };
in makeScript {
  name = "tap-zoho-crm";
  searchPaths = {
    bin = [ env ];
    export = common_vars;
  };
  entrypoint = ''tap-zoho-crm "''${@}"'';
}
