import click
from datetime import (
    datetime,
)
import json
import sys
from tap_batch import (
    logs,
)
from tap_batch.batch import (
    BatchExtractor,
    JobRecord,
)
from typing import (
    NoReturn,
    TypedDict,
)

_TYPE_DATE = {"type": "string", "format": "date-time"}
_TYPE_STRING = {"type": "string"}


class SchemaProperties(TypedDict):
    properties: dict[str, dict[str, str]]


class Schema(TypedDict):
    key_properties: list[str]
    schema: SchemaProperties
    stream: str
    type: str


def get_schema() -> str:
    schema: Schema = {
        "type": "SCHEMA",
        "stream": "executions",
        "key_properties": ["id"],
        "schema": {
            "properties": {
                "id": _TYPE_STRING,
                "name": _TYPE_STRING,
                "created_at": _TYPE_DATE,
                "started_at": _TYPE_DATE,
                "stopped_at": _TYPE_DATE,
                "status": _TYPE_STRING,
                "status_reason": _TYPE_STRING,
            }
        },
    }

    return json.dumps(schema)


class Record(TypedDict):
    record: dict[str, str]
    stream: str
    type: str


def encode_record(metric_record: JobRecord) -> str:
    record: Record = {
        "type": "RECORD",
        "stream": "executions",
        "record": {
            k: (
                v
                if not isinstance(v, datetime)
                else datetime.strftime(v, "%Y-%m-%dT%H:%M:%S.%fZ")
            )
            for k, v in metric_record._asdict().items()
        },
    }

    return json.dumps(record)


class State(TypedDict):
    type: str
    value: dict[str, str]


def get_state(job_record: JobRecord) -> str:
    state: State = {
        "type": "STATE",
        "value": {
            "created_at": datetime.strftime(
                job_record.created_at, "%Y-%m-%dT%H:%M:%S.%fZ"
            )
        },
    }

    return json.dumps(state)


@click.command()
@click.option(
    "--state",
    type=str,
    required=False,
    default=None,
    help="json file S3 URI; e.g. s3://mybucket/folder/state.json",
)
@click.option(
    "--queue",
    type=str,
    required=True,
    help="queue to extract information from",
)
def main(
    queue: str,
    state: str | None,
) -> NoReturn:
    batch_extractor = BatchExtractor.new(queue=queue, state=state)
    logs.info(get_schema())
    jobs = batch_extractor.get_jobs()
    for record in iter(jobs):
        logs.info(encode_record(record))
    if len(jobs) > 0:
        logs.info(get_state(jobs[-1]))
    sys.exit(0)
