from dataclasses import (
    dataclass,
)
from tap_gitlab_dora.api import (
    GitlabApi,
)
from tap_gitlab_dora.compute import (
    Metrics,
)
from tap_gitlab_dora.types import (
    Issue,
    MergeRequest,
)


@dataclass(frozen=True)
class Factory:
    @staticmethod
    def default_api(token: str) -> GitlabApi:
        """
        Creates a default instance of the GitlabApi class with a predefined
        base URL.

        Args:
            token (str): The API token to use for authentication.

        Returns:
            GitlabApi: An instance of GitlabApi with the given token and a
            predefined base URL ("https://gitlab.com/api/v4/projects/20741933").
        """
        return GitlabApi(token, "https://gitlab.com/api/v4/projects/20741933")

    @staticmethod
    def default_metrics(
        issues: list[Issue], merges: list[MergeRequest]
    ) -> Metrics:
        """
        Creates a default instance of the Metrics class using the provided
        issues and merge requests.

        Args:
            issues (list[Issue]): A list of issues to be used for metric
            calculations.
            merges (list[MergeRequest]): A list of merge requests to be used
            for metric calculations.

        Returns:
            Metrics: An instance of Metrics initialized with the provided
            issues and merge requests.
        """
        return Metrics(issues, merges)
