class APIError(Exception):
    def __init__(self) -> None:
        message = "There was an error in the call to the API"
        super().__init__(message)


class IssueDatesError(Exception):
    def __init__(self) -> None:
        message = """
        The value of created_at in a list of closed issues cannot be None.
        """
        super().__init__(message)


class MergeRequestDatesError(Exception):
    def __init__(self) -> None:
        message = """
        The value of merged_at in a list of merged requests cannot be None.
        """
        super().__init__(message)
