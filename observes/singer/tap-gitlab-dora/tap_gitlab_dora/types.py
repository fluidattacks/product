from typing import (
    NamedTuple,
    TypedDict,
)

_TYPE_STRING = {"type": "string"}
_TYPE_NUMBER = {"type": "number"}

GitlabApiResult = dict[str, bool | float | int | str]


class APIResponse(NamedTuple):
    status_code: int | None
    response: list[GitlabApiResult]


class Issue(NamedTuple):
    state: str
    created_at: str
    closed_at: str | None
    labels: str


class MergeRequest(NamedTuple):
    state: str
    created_at: str
    merged_at: str | None


class Deployment(NamedTuple):
    date: str
    value: int


class Record(TypedDict):
    record: dict[str, float | str]
    stream: str
    type: str


class SchemaProperties(TypedDict):
    properties: dict[str, dict[str, str]]


class Schema(TypedDict):
    key_properties: list[str]
    schema: SchemaProperties
    stream: str
    type: str


class MetricsResponse(NamedTuple):
    last_day_deploys: int
    total_deploys: int
    daily_deployment_frequency: str
    average_deployment_time: str
    CFR: str
    MTR: str
