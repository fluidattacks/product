from dataclasses import (
    dataclass,
)
from datetime import (
    datetime,
    timedelta,
    timezone,
)
import json
from tap_gitlab_dora.types import (
    _TYPE_NUMBER,
    _TYPE_STRING,
    Issue,
    MergeRequest,
    MetricsResponse,
    Record,
    Schema,
)


@dataclass(frozen=True)
class Utils:
    @staticmethod
    def get_record(metrics: MetricsResponse, request_time: str) -> str:
        """
        Generates a JSON-encoded record from the provided metrics and request
            time.

        Args:
            metrics (MetricsResponse): The metrics data to include in the
                record.
            request_time (str): The time when the request was made, formatted
                as a string.

        Returns:
            str: A JSON-encoded string representing the record.
        """
        record: Record = {
            "type": "RECORD",
            "stream": "DORA_Metrics",
            "record": {
                "request_time": request_time,
                "last_day_deploys": metrics.last_day_deploys,
                "total_deploys": metrics.total_deploys,
                "daily_deployment_frequency": metrics.daily_deployment_frequency,
                "average_deployment_time": metrics.average_deployment_time,
                "CFR": metrics.CFR,
                "MTR": metrics.MTR,
            },
        }
        return json.dumps(record)

    @staticmethod
    def get_schema() -> str:
        """
        Generates a JSON-encoded schema for the DORA metrics stream.

        Returns:
            str: A JSON-encoded string representing the schema.
        """
        schema: Schema = {
            "type": "SCHEMA",
            "stream": "DORA_Metrics",
            "key_properties": ["id"],
            "schema": {
                "properties": {
                    "request_time": _TYPE_STRING,
                    "last_day_deploys": _TYPE_NUMBER,
                    "total_deploys": _TYPE_NUMBER,
                    "daily_deployment_frequency": _TYPE_STRING,
                    "average_deployment_time": _TYPE_STRING,
                    "CFR": _TYPE_STRING,
                    "MTR": _TYPE_STRING,
                }
            },
        }
        return json.dumps(schema)

    @staticmethod
    def format_time(seconds: float) -> str:
        """
        Formats a duration in seconds into a string in the format
        "DD:HH:MM:SS".

        Args:
            seconds (float): The duration in seconds to format.

        Returns:
            str: The formatted time string.
        """
        days, remainder = divmod(seconds, 86400)
        hours, remainder = divmod(remainder, 3600)
        minutes, seconds = divmod(remainder, 60)

        formatted_time = (
            f"{int(days):02d}:{int(hours):02d}:{int(minutes):02d}:"
            f"{int(seconds):02d}"
        )
        return formatted_time

    @staticmethod
    def get_median(failures: list[int]) -> str:
        """
        Calculates the median of a list of failure durations and formats it.

        Args:
            failures (list[int]): A list of failure durations in seconds.

        Returns:
            str: The median value formatted as a time string.
        """
        mid_point = len(failures) // 2
        sorted_failures = sorted(failures)
        if len(failures) % 2 == 0:
            median_value = (
                sorted_failures[mid_point - 1] + sorted_failures[mid_point]
            ) / 2
        else:
            median_value = sorted_failures[mid_point]
        return Utils.format_time(median_value)

    @staticmethod
    def filter_issues(issue: Issue) -> bool:
        """
        Determines if an issue should be considered as a failure based on its
        state and labels.

        Args:
            issue (Issue): The issue to evaluate.

        Returns:
            bool: True if the issue is closed and labeled as `type::bug` or
                `type::incident`; otherwise, False.
        """
        return issue.state == "closed" and (
            "type::bug" in issue.labels or "type::incident" in issue.labels
        )

    @staticmethod
    def filter_merged_merge_request(merge_request: MergeRequest) -> bool:
        """
        Determines if a merge request is considered merged based on its state.

        Args:
            merge_request (MergeRequest): The merge request to evaluate.

        Returns:
            bool: True if the merge request is in the "merged" state;
                otherwise, False.
        """
        return merge_request.state == "merged"

    def get_failures(self, issues: list[Issue]) -> list[Issue]:
        """
        Filters the list of issues to include only those that are considered
        failures based on specific criteria.

        Args:
            issues (list[Issue]): A list of issues to filter.

        Returns:
            list[Issue]: A list of issues that match the failure criteria.
        """
        return [issue for issue in issues if self.filter_issues(issue)]

    @staticmethod
    def get_last_day_deployments(
        merged_merge_requests: list[MergeRequest], date: datetime
    ) -> list[MergeRequest]:
        """
        Filters the list of merged merge requests to include only those that
        were merged within the last day relative to the specified date.

        Args:
            merged_merge_requests (list[MergeRequest]): A list of merged merge
                requests to filter.
            date (datetime): The reference date to use for filtering.

        Returns:
            list[MergeRequest]: A list of merge requests merged within the last
                day.
        """
        date = date.replace(tzinfo=timezone.utc)
        start_of_yesterday = date - timedelta(days=1)
        return [
            merge_request
            for merge_request in merged_merge_requests
            if merge_request.merged_at
            and start_of_yesterday
            <= datetime.fromisoformat(merge_request.merged_at)
            < date
        ]

    def get_deployments(
        self, merge_requests: list[MergeRequest]
    ) -> list[MergeRequest]:
        """
        Filters the list of merge requests to include only those that are in
        the "merged" state.

        Args:
            merge_requests (list[MergeRequest]): A list of merge requests to
                filter.

        Returns:
            list[MergeRequest]: A list of merged merge requests.
        """
        return [
            merge_request
            for merge_request in merge_requests
            if self.filter_merged_merge_request(merge_request)
        ]
