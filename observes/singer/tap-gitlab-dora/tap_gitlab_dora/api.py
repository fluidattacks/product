from dataclasses import (
    dataclass,
)
from datetime import (
    datetime,
)
import json
import sys
from tap_gitlab_dora import (
    logs,
)
from tap_gitlab_dora.exceptions import (
    APIError,
)
from tap_gitlab_dora.types import (
    APIResponse,
    GitlabApiResult,
    Issue,
    MergeRequest,
)
from typing import (
    Callable,
    Generator,
    NoReturn,
    TypeVar,
)
import urllib.error
import urllib.request

T = TypeVar("T", Issue, MergeRequest)


@dataclass(frozen=True)
class GitlabApi:
    api_token: str
    base_url: str

    @staticmethod
    def _build_api_path(base_path: str, since: datetime) -> str:
        """
        Builds the API path to query resources created after a specific date.

        Args:
            base_path (str): The base API path.
            since (datetime): The date and time after which resources should be
                retrieved.

        Returns:
            str: The API path with the required query parameters.
        """
        since_str = datetime.strftime(since, "%Y-%m-%dT%H:%M:%S.%fZ")
        return f"{base_path}created_after={since_str}&per_page=100".replace(
            " ", "%20"
        )

    @staticmethod
    def _handle_http_error(error: urllib.error.HTTPError) -> NoReturn:
        """
        Handles different HTTP errors.

        Args:
            error (urllib.error.HTTPError): The HTTP error to handle.

        Raises:
            APIError: For HTTP errors not specifically handled.
        """
        if error.code == 401:
            logs.error("Invalid token or credentials")
            sys.exit(1)
        elif error.code == 403:
            logs.error("Unauthorized/Forbidden")
            sys.exit(1)
        else:
            logs.error(f"HTTP Error: {error.code}")
            raise APIError()

    @staticmethod
    def _clean_merge_request(merge_request: GitlabApiResult) -> MergeRequest:
        """
        Cleans and transforms a raw merge request response into a MergeRequest
        object.

        Args:
            merge_request (GitlabApiResult): The raw merge request data.

        Returns:
            MergeRequest: The cleaned MergeRequest object.
        """
        return MergeRequest(
            state=str(merge_request["state"]),
            created_at=str(merge_request["created_at"]),
            merged_at=str(merge_request["merged_at"]),
        )

    @staticmethod
    def _clean_issue(issue: GitlabApiResult) -> Issue:
        """
        Cleans and transforms a raw issue response into an Issue object.

        Args:
            issue (GitlabApiResult): The raw issue data.

        Returns:
            Issue: The cleaned Issue object.
        """
        return Issue(
            state=str(issue["state"]),
            created_at=str(issue["created_at"]),
            closed_at=str(issue["closed_at"]),
            labels=str(issue["labels"]),
        )

    def _request(self, path: str) -> APIResponse:
        """
        Sends a request to the specified API path and retrieves the response.

        Args:
            path (str): The API path to request.

        Returns:
            APIResponse: The response containing the status code and data.

        Raises:
            APIError: If an HTTP error occurs.
        """
        try:
            resource = self.base_url + path
            headers = {
                "Authorization": f"Bearer {self.api_token}",
                "User-Agent": (
                    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) "
                    "AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.2.1 "
                    "Safari/605.1.15"
                ),
            }
            request = urllib.request.Request(resource, headers=headers)

            with urllib.request.urlopen(request, timeout=120) as result:  # type: ignore[misc]
                return APIResponse(
                    status_code=result.status,  # type: ignore[misc]
                    response=json.loads(result.read().decode("utf-8")),  # type: ignore[misc]
                )
        except urllib.error.HTTPError as error:
            self._handle_http_error(error)

    def _paginator_api(
        self, path: str
    ) -> Generator[list[GitlabApiResult], None, None]:
        """
        Paginates through API results.

        Args:
            path (str): The API path to request.

        Yields:
            Generator[list[GitlabApiResult], None, None]: A generator yielding
            pages of API results.
        """
        page = 1
        while True:
            paginated_path = f"{path}&page={page}"
            response = self._request(paginated_path)
            if response.status_code != 200:
                raise APIError()
            if not response.response:
                break
            yield response.response
            page += 1

    def _fetch_and_clean_data(
        self, path: str, cleaner: Callable[[GitlabApiResult], T]
    ) -> list[T]:
        """
        Fetches data from the API and cleans it using the provided cleaner
        function.

        Args:
            path (str): The API path to request.
            cleaner (Callable[[GitlabApiResult], list[MergeRequest | Issue]]):
                A function to clean and transform the raw API data.

        Returns:
            list[MergeRequest | Issue]: A list of cleaned data objects.
        """
        cleaned_data_list: list[T] = []
        for raw_data in self._paginator_api(path):
            cleaned_data_list.extend(cleaner(item) for item in raw_data)
        return cleaned_data_list

    def get_merge_request(self, since: datetime) -> list[MergeRequest]:
        """
        Retrieves and cleans merge requests created after a specific date.

        Args:
            since (datetime): The date and time after which merge requests
                should be retrieved.

        Returns:
            list[MergeRequest]: A list of cleaned MergeRequest objects.
        """
        api_path = self._build_api_path("/merge_requests?", since)
        return self._fetch_and_clean_data(api_path, self._clean_merge_request)

    def get_issues(self, since: datetime) -> list[Issue]:
        """
        Retrieves and cleans issues created after a specific date.

        Args:
            since (datetime): The date and time after which issues should be
                retrieved.

        Returns:
            list[Issue]: A list of cleaned Issue objects.
        """
        api_path = self._build_api_path("/issues?", since)
        return self._fetch_and_clean_data(api_path, self._clean_issue)
