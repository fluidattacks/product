{ makes_inputs, nixpkgs, python_version, src, }:
let
  deps = import ./deps { inherit makes_inputs nixpkgs python_version; };
  build_required_deps = python_pkgs: {
    runtime_deps = with python_pkgs; [
      click
      etl-utils
      fa-purity
      fa-singer-io
      pure-requests
      python-dateutil
      requests
      types-python-dateutil
      types-requests
      utils-logger
    ];
    build_deps = with python_pkgs; [ flit-core ];
    test_deps = with python_pkgs; [
      arch-lint
      mypy
      pytest
      pytest-cov
      pylint
      ruff
    ];
  };
  bundle_builder = lib: pkgDeps:
    makes_inputs.makePythonPyprojectPackage {
      inherit (lib) buildEnv buildPythonPackage;
      inherit pkgDeps src;
    };
  build_bundle = builder:
    # builder: Deps -> (PythonPkgs -> PkgDeps) -> (Deps -> PkgDeps -> Bundle) -> Bundle
    # Deps: are the default project dependencies
    # PythonPkgs -> PkgDeps: is the required dependencies builder
    # Deps -> PkgDeps -> Bundle: is the bundle builder
    builder deps build_required_deps bundle_builder;
  bundle = build_bundle (default: required_deps: builder:
    builder default.lib (required_deps default.python_pkgs));
in bundle // { inherit build_bundle; }
