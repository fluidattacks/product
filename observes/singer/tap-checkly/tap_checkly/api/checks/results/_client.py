from dataclasses import (
    dataclass,
)
from datetime import (
    datetime,
)

from etl_utils.typing import (
    Callable,
    TypeVar,
)
from fa_purity import (
    Cmd,
    FrozenList,
    Maybe,
    PureIter,
    PureIterFactory,
    ResultTransform,
    Stream,
    StreamFactory,
    StreamTransform,
    Unsafe,
)
from fa_purity.json import (
    UnfoldedFactory,
)

from tap_checkly.api import (
    _utils,
)
from tap_checkly.api._raw import (
    RawClient,
)
from tap_checkly.api.checks.results import (
    _decode,
)
from tap_checkly.objs import (
    CheckId,
    CheckResultObj,
)

from .time_range import (
    DateRange,
    date_ranges_dsc,
)

_T = TypeVar("_T")


@dataclass(frozen=True)
class CheckResultClient:
    _client: RawClient
    _check: CheckId
    _per_page: int
    _from_date: datetime
    _to_date: datetime

    def _list_per_date_range(
        self,
        page: int,
        date_range: DateRange,
    ) -> Cmd[FrozenList[CheckResultObj]]:
        return self._client.new_get_list(
            "/v1/check-results/" + self._check.id_str,
            UnfoldedFactory.from_dict(
                {
                    "limit": self._per_page,
                    "page": page,
                    "from": str(int(datetime.timestamp(date_range.from_date))),
                    "to": str(int(datetime.timestamp(date_range.to_date))),
                },
            ),
        ).map(
            lambda items: ResultTransform.all_ok(
                PureIterFactory.from_list(items)
                .map(lambda i: _decode.from_raw_obj(self._check, i))
                .to_list(),
            )
            .alt(Unsafe.raise_exception)
            .to_union(),
        )

    def _date_ranges(self) -> PureIter[DateRange]:
        return date_ranges_dsc(self._from_date, self._to_date)

    def _list_all(
        self,
        date_range: DateRange,
        list_cmd: Callable[[int, DateRange], Cmd[FrozenList[_T]]],
    ) -> Cmd[Maybe[Stream[_T]]]:
        first = list_cmd(1, date_range)
        return first.map(
            lambda i: Maybe.from_optional(i if bool(i) else None).map(
                lambda f: _utils.paginate_all(
                    lambda p: list_cmd(p, date_range) if p > 1 else Cmd.wrap_value(f),
                ),
            ),
        )

    def list_all(self) -> Stream[CheckResultObj]:
        return (
            self._date_ranges()
            .map(lambda d: self._list_all(d, self._list_per_date_range))
            .transform(lambda x: StreamFactory.from_commands(x))
            .transform(lambda x: StreamTransform.until_empty(x))
            .bind(lambda s: s)
        )
