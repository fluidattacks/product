from etl_utils.typing import (
    Tuple,
)
from fa_purity import (
    FrozenDict,
    FrozenList,
    Maybe,
    PureIterFactory,
    PureIterTransform,
    Result,
    ResultE,
    ResultTransform,
    cast_exception,
)
from fa_purity.json import (
    JsonObj,
    JsonUnfolder,
    JsonValue,
    Unfolder,
)

from tap_checkly.api import (
    _utils,
)
from tap_checkly.objs.result import (
    BrowserCheckResult,
    TraceSummary,
    WebVitalMetric,
    WebVitals,
)


def _decode_metric(raw: JsonObj) -> ResultE[Maybe[WebVitalMetric]]:
    _score = (
        JsonUnfolder.optional(
            raw,
            "score",
            _utils.to_str,
        )
        .alt(lambda e: ValueError(f"Error decoding `score` i.e. {e}"))
        .alt(cast_exception)
    )
    _value = (
        JsonUnfolder.optional(
            raw,
            "value",
            lambda v: _utils.to_float(v).lash(
                lambda _: _utils.to_int(v).map(float),
            ),
        )
        .alt(lambda e: ValueError(f"Error decoding `value` i.e. {e}"))
        .alt(cast_exception)
    )

    def _from_values(
        score: Maybe[str],
        value: Maybe[float],
    ) -> ResultE[Maybe[WebVitalMetric]]:
        if score.value_or(None) is None and value.value_or(None) is None:
            return Result.success(Maybe.empty(WebVitalMetric), Exception)
        return (
            score.bind(
                lambda s: value.map(
                    lambda v: Maybe.some(WebVitalMetric(s, v)),
                ),
            )
            .to_result()
            .alt(
                lambda _: ValueError(
                    "One of `score` or `value` attributes on `WebVitalMetric` is `None`",
                ),
            )
            .alt(cast_exception)
        )

    return _score.bind(lambda s: _value.bind(lambda v: _from_values(s, v)))


def _get_metric(raw: JsonObj, key: str) -> ResultE[Maybe[WebVitalMetric]]:
    return (
        JsonUnfolder.optional(
            raw,
            key,
            lambda v: Unfolder.to_json(v).bind(_decode_metric),
        )
        .map(lambda m: m.bind(lambda x: x))
        .alt(lambda e: ValueError(f"Error decoding metric i.e. {e}"))
    )


def _decode_vitals(raw: JsonObj) -> ResultE[WebVitals]:
    def _metric(key: str) -> ResultE[Maybe[WebVitalMetric]]:
        return _get_metric(raw, key)

    return (
        _metric("CLS")
        .bind(
            lambda _cls: _metric("FCP").bind(
                lambda fcp: _metric("LCP").bind(
                    lambda lcp: _metric("TBT").bind(
                        lambda tbt: _metric("TTFB").map(
                            lambda ttfb: WebVitals(_cls, fcp, lcp, tbt, ttfb),
                        ),
                    ),
                ),
            ),
        )
        .alt(lambda e: ValueError(f"Error decoding `WebVitals` i.e. {e}"))
    )


def _decode_summary(raw: JsonObj) -> ResultE[TraceSummary]:
    return (
        JsonUnfolder.require(raw, "consoleErrors", _utils.to_int)
        .bind(
            lambda console: JsonUnfolder.require(
                raw,
                "networkErrors",
                _utils.to_int,
            ).bind(
                lambda network: JsonUnfolder.require(
                    raw,
                    "documentErrors",
                    _utils.to_int,
                ).bind(
                    lambda document: JsonUnfolder.require(
                        raw,
                        "userScriptErrors",
                        _utils.to_int,
                    ).map(
                        lambda user_script: TraceSummary(
                            console,
                            network,
                            document,
                            user_script,
                        ),
                    ),
                ),
            ),
        )
        .alt(lambda e: ValueError(f"Error decoding `TraceSummary` i.e. {e}"))
    )


def _decode_pages(
    raw: FrozenList[JsonValue],
) -> ResultE[FrozenDict[str, WebVitals]]:
    def _decode(j: JsonObj) -> ResultE[Maybe[ResultE[Tuple[str, WebVitals]]]]:
        return JsonUnfolder.optional(j, "url", _utils.to_str).map(
            lambda _url: _url.map(
                lambda url: JsonUnfolder.require(
                    j,
                    "webVitals",
                    Unfolder.to_json,
                )
                .bind(_decode_vitals)
                .map(lambda w: (url, w)),
            ),
        )

    return (
        ResultTransform.all_ok(
            PureIterFactory.from_list(raw)
            .map(lambda v: Unfolder.to_json(v).bind(_decode))
            .to_list(),
        )
        .bind(
            lambda i: ResultTransform.all_ok(
                PureIterTransform.filter_maybe(
                    PureIterFactory.from_list(i),
                ).to_list(),
            ),
        )
        .map(lambda x: FrozenDict(dict(x)))
    ).alt(lambda e: ValueError(f"Error decoding pages i.e. {e}"))


def _force_errors_to_str(item: FrozenList[JsonValue]) -> FrozenList[str]:
    return tuple(str(i) for i in item)


def decode_browser_result(raw: JsonObj) -> ResultE[BrowserCheckResult]:
    empty_pages: FrozenDict[str, WebVitals] = FrozenDict({})
    return (
        JsonUnfolder.require(raw, "type", _utils.to_str)
        .bind(
            lambda framework: JsonUnfolder.require(
                raw,
                "errors",
                lambda v: Unfolder.to_list(v).map(_force_errors_to_str),
            ).bind(
                lambda errors: JsonUnfolder.require(
                    raw,
                    "runtimeVersion",
                    _utils.to_str,
                ).bind(
                    lambda runtime: JsonUnfolder.optional(
                        raw,
                        "traceSummary",
                        lambda v: Unfolder.to_json(v).bind(_decode_summary),
                    ).bind(
                        lambda trace: JsonUnfolder.optional(
                            raw,
                            "pages",
                            lambda v: Unfolder.to_list(v).bind(_decode_pages),
                        ).map(
                            lambda pages: BrowserCheckResult(
                                framework,
                                errors,
                                runtime,
                                trace,
                                pages.value_or(empty_pages),
                            ),
                        ),
                    ),
                ),
            ),
        )
        .alt(
            lambda e: ValueError(
                f"Error decoding `BrowserCheckResult` i.e. {e}",
            ),
        )
    )
