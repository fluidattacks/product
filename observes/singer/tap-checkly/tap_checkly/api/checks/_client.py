from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
)
from datetime import (
    datetime,
)

from fa_purity import (
    Cmd,
    FrozenList,
    PureIterFactory,
    Stream,
    Unsafe,
)
from fa_purity.json import UnfoldedFactory

from tap_checkly.api import (
    _utils,
)
from tap_checkly.api._raw import (
    Credentials,
    RawClient,
)
from tap_checkly.objs import (
    CheckId,
    CheckObj,
    CheckResultObj,
)

from ._decode import (
    CheckDecoder,
)
from .results import (
    CheckResultClient,
)


@dataclass(frozen=True)
class ChecksClient:
    _raw: RawClient
    _per_page: int

    def _list_ids(self, page: int) -> Cmd[FrozenList[CheckId]]:
        return self._raw.new_get_list(
            "/v1/checks",
            UnfoldedFactory.from_dict({"limit": self._per_page, "page": page}),
        ).map(
            lambda items: PureIterFactory.pure_map(
                lambda i: CheckDecoder(i).decode_id().alt(Unsafe.raise_exception).to_union(),
                items,
            ).to_list(),
        )

    def list_ids(self) -> Stream[CheckId]:
        return _utils.paginate_all(self._list_ids)

    def _list_checks(self, page: int) -> Cmd[FrozenList[CheckObj]]:
        return self._raw.new_get_list(
            "/v1/checks",
            UnfoldedFactory.from_dict({"limit": self._per_page, "page": page}),
        ).map(
            lambda items: PureIterFactory.pure_map(
                lambda i: CheckDecoder(i).decode_obj().alt(Unsafe.raise_exception).to_union(),
                items,
            ).to_list(),
        )

    def list_checks(self) -> Stream[CheckObj]:
        return _utils.paginate_all(self._list_checks)

    def list_check_results(
        self,
        check: CheckId,
        _from_date: datetime,
        _to_date: datetime,
    ) -> Stream[CheckResultObj]:
        _client = CheckResultClient(
            self._raw,
            check,
            self._per_page,
            _from_date,
            _to_date,
        )
        return _client.list_all()

    @staticmethod
    def new(
        auth: Credentials,
        per_page: int,
    ) -> ChecksClient:
        return ChecksClient(RawClient.new(auth, 10), per_page)
