from dataclasses import (
    dataclass,
)

from etl_utils import smash
from fa_purity import (
    ResultE,
)
from fa_purity.json import JsonObj, JsonUnfolder, Unfolder

from tap_checkly.api._utils import (
    to_bool,
    to_datetime,
    to_int,
    to_opt_datetime,
    to_opt_str,
    to_str,
)
from tap_checkly.objs import (
    Check,
    CheckConf1,
    CheckConf2,
    CheckId,
    CheckObj,
    IndexedObj,
)


def _decode_conf_1(raw: JsonObj) -> ResultE[CheckConf1]:
    group_1 = smash.smash_result_5(
        JsonUnfolder.require(raw, "activated", to_bool),
        JsonUnfolder.require(raw, "muted", to_bool),
        JsonUnfolder.require(raw, "doubleCheck", to_bool),
        JsonUnfolder.require(raw, "shouldFail", to_bool),
        JsonUnfolder.require(raw, "useGlobalAlertSettings", to_bool),
    )
    return group_1.map(
        lambda g: CheckConf1(*g),
    )


def _decode_conf_2(raw: JsonObj) -> ResultE[CheckConf2]:
    group_1 = smash.smash_result_5(
        JsonUnfolder.optional(raw, "runtimeId", to_opt_str).map(lambda m: m.bind(lambda x: x)),
        JsonUnfolder.require(raw, "checkType", to_str),
        JsonUnfolder.require(raw, "frequency", to_int),
        JsonUnfolder.require(raw, "frequencyOffset", to_int),
        JsonUnfolder.require(raw, "degradedResponseTime", to_int),
    )
    group_2 = JsonUnfolder.require(raw, "maxResponseTime", to_int)
    return group_1.bind(
        lambda g1: group_2.map(
            lambda g2: CheckConf2(*g1, g2),
        ),
    )


@dataclass(frozen=True)
class CheckDecoder:
    raw: JsonObj

    def decode_check(self) -> ResultE[Check]:
        raw = self.raw
        group_1 = smash.smash_result_4(
            JsonUnfolder.require(raw, "name", to_str),
            _decode_conf_1(raw),
            _decode_conf_2(raw),
            JsonUnfolder.require(
                raw,
                "locations",
                lambda v: Unfolder.to_list_of(v, to_str),
            ),
        )
        group_2 = smash.smash_result_2(
            JsonUnfolder.require(raw, "created_at", to_datetime),
            JsonUnfolder.optional(raw, "update_at", to_opt_datetime).map(
                lambda m: m.bind(lambda x: x),
            ),
        )
        return group_1.bind(
            lambda g1: group_2.map(
                lambda g2: Check(
                    *g1,
                    *g2,
                ),
            ),
        )

    def decode_id(self) -> ResultE[CheckId]:
        return JsonUnfolder.require(self.raw, "id", to_str).map(CheckId)

    def decode_obj(self) -> ResultE[CheckObj]:
        return self.decode_id().bind(
            lambda cid: self.decode_check().map(lambda c: IndexedObj(cid, c)),
        )
