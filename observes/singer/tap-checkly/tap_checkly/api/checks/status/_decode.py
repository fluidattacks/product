from dataclasses import (
    dataclass,
)
from datetime import (
    datetime,
)

from fa_purity import (
    Maybe,
    Result,
    ResultE,
)
from fa_purity.json import (
    JsonObj,
    JsonPrimitiveUnfolder,
    JsonUnfolder,
    Unfolder,
)

from tap_checkly._utils import (
    isoparse,
    switch_maybe,
)
from tap_checkly.objs import (
    CheckId,
    CheckStatus,
    CheckStatusObj,
    IndexedObj,
)


@dataclass(frozen=True)
class CheckStatusDecoder:
    raw: JsonObj

    def decode_check(self) -> ResultE[CheckStatus]:
        _name = JsonUnfolder.require(
            self.raw,
            "name",
            Unfolder.to_primitive,
        ).bind(JsonPrimitiveUnfolder.to_str)
        _created_at = (
            JsonUnfolder.require(self.raw, "created_at", Unfolder.to_primitive)
            .bind(JsonPrimitiveUnfolder.to_str)
            .bind(isoparse)
        )
        _has_errors = JsonUnfolder.require(
            self.raw,
            "hasErrors",
            Unfolder.to_primitive,
        ).bind(JsonPrimitiveUnfolder.to_bool)
        _has_failures = JsonUnfolder.require(
            self.raw,
            "hasFailures",
            Unfolder.to_primitive,
        ).bind(JsonPrimitiveUnfolder.to_bool)
        _is_degraded = JsonUnfolder.require(
            self.raw,
            "isDegraded",
            Unfolder.to_primitive,
        ).bind(JsonPrimitiveUnfolder.to_bool)
        _last_check = JsonUnfolder.require(
            self.raw,
            "lastCheckRunId",
            Unfolder.to_primitive,
        ).bind(JsonPrimitiveUnfolder.to_str)
        _last_run = JsonUnfolder.require(
            self.raw,
            "lastRunLocation",
            Unfolder.to_primitive,
        ).bind(JsonPrimitiveUnfolder.to_str)
        _longest_run = JsonUnfolder.require(
            self.raw,
            "longestRun",
            Unfolder.to_primitive,
        ).bind(JsonPrimitiveUnfolder.to_int)
        _shortest_run = JsonUnfolder.require(
            self.raw,
            "shortestRun",
            Unfolder.to_primitive,
        ).bind(JsonPrimitiveUnfolder.to_int)
        _ssl_days = (
            JsonUnfolder.require(
                self.raw,
                "sslDaysRemaining",
                Unfolder.to_primitive,
            )
            .bind(JsonPrimitiveUnfolder.to_opt_int)
            .map(lambda v: Maybe.from_optional(v))
        )
        _updated_at = JsonUnfolder.optional(
            self.raw,
            "updated_at",
            lambda v: Unfolder.to_primitive(v).bind(
                JsonPrimitiveUnfolder.to_opt_str,
            ),
        ).bind(
            lambda m: m.bind_optional(lambda x: x)
            .map(lambda v: isoparse(v).map(lambda x: Maybe.some(x)))
            .value_or(Result.success(Maybe.empty(datetime), Exception)),
        )
        return _name.bind(
            lambda name: _created_at.bind(
                lambda created_at: _has_errors.bind(
                    lambda has_errors: _has_failures.bind(
                        lambda has_failures: _is_degraded.bind(
                            lambda is_degraded: _last_check.bind(
                                lambda last_check_run_id: _last_run.bind(
                                    lambda last_run_location: _longest_run.bind(
                                        lambda longest_run: _shortest_run.bind(
                                            lambda shortest_run: _ssl_days.bind(
                                                lambda ssl_days_remaining: _updated_at.map(
                                                    lambda updated_at: CheckStatus(
                                                        name,
                                                        created_at,
                                                        has_errors,
                                                        has_failures,
                                                        is_degraded,
                                                        last_check_run_id,
                                                        last_run_location,
                                                        longest_run,
                                                        shortest_run,
                                                        ssl_days_remaining,
                                                        updated_at,
                                                    ),
                                                ),
                                            ),
                                        ),
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        )

    def decode_id(self) -> ResultE[Maybe[CheckId]]:
        return JsonUnfolder.optional(
            self.raw,
            "checkId",
            lambda v: Unfolder.to_primitive(v).bind(JsonPrimitiveUnfolder.to_str).map(CheckId),
        )

    def decode_obj(self) -> ResultE[Maybe[CheckStatusObj]]:
        return self.decode_id().bind(
            lambda m: switch_maybe(
                m.map(
                    lambda cid: self.decode_check().map(
                        lambda c: IndexedObj(cid, c),
                    ),
                ),
            ),
        )
