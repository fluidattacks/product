from typing import Any

import requests
from etl_utils.typing import (
    Dict,
    NoReturn,
    TypeVar,
)
from fa_purity import (
    Cmd,
    CmdUnwrapper,
    Coproduct,
    CoproductFactory,
    FrozenList,
    PureIterFactory,
    Result,
    ResultTransform,
    UnionFactory,
    Unsafe,
)
from fa_purity.json import (
    JsonObj,
    JsonValue,
    JsonValueFactory,
    Unfolder,
)
from requests import (
    Response,
)
from requests.exceptions import (
    ConnectionError as RequestsConnectionError,
)
from requests.exceptions import (
    HTTPError,
    JSONDecodeError,
)

HandledErrors = Coproduct[
    HTTPError,
    Coproduct[JSONDecodeError, RequestsConnectionError],
]
_T = TypeVar("_T")


def _unhandled_get(
    endpoint: str,
    headers: JsonObj,
    params: JsonObj,
) -> Cmd[Response | NoReturn]:
    def _action() -> Response:
        encoded_headers: Dict[str, Any] = dict(  # type: ignore[misc]
            headers.map(lambda k: k, lambda v: Unfolder.to_raw(v)),  # type: ignore[misc]
        )
        encoded_params: Dict[str, Any] = dict(  # type: ignore[misc]
            params.map(lambda k: k, lambda v: Unfolder.to_raw(v)),  # type: ignore[misc]
        )
        return requests.get(
            endpoint,
            headers=encoded_headers,  # type: ignore[misc]
            params=encoded_params,  # type: ignore[misc]
            timeout=None,  # noqa: S113 timeout is implemented on job execution level
        )

    return Cmd.wrap_impure(_action)


def _handle_status(response: Response) -> Result[Response, HTTPError]:
    try:
        response.raise_for_status()
        return Result.success(response)
    except HTTPError as err:
        return Result.failure(err)


def _value_decode(raw: JsonValue) -> Result[JsonObj | FrozenList[JsonObj], JSONDecodeError]:
    _union: UnionFactory[JsonObj, FrozenList[JsonObj]] = UnionFactory()
    return raw.map(
        lambda _: Result.failure(JSONDecodeError("Expected a JSON object")),
        lambda i: ResultTransform.all_ok(
            PureIterFactory.from_list(i).map(Unfolder.to_json).to_list(),
        )
        .map(_union.inr)
        .alt(lambda _: JSONDecodeError("Expected a list of JSON objects")),
        lambda d: Result.success(_union.inl(d)),
    )


def _json_decode(
    response: Response,
) -> Result[JsonObj | FrozenList[JsonObj], JSONDecodeError]:
    try:
        raw = response.json()  # type: ignore[misc]
        result = (
            JsonValueFactory.from_any(raw)  # type: ignore[misc]
            .alt(lambda e: JSONDecodeError(e))
            .bind(_value_decode)
        )
        return Result.success(result.alt(Unsafe.raise_exception).to_union())
    except JSONDecodeError as err:
        return Result.failure(err)


def _handle_connection_error(
    action: Cmd[_T],
) -> Cmd[Result[_T, RequestsConnectionError]]:
    def _action(
        unwrapper: CmdUnwrapper,
    ) -> Result[_T, RequestsConnectionError]:
        try:
            return Result.success(unwrapper.act(action))
        except RequestsConnectionError as err:
            return Result.failure(err)

    return Cmd.new_cmd(_action)


def get(
    endpoint: str,
    headers: JsonObj,
    params: JsonObj,
) -> Cmd[Result[JsonObj | FrozenList[JsonObj], HandledErrors]]:
    factory: CoproductFactory[
        HTTPError,
        Coproduct[JSONDecodeError, RequestsConnectionError],
    ] = CoproductFactory()
    factory2: CoproductFactory[
        JSONDecodeError,
        RequestsConnectionError,
    ] = CoproductFactory()
    return _handle_connection_error(
        _unhandled_get(endpoint, headers, params),
    ).map(
        lambda r: r.alt(lambda e: factory.inr(factory2.inr(e)))
        .bind(lambda res: _handle_status(res).alt(factory.inl))
        .bind(
            lambda res: _json_decode(res).alt(
                lambda e: factory.inr(factory2.inl(e)),
            ),
        ),
    )


__all__ = [
    "HTTPError",
    "JSONDecodeError",
    "RequestsConnectionError",
]
