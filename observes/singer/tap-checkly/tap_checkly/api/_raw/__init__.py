from __future__ import (
    annotations,
)

import inspect
import logging
from dataclasses import (
    dataclass,
)

from etl_utils.bug import (
    Bug,
)
from fa_purity import (
    Cmd,
    Coproduct,
    CoproductFactory,
    FrozenList,
    FrozenTools,
    Result,
)
from fa_purity.json import (
    JsonObj as NewJsonObj,
)
from fa_purity.json import (
    JsonPrimitiveFactory,
    JsonUnfolder,
)
from fa_purity.json import (
    JsonValue as NewJsonValue,
)
from pure_requests.basic import (
    Endpoint,
    HttpClient,
    HttpClientFactory,
    Params,
)
from pure_requests.response import (
    handle_status,
    json_decode,
)
from pure_requests.retry import (
    HandledError,
    cmd_if_fail,
    retry_cmd,
    sleep_cmd,
)
from requests import (
    RequestException,
    Response,
)
from requests.exceptions import (
    ConnectionError as RequestsConnectionError,
)
from requests.exceptions import (
    HTTPError,
)

LOG = logging.getLogger(__name__)


@dataclass(frozen=True)
class Credentials:
    account: str
    api_key: str

    def __str__(self) -> str:
        return "masked api_key"


class UnexpectedServerResponseError(Exception):
    pass


@dataclass(frozen=True)
class RawClient:
    _auth: Credentials
    _max_retries: int
    _api_url: str
    _client: HttpClient

    @staticmethod
    def new(auth: Credentials, max_retries: int) -> RawClient:
        headers = {
            "X-Checkly-Account": NewJsonValue.from_primitive(
                JsonPrimitiveFactory.from_raw(auth.account),
            ),
            "Authorization": NewJsonValue.from_primitive(
                JsonPrimitiveFactory.from_raw(f"Bearer {auth.api_key}"),
            ),
        }
        return RawClient(
            auth,
            max_retries,
            "https://api.checklyhq.com",
            HttpClientFactory.new_client(None, FrozenTools.freeze(headers), None),
        )

    def _full_endpoint(self, endpoint: str) -> Endpoint:
        return Endpoint(self._api_url + endpoint)

    def _handle_http_error(
        self,
        error: HTTPError,
    ) -> HandledError[HTTPError, HTTPError]:
        err_code = (
            error.response.status_code  # type: ignore[misc]
            if error.response is not None  # type: ignore[misc]
            else None
        )
        if isinstance(err_code, int) and (  # type: ignore[misc]
            err_code in (429,) or err_code in range(500, 600)
        ):
            return HandledError.handled(error)
        return HandledError.unhandled(error)

    def _handler(
        self,
        item: Result[Response, RequestException],
    ) -> Result[
        Response,
        HandledError[
            HTTPError | RequestsConnectionError,
            RequestException | HTTPError,
        ],
    ]:
        factory: CoproductFactory[
            HTTPError | RequestsConnectionError,
            RequestException | HTTPError,
        ] = CoproductFactory()
        return item.to_coproduct().map(
            lambda r: handle_status(r)
            .alt(self._handle_http_error)
            .alt(
                lambda h: h.value.map(
                    lambda e: HandledError(factory.inl(e)),
                    lambda e: HandledError(factory.inr(e)),
                ),
            ),
            lambda e: Result.failure(HandledError.unhandled(e)),
        )

    def new_get(
        self,
        endpoint: str,
        params: NewJsonObj,
    ) -> Cmd[Coproduct[NewJsonObj, FrozenList[NewJsonObj]]]:
        target = self._full_endpoint(endpoint)
        log = Cmd.wrap_impure(
            lambda: LOG.info("API call (get): %s\nparams = %s", target, params),
        )

        return log + retry_cmd(
            self._client.get(target, Params(params)).map(self._handler),
            lambda i, r: cmd_if_fail(r, sleep_cmd(i**2)),
            self._max_retries,
        ).map(
            lambda r: Bug.assume_success(
                "get",
                inspect.currentframe(),
                (str(target), JsonUnfolder.dumps(params)),
                r,
            ),
        ).map(
            lambda r: Bug.assume_success(
                "get_json_decode",
                inspect.currentframe(),
                (str(target), JsonUnfolder.dumps(params)),
                json_decode(r),
            ),
        )

    def new_get_item(
        self,
        endpoint: str,
        params: NewJsonObj,
    ) -> Cmd[NewJsonObj]:
        return self.new_get(endpoint, params).map(
            lambda c: c.map(
                lambda j: j,
                lambda _: Bug.new(
                    "new_get_item",
                    inspect.currentframe(),
                    None,
                    (endpoint, JsonUnfolder.dumps(params)),
                ).explode(),
            ),
        )

    def new_get_list(
        self,
        endpoint: str,
        params: NewJsonObj,
    ) -> Cmd[FrozenList[NewJsonObj]]:
        return self.new_get(endpoint, params).map(
            lambda c: c.map(
                lambda _: Bug.new(
                    "new_get_list",
                    inspect.currentframe(),
                    None,
                    (endpoint, JsonUnfolder.dumps(params)),
                ).explode(),
                lambda i: i,
            ),
        )
