from dataclasses import (
    dataclass,
)

from etl_utils import smash
from fa_purity import (
    ResultE,
)
from fa_purity.json import (
    JsonObj,
    JsonUnfolder,
    Unfolder,
)

from tap_checkly.api._utils import (
    to_bool,
    to_float,
    to_str,
)
from tap_checkly.objs import (
    CheckId,
    CheckReport,
)


@dataclass(frozen=True)
class _Aggregate:
    avg: float
    p95: float
    p99: float
    success_ratio: float


def _decode_aggregate(raw: JsonObj) -> ResultE[_Aggregate]:
    group_1 = smash.smash_result_4(
        JsonUnfolder.require(raw, "avg", to_float),
        JsonUnfolder.require(raw, "p95", to_float),
        JsonUnfolder.require(raw, "p99", to_float),
        JsonUnfolder.require(raw, "successRatio", to_float),
    )
    return group_1.map(
        lambda g1: _Aggregate(*g1),
    )


@dataclass(frozen=True)
class CheckReportDecoder:
    raw: JsonObj

    def decode_report(self) -> ResultE[CheckReport]:
        aggregate = JsonUnfolder.require(
            self.raw,
            "aggregate",
            lambda v: Unfolder.to_json(v).bind(_decode_aggregate),
        )
        group_1 = smash.smash_result_4(
            JsonUnfolder.require(self.raw, "checkId", to_str).map(CheckId),
            JsonUnfolder.require(self.raw, "checkType", to_str),
            JsonUnfolder.require(self.raw, "deactivated", to_bool),
            JsonUnfolder.require(self.raw, "name", to_str),
        )
        return group_1.bind(
            lambda g: aggregate.map(
                lambda agg: CheckReport(
                    *g,
                    agg.avg,
                    agg.p95,
                    agg.p99,
                    agg.success_ratio,
                ),
            ),
        )
