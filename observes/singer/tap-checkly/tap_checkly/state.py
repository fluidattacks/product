from dataclasses import (
    dataclass,
)

from fa_purity import (
    Maybe,
)

from ._utils import (
    DateInterval,
)


@dataclass(frozen=True)
class EtlState:
    results: Maybe[DateInterval]  # check results stream
