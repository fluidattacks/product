from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
)

from fa_purity import (
    FrozenList,
    PureIter,
    PureIterFactory,
)


@dataclass(frozen=True)
class LocationItem:
    index: int
    location: str

    @staticmethod
    def from_locations(locations: FrozenList[str]) -> PureIter[LocationItem]:
        return (
            PureIterFactory.from_list(locations)
            .enumerate(0)
            .map(
                lambda t: LocationItem(
                    t[0],
                    t[1],
                ),
            )
        )
