from enum import (
    Enum,
    unique,
)


@unique
class SingerStreams(Enum):
    ALERT_CHANNELS = "alert_channels"
    CHECKS = "checks"
    CHECK_STATUS = "check_status"
    CHECK_REPORTS = "check_reports"
    CHECK_GROUPS = "check_groups"
    CHECK_GROUPS_ALERTS = "check_groups_alerts"
    CHECK_GROUPS_LOCATIONS = "check_groups_locations"
    CHECK_LOCATIONS = "check_locations"
    CHECK_RESULTS = "check_results"
    CHECK_RESULTS_API = "check_results_api"
    CHECK_RESULTS_BROWSER = "check_results_browser"
    CHECK_RESULTS_BROWSER_PAGES = "check_results_browser_pages"
