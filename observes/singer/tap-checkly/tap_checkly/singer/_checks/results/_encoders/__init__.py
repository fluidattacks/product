from fa_purity import (
    FrozenList,
    PureIter,
    PureIterFactory,
)
from fa_singer_io.singer import (
    SingerRecord,
)

from tap_checkly.objs import (
    CheckResultObj,
    IndexedObj,
)
from tap_checkly.singer._encoder import (
    ObjEncoder,
)

from . import (
    _api,
    _browser,
    _core,
)


def _to_records(item: CheckResultObj) -> PureIter[SingerRecord]:
    empty: FrozenList[SingerRecord] = ()
    records = (
        *item.obj.api_result.map(
            lambda r: _api.encoder.record(IndexedObj(item.id_obj, r)),
        )
        .map(lambda i: (i,))
        .value_or(empty),
        _core.encoder.record(item),
    )
    return PureIterFactory.from_list(records)


_schemas = (
    _core.encoder.schema,
    _api.encoder.schema,
    *tuple(_browser.encoder.schemas),
)
encoder: ObjEncoder[CheckResultObj] = ObjEncoder.new(
    PureIterFactory.from_list(_schemas),
    _to_records,
)
