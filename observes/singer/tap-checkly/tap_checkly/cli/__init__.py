from datetime import (
    datetime,
)
from pathlib import Path

import click
from etl_utils.typing import (
    NoReturn,
)
from fa_purity import (
    Cmd,
    Maybe,
    ResultE,
    Unsafe,
)
from fa_purity.date_time import (
    DatetimeFactory,
    DatetimeTZ,
)
from fa_purity.json import UnfoldedFactory
from utils_logger import (
    start_session,
)

from tap_checkly._utils import (
    DateInterval,
)
from tap_checkly.api import (
    Credentials,
)
from tap_checkly.state import (
    EtlState,
)
from tap_checkly.streams import (
    SupportedStreams,
    decode_state,
)

from ._emitter import (
    Emitter,
)


def _decode_state(file_path: str) -> Cmd[ResultE[EtlState]]:
    def _action() -> ResultE[EtlState]:
        with Path(file_path).open(encoding="utf-8") as file:
            raw = UnfoldedFactory.load(file)
        return raw.bind(decode_state)

    return Cmd.wrap_impure(_action)


@click.command()
@click.option("--api-user", type=str, required=True)
@click.option("--api-key", type=str, required=True)
@click.option("--reports-start", type=str, required=False)
@click.option("--all-streams", is_flag=True, default=False)
@click.option("--state", type=click.Path(exists=True), default=None)
@click.option("--state", type=click.Path(exists=True), default=None)
@click.argument(
    "name",
    type=click.Choice(
        [x.value for x in iter(SupportedStreams)],
        case_sensitive=False,
    ),
    required=False,
    default=None,
)
def stream(  # noqa: PLR0913
    # reason for `too-many-arguments`
    # cannot group arguments since this is a cli interface function
    name: str | None,
    api_user: str,
    api_key: str,
    all_streams: bool,
    state: str | None,
    reports_start: str | None,
) -> NoReturn:
    creds = Credentials(api_user, api_key)
    selection = (
        tuple(SupportedStreams)
        if all_streams
        else Maybe.from_optional(name)
        .map(SupportedStreams)
        .map(lambda i: (i,))
        .to_result()
        .alt(lambda _: ValueError("Null stream selection"))
        .alt(Unsafe.raise_exception)
        .to_union()
    )
    empty: Maybe[DateInterval] = Maybe.empty()
    _state = (
        _decode_state(state).map(
            lambda r: r.alt(Unsafe.raise_exception).to_union(),
        )
        if state
        else Cmd.wrap_value(EtlState(empty))
    )
    _reports_start = Maybe.from_optional(reports_start).map(
        lambda d: DatetimeTZ.assert_tz(
            datetime.strptime(d, "%Y-%m-%d %H:%M:%S %z"),
        )
        .alt(Unsafe.raise_exception)
        .to_union(),
    )
    cmd: Cmd[None] = start_session() + _state.bind(
        lambda s: Emitter(
            s,
            creds,
            _reports_start.map(DatetimeFactory.to_utc),
        ).emit_streams(selection),
    )
    cmd.compute()


@click.group()
def main() -> None:
    # click main group
    pass


main.add_command(stream)
