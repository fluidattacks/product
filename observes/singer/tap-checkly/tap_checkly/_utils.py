from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
)
from datetime import (
    datetime,
)

from dateutil.parser import (
    isoparse as _isoparse,
)
from etl_utils.typing import (
    TypeVar,
)
from fa_purity import (
    Maybe,
    Result,
    ResultE,
)

_T = TypeVar("_T")
_S = TypeVar("_S")
_F = TypeVar("_F")


def isoparse(raw: str) -> ResultE[datetime]:
    try:
        return Result.success(_isoparse(raw), Exception)
    except ValueError as err:
        return Result.failure(err, datetime).alt(Exception)


def switch_maybe(item: Maybe[Result[_S, _F]]) -> Result[Maybe[_S], _F]:
    _empty: Maybe[_S] = Maybe.empty()
    _empty_result: Result[Maybe[_S], _F] = Result.success(_empty)
    return item.map(lambda r: r.map(lambda x: Maybe.some(x))).value_or(
        _empty_result,
    )


@dataclass(frozen=True)
class _Private:
    pass


@dataclass(frozen=True)
class DateInterval:
    _private: _Private
    oldest: datetime
    newest: datetime

    @staticmethod
    def new(oldest: datetime, newest: datetime) -> ResultE[DateInterval]:
        if newest > oldest:
            return Result.success(
                DateInterval(_Private(), oldest, newest),
                Exception,
            )
        err = ValueError(
            f"Invalid DateInterval: oldest > newest. i.e. {oldest} > {newest}",
        )
        return Result.failure(err, DateInterval).alt(Exception)
