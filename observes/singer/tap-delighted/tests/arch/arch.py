from arch_lint.dag import (
    DagMap,
)
from arch_lint.graph import (
    FullPathModule,
)
from tap_delighted._typing import (
    Dict,
    FrozenSet,
    NoReturn,
    TypeVar,
)

_T = TypeVar("_T")


def raise_or_return(item: _T | Exception) -> _T | NoReturn:
    if isinstance(item, Exception):
        raise item
    return item


def _module(path: str) -> FullPathModule | NoReturn:
    return raise_or_return(FullPathModule.from_raw(path))


_dag: Dict[str, tuple[tuple[str, ...] | str, ...]] = {
    "tap_delighted": (
        "cli",
        "emitter",
        "singer",
        "api",
        ("auth", "streams", "_logger"),
        "_typing",
    ),
    "tap_delighted.singer": (("metrics", "survey", "people"),),
    "tap_delighted.singer.people": (("_bounced", "_people", "_unsubscribed"),),
    "tap_delighted.api": (
        "_client_1",
        "client",
        "core",
        "_utils",
    ),
    "tap_delighted.api.core": (
        "people",
        "ids",
    ),
    "tap_delighted.api._client_1._decode": (
        ("_bounced", "_metrics", "_person", "_survey", "_unsubscribed"),
        "_common",
    ),
}


def project_dag() -> DagMap:
    return raise_or_return(DagMap.new(_dag))


def forbidden_allowlist() -> Dict[FullPathModule, FrozenSet[FullPathModule]]:
    _raw: Dict[str, FrozenSet[str]] = {
        "typing": frozenset({"tap_delighted._typing"}),
        "collections": frozenset({"tap_delighted._typing"}),
    }
    return {
        _module(k): frozenset(_module(i) for i in v) for k, v in _raw.items()
    }
