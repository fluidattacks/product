{ inputs, makePythonVscodeSettings, projectPath, ... }@makes_inputs:
let
  root = projectPath inputs.observesIndex.tap.delighted.root;
  bundle = import "${root}/entrypoint.nix" makes_inputs;
in makePythonVscodeSettings {
  env = bundle.env.dev;
  bins = [ ];
  name = "observes-singer-tap-delighted-env-dev";
}
