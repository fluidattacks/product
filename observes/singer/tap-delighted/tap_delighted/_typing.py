from collections.abc import (
    Callable,
    Iterable,
    Iterator,
)
from typing import (
    Generic,
    IO,
    NoReturn,
    TypeVar,
)

_T = TypeVar("_T")
_A = TypeVar("_A")

Tuple = tuple[_T, _A]
List = list
Dict = dict
Set = set
FrozenSet = frozenset
Type = type[_T]


__all__ = [
    "Callable",
    "Generic",
    "IO",
    "Iterable",
    "Iterator",
    "NoReturn",
    "TypeVar",
]
