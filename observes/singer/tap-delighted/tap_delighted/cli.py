import click
from fa_purity import (
    Cmd,
)
from tap_delighted.auth import (
    Credentials,
)
from tap_delighted.emitter import (
    new_emitter,
)
from tap_delighted.streams import (
    SupportedStreams,
)


@click.command()
@click.option("--api-key", type=str, required=True)
@click.option("--all-streams", is_flag=True, default=False)
@click.argument(
    "name",
    type=click.Choice(
        [x.value for x in iter(SupportedStreams)], case_sensitive=False
    ),
    required=False,
    default=None,
)
def stream(name: str | None, api_key: str, all_streams: bool) -> None:
    creds = Credentials.new(api_key)
    selection = SupportedStreams.ALL if all_streams else SupportedStreams(name)
    emitter = new_emitter(creds)
    cmd: Cmd[None] = emitter.emit(selection)
    cmd.compute()


@click.group()
def main() -> None:
    # cli group entrypoint
    pass


main.add_command(stream)
