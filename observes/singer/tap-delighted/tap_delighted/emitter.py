from .api.client import (
    ApiClient,
)
from .singer.metrics import (
    EncodedMetrics,
)
from .singer.people import (
    EncodedBounced,
    EncodedPersonObj,
    EncodedUnsubscribed,
)
from .singer.survey import (
    EncodedSurveyObj,
)
from .streams import (
    SupportedStreams,
)
from dataclasses import (
    dataclass,
)
from fa_purity import (
    Cmd,
)
from fa_purity.stream.transform import (
    consume,
)
from fa_singer_io.singer import (
    emitter as singer_emitter,
)
import sys
from tap_delighted._typing import (
    IO,
)
from tap_delighted.api import (
    new_client,
)
from tap_delighted.auth import (
    Credentials,
)


@dataclass(frozen=True)
class Emitter:
    _client: ApiClient
    _target: IO[str]

    def _emit_bounced(self) -> Cmd[None]:
        schema = singer_emitter.emit(self._target, EncodedBounced.schema())
        records = (
            self._client.list_all_bounced.map(EncodedBounced.encode)
            .map(lambda e: singer_emitter.emit(self._target, e.record))
            .transform(consume)
        )
        return schema + records

    def _emit_people(self) -> Cmd[None]:
        schema = singer_emitter.emit(self._target, EncodedPersonObj.schema())
        records = (
            self._client.list_all_people.map(EncodedPersonObj.encode)
            .map(lambda e: singer_emitter.emit(self._target, e.record))
            .transform(consume)
        )
        return schema + records

    def _emit_unsubscribed(self) -> Cmd[None]:
        schema = singer_emitter.emit(
            self._target, EncodedUnsubscribed.schema()
        )
        records = (
            self._client.list_all_unsubscribed.map(EncodedUnsubscribed.encode)
            .map(lambda e: singer_emitter.emit(self._target, e.record))
            .transform(consume)
        )
        return schema + records

    def _emit_metrics(self) -> Cmd[None]:
        schema = singer_emitter.emit(self._target, EncodedMetrics.schema())
        records = self._client.get_metrics.map(EncodedMetrics.encode).bind(
            lambda e: singer_emitter.emit(self._target, e.record)
        )
        return schema + records

    def _emit_survey(self) -> Cmd[None]:
        schema = singer_emitter.emit(self._target, EncodedSurveyObj.schema())
        records = (
            self._client.list_all_surveys.map(EncodedSurveyObj.encode)
            .map(lambda e: singer_emitter.emit(self._target, e.record))
            .transform(consume)
        )
        return schema + records

    def emit(self, selection: SupportedStreams) -> Cmd[None]:
        if selection is SupportedStreams.BOUNCED:
            return self._emit_bounced()
        if selection is SupportedStreams.PEOPLE:
            return self._emit_people()
        if selection is SupportedStreams.UNSUBSCRIBED:
            return self._emit_unsubscribed()
        if selection is SupportedStreams.METRICS:
            return self._emit_metrics()
        if selection is SupportedStreams.SURVEY_RESPONSE:
            return self._emit_survey()
        if selection is SupportedStreams.ALL:
            return (
                self._emit_bounced()
                + self._emit_people()
                + self._emit_unsubscribed()
                + self._emit_metrics()
                + self._emit_survey()
            )


def new_emitter(creds: Credentials) -> Emitter:
    client = new_client(creds)
    return Emitter(client, sys.stdout)
