from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
)


@dataclass(frozen=True)
class Credentials:
    api_key: str

    @classmethod
    def new(cls, raw: str) -> Credentials:
        return cls(raw)

    def __str__(self) -> str:
        return "masked api_key"
