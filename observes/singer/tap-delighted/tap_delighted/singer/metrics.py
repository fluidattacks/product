from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
    field,
)
from fa_purity import (
    FrozenDict,
    JsonValue,
    UnfoldedJVal,
)
from fa_purity.json.factory import (
    from_unfolded_dict,
)
from fa_purity.utils import (
    raise_exception,
)
from fa_singer_io.json_schema.factory import (
    from_json,
    from_prim_type,
)
from fa_singer_io.singer import (
    SingerRecord,
    SingerSchema,
)
from tap_delighted.api.core import (
    Metrics,
)
from tap_delighted.streams import (
    SupportedStreams,
)

_STREAM = SupportedStreams.METRICS.value


def _schema() -> SingerSchema:
    properties = FrozenDict(
        {
            "nps_int": JsonValue(from_prim_type(int).encode()),
            "promoter_count_int": JsonValue(from_prim_type(int).encode()),
            "promoter_percent_float": JsonValue(
                from_prim_type(float).encode()
            ),
            "passive_count_int": JsonValue(from_prim_type(int).encode()),
            "passive_percent_float": JsonValue(from_prim_type(float).encode()),
            "detractor_count_int": JsonValue(from_prim_type(int).encode()),
            "detractor_percent_float": JsonValue(
                from_prim_type(float).encode()
            ),
            "response_count_int": JsonValue(from_prim_type(int).encode()),
        }
    )
    schema = FrozenDict({"properties": JsonValue(properties)})
    return (
        SingerSchema.new(
            _STREAM,
            from_json(schema).alt(raise_exception).to_union(),
            frozenset([]),
            None,
        )
        .alt(raise_exception)
        .to_union()
    )


@dataclass(frozen=True)
class _Private:
    pass


@dataclass(frozen=True)
class EncodedMetrics:
    _private: _Private = field(repr=False, hash=False, compare=False)
    record: SingerRecord

    @staticmethod
    def schema() -> SingerSchema:
        return _schema()

    @staticmethod
    def encode(item: Metrics) -> EncodedMetrics:
        encoded: FrozenDict[str, UnfoldedJVal] = FrozenDict(
            {
                "nps_int": item.nps,
                "promoter_count_int": item.promoter_count,
                "promoter_percent_float": item.promoter_percent,
                "passive_count_int": item.passive_count,
                "passive_percent_float": item.passive_percent,
                "detractor_count_int": item.detractor_count,
                "detractor_percent_float": item.detractor_percent,
                "response_count_int": item.response_count,
            }
        )
        record = SingerRecord(_STREAM, from_unfolded_dict(encoded), None)
        return EncodedMetrics(_Private(), record)
