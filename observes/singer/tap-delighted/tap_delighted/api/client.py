from dataclasses import (
    dataclass,
)
from fa_purity import (
    Cmd,
    Stream,
)
from tap_delighted.api.core import (
    Metrics,
    SurveyObj,
)
from tap_delighted.api.core.people import (
    BouncedPerson,
    PersonObj,
    UnsubscribedPerson,
)


@dataclass(frozen=True)
class ApiClient:
    get_metrics: Cmd[Metrics]
    list_all_people: Stream[PersonObj]
    list_all_unsubscribed: Stream[UnsubscribedPerson]
    list_all_bounced: Stream[BouncedPerson]
    list_all_surveys: Stream[SurveyObj]
