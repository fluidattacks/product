from dataclasses import (
    dataclass,
)


@dataclass(frozen=True)
class PersonId:
    value: str


@dataclass(frozen=True)
class SurveyId:
    value: str
