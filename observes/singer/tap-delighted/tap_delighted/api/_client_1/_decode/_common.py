from fa_purity import (
    Maybe,
    ResultE,
)
from fa_purity.json_2 import (
    JsonObj,
    JsonPrimitiveUnfolder,
    JsonUnfolder,
    Unfolder,
)
from fa_purity.union import (
    UnionFactory,
)
from tap_delighted.api._utils import (
    print_json_value,
)


def require_int(value: JsonObj, key: str) -> ResultE[int]:
    return JsonUnfolder.require(
        value,
        key,
        lambda v: Unfolder.to_primitive(v)
        .bind(JsonPrimitiveUnfolder.to_int)
        .alt(
            lambda e: ValueError(
                f"Transform `to_int` failed at key `{key}` value `{print_json_value(v)}` i.e. {e}"
            )
        ),
    )


def require_float(value: JsonObj, key: str) -> ResultE[float]:
    return JsonUnfolder.require(
        value,
        key,
        lambda v: Unfolder.to_primitive(v)
        .bind(JsonPrimitiveUnfolder.to_float)
        .alt(
            lambda e: ValueError(
                f"Transform `to_float` failed at key `{key}` value `{print_json_value(v)}` i.e. {e}"
            )
        ),
    )


def require_str(value: JsonObj, key: str) -> ResultE[str]:
    return JsonUnfolder.require(
        value,
        key,
        lambda v: Unfolder.to_primitive(v)
        .bind(JsonPrimitiveUnfolder.to_str)
        .alt(
            lambda e: ValueError(
                f"Transform `to_str` failed at key `{key}` value `{print_json_value(v)}` i.e. {e}"
            )
        ),
    )


def optional_str(value: JsonObj, key: str) -> ResultE[Maybe[str]]:
    factory: UnionFactory[str, None] = UnionFactory()
    return JsonUnfolder.optional(
        value,
        key,
        lambda v: Unfolder.to_primitive(v)
        .bind(JsonPrimitiveUnfolder.to_str)
        .map(factory.inl)
        .lash(
            lambda _: Unfolder.to_primitive(v)
            .bind(JsonPrimitiveUnfolder.to_none)
            .map(factory.inr)
        )
        .alt(
            lambda e: ValueError(
                "Transform `to_str` & `to_none` failed at key "
                f"`{key}` value `{print_json_value(v)}` i.e. {e}"
            )
        ),
    ).map(lambda m: m.bind_optional(lambda x: x))


def optional_int(value: JsonObj, key: str) -> ResultE[Maybe[int]]:
    factory: UnionFactory[int, None] = UnionFactory()
    return JsonUnfolder.optional(
        value,
        key,
        lambda v: Unfolder.to_primitive(v)
        .bind(JsonPrimitiveUnfolder.to_int)
        .map(factory.inl)
        .lash(
            lambda _: Unfolder.to_primitive(v)
            .bind(JsonPrimitiveUnfolder.to_none)
            .map(factory.inr)
        )
        .alt(
            lambda e: ValueError(
                "Transform `to_int` & `to_none` failed at key "
                f"`{key}` value `{print_json_value(v)}` i.e. {e}"
            )
        ),
    ).map(lambda m: m.bind_optional(lambda x: x))


def optional_json(value: JsonObj, key: str) -> ResultE[Maybe[JsonObj]]:
    return JsonUnfolder.optional(value, key, Unfolder.to_json)
