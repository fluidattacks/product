from ._common import (
    require_float,
    require_int,
)
from dataclasses import (
    dataclass,
)
from fa_purity import (
    ResultE,
)
from fa_purity.json_2 import (
    JsonObj,
    JsonValue,
)
from tap_delighted.api._utils import (
    ApiBug,
)
from tap_delighted.api.core import (
    Metrics,
)


@dataclass(frozen=True)
class MetricsDecoder:
    @staticmethod
    def decode(raw: JsonObj) -> ResultE[Metrics]:
        return (
            require_int(raw, "nps")
            .bind(
                lambda nps: require_int(raw, "promoter_count").bind(
                    lambda promoter_count: require_float(
                        raw, "promoter_percent"
                    ).bind(
                        lambda promoter_percent: require_int(
                            raw, "passive_count"
                        ).bind(
                            lambda passive_count: require_float(
                                raw, "passive_percent"
                            ).bind(
                                lambda passive_percent: require_int(
                                    raw, "detractor_count"
                                ).bind(
                                    lambda detractor_count: require_float(
                                        raw, "detractor_percent"
                                    ).bind(
                                        lambda detractor_percent: require_int(
                                            raw, "response_count"
                                        ).map(
                                            lambda response_count: Metrics(
                                                nps,
                                                promoter_count,
                                                promoter_percent,
                                                passive_count,
                                                passive_percent,
                                                detractor_count,
                                                detractor_percent,
                                                response_count,
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            )
            .alt(
                lambda e: ApiBug.new_json_input(
                    __file__,
                    "MetricsDecoder.decode",
                    e,
                    JsonValue.from_json(raw),
                )
            )
        )
