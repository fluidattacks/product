{ makes_inputs, nixpkgs, python_version, src, }:
let deps = import ./deps { inherit makes_inputs nixpkgs python_version; };
in makes_inputs.makePythonPyprojectPackage {
  inherit (deps.lib) buildEnv buildPythonPackage;
  inherit src;
  pkgDeps = {
    build_deps = with deps.python_pkgs; [ flit-core ];
    runtime_deps = with deps.python_pkgs; [ click gitpython ];
    test_deps = with deps.python_pkgs; [
      arch-lint
      mypy
      pylint
      pytest
      pytest-cov
    ];
  };
}
