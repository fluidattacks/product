from git import (
    Repo,
)
import tempfile


def get_universe_commits() -> list[str]:
    universe_url = "https://gitlab.com/fluidattacks/universe.git"
    main_brach = "trunk"
    commits = []

    with tempfile.TemporaryDirectory() as temp_dir:
        repo = Repo.clone_from(  # type: ignore[misc]
            url=universe_url, to_path=temp_dir
        )
        commits = [commit.hexsha for commit in repo.iter_commits(main_brach)]

    return commits
