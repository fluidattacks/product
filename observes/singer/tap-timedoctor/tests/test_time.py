from datetime import (
    datetime,
)
from dateutil.relativedelta import (
    relativedelta,
)


def test_time() -> None:
    d = datetime(2024, 2, 29)
    result = d - relativedelta(years=1)
    assert result == datetime(2023, 2, 28)
