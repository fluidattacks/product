{ lib, python_pkgs, }:
let
  metadata = {
    name = "tap-google-sheets";
    version = "3.0.0";
  };
  runtime_deps = with python_pkgs; [ backoff requests singer-python ];
  src = lib.fetchPypi {
    inherit (metadata) version;
    pname = metadata.name;
    sha256 = "E3BjxCa8lbD845FJEwdAOvqYtoKU2I7EcpEnkR4VSEE=";
  };
  pkg = (import ./build.nix) { inherit lib src metadata runtime_deps; };
  build_env = extraLibs:
    lib.buildEnv {
      inherit extraLibs;
      ignoreCollisions = false;
    };
in {
  inherit pkg;
  env = {
    runtime = build_env [ pkg ];
    dev = build_env [ pkg ];
  };
}
