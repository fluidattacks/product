{ inputs, makePythonPyprojectPackage, projectPath, pythonOverrideUtils, ...
}@makes_inputs:
let
  python_version = "python311";
  nixpkgs = inputs.nixpkgs-observes // { inherit (inputs) nix-filter; };
  out = import ./build { inherit makes_inputs nixpkgs python_version; };
in out
