from .ids import (
    MilestoneId,
    MrId,
    PipelineId,
    ProjectId,
    UserId,
)
from dataclasses import (
    dataclass,
)
from datetime import (
    datetime,
)
from enum import (
    Enum,
)
from fa_purity import (
    FrozenList,
    Maybe,
)


class MrState(Enum):
    OPENED = "opened"
    CLOSED = "closed"
    MERGED = "merged"
    LOCKED = "locked"


@dataclass(frozen=True)
class MergeRequest:
    assignees: FrozenList[UserId]
    author: UserId
    blocking_discussions_resolved: bool
    changes_count: str
    closed_at: Maybe[datetime]
    closed_by: Maybe[UserId]
    created_at: Maybe[datetime]
    description: str
    # diff_refs NO SUPPORT
    detailed_merge_status: str
    discussion_locked: bool
    downvotes: int
    draft: bool
    first_contribution: bool
    first_deployed_to_production_at: Maybe[datetime]
    force_remove_source_branch: bool
    has_conflicts: bool
    head_pipeline: PipelineId
    labels: FrozenList[str]
    latest_build_finished_at: Maybe[datetime]
    latest_build_started_at: Maybe[datetime]
    merge_commit_sha: Maybe[str]
    merge_error: Maybe[str]
    merge_user: Maybe[UserId]
    merge_when_pipeline_succeeds: bool
    merged_at: Maybe[datetime]
    milestone: MilestoneId
    prepared_at: Maybe[datetime]
    # references NO SUPPORT
    reviewers: FrozenList[UserId]
    sha: str
    should_remove_source_branch: bool
    source_branch: str
    source_project_id: ProjectId
    squash: bool
    squash_commit_sha: str
    state: MrState
    # subscribed NO SUPPORT
    target_branch: str
    target_project_id: ProjectId
    title: str
    updated_at: Maybe[datetime]
    upvotes: int
    user_notes_count: int


__all__ = [
    "MrId",
]
