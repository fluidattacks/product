from fa_purity import (
    FrozenDict,
)
from fa_purity.json_2 import (
    LegacyAdapter,
)
from fa_singer_io.singer import (
    SingerState,
)
from tap_gitlab.state import (
    EtlState,
    MrStateMap,
    MrStreamState,
    PipelineJobsState,
)
from tap_gitlab.state.encoder import (
    encode_etl_state,
)


def squash_state(state: EtlState) -> EtlState:
    return EtlState(
        FrozenDict(
            {
                j: PipelineJobsState(s.state.squash())
                for j, s in state.pipeline_jobs.items()
            }
        ),
        MrStateMap(
            {
                m: MrStreamState(s.state.squash())
                for m, s in state.mrs.items.items()
            }
        ),
    )


def state_to_singer(state: EtlState) -> SingerState:
    encoded = LegacyAdapter.to_legacy_json(
        encode_etl_state(squash_state(state))
    )
    return SingerState(encoded)
