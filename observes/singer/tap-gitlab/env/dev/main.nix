{ inputs, makePythonVscodeSettings, projectPath, ... }@makes_inputs:
let
  root = projectPath inputs.observesIndex.tap.gitlab.root;
  pkg = import "${root}/entrypoint.nix" makes_inputs;
in makePythonVscodeSettings {
  env = pkg.env.dev;
  bins = [ ];
  name = "observes-singer-tap-gitlab-env-dev";
}
