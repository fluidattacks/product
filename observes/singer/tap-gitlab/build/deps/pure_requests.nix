{ lib, makes_inputs, nixpkgs, python_pkgs, python_version, }@args:
let
  bundles = import (makes_inputs.projectPath
    "${makes_inputs.inputs.observesIndex.common.python_pkgs}/pure_requests")
    args;
in bundles."v1.0.0".build_bundle
(default: required_deps: builder: builder lib (required_deps python_pkgs))
