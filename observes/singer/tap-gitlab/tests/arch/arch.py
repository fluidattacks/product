from arch_lint.dag import (
    DAG,
    DagMap,
)
from arch_lint.graph import (
    FullPathModule,
)
from fa_purity import (
    FrozenList,
)
from typing import (
    TypeVar,
)

_T = TypeVar("_T")
RawDag = dict[str, FrozenList[FrozenList[str] | str]]
_api_dag: RawDag = {
    "tap_gitlab.api": (
        (
            "issues",
            "jobs",
            "members",
            "merge_requests",
            "pipelines",
            "project",
        ),
        "http_json_client",
        ("core", "_utils"),
    ),
    "tap_gitlab.api.core": (
        ("mr", "commit", "issue", "job", "pipeline", "project"),
        "ids",
    ),
    "tap_gitlab.api.issues": (
        "_client",
        "_decode",
    ),
    "tap_gitlab.api.jobs": (
        "_client",
        "_decode",
    ),
    "tap_gitlab.api.members": (
        "_client",
        "_decode",
        "_core",
    ),
    "tap_gitlab.api.pipelines": (
        "_client",
        "_decode",
    ),
}
_dag: RawDag = _api_dag | {
    "tap_gitlab": (
        "cli",
        ("emitter", "cleaner"),
        "singer",
        "state",
        "streams",
        "api",
        "intervals",
        ("_logger", "_utils"),
    ),
    "tap_gitlab.emitter": (
        ("_mrs", "_pipe_jobs", "_stateless"),
        "_emitter",
        "_core",
    ),
    "tap_gitlab.state": ("getter", ("encoder", "decoder"), "default", "_objs"),
    "tap_gitlab.state.decoder": (
        ("_mr", "_pipe_jobs"),
        "_core",
    ),
    "tap_gitlab.state.encoder": (
        ("_mr", "_pipe_jobs"),
        "_core",
    ),
    "tap_gitlab.singer": (
        ("issues", "jobs", "members", "mrs"),
        ("state", "_core"),
    ),
    "tap_gitlab.singer.issues": (("records", "schemas"),),
    "tap_gitlab.singer.jobs": (("records", "schemas"),),
    "tap_gitlab.singer.members": (("records", "schemas"),),
    "tap_gitlab.streams": (
        ("_jobs", "_pipelines", "mrs", "pipe_jobs"),
        ("_decoder", "_encoder"),
        ("_core", "_utils"),
    ),
    "tap_gitlab.intervals": (
        ("encoder", "decoder"),
        "fp_interval_utils",
        "progress",
        "_chained",
        "interval",
    ),
    "tap_gitlab.intervals.interval": (
        "op",
        "_factory",
        "_objs",
    ),
    "tap_gitlab._utils": (("decode", "mutable"),),
}


def raise_if_exception(item: _T | Exception) -> _T:
    if isinstance(item, Exception):
        raise item
    return item


def project_dag() -> DagMap:
    return raise_if_exception(DagMap.new(_dag))


def forbidden_allowlist() -> dict[FullPathModule, frozenset[FullPathModule]]:
    _raw: dict[str, frozenset[str]] = {}
    return {
        FullPathModule.assert_module(k): frozenset(
            FullPathModule.assert_module(i) for i in v
        )
        for k, v in _raw.items()
    }
