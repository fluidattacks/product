path_filter: src:
path_filter {
  root = src;
  include = [ "tap_bugsnag" "tests" "pyproject.toml" "mypy.ini" "ruff.toml" ];
}
