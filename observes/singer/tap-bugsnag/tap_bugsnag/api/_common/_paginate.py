from collections.abc import Callable
from typing import (
    TypeVar,
)

from fa_purity import (
    Cmd,
    FrozenList,
    Maybe,
    Stream,
    StreamFactory,
)

from ._core import (
    ResponsePage,
)

_T = TypeVar("_T")


def paginate_all(get: Callable[[Maybe[str]], Cmd[ResponsePage[_T]]]) -> Stream[FrozenList[_T]]:
    def _extract(page: ResponsePage[_T]) -> Maybe[Maybe[str]]:
        return page.next_item.map(lambda v: Maybe.some(v))

    return StreamFactory.generate(get, _extract, Maybe.empty(str)).map(lambda p: p.data)
