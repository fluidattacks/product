import logging
import re
import urllib.parse

from fa_purity import (
    Maybe,
    ResultE,
    ResultFactory,
    cast_exception,
)
from fa_purity.json import (
    JsonObj,
)
from pure_requests.response import (
    json_decode,
)
from requests import (
    Response,
)

from ._core import (
    ResponsePage,
)

LOG = logging.getLogger(__name__)


def _extract_offset(item: Response) -> Maybe[str]:
    link = urllib.parse.unquote(item.headers.get("Link", ""))
    match = Maybe.from_optional(re.search("offset=([a-zA-Z0-9:]+)", link))
    is_next = re.search('rel="next"', link)
    result: Maybe[str] = (
        match.map(lambda x: x.group(1))  # type: ignore[misc]
        if is_next
        else Maybe.empty()
    )
    LOG.debug("link: %s; is_next: %s, offset: %s", link, bool(is_next), result)
    return result


def to_page(item: Response) -> ResultE[ResponsePage[JsonObj]]:
    _factory: ResultFactory[ResponsePage[JsonObj], Exception] = ResultFactory()
    return (
        json_decode(item)
        .alt(cast_exception)
        .bind(
            lambda c: c.map(
                lambda _: _factory.failure(TypeError("expected list")).alt(cast_exception),
                lambda d: _factory.success(ResponsePage(d, _extract_offset(item))),
            ),
        )
    )
