import logging
from typing import (
    TypeVar,
)

from fa_purity import (
    Cmd,
    FrozenDict,
    FrozenList,
    Maybe,
    Result,
    ResultE,
    Stream,
    Unsafe,
    cast_exception,
)
from fa_purity.json import (
    JsonObj,
    Primitive,
    UnfoldedFactory,
)
from pure_requests.basic import (
    Endpoint,
    HttpClient,
    Params,
)
from pure_requests.response import (
    json_decode,
)
from requests import (
    RequestException,
    Response,
)

from . import (
    _decoder,
    _paginate,
)
from ._core import (
    ResponsePage,
)
from ._handlers import (
    handle_response,
)

API_URL_BASE = "https://api.bugsnag.com"
LOG = logging.getLogger(__name__)
_K = TypeVar("_K")
_V = TypeVar("_V")


def api_endpoint(endpoint: str) -> Endpoint:
    return Endpoint(API_URL_BASE + "/" + endpoint.lstrip("/"))


def decode_page(
    item: Cmd[Result[Response, RequestException]],
) -> Cmd[ResultE[ResponsePage[JsonObj]]]:
    return handle_response(item).map(lambda r: r.bind(_decoder.to_page))


def decode_single(item: Cmd[Result[Response, RequestException]]) -> Cmd[ResultE[JsonObj]]:
    return handle_response(item).map(
        lambda r: r.bind(lambda x: json_decode(x).alt(cast_exception)).bind(
            lambda c: c.map(
                lambda j: Result.success(j),
                lambda _: Result.failure(TypeError("Expected non-list"), JsonObj).alt(
                    cast_exception,
                ),
            ),
        ),
    )


def _merge(item: FrozenDict[_K, _V], item2: FrozenDict[_K, _V]) -> FrozenDict[_K, _V]:
    return FrozenDict(dict(item) | dict(item2))


def generic_stream_with_params(
    client: HttpClient,
    endpoint: Endpoint,
    per_page: int,
    more_params: JsonObj,
) -> Stream[FrozenList[JsonObj]]:
    def _get_page(offset: Maybe[str]) -> Cmd[ResponsePage[JsonObj]]:
        params: dict[str, Primitive] = {
            "per_page": per_page,
        }
        encoded_offset: dict[str, str] = offset.map(lambda o: {"offset": o}).value_or({})
        _params = _merge(
            UnfoldedFactory.from_dict(params | encoded_offset),
            more_params,
        )
        cmd = client.get(endpoint, Params(_params))
        return decode_page(cmd).map(lambda r: r.alt(Unsafe.raise_exception).to_union())

    return _paginate.paginate_all(_get_page)


def generic_stream(
    client: HttpClient,
    endpoint: Endpoint,
    per_page: int,
) -> Stream[FrozenList[JsonObj]]:
    return generic_stream_with_params(client, endpoint, per_page, UnfoldedFactory.from_dict({}))


__all__ = [
    "ResponsePage",
]
