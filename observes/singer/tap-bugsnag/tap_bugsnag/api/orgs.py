from dataclasses import (
    dataclass,
)

from fa_purity import (
    FrozenList,
    PureIterFactory,
    ResultE,
    Stream,
    StreamTransform,
    Unsafe,
)
from fa_purity.json import (
    JsonObj,
)
from pure_requests.basic import (
    HttpClient,
)

from . import (
    _common,
    _utils,
)
from .core.ids import (
    OrganizationId,
    ProjectId,
)


@dataclass(frozen=True)
class OrgsApi:
    list_collaborators: Stream[FrozenList[JsonObj]]
    list_projects: Stream[FrozenList[JsonObj]]
    list_projects_ids: Stream[ProjectId]


def _list_collaborators(
    client: HttpClient,
    org_id: OrganizationId,
    per_page: int,
) -> Stream[FrozenList[JsonObj]]:
    return _common.generic_stream(
        client,
        _common.api_endpoint("organizations/" + org_id.id_str + "/collaborators"),
        per_page,
    )


def _list_projects(
    client: HttpClient,
    org_id: OrganizationId,
    per_page: int,
) -> Stream[FrozenList[JsonObj]]:
    return _common.generic_stream(
        client,
        _common.api_endpoint("organizations/" + org_id.id_str + "/projects"),
        per_page,
    )


def _decode_proj_id(raw: JsonObj) -> ResultE[ProjectId]:
    return _utils.require_id(raw).map(ProjectId)


def new_org_api(client: HttpClient, org_id: OrganizationId) -> OrgsApi:
    return OrgsApi(
        _list_collaborators(client, org_id, 100),
        _list_projects(client, org_id, 100),
        _list_projects(client, org_id, 100)
        .map(lambda x: PureIterFactory.from_list(x))
        .transform(lambda x: StreamTransform.chain(x))
        .map(lambda j: _decode_proj_id(j).alt(Unsafe.raise_exception).to_union()),
    )
