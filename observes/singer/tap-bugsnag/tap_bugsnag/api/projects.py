from dataclasses import (
    dataclass,
)

from fa_purity import (
    Cmd,
    FrozenList,
    Stream,
    Unsafe,
)
from fa_purity.json import (
    JsonObj,
    UnfoldedFactory,
)
from pure_requests.basic import (
    HttpClient,
    Params,
)

from . import (
    _common,
)
from .core.ids import (
    ProjectId,
)


@dataclass(frozen=True)
class ProjectsApi:
    get_stability_trend: Cmd[JsonObj]
    list_errors: Stream[FrozenList[JsonObj]]
    list_events: Stream[FrozenList[JsonObj]]
    list_event_fields: Stream[FrozenList[JsonObj]]
    list_releases: Stream[FrozenList[JsonObj]]
    list_pivots: Stream[FrozenList[JsonObj]]


def _stability_trend(
    client: HttpClient,
    proj: ProjectId,
) -> Cmd[JsonObj]:
    cmd = client.get(
        _common.api_endpoint("projects/" + proj.id_str + "/stability_trend"),
        Params(UnfoldedFactory.from_dict({})),
    )
    return _common.decode_single(cmd).map(lambda r: r.alt(Unsafe.raise_exception).to_union())


def _list_errors(
    client: HttpClient,
    proj: ProjectId,
    per_page: int,
) -> Stream[FrozenList[JsonObj]]:
    return _common.generic_stream_with_params(
        client,
        _common.api_endpoint("projects/" + proj.id_str + "/errors"),
        per_page,
        UnfoldedFactory.from_dict({"sort": "unsorted"}),
    )


def _list_events(
    client: HttpClient,
    proj: ProjectId,
    per_page: int,
) -> Stream[FrozenList[JsonObj]]:
    return _common.generic_stream(
        client,
        _common.api_endpoint("projects/" + proj.id_str + "/events"),
        per_page,
    )


def _list_event_fields(
    client: HttpClient,
    proj: ProjectId,
    per_page: int,
) -> Stream[FrozenList[JsonObj]]:
    return _common.generic_stream(
        client,
        _common.api_endpoint("projects/" + proj.id_str + "/event_fields"),
        per_page,
    )


def _list_releases(
    client: HttpClient,
    proj: ProjectId,
    per_page: int,
) -> Stream[FrozenList[JsonObj]]:
    return _common.generic_stream(
        client,
        _common.api_endpoint("projects/" + proj.id_str + "/releases"),
        per_page,
    )


def _list_pivots(
    client: HttpClient,
    proj: ProjectId,
    per_page: int,
) -> Stream[FrozenList[JsonObj]]:
    return _common.generic_stream(
        client,
        _common.api_endpoint("projects/" + proj.id_str + "/pivots"),
        per_page,
    )


def new_proj_api(client: HttpClient, proj: ProjectId) -> ProjectsApi:
    return ProjectsApi(
        _stability_trend(client, proj),
        _list_errors(client, proj, 100),
        _list_events(client, proj, 100),
        _list_event_fields(client, proj, 100),
        _list_releases(client, proj, 10),
        _list_pivots(client, proj, 100),
    )
