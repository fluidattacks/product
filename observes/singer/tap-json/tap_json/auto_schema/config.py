from .jschema.number import (
    IntSizes,
)
from .jschema.object import (
    ObjectPropertyType,
)
from dataclasses import (
    dataclass,
)
from fa_purity import (
    FrozenDict,
    Result,
    ResultE,
)
from fa_purity.json_2 import (
    JsonObj,
    JsonPrimitiveUnfolder,
    JsonUnfolder,
    Unfolder,
)
from fa_purity.pure_iter import (
    PureIterFactory,
)
from fa_purity.result.transform import (
    all_ok,
)
from tap_json.auto_schema.jschema.decode import (
    DEFAULT_DECODER,
    TypeDecoder,
)


@dataclass(frozen=True)
class InferenceConfig:
    min_str_len: int
    min_int_size: IntSizes
    custom_properties_types: FrozenDict[str, ObjectPropertyType]


def _decode_custom(
    raw: FrozenDict[str, JsonObj], type_decoder: TypeDecoder
) -> ResultE[FrozenDict[str, ObjectPropertyType]]:
    return (
        PureIterFactory.from_list(tuple(raw.items()))
        .map(
            lambda t: type_decoder.decode_obj_prop_type(t[1]).map(
                lambda o: (t[0], o)
            )
        )
        .transform(
            lambda p: all_ok(p.to_list()).map(lambda d: FrozenDict(dict(d)))
        )
    )


def decode_config(raw: JsonObj) -> ResultE[InferenceConfig]:
    _min_str = JsonUnfolder.optional(
        raw,
        "min_str_len",
        lambda v: Unfolder.to_primitive(v).bind(JsonPrimitiveUnfolder.to_int),
    ).map(lambda m: m.value_or(1))
    _min_int_size = JsonUnfolder.optional(
        raw,
        "min_int_size",
        lambda v: Unfolder.to_primitive(v)
        .bind(JsonPrimitiveUnfolder.to_str)
        .bind(IntSizes.from_raw),
    ).map(lambda m: m.value_or(IntSizes.SMALL))
    empty: ResultE[FrozenDict[str, ObjectPropertyType]] = Result.success(
        FrozenDict({})
    )
    _custom = JsonUnfolder.optional(
        raw,
        "custom_properties_types",
        lambda v: Unfolder.to_dict_of(v, Unfolder.to_json),
    ).bind(
        lambda m: m.map(lambda v: _decode_custom(v, DEFAULT_DECODER)).value_or(
            empty
        )
    )
    return _min_str.bind(
        lambda min_str: _min_int_size.bind(
            lambda min_int_size: _custom.map(
                lambda c: InferenceConfig(min_str, min_int_size, c)
            )
        )
    )
