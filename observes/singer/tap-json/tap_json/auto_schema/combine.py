from fa_purity import (
    FrozenList,
    Maybe,
)
from fa_purity.pure_iter import (
    PureIterFactory,
)
from tap_json.auto_schema.jschema.number import (
    DecimalType,
    IntSizes,
    IntType,
    NumberType,
)
from tap_json.auto_schema.jschema.object import (
    ObjectPropertyType,
)
from tap_json.auto_schema.jschema.string import (
    MetaType,
    StringType,
)


def combine_strings(left: StringType, right: StringType) -> StringType:
    _precision = (
        left.precision if left.precision > right.precision else right.precision
    )
    if left.format == right.format:
        if left.meta_type == right.meta_type:
            return StringType.new(
                left.meta_type, left.format, _precision
            ).unwrap()
        return StringType.new(
            MetaType.DYNAMIC, left.format, _precision
        ).unwrap()
    return StringType.new(MetaType.DYNAMIC, Maybe.empty(), _precision).unwrap()


def combine_int(left: IntType, right: IntType) -> IntType:
    def _to_int(_type: IntSizes) -> int:
        match _type:
            case IntSizes.SMALL:
                return 1
            case IntSizes.NORMAL:
                return 2
            case IntSizes.BIG:
                return 3

    if _to_int(left.size) > _to_int(right.size):
        return left
    return right


def combine_decimal(high: DecimalType, low: DecimalType) -> DecimalType:
    non_decimal_digits = max(
        [high.precision - high.scale, low.precision - low.scale]
    )
    scale = max([high.scale, low.scale])
    return DecimalType.new(non_decimal_digits + scale, scale).unwrap()


def combine_number(high: NumberType, low: NumberType) -> NumberType:
    return high.map(
        lambda d: low.map(
            lambda d2: NumberType.from_decimal(combine_decimal(d, d2)),
            lambda _: high,  # decimal is > any int type
            lambda _: low,  # decimal is NOT > any float type
        ),
        lambda i: low.map(
            lambda _: low,  # int is NOT > any decimal type
            lambda i2: NumberType.from_int(combine_int(i, i2)),
            lambda _: low,  # int is NOT > any float type
        ),
        lambda _: low,
    )


def combine_object_type(
    high: ObjectPropertyType, low: ObjectPropertyType
) -> ObjectPropertyType:
    return high.map(
        lambda n: low.map(
            lambda n2: ObjectPropertyType.from_number_type(
                combine_number(n, n2)
            ),
            lambda _: low,  # number type is NOT > any string type
            high,  # number type is > bool type
            high,  # number type is > null type
        ),
        lambda s: low.map(
            lambda _: high,  # string type is > any number type
            lambda s2: ObjectPropertyType.from_str(combine_strings(s, s2)),
            high,  # string type is > bool type
            high,  # string type is > null type
        ),
        low.map(
            lambda _: low,  # bool type is NOT > any number type
            lambda _: low,  # bool type is NOT > any string type
            low,  # bool type is not > itself
            high,  # bool type is > null type
        ),
        low.map(  # null type is NOT > any other type
            lambda _: low,
            lambda _: low,
            low,
            low,
        ),
    )


def merge_types(types: FrozenList[ObjectPropertyType]) -> ObjectPropertyType:
    result = PureIterFactory.from_list(types).reduce(
        combine_object_type, ObjectPropertyType.null_type()
    )
    if result == ObjectPropertyType.null_type():
        return ObjectPropertyType.from_str(
            StringType.new(MetaType.DYNAMIC, Maybe.empty(), 256).unwrap()
        )
    return result
