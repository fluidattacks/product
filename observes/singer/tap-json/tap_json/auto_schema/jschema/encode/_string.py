from fa_purity import (
    FrozenDict,
    ResultE,
)
from fa_purity.json_2 import (
    JsonPrimitive,
    JsonValue,
    LegacyAdapter,
)
from fa_singer_io.json_schema import (
    JSchemaFactory,
    JsonSchema,
)
from tap_json.auto_schema.jschema.string import (
    StringType,
)


def encode_string_type(obj: StringType) -> ResultE[JsonSchema]:
    base = LegacyAdapter.json(JSchemaFactory.opt_prim_type(str).encode())
    more_data = FrozenDict(
        {
            "precision": JsonValue.from_primitive(
                JsonPrimitive.from_int(obj.precision)
            ),
            "metatype": JsonValue.from_primitive(
                JsonPrimitive.from_int(obj.meta_type.value)
            ),
        }
        | obj.format.map(
            lambda f: {
                "format": JsonValue.from_primitive(
                    JsonPrimitive.from_str(f.value)
                )
            }
        ).value_or({})
    )
    raw = FrozenDict(dict(base) | dict(more_data))
    return JSchemaFactory.from_json(LegacyAdapter.to_legacy_json(raw))
