from . import (
    _number,
    _object,
    _string,
)
from collections.abc import (
    Callable,
)
from dataclasses import (
    dataclass,
)
from fa_purity import (
    ResultE,
)
from fa_singer_io.json_schema import (
    JsonSchema,
)
from tap_json.auto_schema.jschema.number import (
    NumberType,
)
from tap_json.auto_schema.jschema.object import (
    ObjectPropertyType,
    ObjectType,
)
from tap_json.auto_schema.jschema.string import (
    StringType,
)


@dataclass(frozen=True)
class TypeEncoder:
    encode_number: Callable[[NumberType], ResultE[JsonSchema]]
    encode_string_type: Callable[[StringType], ResultE[JsonSchema]]
    encode_obj_prop_type: Callable[[ObjectPropertyType], ResultE[JsonSchema]]
    encode_obj_type: Callable[[ObjectType], ResultE[JsonSchema]]


DEFAULT_ENCODER = TypeEncoder(
    _number.encode_number,
    _string.encode_string_type,
    _object.encode_obj_prop_type,
    _object.encode_obj_type,
)
