from decimal import (
    Decimal,
)
from fa_purity import (
    FrozenDict,
    ResultE,
)
from fa_purity.json_2 import (
    JsonValue,
    LegacyAdapter,
)
from fa_purity.json_2.primitive import (
    JsonPrimitive,
)
from fa_singer_io.json_schema import (
    JSchemaFactory,
    JsonSchema,
)
from tap_json.auto_schema.jschema.number import (
    DecimalType,
    FloatType,
    IntType,
    NumberType,
    NumSizes,
)


def encode_decimal_type(obj: DecimalType) -> ResultE[JsonSchema]:
    base = LegacyAdapter.json(JSchemaFactory.opt_prim_type(Decimal).encode())
    more_data = FrozenDict(
        {
            "precision": JsonValue.from_primitive(
                JsonPrimitive.from_int(obj.precision)
            ),
            "scale": JsonValue.from_primitive(
                JsonPrimitive.from_int(obj.scale)
            ),
            "size": JsonValue.from_primitive(
                JsonPrimitive.from_str(NumSizes.EXACT.value)
            ),
        }
    )
    raw = FrozenDict(dict(base) | dict(more_data))
    return JSchemaFactory.from_json(LegacyAdapter.to_legacy_json(raw))


def encode_int(obj: IntType) -> ResultE[JsonSchema]:
    base = LegacyAdapter.json(JSchemaFactory.opt_prim_type(int).encode())
    more_data = FrozenDict(
        {
            "size": JsonValue.from_primitive(
                JsonPrimitive.from_str(obj.size.value)
            ),
        }
    )
    raw = FrozenDict(dict(base) | dict(more_data))
    return JSchemaFactory.from_json(LegacyAdapter.to_legacy_json(raw))


def encode_float(obj: FloatType) -> ResultE[JsonSchema]:
    base = LegacyAdapter.json(JSchemaFactory.opt_prim_type(float).encode())
    more_data = FrozenDict(
        {
            "size": JsonValue.from_primitive(
                JsonPrimitive.from_str(obj.size.to_str())
            ),
        }
    )
    raw = FrozenDict(dict(base) | dict(more_data))
    return JSchemaFactory.from_json(LegacyAdapter.to_legacy_json(raw))


def encode_number(obj: NumberType) -> ResultE[JsonSchema]:
    return obj.map(
        encode_decimal_type,
        encode_int,
        encode_float,
    )
