from ._number import (
    encode_number,
)
from ._string import (
    encode_string_type,
)
from fa_purity import (
    FrozenDict,
    Result,
    ResultE,
)
from fa_purity.json_2 import (
    JsonValue,
    LegacyAdapter,
)
from fa_purity.pure_iter.factory import (
    PureIterFactory,
)
from fa_purity.result.transform import (
    all_ok,
)
from fa_singer_io.json_schema import (
    JSchemaFactory,
    JsonSchema,
)
from tap_json.auto_schema.jschema.object import (
    ObjectPropertyType,
    ObjectType,
)


def encode_obj_prop_type(obj: ObjectPropertyType) -> ResultE[JsonSchema]:
    return obj.map(
        encode_number,
        encode_string_type,
        Result.success(JSchemaFactory.from_prim_type(bool)),
        Result.success(JSchemaFactory.from_prim_type(type(None))),
    )


def encode_obj_type(
    obj: ObjectType,
) -> ResultE[JsonSchema]:
    items = PureIterFactory.from_list(tuple(obj.properties.items())).map(
        lambda t: encode_obj_prop_type(t[1]).map(
            lambda r: (
                t[0],
                JsonValue.from_json(LegacyAdapter.json(r.encode())),
            )
        )
    )
    raw = (
        all_ok(items.to_list())
        .map(lambda d: FrozenDict(dict(d)))
        .map(
            lambda d: FrozenDict(
                {
                    "properties": JsonValue.from_json(d),
                }
            )
        )
    )
    return raw.bind(
        lambda j: JSchemaFactory.from_json(LegacyAdapter.to_legacy_json(j))
    )
