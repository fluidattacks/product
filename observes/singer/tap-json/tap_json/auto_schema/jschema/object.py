from __future__ import (
    annotations,
)

from .number import (
    NumberType,
)
from .string import (
    StringType,
)
from collections.abc import (
    Callable,
)
from dataclasses import (
    dataclass,
)
from fa_purity import (
    FrozenDict,
)
from fa_purity._core.coproduct import (
    Coproduct,
)
from typing import (
    TypeVar,
)

_T = TypeVar("_T")


@dataclass(frozen=True)
class NullType:
    pass


@dataclass(frozen=True)
class BoolType:
    pass


@dataclass(frozen=True)
class ObjectPropertyType:
    _inner: Coproduct[
        NumberType, Coproduct[StringType, Coproduct[BoolType, NullType]]
    ]

    @staticmethod
    def from_number_type(number_type: NumberType) -> ObjectPropertyType:
        return ObjectPropertyType(Coproduct.inl(number_type))

    @staticmethod
    def from_str(str_type: StringType) -> ObjectPropertyType:
        return ObjectPropertyType(Coproduct.inr(Coproduct.inl(str_type)))

    @staticmethod
    def bool_type() -> ObjectPropertyType:
        return ObjectPropertyType(
            Coproduct.inr(Coproduct.inr(Coproduct.inl(BoolType())))
        )

    @staticmethod
    def null_type() -> ObjectPropertyType:
        return ObjectPropertyType(
            Coproduct.inr(Coproduct.inr(Coproduct.inr(NullType())))
        )

    def map(
        self,
        number_case: Callable[[NumberType], _T],
        string_case: Callable[[StringType], _T],
        bool_case: _T,
        null_case: _T,
    ) -> _T:
        return self._inner.map(
            number_case,
            lambda c: c.map(
                string_case,
                lambda c2: c2.map(lambda _: bool_case, lambda _: null_case),
            ),
        )

    def __repr__(self) -> str:
        return self._inner.map(
            str,
            lambda c: c.map(str, lambda c2: c2.map(str, str)),
        )


@dataclass(frozen=True)
class ObjectType:
    properties: FrozenDict[str, ObjectPropertyType]
