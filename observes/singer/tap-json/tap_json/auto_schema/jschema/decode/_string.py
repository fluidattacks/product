from . import (
    _core,
)
from ._core import (
    RawType,
)
from fa_purity import (
    Result,
    ResultE,
)
from fa_purity.json_2 import (
    JsonObj,
    JsonPrimitiveUnfolder,
    JsonUnfolder,
    Unfolder,
)
from fa_purity.utils import (
    cast_exception,
)
from tap_json.auto_schema.jschema.string import (
    MetaType,
    StringFormat,
    StringType,
)


def _ensure_str_type(raw: RawType) -> ResultE[RawType]:
    if raw is RawType.STRING:
        return Result.success(raw)
    return Result.failure(ValueError("Expected a str type"), RawType).alt(
        cast_exception
    )


def _decode_string_type(encoded: JsonObj) -> ResultE[StringType]:
    _precision = JsonUnfolder.optional(
        encoded,
        "precision",
        lambda v: Unfolder.to_primitive(v).bind(JsonPrimitiveUnfolder.to_int),
    ).map(lambda m: m.value_or(256))
    _format = JsonUnfolder.optional(
        encoded,
        "format",
        lambda v: Unfolder.to_primitive(v)
        .bind(JsonPrimitiveUnfolder.to_str)
        .bind(StringFormat.from_raw),
    )
    _meta_type: ResultE[MetaType] = JsonUnfolder.optional(
        encoded,
        "metatype",
        lambda v: Unfolder.to_primitive(v)
        .bind(JsonPrimitiveUnfolder.to_str)
        .bind(MetaType.from_raw),
    ).map(lambda m: m.value_or(MetaType.DYNAMIC))
    return _precision.bind(
        lambda precision: _format.bind(
            lambda f: _meta_type.bind(
                lambda meta: StringType.new(meta, f, precision)
            )
        )
    )


def decode_string_type(encoded: JsonObj) -> ResultE[StringType]:
    _type: ResultE[RawType] = JsonUnfolder.require(
        encoded,
        "type",
        _core.decode_type,
    ).bind(_ensure_str_type)
    return _type.bind(lambda _: _decode_string_type(encoded))
