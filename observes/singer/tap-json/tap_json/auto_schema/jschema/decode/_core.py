from __future__ import (
    annotations,
)

from enum import (
    Enum,
)
from fa_purity import (
    FrozenList,
    Result,
    ResultE,
)
from fa_purity.json_2 import (
    JsonPrimitiveUnfolder,
    JsonValue,
    Unfolder,
)
from fa_purity.pure_iter import (
    PureIterFactory,
)
from fa_purity.result.transform import (
    all_ok,
)
from fa_purity.utils import (
    cast_exception,
)


class RawType(Enum):
    STRING = "string"
    NUMBER = "number"
    INTEGER = "integer"
    OBJECT = "object"
    ARRAY = "array"
    BOOLEAN = "boolean"
    NULL = "null"

    @staticmethod
    def from_raw(raw: str) -> ResultE[RawType]:
        try:
            return Result.success(RawType(raw.lower()))
        except ValueError as err:
            return Result.failure(err, RawType).alt(Exception)


def _decode_raw_type(raw: JsonValue) -> ResultE[RawType]:
    return (
        Unfolder.to_primitive(raw)
        .bind(JsonPrimitiveUnfolder.to_str)
        .bind(RawType.from_raw)
    )


def _ensure_one_type(raw: FrozenList[RawType]) -> ResultE[RawType]:
    types = (
        PureIterFactory.from_list(raw)
        .filter(lambda r: r is not RawType.NULL)
        .to_list()
    )
    if len(types) == 1:
        return Result.success(types[0])
    if len(types) == 0:
        return Result.success(RawType.NULL)
    return Result.failure(
        ValueError("Multiple types not supported"), RawType
    ).alt(cast_exception)


def _handle_multi_type(raw: JsonValue) -> ResultE[RawType]:
    return (
        Unfolder.to_list(raw)
        .bind(
            lambda i: all_ok(
                PureIterFactory.from_list(i).map(_decode_raw_type).to_list()
            )
        )
        .bind(_ensure_one_type)
    )


def decode_type(raw: JsonValue) -> ResultE[RawType]:
    return _decode_raw_type(raw).lash(lambda _: _handle_multi_type(raw))
