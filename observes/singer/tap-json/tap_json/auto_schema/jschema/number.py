from __future__ import (
    annotations,
)

from collections.abc import (
    Callable,
)
from dataclasses import (
    dataclass,
    field,
)
from enum import (
    Enum,
)
from fa_purity import (
    Result,
    ResultE,
)
from fa_purity._core.coproduct import (
    Coproduct,
)
from fa_purity.utils import (
    cast_exception,
)
from typing import (
    Literal,
    TypeVar,
)

_T = TypeVar("_T")


class NumSizes(Enum):
    FLOAT = "float"
    BIG_FLOAT = "big_float"
    EXACT = "exact"

    def to_str(self) -> str:
        return self.value

    @staticmethod
    def from_raw(raw: str) -> ResultE[NumSizes]:
        try:
            return Result.success(NumSizes(raw.lower()))
        except ValueError as err:
            return Result.failure(err, NumSizes).alt(Exception)


@dataclass(frozen=True)
class FloatType:
    size: Literal[NumSizes.FLOAT, NumSizes.BIG_FLOAT]


@dataclass(frozen=True)
class DecimalType:
    _private: DecimalType._Private = field(
        repr=False, hash=False, compare=False
    )
    precision: int
    scale: int

    @dataclass(frozen=True)
    class _Private:
        pass

    @staticmethod
    def new(
        precision: int,
        scale: int,
    ) -> ResultE[DecimalType]:
        if precision > 0 and scale >= 0:
            return Result.success(
                DecimalType(DecimalType._Private(), precision, scale)
            )
        return Result.failure(
            ValueError("precision <= 0 or scale < 0"), DecimalType
        ).alt(cast_exception)


class IntSizes(Enum):
    SMALL = "small"
    NORMAL = "normal"
    BIG = "big"

    @staticmethod
    def from_raw(raw: str) -> ResultE[IntSizes]:
        try:
            return Result.success(IntSizes(raw.lower()))
        except ValueError as err:
            return Result.failure(err, IntSizes).alt(Exception)

    def __gt__(self, other: IntSizes) -> bool:
        match self:
            case IntSizes.SMALL:
                return False
            case IntSizes.NORMAL:
                match other:
                    case IntSizes.SMALL:
                        return True
                    case IntSizes.NORMAL:
                        return False
                    case IntSizes.BIG:
                        return False
            case IntSizes.BIG:
                match other:
                    case IntSizes.SMALL:
                        return True
                    case IntSizes.NORMAL:
                        return True
                    case IntSizes.BIG:
                        return False


@dataclass(frozen=True)
class IntType:
    size: IntSizes


@dataclass(frozen=True)
class NumberType:
    _inner: Coproduct[DecimalType, Coproduct[IntType, FloatType]]

    @staticmethod
    def from_decimal(decimal_type: DecimalType) -> NumberType:
        return NumberType(Coproduct.inl(decimal_type))

    @staticmethod
    def from_int(int_type: IntType) -> NumberType:
        return NumberType(Coproduct.inr(Coproduct.inl(int_type)))

    @staticmethod
    def from_float(float_type: FloatType) -> NumberType:
        return NumberType(Coproduct.inr(Coproduct.inr(float_type)))

    def map(
        self,
        decimal_type: Callable[[DecimalType], _T],
        int_type: Callable[[IntType], _T],
        float_type: Callable[[FloatType], _T],
    ) -> _T:
        return self._inner.map(
            decimal_type,
            lambda c: c.map(
                int_type,
                float_type,
            ),
        )

    def __repr__(self) -> str:
        return self._inner.map(
            str,
            lambda c: c.map(str, str),
        )
