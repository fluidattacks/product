from __future__ import (
    annotations,
)

from collections.abc import (
    Callable,
)
from dataclasses import (
    dataclass,
)
from fa_purity import (
    FrozenDict,
    FrozenList,
)
from fa_purity.json_2 import (
    JsonPrimitive,
)
from fa_purity.union import (
    Coproduct,
)
from tap_json._core import (
    CleanString,
)
from typing import (
    TypeVar,
)

_T = TypeVar("_T")
UnfoldedJsonValueFlatDicts = Coproduct[
    JsonPrimitive,
    Coproduct[
        FrozenList["PreFlatJsonValue"],
        FrozenDict[
            CleanString,
            Coproduct[FrozenList["PreFlatJsonValue"], JsonPrimitive],
        ],
    ],
]


@dataclass(frozen=True)
class PreFlatJsonValue:
    _value: UnfoldedJsonValueFlatDicts

    def unfold(
        self,
    ) -> UnfoldedJsonValueFlatDicts:
        return self._value

    @staticmethod
    def from_primitive(value: JsonPrimitive) -> PreFlatJsonValue:
        return PreFlatJsonValue(Coproduct.inl(value))

    @staticmethod
    def from_list(value: FrozenList[PreFlatJsonValue]) -> PreFlatJsonValue:
        return PreFlatJsonValue(Coproduct.inr(Coproduct.inl(value)))

    @staticmethod
    def from_dict(
        value: FrozenDict[
            CleanString, Coproduct[FrozenList[PreFlatJsonValue], JsonPrimitive]
        ]
    ) -> PreFlatJsonValue:
        return PreFlatJsonValue(Coproduct.inr(Coproduct.inr(value)))

    def map(
        self,
        primitive_case: Callable[[JsonPrimitive], _T],
        list_case: Callable[[FrozenList[PreFlatJsonValue]], _T],
        dict_case: Callable[
            [
                FrozenDict[
                    CleanString,
                    Coproduct[FrozenList[PreFlatJsonValue], JsonPrimitive],
                ]
            ],
            _T,
        ],
    ) -> _T:
        return self._value.map(
            primitive_case,
            lambda c: c.map(
                list_case,
                dict_case,
            ),
        )
