from ._flattener import (
    simplify_json,
)
from ._to_table import (
    to_table_records,
)
from fa_purity import (
    PureIter,
)
from fa_purity.json_2 import (
    LegacyAdapter,
)
from fa_singer_io.singer import (
    SingerRecord,
)
from tap_json._core import (
    TableRecordPair,
)


def flat_record(record: SingerRecord) -> PureIter[TableRecordPair]:
    return to_table_records(
        record.stream,
        simplify_json(LegacyAdapter.json(record.record)),
        tuple(),
    )
