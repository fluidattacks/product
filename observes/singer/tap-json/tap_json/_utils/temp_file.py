from __future__ import (
    annotations,
)

from .io_file import (
    FileProperties,
    IOFactory,
    ReadableIO,
    UnnamedIO,
    WritableIO,
)
from dataclasses import (
    dataclass,
)
from fa_purity import (
    Cmd,
    PureIter,
    Stream,
)
from fa_purity.cmd.core import (
    CmdUnwrapper,
    new_cmd,
)
from fa_purity.pure_iter.factory import (
    unsafe_from_cmd,
)
from fa_purity.stream.factory import (
    unsafe_from_cmd as unsafe_build_stream,
)
import logging
from pathlib import (
    Path,
)
from tempfile import (
    NamedTemporaryFile,
)
from typing import (
    Callable,
    IO,
    Iterable,
    TypeVar,
)

LOG = logging.getLogger(__name__)
_T = TypeVar("_T")


@dataclass(frozen=True)
class _Inner:
    file_path: Path


@dataclass(frozen=True)
class TempFile:
    _inner: _Inner

    @staticmethod
    def new() -> Cmd[TempFile]:
        def _action() -> TempFile:
            with NamedTemporaryFile("w", delete=False) as file_object:
                file_object.write("")
                return TempFile(_Inner(Path(file_object.name)))

        return Cmd.from_cmd(_action)

    @property
    def path(self) -> Path:
        "TempFile is mutable hence exposing the path is not an issue"
        return self._inner.file_path

    def append(
        self, write_cmd: Callable[[WritableIO[str]], Cmd[_T]]
    ) -> Cmd[_T]:
        """
        - opens file in append mode and executes `write_cmd`

        [WARNING] executing concurrent `append` commands on the same `TempFile`
        would result in unexpected behavior.
        Wrap this command using `execute_with_lock` from a common `Lock` object
        if concurrency is required.
        """
        return IOFactory.open_append(
            self._inner.file_path, "utf-8", lambda _, w, __: write_cmd(w)
        )

    def read(self, read_cmd: Callable[[ReadableIO[str]], Cmd[_T]]) -> Cmd[_T]:
        return IOFactory.open_read(
            self._inner.file_path, "utf-8", lambda _, w, __: read_cmd(w)
        )

    def read_lines(self) -> Stream[str]:
        def _new_iter(
            unwrapper: CmdUnwrapper, file: ReadableIO[str]
        ) -> Iterable[str]:
            line = unwrapper.act(file.read_line())
            while line:
                yield line
                line = unwrapper.act(file.read_line())

        return IOFactory.open_read_for_stream(
            self._inner.file_path,
            "utf-8",
            lambda _, r, __: Cmd.from_cmd(
                lambda: unsafe_build_stream(
                    Cmd.new_cmd(lambda u: _new_iter(u, r))
                )
            ),
        )

    def write(
        self, write_cmd: Callable[[WritableIO[str]], Cmd[_T]]
    ) -> Cmd[_T]:
        return IOFactory.open_write(
            self._inner.file_path, "utf-8", lambda _, w, __: write_cmd(w)
        )

    def write_lines(self, lines: PureIter[str]) -> Cmd[None]:
        return self.write(lambda f: f.write_lines(lines))


@dataclass(frozen=True)
class TempReadOnlyFile:
    """
    is not possible to get the path of the file
    i.e. for protecting it from opening with write permissions
    """

    _inner: _Inner

    @staticmethod
    def from_iter(content: PureIter[str]) -> Cmd[TempReadOnlyFile]:
        def _action() -> TempReadOnlyFile:
            with NamedTemporaryFile("w", delete=False) as file_object:
                file_object.writelines(content)
                return TempReadOnlyFile(_Inner(Path(file_object.name)))

        return Cmd.from_cmd(_action)

    @staticmethod
    def new(
        write_cmd: Callable[[WritableIO[str]], Cmd[None]]
    ) -> Cmd[TempReadOnlyFile]:
        """
        `write_cmd` initializes the file content. It has write-only access to file
        """
        temp = IOFactory.open_named_temp_write(
            False, "utf-8", lambda p, w, _: write_cmd(w).map(lambda _: p)
        )
        return temp.map(lambda p: TempReadOnlyFile(_Inner(Path(p.name))))

    @staticmethod
    def save(content: Stream[str]) -> Cmd[TempReadOnlyFile]:
        def _new(
            file_props: FileProperties[str], file: WritableIO[str]
        ) -> Cmd[TempReadOnlyFile]:
            msg = Cmd.from_cmd(
                lambda: LOG.debug("Saving stream into %s", file_props.name)
            )
            return msg + file.write_stream(content).map(
                lambda _: TempReadOnlyFile(_Inner(Path(file_props.name)))
            )

        return IOFactory.open_named_temp_write(
            False, "utf-8", lambda p, w, _: _new(p, w)
        )

    @classmethod
    def freeze_io(cls, file: IO[str]) -> Cmd[TempReadOnlyFile]:
        def _action() -> Iterable[str]:
            file.seek(0)
            line = file.readline()
            while line:
                yield line
                line = file.readline()

        start = Cmd.from_cmd(lambda: LOG.debug("Freezing file"))
        end = Cmd.from_cmd(lambda: LOG.debug("Freezing completed!"))
        stream = unsafe_build_stream(Cmd.from_cmd(_action))
        return start + cls.save(stream).bind(lambda f: end.map(lambda _: f))

    @classmethod
    def freeze(cls, file_path: str) -> Cmd[TempReadOnlyFile]:
        def _action(unwrapper: CmdUnwrapper) -> TempReadOnlyFile:
            with open(file_path, "r", encoding="utf-8") as file:
                return unwrapper.act(cls.freeze_io(file))

        return Cmd.new_cmd(_action)

    def over_binary(
        self, cmd_fx: Callable[[UnnamedIO[bytes]], Cmd[_T]]
    ) -> Cmd[_T]:
        def _action(act: CmdUnwrapper) -> _T:
            with open(self._inner.file_path, "rb") as file:
                return act.unwrap(cmd_fx(UnnamedIO(file)))

        return new_cmd(_action)

    def read(self) -> PureIter[str]:
        def _new_iter() -> Iterable[str]:
            with open(self._inner.file_path, "r", encoding="utf-8") as file:
                line = file.readline()
                while line:
                    yield line
                    line = file.readline()

        return unsafe_from_cmd(Cmd.from_cmd(_new_iter))
