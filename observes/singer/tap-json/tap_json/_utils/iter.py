from fa_purity import (
    Cmd,
    PureIter,
)
from fa_purity.pure_iter.factory import (
    unsafe_from_cmd,
)
from typing import (
    Iterable,
    TypeVar,
)

_T = TypeVar("_T")


def append(items_1: PureIter[_T], items_2: PureIter[_T]) -> PureIter[_T]:
    def _new_iter() -> Iterable[_T]:
        for i in items_1:
            yield i
        for j in items_2:
            yield j

    return unsafe_from_cmd(Cmd.from_cmd(_new_iter))
