from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
    field,
)
from fa_purity import (
    Cmd,
    FrozenList,
    PureIter,
    Stream,
)
from fa_purity.cmd.core import (
    CmdUnwrapper,
)
from fa_purity.stream.factory import (
    unsafe_from_cmd,
)
import logging
from pathlib import (
    Path,
)
from tempfile import (
    NamedTemporaryFile,
)
from types import (
    TracebackType,
)
from typing import (
    AnyStr,
    Callable,
    Generic,
    IO,
    Iterable,
    Iterator,
    Type,
    TypeVar,
)

LOG = logging.getLogger(__name__)
_T = TypeVar("_T")


@dataclass(frozen=True)
class _Private:
    pass


class UnnamedIO(IO[AnyStr]):
    "[WARNING] Impure object"

    def __init__(self, io: IO[AnyStr]) -> None:
        self._wrapped_io: IO[AnyStr] = io

    @property
    def name(self) -> str:
        return "[masked]"

    def __enter__(self) -> UnnamedIO[AnyStr]:
        return self

    def __exit__(
        self,
        __t: Type[BaseException] | None,
        __value: BaseException | None,
        __traceback: TracebackType | None,
    ) -> None:
        return self._wrapped_io.__exit__(__t, __value, __traceback)

    def __iter__(self) -> Iterator[AnyStr]:
        return self._wrapped_io.__iter__()

    def __next__(self) -> AnyStr:
        return self._wrapped_io.__next__()

    def close(self) -> None:
        return self._wrapped_io.close()

    def fileno(self) -> int:
        return self._wrapped_io.fileno()

    def flush(self) -> None:
        return self._wrapped_io.flush()

    def isatty(self) -> bool:
        return self._wrapped_io.isatty()

    def read(self, __n: int = -1) -> AnyStr:
        return self._wrapped_io.read(__n)

    def readable(self) -> bool:
        return self._wrapped_io.readable()

    def readline(self, __limit: int = -1) -> AnyStr:
        return self._wrapped_io.readline(__limit)

    def readlines(self, __hint: int = -1) -> list[AnyStr]:
        return self._wrapped_io.readlines(__hint)

    def seek(self, __offset: int, __whence: int = 0) -> int:
        return self._wrapped_io.seek(__offset, __whence)

    def seekable(self) -> bool:
        return self._wrapped_io.seekable()

    def tell(self) -> int:
        return self._wrapped_io.tell()

    def truncate(self, __size: int | None = None) -> int:
        return self._wrapped_io.truncate(__size)

    def writable(self) -> bool:
        return self._wrapped_io.writable()

    def write(self, __s: AnyStr) -> int:
        return self._wrapped_io.write(__s)

    def writelines(self, __lines: Iterable[AnyStr]) -> None:
        return self._wrapped_io.writelines(__lines)


@dataclass(frozen=True)
class SeekableIO(Generic[AnyStr]):
    _private: _Private = field(repr=False, hash=False, compare=False)
    _inner: IO[AnyStr]

    def seek(self, offset: int, origin: int = 0) -> Cmd[int]:
        """
        Set the file cursor to the supplied offset.
        offset is relative to origin, if origin = 0
        then global offset and supplied offset match.
        """
        return Cmd.from_cmd(lambda: self._inner.seek(offset, origin))

    def tell(self) -> Cmd[int]:
        "Actual bytes offset of the file cursor"
        return Cmd.from_cmd(lambda: self._inner.tell())

    def get_inner(self, _private: _Private) -> IO[AnyStr]:
        "Protected method"
        return self._inner


@dataclass(frozen=True)
class FileProperties(Generic[AnyStr]):
    _private: _Private = field(repr=False, hash=False, compare=False)
    _inner: IO[AnyStr]

    @property
    def name(self) -> str:
        return "[masked]"

    def fileno(self) -> Cmd[int]:
        return Cmd.from_cmd(lambda: self._inner.fileno())

    def isatty(self) -> Cmd[bool]:
        return Cmd.from_cmd(lambda: self._inner.isatty())

    def get_inner(self, _private: _Private) -> IO[AnyStr]:
        "Protected method"
        return self._inner


@dataclass(frozen=True)
class ReadableIO(Generic[AnyStr]):
    _private: _Private = field(repr=False, hash=False, compare=False)
    _inner: IO[AnyStr]

    def read(self, __n: int = -1) -> Cmd[AnyStr]:
        return Cmd.from_cmd(lambda: self._inner.read(__n))

    def read_line(self, limit: int = -1) -> Cmd[AnyStr]:
        return Cmd.from_cmd(lambda: self._inner.readline(limit))

    def read_lines(self, hint: int = -1) -> Cmd[FrozenList[AnyStr]]:
        return Cmd.from_cmd(lambda: tuple(self._inner.readlines(hint)))

    def get_inner(self, _private: _Private) -> IO[AnyStr]:
        "Protected method"
        return self._inner


@dataclass(frozen=True)
class WritableIO(Generic[AnyStr]):
    _private: _Private = field(repr=False, hash=False, compare=False)
    _inner: IO[AnyStr]

    def flush(self) -> Cmd[None]:
        return Cmd.from_cmd(lambda: self._inner.flush())

    def truncate(self, __size: int | None = None) -> Cmd[int]:
        return Cmd.from_cmd(lambda: self._inner.truncate(__size))

    def write(self, __s: AnyStr) -> Cmd[int]:
        return Cmd.from_cmd(lambda: self._inner.write(__s))

    def write_lines(self, lines: PureIter[AnyStr]) -> Cmd[None]:
        return Cmd.from_cmd(lambda: self._inner.writelines(lines))

    def write_stream(self, lines: Stream[AnyStr]) -> Cmd[None]:
        return lines.unsafe_to_iter().map(self._inner.writelines)

    def get_inner(self, _private: _Private) -> IO[AnyStr]:
        "Protected method"
        return self._inner


@dataclass(frozen=True)
class IOAdapter(Generic[AnyStr]):
    @staticmethod
    def unsafe_io(
        file: WritableIO[AnyStr]
        | ReadableIO[AnyStr]
        | FileProperties[AnyStr]
        | SeekableIO[AnyStr],
        action: Callable[[IO[AnyStr]], Cmd[_T]],
    ) -> Cmd[_T]:
        return action(file.get_inner(_Private()))


@dataclass(frozen=True)
class IOFactory(Generic[AnyStr]):
    @staticmethod
    def open_read(
        path: Path,
        encoding: str,
        action: Callable[
            [FileProperties[str], ReadableIO[str], SeekableIO[str]], Cmd[_T]
        ],
    ) -> Cmd[_T]:
        """
        [WARNING] the action will be executed and the file immediately closed.
        If action returns an Iterable that depends on file state, then it
        can fail when consumed due to closed file.
        """

        def _action(unwrapper: CmdUnwrapper) -> _T:
            with open(path, "r", encoding=encoding) as file:
                return unwrapper.act(
                    action(
                        FileProperties(_Private(), file),
                        ReadableIO(_Private(), file),
                        SeekableIO(_Private(), file),
                    )
                )

        return Cmd.new_cmd(_action)

    @staticmethod
    def open_read_for_stream(
        path: Path,
        encoding: str,
        action: Callable[
            [FileProperties[str], ReadableIO[str], SeekableIO[str]],
            Cmd[Stream[_T]],
        ],
    ) -> Stream[_T]:
        def _action(unwrapper: CmdUnwrapper) -> Iterable[_T]:
            with open(path, "r", encoding=encoding) as file:
                items = unwrapper.act(
                    action(
                        FileProperties(_Private(), file),
                        ReadableIO(_Private(), file),
                        SeekableIO(_Private(), file),
                    )
                )
                for i in unwrapper.act(items.unsafe_to_iter()):
                    yield i

        return unsafe_from_cmd(Cmd.new_cmd(_action))

    @staticmethod
    def open_write(
        path: Path,
        encoding: str,
        action: Callable[
            [FileProperties[str], WritableIO[str], SeekableIO[str]], Cmd[_T]
        ],
    ) -> Cmd[_T]:
        def _action(unwrapper: CmdUnwrapper) -> _T:
            with open(path, "w", encoding=encoding) as file:
                return unwrapper.act(
                    action(
                        FileProperties(_Private(), file),
                        WritableIO(_Private(), file),
                        SeekableIO(_Private(), file),
                    )
                )

        return Cmd.new_cmd(_action)

    @staticmethod
    def open_append(
        path: Path,
        encoding: str,
        action: Callable[
            [FileProperties[str], WritableIO[str], SeekableIO[str]], Cmd[_T]
        ],
    ) -> Cmd[_T]:
        def _action(unwrapper: CmdUnwrapper) -> _T:
            with open(path, "a", encoding=encoding) as file:
                return unwrapper.act(
                    action(
                        FileProperties(_Private(), file),
                        WritableIO(_Private(), file),
                        SeekableIO(_Private(), file),
                    )
                )

        return Cmd.new_cmd(_action)

    @staticmethod
    def open_read_write(
        path: Path,
        encoding: str,
        action: Callable[
            [
                FileProperties[str],
                ReadableIO[str],
                WritableIO[str],
                SeekableIO[str],
            ],
            Cmd[_T],
        ],
    ) -> Cmd[_T]:
        def _action(unwrapper: CmdUnwrapper) -> _T:
            with open(path, "r+", encoding=encoding) as file:
                return unwrapper.act(
                    action(
                        FileProperties(_Private(), file),
                        ReadableIO(_Private(), file),
                        WritableIO(_Private(), file),
                        SeekableIO(_Private(), file),
                    )
                )

        return Cmd.new_cmd(_action)

    @staticmethod
    def open_named_temp_write(
        delete: bool,
        encoding: str,
        action: Callable[
            [FileProperties[str], WritableIO[str], SeekableIO[str]], Cmd[_T]
        ],
    ) -> Cmd[_T]:
        def _action(unwrapper: CmdUnwrapper) -> _T:
            with NamedTemporaryFile(
                "w", encoding=encoding, delete=delete
            ) as file:
                return unwrapper.act(
                    action(
                        FileProperties(_Private(), file),
                        WritableIO(_Private(), file),
                        SeekableIO(_Private(), file),
                    )
                )

        return Cmd.new_cmd(_action)
