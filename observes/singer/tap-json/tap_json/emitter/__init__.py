from ._input import (
    InputEmitter,
)
from ._transform import (
    adjust_data,
    to_singer_record,
)
from collections.abc import (
    Callable,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    Maybe,
    PureIter,
    Stream,
)
from fa_purity._core.coproduct import (
    Coproduct,
    CoproductFactory,
)
from fa_purity.json.transform import (
    dumps,
)
from fa_purity.json_2 import (
    JsonValue,
    JsonValueFactory,
    LegacyAdapter,
)
from fa_purity.pure_iter import (
    PureIterFactory,
    PureIterTransform,
)
from fa_purity.stream import (
    StreamTransform,
)
from fa_singer_io.singer import (
    SingerMessage,
    SingerRecord,
    SingerSchema,
    SingerState,
)
from fa_singer_io.singer.deserializer import (
    deserialize,
)
from fa_singer_io.singer.emitter import (
    emit,
    encode,
)
import logging
import sys
from tap_json._core import (
    TableRecordPair,
)
from tap_json._utils.singer import (
    handle_singer,
)
from tap_json._utils.temp_file import (
    TempFile,
)
from tap_json.auto_schema import (
    infer_schemas,
)
from tap_json.auto_schema.config import (
    InferenceConfig,
)
from tap_json.linearize import (
    flat_record,
)
from typing import (
    TypeVar,
)

LOG = logging.getLogger(__name__)
_T = TypeVar("_T")


def _emit_schema(schemas: PureIter[SingerSchema]) -> Cmd[None]:
    return schemas.map(lambda s: emit(sys.stdout, s)).transform(
        PureIterTransform.consume
    )


def _ignore_schemas_and_state(
    data: Stream[SingerMessage],
) -> Stream[SingerRecord]:
    items: Stream[Maybe[SingerRecord]] = data.map(
        lambda m: handle_singer(
            m,
            lambda r: Maybe.from_value(r),
            lambda _: Maybe.empty(),
            lambda _: Maybe.empty(),
        )
    )
    return StreamTransform.filter_maybe(items)


def _flat_records(
    data: Stream[Coproduct[SingerRecord, _T]]
) -> Stream[Coproduct[TableRecordPair, _T]]:
    _factory: CoproductFactory[TableRecordPair, _T] = CoproductFactory()
    return data.map(
        lambda m: m.map(
            lambda r: flat_record(r).map(_factory.inl),
            lambda x: PureIterFactory.from_list([_factory.inr(x)]),
        )
    ).transform(lambda s: StreamTransform.chain(s))


def only_emit_schema(config: InferenceConfig) -> Cmd[None]:
    _input = InputEmitter(True).input_stream
    items: Stream[Maybe[TableRecordPair]] = _flat_records(
        _ignore_schemas_and_state(_input).map(lambda t: Coproduct.inl(t))
    ).map(
        lambda c: c.map(lambda r: Maybe.from_value(r), lambda _: Maybe.empty())
    )
    return infer_schemas(config, StreamTransform.filter_maybe(items)).bind(
        _emit_schema
    )


def _to_record(item: TableRecordPair) -> SingerRecord:
    record = (
        PureIterFactory.from_list(tuple(item.record.items()))
        .map(lambda t: (t[0].raw, JsonValue.from_primitive(t[1])))
        .transform(lambda p: FrozenDict(dict(p)))
    )
    return SingerRecord(item.table, LegacyAdapter.to_legacy_json(record), None)


def _save_input(
    data: Stream[Coproduct[TableRecordPair, SingerMessage]], file: TempFile
) -> Stream[Coproduct[TableRecordPair, SingerMessage]]:
    """
    Returns a new stream that saves its data into a TempFile.

    [WARNING] the TempFile will always be empty or not modified
    if the stream has not been consumed before and/or
    the file has not been flushed.
    """
    return data.map(
        lambda c: c.map(
            lambda s: file.append(
                lambda w: w.write(dumps(encode(_to_record(s))) + "\n")
            ),
            lambda m: file.append(lambda w: w.write(dumps(encode(m)) + "\n")),
        ).map(lambda _: c)
    ).transform(lambda s: StreamTransform.squash(s))


def _load_data(file: TempFile) -> Stream[SingerMessage]:
    return file.read_lines().map(
        lambda r: JsonValueFactory.loads(r)
        .bind(lambda j: deserialize(LegacyAdapter.to_legacy_json(j)))
        .unwrap()
    )


def _ignore_schemas(
    data: Stream[SingerMessage],
) -> Stream[Coproduct[SingerRecord, SingerState]]:
    _factory: CoproductFactory[SingerRecord, SingerState] = CoproductFactory()
    _raw: Stream[Maybe[Coproduct[SingerRecord, SingerState]]] = data.map(
        lambda m: handle_singer(
            m,
            lambda x: Maybe.from_value(_factory.inl(x)),
            lambda _: Maybe.empty(),
            lambda x: Maybe.from_value(_factory.inr(x)),
        )
    )
    return StreamTransform.filter_maybe(_raw)


def no_schema_inference(schemas: PureIter[SingerSchema]) -> Cmd[None]:
    def _record_to_msg(record: SingerRecord) -> SingerMessage:
        return record

    _input = _ignore_schemas(InputEmitter(True).input_stream)
    return _emit_schema(schemas) + adjust_data(
        schemas,
        _flat_records(_input).map(
            lambda c: c.map(
                lambda r: _record_to_msg(to_singer_record(r)),
                lambda s: s,
            )
        ),
    ).map(
        lambda c: c.map(
            lambda r: emit(sys.stdout, r),
            lambda s: emit(sys.stdout, s),
        )
    ).transform(
        StreamTransform.consume
    )


def full_execution(config: InferenceConfig) -> Cmd[None]:
    def _saved_stream(file: TempFile) -> Stream[Maybe[TableRecordPair]]:
        _factory: CoproductFactory[
            TableRecordPair, SingerMessage
        ] = CoproductFactory()
        _input = _ignore_schemas(InputEmitter(True).input_stream)
        return _save_input(
            _flat_records(_input).map(
                lambda c: c.map(_factory.inl, _factory.inr)
            ),
            file,
        ).map(
            lambda c: c.map(
                lambda x: Maybe.from_value(x), lambda _: Maybe.empty()
            )
        )

    return TempFile.new().bind(
        lambda f: Cmd.from_cmd(
            lambda: LOG.debug("temp input file at %s", f.path)
        )
        + infer_schemas(
            config, StreamTransform.filter_maybe(_saved_stream(f))
        ).bind(
            lambda schemas: _emit_schema(schemas)
            + adjust_data(schemas, _load_data(f))
            .map(
                lambda c: c.map(
                    lambda r: emit(sys.stdout, r),
                    lambda s: emit(sys.stdout, s),
                )
            )
            .transform(StreamTransform.consume)
        )
    )
