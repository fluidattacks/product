from decimal import (
    Decimal,
    InvalidOperation,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    Maybe,
    PureIter,
    Result,
    ResultE,
    Stream,
)
from fa_purity.json.transform import (
    dumps,
)
from fa_purity.json_2 import (
    JsonObj,
    JsonPrimitive,
    JsonValue,
    LegacyAdapter,
    Unfolder,
)
from fa_purity.pure_iter import (
    PureIterFactory,
    PureIterTransform,
)
from fa_purity.stream import (
    StreamTransform,
)
from fa_purity.union import (
    Coproduct,
    CoproductFactory,
)
from fa_purity.utils import (
    cast_exception,
    raise_exception,
)
from fa_singer_io.json_schema import (
    JSchemaFactory,
)
from fa_singer_io.singer import (
    SingerMessage,
    SingerRecord,
    SingerSchema,
    SingerState,
)
import logging
from tap_json import (
    cast,
)
from tap_json._core import (
    TableRecordPair,
)
from tap_json._utils.dict import (
    get_key,
)
from tap_json._utils.singer import (
    handle_singer,
)
from tap_json.auto_schema import (
    property_type_to_postfix,
)
from tap_json.auto_schema.jschema.decode import (
    DEFAULT_DECODER,
)
from tap_json.auto_schema.jschema.number import (
    IntSizes,
    IntType,
)
from tap_json.auto_schema.jschema.object import (
    ObjectPropertyType,
    ObjectType,
)
from tap_json.auto_schema.jschema.string import (
    StringFormat,
    StringType,
)

LOG = logging.getLogger(__name__)


def _str_to_decimal(data: str) -> ResultE[Decimal]:
    try:
        return Result.success(Decimal(data))
    except InvalidOperation as err:
        return Result.failure(err, Decimal).alt(cast_exception)


def _to_decimal(data: JsonPrimitive) -> Decimal:
    return data.map(
        lambda s: _str_to_decimal(s).value_or(Decimal(0)),
        lambda i: Decimal(i),
        lambda f: Decimal(str(f)),
        lambda d: d,
        lambda b: Decimal(1) if b else Decimal(0),
        lambda: Decimal(0),
    )


def _str_to_float(data: str) -> ResultE[float]:
    try:
        return Result.success(float(data))
    except ValueError as err:
        return Result.failure(err, float).alt(cast_exception)


def _to_float(data: JsonPrimitive) -> float:
    return data.map(
        lambda s: _str_to_float(s).value_or(0),
        lambda i: i,
        lambda f: f,
        lambda d: float(d),
        lambda b: 1 if b else 0,
        lambda: 0,
    )


def _str_to_int(data: str) -> ResultE[int]:
    try:
        return Result.success(int(data))
    except ValueError as err:
        return Result.failure(err, int).alt(cast_exception)


def _int_max(_type: IntType) -> int:
    match _type.size:
        case IntSizes.SMALL:
            return 32767
        case IntSizes.NORMAL:
            return 2147483647
        case IntSizes.BIG:
            return 9223372036854775807


def _int_min(_type: IntType) -> int:
    match _type.size:
        case IntSizes.SMALL:
            return -32768
        case IntSizes.NORMAL:
            return -2147483648
        case IntSizes.BIG:
            return -9223372036854775808


def _safe_round(_type: IntType, data: float | Decimal) -> int:
    if data == float("inf") or data == Decimal("inf"):
        return _int_max(_type)
    if data == float("-inf") or data == Decimal("-inf"):
        return _int_min(_type)
    return int(data)


def _to_int(main: IntType, data: JsonPrimitive) -> int:
    return data.map(
        lambda s: _str_to_int(s).value_or(0),
        lambda i: i,
        lambda f: _safe_round(main, f),
        lambda d: _safe_round(main, d),
        lambda b: 1 if b else 0,
        lambda: 0,
    )


def _adjust_to_str_type(
    _type: StringType, data: JsonPrimitive
) -> JsonPrimitive:
    _is_datetime = _type.format.map(
        lambda f: f is StringFormat.DATE_TIME
    ).value_or(False)
    empty = JsonPrimitive.from_str("")
    if _is_datetime:
        return (
            cast.primitive_to_date(data)
            .map(
                lambda d: JsonPrimitive.from_str(
                    d.strftime("%Y-%m-%dT%H:%M:%SZ")
                )
            )
            .value_or(empty)
        )
    return data.map(
        lambda x: JsonPrimitive.from_str(str(x)),
        lambda x: JsonPrimitive.from_str(str(x)),
        lambda x: JsonPrimitive.from_str(str(x)),
        lambda x: JsonPrimitive.from_str(str(x)),
        lambda x: JsonPrimitive.from_str(str(x)),
        lambda: empty,
    )


def _adjust_to_type(
    _type: ObjectPropertyType, data: JsonPrimitive
) -> JsonPrimitive:
    return _type.map(
        lambda n: n.map(
            lambda _: JsonPrimitive.from_decimal(_to_decimal(data)),
            lambda i: JsonPrimitive.from_int(_to_int(i, data)),
            lambda _: JsonPrimitive.from_float(_to_float(data)),
        ),
        lambda s: _adjust_to_str_type(s, data),
        data.map(
            lambda x: JsonPrimitive.from_bool(bool(x)),
            lambda x: JsonPrimitive.from_bool(bool(x)),
            lambda x: JsonPrimitive.from_bool(bool(x)),
            lambda x: JsonPrimitive.from_bool(bool(x)),
            lambda x: JsonPrimitive.from_bool(bool(x)),
            lambda: JsonPrimitive.from_bool(False),
        ),
        JsonPrimitive.empty(),
    )


def _apply_postfix(obj_type: ObjectPropertyType, key: str) -> str:
    return key + "_" + property_type_to_postfix(obj_type)


def _adjust_data(_type: ObjectType, record: JsonObj) -> JsonObj:
    item = (
        PureIterFactory.from_list(tuple(record.items()))
        .map(
            lambda t: get_key(_type.properties, t[0]).map(
                lambda obj_type: (
                    _apply_postfix(obj_type, t[0]),
                    JsonValue.from_primitive(
                        _adjust_to_type(
                            obj_type,
                            Unfolder.to_primitive(t[1]).unwrap(),
                        )
                    ),
                )
            )
        )
        .transform(lambda p: PureIterTransform.filter_maybe(p))
        .transform(lambda p: FrozenDict(dict(p)))
    )
    return item


def _adjust_record(_type: ObjectType, record: SingerRecord) -> SingerRecord:
    result = SingerRecord(
        record.stream,
        LegacyAdapter.to_legacy_json(
            _adjust_data(_type, LegacyAdapter.json(record.record))
        ),
        None,
    )
    LOG.debug(
        "Input: stream=%s\ndata=%s\nOutput: %s\n\n",
        record.stream,
        dumps(record.record),
        dumps(result.record),
    )
    return result


def _to_type_map(
    schemas: PureIter[SingerSchema],
) -> FrozenDict[str, ObjectType]:
    return schemas.map(
        lambda s: DEFAULT_DECODER.decode_obj_type(s)
        .map(lambda o: (s.stream, o))
        .alt(
            lambda e: ValueError(
                f"Error decoding {s.stream} {dumps(s.schema.encode())} i.e. {e}"
            )
        )
        .alt(raise_exception)
        .unwrap()
    ).transform(lambda i: FrozenDict(dict(i)))


def _get_schema_and_adjust(
    schemas: FrozenDict[str, ObjectType], record: SingerRecord
) -> Cmd[Maybe[SingerRecord]]:
    _warn: Cmd[Maybe[SingerRecord]] = Cmd.from_cmd(
        lambda: LOG.warning("Schema %s not found", record.stream)
    ).map(lambda _: Maybe.empty())
    return (
        get_key(schemas, record.stream)
        .map(lambda o: _adjust_record(o, record))
        .map(lambda r: Cmd.from_cmd(lambda: Maybe.from_value(r)))
        .value_or(_warn)
    )


def _ignore_schemas(schema: SingerSchema) -> Cmd[None]:
    return Cmd.from_cmd(
        lambda: LOG.warning(
            "Input singer schema for stream %s is ignored", schema.stream
        )
    )


def to_singer_record(pair: TableRecordPair) -> SingerRecord:
    item = (
        PureIterFactory.from_list(tuple(pair.record.items()))
        .map(lambda t: (t[0].raw, JsonValue.from_primitive(t[1])))
        .transform(lambda d: FrozenDict(dict(d)))
    )
    return SingerRecord(pair.table, LegacyAdapter.to_legacy_json(item), None)


def _remove_postfix(key: str) -> str:
    if key.endswith("_str"):
        return key.removesuffix("_str")
    if key.endswith("_float"):
        return key.removesuffix("_float")
    if key.endswith("_int"):
        return key.removesuffix("_int")
    if key.endswith("_datetime"):
        return key.removesuffix("_datetime")
    if key.endswith("_bool"):
        return key.removesuffix("_bool")
    if key.endswith("_null"):
        return key.removesuffix("_null")
    return key


def _schema_remove_postfix(schema: SingerSchema) -> SingerSchema:
    properties = Unfolder.to_json(
        LegacyAdapter.json_value(schema.schema.encode()["properties"])
    ).unwrap()
    adjusted_properties = (
        PureIterFactory.from_list(tuple(properties.items()))
        .map(lambda t: (_remove_postfix(t[0]), t[1]))
        .transform(lambda d: FrozenDict(dict(d)))
    )
    new_schema = FrozenDict(
        {"properties": JsonValue.from_json(adjusted_properties)}
    )
    return SingerSchema.new(
        schema.stream,
        JSchemaFactory.from_json(
            LegacyAdapter.to_legacy_json(new_schema)
        ).unwrap(),
        schema.key_properties,
        schema.bookmark_properties,
    ).unwrap()


def adjust_data(
    schemas: PureIter[SingerSchema], data: Stream[SingerMessage]
) -> Stream[Coproduct[SingerRecord, SingerState]]:
    types_map = _to_type_map(schemas.map(_schema_remove_postfix))
    _factory: CoproductFactory[SingerRecord, SingerState] = CoproductFactory()
    items: Stream[Cmd[Maybe[Coproduct[SingerRecord, SingerState]]]] = data.map(
        lambda m: handle_singer(
            m,
            lambda r: _get_schema_and_adjust(types_map, r).map(
                lambda m: m.map(_factory.inl)
            ),
            lambda s: _ignore_schemas(s).map(lambda _: Maybe.empty()),
            lambda s: Cmd.from_cmd(lambda: Maybe.from_value(_factory.inr(s))),
        )
    )
    return StreamTransform.squash(items).transform(
        lambda s: StreamTransform.filter_maybe(s)
    )
