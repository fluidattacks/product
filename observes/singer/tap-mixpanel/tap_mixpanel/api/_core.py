from __future__ import (
    annotations,
)

import hashlib
from dataclasses import (
    dataclass,
    field,
)
from datetime import (
    date,
)

from etl_utils.typing import (
    Callable,
    FrozenSet,
)
from fa_purity import (
    FrozenDict,
    ResultE,
    Stream,
)
from fa_purity.json import (
    JsonObj,
    JsonPrimitive,
    JsonPrimitiveUnfolder,
    JsonUnfolder,
    JsonValue,
    Unfolder,
)


def _hash(raw: str) -> str:
    return hashlib.sha256(raw.encode("utf-8")).hexdigest()[:128]


@dataclass(frozen=True)
class Credentials:
    user: str
    secret: str

    def __repr__(self) -> str:
        return f"[masked] user_signature={_hash(self.user)} secret_signature={_hash(self.secret)}"


@dataclass(frozen=True)
class ProjectId:
    raw: str


@dataclass(frozen=True)
class _Private:
    pass


@dataclass(frozen=True)
class Event:
    _private: _Private = field(repr=False, hash=False, compare=False)
    raw: JsonObj

    @staticmethod
    def flatten_properties(raw: JsonObj) -> ResultE[Event]:
        event = JsonUnfolder.require(
            raw,
            "event",
            lambda v: Unfolder.to_primitive(v).bind(JsonPrimitiveUnfolder.to_str),
        )
        return event.bind(
            lambda e: JsonUnfolder.require(raw, "properties", Unfolder.to_json).map(
                lambda j: FrozenDict(
                    dict(j) | {"event": JsonValue.from_primitive(JsonPrimitive.from_str(e))},
                ),
            ),
        ).map(lambda j: Event(_Private(), j))


@dataclass(frozen=True)
class EventName:
    raw: str


@dataclass(frozen=True)
class TargetDate:
    target_date: date


@dataclass(frozen=True)
class ApiClient:
    export: Callable[[FrozenSet[EventName], TargetDate], Stream[Event]]
