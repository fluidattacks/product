import inspect
import json
import logging

from etl_utils.typing import (
    FrozenSet,
    Iterable,
)
from fa_purity import (
    Cmd,
    FrozenList,
    Stream,
    Unsafe,
)
from fa_purity.json import (
    JsonUnfolder,
    UnfoldedFactory,
)
from pure_requests.basic import (
    Authentication,
    AuthMethod,
    Endpoint,
    HttpClientFactory,
    Params,
)
from requests import (
    Response,
)

from tap_mixpanel._bug import (
    Bug,
)

from ._core import (
    Credentials,
    Event,
    EventName,
    ProjectId,
    TargetDate,
)

LOG = logging.getLogger(__name__)
EXPORT_API = "https://data.mixpanel.com/api/2.0"


def _ensure_str(response: Response) -> Iterable[str]:
    return (str(i) for i in response.iter_lines(decode_unicode=True))


def _to_lines(response: Response) -> Iterable[Event]:
    return (
        Bug.assume_success(
            "export_json_decode",
            inspect.currentframe(),
            (i,),
            UnfoldedFactory.loads(i).map(
                lambda j: Bug.assume_success(
                    "decode_event",
                    inspect.currentframe(),
                    (JsonUnfolder.dumps(j),),
                    Event.flatten_properties(j),
                ),
            ),
        )
        for i in _ensure_str(response)
    )


def _encode_list(items: FrozenList[str]) -> str:
    return json.dumps(tuple(items))  # type: ignore[misc]


def export(
    creds: Credentials,
    project: ProjectId,
    events: FrozenSet[EventName],
    target_date: TargetDate,
) -> Stream[Event]:
    client = HttpClientFactory.new_client(
        Authentication(creds.user, creds.secret, AuthMethod.BASIC),
        None,
        True,
    )
    # If stream is not enabled it will probably fail due to a memory error.
    endpoint = Endpoint(EXPORT_API + "/export")
    params = UnfoldedFactory.from_dict(
        {
            "project_id": project.raw,
            "from_date": target_date.target_date.strftime("%Y-%m-%d"),
            "to_date": target_date.target_date.strftime("%Y-%m-%d"),
            "event": _encode_list(tuple(e.raw for e in events)),
        },
    )
    msg = Cmd.wrap_impure(lambda: LOG.info("[API] GET %s params=%s", endpoint.raw, params))
    _get_export = (
        client.get(endpoint, Params(params))
        .map(
            lambda r: Bug.assume_success(
                "ExportApi",
                inspect.currentframe(),
                (endpoint.raw, str(params)),
                r,
            ),
        )
        .map(_to_lines)
    )
    return Unsafe.stream_from_cmd(msg + _get_export)
