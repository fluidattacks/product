{ inputs, ... }@makes_inputs:
let
  python_version = "python311";
  nixpkgs = inputs.nixpkgs-observes // { inherit (inputs) nix-filter; };
  out = import ./build {
    inherit makes_inputs nixpkgs python_version;
    src = inputs.nix-filter {
      root = ./.;
      include =
        [ "tap_dynamo" "tests" "pyproject.toml" "mypy.ini" ".pylintrc" ];
    };
  };
in out
