{ inputs, projectPath, ... }@makes_inputs:
import (projectPath "/observes/common/check/types") {
  inherit makes_inputs;
  inherit (inputs) nixpkgs;
  pkg_index_item = inputs.observesIndex.tap.dynamo;
}
