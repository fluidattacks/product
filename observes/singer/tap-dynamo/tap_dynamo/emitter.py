from .dynamo_items import (
    DynamoValue,
    DynamoValueTransform,
    DynamoValueUnfolder,
    ScalarUnfolder,
)
from .streams import (
    extract_segment,
    PageData,
    TableSegment,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    FrozenList,
    Maybe,
    PureIter,
    Stream,
)
from fa_purity.cmd import (
    CmdUnwrapper,
)
from fa_purity.json_2 import (
    LegacyAdapter,
)
from fa_purity.pure_iter import (
    PureIterFactory,
    PureIterTransform,
)
from fa_purity.pure_iter.factory import (
    from_flist,
)
from fa_purity.pure_iter.transform import (
    consume as piter_consume,
)
from fa_purity.stream.transform import (
    consume,
)
from fa_purity.utils import (
    raise_exception,
)
from fa_singer_io.singer import (
    emitter,
    SingerRecord,
)
import sys
from tap_dynamo import (
    _utils,
)
from tap_dynamo.client import (
    Client,
)
from threading import (
    Lock,
)


def _facet_filter(
    item: FrozenDict[str, DynamoValue]
) -> Maybe[FrozenDict[str, DynamoValue]]:
    pk = (
        DynamoValueUnfolder.to_scalar(item["pk"])
        .bind(ScalarUnfolder.to_str)
        .alt(raise_exception)
        .to_union()
    )
    sk = (
        DynamoValueUnfolder.to_scalar(item["sk"])
        .bind(ScalarUnfolder.to_str)
        .alt(raise_exception)
        .to_union()
    )
    if pk.startswith("LINES#GROUP#") and sk.startswith("STATE#"):
        return Maybe.empty()
    return Maybe.from_value(item)


def to_singer(page: PageData) -> PureIter[SingerRecord]:
    _items = (
        PureIterFactory.from_list(page.items)
        .map(_facet_filter)
        .transform(lambda i: PureIterTransform.filter_maybe(i))
    )
    return _items.map(
        lambda d: FrozenDict(
            {
                k: LegacyAdapter.to_legacy_value(
                    DynamoValueTransform.to_json_value(v)
                )
                for k, v in d.items()
            }
        )
    ).map(
        lambda j: SingerRecord(
            page.t_segment.table_name,
            j,
            None,
        )
    )


def _emit_page(page: PageData) -> Cmd[None]:
    return (
        to_singer(page)
        .map(lambda i: emitter.emit(sys.stdout, i))
        .transform(piter_consume)
    )


def _emit_pages(lock: Lock, pages: Stream[PageData]) -> Cmd[None]:
    def _action(unwrapper: CmdUnwrapper) -> None:
        commands = unwrapper.act(pages.map(_emit_page).unsafe_to_iter())
        for cmd in commands:
            lock.acquire()
            try:
                unwrapper.act(cmd)
            finally:
                lock.release()

    return Cmd.new_cmd(_action)


def _process_streams_chunk(streams: FrozenList[Stream[PageData]]) -> Cmd[None]:
    lock = Lock()
    return _utils.threads_map(
        from_flist(streams).map(lambda p: _emit_pages(lock, p)).to_list()
    ).map(lambda _: None)


def stream_table(
    db_client: Client, table_name: str, segments: int, max_concurrency: int
) -> Cmd[None]:
    items = (
        PureIterFactory.from_range(range(segments)).map(
            lambda i: extract_segment(
                db_client, TableSegment(table_name, i, segments)
            )
        )
    ).chunked(max_concurrency)
    return piter_consume(items.map(_process_streams_chunk))


def stream_tables(
    client: Client,
    tables: FrozenList[str],
    segmentation: int,
    max_concurrency: int,
) -> Cmd[None]:
    pages = from_flist(tables).map(
        lambda t: stream_table(client, t, segmentation, max_concurrency)
    )
    return piter_consume(pages)


def stream_segment(db_client: Client, segment: TableSegment) -> Cmd[None]:
    return (
        extract_segment(db_client, segment)
        .map(to_singer)
        .map(lambda s: s.map(lambda r: emitter.emit(sys.stdout, r)))
        .map(piter_consume)
        .transform(lambda x: consume(x))
    )
