from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
    field,
)
from fa_purity import (
    FrozenDict,
    FrozenList,
    Maybe,
    Result,
    ResultE,
    Stream,
)
from fa_purity.cmd import (
    Cmd,
    unsafe_unwrap,
)
from fa_purity.pure_iter import (
    PureIterFactory,
)
from fa_purity.result import (
    ResultFactory,
)
from fa_purity.result.transform import (
    all_ok,
)
from fa_purity.stream.factory import (
    unsafe_from_cmd,
)
from fa_purity.utils import (
    raise_exception,
)
from tap_dynamo.client import (
    Client,
    ScanArgs,
)
from tap_dynamo.dynamo_items import (
    DynamoValue,
    DynamoValueFactory,
    DynamoValueUnfolder,
    ScalarUnfolder,
)
from typing import (
    Any,
    Callable,
    Iterable,
    TypeVar,
)

_K = TypeVar("_K")
_V = TypeVar("_V")
_P = TypeVar("_P")
_S = TypeVar("_S")
_F = TypeVar("_F")


@dataclass(frozen=True)
class _Private:
    pass


@dataclass(frozen=True)
class TableSegment:
    table_name: str
    segment: int
    total_segments: int


@dataclass(frozen=True)
class ItemKey:
    _private: _Private = field(repr=False, hash=False, compare=False)
    key: FrozenDict[str, str]

    @staticmethod
    def from_dynamo_dict(
        item: FrozenDict[str, DynamoValue]
    ) -> ResultE[ItemKey]:
        return (
            PureIterFactory.from_list(tuple(item.items()))
            .map(
                lambda t: DynamoValueUnfolder.to_scalar(t[1])
                .bind(ScalarUnfolder.to_str)
                .map(lambda v: (t[0], v))
            )
            .transform(lambda p: all_ok(p.to_list()))
            .map(lambda i: FrozenDict(dict(i)))
            .map(lambda d: ItemKey(_Private(), d))
        )


@dataclass(frozen=True)
class PageData:
    t_segment: TableSegment
    items: FrozenList[FrozenDict[str, DynamoValue]]
    exclusive_start_key: Maybe[ItemKey]


@dataclass(frozen=True)  # type: ignore[misc]
class _ScanResponse:  # type: ignore[misc]
    t_segment: TableSegment
    response: FrozenDict[str, Any]  # type: ignore[misc]


def _paginate_table(
    client: Client,
    table_segment: TableSegment,
    ex_start_key: Maybe[ItemKey],
) -> Cmd[_ScanResponse]:
    table = client.table(table_segment.table_name)
    scan_args = ScanArgs(
        1000,
        False,
        table_segment.segment,
        table_segment.total_segments,
        ex_start_key.map(lambda k: k.key).value_or(None),
    )
    result = table.scan(scan_args)  # type: ignore[misc]
    return result.map(  # type: ignore[misc]
        lambda r: _ScanResponse(t_segment=table_segment, response=r)  # type: ignore[misc]
    )


def _require_key(items: FrozenDict[_K, _V], key: _K) -> ResultE[_V]:
    _factory: ResultFactory[_V, Exception] = ResultFactory()
    if key in items:
        return _factory.success(items[key])
    return _factory.failure(KeyError(f"Key `{str(key)}` not found"))


def _switch(items: Maybe[Result[_S, _F]]) -> Result[Maybe[_S], _F]:
    _factory: ResultFactory[Maybe[_S], _F] = ResultFactory()
    return items.map(lambda r: r.map(lambda v: Maybe.from_value(v))).value_or(
        _factory.success(Maybe.empty())
    )


def _response_to_page(
    scan_response: _ScanResponse,
) -> ResultE[Maybe[PageData]]:
    response = FrozenDict(dict(scan_response.response))  # type: ignore[misc]
    if response.get("Count") == 0:  # type: ignore[misc]
        return Result.success(Maybe.empty())

    last_key = Maybe.from_optional(
        _require_key(response, "LastEvaluatedKey").value_or(None)  # type: ignore[misc]
    ).map(
        lambda v: DynamoValueFactory.from_any(v)
        .bind(DynamoValueUnfolder.to_dict)
        .bind(ItemKey.from_dynamo_dict)
    )
    items = (
        _require_key(response, "Items")  # type: ignore[misc]
        .bind(DynamoValueFactory.from_any)
        .bind(DynamoValueUnfolder.to_list)
    ).bind(
        lambda v: all_ok(
            PureIterFactory.from_list(v)
            .map(DynamoValueUnfolder.to_dict)
            .to_list()
        )
    )
    return items.bind(
        lambda i: _switch(last_key).map(
            lambda key: PageData(
                scan_response.t_segment,
                i,
                key,
            )
        )
    ).map(lambda x: Maybe.from_value(x))


def extract_until_end(
    getter: Callable[[Maybe[_K]], Cmd[Maybe[_P]]],
    extract_next: Callable[[_P], Maybe[_K]],
) -> Stream[_P]:
    def _inner() -> Iterable[_P]:
        last_key: Maybe[_K] = Maybe.empty()
        end_reached: bool = False
        while not end_reached:
            result: Maybe[_P] = unsafe_unwrap(getter(last_key))
            if result.map(lambda _: True).value_or(False) is False:
                end_reached = True
                continue
            item = (
                result.to_result()
                .alt(lambda _: Exception("Impossible!"))
                .alt(raise_exception)
                .to_union()
            )
            last_key = extract_next(item)
            if last_key.map(lambda _: True).value_or(False) is False:
                end_reached = True
            yield item

    return unsafe_from_cmd(Cmd.from_cmd(_inner))


def extract_segment(
    db_client: Client, segment: TableSegment
) -> Stream[PageData]:
    def getter(last_key: Maybe[ItemKey]) -> Cmd[Maybe[PageData]]:
        response = _paginate_table(db_client, segment, last_key)
        return response.map(_response_to_page).map(
            lambda r: r.alt(raise_exception).to_union()
        )

    return extract_until_end(getter, lambda p: p.exclusive_start_key)
