from ._scalar import (
    Scalar,
)
from ._scalar_set import (
    ScalarSet,
)
from ._value import (
    DynamoValue,
)
import base64
from boto3.dynamodb.types import (
    Binary,
)
from dataclasses import (
    dataclass,
)
from fa_purity import (
    FrozenList,
    ResultE,
)
from fa_purity.frozen import (
    FrozenDict,
)
from fa_purity.json_2 import (
    JsonPrimitive,
    JsonValue,
)
from fa_purity.pure_iter import (
    PureIterFactory,
)
from fa_purity.result import (
    ResultFactory,
)


@dataclass(frozen=True)
class DynamoValueUnfolder:
    @staticmethod
    def to_dict(item: DynamoValue) -> ResultE[FrozenDict[str, DynamoValue]]:
        _factory: ResultFactory[
            FrozenDict[str, DynamoValue], Exception
        ] = ResultFactory()
        return item.map(
            lambda _: _factory.failure(
                ValueError("Expected dynamo dict but got a scalar")
            ),
            lambda _: _factory.failure(
                ValueError("Expected dynamo dict but got a scalar set")
            ),
            lambda _: _factory.failure(
                ValueError("Expected dynamo dict but got a list")
            ),
            _factory.success,
        )

    @staticmethod
    def to_list(item: DynamoValue) -> ResultE[FrozenList[DynamoValue]]:
        _factory: ResultFactory[
            FrozenList[DynamoValue], Exception
        ] = ResultFactory()
        return item.map(
            lambda _: _factory.failure(
                ValueError("Expected dynamo dict but got a scalar")
            ),
            lambda _: _factory.failure(
                ValueError("Expected dynamo dict but got a scalar set")
            ),
            _factory.success,
            lambda _: _factory.failure(
                ValueError("Expected dynamo dict but got a dict")
            ),
        )

    @staticmethod
    def to_set(item: DynamoValue) -> ResultE[ScalarSet]:
        _factory: ResultFactory[ScalarSet, Exception] = ResultFactory()
        return item.map(
            lambda _: _factory.failure(
                ValueError("Expected dynamo dict but got a scalar")
            ),
            _factory.success,
            lambda _: _factory.failure(
                ValueError("Expected dynamo dict but got a scalar list")
            ),
            lambda _: _factory.failure(
                ValueError("Expected dynamo dict but got a dict")
            ),
        )

    @staticmethod
    def to_scalar(item: DynamoValue) -> ResultE[Scalar]:
        _factory: ResultFactory[Scalar, Exception] = ResultFactory()
        return item.map(
            _factory.success,
            lambda _: _factory.failure(
                ValueError("Expected dynamo dict but got a set")
            ),
            lambda _: _factory.failure(
                ValueError("Expected dynamo dict but got a scalar list")
            ),
            lambda _: _factory.failure(
                ValueError("Expected dynamo dict but got a dict")
            ),
        )


@dataclass(frozen=True)
class DynamoValueTransform:
    @staticmethod
    def _encode_binary(binary: Binary) -> str:
        return base64.b64encode(
            binary.value  # type: ignore[attr-defined, misc]
        ).decode("utf-8")

    @classmethod
    def to_json_value(cls, item: DynamoValue) -> JsonValue:
        return item.map(
            lambda s: JsonValue.from_primitive(
                s.map(
                    JsonPrimitive.from_str,
                    JsonPrimitive.from_int,
                    JsonPrimitive.from_decimal,
                    lambda b: JsonPrimitive.from_str(cls._encode_binary(b)),
                    JsonPrimitive.from_bool,
                    lambda: JsonPrimitive.empty(),
                )
            ),
            lambda ss: ss.map(
                lambda i: JsonValue.from_list(
                    PureIterFactory.from_list(tuple(i))
                    .map(
                        lambda v: JsonValue.from_primitive(
                            JsonPrimitive.from_str(v)
                        )
                    )
                    .to_list()
                ),
                lambda i: JsonValue.from_list(
                    PureIterFactory.from_list(tuple(i))
                    .map(
                        lambda v: JsonValue.from_primitive(
                            JsonPrimitive.from_int(v)
                        )
                    )
                    .to_list()
                ),
                lambda i: JsonValue.from_list(
                    PureIterFactory.from_list(tuple(i))
                    .map(
                        lambda v: JsonValue.from_primitive(
                            JsonPrimitive.from_decimal(v)
                        )
                    )
                    .to_list()
                ),
                lambda i: JsonValue.from_list(
                    PureIterFactory.from_list(tuple(i))
                    .map(
                        lambda v: JsonValue.from_primitive(
                            JsonPrimitive.from_str(cls._encode_binary(v))
                        )
                    )
                    .to_list()
                ),
            ),
            lambda i: JsonValue.from_list(
                PureIterFactory.from_list(i).map(cls.to_json_value).to_list()
            ),
            lambda d: JsonValue.from_json(
                FrozenDict({k: cls.to_json_value(v) for k, v in d.items()})
            ),
        )


@dataclass(frozen=True)
class ScalarUnfolder:
    @staticmethod
    def to_str(item: Scalar) -> ResultE[str]:
        _factory: ResultFactory[str, Exception] = ResultFactory()
        return item.map(
            lambda x: _factory.success(x),
            lambda _: _factory.failure(TypeError("`int` not expected!")),
            lambda _: _factory.failure(TypeError("`Decimal` not expected!")),
            lambda _: _factory.failure(TypeError("`Binary` not expected!")),
            lambda _: _factory.failure(TypeError("`bool` not expected!")),
            lambda: _factory.failure(TypeError("`None` not expected!")),
        )
