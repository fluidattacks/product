from __future__ import (
    annotations,
)

from ._scalar import (
    Scalar,
)
from ._scalar_set import (
    ScalarSet,
)
from dataclasses import (
    dataclass,
    field,
)
from fa_purity import (
    FrozenDict,
    FrozenList,
)
from fa_purity._core.coproduct import (
    Coproduct,
)
from fa_purity.utils import (
    raise_exception,
)
from typing import (
    Callable,
    TypeVar,
)

_T = TypeVar("_T")


@dataclass(frozen=True)
class _Private:
    pass


@dataclass(frozen=True)
class DynamoValue:
    _private: _Private = field(repr=False, hash=False, compare=False)
    _inner_scalar: Coproduct[Scalar, None]
    _inner_set: Coproduct[ScalarSet, None]
    _inner_list: Coproduct[FrozenList[DynamoValue], None]
    _inner_dict: Coproduct[FrozenDict[str, DynamoValue], None]

    @staticmethod
    def from_scalar(raw: Scalar) -> DynamoValue:
        return DynamoValue(
            _Private(),
            Coproduct.inl(raw),
            Coproduct.inr(None),
            Coproduct.inr(None),
            Coproduct.inr(None),
        )

    @staticmethod
    def from_set(raw: ScalarSet) -> DynamoValue:
        return DynamoValue(
            _Private(),
            Coproduct.inr(None),
            Coproduct.inl(raw),
            Coproduct.inr(None),
            Coproduct.inr(None),
        )

    @staticmethod
    def from_list(raw: FrozenList[DynamoValue]) -> DynamoValue:
        return DynamoValue(
            _Private(),
            Coproduct.inr(None),
            Coproduct.inr(None),
            Coproduct.inl(raw),
            Coproduct.inr(None),
        )

    @staticmethod
    def from_dict(raw: FrozenDict[str, DynamoValue]) -> DynamoValue:
        return DynamoValue(
            _Private(),
            Coproduct.inr(None),
            Coproduct.inr(None),
            Coproduct.inr(None),
            Coproduct.inl(raw),
        )

    def map(
        self,
        scalar_case: Callable[[Scalar], _T],
        set_case: Callable[[ScalarSet], _T],
        list_case: Callable[[FrozenList[DynamoValue]], _T],
        dict_case: Callable[[FrozenDict[str, DynamoValue]], _T],
    ) -> _T:
        return self._inner_scalar.map(
            scalar_case,
            lambda _: self._inner_set.map(
                set_case,
                lambda _: self._inner_list.map(
                    list_case,
                    lambda _: self._inner_dict.map(
                        dict_case,
                        lambda _: raise_exception(ValueError("Impossible!")),
                    ),
                ),
            ),
        )
