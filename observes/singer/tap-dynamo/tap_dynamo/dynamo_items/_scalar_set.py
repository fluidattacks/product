from __future__ import (
    annotations,
)

from boto3.dynamodb.types import (
    Binary,
)
from dataclasses import (
    dataclass,
    field,
)
from decimal import (
    Decimal,
)
from fa_purity._core.coproduct import (
    Coproduct,
)
from fa_purity.utils import (
    raise_exception,
)
from typing import (
    Callable,
    FrozenSet,
    TypeVar,
)

_T = TypeVar("_T")


@dataclass(frozen=True)
class _Private:
    pass


@dataclass(frozen=True)
class ScalarSet:
    _private: _Private = field(repr=False, hash=False, compare=False)
    _inner_str: Coproduct[FrozenSet[str], None]
    _inner_int: Coproduct[FrozenSet[int], None]
    _inner_decimal: Coproduct[FrozenSet[Decimal], None]
    _inner_binary: Coproduct[FrozenSet[Binary], None]

    @staticmethod
    def from_str(raw: FrozenSet[str]) -> ScalarSet:
        return ScalarSet(
            _Private(),
            Coproduct.inl(raw),
            Coproduct.inr(None),
            Coproduct.inr(None),
            Coproduct.inr(None),
        )

    @staticmethod
    def from_int(raw: FrozenSet[int]) -> ScalarSet:
        return ScalarSet(
            _Private(),
            Coproduct.inr(None),
            Coproduct.inl(raw),
            Coproduct.inr(None),
            Coproduct.inr(None),
        )

    @staticmethod
    def from_decimal(raw: FrozenSet[Decimal]) -> ScalarSet:
        return ScalarSet(
            _Private(),
            Coproduct.inr(None),
            Coproduct.inr(None),
            Coproduct.inl(raw),
            Coproduct.inr(None),
        )

    @staticmethod
    def from_binary(raw: FrozenSet[Binary]) -> ScalarSet:
        return ScalarSet(
            _Private(),
            Coproduct.inr(None),
            Coproduct.inr(None),
            Coproduct.inr(None),
            Coproduct.inl(raw),
        )

    def map(
        self,
        str_case: Callable[[FrozenSet[str]], _T],
        int_case: Callable[[FrozenSet[int]], _T],
        decimal_case: Callable[[FrozenSet[Decimal]], _T],
        binary_case: Callable[[FrozenSet[Binary]], _T],
    ) -> _T:
        return self._inner_str.map(
            str_case,
            lambda _: self._inner_int.map(
                int_case,
                lambda _: self._inner_decimal.map(
                    decimal_case,
                    lambda _: self._inner_binary.map(
                        binary_case,
                        lambda _: raise_exception(ValueError("Impossible!")),
                    ),
                ),
            ),
        )
