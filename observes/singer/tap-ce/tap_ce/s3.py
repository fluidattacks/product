from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
)
from json import (
    loads,
)

import boto3
from fa_purity import (
    FrozenDict,
)
from mypy_boto3_s3.client import (
    S3Client,
)


@dataclass(frozen=True)
class S3URI:
    bucket: str
    obj_key: str

    @staticmethod
    def new(uri: str) -> S3URI:
        s3_prefix = "s3://"
        if uri.startswith(s3_prefix):
            required_elements = 2
            uri_elements = uri.removeprefix(s3_prefix).split("/", 1)
            if len(uri_elements) != required_elements:
                error_msg = f"invalid s3 URI i.e. {uri}"
                raise ValueError(error_msg)
            return S3URI(uri_elements[0], uri_elements[1])
        error_msg = f"invalid s3 URI i.e. {uri}"
        raise ValueError(error_msg)


@dataclass(frozen=True)
class S3Extractor:
    uri: S3URI
    client: S3Client

    @staticmethod
    def new(uri: str) -> S3Extractor:
        s3_uri = S3URI.new(uri)
        s3_client = boto3.client("s3", region_name="us-east-1")
        return S3Extractor(s3_uri, s3_client)

    def obj_exist(self) -> bool:
        try:
            self.client.get_object(Bucket=self.uri.bucket, Key=self.uri.obj_key)
        except (
            self.client.exceptions.NoSuchBucket,
            self.client.exceptions.NoSuchKey,
        ):
            return False
        return True

    def obj_is_empty(self) -> bool:
        try:
            s3_response = self.client.head_object(Bucket=self.uri.bucket, Key=self.uri.obj_key)
            bytes_size = s3_response["ContentLength"]
        except Exception as e:
            error_msg = "Error validating file in S3"
            raise ValueError(error_msg) from e
        return not bytes_size > 0

    def get_state_content(self) -> FrozenDict[str, str]:
        try:
            s3_response = self.client.get_object(Bucket=self.uri.bucket, Key=self.uri.obj_key)
            parsed_data: FrozenDict[str, str] = loads(s3_response["Body"].read().decode("utf-8"))
        except Exception as e:
            error_msg = "Error parsing state content from S3"
            raise ValueError(error_msg) from e
        return parsed_data
