{ lib, python_pkgs, }:
lib.buildPythonPackage rec {
  pname = "mypy-boto3-cloudwatch";
  version = "1.34.0";
  src = lib.fetchPypi {
    inherit pname version;
    sha256 = "zBiqKxqJ60iYpsIT21lKv2L+mvym6Ma9mqBLmZYZNjU=";
  };
  nativeBuildInputs = with python_pkgs; [ boto3 ];
  propagatedBuildInputs = with python_pkgs; [ botocore typing-extensions ];
}
