{ inputs, outputs, ... }:
let
  products = with inputs.observesIndex; {
    observesIntegratesDal = common.integrates_dal;
    observesConnectionManager = common.connection_manager;
    observesLambdaBaseRuntime = lambda.base_runtime;
    observesCodeEtl = etl.code;
    observesEtlTimedoctor = etl.timedoctor;
    observesLambdaIntegratesHistoric = lambda.integrates_historic;
    observesServiceDbMigration = service.db_migration;
    observesServiceDbSnapshot = service.db_snapshot;
    observesTapBatch = tap.batch;
    observesTapCsv = tap.csv;
    observesTapCheckly = tap.checkly;
    observesTapCloudWatch = tap.cloudwatch;
    observesTapDelighted = tap.delighted;
    observesTapDynamo = tap.dynamo;
    observesTapFlow = tap.flow;
    observesTapGitlab = tap.gitlab;
    observesTapGitlabDora = tap.gitlab_dora;
    observesTapGoogleSheets = tap.google_sheets;
    observesTapJson = tap.json;
    observesTapMixpanel = tap.mixpanel;
    observesTapTimedoctor = tap.timedoctor;
    observesTapZohoCrm = tap.zoho_crm;
    observesTapZohoPeople = tap.zoho_people;
    observesTargetWarehouse = target.warehouse;
    observesTargetS3 = target.s3;
    observesEtlDynamoConf = etl.dynamo;
    observesEtlGitlab = etl.gitlab;
    observesEtlGoogleSheets = etl.google_sheets;
    observesUtilsLogger = common.utils_logger;
    observesUtilsLogger2 = common.utils_logger_2;
  };
in {
  dev = builtins.mapAttrs (_: v: {
    source = [ outputs."${v.env.dev}" outputs."/common/dev/global_deps" ];
  }) products;
}
