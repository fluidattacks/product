# shellcheck shell=bash

sops_export_vars "common/secrets/dev.yaml" \
  INTEGRATES_API_TOKEN \
  && code-upload-groups-on-aws
