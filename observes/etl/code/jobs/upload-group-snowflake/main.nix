{ inputs, makeScript, makeSearchPaths, projectPath, outputs, ... }@makes_inputs:
let
  deps = with inputs.observesIndex; [ service.success_indicators.root ];
  env = builtins.map inputs.getRuntime deps;
  searchPaths =
    import (projectPath "/observes/etl/code/bin/search_paths.nix") makes_inputs;
  env_vars = makeSearchPaths {
    append = false;
    inherit (searchPaths) export;
  };
in makeScript {
  searchPaths = {
    bin = env ++ searchPaths.bin
      ++ [ outputs."/melts" inputs.nixpkgs.coreutils ];
    source = [ env_vars outputs."/common/utils/sops" ];
  };
  name = "observes-etl-code-upload-group-snowflake";
  entrypoint = ./entrypoint.sh;
}
