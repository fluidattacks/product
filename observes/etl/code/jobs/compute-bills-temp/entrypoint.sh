# shellcheck shell=bash

function job_compute_bills {
  local bucket_month
  local folder
  local month="${1}"

  sops_export_vars 'observes/secrets/prod.yaml' \
    'bugsnag_notifier_code_etl' \
    && sops_export_vars 'common/secrets/dev.yaml' \
      INTEGRATES_API_TOKEN \
    && export bugsnag_notifier_key="${bugsnag_notifier_code_etl}" \
    && folder="$(mktemp -d)" \
    && bucket_month="s3://integrates/continuous-data/bills/temp/$(date +%Y)/${month}" \
    && echo "[INFO] Temporary results folder: ${folder}" \
    && observes-etl-code compute-bills \
      "${folder}" \
      "$(date +%Y)" \
      "${month}" \
      "${INTEGRATES_API_TOKEN}" \
    && echo "[INFO] Syncing data from: ${folder} to ${bucket_month}" \
    && aws_s3_sync "${folder}" "${bucket_month}"
}

job_compute_bills "${@}"
