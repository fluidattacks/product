# shellcheck shell=bash

function job_code_amend {
  local group="${1}"

  sops_export_vars 'observes/secrets/prod.yaml' \
    'bugsnag_notifier_code_etl' \
    && sops_export_vars 'common/secrets/dev.yaml' \
      INTEGRATES_API_TOKEN \
    && export bugsnag_notifier_key="${bugsnag_notifier_code_etl}" \
    && echo "[INFO] Amending ${group}" \
    && observes-etl-code amend-authors-snowflake \
      --integrates-token "${INTEGRATES_API_TOKEN}" \
      --namespace "${group}"
}

job_code_amend "${@}"
