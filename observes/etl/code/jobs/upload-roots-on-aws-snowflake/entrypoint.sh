# shellcheck shell=bash

function main {

  sops_export_vars "common/secrets/dev.yaml" \
    INTEGRATES_API_TOKEN \
    && code-upload-roots-on-aws-snowflake
}

main
