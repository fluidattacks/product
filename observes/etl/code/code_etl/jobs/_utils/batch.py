import logging
from dataclasses import (
    dataclass,
)

import boto3
from fa_purity import (
    Cmd,
    FrozenList,
    Maybe,
    PureIterFactory,
    Stream,
    StreamFactory,
    StreamTransform,
)
from mypy_boto3_batch.client import (
    BatchClient,
)
from mypy_boto3_batch.literals import (
    JobStatusType,
)
from mypy_boto3_batch.type_defs import (
    JobSummaryTypeDef,
    ListJobsResponseTypeDef,
)

LOG = logging.getLogger(__name__)


@dataclass(frozen=True)
class QueueName:
    name: str


@dataclass(frozen=True)
class JobId:
    id_str: str


new_batch_client = Cmd.wrap_impure(lambda: boto3.client("batch"))


def _decode_job_obj(
    raw: JobSummaryTypeDef,
) -> JobId:
    return JobId(raw["jobId"])


@dataclass
class JobsPage:
    items: FrozenList[JobId]
    next_item: Maybe[str]


def _decode_response(response: ListJobsResponseTypeDef) -> JobsPage:
    items = PureIterFactory.from_list(response["jobSummaryList"]).map(_decode_job_obj)
    _next = Maybe.from_optional(response.get("nextToken"))
    return JobsPage(items.to_list(), _next)


def _list_jobs_page(
    client: BatchClient,
    queue: QueueName,
    status: JobStatusType,
    _next: Maybe[str],
) -> Cmd[JobsPage]:
    def _action() -> JobsPage:
        result = _next.map(
            lambda n: client.list_jobs(jobQueue=queue.name, jobStatus=status, nextToken=n),
        ).or_else_call(lambda: client.list_jobs(jobQueue=queue.name, jobStatus=status))
        return _decode_response(result)

    return Cmd.wrap_impure(_action)


def list_jobs(
    client: BatchClient,
    queue: QueueName,
    status: JobStatusType,
) -> Stream[JobId]:
    def _extract(page: JobsPage) -> Maybe[Maybe[str]]:
        return page.next_item.map(lambda n: Maybe.some(n))

    def _cmd(index: Maybe[str]) -> Cmd[JobsPage]:
        return _list_jobs_page(client, queue, status, index)

    return (
        StreamFactory.generate(_cmd, _extract, Maybe.empty(str))
        .map(lambda j: PureIterFactory.from_list(j.items))
        .transform(lambda x: StreamTransform.chain(x))
    )


def cancel_job(client: BatchClient, job: JobId, reason: str) -> Cmd[None]:
    def _action() -> None:
        LOG.info("Cancelling job: %s", job.id_str)
        client.cancel_job(jobId=job.id_str, reason=reason)

    return Cmd.wrap_impure(_action)


def terminate_job(client: BatchClient, job: JobId, reason: str) -> Cmd[None]:
    def _action() -> None:
        LOG.info("Terminating job: %s", job.id_str)
        client.terminate_job(jobId=job.id_str, reason=reason)

    return Cmd.wrap_impure(_action)


def cancel_all_jobs(queue: QueueName, status: JobStatusType, reason: str) -> Cmd[None]:
    return new_batch_client.bind(
        lambda c: new_batch_client.bind(
            lambda c2: list_jobs(c, queue, status)
            .map(lambda j: cancel_job(c2, j, reason))
            .transform(lambda s: StreamTransform.consume(s)),
        ),
    )


def terminate_all_jobs(queue: QueueName, status: JobStatusType, reason: str) -> Cmd[None]:
    return new_batch_client.bind(
        lambda c: new_batch_client.bind(
            lambda c2: list_jobs(c, queue, status)
            .map(lambda j: terminate_job(c2, j, reason))
            .transform(lambda s: StreamTransform.consume(s)),
        ),
    )
