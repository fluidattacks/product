import inspect
import logging
import os

from etl_utils.bug import (
    Bug,
)
from etl_utils.process import (
    RunningSubprocess,
    Subprocess,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    Maybe,
    PureIterFactory,
    PureIterTransform,
    ResultFactory,
)
from fa_purity.date_time import (
    DatetimeUTC,
)
from integrates_dal.client import (
    GroupsClient,
    OrgsClient,
)
from integrates_dal.clients import (
    new_group_client,
    new_org_client,
)
from integrates_dal.list_groups import (
    list_all_groups,
)

from code_etl.integrates_api import (
    IntegratesClient,
    IntegratesClientFactory,
    IntegratesToken,
    RootNickname,
)
from code_etl.objs import (
    GroupId,
)

LOG = logging.getLogger(__name__)


def upload_group_on_aws() -> str:
    return os.environ["UPLOAD_GROUP_ON_AWS"]


def upload_root_on_aws() -> str:
    return os.environ["UPLOAD_ROOT_ON_AWS"]


def send_job(
    group: GroupId,
    root: Maybe[RootNickname],
    date: DatetimeUTC,
    dry_run: bool,
) -> Cmd[None]:
    cmd = root.map(
        lambda r: (upload_root_on_aws(), group.name, r.name, date.date_time.isoformat()),
    ).value_or(
        (
            upload_group_on_aws(),
            group.name,
            date.date_time.isoformat(),
        ),
    )
    process = RunningSubprocess.run_universal_newlines(
        Subprocess(cmd, None, None, None, FrozenDict(dict(os.environ))),
    )
    return_code = process.bind(lambda p: p.wait(None))
    factory: ResultFactory[None, Exception] = ResultFactory()
    send = return_code.map(
        lambda c: factory.success(None)
        if c == 0
        else factory.failure(
            Exception(f"Process ended with return code: {c}"),
        ),
    ).map(
        lambda r: Bug.assume_success(
            "send_job",
            inspect.currentframe(),
            (str(group), str(root)),
            r,
        ),
    )
    msg = Cmd.wrap_impure(
        lambda: LOG.info(
            "Sending aws batch job for %s and %s",
            group.name,
            root.map(lambda r: r.name).value_or(None),
        ),
    )
    dry = Cmd.wrap_impure(
        lambda: LOG.info(
            "[dry-run] job for %s and %s will be sent",
            group,
            root.map(lambda r: r.name).value_or(None),
        ),
    )
    return dry if dry_run else msg + send


def _send_group_roots(
    client: IntegratesClient,
    group: GroupId,
    date: DatetimeUTC,
    dry_run: bool,
) -> Cmd[None]:
    return (
        client.get_roots(group)
        .map(lambda r: Bug.assume_success("get_roots", inspect.currentframe(), (str(group),), r))
        .bind(
            lambda r: PureIterFactory.from_list(tuple(r))
            .map(lambda root: send_job(group, Maybe.some(root), date, dry_run))
            .transform(PureIterTransform.consume),
        )
    )


def _send_jobs(
    client: OrgsClient,
    client_2: GroupsClient,
    date: DatetimeUTC,
    dry_run: bool,
) -> Cmd[None]:
    return list_all_groups(client, client_2).bind(
        lambda i: PureIterFactory.from_list(i)
        .map(lambda g: GroupId(g.name))
        .map(lambda g: send_job(g, Maybe.empty(), date, dry_run))
        .transform(PureIterTransform.consume),
    )


def dry_send_jobs(date: DatetimeUTC) -> Cmd[None]:
    return new_group_client().bind(
        lambda g: new_org_client().bind(lambda o: _send_jobs(o, g, date, True)),
    )


def send_jobs(date: DatetimeUTC) -> Cmd[None]:
    return new_group_client().bind(
        lambda g: new_org_client().bind(lambda o: _send_jobs(o, g, date, False)),
    )


def send_uploads_per_root(token: IntegratesToken, group: GroupId, date: DatetimeUTC) -> Cmd[None]:
    return IntegratesClientFactory.new_client(token).bind(
        lambda i: _send_group_roots(i, group, date, False),
    )


def send_uploads_per_root_snowflake(
    token: IntegratesToken,
    date: DatetimeUTC,
    dry_run: bool,
) -> Cmd[None]:
    return (
        new_group_client()
        .bind(
            lambda groups_client: new_org_client().bind(
                lambda organization_client: list_all_groups(organization_client, groups_client),
            ),
        )
        .bind(
            lambda groups: PureIterFactory.from_list(groups)
            .map(
                lambda group: IntegratesClientFactory.new_client(token).bind(
                    lambda integrates_client: _send_group_roots(
                        integrates_client,
                        GroupId(group.name),
                        date,
                        dry_run,
                    ),
                ),
            )
            .transform(PureIterTransform.consume),
        )
    )
