import inspect
import logging
import os

from etl_utils.bug import (
    Bug,
)
from etl_utils.process import (
    RunningSubprocess,
    Subprocess,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    PureIterFactory,
    PureIterTransform,
    ResultFactory,
)
from integrates_dal.client import (
    GroupsClient,
    OrgsClient,
)
from integrates_dal.clients import (
    new_group_client,
    new_org_client,
)
from integrates_dal.list_groups import (
    list_all_groups,
)

from code_etl.objs import (
    GroupId,
)

LOG = logging.getLogger(__name__)


def amend_group_on_aws() -> str:
    return os.environ["AMEND_GROUP_ON_AWS"]


def send_job(group: GroupId, dry_run: bool) -> Cmd[None]:
    cmd = (amend_group_on_aws(), group.name)
    process = RunningSubprocess.run_universal_newlines(
        Subprocess(cmd, None, None, None, FrozenDict(dict(os.environ))),
    )
    return_code = process.bind(lambda p: p.wait(None))
    factory: ResultFactory[None, Exception] = ResultFactory()
    send = return_code.map(
        lambda c: factory.success(None)
        if c == 0
        else factory.failure(
            Exception(f"Process ended with return code: {c}"),
        ),
    ).map(lambda r: Bug.assume_success("send_job", inspect.currentframe(), (str(group),), r))
    msg = Cmd.wrap_impure(
        lambda: LOG.info(
            "Sending aws batch job for %s",
            group.name,
        ),
    )
    dry = Cmd.wrap_impure(
        lambda: LOG.info(
            "[dry-run] job for %s will be sent",
            group,
        ),
    )
    return dry if dry_run else msg + send


def _send_jobs(
    client: OrgsClient,
    client_2: GroupsClient,
    dry_run: bool,
) -> Cmd[None]:
    return list_all_groups(client, client_2).bind(
        lambda i: PureIterFactory.from_list(i)
        .map(lambda g: GroupId(g.name))
        .map(lambda g: send_job(g, dry_run))
        .transform(PureIterTransform.consume),
    )


def dry_send_jobs() -> Cmd[None]:
    return new_group_client().bind(
        lambda g: new_org_client().bind(lambda o: _send_jobs(o, g, True)),
    )


def send_jobs() -> Cmd[None]:
    return new_group_client().bind(
        lambda g: new_org_client().bind(lambda o: _send_jobs(o, g, False)),
    )
