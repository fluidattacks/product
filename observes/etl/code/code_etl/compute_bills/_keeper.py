from __future__ import (
    annotations,
)

import logging
from csv import (
    QUOTE_NONNUMERIC,
    DictWriter,
)
from dataclasses import (
    dataclass,
)

from etl_utils.typing import (
    IO,
    Callable,
    Dict,
    FrozenSet,
    Optional,
    Tuple,
)
from fa_purity import (
    Cmd,
    CmdTransform,
)

from code_etl.compute_bills.core import (
    Contribution,
    FinalActiveUsersReport,
)
from code_etl.integrates_api import (
    IntegratesClient,
)
from code_etl.objs import (
    GroupId,
    OrgName,
    User,
)

from . import (
    _get_org,
)

LOG = logging.getLogger(__name__)


def _group_to_str(grp: GroupId) -> str:
    return grp.name


@dataclass(frozen=True)
class ReportRow:
    user: User
    contrib: Contribution
    groups: FrozenSet[GroupId]


@dataclass(frozen=True)
class _ReportKeeper:
    writer: DictWriter[str]
    get_org: Callable[[GroupId], Optional[OrgName]]


@dataclass(frozen=True)
class ReportKeeper:
    _inner: _ReportKeeper

    def _write_row(
        self,
        current: GroupId,
        row: ReportRow,
    ) -> Cmd[None]:
        if current not in row.groups:
            title = "A user in the final report does not belong to the group"
            raise ValueError(title + ": " + current.name)
        org = self._inner.get_org(current)

        def _group_filter(grp: GroupId) -> bool:
            return self._inner.get_org(grp) == org

        groups_contributed = frozenset(filter(_group_filter, row.groups)) if org else row.groups
        data: Dict[str, str] = {
            "actor": row.user.name + " <" + row.user.email + ">",
            "groups": ", ".join(map(_group_to_str, groups_contributed)),
            "commit": row.contrib.commit_id.hash.hash,
            "repository": row.contrib.commit_id.repo.repository.name,
        }

        def _action() -> None:
            self._inner.writer.writerow(data)

        return Cmd.wrap_impure(_action)

    def save(
        self,
        group: GroupId,
        report: FinalActiveUsersReport,
    ) -> Cmd[None]:
        def _write(item: Tuple[User, Tuple[Contribution, FrozenSet[GroupId]]]) -> Cmd[None]:
            return self._write_row(group, ReportRow(item[0], item[1][0], item[1][1]))

        write_rows = tuple(map(_write, report.data.items()))

        def _action() -> None:
            self._inner.writer.writeheader()

        return Cmd.wrap_impure(_action) + CmdTransform.serial_merge(write_rows).map(lambda _: None)

    @staticmethod
    def new(file: IO[str], client: IntegratesClient) -> ReportKeeper:
        file_columns = frozenset(["actor", "groups", "commit", "repository"])
        writer = DictWriter(
            file,
            sorted(file_columns),
            quoting=QUOTE_NONNUMERIC,
        )

        return ReportKeeper(_ReportKeeper(writer, lambda g: _get_org.get_org(client, g)))
