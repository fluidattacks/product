import inspect

from connection_manager import (
    ConnectionConf,
    ConnectionManagerFactory,
    Databases,
    DbClients,
    Roles,
    Warehouses,
)
from etl_utils.bug import (
    Bug,
)
from etl_utils.typing import (
    Callable,
    TypeVar,
)
from fa_purity import (
    Cmd,
    ResultE,
    cast_exception,
)

_T = TypeVar("_T")


def with_connection(
    process: Callable[[DbClients], Cmd[ResultE[_T]]],
) -> Cmd[ResultE[_T]]:
    conf = ConnectionConf(
        Warehouses.GENERIC_COMPUTE,
        Roles.ETL_UPLOADER,
        Databases.OBSERVES,
    )
    return (
        ConnectionManagerFactory.observes_manager()
        .map(lambda r: Bug.assume_success("connection_manager", inspect.currentframe(), (), r))
        .bind(
            lambda manager: manager.execute_with_snowflake(process, conf).map(
                lambda r: r.alt(lambda c: c.map(lambda e: e, cast_exception)),
            ),
        )
    )
