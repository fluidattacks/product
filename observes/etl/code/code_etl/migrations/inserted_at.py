import inspect

from etl_utils.bug import (
    Bug,
)
from etl_utils.typing import (
    Dict,
)
from fa_purity import (
    Cmd,
    FrozenTools,
)
from redshift_client.core.id_objs import (
    Identifier,
    SchemaId,
    TableId,
)
from redshift_client.sql_client import (
    Query,
    SqlCursor,
)

SCHEMA = SchemaId(Identifier.new("code"))
COMMITS_TABLE = TableId(Identifier.new("commits"))
REGISTRATION_TABLE = TableId(Identifier.new("registrations"))


def add_inserted_at_column(sql: SqlCursor) -> Cmd[None]:
    statement = """
    ALTER TABLE {schema}.{table}
    ADD COLUMN inserted_at TIMESTAMP WITH TIME ZONE
    DEFAULT NULL;
    """
    identifiers: Dict[str, str] = {
        "schema": SCHEMA.name.to_str(),
        "table": COMMITS_TABLE.name.to_str(),
    }
    return sql.execute(Query.dynamic_query(statement, FrozenTools.freeze(identifiers)), None).map(
        lambda r: Bug.assume_success(
            "_add_inserted_at_column",
            inspect.currentframe(),
            tuple({}),
            r,
        ),
    )


def compute_column_values(sql: SqlCursor) -> Cmd[None]:
    statement = """
    UPDATE {schema}.{commits_table}
    SET inserted_at =
        CASE
            WHEN seen_at != '1970-01-01'::date THEN seen_at
            ELSE (
                SELECT seen_at FROM {schema}.{registrations_table} T
                WHERE T."group" = {schema}.{commits_table}.namespace
                AND T.repository = {schema}.{commits_table}.repository
            )
        END
    """
    identifiers: Dict[str, str] = {
        "schema": SCHEMA.name.to_str(),
        "commits_table": COMMITS_TABLE.name.to_str(),
        "registrations_table": REGISTRATION_TABLE.name.to_str(),
    }
    return sql.execute(
        Query.dynamic_query(statement, FrozenTools.freeze(identifiers)),
        None,
    ).map(
        lambda r: Bug.assume_success(
            "_delete_legacy_reg",
            inspect.currentframe(),
            (),
            r,
        ),
    )


def adjust_inserted_at(
    sql: SqlCursor,
) -> Cmd[None]:
    statement = """
    UPDATE {schema}.{commits_table}
    SET inserted_at =
        CASE
            WHEN inserted_at = '1970-01-01'::date THEN (
                SELECT seen_at FROM {schema}.{registrations_table} T
                WHERE T."group" = {schema}.{commits_table}.namespace
                AND T.repository = {schema}.{commits_table}.repository
            )
            ELSE inserted_at
        END
    """
    identifiers: Dict[str, str] = {
        "schema": SCHEMA.name.to_str(),
        "commits_table": COMMITS_TABLE.name.to_str(),
        "registrations_table": REGISTRATION_TABLE.name.to_str(),
    }
    return sql.execute(
        Query.dynamic_query(statement, FrozenTools.freeze(identifiers)),
        None,
    ).map(
        lambda r: Bug.assume_success(
            "adjust_inserted_at",
            inspect.currentframe(),
            (),
            r,
        ),
    )
