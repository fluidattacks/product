from dataclasses import (
    dataclass,
)

from code_etl.objs import (
    GroupId,
)


@dataclass(frozen=True)
class RootNickname:
    name: str


@dataclass(frozen=True)
class IgnoredPath:
    group: GroupId
    nickname: str
    file_path: str
