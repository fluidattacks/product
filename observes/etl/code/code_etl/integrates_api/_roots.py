import inspect

from etl_utils.bug import (
    Bug,
)
from etl_utils.typing import (
    FrozenSet,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    FrozenList,
    FrozenTools,
    Maybe,
    PureIterFactory,
    PureIterTransform,
    Result,
    ResultE,
    ResultTransform,
)
from fa_purity.json import (
    JsonObj,
    JsonPrimitiveUnfolder,
    JsonUnfolder,
    Unfolder,
)

from code_etl.objs import (
    GroupId,
)

from ._core import (
    RootNickname,
)
from ._error import (
    ApiError,
)
from ._raw_client import (
    GraphQlAsmClient,
)


def _decode_root(raw: JsonObj) -> ResultE[Maybe[RootNickname]]:
    if raw == FrozenDict({}):
        return Result.success(Maybe.empty())
    return (
        JsonUnfolder.require(raw, "nickname", Unfolder.to_primitive)
        .bind(JsonPrimitiveUnfolder.to_str)
        .map(RootNickname)
        .map(lambda v: Maybe.some(v))
    )


def _decode_roots_key(raw: JsonObj) -> ResultE[FrozenList[JsonObj]]:
    return JsonUnfolder.require(raw, "group", Unfolder.to_json).bind(
        lambda j: JsonUnfolder.require(
            j,
            "roots",
            lambda v: Unfolder.to_list_of(v, Unfolder.to_json),
        ),
    )


def _decode_roots(raw: JsonObj) -> ResultE[FrozenSet[RootNickname]]:
    return (
        _decode_roots_key(raw)
        .map(lambda i: PureIterFactory.from_list(i).map(_decode_root))
        .bind(lambda i: ResultTransform.all_ok(i.to_list()))
        .map(lambda i: PureIterTransform.filter_maybe(PureIterFactory.from_list(i)).to_list())
        .map(frozenset)
    )


def get_roots(
    client: GraphQlAsmClient,
    group: GroupId,
) -> Cmd[Result[FrozenSet[RootNickname], ApiError]]:
    query = """
    query ObservesGetRoots($groupName: String!) {
        group(groupName: $groupName){
            roots {
                ...on GitRoot{
                    nickname
                }
            }
        }
    }
    """
    values = {"groupName": group.name}
    return client.get(query, FrozenTools.freeze(values)).map(
        lambda r: r.map(
            lambda j: Bug.assume_success(
                "decode_roots",
                inspect.currentframe(),
                (str(values),),
                _decode_roots(j),
            ),
        ),
    )
