import inspect
from collections.abc import (
    Callable,
)

from etl_utils.bug import (
    Bug,
)
from etl_utils.typing import (
    FrozenSet,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    FrozenList,
    FrozenTools,
    PureIterFactory,
    Result,
    ResultE,
)
from fa_purity.json import (
    JsonObj,
    JsonPrimitiveUnfolder,
    JsonUnfolder,
    Unfolder,
)

from code_etl.integrates_api._error import (
    DecodeError,
)
from code_etl.objs import (
    GroupId,
    RepoId,
)

from ._core import (
    IgnoredPath,
)
from ._error import (
    ApiError,
)
from ._raw_client import (
    GraphQlAsmClient,
)


def _decode_ignored_paths(_raw: JsonObj, group: GroupId) -> ResultE[FrozenList[IgnoredPath]]:
    if _raw == FrozenDict({}):
        return Result.success(())

    node = JsonUnfolder.require(
        _raw,
        "node",
        lambda v: Unfolder.to_json(v),
    )

    nickname = node.bind(
        lambda _node: JsonUnfolder.require(
            _node,
            "nickname",
            lambda v: Unfolder.to_primitive(v).bind(JsonPrimitiveUnfolder.to_str),
        ),
    )
    files = node.bind(
        lambda _node: JsonUnfolder.require(
            _node,
            "gitignore",
            lambda i: Unfolder.to_list_of(
                i,
                lambda v: Unfolder.to_primitive(v).bind(JsonPrimitiveUnfolder.to_str),
            ),
        ),
    )
    return nickname.bind(
        lambda n: files.map(lambda fs: tuple(IgnoredPath(group, n, f) for f in fs)),
    )


def _decode_raw_ignored_paths(
    raw: JsonObj,
    group: GroupId,
) -> Result[FrozenList[IgnoredPath], DecodeError]:
    group_info = JsonUnfolder.require(
        raw,
        "group",
        lambda group_info: Unfolder.to_json(group_info),
    )

    roots_info = group_info.bind(
        lambda roots_info: JsonUnfolder.require(
            roots_info,
            "gitRoots",
            lambda git_root: Unfolder.to_json(git_root),
        ),
    )

    edges = roots_info.bind(
        lambda roots: JsonUnfolder.require(
            roots,
            "edges",
            lambda _edges: Unfolder.to_list_of(
                _edges,
                lambda i: Unfolder.to_json(i).bind(lambda x: _decode_ignored_paths(x, group)),
            ),
        ),
    )

    return edges.map(
        lambda i: PureIterFactory.from_list(i)
        .bind(lambda x: PureIterFactory.from_list(x))
        .to_list(),
    ).alt(lambda e: DecodeError("raw_ignored_paths", JsonUnfolder.dumps(raw), e))


def _get_ignored_paths() -> (
    Callable[
        [GraphQlAsmClient, RepoId],
        Cmd[Result[FrozenSet[IgnoredPath], ApiError]],
    ]
):
    _cached_ignored_paths: dict[tuple[str, str], frozenset[IgnoredPath]] = {}

    def _get_gitignore(
        client: GraphQlAsmClient,
        repo: RepoId,
    ) -> Cmd[Result[FrozenSet[IgnoredPath], ApiError]]:
        nonlocal _cached_ignored_paths

        cache_key = (repo.group.name, repo.repository.name)
        if cache_key in _cached_ignored_paths:
            return Cmd.wrap_value(Result.success(_cached_ignored_paths[cache_key], ApiError))

        query = """
        query IgnoredPaths($groupName: String!, $nickname: String!){
            group(groupName: $groupName){
                gitRoots(nickname: $nickname){
                    edges {
                        node {
                            nickname
                            gitignore
                            branch
                            id
                        }
                    }
                }
            }
        }
        """
        values = {
            "groupName": repo.group.name,
            "nickname": repo.repository.name,
        }
        return client.get(query, FrozenTools.freeze(values)).map(
            lambda r: r.map(
                lambda j: Bug.assume_success(
                    "decode_raw_ignored_paths",
                    inspect.currentframe(),
                    (str(j),),
                    _decode_raw_ignored_paths(j, repo.group)
                    .map(lambda v: frozenset(v))
                    .alt(lambda d: Exception(d)),
                ),
            ).map(lambda result: _cached_ignored_paths.setdefault(cache_key, result)),
        )

    return _get_gitignore


get_ignored_paths = _get_ignored_paths()
