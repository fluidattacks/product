import inspect

from etl_utils.bug import (
    Bug,
)
from fa_purity import (
    Cmd,
    FrozenTools,
    Result,
    ResultE,
    ResultTransform,
    cast_exception,
)
from fa_purity.json import (
    JsonObj,
    JsonPrimitiveUnfolder,
    JsonUnfolder,
    Unfolder,
)

from code_etl.objs import (
    GroupId,
    OrgName,
)

from ._error import (
    ApiError,
)
from ._raw_client import (
    GraphQlAsmClient,
)


def _decode_org(raw: JsonObj) -> ResultE[OrgName]:
    group = ResultTransform.try_get(raw, "group")
    return group.bind(
        lambda g: Unfolder.to_json(g)
        .bind(
            lambda j: JsonUnfolder.require(
                j,
                "organization",
                lambda v: Unfolder.to_primitive(v).bind(JsonPrimitiveUnfolder.to_str),
            ),
        )
        .alt(
            lambda e: ValueError(
                "Decode error at group org input: " + JsonUnfolder.dumps(raw) + f"i.e. {e}",
            ),
        )
        .alt(cast_exception)
        .map(OrgName),
    )


def get_org(client: GraphQlAsmClient, group: GroupId) -> Cmd[Result[OrgName, ApiError]]:
    query = """
    query ObservesGetGroupOrganization($groupName: String!){
        group(groupName: $groupName){
            organization
        }
    }
    """
    values = {"groupName": group.name}
    return client.get(query, FrozenTools.freeze(values)).map(
        lambda r: r.map(
            lambda d: Bug.assume_success(
                "decode_org",
                inspect.currentframe(),
                (str(group),),
                _decode_org(d),
            ),
        ),
    )
