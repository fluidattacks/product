from dataclasses import (
    dataclass,
)

from etl_utils.typing import (
    TypeVar,
)
from fa_purity import (
    FrozenList,
    Result,
)
from fa_purity.json import (
    JsonValue,
)

_T = TypeVar("_T")


@dataclass
class ApiError(Exception):
    errors: FrozenList[JsonValue]


@dataclass
class DecodeError(Exception):
    description: str
    value: str
    previous: Exception


ApiResult = Result[_T, ApiError]  # type: ignore[misc]
