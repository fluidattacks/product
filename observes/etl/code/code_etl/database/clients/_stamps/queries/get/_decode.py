from dataclasses import (
    dataclass,
)

from etl_utils.decode import (
    require_index,
)
from etl_utils.typing import (
    Tuple,
    TypeVar,
)
from fa_purity import (
    FrozenDict,
    Maybe,
    Result,
    ResultE,
    ResultFactory,
)
from fa_purity.date_time import (
    DatetimeFactory,
    DatetimeTZ,
    DatetimeUTC,
)
from fa_purity.json import (
    JsonPrimitiveUnfolder,
)
from redshift_client.core.id_objs import (
    ColumnId,
)
from redshift_client.sql_client import (
    DbPrimitive,
    RowData,
)

from code_etl.database.clients import (
    _utils,
)
from code_etl.database.clients._stamps.queries.init_table import (
    AUTHOR_EMAIL_COLUMN,
    AUTHOR_NAME_COLUMN,
    AUTHORED_AT_COLUMN,
    COMMITTED_AT_COLUMN,
    COMMITTER_EMAIL_COLUMN,
    COMMITTER_NAME_COLUMN,
    GROUP_COLUMN,
    HASH_COLUMN,
    INSERTED_AT_COLUMN,
    MESSAGE_COLUMN,
    REPOSITORY_COLUMN,
    SEEN_AT_COLUMN,
    SUMMARY_COLUMN,
    TOTAL_DELETIONS_COLUMN,
    TOTAL_FILES_COLUMN,
    TOTAL_INSERTIONS_COLUMN,
    TOTAL_LINES_COLUMN,
)
from code_etl.objs import (
    Commit,
    CommitData,
    CommitDataId,
    CommitId,
    CommitStamp,
    Deltas,
    GroupId,
    RepoId,
    RepoName,
    User,
)
from code_etl.str_utils import (
    truncate,
)

_K = TypeVar("_K")
_V = TypeVar("_V")


def _require_key(items: FrozenDict[_K, _V], key: _K) -> ResultE[_V]:
    factory: ResultFactory[_V, Exception] = ResultFactory()
    if key in items:
        return factory.success(items[key])
    return factory.failure(KeyError(f"key `{key}` not found"))


def _datetime_utc(raw: DbPrimitive) -> ResultE[DatetimeUTC]:
    return _utils.assert_datetime(raw).bind(
        lambda d: DatetimeTZ.assert_tz(d).map(lambda x: DatetimeFactory.to_utc(x)),
    )


def _to_str(raw: DbPrimitive) -> ResultE[str]:
    return raw.map(
        lambda j: JsonPrimitiveUnfolder.to_str(j),
        lambda _: Result.failure(Exception("Expected `str` not `datetime`")),
    )


def _to_int(raw: DbPrimitive) -> ResultE[int]:
    return raw.map(
        lambda j: JsonPrimitiveUnfolder.to_int(j),
        lambda _: Result.failure(Exception("Expected `int` not `datetime`")),
    )


@dataclass(frozen=True)
class StampDecoder:
    index_map: FrozenDict[ColumnId, int]

    def _decode_user(
        self,
        raw: RowData,
        name: ColumnId,
        email: ColumnId,
        created_at: ColumnId,
    ) -> ResultE[Tuple[User, DatetimeUTC]]:
        author_name: ResultE[str] = _require_key(self.index_map, name).bind(
            lambda i: require_index(raw.data, i).bind(_to_str),
        )
        author_email: ResultE[str] = _require_key(self.index_map, email).bind(
            lambda i: require_index(raw.data, i).bind(_to_str),
        )
        authored_at = _require_key(self.index_map, created_at).bind(
            lambda i: require_index(raw.data, i).bind(_datetime_utc),
        )
        return author_name.bind(
            lambda n: author_email.bind(lambda e: authored_at.map(lambda d: (User(n, e), d))),
        )

    def decode_deltas(self, raw: RowData) -> ResultE[Deltas]:
        _total_insertions = _require_key(self.index_map, TOTAL_INSERTIONS_COLUMN).bind(
            lambda i: require_index(raw.data, i).bind(_to_int),
        )
        _total_deletions = _require_key(self.index_map, TOTAL_DELETIONS_COLUMN).bind(
            lambda i: require_index(raw.data, i).bind(_to_int),
        )
        _total_lines = _require_key(self.index_map, TOTAL_LINES_COLUMN).bind(
            lambda i: require_index(raw.data, i).bind(_to_int),
        )
        _total_files = _require_key(self.index_map, TOTAL_FILES_COLUMN).bind(
            lambda i: require_index(raw.data, i).bind(_to_int),
        )
        return _total_insertions.bind(
            lambda ti: _total_deletions.bind(
                lambda td: _total_lines.bind(
                    lambda tl: _total_files.map(lambda tf: Deltas(ti, td, tl, tf)),
                ),
            ),
        )

    def decode_data(self, raw: RowData) -> ResultE[CommitData]:
        _author = self._decode_user(
            raw,
            AUTHOR_NAME_COLUMN,
            AUTHOR_EMAIL_COLUMN,
            AUTHORED_AT_COLUMN,
        )
        _committer = self._decode_user(
            raw,
            COMMITTER_NAME_COLUMN,
            COMMITTER_EMAIL_COLUMN,
            COMMITTED_AT_COLUMN,
        )
        _message = (
            _require_key(self.index_map, MESSAGE_COLUMN)
            .bind(lambda i: require_index(raw.data, i).bind(_to_str))
            .map(lambda s: truncate(s, 4096))
        )
        _summary = (
            _require_key(self.index_map, SUMMARY_COLUMN)
            .bind(lambda i: require_index(raw.data, i).bind(_to_str))
            .map(lambda s: truncate(s, 256))
        )
        _deltas = self.decode_deltas(raw)
        return _author.bind(
            lambda a: _committer.bind(
                lambda c: _message.bind(
                    lambda m: _summary.bind(
                        lambda s: _deltas.map(
                            lambda d: CommitData(a[0], a[1], c[0], c[1], m, s, d, Maybe.empty()),
                        ),
                    ),
                ),
            ),
        )

    def decode_repo_id(self, raw: RowData) -> ResultE[RepoId]:
        _group = (
            _require_key(self.index_map, GROUP_COLUMN)
            .bind(lambda i: require_index(raw.data, i).bind(_to_str))
            .map(GroupId)
        )
        _repo = (
            _require_key(self.index_map, REPOSITORY_COLUMN)
            .bind(lambda i: require_index(raw.data, i).bind(_to_str))
            .map(RepoName)
        )
        return _group.bind(lambda g: _repo.map(lambda r: RepoId(g, r)))

    def decode_commit_data_id(self, raw: RowData) -> ResultE[CommitDataId]:
        _hash = _require_key(self.index_map, HASH_COLUMN).bind(
            lambda i: require_index(raw.data, i).bind(_to_str),
        )
        return self.decode_repo_id(raw).bind(
            lambda r: _hash.map(lambda h: CommitDataId(r, CommitId(h))),
        )

    def _is_new(self, seen_at: DatetimeUTC) -> bool:
        return seen_at == DatetimeFactory.EPOCH_START

    def decode_stamp(self, raw: RowData) -> ResultE[CommitStamp]:
        _seen_at = _require_key(self.index_map, SEEN_AT_COLUMN).bind(
            lambda i: require_index(raw.data, i).bind(_datetime_utc),
        )
        _inserted_at = _require_key(self.index_map, INSERTED_AT_COLUMN).bind(
            lambda i: require_index(raw.data, i).bind(_datetime_utc),
        )
        return self.decode_commit_data_id(raw).bind(
            lambda i: self.decode_data(raw).bind(
                lambda d: _seen_at.bind(
                    lambda s: _inserted_at.map(
                        lambda inserted: CommitStamp.initial_stamp(Commit(i, d), inserted)
                        if self._is_new(s)
                        else CommitStamp.normal_stamp(Commit(i, d), s, inserted),
                    ),
                ),
            ),
        )
