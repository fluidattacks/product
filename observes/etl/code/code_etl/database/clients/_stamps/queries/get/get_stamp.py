import inspect

from etl_utils.bug import (
    Bug,
)
from etl_utils.typing import (
    Dict,
    Tuple,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    FrozenTools,
    Maybe,
    PureIterFactory,
)
from fa_purity.json import (
    Primitive,
)
from redshift_client.core.id_objs import (
    SchemaId,
    TableId,
)
from redshift_client.sql_client import (
    DbPrimitiveFactory,
    QueryValues,
)
from snowflake_client import (
    SnowflakeCursor,
    SnowflakeQuery,
)

from code_etl.database.clients._stamps.queries.init_table import (
    GROUP_COLUMN,
    HASH_COLUMN,
    REPOSITORY_COLUMN,
    table_definition,
)
from code_etl.objs import (
    CommitDataId,
    CommitStamp,
)

from ._decode import (
    StampDecoder,
)


def _query(
    schema: SchemaId,
    table: TableId,
    commit_id: CommitDataId,
) -> Tuple[SnowflakeQuery, QueryValues]:
    _fields = PureIterFactory.from_list(table_definition().order).map(lambda c: c.name.to_str())
    _attrs = ",".join(_fields)
    statement = (
        f"SELECT {_attrs}"  # noqa: S608
        " FROM {schema}.{table}"
        " WHERE {group_column} = %(namespace)s"
        " AND {repository_column} = %(repository)s"
        " AND {hash_column} = %(hash)s"
        " LIMIT 1"
    )
    id_args: Dict[str, str] = {
        "group_column": GROUP_COLUMN.name.to_str(),
        "repository_column": REPOSITORY_COLUMN.name.to_str(),
        "hash_column": HASH_COLUMN.name.to_str(),
        "schema": schema.name.to_str(),
        "table": table.name.to_str(),
    }
    args: Dict[str, Primitive] = {
        "namespace": commit_id.repo.group.name,
        "repository": commit_id.repo.repository.name,
        "hash": commit_id.hash.hash,
    }
    query = Bug.assume_success(
        "get_stamp_query",
        inspect.currentframe(),
        (str(statement), str(id_args)),
        SnowflakeQuery.dynamic_query(statement, FrozenTools.freeze(id_args)),
    )
    return (
        query,
        QueryValues(DbPrimitiveFactory.from_raw_prim_dict(FrozenTools.freeze(args))),
    )


def get_stamp(
    cursor: SnowflakeCursor,
    schema: SchemaId,
    table: TableId,
    commit_id: CommitDataId,
) -> Cmd[Maybe[CommitStamp]]:
    request = cursor.execute(*_query(schema, table, commit_id)).map(
        lambda r: Bug.assume_success(
            "get_stamp_request",
            inspect.currentframe(),
            (str(schema), str(table), str(commit_id)),
            r,
        ),
    )
    index_map = (
        PureIterFactory.from_list(table_definition().order)
        .enumerate(0)
        .map(lambda t: (t[1], t[0]))
        .transform(lambda p: FrozenDict(dict(p)))
    )
    fetch = cursor.fetch_one.map(
        lambda r: Bug.assume_success(
            "get_stamp_fetch_result",
            inspect.currentframe(),
            (str(schema), str(table), str(commit_id)),
            r,
        ),
    ).map(
        lambda m: m.map(
            lambda r: Bug.assume_success(
                "get_stamp_decode",
                inspect.currentframe(),
                (str(schema), str(table), str(commit_id)),
                StampDecoder(index_map).decode_stamp(r),
            ),
        ),
    )
    return request + fetch
