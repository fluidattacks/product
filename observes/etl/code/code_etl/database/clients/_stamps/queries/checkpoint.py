import inspect

from etl_utils.bug import (
    Bug,
)
from etl_utils.decode import (
    require_index,
)
from etl_utils.typing import (
    Dict,
    Tuple,
)
from fa_purity import (
    Cmd,
    FrozenTools,
    Maybe,
    ResultE,
    ResultFactory,
)
from fa_purity.json import (
    JsonPrimitiveUnfolder,
    Primitive,
)
from redshift_client.core.id_objs import (
    SchemaId,
    TableId,
)
from redshift_client.sql_client import (
    DbPrimitiveFactory,
    QueryValues,
    RowData,
)
from snowflake_client import (
    SnowflakeCursor,
    SnowflakeQuery,
)

from code_etl.database.clients._stamps.queries.init_table import (
    GROUP_COLUMN,
    HASH_COLUMN,
    REPOSITORY_COLUMN,
)
from code_etl.objs import (
    RepoId,
)


def _query(schema: SchemaId, table: TableId, repo: RepoId) -> Tuple[SnowflakeQuery, QueryValues]:
    statement = """
    SELECT {hash_column} FROM {schema}.{table}
    WHERE
        {group_column} = %(group)s
        and {repository_column} = %(repository)s
    ORDER BY inserted_at DESC, authored_at DESC
    LIMIT 1
    """
    identifiers: Dict[str, str] = {
        "hash_column": HASH_COLUMN.name.to_str(),
        "group_column": GROUP_COLUMN.name.to_str(),
        "repository_column": REPOSITORY_COLUMN.name.to_str(),
        "schema": schema.name.to_str(),
        "table": table.name.to_str(),
    }
    args: Dict[str, Primitive] = {
        "group": repo.group.name,
        "repository": repo.repository.name,
    }
    query = Bug.assume_success(
        "last_commit_hash_query",
        inspect.currentframe(),
        (str(statement), str(identifiers)),
        SnowflakeQuery.dynamic_query(statement, FrozenTools.freeze(identifiers)),
    )
    return (
        query,
        QueryValues(DbPrimitiveFactory.from_raw_prim_dict(FrozenTools.freeze(args))),
    )


def _decode(raw: Maybe[RowData]) -> ResultE[Maybe[str]]:
    _factory: ResultFactory[Maybe[str], Exception] = ResultFactory()
    return raw.map(
        lambda r: require_index(r.data, 0).bind(
            lambda j: j.map(
                lambda v: JsonPrimitiveUnfolder.to_str(v).bind(
                    lambda s: _factory.success(Maybe.some(s)),
                ),
                lambda _: _factory.failure(
                    TypeError("Expected `str` but got `datetime`"),
                ),
            ),
        ),
    ).value_or(_factory.success(Maybe.empty()))


def last_commit_hash(
    cursor: SnowflakeCursor,
    schema: SchemaId,
    table: TableId,
    repo: RepoId,
) -> Cmd[Maybe[str]]:
    request = cursor.execute(*_query(schema, table, repo)).map(
        lambda r: Bug.assume_success(
            "last_commit_hash_request",
            inspect.currentframe(),
            (str(schema), str(table), str(repo)),
            r,
        ),
    )
    fetch = cursor.fetch_one.map(
        lambda r: Bug.assume_success(
            "last_commit_hash_fetch_result",
            inspect.currentframe(),
            (str(schema), str(table), str(repo)),
            r,
        ),
    ).map(
        lambda r: Bug.assume_success(
            "last_commit_hash_decode_result",
            inspect.currentframe(),
            (str(schema), str(table), str(repo)),
            _decode(r),
        ),
    )
    return request + fetch
