import inspect

from etl_utils.bug import (
    Bug,
)
from etl_utils.typing import (
    Dict,
    TypeVar,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    FrozenList,
    FrozenTools,
    PureIterFactory,
)
from fa_purity.json import (
    Primitive,
)
from redshift_client.client import (
    GroupedRows,
    TableRow,
)
from redshift_client.core.id_objs import (
    ColumnId,
    DbTableId,
)
from redshift_client.sql_client import (
    DbPrimitive,
    DbPrimitiveFactory,
)
from snowflake_client import (
    ClientFactory,
    SnowflakeCursor,
)

from code_etl.objs import (
    FileStampRelation,
)

from .init_table import (
    FILE_PATH_COLUMN,
    GROUP_COLUMN,
    HASH_COLUMN,
    REPOSITORY_COLUMN,
    table_definition,
)

_K = TypeVar("_K")


def _to_db_primitive_values(raw: FrozenDict[_K, Primitive]) -> FrozenDict[_K, DbPrimitive]:
    return FrozenDict({k: DbPrimitiveFactory.from_raw(v) for k, v in raw.items()})


def _encode_stamp(stamp: FileStampRelation) -> TableRow:
    encoded: Dict[ColumnId, Primitive] = {
        GROUP_COLUMN: stamp.commit_id.repo.group.name,
        REPOSITORY_COLUMN: stamp.commit_id.repo.repository.name,
        HASH_COLUMN: stamp.commit_id.hash.hash,
        FILE_PATH_COLUMN: stamp.file_path,
    }
    result = TableRow.new(
        table_definition(),
        _to_db_primitive_values(FrozenTools.freeze(encoded)),
    )
    return Bug.assume_success("_encode_stamp", inspect.currentframe(), (), result)


def insert_files(
    cursor: SnowflakeCursor,
    table: DbTableId,
    stamps: FrozenList[FileStampRelation],
) -> Cmd[None]:
    client = ClientFactory.new_table_client(cursor)
    _rows = GroupedRows.new(
        table_definition(),
        PureIterFactory.from_list(stamps).map(_encode_stamp).to_list(),
    )
    rows = Bug.assume_success("insert_stamps_GroupedRows", inspect.currentframe(), (), _rows)
    return client.named_insert(table, rows).map(
        lambda r: Bug.assume_success(
            "insert_stamps",
            inspect.currentframe(),
            (),
            r,
        ),
    )
