import logging

from connection_manager import (
    ClientAdapter,
)
from fa_purity import (
    Cmd,
    Coproduct,
    FrozenList,
    PureIterFactory,
)
from redshift_client.core.id_objs import (
    DbTableId,
)
from snowflake_client import (
    ClientFactory,
    SnowflakeCursor,
)

from code_etl.database import (
    All,
    StampsClient,
)
from code_etl.objs import (
    DbCommitStamp,
    GroupId,
)

from . import (
    _extractor,
)
from .queries import (
    amend_users,
    checkpoint,
    count,
    exist,
    get,
    init_table,
    insert_stamps,
)
from .queries.file_commit import (
    init_table as files_init_table,
)
from .queries.file_commit import (
    insert as insert_files,
)
from .queries.get import (
    get_stamp,
)

LOG = logging.getLogger(__name__)


def _adapter(item: GroupId | All) -> Coproduct[GroupId, All]:
    if isinstance(item, GroupId):
        return Coproduct.inl(item)
    return Coproduct.inr(item)


def _insert(
    cursor: SnowflakeCursor,
    commits: DbTableId,
    files_table: DbTableId,
    stamps: FrozenList[DbCommitStamp],
) -> Cmd[None]:
    msg_1 = Cmd.wrap_impure(lambda: LOG.info("Inserting %s stamps...", len(stamps)))
    insert_1: Cmd[None] = msg_1 + insert_stamps.insert_stamps(cursor, commits, stamps)
    files = (
        PureIterFactory.from_list(stamps)
        .map(lambda c: c.stamp)
        .bind(_extractor.extract_relations)
        .to_list()
    )
    msg_2 = Cmd.wrap_impure(lambda: LOG.info("Inserting %s file relations...", len(files)))
    insert_2: Cmd[None] = msg_2 + insert_files.insert_files(
        cursor,
        files_table,
        files,
    )
    return insert_1 + insert_2


def new_client(sql: SnowflakeCursor, commits: DbTableId, files: DbTableId) -> StampsClient:
    """
    Create a stamps client.

    [WARNING] take into account that this client uses a **common** SqlCursor
    for its methods. Therefore you may get **bugs** on iterated get requests
    i.e. group_data stream can generate interference over other methods execution.

    [TIP] Use an exclusive client for the stream method and/or use exclusive clients
    for each method (useful when executing concurrently)
    """
    table_client = ClientAdapter.snowflake_table_client_adapter(ClientFactory.new_table_client(sql))
    return StampsClient(
        init_commits_table=init_table.init_table(table_client, commits),
        init_files_table=files_init_table.init_table(table_client, files),
        count_items=lambda g: count.count_commits(sql, commits.schema, commits.table, _adapter(g)),
        most_recent_hash=lambda r: checkpoint.last_commit_hash(
            sql,
            commits.schema,
            commits.table,
            r,
        ),
        insert_stamps=lambda s: _insert(sql, commits, files, s),
        group_data=lambda g: get.group_data(sql, commits, _adapter(g), 200),
        amend_users=lambda p: amend_users.amend_users(sql, commits.schema, commits.table, p),
        get_stamp=lambda i: get_stamp.get_stamp(sql, commits.schema, commits.table, i),
        exist=lambda c: exist.stamp_exist(sql, commits, c),
    )
