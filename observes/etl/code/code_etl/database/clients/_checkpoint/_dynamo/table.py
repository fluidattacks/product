from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
)

import boto3
import boto3.session
from fa_purity import (
    Cmd,
    Maybe,
)
from mypy_boto3_dynamodb.service_resource import (
    Table as _Table,
)

from code_etl.database.clients._checkpoint._dynamo.get import (
    get as get_module,
)
from code_etl.database.clients._checkpoint._dynamo.update import (
    update as update_module,
)
from code_etl.objs import (
    Checkpoint,
    RepoId,
)


@dataclass(frozen=True)
class DynamoTableClient:
    """
    DynamoDb table client.

    This class is intended to be thread-safe.
    Every thread must have its own instance of the class.

    https://boto3.amazonaws.com/v1/documentation/api/latest/guide/resources.html
    #multithreading-or-multiprocessing-with-resources

    When using MultiUploader, each command to be executed must have instantiated
    a new instance of the class.
    """

    @dataclass(frozen=True)
    class _Private:
        pass

    _private: DynamoTableClient._Private
    _table: _Table

    @staticmethod
    def new(table_name: str) -> Cmd[DynamoTableClient]:
        return Cmd.wrap_impure(
            lambda: DynamoTableClient(
                DynamoTableClient._Private(),
                boto3.session.Session().resource("dynamodb").Table(table_name),
            ),
        )

    def update_or_put(self, item: Checkpoint) -> Cmd[None]:
        return update_module.update_checkpoint(self._table, item)

    def get(self, repo_id: RepoId) -> Cmd[Maybe[Checkpoint]]:
        return get_module.get_checkpoint(self._table, repo_id.group.name, repo_id.repository.name)

    def delete(self, repo_id: RepoId) -> Cmd[None]:
        def _action() -> None:
            self._table.delete_item(
                Key={
                    "group": repo_id.group.name,
                    "repository": repo_id.repository.name,
                },
                ReturnValues="NONE",
            )

        return Cmd.wrap_impure(_action)
