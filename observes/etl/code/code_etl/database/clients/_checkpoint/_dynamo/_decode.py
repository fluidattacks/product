import inspect
from datetime import (
    UTC,
    datetime,
)

from etl_utils.bug import (
    Bug,
)
from fa_purity import (
    Maybe,
    ResultE,
)
from fa_purity.date_time import (
    DatetimeFactory,
    DatetimeUTC,
)
from fa_purity.json import (
    JsonObj,
    JsonPrimitiveUnfolder,
    JsonUnfolder,
    Unfolder,
)

from code_etl.objs import (
    Checkpoint,
    CommitDataId,
    CommitId,
    GroupId,
    RepoId,
    RepoName,
)


def _decode_raw_item(item: JsonObj, group_name: str, repo_nickname: str) -> ResultE[Checkpoint]:
    inserted_at = JsonUnfolder.require(
        item,
        "inserted_at",
        lambda item_info: Unfolder.to_primitive(item_info)
        .bind(JsonPrimitiveUnfolder.to_str)
        .bind(
            lambda s: DatetimeUTC.assert_utc(
                DatetimeFactory.to_tz(datetime.fromisoformat(s), UTC),
            ),
        ),
    )

    is_initial = JsonUnfolder.require(
        item,
        "is_initial",
        lambda item_info: Unfolder.to_primitive(item_info).bind(JsonPrimitiveUnfolder.to_bool),
    )

    _hash = JsonUnfolder.require(
        item,
        "commit_hash",
        lambda item_info: Unfolder.to_primitive(item_info).bind(JsonPrimitiveUnfolder.to_str),
    )

    return inserted_at.bind(
        lambda i: is_initial.bind(
            lambda init: _hash.map(
                lambda h: Checkpoint(
                    commit_id=CommitDataId(
                        RepoId(GroupId(group_name), RepoName(repo_nickname)),
                        CommitId(h),
                    ),
                    inserted_at=i,
                    is_initial=init,
                ),
            ),
        ),
    )


def decode_checkpoint(
    raw: Maybe[JsonObj],
    group_name: str,
    repo_nickname: str,
) -> Maybe[Checkpoint]:
    return raw.map(
        lambda item: Bug.assume_success(
            "get_checkpoint_decode",
            inspect.currentframe(),
            (str(item), repo_nickname, group_name),
            _decode_raw_item(item, group_name, repo_nickname),
        ),
    )
