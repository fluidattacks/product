from datetime import (
    datetime,
)

from fa_purity import (
    ResultE,
    ResultFactory,
    cast_exception,
)
from fa_purity.date_time import (
    DatetimeFactory,
    DatetimeTZ,
    DatetimeUTC,
)
from redshift_client.sql_client import (
    DbPrimitive,
)


def assert_datetime(raw: DbPrimitive) -> ResultE[datetime]:
    _factory: ResultFactory[datetime, Exception] = ResultFactory()
    return raw.map(
        lambda _: _factory.failure(TypeError("expected 'datetime' got a `JsonPrimitive`")).alt(
            cast_exception,
        ),
        lambda d: _factory.success(d),
    )


def to_datetime_utc(raw: DbPrimitive) -> ResultE[DatetimeUTC]:
    return assert_datetime(raw).bind(
        lambda d: DatetimeTZ.assert_tz(d).map(lambda x: DatetimeFactory.to_utc(x)),
    )
