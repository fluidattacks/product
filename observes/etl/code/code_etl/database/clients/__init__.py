from __future__ import (
    annotations,
)

import logging
from dataclasses import (
    dataclass,
)

from fa_purity import (
    Cmd,
)
from redshift_client.core.id_objs import (
    DbTableId,
    Identifier,
    SchemaId,
    TableId,
)
from snowflake_client import (
    SnowflakeCursor,
)

from code_etl.database import (
    BillingClient,
    DbCheckpointClient,
)
from code_etl.database._core import (
    RegistrationClient,
    StampsClient,
)

from . import (
    _billing,
    _checkpoint,
    _registration,
    _stamps,
)

LOG = logging.getLogger(__name__)


@dataclass(frozen=True)
class CommitsTable:
    table_id: DbTableId


@dataclass(frozen=True)
class FilesTable:
    table_id: DbTableId


@dataclass(frozen=True)
class RegistrationTable:
    table_id: DbTableId


@dataclass(frozen=True)
class CheckpointTable:
    table_id: DbTableId


@dataclass(frozen=True)
class ProgressStartTable:
    table_id: DbTableId


@dataclass(frozen=True)
class ProgressEndTable:
    table_id: DbTableId


@dataclass(frozen=True)
class ClientFactory:
    _commits_table: CommitsTable
    _files_table: FilesTable
    _registrations_table: RegistrationTable
    _checkpoints: CheckpointTable
    _progress_start: ProgressStartTable
    _progress_end: ProgressEndTable

    @staticmethod
    def with_default_table_names(schema: SchemaId) -> ClientFactory:
        commits = TableId(Identifier.new("commits"))
        files = TableId(Identifier.new("files"))
        registrations = TableId(Identifier.new("registrations"))
        checkpoints = TableId(Identifier.new("observes_repos_checkpoints"))
        start_point = TableId(Identifier.new("observes_repos_start_progress"))
        end_point = TableId(Identifier.new("observes_repos_end_progress"))
        return ClientFactory(
            CommitsTable(DbTableId(schema, commits)),
            FilesTable(DbTableId(schema, files)),
            RegistrationTable(DbTableId(schema, registrations)),
            CheckpointTable(DbTableId(schema, checkpoints)),
            ProgressStartTable(DbTableId(schema, start_point)),
            ProgressEndTable(DbTableId(schema, end_point)),
        )

    @classmethod
    def production(cls) -> ClientFactory:
        schema = SchemaId(Identifier.new("code"))
        return cls.with_default_table_names(schema)

    def new_billing_client(
        self,
        cursor: SnowflakeCursor,
    ) -> BillingClient:
        return _billing.new_client(
            cursor,
            self._commits_table.table_id.schema,
            self._commits_table.table_id.table,
        )

    def new_registration_client(
        self,
        cursor: SnowflakeCursor,
    ) -> RegistrationClient:
        return _registration.new_client(self._registrations_table.table_id, cursor)

    def new_stamps_client(
        self,
        cursor: SnowflakeCursor,
    ) -> StampsClient:
        return _stamps.new_client(
            cursor,
            self._commits_table.table_id,
            self._files_table.table_id,
        )

    def new_dynamodb_checkpoint_client(self) -> Cmd[DbCheckpointClient]:
        return _checkpoint.new_dynamo_client(
            self._checkpoints.table_id.table.name.to_str(),
            self._progress_start.table_id.table.name.to_str(),
            self._progress_end.table_id.table.name.to_str(),
        )
