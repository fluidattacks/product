import inspect

from etl_utils.bug import (
    Bug,
)
from etl_utils.typing import (
    Dict,
    Tuple,
)
from fa_purity import (
    Cmd,
    FrozenTools,
)
from fa_purity.json import (
    Primitive,
)
from redshift_client.core.id_objs import (
    SchemaId,
    TableId,
)
from redshift_client.sql_client import (
    DbPrimitiveFactory,
    QueryValues,
)
from snowflake_client import (
    SnowflakeCursor,
    SnowflakeQuery,
)

from code_etl.objs import (
    RepoId,
)

from .init_table import (
    GROUP_COLUMN,
    REPO_COLUMN,
)


def _query(
    schema: SchemaId,
    table: TableId,
    repo: RepoId,
) -> Tuple[SnowflakeQuery, QueryValues]:
    statement = """
        SELECT 1
        FROM {schema}.{table}
        WHERE {group} = %(group)s
        AND {repo} = %(repo)s
    """
    identifiers: Dict[str, str] = {
        "group": GROUP_COLUMN.name.to_str(),
        "repo": REPO_COLUMN.name.to_str(),
        "schema": schema.name.to_str(),
        "table": table.name.to_str(),
    }
    args: Dict[str, Primitive] = {
        "group": repo.group.name,
        "repo": repo.repository.name,
    }
    query = Bug.assume_success(
        "registration_exist_query",
        inspect.currentframe(),
        (str(statement), str(identifiers)),
        SnowflakeQuery.dynamic_query(statement, FrozenTools.freeze(identifiers)),
    )
    return (
        query,
        QueryValues(DbPrimitiveFactory.from_raw_prim_dict(FrozenTools.freeze(args))),
    )


def registration_exist(
    cursor: SnowflakeCursor,
    schema: SchemaId,
    table: TableId,
    repo: RepoId,
) -> Cmd[bool]:
    request = cursor.execute(*_query(schema, table, repo)).map(
        lambda r: Bug.assume_success(
            "registration_exist_request",
            inspect.currentframe(),
            (str(schema), str(table), str(repo)),
            r,
        ),
    )
    return request + cursor.fetch_one.map(
        lambda r: Bug.assume_success(
            "registration_exist_fetch_result",
            inspect.currentframe(),
            (str(schema), str(table), str(repo)),
            r,
        ),
    ).map(lambda b: b.map(lambda _: True).value_or(False))
