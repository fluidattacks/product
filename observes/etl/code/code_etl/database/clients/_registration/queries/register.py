import inspect
import logging
from datetime import (
    datetime,
)

from etl_utils.bug import (
    Bug,
)
from etl_utils.typing import (
    Dict,
    Tuple,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    FrozenList,
    FrozenTools,
    PureIter,
    PureIterFactory,
)
from fa_purity.json import (
    Primitive,
)
from redshift_client.core.id_objs import (
    DbTableId,
)
from redshift_client.sql_client import (
    DbPrimitive,
    DbPrimitiveFactory,
    QueryValues,
)
from snowflake_client import (
    SnowflakeCursor,
    SnowflakeQuery,
)

from code_etl.objs import (
    RepoRegistration,
)

from .init_table import (
    GROUP_COLUMN,
    REPO_COLUMN,
    SEEN_AT_COLUMN,
)

LOG = logging.getLogger(__name__)


def _encode_registration(
    reg: RepoRegistration,
) -> FrozenDict[str, DbPrimitive]:
    args: Dict[str, Primitive | datetime] = {
        "group": reg.repo.group.name,
        "repository": reg.repo.repository.name,
        "seen_at": reg.seen_at.date_time,
    }
    return DbPrimitiveFactory.from_raw_dict(FrozenTools.freeze(args))


def _query(
    table: DbTableId,
    registrations: PureIter[RepoRegistration],
) -> Tuple[SnowflakeQuery, PureIter[QueryValues]]:
    statement = """
    MERGE INTO {schema}.{table}
        USING (
            SELECT * FROM (
                VALUES (%(group)s,%(repository)s,%(seen_at)s)
            ) virtual_table(
                {group_column},{repo_column},{seen_at_column}
            )
        ) source
    ON {table}.{group_column} = source.{group_column}
    AND {table}.{repo_column} = source.{repo_column}
        WHEN NOT MATCHED THEN INSERT ({group_column},{repo_column},{seen_at_column})
        VALUES (source.{group_column},source.{repo_column},source.{seen_at_column});
    """
    identifiers: Dict[str, str] = {
        "group_column": GROUP_COLUMN.name.to_str(),
        "repo_column": REPO_COLUMN.name.to_str(),
        "seen_at_column": SEEN_AT_COLUMN.name.to_str(),
        "schema": table.schema.name.to_str(),
        "table": table.table.name.to_str(),
    }
    query = Bug.assume_success(
        "register_query",
        inspect.currentframe(),
        (str(statement), str(identifiers)),
        SnowflakeQuery.dynamic_query(statement, FrozenTools.freeze(identifiers)),
    )
    return (
        query,
        registrations.map(lambda r: QueryValues(_encode_registration(r))),
    )


def register(
    cursor: SnowflakeCursor,
    table: DbTableId,
    registrations: FrozenList[RepoRegistration],
) -> Cmd[None]:
    query, values = _query(table, PureIterFactory.from_list(registrations))
    log_info = Cmd.wrap_impure(
        lambda: LOG.info("Registering repos: %s", str([r.repo for r in registrations])),
    )
    return log_info + cursor.batch(query, values.to_list()).map(
        lambda r: Bug.assume_success(
            "register_request",
            inspect.currentframe(),
            (str(table), str(registrations)),
            r,
        ),
    )
