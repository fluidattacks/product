import inspect

from etl_utils.bug import (
    Bug,
)
from fa_purity import (
    Cmd,
    FrozenDict,
)
from redshift_client.core.column import (
    Column,
)
from redshift_client.core.data_type.core import (
    DataType,
    PrecisionType,
    PrecisionTypes,
    StaticTypes,
)
from redshift_client.core.id_objs import (
    ColumnId,
    DbTableId,
    Identifier,
)
from redshift_client.core.table import (
    Table,
)
from redshift_client.sql_client import (
    DbPrimitiveFactory,
)
from snowflake_client import (
    TableClient,
)

GROUP_COLUMN = ColumnId(Identifier.new("group"))
REPO_COLUMN = ColumnId(Identifier.new("repository"))
SEEN_AT_COLUMN = ColumnId(Identifier.new("seen_at"))


def _var_char(limit: int) -> DataType:
    return DataType(PrecisionType(PrecisionTypes.VARCHAR, limit))


def table_definition() -> Table:
    none = DbPrimitiveFactory.from_raw(None)
    columns: FrozenDict[ColumnId, Column] = FrozenDict(
        {
            GROUP_COLUMN: Column(_var_char(64), False, none),
            REPO_COLUMN: Column(_var_char(4096), False, none),
            SEEN_AT_COLUMN: Column(DataType(StaticTypes.TIMESTAMPTZ), False, none),
        },
    )
    table = Table.new(
        (GROUP_COLUMN, REPO_COLUMN, SEEN_AT_COLUMN),
        columns,
        frozenset([GROUP_COLUMN, REPO_COLUMN]),
    )
    return Bug.assume_success("table_definition", inspect.currentframe(), (), table)


def init_table(client: TableClient, table_id: DbTableId) -> Cmd[None]:
    result = client.new_if_not_exist(
        table_id,
        table_definition(),
    )
    return result.map(
        lambda r: Bug.assume_success(
            "init_registration_table_execution",
            inspect.currentframe(),
            (),
            r,
        ),
    )
