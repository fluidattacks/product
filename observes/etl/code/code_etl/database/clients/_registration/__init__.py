import logging

from fa_purity._core.coproduct import (
    Coproduct,
)
from redshift_client.core.id_objs import (
    DbTableId,
)
from snowflake_client import (
    ClientFactory,
    SnowflakeCursor,
)

from code_etl.database import (
    All,
    RegistrationClient,
)
from code_etl.objs import (
    GroupId,
)

from .queries import (
    count,
    exist,
    get,
    init_table,
    register,
)

LOG = logging.getLogger(__name__)


def _adapter(item: GroupId | All) -> Coproduct[GroupId, All]:
    if isinstance(item, GroupId):
        return Coproduct.inl(item)
    return Coproduct.inr(item)


def new_client(table_id: DbTableId, cursor: SnowflakeCursor) -> RegistrationClient:
    _schema = table_id.schema
    _table = table_id.table
    return RegistrationClient(
        init_table.init_table(ClientFactory.new_table_client(cursor), table_id),
        lambda g: count.count_registrations(cursor, _schema, _table, _adapter(g)),
        lambda r: register.register(cursor, table_id, r),
        lambda g: get.stream_registrations(cursor, _schema, _table, _adapter(g)),
        lambda r: get.get_registration(cursor, table_id, r),
        lambda r: exist.registration_exist(cursor, _schema, _table, r),
    )
