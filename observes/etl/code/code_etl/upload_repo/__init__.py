import logging
from dataclasses import (
    dataclass,
)
from pathlib import (
    Path,
)

from etl_utils.parallel import (
    ThreadPool,
)
from etl_utils.typing import (
    Callable,
)
from fa_purity import (
    Cmd,
    Maybe,
    PureIter,
    ResultE,
)
from fa_purity.date_time import (
    DatetimeUTC,
)
from snowflake_client import (
    SnowflakeConnection,
    SnowflakeCursor,
)

from code_etl.integrates_api import (
    IntegratesClient,
    IntegratesClientFactory,
    IntegratesToken,
)
from code_etl.mailmap import (
    Mailmap,
)
from code_etl.objs import (
    GroupId,
    RepoId,
)
from code_etl.upload_repo._extractor import (
    RepoObj,
)

from ._uploader import (
    Uploader as _Uploader,
)

LOG = logging.getLogger(__name__)


@dataclass(frozen=True)
class _MultiUploader:
    _connection: SnowflakeConnection
    _token: IntegratesToken
    _group: GroupId
    _mailmap: Maybe[Mailmap]
    _date_now: DatetimeUTC
    _seen_at: DatetimeUTC

    def _new_clients(self, path: Path) -> Cmd[tuple[Cmd[SnowflakeCursor], IntegratesClient]]:
        new_sql = self._connection.cursor(LOG.getChild(str(path)))
        return IntegratesClientFactory.new_client(self._token).map(
            lambda integrates_client: (new_sql, integrates_client),
        )

    def _act_over_each_repo(
        self,
        repos: PureIter[ResultE[RepoObj]],
    ) -> Cmd[None]:
        def _warning(error: Exception) -> Cmd[None]:
            return Cmd.wrap_impure(lambda: LOG.error("%s", error))

        def _upload(
            clients: tuple[Cmd[SnowflakeCursor], IntegratesClient],
            repo: RepoObj,
        ) -> Cmd[None]:
            return _Uploader.new_with_default_clients(
                clients[0],
                clients[1],
                self._mailmap,
                self._date_now,
                self._seen_at,
                RepoId(self._group, repo.name),
                repo,
            ).bind(lambda u: u.full_upload)

        commands = repos.map(
            lambda r: r.map(
                lambda r_obj: self._new_clients(r_obj.path).bind(lambda c: _upload(c, r_obj)),
            )
            .alt(_warning)
            .to_union(),
        )
        return ThreadPool.new(5).bind(lambda p: p.in_threads_none(commands))

    def upload(self, repo_paths: PureIter[Path]) -> Cmd[None]:
        return self._act_over_each_repo(
            repo_paths.map(
                lambda p: RepoObj.to_repo(p).alt(lambda e: Exception(f"Invalid repo {p} i.e. {e}")),
            ),
        )



def get_uploader(
    connection: SnowflakeConnection,
    token: IntegratesToken,
    group: GroupId,
    mailmap: Maybe[Mailmap],
    date_now: DatetimeUTC,
    seen_at: DatetimeUTC,
) -> Callable[[PureIter[Path]], Cmd[None]]:
    return _MultiUploader(
            connection,
            token,
            group,
            mailmap,
            date_now,
            seen_at,
        ).upload
