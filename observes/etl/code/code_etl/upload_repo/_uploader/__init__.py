from __future__ import (
    annotations,
)

import logging
from dataclasses import (
    dataclass,
)

from etl_utils import (
    smash,
)
from fa_purity import (
    Cmd,
    CmdUnwrapper,
    Maybe,
    PureIter,
    PureIterFactory,
)
from fa_purity.date_time import (
    DatetimeUTC,
)
from snowflake_client import (
    SnowflakeCursor,
)

from code_etl.database import (
    DbCheckpointClient,
    RegistrationClient,
    StampsClient,
)
from code_etl.database.clients import (
    ClientFactory,
)
from code_etl.integrates_api import (
    IntegratesClient,
)
from code_etl.mailmap import (
    Mailmap,
)
from code_etl.objs import (
    Checkpoint,
    Commit,
    CommitDataId,
    CommitStamp,
    RepoContext,
    RepoId,
    RepoRegistration,
)
from code_etl.upload_repo._checkpoint import (
    CheckpointClient,
    EmittedCommitFactory,
    get_checkpoint,
)
from code_etl.upload_repo._extractor import (
    Extractor,
    RepoObj,
    new_extractor,
)
from code_etl.upload_repo._uploader._core import (
    State,
)
from code_etl.upload_repo._utils import (
    get_context,
)
from code_etl.upload_repo.actions import (
    upload_filtered_stamps,
)

LOG = logging.getLogger(__name__)

CHUNCK_SIZE_FOR_INSERTION = 2000


@dataclass(frozen=True)
class Uploader:
    _stamps_client: StampsClient
    _recorder: RegistrationClient
    _integrates_client: IntegratesClient
    _checkpoint_client: CheckpointClient
    _date_now: DatetimeUTC
    _seen_at_date: DatetimeUTC
    _extractor: Extractor
    _context: RepoContext

    @staticmethod
    def new(
        stamps_client: StampsClient,
        recorder: RegistrationClient,
        arm_client: IntegratesClient,
        db_checkpoint: DbCheckpointClient,
        mailmap: Maybe[Mailmap],
        date_now: DatetimeUTC,
        seen_at_date: DatetimeUTC,
        repo_id: RepoId,
        repo: RepoObj,
    ) -> Cmd[Uploader]:
        context = get_context(recorder, repo_id, date_now)
        return context.map(
            lambda ctx: Uploader(
                stamps_client,
                recorder,
                arm_client,
                CheckpointClient.new(db_checkpoint),
                date_now,
                seen_at_date,
                new_extractor(ctx, mailmap, date_now, stamps_client, repo),
                ctx,
            ),
        )

    @staticmethod
    def new_with_default_clients(
        new_sql: Cmd[SnowflakeCursor],
        arm_client: IntegratesClient,
        mailmap: Maybe[Mailmap],
        date_now: DatetimeUTC,
        seen_at_date: DatetimeUTC,
        repo_id: RepoId,
        repo: RepoObj,
    ) -> Cmd[Uploader]:
        factory = ClientFactory.production()
        new_stamps = new_sql.map(factory.new_stamps_client)
        new_recorder = new_sql.map(factory.new_registration_client)
        context = new_recorder.bind(lambda r: get_context(r, repo_id, date_now))
        new_db_checkpoint = factory.new_dynamodb_checkpoint_client()

        def _inner(
            context: RepoContext,
        ) -> Cmd[Uploader]:
            _new_extractor = new_stamps.map(
                lambda s: new_extractor(context, mailmap, date_now, s, repo),
            )
            return smash.smash_cmds_4(
                new_stamps,
                new_recorder,
                new_db_checkpoint,
                _new_extractor,
            ).map(
                lambda t: Uploader(
                    t[0],
                    t[1],
                    arm_client,
                    CheckpointClient.new(t[2]),
                    date_now,
                    seen_at_date,
                    t[3],
                    context,
                ),
            )

        return context.bind(_inner)

    def _to_stamps(
        self,
        commits: PureIter[Commit],
        end: Maybe[Checkpoint],
    ) -> PureIter[CommitStamp]:
        if self._context.is_new:
            return commits.map(lambda c: CommitStamp.initial_stamp(c, self._date_now))

        end_point = end.value_or(None)
        if end_point:
            return commits.map(
                lambda c: CommitStamp.initial_stamp(c, end_point.inserted_at)
                if end_point.is_initial
                else CommitStamp.normal_stamp(c, end_point.inserted_at, end_point.inserted_at),
            )

        # no new no end so we have arrive until head
        return commits.map(
            lambda c: CommitStamp.normal_stamp(c, self._seen_at_date, self._date_now),
        )

    def _process_commit(
        self,
        commit: Commit,
        state: State,
    ) -> Cmd[State]:
        if not state.start_processing:
            if commit.commit_id.hash == state.start.commit_id.hash:
                return Cmd.wrap_value(
                    State(
                        start=state.start,
                        end=state.end,
                        items=state.items,
                        start_found=False,
                        end_found=False,
                        upload=False,
                        start_processing=True,
                    ),
                )

            return Cmd.wrap_value(state)

        emmited = EmittedCommitFactory.new(self._stamps_client, commit.commit_id)

        if commit.commit_id.hash == self._extractor.head_commit_id:
            return emmited.bind(
                lambda cp: cp.map(
                    lambda emt: get_checkpoint(self._stamps_client, Maybe.some(emt)).map(
                        lambda c: State(
                            start=state.start,
                            end=c,
                            items=state.items,
                            start_found=state.start_found,
                            end_found=True,
                            upload=True,
                            start_processing=True,
                        ),
                    ),
                    lambda _: Cmd.wrap_value(
                        State(
                            start=state.start,
                            end=Maybe.empty(),
                            items=(*state.items, commit),
                            start_found=state.start_found,
                            end_found=state.end_found,
                            upload=True,
                            start_processing=True,
                        ),
                    ),
                ),
            )

        if not state.start_found:
            return emmited.bind(
                lambda cp: cp.map(
                    lambda _: self._checkpoint_client.upsert_checkpoint(
                        Checkpoint(
                            commit.commit_id,
                            self._context.is_new,
                            commit.data.committed_at,
                        ),
                        "upsert_start",
                    ).map(
                        lambda c: State(
                            start=c,
                            end=state.end,
                            items=state.items,
                            start_found=state.start_found,
                            end_found=state.end_found,
                            upload=False,
                            start_processing=True,
                        ),
                    ),
                    lambda _: Cmd.wrap_value(
                        State(
                            start=state.start,
                            end=state.end,
                            items=(*state.items, commit),
                            start_found=True,
                            end_found=state.end_found,
                            upload=False,
                            start_processing=True,
                        ),
                    ),
                ),
            )

        return emmited.bind(
            lambda cp: cp.map(
                lambda c: get_checkpoint(self._stamps_client, Maybe.some(c)).map(
                    lambda m: State(
                        start=state.start,
                        end=m,
                        items=state.items,
                        start_found=state.start_found,
                        end_found=True,
                        upload=True,
                        start_processing=True,
                    ),
                ),
                lambda nec: self._checkpoint_client.upsert_checkpoint(
                    Checkpoint(nec.commit_id, False, self._date_now),
                    "upsert_end",
                ).map(
                    lambda c: State(
                        start=state.start,
                        end=Maybe.some(c),
                        items=(*state.items, commit),
                        start_found=state.start_found,
                        end_found=False,
                        upload=False,
                        start_processing=True,
                    ),
                ),
            ),
        )

    def _process(self, start_point: Checkpoint) -> Cmd[None]:
        if start_point.commit_id.hash == self._extractor.head_commit_id:
            return Cmd.wrap_impure(
                lambda: LOG.info("Repo already up to date"),
            ) + self._checkpoint_client.set_main_checkpoint(
                Checkpoint(start_point.commit_id, False, self._date_now),
            )

        def _action(unwrapper: CmdUnwrapper) -> None:
            processing_from_init = (
                self._context.is_new or start_point.commit_id.hash == self._extractor.init_commit_id
            )
            state = State(
                start=start_point,
                end=Maybe.empty(),
                items=(),
                start_found=False,
                end_found=False,
                upload=False,
                start_processing=processing_from_init,
            )
            for commit in self._extractor.from_init:
                state = unwrapper.act(self._process_commit(commit, state))
                if state.upload or (
                    self._context.is_new and len(state.items) == CHUNCK_SIZE_FOR_INSERTION
                ):
                    unwrapper.act(self._upload(state))
                    state = State(
                        start=Checkpoint(
                            commit.commit_id,
                            self._context.is_new,
                            self._date_now,
                        ),
                        end=Maybe.empty(),
                        items=(),
                        start_found=False,
                        end_found=False,
                        upload=False,
                        start_processing=True,
                    )

        return Cmd.new_cmd(_action)

    @property
    def _register(self) -> Cmd[None]:
        info = Cmd.wrap_impure(
            lambda: LOG.info("Repo already registered i.e. `%s`", self._context.repo),
        )
        return (
            self._recorder.register((RepoRegistration(self._context.repo, self._date_now),))
            if self._context.is_new
            else info
        )

    def _upload(self, state: State) -> Cmd[None]:
        remove_start_point = self._checkpoint_client.remove_checkpoint(
            state.start,
            "remove_start",
        )

        end_checkpoint = state.end.value_or(None)
        remove_end_point: Cmd[None] = (
            self._checkpoint_client.remove_checkpoint(end_checkpoint, "remove_end")
            if end_checkpoint
            else Cmd.wrap_value(None)
        )

        head_checkpoint = Checkpoint(
            CommitDataId(self._context.repo, self._extractor.head_commit_id),
            self._context.is_new,
            self._date_now,
        )

        set_checkpoint = self._checkpoint_client.set_main_checkpoint(
            state.end.value_or(head_checkpoint),
        )

        return (
            upload_filtered_stamps(
                self._stamps_client,
                self._integrates_client,
                self._to_stamps(
                    PureIterFactory.from_list(state.items),
                    state.end,
                ),
                was_in_a_gap=state.end_found,
            )
            + remove_start_point
            + remove_end_point
            + set_checkpoint
        )

    @property
    def full_upload(
        self,
    ) -> Cmd[None]:
        init_chekcpoint = Checkpoint(
            CommitDataId(self._context.repo, self._extractor.init_commit_id),
            self._context.is_new,
            self._date_now,
        )

        get_start_checkpoint = self._checkpoint_client.get_start_progress(self._context.repo)

        get_main_checkpoint_point = self._checkpoint_client.get_main_checkpoint(
            self._context.repo,
        ).map(
            lambda m: m.to_result()
            .to_coproduct()
            .map(
                lambda c: c if self._extractor.exist(c.commit_id.hash) else init_chekcpoint,
                lambda _: init_chekcpoint,
            ),
        )

        _get_start_point = get_start_checkpoint.bind(
            lambda maybesc: get_main_checkpoint_point.map(
                lambda mainc: maybesc.to_result()
                .to_coproduct()
                .map(
                    lambda startc: startc
                    if self._extractor.exist(startc.commit_id.hash)
                    else mainc,
                    lambda _: mainc,
                ),
            ),
        )

        return _get_start_point.bind(lambda start: self._register + self._process(start))
