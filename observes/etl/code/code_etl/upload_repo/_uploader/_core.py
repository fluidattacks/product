from dataclasses import (
    dataclass,
)

from fa_purity import (
    FrozenList,
    Maybe,
)

from code_etl.objs import (
    Checkpoint,
    Commit,
)


@dataclass(frozen=True)
class State:
    start: Checkpoint
    end: Maybe[Checkpoint]
    items: FrozenList[Commit]
    start_found: bool
    end_found: bool
    upload: bool
    start_processing: bool
