import inspect
import logging

from etl_utils.bug import (
    Bug,
)
from fa_purity import (
    Cmd,
    PureIter,
    StreamFactory,
    StreamTransform,
)

from code_etl._utils import (
    log_info,
)
from code_etl.database import (
    StampsClient,
)
from code_etl.integrates_api import (
    IntegratesClient,
)
from code_etl.objs import (
    CommitStamp,
    DbCommitStamp,
)

from ._ignored import (
    IgnoredFilter,
)

LOG = logging.getLogger(__name__)


def ignored_as_init_commits(client: IntegratesClient, stamp: CommitStamp) -> Cmd[CommitStamp]:
    ignored_paths = client.get_ignored_paths(stamp.commit.commit_id.repo)
    return ignored_paths.map(
        lambda r: Bug.assume_success("ignored_paths", inspect.currentframe(), (str(stamp),), r),
    ).map(
        lambda i: IgnoredFilter(i)
        .filter_stamp(stamp)
        .or_else_call(lambda: CommitStamp.initial_stamp(stamp.commit, stamp.inserted_at)),
    )


def upload_filtered_stamps(
    client: StampsClient,
    arm_client: IntegratesClient,
    stamps: PureIter[CommitStamp],
    was_in_a_gap: bool,
) -> Cmd[None]:
    start_msg = log_info(LOG, "Uploading filtered stamps STARTED")
    end_msg = log_info(LOG, "Uploading filtered stamps ENDED")

    _stamps = stamps.map(lambda s: ignored_as_init_commits(arm_client, s)).transform(
        lambda x: StreamFactory.from_commands(x),
    )
    actions = (
        _stamps.map(lambda c: DbCommitStamp(c, was_in_a_gap))
        .chunked(2000)
        .map(
            lambda st: client.insert_stamps(st) + log_info(LOG, "Inserted %s stamps", str(len(st))),
        )
    )
    return start_msg + StreamTransform.consume(actions) + end_msg
