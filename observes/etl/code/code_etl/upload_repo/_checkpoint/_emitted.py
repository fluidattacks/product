from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
    field,
)

from fa_purity import (
    Cmd,
)
from fa_purity._core.coproduct import (
    Coproduct,
)

from code_etl.database import (
    StampsClient,
)
from code_etl.objs import (
    CommitDataId,
)


@dataclass(frozen=True)
class _Private:
    pass


@dataclass(frozen=True)
class EmittedCommitId:
    _private: _Private = field(repr=False, hash=False, compare=False)
    stamps: StampsClient
    commit_id: CommitDataId


@dataclass(frozen=True)
class NotEmittedCommitId:
    _private: _Private = field(repr=False, hash=False, compare=False)
    stamps: StampsClient
    commit_id: CommitDataId


@dataclass(frozen=True)
class EmittedCommitFactory:
    @staticmethod
    def new(
        stamps: StampsClient,
        commit_id: CommitDataId,
    ) -> Cmd[Coproduct[EmittedCommitId, NotEmittedCommitId]]:
        return stamps.exist(commit_id).map(
            lambda b: Coproduct.inl(EmittedCommitId(_Private(), stamps, commit_id))
            if b
            else Coproduct.inr(NotEmittedCommitId(_Private(), stamps, commit_id)),
        )
