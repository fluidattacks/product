import inspect
from datetime import datetime
from pathlib import (
    Path,
)

import click
from etl_utils.bug import (
    Bug,
)
from etl_utils.parallel import (
    ThreadPool,
)
from etl_utils.typing import (
    NoReturn,
)
from fa_purity import (
    Cmd,
    FrozenList,
    Maybe,
    PureIterFactory,
    Result,
)
from fa_purity.date_time import (
    DatetimeFactory,
    DatetimeUTC,
)
from utils_logger import (
    start_session,
)

from code_etl import (
    amend,
)
from code_etl.compute_bills import (
    main as bill_reports,
)
from code_etl.integrates_api import (
    IntegratesClientFactory,
    IntegratesToken,
)
from code_etl.objs import (
    GroupId,
)
from code_etl.upload_repo import (
    get_uploader,
)

from ._connection_utils import (
    with_connection,
)
from ._utils import (
    Date,
)

mailmap_file = click.Path(
    exists=True,
    file_okay=True,
    dir_okay=False,
    writable=False,
    readable=True,
    resolve_path=True,
    allow_dash=False,
    path_type=str,
)


@click.command()
@click.option("--integrates-token", type=str, required=True)
@click.option("--namespace", type=str, required=True)
def amend_authors_snowflake(
    integrates_token: str,
    namespace: str,
) -> NoReturn:
    mailmap = IntegratesClientFactory.new_client(IntegratesToken.new(integrates_token)).bind(
        lambda c: c.get_group_mailmap(GroupId(namespace)).map(
            lambda r: Bug.assume_success("group_mailmap", inspect.currentframe(), (namespace,), r),
        ),
    )
    cmd: Cmd[None] = start_session() + ThreadPool.new(10).bind(
        lambda pool: mailmap.bind(
            lambda m: with_connection(
                lambda c: amend.amend_users(
                    c.connection,
                    GroupId(namespace),
                    Maybe.some(m),
                    pool,
                ).map(lambda n: Result.success(n)),
            ).map(
                lambda r: Bug.assume_success(
                    "amend_authors_snowflake",
                    inspect.currentframe(),
                    (),
                    r,
                ),
            ),
        ),
    )
    cmd.compute()


@click.command()
@click.argument("folder", type=str)
@click.argument("year", type=int)
@click.argument("month", type=int)
@click.argument("integrates_token", type=str)
def compute_bills(
    folder: str,
    year: int,
    month: int,
    integrates_token: str,
) -> NoReturn:
    token = IntegratesToken.new(integrates_token)
    cmd: Cmd[None] = start_session() + with_connection(
        lambda c: IntegratesClientFactory.new_client(token)
        .bind(
            lambda ic: bill_reports(
                c.connection,
                ic,
                Path(folder),
                Bug.assume_success(
                    "compute_bills_input_date",
                    inspect.currentframe(),
                    (str(year), str(month)),
                    Date.new(year, month, 1),
                ),
            ),
        )
        .map(lambda n: Result.success(n)),
    ).map(lambda r: Bug.assume_success("compute_bills", inspect.currentframe(), (), r))
    cmd.compute()


@click.command()
@click.option("--namespace", type=str, required=True)
@click.option("--integrates-token", type=str, required=True)
@click.option("--seen-at", type=str, required=True)
@click.argument("repositories", type=str, nargs=-1)
def upload_code_snowflake(
    namespace: str,
    integrates_token: str,
    seen_at: str,
    repositories: FrozenList[str],
) -> NoReturn:
    repos = PureIterFactory.from_list(repositories).map(lambda r: Path(r).resolve())
    token = IntegratesToken.new(integrates_token)
    mailmap = IntegratesClientFactory.new_client(IntegratesToken.new(integrates_token)).bind(
        lambda c: c.get_group_mailmap(GroupId(namespace)).map(
            lambda r: Bug.assume_success("group_mailmap", inspect.currentframe(), (namespace,), r),
        ),
    )
    seen_at_date = Bug.assume_success(
        "seen_at",
        inspect.currentframe(),
        (seen_at,),
        DatetimeUTC.assert_utc(datetime.fromisoformat(seen_at.strip())),
    )

    cmd = start_session() + mailmap.bind(
        lambda m: with_connection(
            lambda c: DatetimeFactory.date_now()
            .bind(
                lambda now: get_uploader(
                    c.connection,
                    token,
                    GroupId(namespace),
                    Maybe.some(m),
                    now,
                    seen_at_date,
                )(repos),
            )
            .map(lambda n: Result.success(n)),
        ).map(
            lambda r: Bug.assume_success(
                "upload_code_snowflake",
                inspect.currentframe(),
                (),
                r,
            ),
        ),
    )
    cmd.compute()


@click.group()
def main() -> None:
    # cli main entrypoint
    pass


main.add_command(compute_bills)
main.add_command(amend_authors_snowflake)
main.add_command(upload_code_snowflake)
