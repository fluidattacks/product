import logging

from etl_utils.parallel import (
    ThreadPool,
)
from fa_purity import (
    Cmd,
    Maybe,
    Stream,
    StreamTransform,
)

from code_etl.database import (
    StampsClient,
    UsersPatch,
)
from code_etl.mailmap import (
    Mailmap,
)
from code_etl.objs import (
    CommitStamp,
)

from .core import (
    AmendUsers,
)

LOG = logging.getLogger(__name__)


def _users_patch(
    mailmap: Maybe[Mailmap],
    item: CommitStamp,
) -> Maybe[UsersPatch]:
    return mailmap.map(AmendUsers).bind(lambda a: a.get_users_patch(item))


def amend_in_threads(
    pool: ThreadPool,
    new_client: Cmd[StampsClient],
    mailmap: Maybe[Mailmap],
    data: Stream[CommitStamp],
) -> Cmd[None]:
    mutation_msg = Cmd.wrap_impure(lambda: LOG.info("Mutation `amend_users` started"))
    patches = data.map(lambda s: _users_patch(mailmap, s)).transform(
        lambda s: StreamTransform.filter_maybe(s),
    )
    commands = patches.chunked(2000).map(lambda s: new_client.bind(lambda c: c.amend_users(s)))
    return mutation_msg + pool.in_threads_none(commands)
