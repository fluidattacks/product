import logging

from etl_utils.parallel import (
    ThreadPool,
)
from fa_purity import (
    Cmd,
    Maybe,
)
from snowflake_client import (
    SnowflakeConnection,
)

from code_etl.database.clients import (
    ClientFactory,
)
from code_etl.mailmap import (
    Mailmap,
)
from code_etl.objs import (
    GroupId,
)

from . import (
    actions,
)

LOG = logging.getLogger(__name__)


def amend_users(
    connection: SnowflakeConnection,
    group: GroupId,
    mailmap: Maybe[Mailmap],
    pool: ThreadPool,
) -> Cmd[None]:
    factory = ClientFactory.production()
    fetch_client = connection.cursor(LOG.getChild("fetch_client_" + group.name)).map(
        factory.new_stamps_client,
    )
    action_client = connection.cursor(LOG.getChild("action_client_" + group.name)).map(
        factory.new_stamps_client,
    )
    return fetch_client.bind(lambda c: c.group_data(group)).bind(
        lambda data: actions.amend_in_threads(pool, action_client, mailmap, data),
    )
