python_pkgs:
let
  get_or_default = attrs: key: default:
    if builtins.hasAttr key attrs then builtins.getAttr key attrs else default;
in python_pkgs.gql.overridePythonAttrs (old: {
  doCheck = false;
  propagatedBuildInputs = (get_or_default old "propagatedBuildInputs" [ ])
    ++ [ python_pkgs.requests-toolbelt ];
})
