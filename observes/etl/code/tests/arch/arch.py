from arch_lint.dag import (
    DagMap,
)
from arch_lint.graph import (
    FullPathModule,
)
from etl_utils.typing import (
    Dict,
    FrozenSet,
)
from fa_purity import (
    FrozenList,
)

root = FullPathModule.assert_module("code_etl")
_dag: Dict[str, FrozenList[FrozenList[str] | str]] = {
    "code_etl": (
        "cli",
        ("migrations", "jobs"),
        ("compute_bills", "upload_repo"),
        "amend",
        ("database", "integrates_api"),
        "mailmap",
        "_connection_utils",
        "objs",
        (
            "_logger",
            "_utils",
            "str_utils",
        ),
    ),
    "code_etl.jobs._utils": (("batch", "upload", "amend"),),
    "code_etl.compute_bills": (
        ("_keeper", "_report"),
        (
            "_get_org",
            "core",
        ),
    ),
    "code_etl.database.clients": (
        ("_checkpoint", "_registration", "_stamps", "_billing"),
        "_utils",
    ),
    "code_etl.database.clients._checkpoint": (("_dynamo"),),
    "code_etl.database.clients._checkpoint._dynamo": (
        "table",
        ("get", "update"),
        "_decode",
        "encode",
    ),
    "code_etl.database.clients._checkpoint._dynamo.get": ("get",),
    "code_etl.database.clients._checkpoint._dynamo.update": ("update",),
    "code_etl.database.clients._stamps": (("_extractor", "queries"),),
    "code_etl.database.clients._stamps.queries": (
        (
            "file_commit",
            "amend_users",
            "get",
            "exist",
            "insert_stamps",
            "checkpoint",
            "count",
        ),
        "init_table",
    ),
    "code_etl.database.clients._stamps.queries.file_commit": (
        "insert",
        "init_table",
    ),
    "code_etl.database.clients._stamps.queries.get": ("get_stamp", "_decode"),
    "code_etl.database.clients._registration.queries": (
        ("get", "exist", "register", "count"),
        "init_table",
    ),
    "code_etl.database.clients._registration.queries.get": (
        ("_stream", "_single"),
        "_decode",
    ),
    "code_etl.database": (
        "clients",
        "_core",
        "_delta_update",
    ),
    "code_etl.amend": (
        "actions",
        "core",
    ),
    "code_etl.integrates_api": (
        (
            "_ignored_paths",
            "_group_org",
            "_roots",
            "_mailmap",
            "_group_mailmap",
        ),
        "_raw_client",
        "_retry",
        ("_core", "_error"),
    ),
    "code_etl.integrates_api._retry": (
        ("delay", "handlers"),
        "_core",
    ),
    "code_etl.upload_repo": (
        "_uploader",
        "actions",
        "_checkpoint",
        ("_extractor", "_ignored"),
        "_utils",
    ),
    "code_etl.upload_repo._uploader": ("_core",),
    "code_etl.upload_repo._extractor": (
        "_extractor_1",
        ("_factory", "_core"),
    ),
    "code_etl.upload_repo._checkpoint": (
        "_checkpoint",
        ("_emitted"),
    ),
    "code_etl.migrations": ("inserted_at",),
    "code_etl._utils": (("cache", "ratelimit"),),
}


def project_dag() -> DagMap:
    item = DagMap.new(_dag)
    if isinstance(item, Exception):
        raise item
    return item


def forbidden_allowlist() -> Dict[FullPathModule, FrozenSet[FullPathModule]]:
    _raw: Dict[str, FrozenSet[str]] = {
        "git": frozenset(
            {
                "code_etl.upload_repo._extractor",
            },
        ),
    }
    return {
        FullPathModule.assert_module(k): frozenset(FullPathModule.assert_module(i) for i in v)
        for k, v in _raw.items()
    }
