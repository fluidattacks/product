from fa_purity.json import (
    JsonValueFactory,
    Unfolder,
)

API_GET_GITIGNORE_RESULT = JsonValueFactory.from_any(
    {
        "group": {
            "gitRoots": {
                "edges": [
                    {
                        "node": {
                            "nickname": "test_repo",
                            "branch": "main",
                            "gitignore": [
                                "module1/test/",
                                "asserts/",
                                "module2/",
                                "module3/db/dynamodb/infra/main.tf",
                            ],
                            "id": "1509c42d-599c-47b9-a55c-7a73c2ac1534",
                        },
                    },
                ],
            },
        },
    },
).bind(Unfolder.to_json)
