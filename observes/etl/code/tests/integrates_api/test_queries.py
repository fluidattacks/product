import inspect

from etl_utils.bug import (
    Bug,
)
from fa_purity import (
    FrozenList,
    PureIterFactory,
)

from code_etl.integrates_api._core import (
    IgnoredPath,
)
from code_etl.integrates_api._ignored_paths import (
    _decode_raw_ignored_paths,
)
from code_etl.objs import (
    GroupId,
)
from tests.integrates_api._utils import (
    API_GET_GITIGNORE_RESULT,
)


def _assert_decode_ignored_paths(paths: FrozenList[IgnoredPath]) -> None:
    ignores = (
        "module1/test/",
        "asserts/",
        "module2/",
        "module3/db/dynamodb/infra/main.tf",
    )

    assert PureIterFactory.from_list(paths).map(lambda p: p.file_path).to_list() == ignores


def test_decode_ignored_paths() -> None:
    Bug.assume_success(
        "test",
        inspect.currentframe(),
        (),
        API_GET_GITIGNORE_RESULT.bind(
            lambda api_json: _decode_raw_ignored_paths(
                api_json,
                GroupId("group_name"),
            )
            .map(lambda v: _assert_decode_ignored_paths(v))
            .alt(lambda e: Exception(e.description)),
        ),
    )
