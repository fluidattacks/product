import re
from datetime import (
    UTC,
)

from etl_utils.typing import (
    List,
    NoReturn,
)
from fa_purity import (
    FrozenDict,
    PureIterFactory,
    Unsafe,
)
from fa_purity.date_time import (
    DatetimeFactory,
    DatetimeUTC,
    RawDatetime,
)
from redshift_client.core.table import (
    Table,
)
from redshift_client.sql_client import (
    DbPrimitive,
    QueryValues,
)
from snowflake_client import (
    SnowflakeQuery,
)


def encode_obj_completeness(
    table: Table,
    encoded_data: FrozenDict[str, DbPrimitive],
) -> None | NoReturn:
    columns = (
        PureIterFactory.from_list(tuple(table.columns.keys()))
        .map(lambda c: c.name.to_str())
        .transform(lambda p: frozenset(p))
    )
    encoded_keys = encoded_data.keys()
    # All table columns present?
    for k in columns:
        assert k in encoded_keys
    # All encoded columns belongs to the table columns?
    for k in encoded_keys:
        assert k in columns
    return None


def check_query_replacements(query: SnowflakeQuery, values: QueryValues) -> None | NoReturn:
    template = str(query.statement)
    _placeholders: List[str] = re.findall(r"%\((\w+)\)s", template)
    placeholders = frozenset(_placeholders)
    for placeholder in placeholders:
        if placeholder not in values.values:  # noqa: PD011 False positive: pandas is not even used
            msg = f"value placeholder `%({placeholder})s` not mapped"
            raise KeyError(msg)
    return None


def assert_new_utc(
    year: int,
    month: int,
    day: int,
    hour: int,
    minute: int,
    second: int,
    microsecond: int,
) -> DatetimeUTC | NoReturn:
    return (
        DatetimeFactory.new_utc(
            RawDatetime(
                year=year,
                month=month,
                day=day,
                hour=hour,
                minute=minute,
                second=second,
                microsecond=microsecond,
                time_zone=UTC,
            ),
        )
        .alt(Unsafe.raise_exception)
        .to_union()
    )
