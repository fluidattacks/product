import pytest
from etl_utils.typing import (
    Callable,
)
from fa_purity import (
    Cmd,
    Maybe,
    PureIterFactory,
)

import tests.mock.client as mock_client
from code_etl.objs import (
    CommitDataId,
    CommitId,
    RepoContext,
)
from code_etl.upload_repo._extractor import (
    Extractor,
    new_extractor,
)
from tests.mock import (
    MOCK_DATE_NOW,
)
from tests.mock.db import (
    MockDB,
)
from tests.mock.repo import (
    REPO_ID,
    mock_repo,
)


def _test_with_empty_db(test: Callable[[MockDB], None]) -> None:
    cmd: Cmd[None] = MockDB.empty().map(test)
    with pytest.raises(SystemExit):
        cmd.compute()


def _extractor(db: MockDB) -> Extractor:
    context = RepoContext(REPO_ID, "1" * 40, True)
    return new_extractor(
        context,
        Maybe.empty(),
        MOCK_DATE_NOW,
        mock_client.mock_client_stamps(db),
        mock_repo(),
    )


def _test_from_init(db: MockDB) -> None:
    all_commits = (
        "9f1d958f408ad38235109cccc6761fe4e8696265",
        "0d38170b0322135ca88caf0473ba42f8b0784cc0",
        "bea34d1e06036684d9b07061571a861da48d0aa5",
        "07b8f7d2ff61e3abf3714cae56c06cd1460ef582",
        "6745988f8e924f7e181dbe8fa655f86dd32d6f27",
        "ce34f18fcbacd496cc536a3304baa020d7f65975",
        "9237bd6e4bc20ad40189a6ff7be217ec787bbda8",
        "285eb29c2403e415d51fc5ec4a59b91b4df26fda",
        "159c3cfa98291d3e9f1e38096760994d481b2772",
        "00240fa1383895859bfade3d24efb9afb1eecf8b",
        "0e17f6e9966b0cf0804c6892232c35d86c02be30",
        "db98922d63b6fe5c3e443ed29fe85804f6a634f6",
    )
    assert (
        _extractor(db).from_init.map(lambda c: c.commit_id).to_list()
        == PureIterFactory.from_list(all_commits[::-1])
        .map(lambda h: CommitDataId(REPO_ID, CommitId(h)))
        .to_list()
    )


def test_from_init() -> None:
    _test_with_empty_db(_test_from_init)


def _test_exist(db: MockDB) -> None:
    existent = CommitId("db98922d63b6fe5c3e443ed29fe85804f6a634f6")
    non_existent = CommitId("1" * 40)
    assert _extractor(db).exist(existent)
    assert not _extractor(db).exist(non_existent)


def test_exist() -> None:
    _test_with_empty_db(_test_exist)


def _test_ids_after(db: MockDB) -> None:
    expected_hashes = (
        "bea34d1e06036684d9b07061571a861da48d0aa5",
        "0d38170b0322135ca88caf0473ba42f8b0784cc0",
        "9f1d958f408ad38235109cccc6761fe4e8696265",
    )
    assert (
        _extractor(db).ids_after(CommitId("07b8f7d2ff61e3abf3714cae56c06cd1460ef582")).to_list()
        == PureIterFactory.from_list(expected_hashes)
        .map(lambda h: CommitDataId(REPO_ID, CommitId(h)))
        .to_list()
    )


def test_ids_after() -> None:
    _test_with_empty_db(_test_ids_after)


def test_init_commit() -> None:
    def _test(db: MockDB) -> None:
        assert _extractor(db).init_commit_id == CommitId("db98922d63b6fe5c3e443ed29fe85804f6a634f6")

    _test_with_empty_db(_test)


def test_head_commit() -> None:
    def _test(db: MockDB) -> None:
        assert _extractor(db).head_commit_id == CommitId("9f1d958f408ad38235109cccc6761fe4e8696265")

    _test_with_empty_db(_test)


def _test_commits_between(db: MockDB) -> None:
    expected_hashes = (
        "ce34f18fcbacd496cc536a3304baa020d7f65975",
        "6745988f8e924f7e181dbe8fa655f86dd32d6f27",
        "07b8f7d2ff61e3abf3714cae56c06cd1460ef582",
    )
    start = CommitId("9237bd6e4bc20ad40189a6ff7be217ec787bbda8")
    end = CommitId("bea34d1e06036684d9b07061571a861da48d0aa5")
    result = _extractor(db).commits_between(start, end).map(lambda c: c.commit_id).to_list()
    expected = (
        PureIterFactory.from_list(expected_hashes)
        .map(lambda h: CommitDataId(REPO_ID, CommitId(h)))
        .to_list()
    )
    assert result == expected


def test_commits_between() -> None:
    _test_with_empty_db(_test_commits_between)
