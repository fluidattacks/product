import pytest
from etl_utils.typing import (
    Dict,
    List,
)
from fa_purity import (
    FrozenDict,
    Unsafe,
)
from fa_purity.json import (
    JsonValueFactory,
    Unfolder,
)

from code_etl.integrates_api._mailmap import (
    _decode_mailmap,
)
from code_etl.mailmap import (
    Mailmap,
    MailmapFactory,
    MailmapItem,
)
from code_etl.objs import (
    User,
)


@pytest.mark.parametrize(
    ("canon", "aliases"),
    [
        (
            User("Mallory Santos", "mallory@entry.com"),
            [
                User("Mallory Santos", "mallory@subentry.com"),
                User("Mallory N Santos", "mallory-n@subentry.com"),
            ],
        ),
    ],
)
def test_from_lines(canon: User, aliases: List[User]) -> None:
    mailmap_items = (MailmapItem(canon, alias) for alias in aliases)
    mailmap = MailmapFactory.from_lines(tuple(item.encode() for item in mailmap_items))
    for alias in aliases:
        assert mailmap.alias_map[alias] == canon


@pytest.mark.parametrize(
    ("input_mailmap", "decoded_mailmap"),
    [
        (
            {
                "mailmapEntriesWithSubentries": [
                    {
                        "entry": {
                            "mailmapEntryName": "Alice Garcia",
                            "mailmapEntryEmail": "alice@entry.com",
                        },
                        "subentries": [
                            {
                                "mailmapSubentryName": "Alice Garcia",
                                "mailmapSubentryEmail": "alice@subentry.com",
                            },
                            {
                                "mailmapSubentryName": "Alice B Garcia",
                                "mailmapSubentryEmail": "alice-b@subentry.com",
                            },
                        ],
                    },
                    {
                        "entry": {
                            "mailmapEntryName": "Bob Singh",
                            "mailmapEntryEmail": "bob@entry.com",
                        },
                        "subentries": [
                            {
                                "mailmapSubentryName": "Bob Singh",
                                "mailmapSubentryEmail": "bob@subentry.com",
                            },
                            {
                                "mailmapSubentryName": "Bob C Singh",
                                "mailmapSubentryEmail": "bob-c@subentry.com",
                            },
                        ],
                    },
                    {
                        "entry": {
                            "mailmapEntryName": "Eve Anderson",
                            "mailmapEntryEmail": "eve@entry.com",
                        },
                        "subentries": [
                            {
                                "mailmapSubentryName": "Eve Anderson",
                                "mailmapSubentryEmail": "eve@subentry.com",
                            },
                            {
                                "mailmapSubentryName": "Eve F Anderson",
                                "mailmapSubentryEmail": "eve-f@subentry.com",
                            },
                        ],
                    },
                    {
                        "entry": {
                            "mailmapEntryName": "Jane Doe",
                            "mailmapEntryEmail": "jane@entry.com",
                        },
                        "subentries": [
                            {
                                "mailmapSubentryName": "Jane K Doe",
                                "mailmapSubentryEmail": "jane-k@subentry.com",
                            },
                        ],
                    },
                ],
            },
            Mailmap(
                FrozenDict(
                    {
                        User("Alice Garcia", "alice@subentry.com"): User(
                            "Alice Garcia",
                            "alice@entry.com",
                        ),
                        User("Alice B Garcia", "alice-b@subentry.com"): User(
                            "Alice Garcia",
                            "alice@entry.com",
                        ),
                        User("Bob Singh", "bob@subentry.com"): User("Bob Singh", "bob@entry.com"),
                        User("Bob C Singh", "bob-c@subentry.com"): User(
                            "Bob Singh",
                            "bob@entry.com",
                        ),
                        User("Eve Anderson", "eve@subentry.com"): User(
                            "Eve Anderson",
                            "eve@entry.com",
                        ),
                        User("Eve F Anderson", "eve-f@subentry.com"): User(
                            "Eve Anderson",
                            "eve@entry.com",
                        ),
                        User("Jane K Doe", "jane-k@subentry.com"): User(
                            "Jane Doe",
                            "jane@entry.com",
                        ),
                    },
                ),
            ),
        ),
    ],
)
def test_decode_mailmap(
    input_mailmap: Dict[str, List[Dict[str, Dict[str, str] | Dict[str, List[Dict[str, str]]]]]],
    decoded_mailmap: Mailmap,
) -> None:
    raw = (
        JsonValueFactory.from_any(input_mailmap)
        .bind(Unfolder.to_json)
        .alt(Unsafe.raise_exception)
        .to_union()
    )
    _decoded_mailmap = _decode_mailmap(raw).alt(Unsafe.raise_exception).to_union()
    assert _decoded_mailmap == decoded_mailmap
