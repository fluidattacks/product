from __future__ import (
    annotations,
)

from collections.abc import (
    Iterable,
)
from dataclasses import (
    dataclass,
)

from etl_utils.typing import (
    Dict,
    Generic,
    Tuple,
    TypeVar,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    Maybe,
    Stream,
    Unsafe,
)

_K = TypeVar("_K")
_V = TypeVar("_V")


@dataclass(frozen=True)
class MutableMap(Generic[_K, _V]):
    _items: Dict[_K, _V]

    @staticmethod
    def new() -> Cmd[MutableMap[_K, _V]]:
        return Cmd.wrap_impure(lambda: MutableMap({}))

    @staticmethod
    def initialize(data: FrozenDict[_K, _V]) -> Cmd[MutableMap[_K, _V]]:
        return Cmd.wrap_impure(lambda: MutableMap(dict(data)))

    def stream(self) -> Stream[Tuple[_K, _V]]:
        def action() -> Iterable[Tuple[_K, _V]]:
            return iter(self._items.items())

        return Unsafe.stream_from_cmd(Cmd.wrap_impure(action))

    def freeze(self) -> Cmd[FrozenDict[_K, _V]]:
        return Cmd.wrap_impure(lambda: FrozenDict(self._items))

    def get(self, key: _K) -> Cmd[Maybe[_V]]:
        def action() -> Maybe[_V]:
            if key in self._items:
                return Maybe.some(self._items[key])
            return Maybe.empty()

        return Cmd.wrap_impure(action)

    def delete_if_exist(self, key: _K) -> Cmd[None]:
        def action() -> None:
            if key in self._items:
                self._items.pop(key)

        return Cmd.wrap_impure(action)

    def set(self, key: _K, value: _V) -> Cmd[None]:
        def action() -> None:
            self._items[key] = value

        return Cmd.wrap_impure(action)
