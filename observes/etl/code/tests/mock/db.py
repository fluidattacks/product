from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
)

from etl_utils import (
    smash,
)
from fa_purity import (
    Cmd,
    FrozenDict,
)

from code_etl.objs import (
    Checkpoint,
    CommitDataId,
    DbCommitStamp,
    RepoId,
    RepoRegistration,
)

from .mutable_dict import (
    MutableMap,
)


@dataclass(frozen=True)
class MockDB:
    registrations: MutableMap[RepoId, RepoRegistration]
    stamps: MutableMap[CommitDataId, DbCommitStamp]
    checkpoints: MutableMap[RepoId, Checkpoint]
    start_progress: MutableMap[RepoId, Checkpoint]
    end_progress: MutableMap[RepoId, Checkpoint]

    @staticmethod
    def initialize(
        registrations: FrozenDict[RepoId, RepoRegistration],
        stamps: FrozenDict[CommitDataId, DbCommitStamp],
        checkpoints: FrozenDict[RepoId, Checkpoint],
        start_progress: FrozenDict[RepoId, Checkpoint],
        end_progress: FrozenDict[RepoId, Checkpoint],
    ) -> Cmd[MockDB]:
        return smash.smash_cmds_5(
            MutableMap.initialize(registrations),
            MutableMap.initialize(stamps),
            MutableMap.initialize(checkpoints),
            MutableMap.initialize(start_progress),
            MutableMap.initialize(end_progress),
        ).map(lambda t: MockDB(*t))

    @classmethod
    def empty(cls) -> Cmd[MockDB]:
        return cls.initialize(
            FrozenDict({}),
            FrozenDict({}),
            FrozenDict({}),
            FrozenDict({}),
            FrozenDict({}),
        )
