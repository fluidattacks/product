from tests import (
    _utils,
)

MOCK_DATE_NOW = _utils.assert_new_utc(
    year=2024,
    month=5,
    day=15,
    hour=10,
    minute=20,
    second=30,
    microsecond=0,
)
