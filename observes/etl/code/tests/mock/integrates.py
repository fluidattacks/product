from etl_utils.typing import (
    FrozenSet,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    ResultFactory,
)

from code_etl.integrates_api import (
    ApiError,
    IgnoredPath,
    IntegratesClient,
    Mailmap,
    RootNickname,
)
from code_etl.objs import (
    OrgName,
    User,
)


def mock_client() -> IntegratesClient:
    empty: FrozenSet[IgnoredPath] = frozenset({})
    _factory: ResultFactory[FrozenSet[IgnoredPath], ApiError] = ResultFactory()
    _factory_2: ResultFactory[OrgName, ApiError] = ResultFactory()
    _factory_3: ResultFactory[FrozenSet[RootNickname], ApiError] = ResultFactory()
    _factory_4: ResultFactory[Mailmap, ApiError] = ResultFactory()
    mock_mailmap = _factory_4.success(
        Mailmap(
            FrozenDict(
                {
                    User(
                        name="Alice Garcia",
                        email="alice@entry.com",
                    ): User(
                        name="Alice B Garcia",
                        email="alice-b@subentry.com",
                    ),
                },
            ),
        ),
    )
    return IntegratesClient(
        lambda _: Cmd.wrap_value(_factory.success(empty)),
        lambda _: Cmd.wrap_value(_factory_2.success(OrgName("test_org"))),
        lambda _: Cmd.wrap_value(
            _factory_3.success(frozenset({RootNickname("root_1"), RootNickname("root_2")})),
        ),
        lambda _: Cmd.wrap_value(mock_mailmap),
        lambda _: Cmd.wrap_value(mock_mailmap),
    )
