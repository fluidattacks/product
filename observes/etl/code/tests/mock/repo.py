import os
import tarfile
from pathlib import (
    Path,
)
from tempfile import (
    mkdtemp,
)

from etl_utils.typing import (
    Dict,
)
from fa_purity import (
    Cmd,
    CmdUnwrapper,
    Unsafe,
)

from code_etl.objs import (
    GroupId,
    RepoId,
    RepoName,
)
from code_etl.upload_repo._extractor import (
    RepoObj,
)

_cache: Dict[str, Path] = {}
REPO_ID = RepoId(GroupId("test"), RepoName("some_repo"))


def _change_ownership(path: Path) -> Cmd[None]:
    def _action() -> None:
        uid = os.getuid()
        gid = os.getgid()
        for root, dirs, files in os.walk(path):
            for d in dirs:
                os.chown(Path(root) / d, uid, gid)
            for f in files:
                os.chown(Path(root) / f, uid, gid)
        os.chown(path, uid, gid)

    return Cmd.wrap_impure(_action)


def _get_mock_repo_path() -> Cmd[Path]:
    def _action(unwrapper: CmdUnwrapper) -> Path:
        self_parent = Path(__file__).resolve().parent
        tar_path = Path(self_parent) / "data" / "test_repo.tgz"
        with tarfile.open(tar_path, "r:gz") as tar:
            temp_dir = mkdtemp()
            tar.extractall(temp_dir)  # noqa: S202
            result = Path(temp_dir) / "test_repo"
            unwrapper.act(_change_ownership(result))
            return result

    return Cmd.new_cmd(_action)


def _mock_repo_path() -> Path:
    def _action(unwrapper: CmdUnwrapper) -> Path:
        if "repo" in _cache:
            return _cache["repo"]
        result = unwrapper.act(_get_mock_repo_path())
        _cache["repo"] = result
        return result

    return Unsafe.compute(Cmd.new_cmd(_action))


def mock_repo() -> RepoObj:
    return RepoObj.to_repo(_mock_repo_path()).alt(Unsafe.raise_exception).to_union()
