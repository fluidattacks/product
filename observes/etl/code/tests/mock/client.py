from fa_purity import (
    Cmd,
    Maybe,
    PureIterFactory,
    PureIterTransform,
    Unsafe,
)

from code_etl.database import (
    StampsClient,
)

from .db import (
    MockDB,
)


def mock_client_stamps(db: MockDB) -> StampsClient:
    return StampsClient(
        init_commits_table=Cmd.wrap_impure(
            lambda: Unsafe.raise_exception(NotImplementedError("mock init_table")),
        ),
        init_files_table=Cmd.wrap_impure(
            lambda: Unsafe.raise_exception(NotImplementedError("mock init_table")),
        ),
        count_items=lambda _: Cmd.wrap_impure(
            lambda: Unsafe.raise_exception(NotImplementedError("mock count")),
        ),
        most_recent_hash=lambda _: Cmd.wrap_value(Maybe.some("9" * 40)),
        insert_stamps=lambda s: PureIterFactory.from_list(s)
        .map(lambda i: db.stamps.set(i.stamp.commit.commit_id, i))
        .transform(PureIterTransform.consume),
        group_data=lambda g: Cmd.wrap_value(
            db.stamps.stream()
            .filter(
                lambda t: t[0].repo.group == g,
            )
            .map(lambda t: t[1].stamp),
        ),
        amend_users=lambda _: Cmd.wrap_impure(
            lambda: Unsafe.raise_exception(NotImplementedError("amend_users")),
        ),
        get_stamp=lambda s: db.stamps.get(s).map(lambda m: m.map(lambda s: s.stamp)),
        exist=lambda s: db.stamps.get(s).map(lambda m: m.map(lambda _: True).value_or(False)),
    )
