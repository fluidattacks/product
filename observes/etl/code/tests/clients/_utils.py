import os

import boto3
from fa_purity import (
    Cmd,
)


def mock_aws_credentials() -> Cmd[None]:
    """Mock AWS Credentials for moto."""

    def _action() -> None:
        os.environ["AWS_ACCESS_KEY_ID"] = "testing"
        os.environ["AWS_SECRET_ACCESS_KEY"] = "testing"  # noqa: S105
        os.environ["AWS_SECURITY_TOKEN"] = "testing"  # noqa: S105
        os.environ["AWS_SESSION_TOKEN"] = "testing"  # noqa: S105
        os.environ["AWS_DEFAULT_REGION"] = "us-east-1"

    return Cmd.wrap_impure(_action)


def create_dynamo_table(table_name: str) -> Cmd[None]:
    """Create a DynamoDB table."""

    def _action() -> None:
        client = boto3.client("dynamodb", region_name="us-east-1")
        client.create_table(
            TableName=table_name,
            KeySchema=[
                {"AttributeName": "group", "KeyType": "HASH"},
                {"AttributeName": "repository", "KeyType": "RANGE"},
            ],
            AttributeDefinitions=[
                {"AttributeName": "group", "AttributeType": "S"},
                {"AttributeName": "repository", "AttributeType": "S"},
            ],
            ProvisionedThroughput={
                "ReadCapacityUnits": 1,
                "WriteCapacityUnits": 1,
            },
        )

    return mock_aws_credentials() + Cmd.wrap_impure(_action)
