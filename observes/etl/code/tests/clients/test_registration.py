from fa_purity import (
    PureIterFactory,
    Unsafe,
)
from fa_purity.date_time import (
    DatetimeFactory,
)
from redshift_client.core.id_objs import (
    DbTableId,
    Identifier,
    SchemaId,
    TableId,
)
from redshift_client.sql_client import (
    DbPrimitiveFactory,
    RowData,
)

from code_etl.database.clients._registration.queries.get import (
    _decode,
)
from code_etl.database.clients._registration.queries.get._single import (
    _query as get_query,
)
from code_etl.database.clients._registration.queries.init_table import (
    table_definition,
)
from code_etl.database.clients._registration.queries.register import (
    _encode_registration,
)
from code_etl.database.clients._registration.queries.register import (
    _query as insert_query,
)
from code_etl.objs import (
    GroupId,
    RepoId,
    RepoName,
    RepoRegistration,
)
from tests import (
    _utils,
)

mock_registration = RepoRegistration(
    RepoId(GroupId("test_group"), RepoName("the_repo")),
    DatetimeFactory.EPOCH_START,
)


def test_table() -> None:
    assert table_definition()


def test_encode() -> None:
    _utils.encode_obj_completeness(table_definition(), _encode_registration(mock_registration))


def test_insert_query() -> None:
    _table = DbTableId(SchemaId(Identifier.new("test")), TableId(Identifier.new("test_table")))
    _query, _values = insert_query(_table, PureIterFactory.from_list((mock_registration,)))
    for v in _values:
        _utils.check_query_replacements(_query, v)


def test_get_decode() -> None:
    raw: RowData = RowData(
        (
            DbPrimitiveFactory.from_raw(mock_registration.repo.group.name),
            DbPrimitiveFactory.from_raw(mock_registration.repo.repository.name),
            DbPrimitiveFactory.from_raw(mock_registration.seen_at.date_time),
        ),
    )
    assert (
        _decode.decode_registration(raw).alt(Unsafe.raise_exception).to_union() == mock_registration
    )


def test_get_query() -> None:
    _table = DbTableId(SchemaId(Identifier.new("test")), TableId(Identifier.new("test_table")))
    _repo = RepoId(GroupId("test"), RepoName("some_repo"))
    _utils.check_query_replacements(*get_query(_table, _repo))
