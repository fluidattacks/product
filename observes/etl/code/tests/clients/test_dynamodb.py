import pytest
from fa_purity import (
    Cmd,
)
from moto import (
    mock_aws,
)

from code_etl.database.clients._checkpoint._dynamo.table import (
    DynamoTableClient,
)
from code_etl.objs import (
    Checkpoint,
    CommitDataId,
    CommitId,
    GroupId,
    RepoId,
    RepoName,
)
from tests._utils import (
    assert_new_utc,
)

REPO_ID = RepoId(group=GroupId("test_group"), repository=RepoName("repository"))

MOCK_CHECKPOINT = Checkpoint(
    is_initial=True,
    commit_id=CommitDataId(
        repo=REPO_ID,
        hash=CommitId("hash"),
    ),
    inserted_at=assert_new_utc(2024, 10, 1, 12, 00, 00, 00),
)


def assert_checkpoint(checkpoint: Checkpoint) -> None:
    assert checkpoint == MOCK_CHECKPOINT


def _test_dynamodb_table_class(client: DynamoTableClient) -> Cmd[None]:
    update = client.update_or_put(MOCK_CHECKPOINT)
    get = client.get(REPO_ID).map(lambda x: x.map(assert_checkpoint))
    remove = client.delete(REPO_ID)
    get_not_found = (
        client.get(REPO_ID)
        .map(lambda x: x.bind(lambda _: pytest.fail("Checkpoint should not exist.")))
        .map(lambda _: None)
    )

    return update + get + remove + get_not_found


@mock_aws
def test_dynamodb_table_class() -> None:
    pytest.skip("Differences between moto and DynamoDB")
