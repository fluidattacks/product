{ inputs, ... }@makes_inputs:
let
  python_version = "python311";
  nixpkgs = inputs.nixpkgs-observes-2 // { inherit (inputs) nix-filter; };
  out = import ./build {
    inherit makes_inputs nixpkgs python_version;
    src = nixpkgs.nix-filter {
      root = ./.;
      include = [ "code_etl" "tests" "pyproject.toml" "mypy.ini" "ruff.toml" ];
    };
  };
in out
