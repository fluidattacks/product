from os import (
    environ,
)

from code_etl.integrates_api import (
    IntegratesToken,
)
from code_etl.objs import (
    GroupId,
)


def get_token() -> IntegratesToken:
    return IntegratesToken.new(environ["TEST_ARM_TOKEN"])


def get_group() -> GroupId:
    return GroupId(environ["TEST_ARM_GROUP"])
