import pytest
from etl_utils.parallel import (
    parallel_cmds,
)
from fa_purity import (
    Unsafe,
)

from code_etl.integrates_api import (
    IntegratesClientFactory,
)

from ._common import (
    get_group,
    get_token,
)


def test_single() -> None:
    client = IntegratesClientFactory.new_client(get_token())
    items = client.bind(lambda c: c.get_org(get_group()))

    with pytest.raises(SystemExit):
        items.map(lambda x: x.alt(Unsafe.raise_exception).to_union()).compute()


def test_stress() -> None:
    client = IntegratesClientFactory.new_client(get_token())
    items = client.map(lambda c: tuple(c.get_org(get_group()) for _ in range(100))).bind(
        lambda x: parallel_cmds(x, 50),
    )

    with pytest.raises(SystemExit):
        items.map(lambda rs: (r.alt(Unsafe.raise_exception).to_union() for r in rs)).compute()
