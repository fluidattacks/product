{ makeScript, ... }@makes_inputs:
let searchPaths = import ./search_paths.nix makes_inputs;
in makeScript {
  inherit searchPaths;
  name = "observes-etl-code";
  entrypoint = ''
    observes-etl-code "''${@}"
  '';
}
