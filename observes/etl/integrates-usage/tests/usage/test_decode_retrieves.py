from dateutil.relativedelta import relativedelta
from fa_purity import Unsafe
from fa_purity.date_time import DatetimeFactory
from fa_purity.json import UnfoldedFactory

from integrates_usage_etl._cloudwatch._core import LogQueryStatus, QueryResult
from integrates_usage_etl._date_range import DateRange, NonZeroRelativeDelta
from integrates_usage_etl.usage._core import RetrievesUsage, Usage
from integrates_usage_etl.usage._utils import FinishedQueryResult
from integrates_usage_etl.usage.retrieves import _decode_usage


def test_decode_retrieves() -> None:
    mock_range = DateRange.new(
        DatetimeFactory.EPOCH_START,
        NonZeroRelativeDelta.new(relativedelta(days=1)).alt(Unsafe.raise_exception).to_union(),
    )
    raw = (
        (UnfoldedFactory.from_dict({"field": "transact_count", "value": "5"}),),
        (
            UnfoldedFactory.from_dict({"field": "group_name", "value": "group_1"}),
            UnfoldedFactory.from_dict({"field": "transact_count", "value": "11"}),
        ),
        (
            UnfoldedFactory.from_dict({"field": "group_name", "value": "group_2"}),
            UnfoldedFactory.from_dict({"field": "transact_count", "value": "22"}),
        ),
    )
    expected = (
        RetrievesUsage(Usage(mock_range, "", 5)),
        RetrievesUsage(Usage(mock_range, "group_1", 11)),
        RetrievesUsage(Usage(mock_range, "group_2", 22)),
    )
    assert (
        FinishedQueryResult.from_result(QueryResult(LogQueryStatus.COMPLETE, raw))
        .bind(
            lambda f: _decode_usage(mock_range, f),
        )
        .alt(Unsafe.raise_exception)
        .to_union()
        == expected
    )
