from fa_purity import Maybe, Unsafe

from integrates_usage_etl.state._core import EtlState
from integrates_usage_etl.state.decode import decode_state
from integrates_usage_etl.state.encode import encode_state
from tests import _utils


def test_encode() -> None:
    state = EtlState(Maybe.some(_utils.mock_range()))
    decoded = decode_state(encode_state(state)).alt(Unsafe.raise_exception).to_union()
    assert state == decoded
    assert encode_state(state) == encode_state(decoded)
