from arch_lint.dag import (
    DagMap,
)
from arch_lint.graph import (
    FullPathModule,
)
from etl_utils.typing import (
    Dict,
    FrozenSet,
    TypeVar,
)

_T = TypeVar("_T")


def raise_or_return(item: _T | Exception) -> _T:
    if isinstance(item, Exception):
        raise item
    return item


def _module(path: str) -> FullPathModule:
    return raise_or_return(FullPathModule.from_raw(path))


_dag: Dict[str, tuple[tuple[str, ...] | str, ...]] = {
    "integrates_usage_etl": (
        "_cli",
        "etl",
        ("_db", "state"),
        "usage",
        "_cloudwatch",
        ("_utils", "_date_range", "_logger", "_s3"),
    ),
    "integrates_usage_etl.state": (
        ("encode", "decode"),
        "_core",
    ),
    "integrates_usage_etl._cloudwatch": (
        "_client_1",
        "_core",
    ),
    "integrates_usage_etl._db": (
        "_save",
        "_table",
        "_core",
    ),
    "integrates_usage_etl.etl": (
        "_retrieves",
        "init",
        ("_conf", "_next_ranges"),
    ),
    "integrates_usage_etl.usage": (
        "retrieves",
        ("_core", "_utils"),
    ),
}


def project_dag() -> DagMap:
    return raise_or_return(DagMap.new(_dag))


def forbidden_allowlist() -> Dict[FullPathModule, FrozenSet[FullPathModule]]:
    _raw: Dict[str, FrozenSet[str]] = {}
    return {_module(k): frozenset(_module(i) for i in v) for k, v in _raw.items()}
