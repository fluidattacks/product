from dataclasses import dataclass

from snowflake_client import ClientFactory, SnowflakeCursor

from ._core import DbClient
from ._save import save_usage
from ._table import init_table


@dataclass(frozen=True)
class DbClientFactory:
    @staticmethod
    def new(cursor: SnowflakeCursor) -> DbClient:
        client = ClientFactory.new_table_client(cursor)
        return DbClient(
            lambda t: init_table(client, t),
            lambda t, u: save_usage(client, t, u),
        )


__all__ = [
    "DbClient",
]
