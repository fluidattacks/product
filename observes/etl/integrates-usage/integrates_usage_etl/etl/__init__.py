from connection_manager import (
    ConnectionManagerFactory,
    DbClients,
)
from etl_utils.parallel import ThreadPool
from fa_purity import Cmd, ResultE, cast_exception

from integrates_usage_etl import _utils
from integrates_usage_etl._s3 import S3URI, S3Factory
from integrates_usage_etl.etl._conf import CONNECTION_CONF
from integrates_usage_etl.state import EtlState
from integrates_usage_etl.state.encode import encode_state

from ._retrieves import retrieves_etl
from .init import setup


def _save_state(state_uri: S3URI, state: EtlState) -> Cmd[ResultE[None]]:
    return S3Factory.new_s3_client().bind(lambda c: c.save(state_uri, encode_state(state)))


def _main(clients: DbClients) -> Cmd[ResultE[None]]:
    return _utils.std_chain_cmd_result(
        setup(),
        lambda t: ThreadPool.new(10).bind(
            lambda p: _utils.std_chain_cmd_result(
                retrieves_etl(t[0], t[1], p, clients.connection.cursor),
                lambda s: _save_state(t[0].conf.state_uri, s),
            ),
        ),
    )


def execute_etls() -> Cmd[ResultE[None]]:
    return _utils.std_chain_cmd_result(
        ConnectionManagerFactory.observes_manager(),
        lambda m: m.execute_with_snowflake(_main, CONNECTION_CONF).map(
            lambda r: r.alt(lambda c: c.map(cast_exception, cast_exception)),
        ),
    )
