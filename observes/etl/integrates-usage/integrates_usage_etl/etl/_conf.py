from dataclasses import dataclass

from connection_manager import (
    ConnectionConf,
    Databases,
    Roles,
    Warehouses,
)
from fa_purity import Unsafe
from fa_purity.date_time import DatetimeUTC

from integrates_usage_etl._s3 import S3URI

CONNECTION_CONF = ConnectionConf(
    Warehouses.GENERIC_COMPUTE,
    Roles.ETL_UPLOADER,
    Databases.OBSERVES,
)

STATE_URI = (
    S3URI.from_raw("s3://observes.state/integrates_usage/product_state.json")
    .alt(Unsafe.raise_exception)
    .to_union()
)


@dataclass(frozen=True)
class EtlConf:
    state_uri: S3URI
    max_history_periods: int
    max_update_periods: int


@dataclass(frozen=True)
class EtlSetup:
    current_date: DatetimeUTC
    conf: EtlConf
