from __future__ import annotations

from dataclasses import dataclass

from dateutil.relativedelta import relativedelta
from fa_purity import FrozenList, Maybe, PureIterFactory, Result, ResultE, Unsafe, cast_exception
from fa_purity.date_time import DatetimeUTC


@dataclass(frozen=True)
class NonZeroRelativeDelta:
    """Ensures that the relative delta is not zero."""

    @dataclass(frozen=True)
    class _Private:
        pass

    _private: NonZeroRelativeDelta._Private
    delta: relativedelta

    @staticmethod
    def new(delta: relativedelta) -> ResultE[NonZeroRelativeDelta]:
        zero = relativedelta(days=0)
        if delta != zero:
            return Result.success(NonZeroRelativeDelta(NonZeroRelativeDelta._Private(), delta))
        return Result.failure(ValueError("Delta is zero"))

    def __neg__(self) -> NonZeroRelativeDelta:
        return NonZeroRelativeDelta(NonZeroRelativeDelta._Private(), -self.delta)


@dataclass(frozen=True)
class DateRange:
    """Ensures that its properties comply with:  start < end."""

    @dataclass(frozen=True)
    class _Private:
        pass

    _private: DateRange._Private
    start: DatetimeUTC
    end: DatetimeUTC

    @staticmethod
    def from_endpoints(start: DatetimeUTC, end: DatetimeUTC) -> ResultE[DateRange]:
        if end.date_time > start.date_time:
            return Result.success(DateRange(DateRange._Private(), start, end))
        return Result.failure(
            ValueError("`DateRange` must comply with `start < end`."),
        )

    @classmethod
    def new(cls, reference: DatetimeUTC, delta: NonZeroRelativeDelta) -> DateRange:
        """Create a `DateRange` instance."""
        second_date = (
            DatetimeUTC.assert_utc(reference.date_time + delta.delta)
            .alt(Unsafe.raise_exception)
            .to_union()
        )
        if second_date.date_time > reference.date_time:
            return cls.from_endpoints(reference, second_date).alt(Unsafe.raise_exception).to_union()
        if second_date.date_time < reference.date_time:
            return cls.from_endpoints(second_date, reference).alt(Unsafe.raise_exception).to_union()
        err = "Impossible"
        raise ValueError(err)


@dataclass(frozen=True)
class ContinuousDateRanges:
    """
    Ensures that the list of date ranges comply the following.

    For each data range in the list.

    - the start is equal to the previous date range end (if exist).
    """

    @dataclass(frozen=True)
    class _Private:
        pass

    @dataclass(frozen=True)
    class _State:
        previous: Maybe[DateRange]
        is_valid: bool

    _private: ContinuousDateRanges._Private
    ranges: FrozenList[DateRange]

    @staticmethod
    def new(ranges: FrozenList[DateRange]) -> ResultE[ContinuousDateRanges]:
        init: ContinuousDateRanges._State = ContinuousDateRanges._State(Maybe.empty(), True)

        def _gen(
            state: ContinuousDateRanges._State,
            current: DateRange,
        ) -> tuple[ContinuousDateRanges._State, ContinuousDateRanges._State]:
            state = ContinuousDateRanges._State(
                Maybe.some(current),
                state.previous.map(lambda p: p.end.date_time == current.start.date_time).value_or(
                    state.is_valid,
                ),
            )
            return (state, state)

        break_point = (
            PureIterFactory.from_list(ranges)
            .generate(
                _gen,
                init,
            )
            .find_first(lambda s: not s.is_valid)
            .bind(lambda s: s.previous)
        )
        return break_point.map(
            lambda d: Result.failure(ValueError(f"Breakpoint after {d}")).alt(cast_exception),
        ).or_else_call(
            lambda: Result.success(ContinuousDateRanges(ContinuousDateRanges._Private(), ranges)),
        )
