from __future__ import (
    annotations,
)

from collections.abc import Callable
from dataclasses import (
    dataclass,
)
from tempfile import (
    TemporaryFile,
)

import boto3
from fa_purity import (
    Cmd,
    Result,
    ResultE,
)
from fa_purity.json import (
    JsonObj,
    JsonUnfolder,
    UnfoldedFactory,
)
from mypy_boto3_s3 import (
    S3Client,
)

from . import _utils


@dataclass(frozen=True)
class S3URI:
    bucket: str
    file_path: str

    @staticmethod
    def from_raw(raw: str) -> ResultE[S3URI]:
        try:
            if raw.startswith("s3://"):
                _raw = raw.removeprefix("s3://")
                _splitted = _raw.split("/")
                bucket = _splitted[0]
                obj_file = "/".join(_splitted[1:])
                if obj_file:
                    return Result.success(S3URI(bucket, obj_file), Exception)
            return Result.failure(ValueError("Invalid s3 file obj URI"), S3URI).alt(Exception)
        except IndexError as err:
            return Result.failure(err, S3URI).alt(Exception)

    @property
    def uri(self) -> str:
        return "s3://" + "/".join([self.bucket, self.file_path])


@dataclass(frozen=True)
class PureS3Client:
    save: Callable[[S3URI, JsonObj], Cmd[ResultE[None]]]
    load_json: Callable[[S3URI], Cmd[ResultE[JsonObj]]]


@dataclass(frozen=True)
class _Client:
    _client: S3Client

    def save(self, uri: S3URI, data: JsonObj) -> Cmd[ResultE[None]]:
        def _action() -> None:
            with TemporaryFile() as file:
                file.write(JsonUnfolder.dumps(data).encode("UTF-8"))
                file.seek(0)
                self._client.upload_fileobj(file, uri.bucket, uri.file_path)

        return Cmd.wrap_impure(lambda: _utils.error_handler(_action))

    def load_json(self, uri: S3URI) -> Cmd[ResultE[JsonObj]]:
        def _action() -> ResultE[JsonObj]:
            response = self._client.get_object(
                Bucket=uri.bucket,
                Key=uri.file_path,
            )
            raw = response["Body"].read().decode("utf-8")
            return UnfoldedFactory.loads(raw)

        return Cmd.wrap_impure(lambda: _utils.error_handler(_action).bind(lambda x: x))

    def client(self) -> PureS3Client:
        return PureS3Client(
            self.save,
            self.load_json,
        )


@dataclass(frozen=True)
class S3Factory:
    @staticmethod
    def new_s3_client() -> Cmd[PureS3Client]:
        return Cmd.wrap_impure(lambda: _Client(boto3.client("s3")).client())
