from dataclasses import dataclass

from integrates_usage_etl._cloudwatch import (
    LogGroup,
)
from integrates_usage_etl._date_range import DateRange

INTEGRATES_LOG_GROUP = LogGroup(("integrates",))


@dataclass(frozen=True)
class Usage:
    date_range: DateRange
    group: str
    count: int


@dataclass(frozen=True)
class RetrievesUsage:
    usage: Usage
