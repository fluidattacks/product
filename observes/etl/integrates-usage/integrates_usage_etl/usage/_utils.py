from __future__ import annotations

from dataclasses import dataclass

from fa_purity import Cmd, Coproduct, CoproductFactory, ResultE, Unsafe
from fa_purity._core.result import Result
from pure_requests.retry import HandledError, cmd_if_fail, retry_cmd, sleep_cmd

from integrates_usage_etl._cloudwatch import (
    CloudwatchLogClient,
    LogQueryStatus,
    QueryId,
    QueryResult,
)


@dataclass(frozen=True)
class FinishedQueryResult:
    class _Private:
        pass

    _private: FinishedQueryResult._Private
    result: QueryResult

    @staticmethod
    def from_result(result: QueryResult) -> ResultE[FinishedQueryResult]:
        if result.status in (
            LogQueryStatus.COMPLETE,
            LogQueryStatus.CANCELLED,
            LogQueryStatus.FAILED,
            LogQueryStatus.TIMEOUT,
        ):
            return Result.success(FinishedQueryResult(FinishedQueryResult._Private(), result))
        return Result.failure(ValueError("Not a `FinishedQueryResult`"))


@dataclass(frozen=True)
class UnfinishedQueryResult:
    class _Private:
        pass

    _private: UnfinishedQueryResult._Private
    result: QueryResult

    @staticmethod
    def from_result(result: QueryResult) -> ResultE[UnfinishedQueryResult]:
        if result.status not in (
            LogQueryStatus.COMPLETE,
            LogQueryStatus.CANCELLED,
            LogQueryStatus.FAILED,
            LogQueryStatus.TIMEOUT,
        ):
            return Result.success(UnfinishedQueryResult(UnfinishedQueryResult._Private(), result))
        return Result.failure(ValueError("Not a `UnfinishedQueryResult`"))


def _classify_result(result: QueryResult) -> Coproduct[FinishedQueryResult, UnfinishedQueryResult]:
    factory: CoproductFactory[FinishedQueryResult, UnfinishedQueryResult] = CoproductFactory()
    return (
        FinishedQueryResult.from_result(result)
        .map(factory.inl)
        .lash(lambda _: UnfinishedQueryResult.from_result(result).map(factory.inr))
        .alt(lambda _: Exception("Impossible"))
        .alt(Unsafe.raise_exception)
        .to_union()
    )


def is_completed_query(result: FinishedQueryResult) -> ResultE[FinishedQueryResult]:
    if result.result.status == LogQueryStatus.COMPLETE:
        return Result.success(result)
    return Result.failure(ValueError("`FinishedQueryResult` is not completed"))


def wait_completion(
    client: CloudwatchLogClient,
    query_id: QueryId,
) -> Cmd[Result[FinishedQueryResult, Coproduct[UnfinishedQueryResult, Exception]]]:
    completed_query: Cmd[
        Result[FinishedQueryResult, HandledError[UnfinishedQueryResult, Exception]]
    ] = client.get_result(query_id).map(
        lambda r: r.map(_classify_result)
        .to_coproduct()
        .map(
            lambda c: c.map(
                lambda f: Result.success(f),
                lambda u: Result.failure(HandledError.handled(u)),
            ),
            lambda e: Result.failure(HandledError.unhandled(e)),
        ),
    )
    factory: CoproductFactory[UnfinishedQueryResult, Exception] = CoproductFactory()
    return retry_cmd(
        completed_query,
        lambda i, r: cmd_if_fail(r, sleep_cmd(5 + 55 * (i - 1))),
        9,
    ).bind(
        lambda r: r.to_coproduct().map(
            lambda f: Cmd.wrap_value(Result.success(f)),
            lambda _: sleep_cmd(60)
            + client.get_result(query_id).map(
                # the last retry; it is not included inside the retry_cmd strategy
                # because it does not return the last handled failure
                # that is intended to be returned
                lambda r: r.map(_classify_result)
                .map(
                    lambda c: c.map(
                        lambda f: Result.success(f),
                        lambda u: Result.failure(factory.inl(u)),
                    ),
                )
                .to_coproduct()
                .map(
                    lambda r: r,
                    lambda e: Result.failure(factory.inr(e)),
                ),
            ),
        ),
    )
