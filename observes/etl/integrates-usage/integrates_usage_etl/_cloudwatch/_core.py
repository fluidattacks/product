from collections.abc import Callable
from dataclasses import dataclass
from enum import Enum

from fa_purity import Cmd, FrozenList, ResultE
from fa_purity.date_time import DatetimeUTC
from fa_purity.json import JsonObj

Start = DatetimeUTC
End = DatetimeUTC


@dataclass(frozen=True)
class LogGroup:
    path_items: FrozenList[str]


class LogQueryLanguage(Enum):
    CWLI = "CWLI"
    SQL = "SQL"
    PPL = "PPL"


class LogQueryStatus(Enum):
    COMPLETE = "Complete"
    CANCELLED = "Cancelled"
    FAILED = "Failed"
    RUNNING = "Running"
    SCHEDULED = "Scheduled"
    TIMEOUT = "Timeout"
    UNKNOWN = "Unknown"


@dataclass(frozen=True)
class LogQuery:
    language: LogQueryLanguage
    query: str


@dataclass(frozen=True)
class QueryId:
    query: str


@dataclass(frozen=True)
class QueryResult:
    status: LogQueryStatus
    data: FrozenList[FrozenList[JsonObj]]


@dataclass(frozen=True)
class CloudwatchLogClient:
    start_query: Callable[[FrozenList[LogGroup], LogQuery, Start, End], Cmd[ResultE[QueryId]]]
    get_result: Callable[[QueryId], Cmd[ResultE[QueryResult]]]
