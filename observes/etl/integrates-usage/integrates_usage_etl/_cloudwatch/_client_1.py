import inspect
from dataclasses import dataclass

import boto3
from etl_utils.bug import Bug
from fa_purity import Cmd, FrozenList, PureIterFactory, ResultE
from fa_purity.json import JsonValueFactory, Unfolder
from mypy_boto3_logs.client import CloudWatchLogsClient as RawClient
from mypy_boto3_logs.type_defs import GetQueryResultsResponseTypeDef, StartQueryResponseTypeDef

from integrates_usage_etl import _utils
from integrates_usage_etl._cloudwatch._core import (
    CloudwatchLogClient,
    End,
    LogGroup,
    LogQuery,
    LogQueryStatus,
    QueryId,
    QueryResult,
    Start,
)


def _float_to_int(value: float) -> int:
    return round(value)


def _to_full_path(group: LogGroup) -> str:
    return "/".join(group.path_items)


def _decode_result(raw: GetQueryResultsResponseTypeDef) -> ResultE[QueryResult]:
    data = JsonValueFactory.from_any(raw["results"]).bind(
        lambda v: Unfolder.to_list_of(v, lambda v2: Unfolder.to_list_of(v2, Unfolder.to_json)),
    )
    return data.map(lambda d: QueryResult(LogQueryStatus(raw["status"]), d))


@dataclass(frozen=True)
class _Client1:
    _client: RawClient

    def start_query(
        self,
        log_groups: FrozenList[LogGroup],
        query: LogQuery,
        start: Start,
        end: End,
    ) -> Cmd[ResultE[QueryId]]:
        def _action() -> StartQueryResponseTypeDef:
            return self._client.start_query(
                logGroupNames=PureIterFactory.pure_map(_to_full_path, log_groups).to_list(),
                startTime=_float_to_int(start.date_time.timestamp()),
                endTime=_float_to_int(end.date_time.timestamp()),
                queryString=query.query,
            )

        return Cmd.wrap_impure(lambda: _utils.error_handler(_action)).map(
            lambda r: r.map(lambda i: QueryId(i["queryId"])),
        )

    def get_result(self, query: QueryId) -> Cmd[ResultE[QueryResult]]:
        def _action() -> GetQueryResultsResponseTypeDef:
            return self._client.get_query_results(queryId=query.query)

        return Cmd.wrap_impure(lambda: _utils.error_handler(_action)).map(
            lambda r: r.map(
                lambda raw: Bug.assume_success(
                    "decode_get_result",
                    inspect.currentframe(),
                    (str(raw),),
                    _decode_result(raw),
                ),
            ),
        )


@dataclass(frozen=True)
class CloudwatchLogClientFactory:
    @staticmethod
    def new() -> Cmd[ResultE[CloudwatchLogClient]]:
        def _action() -> ResultE[CloudwatchLogClient]:
            client = _utils.error_handler(lambda: boto3.client("logs")).map(_Client1)
            return client.map(
                lambda c: CloudwatchLogClient(
                    c.start_query,
                    c.get_result,
                ),
            )

        return Cmd.wrap_impure(_action)
