from fa_purity import FrozenDict
from fa_purity.json import JsonObj, JsonPrimitiveFactory, JsonValue, Primitive, UnfoldedFactory

from integrates_usage_etl._date_range import DateRange

from ._core import EtlState


def _encode_obj(obj_type: str, value: JsonValue) -> JsonObj:
    return FrozenDict(
        {"type": JsonValue.from_primitive(JsonPrimitiveFactory.from_raw(obj_type)), "value": value},
    )


def _encode_date_range(raw: DateRange) -> JsonObj:
    obj: FrozenDict[str, Primitive] = FrozenDict(
        {
            "start": raw.start.date_time.isoformat(),
            "end": raw.end.date_time.isoformat(),
        },
    )
    return _encode_obj("DateRange", JsonValue.from_json(UnfoldedFactory.from_dict(obj)))


def encode_state(raw: EtlState) -> JsonObj:
    obj: JsonObj = raw.retrieves_usage.map(
        lambda u: FrozenDict(
            {"retrieves_usage": JsonValue.from_json(_encode_date_range(u))},
        ),
    ).value_or(FrozenDict({}))
    return _encode_obj("EtlState", JsonValue.from_json(obj))
