import inspect
from collections.abc import Callable
from typing import TypeVar

from etl_utils.bug import Bug
from fa_purity import (
    Cmd,
    Coproduct,
    FrozenDict,
    FrozenList,
    Maybe,
    PureIter,
    PureIterFactory,
    Result,
    ResultE,
    Stream,
    Unsafe,
)

_T = TypeVar("_T")
_K = TypeVar("_K")
_V = TypeVar("_V")
_S = TypeVar("_S")
_F = TypeVar("_F")
_S2 = TypeVar("_S2")
_F2 = TypeVar("_F2")


def chain_cmd_result(
    cmd_1: Cmd[Result[_S, _F]],
    cmd_2: Callable[[_S], Cmd[Result[_S2, _F2]]],
) -> Cmd[Result[_S2, Coproduct[_F, _F2]]]:
    """
    Chains commands results.

    Where the second command should only
    be executed if the first result was success.
    This returns a Cmd that will:
    - execute the first cmd
    - if the result is failure then return it
    - if the result is success then executes the supplied function
    and return the result of the returned Cmd.
    """
    return cmd_1.bind(
        lambda r: r.to_coproduct().map(
            lambda s: cmd_2(s).map(
                lambda r: r.alt(Coproduct.inr),
            ),
            lambda f: Cmd.wrap_value(Result.failure(Coproduct.inl(f))),
        ),
    )


def std_chain_cmd_result(
    cmd_1: Cmd[ResultE[_S]],
    cmd_2: Callable[[_S], Cmd[ResultE[_S2]]],
) -> Cmd[ResultE[_S2]]:
    return chain_cmd_result(cmd_1, cmd_2).map(
        lambda r: r.alt(
            lambda c: c.map(
                lambda e: e,
                lambda e: e,
            ),
        ),
    )


def consume_stream_until_fail(items: Stream[Result[_S, _F]]) -> Cmd[Maybe[_F]]:
    return items.find_first(lambda r: r.map(lambda _: False).value_or(True)).map(
        lambda m: m.map(
            lambda r: r.map(
                lambda _: Unsafe.raise_exception(
                    ValueError("Not possible. If present, result is a failure"),
                ),
            ).to_union(),
        ),
    )


def error_handler(procedure: Callable[[], _T]) -> ResultE[_T]:
    try:
        return Result.success(procedure())
    except Exception as e:  # noqa: BLE001
        # because the error is returned not ignored
        return Result.failure(e)


def str_to_int(raw: str) -> ResultE[int]:
    try:
        return Result.success(int(raw))
    except ValueError as err:
        return Result.failure(err)


def build_dict_unique_keys(raw: FrozenList[tuple[_K, _V]]) -> ResultE[FrozenDict[_K, _V]]:
    """
    Build a dict ensuring that the raw items do not repeat keys.

    e.g. `(("foo", "val"), (("foo", "val2"))` fails because `foo` repeats
    """
    init: FrozenDict[_K, int] = FrozenDict({})
    keys_count: PureIter[Maybe[_K]] = (
        PureIterFactory.from_list(raw)
        .map(lambda t: t[0])
        .generate(
            lambda d, s: (
                FrozenDict(dict(d) | {s: 1}) if s not in d else FrozenDict(dict(d) | {s: d[s] + 1}),
                Maybe.empty() if s not in d else Maybe.some(s),
            ),
            init,
        )
    )
    repeated_key: Maybe[_K] = keys_count.find_first(
        lambda m: m.map(lambda _: True).value_or(False),
    ).map(
        lambda m: m.to_result()
        .to_coproduct()
        .map(
            lambda x: x,
            lambda _: Bug.new(
                "impossible",
                inspect.currentframe(),
                ValueError("The found item should be not empty"),
                (str(keys_count.to_list()),),
            ).explode(),
        ),
    )
    return (
        repeated_key.to_result()
        .to_coproduct()
        .map(
            lambda s: Result.failure(
                Bug.new(
                    "repeated_key",
                    inspect.currentframe(),
                    KeyError(s),
                    (str(raw),),
                ),
            ),
            lambda _: Result.success(FrozenDict(dict(raw))),
        )
    )


def get_index(items: FrozenList[_T], index: int) -> Maybe[_T]:
    try:
        return Maybe.some(items[index])
    except IndexError:
        return Maybe.empty()
