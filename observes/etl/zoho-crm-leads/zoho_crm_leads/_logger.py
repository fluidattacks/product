from etl_utils.secrets import (
    GenericSecret,
    ObservesSecretsFactory,
)
from fa_purity import (
    Cmd,
)
from utils_logger import (
    set_main_log,
)
from utils_logger.env import (
    current_app_env,
    observes_debug,
)
from utils_logger.handlers import (
    LoggingConf,
)


def set_logger(root_name: str, version: str) -> Cmd[None]:
    default_key = GenericSecret("")
    n_key = ObservesSecretsFactory.from_env().bind(
        lambda r: r.to_coproduct().map(
            lambda s: s.get_secret("bugsnag_notifier_integrates_usage").map(
                lambda r: r.value_or(default_key),
            ),
            lambda _: Cmd.wrap_value(default_key),
        ),
    )
    app_env = current_app_env()
    debug = observes_debug()
    conf = n_key.bind(
        lambda key: app_env.map(
            lambda env: LoggingConf(
                "etl",
                version,
                "./observes/etl/zoho-crm-leads",
                False,
                key.value,
                env,
                "observes",
                "cxtp_iwvunckRRjkHAO6HDulc4qqflKd888",
                "zoho_crm_leads",
            ),
        ),
    )
    return debug.bind(lambda d: conf.bind(lambda c: set_main_log(root_name, c, d, False)))
