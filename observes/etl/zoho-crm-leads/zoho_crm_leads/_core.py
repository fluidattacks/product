import logging

import requests
from etl_utils.secrets import (
    ZohoCreds,
)
from fa_purity import (
    Cmd,
)

LOG = logging.getLogger(__name__)
NOTIFIER_TIMEOUT_SEC = 10


def get_access_token(creds: ZohoCreds) -> Cmd[str | None]:
    def _action() -> str | None:
        url = (
            "https://accounts.zoho.com/oauth/v2/token"
            f"?refresh_token={creds.refresh_token}"
            f"&client_id={creds.client_id}"
            f"&client_secret={creds.client_secret}"
            "&grant_type=refresh_token"
        )

        try:
            response = requests.post(url, timeout=NOTIFIER_TIMEOUT_SEC)
            response.raise_for_status()
            return response.json().get("access_token")  # type: ignore[misc, no-any-return]
        except requests.RequestException:
            logging.exception("Request failed")
            return None

    return Cmd.wrap_impure(_action)


def http_request(
    access_token: str,
    body: dict[str, list[dict[str, str | float]]],
    entity: str,
) -> Cmd[None]:
    def _action() -> None:
        url = f"https://www.zohoapis.com/crm/v7/{entity}"
        headers = {
            "Authorization": f"Zoho-oauthtoken {access_token}",
            "Content-Type": "application/json",
        }

        try:
            response = requests.patch(url, headers=headers, json=body, timeout=NOTIFIER_TIMEOUT_SEC)
            response.raise_for_status()
            logging.info(
                ("Successfully sent data to Zoho CRM - Module: %s | Status: %s | Response: %s"),
                entity,
                response.status_code,
                response.json(),  # type: ignore[misc]
            )

        except requests.RequestException as e:
            status_code = e.response.status_code if e.response is not None else 500
            error_detail = {"message": "Unknown error", "status": "error"}

            if e.response is not None:
                status_code = e.response.status_code
                try:
                    error_detail = e.response.json()
                except ValueError:
                    error_detail["message"] = e.response.text

            logging.exception(
                (
                    "Request to Zoho CRM API failed - Module: %s | Status: %s | "
                    "Error: %s | Body: %s"
                ),
                entity,
                status_code,
                error_detail,
                body,
            )

    return Cmd.wrap_impure(_action)
