{ lib, makes_inputs, nixpkgs, python_pkgs, python_version, }@args:
let
  bundles = import (makes_inputs.projectPath
    "${makes_inputs.inputs.observesIndex.common.python_pkgs}/redshift_client")
    args;
in bundles."v8.0.1".build_bundle (default: required_deps: builder:
  builder lib (required_deps (python_pkgs // {
    inherit (default.python_pkgs) types-psycopg2 types-boto3;
  })))
