{ makes_inputs, nixpkgs, pynix, python_version, src, }:
let
  deps = import ./deps { inherit makes_inputs nixpkgs pynix python_version; };
  pkgDeps = {
    runtime_deps = with deps.python_pkgs; [
      boto3
      etl-utils
      redshift-client
      click
      requests
      types-requests
      utils-logger
    ];
    build_deps = with deps.python_pkgs; [ flit-core ];
    test_deps = with deps.python_pkgs; [
      arch-lint
      mypy
      ruff
      pytest
      pytest-cov
    ];
  };
  bundle = pynix.stdBundle { inherit pkgDeps src; };
  vs_settings = pynix.vscodeSettingsShell { pythonEnv = bundle.env.dev; };
  coverage = pynix.coverageReport {
    inherit src;
    parentModulePath =
      makes_inputs.inputs.observesIndex.etl.zoho_crm_leads.root;
    pythonDevEnv = bundle.env.dev;
    module = makes_inputs.inputs.observesIndex.etl.zoho_crm_leads.pkg_name;
    testsFolder = "tests";
  };
in bundle // {
  inherit coverage;
  dev_hook = vs_settings.hook.text;
}
