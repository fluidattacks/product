from etl_utils.natural import NaturalOperations
from fa_purity import NewFrozenList, Unsafe
from gitlab_sdk.ids import MrInternalId

from gitlab_etl.state._core import (
    Completeness,
    ContinuousMrRanges,
    MrCompleteness,
    MrIdRange,
)
from gitlab_etl.state.decode import decode_mr_completeness
from gitlab_etl.state.encode import encode_completeness


def mock_state() -> MrCompleteness:
    return (
        MrCompleteness.new(
            ContinuousMrRanges.new(
                NewFrozenList.new(
                    MrIdRange.single(MrInternalId(NaturalOperations.absolute(54))),
                    MrIdRange.single(MrInternalId(NaturalOperations.absolute(55))),
                    MrIdRange.single(MrInternalId(NaturalOperations.absolute(56))),
                ),
            )
            .alt(Unsafe.raise_exception)
            .to_union(),
            NewFrozenList.new(
                Completeness.COMPLETED,
                Completeness.MISSING,
                Completeness.ERROR,
            ),
        )
        .alt(Unsafe.raise_exception)
        .to_union()
    )


def test_encode_decode_state() -> None:
    state = mock_state()
    assert (
        decode_mr_completeness(encode_completeness(state)).alt(Unsafe.raise_exception).to_union()
        == state
    )
