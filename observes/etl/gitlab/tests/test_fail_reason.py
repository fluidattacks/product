from gitlab_etl.core import (
    FailReason,
)
from gitlab_etl.db._client_1.fail_reason.save import (
    _encode_reason,
)
from gitlab_etl.db._client_1.fail_reason.table import (
    fail_reason_table,
)
from gitlab_etl.sdk.core import (
    JobId,
)


def test_table() -> None:
    assert fail_reason_table()


def test_encode() -> None:
    assert _encode_reason(JobId(123), FailReason.UNKNOWN)
