from typing import (
    TypeVar,
)

from arch_lint.dag import (
    DagMap,
)
from arch_lint.graph import (
    FullPathModule,
)
from fa_purity import (
    FrozenList,
)

_dag: dict[str, FrozenList[FrozenList[str] | str]] = {
    "gitlab_etl": (
        "_cli",
        "jobs",
        "etl",
        "fail_reasons",
        ("db", "state"),
        "core",
        ("sdk", "_s3"),
        ("_logger", "_utils"),
    ),
    "gitlab_etl.db": (
        "_client_1",
        "merge_request",
        "core",
    ),
    "gitlab_etl.db._client_1": (("fail_reason", "get_jobs"),),
    "gitlab_etl.db._client_1.fail_reason": (
        "save",
        "table",
    ),
    "gitlab_etl.db.merge_request": (
        "_save",
        "_encode",
        ("_table", "_secondary"),
        "_core",
    ),
    "gitlab_etl.sdk": (
        "client",
        "_client_1",
        "core",
    ),
    "gitlab_etl.state": (
        ("encode", "decode"),
        "utils",
        "_core",
    ),
    "gitlab_etl.etl": ("merge_requests",),
    "gitlab_etl.jobs": (
        "universe",
        "base",
        "_db_setup",
        "_secrets",
    ),
    "gitlab_etl.jobs.universe": (("mrs", "fail_reasons"),),
}
_T = TypeVar("_T")


def raise_or_return(item: Exception | _T) -> _T:
    if isinstance(item, Exception):
        raise item
    return item


def project_dag() -> DagMap:
    return raise_or_return(DagMap.new(_dag))


def forbidden_allowlist() -> dict[FullPathModule, frozenset[FullPathModule]]:
    _raw: dict[str, frozenset[str]] = {}
    return {
        raise_or_return(FullPathModule.from_raw(k)): frozenset(
            raise_or_return(FullPathModule.from_raw(i)) for i in v
        )
        for k, v in _raw.items()
    }
