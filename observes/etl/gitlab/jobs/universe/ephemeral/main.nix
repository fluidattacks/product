{ makeScript, outputs, ... }:
makeScript {
  searchPaths = {
    bin = [ outputs."/observes/etl/gitlab/jobs/ephemeral" ];
    source = [
      outputs."/observes/common/db-creds"
      outputs."/common/utils/aws"
      outputs."/common/utils/sops"
    ];
  };
  replace = { __argInferenceConf__ = ../inference_conf.json; };
  name = "observes-etl-gitlab-universe-ephemeral";
  entrypoint = ./entrypoint.sh;
}
