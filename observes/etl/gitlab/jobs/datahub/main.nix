{ makeScript, outputs, ... }:
makeScript {
  searchPaths = {
    bin = [ outputs."/observes/etl/gitlab/jobs" ];
    source = [ outputs."/observes/common/db-creds" ];
  };
  replace = { __argInferenceConf__ = ./inference_conf.json; };
  name = "observes-etl-gitlab-datahub";
  entrypoint = ./entrypoint.sh;
}
