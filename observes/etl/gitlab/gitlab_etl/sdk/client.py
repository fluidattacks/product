from dataclasses import (
    dataclass,
)

from gitlab_sdk import Credentials

from ._client_1 import (
    get_logs,
)
from .core import (
    GitlabClient,
    ProjectId,
)


@dataclass(frozen=True)
class ClientFactory:
    @staticmethod
    def new_client(creds: Credentials, project: ProjectId) -> GitlabClient:
        return GitlabClient(lambda j, m: get_logs.get_log(creds, project, j, m))


__all__ = [
    "GitlabClient",
]
