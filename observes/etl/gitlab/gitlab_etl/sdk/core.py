from __future__ import (
    annotations,
)

from collections.abc import (
    Callable,
)
from dataclasses import (
    dataclass,
    field,
)

from fa_purity import (
    Cmd,
    FrozenList,
    Maybe,
    PureIterFactory,
    Stream,
)

from gitlab_etl import (
    _utils,
)


@dataclass(frozen=True)
class _Private:
    pass


@dataclass(frozen=True)
class JobId:
    id_obj: int


@dataclass(frozen=True)
class ProjectId:
    id_obj: int


@dataclass(frozen=True)
class Limit:
    max_items: Maybe[int]


@dataclass(frozen=True)
class GitlabEndpoint:
    _private: _Private = field(repr=False, hash=False, compare=False)
    raw: str

    @staticmethod
    def new(relative: FrozenList[str | int]) -> GitlabEndpoint:
        path = PureIterFactory.from_list(relative).map(
            lambda i: i.lstrip("/").rstrip("/") if isinstance(i, str) else _utils.int_to_str(i),
        )
        return GitlabEndpoint(_Private(), "https://gitlab.com/api/v4/" + "/".join(path))


@dataclass(frozen=True)
class GitlabClient:
    get_logs: Callable[[JobId, Limit], Cmd[Stream[str]]]
