import inspect
from collections.abc import (
    Iterable,
)
from itertools import (
    islice,
)

from etl_utils.bug import (
    Bug,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    Stream,
    Unsafe,
)
from fa_purity.json import (
    JsonObj,
    JsonPrimitiveFactory,
    JsonValue,
)
from gitlab_sdk import Credentials
from pure_requests.basic import (
    Endpoint,
    HttpClientFactory,
    Params,
)
from requests import (
    Response,
)

from gitlab_etl.sdk.core import (
    GitlabEndpoint,
    JobId,
    Limit,
    ProjectId,
)


def _headers(creds: Credentials) -> JsonObj:
    return FrozenDict(
        {"Private-Token": JsonValue.from_primitive(JsonPrimitiveFactory.from_raw(creds.api_key))},
    )


def _extract_log(response: Response, limit: Limit) -> Stream[str]:
    def _truncated(max_items: int) -> Iterable[str]:
        return (  # type: ignore[misc]
            str(line)  # type: ignore[misc]
            for line in islice(response.iter_lines(), max_items)  # type: ignore[misc]
        )

    def _not_truncated() -> Iterable[str]:
        return (str(line) for line in response.iter_lines())

    return limit.max_items.map(
        lambda m: Unsafe.stream_from_cmd(Cmd.wrap_impure(lambda: _truncated(m))),
    ).or_else_call(lambda: Unsafe.stream_from_cmd(Cmd.wrap_impure(lambda: _not_truncated())))


def get_log(
    creds: Credentials,
    project_id: ProjectId,
    job: JobId,
    limit: Limit,
) -> Cmd[Stream[str]]:
    endpoint = GitlabEndpoint.new(("projects", project_id.id_obj, "jobs", job.id_obj, "trace"))
    return (
        HttpClientFactory.new_client(None, _headers(creds), True)
        .get(Endpoint(endpoint.raw), Params(FrozenDict({})))
        .map(lambda r: r.map(lambda e: _extract_log(e, limit)))
        .map(
            lambda r: Bug.assume_success(
                "get_log",
                inspect.currentframe(),
                (str(project_id.id_obj), str(job.id_obj)),
                r,
            ),
        )
    )
