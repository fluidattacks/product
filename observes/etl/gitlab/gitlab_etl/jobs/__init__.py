from ._secrets import SupportedRepos

__all__ = [
    "SupportedRepos",
]
