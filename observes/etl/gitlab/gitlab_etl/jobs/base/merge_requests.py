import inspect
import logging

from connection_manager import (
    ClientAdapter,
    DbClients,
)
from etl_utils.bug import Bug
from etl_utils.natural import NaturalOperations
from etl_utils.parallel import ThreadPool
from fa_purity import (
    Cmd,
    CmdSmash,
    CmdTransform,
    NewFrozenList,
    ResultE,
    UnitType,
    Unsafe,
)
from gitlab_sdk import Credentials
from gitlab_sdk.ids import MrInternalId, ProjectId
from gitlab_sdk.merge_requests import MrClientFactory

from gitlab_etl import _utils
from gitlab_etl._s3 import S3URI, PureS3Client, S3Factory
from gitlab_etl.db.merge_request import MrDbClient, MrDbClientFactory
from gitlab_etl.etl.merge_requests import MrEtl
from gitlab_etl.state import Completeness, ContinuousMrRanges, MrCompleteness, MrIdRange
from gitlab_etl.state.decode import decode_mr_completeness
from gitlab_etl.state.encode import encode_completeness

LOG = logging.getLogger(__name__)


def _mrs_etl_job(s3: PureS3Client, etl: MrEtl, state_file: S3URI) -> Cmd[ResultE[None]]:
    mr_state = s3.load_json(state_file).map(lambda r: r.bind(decode_mr_completeness))
    return CmdTransform.chain_cmd_result(
        mr_state,
        lambda s1: etl.full_etl(s1, NaturalOperations.absolute(1000)).bind(
            lambda s2: s3.save(state_file, encode_completeness(s2)),
        ),
    ).map(_utils.merge_failures)


def _new_db_client(clients: DbClients) -> Cmd[MrDbClient]:
    return (
        clients.connection.cursor(LOG)
        .map(clients.new_table_client)
        .map(ClientAdapter.snowflake_table_client_adapter)
        .map(MrDbClientFactory.new)
    )


def mrs_etl_job(
    clients: DbClients,
    token: Credentials,
    project_id: ProjectId,
    state_file: S3URI,
) -> Cmd[ResultE[None]]:
    mr_etl = ThreadPool.new(100).bind(
        lambda pool: _new_db_client(clients).map(
            lambda db: MrEtl(MrClientFactory.new(token), db, project_id, pool),
        ),
    )
    return CmdSmash.smash_cmds_2(
        S3Factory.new_s3_client(),
        mr_etl,
    ).bind(lambda t: _mrs_etl_job(*t, state_file))


def init_mrs_etl(clients: DbClients) -> Cmd[ResultE[UnitType]]:
    return (
        _new_db_client(clients)
        .bind(lambda db: db.init_tables)
        .map(lambda r: r.alt(lambda e: Bug.new("init_mrs_etl", inspect.currentframe(), e, ())))
    )


def init_state(s3: PureS3Client, state_file: S3URI) -> Cmd[ResultE[None]]:
    init = (
        MrCompleteness.new(
            ContinuousMrRanges.new(
                NewFrozenList.new(
                    MrIdRange.single(MrInternalId(NaturalOperations.absolute(1))),
                ),
            )
            .alt(Unsafe.raise_exception)
            .to_union(),
            NewFrozenList.new(
                Completeness.MISSING,
            ),
        )
        .alt(Unsafe.raise_exception)
        .to_union()
    )
    return s3.save(state_file, encode_completeness(init))
