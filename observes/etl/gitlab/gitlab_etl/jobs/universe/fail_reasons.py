import logging

from fa_purity import (
    Cmd,
    CmdTransform,
    Result,
    ResultE,
)

from gitlab_etl import (
    _utils,
    fail_reasons,
)
from gitlab_etl.db import (
    new_db_client,
)
from gitlab_etl.jobs import (
    _db_setup,
)
from gitlab_etl.jobs._secrets import SupportedRepos, get_gitlab_token
from gitlab_etl.sdk.core import (
    ProjectId,
)

LOG = logging.getLogger(__name__)
PROJECT = ProjectId(20741933)


def init_fail_reasons() -> Cmd[ResultE[None]]:
    return _db_setup.with_connection(
        lambda c: c.connection.cursor(LOG).bind(
            lambda sql: new_db_client(sql, PROJECT).init_fail_reason_table.map(
                lambda n: Result.success(n),
            ),
        ),
    )


def process_fail_reasons(repo: SupportedRepos) -> Cmd[ResultE[None]]:
    return _db_setup.get_target_date().bind(
        lambda d: CmdTransform.chain_cmd_result(
            get_gitlab_token(repo),
            lambda token: _db_setup.with_connection(
                lambda clients: fail_reasons.fail_reasons(
                    clients.connection,
                    token,
                    PROJECT,
                    d,
                ).map(lambda n: Result.success(n)),
            ),
        ).map(_utils.merge_failures),
    )
