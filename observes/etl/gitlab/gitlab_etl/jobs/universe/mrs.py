import inspect
import logging

from etl_utils.bug import Bug
from etl_utils.natural import NaturalOperations
from fa_purity import (
    Cmd,
    CmdTransform,
    ResultE,
    UnitType,
)
from gitlab_sdk.ids import ProjectId

from gitlab_etl import _utils
from gitlab_etl._s3 import S3URI, S3Factory
from gitlab_etl.jobs._db_setup import with_connection
from gitlab_etl.jobs._secrets import SupportedRepos, get_gitlab_token
from gitlab_etl.jobs.base.merge_requests import init_mrs_etl, init_state, mrs_etl_job

LOG = logging.getLogger(__name__)
UNIVERSE_PROJECT = ProjectId(NaturalOperations.absolute(20741933))
MRS_STATE_FILE = S3URI("observes.state", "gitlab_etl/etl_state_v2.json")


def mrs_job() -> Cmd[ResultE[None]]:
    return CmdTransform.chain_cmd_result(
        get_gitlab_token(SupportedRepos.UNIVERSE),
        lambda token: with_connection(
            lambda c: mrs_etl_job(c, token, UNIVERSE_PROJECT, MRS_STATE_FILE),
        ),
    ).map(_utils.merge_failures)


def init_mrs_job() -> Cmd[ResultE[UnitType]]:
    return with_connection(
        init_mrs_etl,
    ).map(lambda r: r.alt(lambda e: Bug.new("init_mrs_etl", inspect.currentframe(), e, ())))


def init_state_job() -> Cmd[ResultE[None]]:
    return S3Factory.new_s3_client().bind(lambda c: init_state(c, MRS_STATE_FILE))
