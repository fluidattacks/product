from __future__ import annotations

from dataclasses import dataclass
from enum import Enum

from etl_utils.natural import Natural
from fa_purity import (
    Bool,
    Maybe,
    NewFrozenList,
    PureIter,
    PureIterFactory,
    PureIterTransform,
    Result,
    ResultE,
    Unsafe,
    cast_exception,
)
from gitlab_sdk.ids import MrInternalId


@dataclass(frozen=True)
class MrIdRange:
    """Ensures that its properties comply with:  start < end."""

    @dataclass(frozen=True)
    class _Private:
        pass

    _private: MrIdRange._Private
    start: MrInternalId
    end: MrInternalId

    @staticmethod
    def from_endpoints(start: MrInternalId, end: MrInternalId) -> ResultE[MrIdRange]:
        if end.internal.value > start.internal.value:
            return Result.success(MrIdRange(MrIdRange._Private(), start, end))
        return Result.failure(
            ValueError("`MrIdRange` must comply with `start < end`."),
        )

    @classmethod
    def single(cls, item: MrInternalId) -> MrIdRange:
        return (
            cls.from_endpoints(
                item,
                MrInternalId(Natural.succ(item.internal)),
            )
            .alt(Unsafe.raise_exception)
            .to_union()
        )

    def to_iter(self) -> PureIter[MrInternalId]:
        generator = PureIterFactory.infinite_gen(
            lambda i: MrInternalId(Natural.succ(i.internal)),
            self.start,
        ).map(
            lambda i: Bool.from_primitive(i.internal.value < self.end.internal.value).map(
                lambda _: Maybe.some(i),
                lambda _: Maybe.empty(),
            ),
        )
        return PureIterTransform.until_empty(generator)


@dataclass(frozen=True)
class ContinuousMrRanges:
    """
    Ensures that the list of mr ranges comply the following.

    For each mr range in the list.

    - the start is equal to the previous mr range end (if exist).
    """

    @dataclass(frozen=True)
    class _Private:
        pass

    @dataclass(frozen=True)
    class _State:
        previous: Maybe[MrIdRange]
        is_valid: bool

    _private: ContinuousMrRanges._Private
    ranges: NewFrozenList[MrIdRange]

    @staticmethod
    def new(ranges: NewFrozenList[MrIdRange]) -> ResultE[ContinuousMrRanges]:
        init: ContinuousMrRanges._State = ContinuousMrRanges._State(Maybe.empty(), True)

        def _gen(
            state: ContinuousMrRanges._State,
            current: MrIdRange,
        ) -> tuple[ContinuousMrRanges._State, ContinuousMrRanges._State]:
            state = ContinuousMrRanges._State(
                Maybe.some(current),
                state.previous.map(lambda p: p.end == current.start).value_or(
                    state.is_valid,
                ),
            )
            return (state, state)

        break_point = (
            PureIterFactory.from_list(ranges.items)
            .generate(
                _gen,
                init,
            )
            .find_first(lambda s: not s.is_valid)
            .bind(lambda s: s.previous)
        )
        return break_point.map(
            lambda d: Result.failure(ValueError(f"Breakpoint after {d}")).alt(cast_exception),
        ).or_else_call(
            lambda: Result.success(ContinuousMrRanges(ContinuousMrRanges._Private(), ranges)),
        )

    @classmethod
    def single(cls, item: MrIdRange) -> ContinuousMrRanges:
        return cls.new(NewFrozenList.new(item)).alt(Unsafe.raise_exception).to_union()


class Completeness(Enum):
    COMPLETED = "completed"
    MISSING = "missing"
    ERROR = "error"


@dataclass(frozen=True)
class MrCompleteness:
    """
    Type that represents mr ranges completeness.

    Since the `MrIdRange` endpoints are different by definition,
    to represent a single mr id completeness, ranges must be
    interpreted as closed-open intervals.

    In this case completeness over a range `(a, b)` does NOT include `b` itself,
    and therefore a single mr id `x` should be represented as the range `(x, x + 1)`
    """

    @dataclass(frozen=True)
    class _Private:
        pass

    _private: MrCompleteness._Private
    items: ContinuousMrRanges
    completeness: NewFrozenList[Completeness]

    @staticmethod
    def new(
        items: ContinuousMrRanges,
        completeness: NewFrozenList[Completeness],
    ) -> ResultE[MrCompleteness]:
        if len(items.ranges.items) == len(completeness.items):
            return Result.success(MrCompleteness(MrCompleteness._Private(), items, completeness))
        err = "Items of `MrCompleteness` does not match"
        return Result.failure(ValueError(err))

    def tagged_ranges(self) -> PureIter[tuple[MrIdRange, Completeness]]:
        return (
            PureIterFactory.from_list(self.items.ranges.items)
            .enumerate(0)
            .map(lambda t: (t[1], self.completeness.items[t[0]]))
        )
