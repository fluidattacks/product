from fa_purity import FrozenDict
from fa_purity.json import JsonObj, JsonPrimitiveFactory, JsonValue, JsonValueFactory

from gitlab_etl.state._core import Completeness, MrCompleteness, MrIdRange


def _int_to_str(value: int) -> str:
    return str(value)


def _encode_obj(obj_type: str, value: JsonValue) -> JsonObj:
    return FrozenDict(
        {"type": JsonValue.from_primitive(JsonPrimitiveFactory.from_raw(obj_type)), "value": value},
    )


def _encode_range(item: MrIdRange) -> JsonObj:
    encoded = JsonValueFactory.from_dict(
        {
            "start": item.start.internal.value,
            "end": item.end.internal.value,
        },
    )
    return _encode_obj("MrIdRange", encoded)


def _encode_pair(item: MrIdRange, completeness: Completeness) -> JsonObj:
    return FrozenDict(
        {
            "range": JsonValueFactory.from_unfolded(_encode_range(item)),
            "completeness": JsonValueFactory.from_unfolded(completeness.value),
        },
    )


def encode_completeness(state: MrCompleteness) -> JsonObj:
    encoded = (
        state.tagged_ranges()
        .map(lambda t: JsonValue.from_json(_encode_pair(*t)))
        .transform(lambda p: JsonValue.from_list(p.to_list()))
    )
    return _encode_obj("MrCompleteness", encoded)
