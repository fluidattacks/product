import logging

from fa_purity import (
    Cmd,
    Maybe,
    PureIterFactory,
    Stream,
    StreamTransform,
)
from gitlab_sdk import Credentials
from snowflake_client import (
    SnowflakeConnection,
)

from gitlab_etl.db.core import (
    DateRange,
)
from gitlab_etl.sdk.client import (
    ClientFactory,
)

from .core import (
    FailReason,
)
from .db import (
    FailReasonDbClient,
    new_db_client,
)
from .sdk.core import (
    GitlabClient,
    JobId,
    Limit,
    ProjectId,
)

LOG = logging.getLogger(__name__)


def _fail_reason(line: str) -> FailReason:
    if "ERROR: Preparation failed: exit status 1" in line:
        return FailReason.CI_ERROR
    if "Cannot connect to the Docker daemon" in line:
        return FailReason.AWS_INTERRUPTION
    return FailReason.UNKNOWN


def determine_fail_reason(log_lines: Stream[str]) -> Cmd[FailReason]:
    return (
        log_lines.map(lambda i: _fail_reason(i))
        .find_first(lambda f: f is not FailReason.UNKNOWN)
        .map(lambda m: m.value_or(FailReason.UNKNOWN))
    )


def _process_job(client: FailReasonDbClient, gitlab: GitlabClient, job: JobId) -> Cmd[None]:
    saving = Cmd.wrap_impure(lambda: LOG.info("saving %s", job))
    return (
        gitlab.get_logs(job, Limit(Maybe.empty()))
        .bind(lambda logs: determine_fail_reason(logs))
        .bind(lambda f: saving + client.save_fail_reason_if_not_exist(job, f))
    )


def process_fail_reasons(
    fetch_client: FailReasonDbClient,
    save_client: FailReasonDbClient,
    gitlab: GitlabClient,
    dates: DateRange,
) -> Cmd[None]:
    start = Cmd.wrap_impure(lambda: LOG.info("process_fail_reasons between %s", dates))
    return start + fetch_client.get_jobs_created_between(dates).bind(
        lambda s: StreamTransform.chain(s.map(lambda x: PureIterFactory.from_list(x)))
        .map(lambda j: _process_job(save_client, gitlab, j))
        .transform(StreamTransform.consume),
    )


def fail_reasons(
    connection: SnowflakeConnection,
    gitlab_creds: Credentials,
    project: ProjectId,
    dates: DateRange,
) -> Cmd[None]:
    new_client = connection.cursor(LOG).map(lambda sql: new_db_client(sql, project))
    return new_client.bind(
        lambda client_1: new_client.bind(
            lambda client_2: process_fail_reasons(
                client_1,
                client_2,
                ClientFactory.new_client(gitlab_creds, project),
                dates,
            ),
        ),
    )
