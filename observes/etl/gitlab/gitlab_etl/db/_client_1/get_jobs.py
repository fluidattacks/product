import inspect

from etl_utils.bug import (
    Bug,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    FrozenList,
    PureIterFactory,
    ResultE,
    ResultTransform,
    Stream,
)
from fa_purity.json import (
    JsonPrimitiveUnfolder,
)
from redshift_client.core.id_objs import (
    DbTableId,
    Identifier,
    SchemaId,
    TableId,
)
from redshift_client.sql_client import (
    DbPrimitiveFactory,
    QueryValues,
    RowData,
)
from snowflake_client import (
    SnowflakeCursor,
    SnowflakeQuery,
)

from gitlab_etl import (
    _utils,
)
from gitlab_etl.db.core import (
    DateRange,
)
from gitlab_etl.sdk.core import (
    JobId,
    ProjectId,
)

JOBS_TABLE_ID = DbTableId(
    SchemaId(Identifier.new("gitlab-ci")),
    TableId(Identifier.new("jobs")),
)


def _decode(row: RowData) -> ResultE[JobId]:
    return (
        _utils.require_index(row.data, 0)
        .bind(_utils.to_json_primitive)
        .bind(JsonPrimitiveUnfolder.to_int)
        .map(JobId)
    )


def get_jobs_created_between(
    cursor: SnowflakeCursor,
    dates: DateRange,
    project: ProjectId,
) -> Cmd[Stream[FrozenList[JobId]]]:
    stm = """
    SELECT job_id FROM {schema}.{table} WHERE
        project_id=%(project)s and
        failure_reason is not null and
        failure_reason != 'script_failure' and
        created_at >= %(after_date)s
    """ + dates.end.map(lambda _: " and created_at <= %(before_date)s").value_or("")  # noqa: S608
    # The use of value templates make this SQL query construction safe
    identifiers: FrozenDict[str, str] = FrozenDict(
        {
            "schema": JOBS_TABLE_ID.schema.name.to_str(),
            "table": JOBS_TABLE_ID.table.name.to_str(),
        },
    )
    query = Bug.assume_success(
        "get_jobs_created_between_query",
        inspect.currentframe(),
        (stm, str(identifiers)),
        SnowflakeQuery.dynamic_query(stm, identifiers),
    )
    values = DbPrimitiveFactory.from_raw_dict(
        FrozenDict(
            {
                "project": project.id_obj,
                "after_date": dates.start.date_time,
                "before_date": dates.end.map(lambda d: d.date_time).value_or(None),
            },
        ),
    )
    _get = cursor.execute(query, QueryValues(values)).map(
        lambda r: Bug.assume_success(
            "get_jobs_created_between",
            inspect.currentframe(),
            (str(query),),
            r,
        ),
    )
    return _get.map(
        lambda _: cursor.fetch_chunks_stream(1000)
        .map(
            lambda r: Bug.assume_success(
                "get_jobs_created_after_stream",
                inspect.currentframe(),
                (str(query),),
                r,
            ),
        )
        .map(
            lambda i: Bug.assume_success(
                "get_jobs_created_after_stream_decode",
                inspect.currentframe(),
                (str(i),),
                ResultTransform.all_ok(PureIterFactory.from_list(i).map(_decode).to_list()),
            ),
        ),
    )
