import inspect

from etl_utils.bug import (
    Bug,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    PureIter,
)
from redshift_client.client import (
    TableClient,
)
from redshift_client.sql_client import (
    DbPrimitiveFactory,
    Limit,
    QueryValues,
    RowData,
)
from snowflake_client import (
    SnowflakeCursor,
    SnowflakeQuery,
)

from gitlab_etl.core import (
    FailReason,
)
from gitlab_etl.sdk.core import (
    JobId,
)

from .table import (
    FAIL_REASON_TABLE_ID,
    JOB_ID_COLUMN,
    REASON_COLUMN,
    fail_reason_table,
)


def _encode_reason(job: JobId, reason: FailReason) -> RowData:
    return RowData(
        (
            DbPrimitiveFactory.from_raw(job.id_obj),
            DbPrimitiveFactory.from_raw(reason.value),
        ),
    )


def save_if_not_exist(
    cursor: SnowflakeCursor,
    items: tuple[JobId, FailReason],
) -> Cmd[None]:
    statement = """
    MERGE INTO {schema}.{table}
        USING (
            SELECT * FROM (
                VALUES (%(job_id)s, %(reason)s)
            ) virtual_table(
                {job_id_column}, {reason_column}
            )
        ) source
    ON {table}.{job_id_column} = source.{job_id_column}
        WHEN NOT MATCHED THEN INSERT ({job_id_column}, {reason_column})
        VALUES (source.{job_id_column},source.{reason_column});
    """
    identifiers: FrozenDict[str, str] = FrozenDict(
        {
            "schema": FAIL_REASON_TABLE_ID.schema.name.to_str(),
            "table": FAIL_REASON_TABLE_ID.table.name.to_str(),
            "job_id_column": JOB_ID_COLUMN.name.to_str(),
            "reason_column": REASON_COLUMN.name.to_str(),
        },
    )
    query = Bug.assume_success(
        "save_if_not_exist_query",
        inspect.currentframe(),
        (statement, str(identifiers)),
        SnowflakeQuery.dynamic_query(statement, identifiers),
    )
    values = DbPrimitiveFactory.from_raw_prim_dict(
        FrozenDict(
            {
                "job_id": items[0].id_obj,
                "reason": items[1].value,
            },
        ),
    )
    return cursor.execute(query, QueryValues(values)).map(
        lambda r: Bug.assume_success(
            "save_if_not_exist",
            inspect.currentframe(),
            (str(query), str(values)),
            r,
        ),
    )


def save_fail_reasons(client: TableClient, items: PureIter[tuple[JobId, FailReason]]) -> Cmd[None]:
    return client.insert(
        FAIL_REASON_TABLE_ID,
        fail_reason_table(),
        items.map(lambda t: _encode_reason(t[0], t[1])),
        Limit(1000),
    ).map(lambda r: Bug.assume_success("save_fail_reasons", inspect.currentframe(), (), r))
