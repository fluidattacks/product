from connection_manager import (
    ClientAdapter,
)
from snowflake_client import (
    ClientFactory,
    SnowflakeCursor,
)

from gitlab_etl.db.core import (
    FailReasonDbClient,
)
from gitlab_etl.sdk.core import (
    ProjectId,
)

from .fail_reason.save import (
    save_if_not_exist,
)
from .fail_reason.table import (
    init_table,
)
from .get_jobs import (
    get_jobs_created_between,
)


def new_db_client(cursor: SnowflakeCursor, project: ProjectId) -> FailReasonDbClient:
    table_client = ClientAdapter.snowflake_table_client_adapter(
        ClientFactory.new_table_client(cursor),
    )
    return FailReasonDbClient(
        init_table(table_client),
        lambda d: get_jobs_created_between(cursor, d, project),
        lambda j, r: save_if_not_exist(cursor, (j, r)),
    )
