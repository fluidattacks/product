from connection_manager import CommonTableClient
from fa_purity import (
    Cmd,
    CmdTransform,
    NewFrozenList,
    ResultE,
    UnitType,
)

from gitlab_etl._utils import none_result_to_unit
from gitlab_etl.db.merge_request._core import MrObj
from gitlab_etl.db.merge_request._encode import EncodedMrs

from . import _secondary, _table


def save_mrs(
    client: CommonTableClient,
    items: NewFrozenList[MrObj],
) -> Cmd[ResultE[UnitType]]:
    encoded = EncodedMrs.encode_objs(items)
    main_insert = client.named_insert(_table.MR_TABLE_ID, encoded.main_rows).map(
        none_result_to_unit,
    )
    assignees_insert = client.named_insert(
        _secondary.MR_ASSIGNEES_TABLE_ID,
        encoded.assignees_rows,
    ).map(none_result_to_unit)
    reviewer_insert = client.named_insert(
        _secondary.MR_REVIEWERS_TABLE_ID,
        encoded.reviewers_rows,
    ).map(none_result_to_unit)
    labels_insert = client.named_insert(_secondary.MR_LABELS_TABLE_ID, encoded.labels_rows).map(
        none_result_to_unit,
    )
    return CmdTransform.all_cmds_ok(
        main_insert,
        NewFrozenList.new(
            assignees_insert,
            reviewer_insert,
            labels_insert,
        ),
    )
