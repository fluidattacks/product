import inspect

from connection_manager import CommonTableClient
from etl_utils.bug import Bug
from fa_purity import Cmd, FrozenDict, ResultE, UnitType
from redshift_client.core.column import (
    Column,
    ColumnId,
)
from redshift_client.core.data_type.core import (
    DataType,
    PrecisionType,
    PrecisionTypes,
    StaticTypes,
)
from redshift_client.core.id_objs import (
    DbTableId,
    Identifier,
    SchemaId,
    TableId,
)
from redshift_client.core.table import Table
from redshift_client.sql_client import DbPrimitiveFactory

from gitlab_etl._utils import none_to_unit

MR_TABLE_ID = DbTableId(
    SchemaId(Identifier.new("gitlab-ci")),
    TableId(Identifier.new("merge_requests_new")),
)
# ---
PROJECT_ID_COLUMN = ColumnId(Identifier.new("project_id"))
INTERNAL_ID_COLUMN = ColumnId(Identifier.new("internal_id"))
GLOBAL_ID_COLUMN = ColumnId(Identifier.new("global_id"))
# ---
SHA_COLUMN = ColumnId(Identifier.new("sha"))
MERGE_COMMIT_SHA_COLUMN = ColumnId(Identifier.new("merge_commit_sha"))
SQUASH_COMMIT_SHA_COLUMN = ColumnId(Identifier.new("squash_commit_sha"))
# ---
CREATED_AT_COLUMN = ColumnId(Identifier.new("created_at"))
PREPARED_AT_COLUMN = ColumnId(Identifier.new("prepared_at"))
UPDATED_AT_COLUMN = ColumnId(Identifier.new("updated_at"))
MERGED_AT_COLUMN = ColumnId(Identifier.new("merged_at"))
CLOSED_AT_COLUMN = ColumnId(Identifier.new("closed_at"))
# ---
AUTHOR_COLUMN = ColumnId(Identifier.new("author"))
MERGE_USER_COLUMN = ColumnId(Identifier.new("merge_user"))
CLOSED_BY_COLUMN = ColumnId(Identifier.new("closed_by"))
# ---
STATE_COLUMN = ColumnId(Identifier.new("state"))
DETAILED_MERGE_STATUS_COLUMN = ColumnId(Identifier.new("detailed_merge_status"))
HAS_CONFLICTS_COLUMN = ColumnId(Identifier.new("has_conflicts"))
USER_NOTES_COUNT_COLUMN = ColumnId(Identifier.new("user_notes_count"))
MERGE_ERROR_COLUMN = ColumnId(Identifier.new("merge_error"))
# ---
TITLE_COLUMN = ColumnId(Identifier.new("title"))
DESCRIPTION_COLUMN = ColumnId(Identifier.new("description"))
DRAFT_COLUMN = ColumnId(Identifier.new("draft"))
SQUASH_COLUMN = ColumnId(Identifier.new("squash"))
IMPORTED_COLUMN = ColumnId(Identifier.new("imported"))
IMPORTED_FROM_COLUMN = ColumnId(Identifier.new("imported_from"))
FIRST_CONTRIBUTION_COLUMN = ColumnId(Identifier.new("first_contribution"))
MERGE_AFTER_COLUMN = ColumnId(Identifier.new("merge_after"))
# ---
SOURCE_PROJECT_ID_COLUMN = ColumnId(Identifier.new("source_project_id"))
SOURCE_BRANCH_COLUMN = ColumnId(Identifier.new("source_branch"))
TARGET_PROJECT_ID_COLUMN = ColumnId(Identifier.new("target_project_id"))
TARGET_BRANCH_COLUMN = ColumnId(Identifier.new("target_branch"))
# ---
MILESTONE_INTERNAL_ID_COLUMN = ColumnId(Identifier.new("milestone_internal_id"))
# ---
TASK_COMPLETION_COUNT_COLUMN = ColumnId(Identifier.new("task_completion_count"))
TASK_COMPLETION_COMPLETED_COUNT_COLUMN = ColumnId(Identifier.new("task_completion_completed_count"))


def mr_table() -> Table:
    empty = DbPrimitiveFactory.from_raw(None)
    id_column = Column(DataType(StaticTypes.BIGINT), False, empty)
    bool_column = Column(DataType(StaticTypes.BOOLEAN), False, empty)
    str_column = Column(
        DataType(PrecisionType(PrecisionTypes.VARCHAR, 256)),
        False,
        empty,
    )
    int_column = Column(DataType(StaticTypes.INTEGER), False, empty)
    nullable_str_column = Column(
        DataType(PrecisionType(PrecisionTypes.VARCHAR, 256)),
        True,
        empty,
    )
    nullable_id_column = Column(DataType(StaticTypes.BIGINT), True, empty)
    nullable_sha_column = Column(
        DataType(PrecisionType(PrecisionTypes.VARCHAR, 64)),
        True,
        empty,
    )
    nullable_date_time = Column(
        DataType(StaticTypes.TIMESTAMPTZ),
        True,
        empty,
    )
    _columns: FrozenDict[ColumnId, Column] = FrozenDict(
        {
            PROJECT_ID_COLUMN: id_column,
            INTERNAL_ID_COLUMN: id_column,
            GLOBAL_ID_COLUMN: id_column,
            SHA_COLUMN: Column(
                DataType(PrecisionType(PrecisionTypes.VARCHAR, 64)),
                False,
                empty,
            ),
            MERGE_COMMIT_SHA_COLUMN: nullable_sha_column,
            SQUASH_COMMIT_SHA_COLUMN: nullable_sha_column,
            CREATED_AT_COLUMN: Column(
                DataType(StaticTypes.TIMESTAMPTZ),
                False,
                empty,
            ),
            PREPARED_AT_COLUMN: nullable_date_time,
            UPDATED_AT_COLUMN: nullable_date_time,
            MERGED_AT_COLUMN: nullable_date_time,
            CLOSED_AT_COLUMN: nullable_date_time,
            AUTHOR_COLUMN: id_column,
            MERGE_USER_COLUMN: nullable_id_column,
            CLOSED_BY_COLUMN: nullable_id_column,
            STATE_COLUMN: Column(
                DataType(PrecisionType(PrecisionTypes.VARCHAR, 16)),
                False,
                empty,
            ),
            DETAILED_MERGE_STATUS_COLUMN: str_column,
            HAS_CONFLICTS_COLUMN: bool_column,
            USER_NOTES_COUNT_COLUMN: int_column,
            MERGE_ERROR_COLUMN: Column(
                DataType(PrecisionType(PrecisionTypes.VARCHAR, 256)),
                True,
                empty,
            ),
            TITLE_COLUMN: str_column,
            DESCRIPTION_COLUMN: nullable_str_column,
            DRAFT_COLUMN: bool_column,
            SQUASH_COLUMN: bool_column,
            IMPORTED_COLUMN: bool_column,
            IMPORTED_FROM_COLUMN: str_column,
            FIRST_CONTRIBUTION_COLUMN: bool_column,
            MERGE_AFTER_COLUMN: nullable_date_time,
            SOURCE_PROJECT_ID_COLUMN: id_column,
            SOURCE_BRANCH_COLUMN: str_column,
            TARGET_PROJECT_ID_COLUMN: id_column,
            TARGET_BRANCH_COLUMN: str_column,
            TASK_COMPLETION_COUNT_COLUMN: int_column,
            TASK_COMPLETION_COMPLETED_COUNT_COLUMN: int_column,
        },
    )
    order = (
        PROJECT_ID_COLUMN,
        INTERNAL_ID_COLUMN,
        GLOBAL_ID_COLUMN,
        SHA_COLUMN,
        MERGE_COMMIT_SHA_COLUMN,
        SQUASH_COMMIT_SHA_COLUMN,
        CREATED_AT_COLUMN,
        PREPARED_AT_COLUMN,
        UPDATED_AT_COLUMN,
        MERGED_AT_COLUMN,
        CLOSED_AT_COLUMN,
        AUTHOR_COLUMN,
        MERGE_USER_COLUMN,
        CLOSED_BY_COLUMN,
        STATE_COLUMN,
        DETAILED_MERGE_STATUS_COLUMN,
        HAS_CONFLICTS_COLUMN,
        USER_NOTES_COUNT_COLUMN,
        MERGE_ERROR_COLUMN,
        TITLE_COLUMN,
        DESCRIPTION_COLUMN,
        DRAFT_COLUMN,
        SQUASH_COLUMN,
        IMPORTED_COLUMN,
        IMPORTED_FROM_COLUMN,
        FIRST_CONTRIBUTION_COLUMN,
        MERGE_AFTER_COLUMN,
        SOURCE_PROJECT_ID_COLUMN,
        SOURCE_BRANCH_COLUMN,
        TARGET_PROJECT_ID_COLUMN,
        TARGET_BRANCH_COLUMN,
        TASK_COMPLETION_COUNT_COLUMN,
        TASK_COMPLETION_COMPLETED_COUNT_COLUMN,
    )
    result = Table.new(order, _columns, frozenset([PROJECT_ID_COLUMN, INTERNAL_ID_COLUMN]))
    return Bug.assume_success("mr_table", inspect.currentframe(), (), result)


def init_mr_table(client: CommonTableClient) -> Cmd[ResultE[UnitType]]:
    return client.new_if_not_exist(
        MR_TABLE_ID,
        mr_table(),
    ).map(
        lambda r: r.map(none_to_unit).alt(
            lambda e: Bug.new(
                "init_mr_table",
                inspect.currentframe(),
                e,
                (),
            ),
        ),
    )
