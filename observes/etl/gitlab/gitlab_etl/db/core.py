from __future__ import (
    annotations,
)

from collections.abc import Callable
from dataclasses import (
    dataclass,
    field,
)

from fa_purity import (
    Cmd,
    FrozenList,
    Maybe,
    Stream,
)
from fa_purity.date_time import (
    DatetimeUTC,
)

from gitlab_etl.core import (
    FailReason,
)
from gitlab_etl.sdk.core import (
    JobId,
)


@dataclass(frozen=True)
class _Private:
    pass


@dataclass(frozen=True)
class DateRange:
    _private: _Private = field(repr=False, hash=False, compare=False)
    start: DatetimeUTC
    end: Maybe[DatetimeUTC]

    @staticmethod
    def only_start(start: DatetimeUTC) -> DateRange:
        return DateRange(_Private(), start, Maybe.empty())


@dataclass(frozen=True)
class FailReasonDbClient:
    init_fail_reason_table: Cmd[None]
    get_jobs_created_between: Callable[[DateRange], Cmd[Stream[FrozenList[JobId]]]]
    save_fail_reason_if_not_exist: Callable[[JobId, FailReason], Cmd[None]]
