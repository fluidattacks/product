import inspect
from typing import (
    NoReturn,
)

import click
from etl_utils.bug import (
    Bug,
)
from fa_purity import (
    Cmd,
)

from gitlab_etl.jobs import SupportedRepos
from gitlab_etl.jobs.universe import (
    fail_reasons,
)

from .mr import merge_requests


@click.command()
@click.option(
    "--repo",
    required=True,
    help="Some supported repo name",
    type=str,
)
def init(repo: str) -> NoReturn:
    cmd: Cmd[None] = fail_reasons.init_fail_reasons().map(
        lambda r: Bug.assume_success("init_fail_reasons", inspect.currentframe(), (str(repo),), r),
    )
    cmd.compute()


@click.command()
@click.option(
    "--repo",
    required=True,
    help="Some supported repo name",
    type=str,
)
def fail_reason(repo: str) -> NoReturn:
    cmd: Cmd[None] = fail_reasons.process_fail_reasons(SupportedRepos(repo.lower())).map(
        lambda r: Bug.assume_success("fail_reason", inspect.currentframe(), (str(repo),), r),
    )
    cmd.compute()


@click.group()
def main() -> None:
    # main cli entrypoint
    pass


main.add_command(init)
main.add_command(fail_reason)
main.add_command(merge_requests)
