from enum import (
    Enum,
)


class FailReason(Enum):
    CI_ERROR = "CI_ERROR"
    AWS_INTERRUPTION = "AWS_INTERRUPTION"
    UNKNOWN = "UNKNOWN"
