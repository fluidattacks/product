{ inputs, makeScript, outputs, projectPath, ... }@makes_inputs:
let
  root = projectPath inputs.observesIndex.etl.gitlab.root;
  bundle = import "${root}/entrypoint.nix" makes_inputs;
  env = bundle.env.runtime;
  common_vars = import (projectPath "/observes/common/common_vars.nix") {
    inherit projectPath;
  };
in makeScript {
  name = "gitlab-etl";
  searchPaths = {
    bin = [ env ];
    export = common_vars;
  };
  entrypoint = ''
    gitlab-etl "''${@}"
  '';
}
