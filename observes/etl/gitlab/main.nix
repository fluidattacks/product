{ inputs, makeScript, outputs, ... }:
let
  deps = with inputs.observesIndex; [
    tap.json.root
    tap.gitlab.root
    target.warehouse.root
  ];
  env = builtins.map inputs.getRuntime deps;
in makeScript {
  searchPaths = {
    bin = env;
    source = [ outputs."/observes/common/db-creds" ];
  };
  name = "observes-etl-gitlab";
  entrypoint = ./entrypoint.sh;
}
