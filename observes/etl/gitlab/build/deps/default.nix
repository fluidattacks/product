{ makes_inputs, nixpkgs, pynix, python_version, }:
let
  inherit (pynix) lib;

  layer_1 = python_pkgs:
    python_pkgs // {
      arch-lint = let
        result = import ./arch_lint.nix { inherit nixpkgs pynix python_pkgs; };
      in result.pkg;
    };
  layer_2 = python_pkgs:
    python_pkgs // {
      fa-purity = let
        result = import ./fa_purity.nix { inherit nixpkgs pynix python_pkgs; };
      in result.pkg;
    };
  layer_3 = python_pkgs:
    python_pkgs // {
      utils-logger = let
        result = import ./utils_logger.nix {
          inherit lib makes_inputs nixpkgs python_pkgs python_version;
        };
      in result.pkg;
      etl-utils = let
        result = import ./etl_utils.nix {
          inherit makes_inputs nixpkgs pynix python_pkgs;
        };
      in result.pkg;
      mypy-boto3-redshift = import ./boto3/redshift-stubs.nix lib python_pkgs;
      types-boto3 = import ./boto3/stubs.nix lib python_pkgs;
    };
  layer_4 = python_pkgs:
    python_pkgs // {
      pure-requests = let
        result = import ./pure_requests.nix {
          inherit lib makes_inputs nixpkgs python_pkgs python_version;
        };
      in result.pkg;
      connection-manager = let
        result = import ./connection_manager.nix {
          inherit lib makes_inputs nixpkgs python_pkgs python_version;
        };
      in pynix.overrideUtils.applyDeepPkgOverrides [
        python_pkgs.etl-utils
        python_pkgs.fa-purity
      ] result.pkg;
      gitlab-sdk = let
        result = import ./gitlab_sdk.nix {
          inherit lib makes_inputs nixpkgs python_pkgs python_version;
        };
      in result.pkg;
    };
  python_pkgs = pynix.utils.compose [ layer_4 layer_3 layer_2 layer_1 ]
    pynix.lib.pythonPackages;
in { inherit lib python_pkgs; }
