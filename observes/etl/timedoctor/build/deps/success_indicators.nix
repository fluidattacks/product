{ lib, makes_inputs, nixpkgs, python_pkgs, python_version, }:
let
  raw_src = makes_inputs.projectPath
    makes_inputs.inputs.observesIndex.service.success_indicators.root;
  src = import "${raw_src}/build/filter.nix" nixpkgs.nix-filter raw_src;
  bundle = import "${raw_src}/build" {
    inherit makes_inputs nixpkgs python_version src;
    pynix = makes_inputs.inputs.buildPynix {
      inherit nixpkgs;
      pythonVersion = python_version;
    };
  };
in bundle
