# PythonOverride = PythonPkgDerivation -> PythonPkgDerivation
# DerivationOverride = Derivation -> Derivation
{ lib, makes_inputs, nixpkgs, python_pkgs, python_version }@args:
let
  utils =
    import (makes_inputs.projectPath "/observes/common/override_utils.nix") {
      inherit (makes_inputs) pythonOverrideUtils;
    };
  bundles = import (makes_inputs.projectPath
    "${makes_inputs.inputs.observesIndex.common.python_pkgs}/snowflake_client")
    args;
  bundle = bundles."v3.0.2".build_bundle (default: required_deps: builder:
    builder lib (required_deps (python_pkgs // {
      inherit (default.python_pkgs) types-boto3 snowflake-connector-python;
      redshift-client =
        utils.superficial_pkg_override python_pkgs.mypy-boto3-redshift
        default.python_pkgs.redshift-client;
    })));
in import (makes_inputs.projectPath "/observes/common/bundle_patch.nix") {
  inherit (lib) buildEnv;
  inherit bundle;
}
