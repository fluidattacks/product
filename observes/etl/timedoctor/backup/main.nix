{ inputs, makeScript, outputs, projectPath, ... }:
let
  deps = with inputs.observesIndex; [
    service.success_indicators.root
    tap.timedoctor.root
  ];
  env = builtins.map inputs.getRuntime deps;
  common_vars = import (projectPath "/observes/common/common_vars.nix") {
    inherit projectPath;
  };
in makeScript {
  searchPaths = {
    bin = env
      ++ [ inputs.nixpkgs.awscli inputs.nixpkgs.coreutils inputs.nixpkgs.jq ];
    export = common_vars;
    source = [
      outputs."/common/utils/aws"
      outputs."/common/utils/sops"
      outputs."/observes/common/db-creds"
    ];
  };
  name = "observes-etl-timedoctor-backup";
  entrypoint = ./entrypoint.sh;
}
