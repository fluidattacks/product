import inspect
import logging
from dataclasses import (
    dataclass,
)

from connection_manager import (
    DbClients,
)
from dateutil.relativedelta import (
    relativedelta,
)
from etl_utils.bug import (
    Bug,
)
from etl_utils.date_range import (
    DateRange,
)
from etl_utils.parallel import (
    ThreadPool,
)
from fa_purity import (
    Cmd,
    PureIterFactory,
    Result,
    StreamTransform,
)
from fa_purity.date_time import (
    DatetimeFactory,
)
from redshift_client.core.id_objs import (
    Identifier,
    SchemaId,
)
from success_indicators import ClientFactory, SingleJob
from timedoctor_sdk.core import (
    ApiClient,
)

from timedoctor_etl.etls.state import (
    State,
)
from timedoctor_etl.uploader import (
    Initializer,
    Uploader,
    new_initializer,
    new_uploader,
)

from . import (
    _setup,
)
from ._setup import (
    CoreData,
)
from .state import (
    EtlDateRanges,
    calc_next_date_ranges,
    load_state,
    save_state,
)

LOG = logging.getLogger(__name__)
MAIN_SCHEMA = SchemaId(Identifier.new("timedoctor"))


@dataclass(frozen=True)
class EtlClients:
    client: ApiClient
    init: Initializer
    uploader: Uploader


def _computer_activity_etl(
    pool: ThreadPool,
    inputs: EtlClients,
    core: CoreData,
    dates: DateRange,
) -> Cmd[None]:
    start_msg = Cmd.wrap_impure(lambda: LOG.info("Starting computer activity ETL"))
    return start_msg + (
        inputs.init.init_activities
        + pool.in_threads_none(
            PureIterFactory.from_list(tuple(core.users_map.keys())).map(
                lambda u: inputs.client.get_activity(core.company, u, dates)
                .map(inputs.uploader.upload_activities)
                .transform(StreamTransform.consume),
            ),
        )
    )


def _worklogs_etl(
    pool: ThreadPool,
    inputs: EtlClients,
    core: CoreData,
    dates: DateRange,
) -> Cmd[None]:
    start_msg = Cmd.wrap_impure(lambda: LOG.info("Starting worklogs ETL"))
    return start_msg + (
        inputs.init.init_worklogs
        + pool.in_threads_none(
            PureIterFactory.from_list(tuple(core.users_map.keys())).map(
                lambda u: inputs.client.get_worklogs(core.company, u, dates)
                .map(inputs.uploader.upload_worklogs)
                .transform(StreamTransform.consume),
            ),
        )
    )


def _new_clients(
    db_clients: DbClients,
    client: ApiClient,
    schema: SchemaId,
    core: CoreData,
) -> Cmd[EtlClients]:
    _new_tb_client = db_clients.connection.cursor(LOG).map(db_clients.new_table_client)
    _new_init = _new_tb_client.map(lambda c: new_initializer(c, schema))
    _new_uploader = _new_tb_client.map(lambda c: new_uploader(c, schema, core.users_map))
    return _new_init.bind(
        lambda init: _new_uploader.map(lambda uploader: EtlClients(client, init, uploader)),
    )


def _start_etls(
    schema: SchemaId,
    db_clients: DbClients,
    client: ApiClient,
    ranges: EtlDateRanges,
) -> Cmd[None]:
    return ThreadPool.new(100).bind(
        lambda pool: _setup.get_core_data(client).bind(
            lambda core: _new_clients(db_clients, client, schema, core).bind(
                lambda clients: pool.in_threads_none(
                    PureIterFactory.from_list(
                        (
                            _computer_activity_etl(pool, clients, core, ranges.worklogs),
                            _worklogs_etl(pool, clients, core, ranges.worklogs),
                        ),
                    ),
                )
                + save_state(
                    State(
                        ranges.computer_activity.to_date,
                        ranges.worklogs.to_date,
                    ),
                ).map(
                    lambda r: Bug.assume_success(
                        "save_state",
                        inspect.currentframe(),
                        (),
                        r,
                    ),
                ),
            ),
        ),
    )


def start_etls(schema: SchemaId, state: State, days: int) -> Cmd[None]:
    start_msg = Cmd.wrap_impure(lambda: LOG.info("ETL executing over %s", schema))
    next_range = DatetimeFactory.date_now().map(
        lambda today: Bug.assume_success(
            "",
            inspect.currentframe(),
            (str(today), str(days)),
            calc_next_date_ranges(today, state, relativedelta(days=days)),
        ),
    )
    return start_msg + next_range.bind(
        lambda dr: _setup.with_common_setup(
            lambda d, a: _start_etls(schema, d, a, dr).map(lambda x: Result.success(x)),
        ).map(lambda r: Bug.assume_success("start_etls_setup", inspect.currentframe(), (), r)),
    )


def main_etl() -> Cmd[None]:
    update_indicator = ClientFactory.observes_client().update_single_job(SingleJob.timedoctor_etl)
    return (
        load_state()
        .map(lambda r: Bug.assume_success("load_state", inspect.currentframe(), (), r))
        .bind(lambda s: start_etls(SchemaId(Identifier.new("timedoctor")), s, 30))
    ) + update_indicator
