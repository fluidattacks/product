from pathlib import (
    Path,
)

from etl_utils.env_var import (
    require_env_var,
)
from etl_utils.secrets._core import (
    get_secrets,
)
from fa_purity import (
    Cmd,
    Result,
    ResultE,
)
from fa_purity.json import (
    JsonObj,
    JsonPrimitiveUnfolder,
    JsonUnfolder,
    JsonValue,
    Unfolder,
)
from timedoctor_sdk.auth import (
    Credentials,
)


def _decode_str(value: JsonValue) -> ResultE[str]:
    return Unfolder.to_primitive(value).bind(JsonPrimitiveUnfolder.to_str)


def _decode_secrets(secrets: JsonObj) -> ResultE[Credentials]:
    return JsonUnfolder.require(secrets, "ANALYTICS_TIMEDOCTOR_USER", _decode_str).bind(
        lambda user: JsonUnfolder.require(secrets, "ANALYTICS_TIMEDOCTOR_PASSWD", _decode_str).map(
            lambda passwd: Credentials(user, passwd),
        ),
    )


def _extract_secrets(secrets: Path) -> Cmd[ResultE[Credentials]]:
    return get_secrets(secrets).map(lambda r: r.bind(_decode_secrets))


def creds_from_env() -> Cmd[ResultE[Credentials]]:
    return require_env_var("SECRETS_FILE").bind(
        lambda r: r.to_coproduct().map(
            lambda f: _extract_secrets(Path(f)),
            lambda e: Cmd.wrap_value(Result.failure(e)),
        ),
    )
