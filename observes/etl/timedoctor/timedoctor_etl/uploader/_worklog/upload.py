import inspect
import logging

from dateutil.relativedelta import (
    relativedelta,
)
from etl_utils.bug import (
    Bug,
)
from etl_utils.retry import (
    cmd_if_fail,
    retry_cmd,
    sleep_cmd,
)
from etl_utils.typing import Dict
from fa_purity import (
    Cmd,
    Coproduct,
    FrozenDict,
    FrozenList,
    Maybe,
    PureIterFactory,
)
from fa_purity.json import (
    JsonPrimitive,
)
from redshift_client.client import (
    GroupedRows,
    TableRow,
)
from redshift_client.core.column import (
    ColumnId,
)
from redshift_client.core.id_objs import (
    DbTableId,
)
from redshift_client.sql_client import (
    DbPrimitive,
    DbPrimitiveFactory,
)
from snowflake_client import (
    TableClient,
)
from timedoctor_sdk.core import (
    UserId,
    UserName,
    Worklog,
    WorklogMode,
)

from .core import (
    DEVICE_ID,
    DURATION,
    EDITED,
    END_TIME,
    PROJECT_ID,
    PROJECT_NAME,
    START_TIME,
    TASK_ID,
    TASK_NAME,
    USER_ID,
    USER_NAME,
    WORK_MODE,
    table_definition,
)

LOG = logging.getLogger(__name__)


def _encode(user_map: FrozenDict[UserId, UserName], item: Worklog) -> TableRow:
    end_time = item.start.date_time + relativedelta(seconds=int(item.time))
    _item: Dict[ColumnId, DbPrimitive] = {
        TASK_ID: DbPrimitiveFactory.from_raw(item.worklog_id.task_id.task),
        TASK_NAME: DbPrimitiveFactory.from_raw(item.worklog_id.task_name.name),
        PROJECT_ID: DbPrimitiveFactory.from_raw(item.worklog_id.project_id.project_id),
        PROJECT_NAME: DbPrimitiveFactory.from_raw(item.worklog_id.project.name),
        DEVICE_ID: DbPrimitiveFactory.from_raw(item.worklog_id.device.device),
        USER_ID: DbPrimitiveFactory.from_raw(item.worklog_id.user.user_id),
        USER_NAME: DbPrimitiveFactory.from_raw(
            Maybe.from_optional(user_map.get(item.worklog_id.user))
            .map(lambda u: u.name)
            .value_or("unknown"),
        ),
        START_TIME: DbPrimitiveFactory.from_raw(item.start.date_time),
        DURATION: DbPrimitiveFactory.from_raw(item.time),
        WORK_MODE: DbPrimitiveFactory.from_raw(item.mode.value),
        END_TIME: DbPrimitiveFactory.from_raw(end_time),
        EDITED: Coproduct.inl(JsonPrimitive.from_bool(item.mode is WorklogMode.MANUAL)),
    }
    result = TableRow.new(table_definition(), FrozenDict(_item))
    return Bug.assume_success("_encode_worklogs", inspect.currentframe(), (str(item),), result)


def _group(user_map: FrozenDict[UserId, UserName], items: FrozenList[Worklog]) -> GroupedRows:
    result = GroupedRows.new(
        table_definition(),
        PureIterFactory.from_list(items).map(lambda i: _encode(user_map, i)).to_list(),
    )
    return Bug.assume_success("_group_worklogs", inspect.currentframe(), (), result)


def upload_worklogs(
    client: TableClient,
    table: DbTableId,
    user_map: FrozenDict[UserId, UserName],
    items: FrozenList[Worklog],
) -> Cmd[None]:
    msg = Cmd.wrap_impure(lambda: LOG.info("Uploading %s worklogs", len(items)))
    result = retry_cmd(
        client.named_insert(table, _group(user_map, items)),
        lambda i, r: cmd_if_fail(r, sleep_cmd(i**2)),
        10,
    )
    return msg + result.map(
        lambda r: Bug.assume_success("upload_worklogs", inspect.currentframe(), (), r),
    )
