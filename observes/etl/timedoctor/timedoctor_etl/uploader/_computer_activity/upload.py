import inspect
import logging

from etl_utils.bug import (
    Bug,
)
from etl_utils.retry import (
    cmd_if_fail,
    retry_cmd,
    sleep_cmd,
)
from etl_utils.typing import (
    Dict,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    FrozenList,
    PureIterFactory,
)
from redshift_client.client import (
    GroupedRows,
    TableRow,
)
from redshift_client.core.column import (
    ColumnId,
)
from redshift_client.core.id_objs import (
    DbTableId,
)
from redshift_client.sql_client import (
    DbPrimitive,
    DbPrimitiveFactory,
)
from snowflake_client import (
    TableClient,
)
from timedoctor_sdk.core import (
    ComputerActivity,
)

from .core import (
    AVG_ACTIVITY,
    DATE,
    DELETED,
    DEVICE,
    ENTITY,
    FILE_INDEX,
    META_BLUR,
    META_CLICKS,
    META_CREATED_AT,
    META_KEYS,
    META_MOVEMENTS,
    META_OBJ_TYPE,
    META_PERIOD,
    META_PROJECT_ID,
    META_TASK_ID,
    MIME_TYPE,
    USER_ID,
    table_definition,
)

LOG = logging.getLogger(__name__)


def _encode(item: ComputerActivity) -> TableRow:
    _item: Dict[ColumnId, DbPrimitive] = {
        USER_ID: DbPrimitiveFactory.from_raw(item.user.user_id),
        DATE: DbPrimitiveFactory.from_raw(item.date.date_time),
        DEVICE: DbPrimitiveFactory.from_raw(item.device.device),
        FILE_INDEX: DbPrimitiveFactory.from_raw(item.file_index),
        MIME_TYPE: DbPrimitiveFactory.from_raw(item.mime_type),
        DELETED: DbPrimitiveFactory.from_raw(item.deleted),
        AVG_ACTIVITY: DbPrimitiveFactory.from_raw(item.avg_activity.value_or(None)),
        ENTITY: DbPrimitiveFactory.from_raw(item.entity.value),
        META_BLUR: DbPrimitiveFactory.from_raw(item.meta.bind(lambda m: m.blur).value_or(None)),
        META_CLICKS: DbPrimitiveFactory.from_raw(item.meta.bind(lambda m: m.clicks).value_or(None)),
        META_MOVEMENTS: DbPrimitiveFactory.from_raw(
            item.meta.bind(lambda m: m.movements).value_or(None),
        ),
        META_KEYS: DbPrimitiveFactory.from_raw(item.meta.bind(lambda m: m.keys).value_or(None)),
        META_PERIOD: DbPrimitiveFactory.from_raw(item.meta.bind(lambda m: m.period).value_or(None)),
        META_CREATED_AT: DbPrimitiveFactory.from_raw(
            item.meta.bind(lambda m: m.created_at).map(lambda d: d.date_time).value_or(None),
        ),
        META_PROJECT_ID: DbPrimitiveFactory.from_raw(
            item.meta.bind(lambda m: m.project).map(lambda p: p.project_id).value_or(None),
        ),
        META_TASK_ID: DbPrimitiveFactory.from_raw(
            item.meta.bind(lambda m: m.task_id).map(lambda t: t.task).value_or(None),
        ),
        META_OBJ_TYPE: DbPrimitiveFactory.from_raw(
            item.meta.bind(lambda m: m.obj_type).value_or(None),
        ),
    }
    result = TableRow.new(table_definition(), FrozenDict(_item))
    return Bug.assume_success("_encode_activity", inspect.currentframe(), (str(item),), result)


def _group(items: FrozenList[ComputerActivity]) -> GroupedRows:
    result = GroupedRows.new(
        table_definition(),
        PureIterFactory.from_list(items).map(_encode).to_list(),
    )
    return Bug.assume_success("_group_activities", inspect.currentframe(), (), result)


def upload_activities(
    client: TableClient,
    table: DbTableId,
    items: FrozenList[ComputerActivity],
) -> Cmd[None]:
    msg = Cmd.wrap_impure(lambda: LOG.info("Uploading %s activities", len(items)))
    result = retry_cmd(
        client.named_insert(table, _group(items)),
        lambda i, r: cmd_if_fail(r, sleep_cmd(i**2)),
        10,
    )
    return msg + result.map(
        lambda r: Bug.assume_success("upload_activities", inspect.currentframe(), (), r),
    )
