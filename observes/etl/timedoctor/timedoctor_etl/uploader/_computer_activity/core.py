import inspect

from etl_utils.bug import (
    Bug,
)
from fa_purity import (
    Cmd,
    FrozenDict,
)
from redshift_client.core.column import (
    Column,
)
from redshift_client.core.data_type.core import (
    DataType,
    PrecisionType,
    PrecisionTypes,
    StaticTypes,
)
from redshift_client.core.id_objs import (
    ColumnId,
    DbTableId,
    Identifier,
)
from redshift_client.core.table import (
    Table,
)
from redshift_client.sql_client import (
    DbPrimitiveFactory,
)
from snowflake_client import (
    TableClient,
)

USER_ID = ColumnId(Identifier.new("user_id"))
DATE = ColumnId(Identifier.new("date"))
DEVICE = ColumnId(Identifier.new("device"))
FILE_INDEX = ColumnId(Identifier.new("file_index"))
MIME_TYPE = ColumnId(Identifier.new("mime_type"))
DELETED = ColumnId(Identifier.new("deleted"))
AVG_ACTIVITY = ColumnId(Identifier.new("avg_activity"))
ENTITY = ColumnId(Identifier.new("entity"))
META_BLUR = ColumnId(Identifier.new("meta_blur"))
META_CLICKS = ColumnId(Identifier.new("meta_clicks"))
META_MOVEMENTS = ColumnId(Identifier.new("meta_movements"))
META_KEYS = ColumnId(Identifier.new("meta_keys"))
META_PERIOD = ColumnId(Identifier.new("meta_period"))
META_CREATED_AT = ColumnId(Identifier.new("meta_created_at"))
META_PROJECT_ID = ColumnId(Identifier.new("meta_project_id"))
META_TASK_ID = ColumnId(Identifier.new("meta_task_id"))
META_OBJ_TYPE = ColumnId(Identifier.new("meta_obj_type"))


def _var_char(limit: int) -> DataType:
    return DataType(PrecisionType(PrecisionTypes.VARCHAR, limit))


def table_definition() -> Table:
    none = DbPrimitiveFactory.from_raw(None)
    columns: FrozenDict[ColumnId, Column] = FrozenDict(
        {
            USER_ID: Column(_var_char(256), False, none),
            DATE: Column(DataType(StaticTypes.TIMESTAMPTZ), False, none),
            DEVICE: Column(_var_char(256), False, none),
            FILE_INDEX: Column(_var_char(256), False, none),
            MIME_TYPE: Column(_var_char(256), False, none),
            DELETED: Column(DataType(StaticTypes.BOOLEAN), False, none),
            AVG_ACTIVITY: Column(DataType(StaticTypes.INTEGER), True, none),
            ENTITY: Column(_var_char(256), False, none),
            META_BLUR: Column(DataType(StaticTypes.BOOLEAN), True, none),
            META_CLICKS: Column(DataType(StaticTypes.INTEGER), True, none),
            META_MOVEMENTS: Column(DataType(StaticTypes.INTEGER), True, none),
            META_KEYS: Column(DataType(StaticTypes.INTEGER), True, none),
            META_PERIOD: Column(DataType(StaticTypes.INTEGER), True, none),
            META_CREATED_AT: Column(DataType(StaticTypes.TIMESTAMPTZ), True, none),
            META_PROJECT_ID: Column(_var_char(256), True, none),
            META_TASK_ID: Column(_var_char(256), True, none),
            META_OBJ_TYPE: Column(_var_char(256), True, none),
        },
    )
    _order = (
        USER_ID,
        DATE,
        DEVICE,
        FILE_INDEX,
        MIME_TYPE,
        DELETED,
        AVG_ACTIVITY,
        ENTITY,
        META_BLUR,
        META_CLICKS,
        META_MOVEMENTS,
        META_KEYS,
        META_PERIOD,
        META_CREATED_AT,
        META_PROJECT_ID,
        META_TASK_ID,
        META_OBJ_TYPE,
    )
    table = Table.new(
        _order,
        columns,
        frozenset([USER_ID, DATE, DEVICE, FILE_INDEX, ENTITY]),
    )
    return Bug.assume_success("activities_table_def", inspect.currentframe(), (), table)


def init_table(client: TableClient, table_id: DbTableId) -> Cmd[None]:
    table = table_definition()
    result = client.new_if_not_exist(
        table_id,
        table,
    )
    return result.map(
        lambda r: Bug.assume_success(
            "init_activities_table_execution",
            inspect.currentframe(),
            (),
            r,
        ),
    )
