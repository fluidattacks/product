from dataclasses import (
    dataclass,
)

from etl_utils.typing import (
    Callable,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    FrozenList,
)
from redshift_client.core.id_objs import (
    DbTableId,
    Identifier,
    SchemaId,
    TableId,
)
from snowflake_client import (
    TableClient,
)
from timedoctor_sdk.core import (
    ComputerActivity,
    UserId,
    UserName,
    Worklog,
)

from ._computer_activity.core import (
    init_table as init_activities,
)
from ._computer_activity.upload import (
    upload_activities,
)
from ._worklog.core import (
    init_table as init_worklog,
)
from ._worklog.upload import (
    upload_worklogs,
)

ACTIVITIES_TABLE = TableId(Identifier.new("computer_activity_new"))
WORKLOGS_TABLE = TableId(Identifier.new("worklogs_new"))


@dataclass(frozen=True)
class Initializer:
    init_activities: Cmd[None]
    init_worklogs: Cmd[None]


@dataclass(frozen=True)
class Uploader:
    upload_activities: Callable[[FrozenList[ComputerActivity]], Cmd[None]]
    upload_worklogs: Callable[[FrozenList[Worklog]], Cmd[None]]


def new_initializer(client: TableClient, schema: SchemaId) -> Initializer:
    return Initializer(
        init_activities(client, DbTableId(schema, ACTIVITIES_TABLE)),
        init_worklog(client, DbTableId(schema, WORKLOGS_TABLE)),
    )


def new_uploader(
    client: TableClient,
    schema: SchemaId,
    user_map: FrozenDict[UserId, UserName],
) -> Uploader:
    return Uploader(
        lambda i: upload_activities(client, DbTableId(schema, ACTIVITIES_TABLE), i),
        lambda i: upload_worklogs(client, DbTableId(schema, WORKLOGS_TABLE), user_map, i),
    )
