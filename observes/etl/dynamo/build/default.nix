{ makes_inputs, nixpkgs, python_version, src, }:
let
  deps = import ./deps { inherit makes_inputs nixpkgs python_version; };
  pkgDeps = {
    runtime_deps = with deps.python_pkgs; [
      boto3
      click
      etl-utils
      fa-purity
      target-warehouse
      utils-logger
    ];
    build_deps = with deps.python_pkgs; [ flit-core ];
    test_deps = with deps.python_pkgs; [ mypy pylint pytest pytest-cov ];
  };
  packages = makes_inputs.makePythonPyprojectPackage {
    inherit (deps.lib) buildEnv buildPythonPackage;
    inherit pkgDeps src;
  };
  homeless_patch = pkg:
    pkg.overrideAttrs (finalAttrs: previousAttrs: {
      postPhases = [ "rmHomelessShelter" ];
      rmHomelessShelter = ''
        rm -rf /homeless-shelter
      '';
    });
  patched_env =
    nixpkgs.lib.attrsets.mapAttrs (name: homeless_patch) packages.env;
in packages // { env = patched_env; }
