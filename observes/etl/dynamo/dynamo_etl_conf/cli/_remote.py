import click
from dynamo_etl_conf._core import (
    Segment,
)
from dynamo_etl_conf.executor import (
    new_remote_executor,
)
from fa_purity import (
    Cmd,
    Unsafe,
)
import logging
from typing import (
    NoReturn,
)

LOG = logging.getLogger(__name__)


@click.command()
@click.argument("total", type=int)
@click.argument("segment", type=str)
def phase_1(total: int, segment: str) -> NoReturn:
    seg = Segment.from_raw(segment).alt(Unsafe.raise_exception).to_union()
    executor = new_remote_executor()
    cmd: Cmd[None] = executor.phase_1(total, seg).map(
        lambda r: r.alt(Unsafe.raise_exception).to_union()
    )
    cmd.compute()


@click.command()
def phase_2() -> NoReturn:
    executor = new_remote_executor()
    cmd: Cmd[None] = executor.phase_2.map(
        lambda r: r.alt(Unsafe.raise_exception).to_union()
    )
    cmd.compute()


@click.command()
def phase_3() -> NoReturn:
    executor = new_remote_executor()
    cmd: Cmd[None] = executor.full_phase_3.map(
        lambda r: r.alt(Unsafe.raise_exception).to_union()
    )
    cmd.compute()


@click.command()
def phase_4() -> NoReturn:
    executor = new_remote_executor()
    cmd: Cmd[None] = executor.phase_4.map(
        lambda r: r.alt(Unsafe.raise_exception).to_union()
    )
    cmd.compute()


@click.command()
@click.option(
    "--use-snowflake",
    default=False,
    is_flag=True,
)
def full_pipeline(
    use_snowflake: bool,  # pylint: disable=unused-argument
) -> NoReturn:
    executor = new_remote_executor()
    cmd: Cmd[None] = executor.full_pipeline.map(
        lambda r: r.alt(Unsafe.raise_exception).to_union()
    )
    cmd.compute()


@click.group()
def remote() -> None:
    # `remote` cli group
    pass


remote.add_command(phase_1)
remote.add_command(phase_2)
remote.add_command(phase_3)
remote.add_command(phase_4)
remote.add_command(full_pipeline)
