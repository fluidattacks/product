from __future__ import (
    annotations,
)

from collections.abc import (
    Iterable,
)
from fa_purity import (
    Cmd,
    CmdUnwrapper,
    PureIter,
    Result,
    ResultFactory,
    Stream,
    Unsafe,
)
import logging
from typing import (
    Callable,
    TypeVar,
)

LOG = logging.getLogger(__name__)
_T = TypeVar("_T")
_S = TypeVar("_S")
_A = TypeVar("_A")
_F = TypeVar("_F")


def append_to_stream(stream: Stream[_T], item: _T) -> Stream[_T]:
    def _iter(unwrapper: CmdUnwrapper) -> Iterable[_T]:
        yield from unwrapper.act(Unsafe.stream_to_iter(stream))
        yield item

    new_iter = Cmd.new_cmd(_iter)
    return Unsafe.stream_from_cmd(new_iter)


def join_stream(stream_1: Stream[_T], stream_2: Stream[_T]) -> Stream[_T]:
    def _iter(unwrapper: CmdUnwrapper) -> Iterable[_T]:
        yield from unwrapper.act(Unsafe.stream_to_iter(stream_1))
        yield from unwrapper.act(Unsafe.stream_to_iter(stream_2))

    new_iter = Cmd.new_cmd(_iter)
    return Unsafe.stream_from_cmd(new_iter)


def chain_cmd_result(
    cmd_1: Cmd[Result[_S, _F]], cmd_2: Callable[[_S], Cmd[Result[_A, _F]]]
) -> Cmd[Result[_A, _F]]:
    factory: ResultFactory[_A, _F] = ResultFactory()
    return cmd_1.bind(
        lambda r: r.map(cmd_2)
        .alt(lambda e: Cmd.wrap_value(factory.failure(e)))
        .to_union()
    )


def consume_results(
    cmds: PureIter[Cmd[Result[None, _F]]]
) -> Cmd[Result[None, _F]]:
    def _action(unwrapper: CmdUnwrapper) -> Result[None, _F]:
        for cmd in cmds:
            result = unwrapper.act(cmd)
            success = result.map(lambda _: True).value_or(False)
            if not success:
                return result
        return Result.success(None)

    return Cmd.new_cmd(_action)


def consume_result_stream(
    results: Stream[Result[_T, _F]], success_result: Result[_T, _F]
) -> Cmd[Result[_T, _F]]:
    def _action(unwrapper: CmdUnwrapper) -> Result[_T, _F]:
        for result in unwrapper.act(Unsafe.stream_to_iter(results)):
            success = result.map(lambda _: True).value_or(False)
            if not success:
                return result
        return success_result

    return Cmd.new_cmd(_action)
