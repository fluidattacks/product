{ inputs, makePythonVscodeSettings, projectPath, ... }@makes_inputs:
let
  root = projectPath inputs.observesIndex.etl.google_sheets.root;
  bundle = import "${root}/entrypoint.nix" makes_inputs;
in makePythonVscodeSettings {
  env = bundle.env.dev;
  bins = bundle.bin_deps;
  name = "observes-etl-google-sheets-env-dev";
}
