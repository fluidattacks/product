{ inputs, projectPath, ... }@makes_inputs:
let
  python_version = "python311";
  nixpkgs = inputs.nixpkgs-observes // { inherit (inputs) nix-filter; };
  patch_src = src:
    builtins.derivation {
      inherit src;
      etl_secrets = projectPath "/observes/secrets/prod.yaml";
      name = "patched_src";
      system = builtins.currentSystem;
      builder = "${nixpkgs.bash}/bin/bash";
      args = [ ./build/patch.sh ];
      PATH = "${nixpkgs.coreutils}/bin";
    };
  out = import ./build {
    inherit makes_inputs nixpkgs python_version;
    src = nixpkgs.nix-filter {
      root = patch_src ./.;
      include =
        [ "google_sheets_etl" "tests" "pyproject.toml" "mypy.ini" ".pylintrc" ];
    };
  };
in out
