from . import (
    _conf,
)
from ._get import (
    get_secret,
)
from dataclasses import (
    dataclass,
)
from fa_purity import (
    Cmd,
    FrozenDict,
)
from fa_purity.json_2 import (
    JsonObj,
    JsonPrimitiveUnfolder,
    JsonValue,
    Unfolder,
)
from google_sheets_etl._core import (
    SheetId,
)
from google_sheets_etl.bin_sdk.tap import (
    TapConfig,
)
from google_sheets_etl.bin_sdk.target import (
    TargetWarehouseConf,
)
from google_sheets_etl.utils import (
    PkgPath,
)
import os
from pathlib import (
    Path,
)


@dataclass(frozen=True)
class SecretManager:
    @staticmethod
    def _raw_conf() -> Cmd[JsonObj]:
        return get_secret(PkgPath.new(Path("secrets/data/conf.json")).path)

    @staticmethod
    def get_sheet_ids() -> Cmd[FrozenDict[str, SheetId]]:
        return get_secret(
            PkgPath.new(Path("secrets/data/sheet_ids.json")).path
        ).map(
            lambda j: Unfolder.to_dict_of(
                JsonValue.from_json(j),
                lambda v: Unfolder.to_primitive(v)
                .bind(JsonPrimitiveUnfolder.to_str)
                .map(SheetId),
            ).unwrap()
        )

    @classmethod
    def tap_conf(cls) -> Cmd[TapConfig]:
        return cls._raw_conf().map(lambda r: _conf.decode_conf(r).unwrap())

    @staticmethod
    def prod_secrets() -> Cmd[JsonObj]:
        # [INFO] This prod.yaml is auto copied from observes secrets at build time
        prod_path = PkgPath.new(Path("secrets/data/prod.yaml")).path
        secrets_path = (
            prod_path
            if prod_path.exists()
            else Path(os.environ["OBSERVES_SECRETS"])
        )
        return get_secret(secrets_path)

    @classmethod
    def target_conf(cls) -> Cmd[TargetWarehouseConf]:
        return cls.prod_secrets().map(
            lambda _: TargetWarehouseConf()
        )
