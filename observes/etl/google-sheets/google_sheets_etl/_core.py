from dataclasses import (
    dataclass,
)


@dataclass
class SheetId:
    raw: str

    def __repr__(self) -> str:
        return "[masked]"
