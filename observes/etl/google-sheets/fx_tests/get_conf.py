from google_sheets_etl.bin_sdk.tap import (
    TapConfig,
)
from google_sheets_etl.bin_sdk.target import (
    TargetWarehouseConf,
)
from google_sheets_etl.secrets import (
    SecretManager,
)
from google_sheets_etl.utils.cache import (
    Cache,
)

_cache: Cache[TapConfig] = Cache(None)
_cache_2: Cache[TargetWarehouseConf] = Cache(None)


def get_conf() -> TapConfig:
    return _cache.get_or_set(SecretManager.tap_conf())


def get_db_conf() -> TargetWarehouseConf:
    return _cache_2.get_or_set(SecretManager.target_conf())
