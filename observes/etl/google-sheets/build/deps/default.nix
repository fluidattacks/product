{ makes_inputs, nixpkgs, python_version, }:
let
  lib = {
    buildEnv = nixpkgs."${python_version}".buildEnv.override;
    inherit (nixpkgs."${python_version}".pkgs) buildPythonPackage;
    inherit (nixpkgs.python3Packages) fetchPypi;
  };
  _makes_inputs = makes_inputs // {
    inherit (makes_inputs.inputs) observesIndex nix-filter;
  };
  utils = makes_inputs.pythonOverrideUtils;

  layer_1 = python_pkgs:
    python_pkgs // {
      arch-lint = let
        result = import ./arch_lint.nix {
          inherit lib makes_inputs nixpkgs python_pkgs python_version;
        };
      in result.pkg;
    };
  layer_2 = python_pkgs:
    python_pkgs // {
      fa-purity = let
        result = import ./fa_purity.nix {
          inherit lib makes_inputs nixpkgs python_pkgs python_version;
        };
      in result.pkg;
    };
  layer_3 = python_pkgs:
    python_pkgs // {
      utils-logger = let
        result = import ./utils_logger.nix {
          inherit lib makes_inputs nixpkgs python_pkgs python_version;
        };
      in result.pkg;
    };

  python_pkgs = utils.compose [ layer_3 layer_2 layer_1 ]
    nixpkgs."${python_version}Packages";
  new_nixpkgs = nixpkgs // {
    tap-google-sheets =
      import ./tap-google-sheets.nix { inherit lib makes_inputs nixpkgs; };
    target-warehouse = import ./target-warehouse.nix {
      inherit lib nixpkgs;
      makes_inputs = _makes_inputs;
    };
  };
in {
  inherit lib python_pkgs;
  nixpkgs = new_nixpkgs;
}
