{ lib, makes_inputs, nixpkgs, }:
let
  root =
    makes_inputs.projectPath makes_inputs.observesIndex.target.warehouse.root;
  bundle = import "${root}/entrypoint.nix" makes_inputs;
  utils = import ./bin_utils.nix;
in utils.wrap_binary {
  name = "wrapped-target-warehouse";
  binary = "${bundle.pkg}/bin/target-warehouse";
  inherit nixpkgs;
}
