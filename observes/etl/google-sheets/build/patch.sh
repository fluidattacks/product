# shellcheck shell=bash

mkdir "${out}"
shopt -s dotglob
cp -r "${src}"/* "${out}"
shopt -u dotglob
chmod -R +w "${out}"
cp "${etl_secrets}" "${out}/google_sheets_etl/secrets/data/prod.yaml"
