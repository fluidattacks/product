from google_sheets_etl.utils.io_file import (
    UnnamedIO,
)
from tempfile import (
    NamedTemporaryFile,
)


def test_unnamed_io() -> None:
    with NamedTemporaryFile("r") as file:
        x = UnnamedIO(file)
        assert x.name == "[masked]"
