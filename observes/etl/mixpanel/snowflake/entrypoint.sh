# shellcheck shell=bash

function main {
  local events

  echo '[INFO] Executing setup' \
    && export_notifier_key \
    && common_aws_region \
    && sops_export_vars 'observes/secrets/prod.yaml' \
      bugsnag_notifier_mixpanel_etl \
      mixpanel_user \
      mixpanel_secret \
    && export bugsnag_notifier_key="${bugsnag_notifier_mixpanel_etl}" \
    && export MIXPANEL_USER="${mixpanel_user}" \
    && export MIXPANEL_PASSWORD="${mixpanel_secret}" \
    && events=$(paste -s -d, "__argEventsList__") \
    && echo '[INFO] Running tap' \
    && tap-mixpanel stream-events --project "1481975" "${events}" \
    | tap-json infer-and-adjust \
      --inference-conf "__argInferenceConf__" \
      > mixpanel_data.singer \
    && target-warehouse only-append \
      --columns-map "__argColumnsMap__" \
      --schema-name "mixpanel_integrates" \
      --truncate \
      --use-snowflake \
      < mixpanel_data.singer \
    && success-indicators \
      single-job \
      --job "mixpanel_integrates" \
    && rm mixpanel_data.singer
}

main "${@}"
