{ inputs, makeScript, outputs, projectPath, ... }:
let
  deps = with inputs.observesIndex; [
    service.success_indicators.root
    tap.mixpanel.root
    tap.json.root
    target.warehouse.root
  ];
  env = builtins.map inputs.getRuntime deps;
  common_vars = import (projectPath "/observes/common/common_vars.nix") {
    inherit projectPath;
  };
in makeScript {
  replace = {
    __argEventsList__ = ./events.lst;
    __argColumnsMap__ = ./columns_map.json;
    __argInferenceConf__ = ./inference_conf.json;
  };
  searchPaths = {
    bin = env;
    export = common_vars;
    source =
      [ outputs."/common/utils/sops" outputs."/observes/common/db-creds" ];
  };
  name = "observes-etl-mixpanel-snowflake";
  entrypoint = ./entrypoint.sh;
}
