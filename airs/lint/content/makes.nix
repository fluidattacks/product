{ inputs, makeDerivation, outputs, projectPath, ... }: {
  jobs."/airs/lint/content" = makeDerivation {
    env = {
      envAirs = projectPath "/airs";
      envExclude = ./exclude.lst;
    };
    builder = ./builder.sh;
    name = "airs-lint-content";
    searchPaths = {
      bin = [ inputs.nixpkgs.findutils inputs.nixpkgs.gnused ];
      source = [ outputs."/airs/lint/md" outputs."/common/utils/common" ];
    };
  };
}
