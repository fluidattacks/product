{ inputs, makeScript, ... }: {
  jobs."/airs/lint/markdownlint" = makeScript {
    name = "airs-markdownlint";
    entrypoint = ./entrypoint.sh;
    searchPaths = { bin = [ inputs.nixpkgs.markdownlint-cli ]; };
  };
}
