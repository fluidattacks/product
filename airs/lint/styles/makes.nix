{ inputs, makeScript, projectPath, ... }: {
  jobs."/airs/lint/styles" = makeScript {
    replace.__argAirsFront__ = projectPath "/airs/front";
    entrypoint = ./entrypoint.sh;
    name = "airs-lint-styles";
    searchPaths.bin = [
      inputs.nixpkgs.findutils
      inputs.nixpkgs.nodejs_20
      inputs.nixpkgs.gnugrep
      inputs.nixpkgs.gnused
      inputs.nixpkgs.utillinux
      inputs.nixpkgs.autoconf
      inputs.nixpkgs.bash
      inputs.nixpkgs.binutils.bintools
      inputs.nixpkgs.gcc
      inputs.nixpkgs.gnumake
      inputs.nixpkgs.python311
    ];
  };
}
