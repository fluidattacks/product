# shellcheck shell=bash

function main {
  copy __argAirsFront__ out \
    && pushd out \
    && npm clean-install --ignore-scripts \
    && npm run lint:stylelint \
    && popd \
    && rm -rf out/ \
    || return 1
}

main "${@}"
