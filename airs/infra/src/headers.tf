# Production

resource "cloudflare_worker_script" "prod" {
  account_id = var.cloudflareAccountId
  name       = "airs_headers"
  content    = file("js/headers/prod.js")
}

resource "cloudflare_worker_route" "prod" {
  zone_id     = data.cloudflare_zones.fluidattacks_com.zones[0]["id"]
  pattern     = "${data.cloudflare_zones.fluidattacks_com.zones[0]["name"]}/*"
  script_name = cloudflare_worker_script.prod.name
}


# Development

resource "cloudflare_worker_script" "dev" {
  account_id = var.cloudflareAccountId
  name       = "airs_headers_dev"
  content    = file("js/headers/dev.js")
}

resource "cloudflare_worker_route" "dev" {
  zone_id     = data.cloudflare_zones.fluidattacks_com.zones[0]["id"]
  pattern     = "web.eph.${data.cloudflare_zones.fluidattacks_com.zones[0]["name"]}/*"
  script_name = cloudflare_worker_script.dev.name
}
