# Production

resource "cloudflare_page_rule" "prod" {
  zone_id  = data.cloudflare_zones.fluidattacks_com.zones[0]["id"]
  target   = "${data.cloudflare_zones.fluidattacks_com.zones[0]["name"]}/*"
  status   = "active"
  priority = 1

  actions {

    cache_level            = "cache_everything"
    edge_cache_ttl         = 7600
    browser_cache_ttl      = 1800
    bypass_cache_on_cookie = "CookieConsent"
    rocket_loader          = "on"
  }
}


# Development

resource "cloudflare_page_rule" "dev" {
  zone_id  = data.cloudflare_zones.fluidattacks_com.zones[0]["id"]
  target   = "web.eph.${data.cloudflare_zones.fluidattacks_com.zones[0]["name"]}/*"
  status   = "active"
  priority = 1

  actions {
    cache_level       = "cache_everything"
    edge_cache_ttl    = 7600
    browser_cache_ttl = 7600
    rocket_loader     = "on"
  }
}

resource "cloudflare_ruleset" "email_obfuscation_rules_dev_airs" {
  zone_id     = data.cloudflare_zones.fluidattacks_com.zones[0]["id"]
  name        = "Set email obfuscation settings"
  description = "Set email obfuscation settings off for blogs and advisories"
  kind        = "zone"
  phase       = "http_config_settings"

  # Development

  rules {
    action_parameters {
      email_obfuscation = false
    }
    action      = "set_config"
    expression  = "(http.host eq \"web.eph.fluidattacks.com\" and http.request.uri.path matches \".*/(advisories|blog)/.*\")"
    description = "Set email_obfuscation settings for dev advisories"
    enabled     = true
  }

  # Production
  rules {
    action_parameters {
      email_obfuscation = false
    }
    action      = "set_config"
    expression  = "(http.host eq \"fluidattacks.com\" and http.request.uri.path matches \".*/(advisories|blog)/.*\")"
    description = "Set email_obfuscation settings for dev advisories"
    enabled     = true
  }
}
