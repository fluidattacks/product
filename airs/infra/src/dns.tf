# Production

resource "cloudflare_record" "prod" {
  zone_id = data.cloudflare_zones.fluidattacks_com.zones[0]["id"]
  name    = data.cloudflare_zones.fluidattacks_com.zones[0]["name"]
  type    = "CNAME"
  value   = aws_s3_bucket_website_configuration.prod.website_endpoint
  proxied = true
  ttl     = 1
}


# Development

resource "cloudflare_record" "dev" {
  zone_id = data.cloudflare_zones.fluidattacks_com.zones[0]["id"]
  name    = "web.eph.${data.cloudflare_zones.fluidattacks_com.zones[0]["name"]}"
  type    = "CNAME"
  value   = aws_s3_bucket_website_configuration.dev.website_endpoint
  proxied = true
  ttl     = 1
}
