---
slug: learn/seguridad-zero-trust/
title: Modelo de seguridad zero trust
date: 2024-04-26
subtitle: Nunca confíes. Siempre verifica.
category: filosofía
tags: ciberseguridad, empresa, tendencia, riesgo, software
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1714084288/learn/zero-trust-security/cover_zero_trust_security.webp
alt: Foto por Kai Pilger en Unsplash
description: Una visión general de la filosofía de ciberseguridad que inherentemente dice "no" a todo aquel que intente acceder a los recursos digitales de una empresa.
keywords: Zero Trust, Seguridad Zero Trust, Confianza Cero, Arquitectura Zero Trust, Modelo De Seguridad, Principios De Zero Trust, Castle And Moat, Hacking Etico, Pentesting
author: Wendy Rodriguez
writer: wrodriguez
name: Wendy Rodriguez
about1: Escritor y editor
source: https://unsplash.com/photos/closeup-photo-of-street-go-and-stop-signage-displaying-stop-1k3vsv7iIIc
---

Los ciberdelincuentes de ahora son más astutos
y ambiciosos que nunca.
Disponen de medios,
tiempo e inteligencia para intentar penetrar en cualquier parte
(red o activo tecnológico) que consideren vulnerable.
Siempre hay formas de mejorar la seguridad de tu empresa:
Pasos a seguir para reducir las posibilidades de ciberataques peligrosos.
Una de las estrategias que más está dando que hablar
es la filosofía de confianza cero
(también conocida como *zero trust*) y su modelo.

Este modelo fue acuñado en [2010](https://www.techtarget.com/whatis/feature/History-and-evolution-of-zero-trust-security)
y se ha hecho cada vez más popular como resultado del complejo
y cambiante panorama de la ciberseguridad,
la expansión de la computación en nube y
la necesidad de acceso remoto,
y el aumento de regulaciones estrictas sobre privacidad
que exigen estrategias de seguridad más exhaustivas.
[Gartner](https://www.gartner.com/en/publications/zero-trust-principles-to-improve-security-playbook#:~:text=Public%2Dsector%20IT%20leaders%20are,fail%20to%20realize%20the%20benefits.)
predice que "más del 60% de las organizaciones adoptarán principios
de *zero trust* como punto de referencia para la seguridad de aquí a 2025".
El objetivo de la arquitectura de *zero trust*
es eliminar la confianza inherente
e implementar fuertes controles de gestión de identidad
y acceso (IAM por sus iniciales en inglés),
lo que reduciría el riesgo cibernético de una empresa al reducir
el acceso solo a los recursos o datos esenciales
dentro de su perímetro e impedir el acceso no autorizado.

Entendamos primero con qué han estado trabajando
hasta ahora las empresas de todo el mundo
y por qué las ha hecho vulnerables a algunos tipos de ataques.

## El viejo *castle-and-moat*

Imaginemos un edificio con apartamentos residenciales,
zonas sociales, oficinas administrativas
y espacios comerciales.
Cada vez que un residente entra al edificio,
se le exige una prueba de residencia.
Una vez presentada,
los residentes tienen vía libre y pueden entrar
en cualquier lugar que deseen,
incluso en apartamentos que no les pertenecen.
Se confía en ellos y pueden acceder sin impedimentos.

La seguridad tradicional de las redes se basa en ese concepto
de "confianza por defecto",
y se denomina enfoque de "castillo y foso" (castle-and-moat en inglés).
Considérese la red dentro de una empresa como un castillo
y el perímetro de la red como un foso,
confiando en cualquiera
y en todo lo que esté dentro de la red.
Nadie fuera de ella puede acceder a los datos,
pero cuando ya se está dentro de la red,
el movimiento lateral resulta sencillo.
Las compañías confiaban en este enfoque cuando todos sus datos
y recursos residían en un lugar físico,
un centro de datos *on-premises*,
al que los empleados accedían a través de un dispositivo perteneciente
a la compañía y se encontraban en una sede de la misma.
Pero varios factores han ido convirtiendo
el enfoque de *castle-and-moat* en obsoleto.
La pandemia de 2020 aceleró este proceso,
haciendo necesarios el trabajo y el acceso remoto.
Los servicios en la nube se han convertido en una parte integral
de las estructuras informáticas modernas,
acabando con los centros de datos *on-premises*.
Las ciberamenazas siguen evolucionando,
encontrando formas de burlar la seguridad tradicional de las redes.
Estos factores, entre otros,
han dejado atrás este enfoque,
pero tampoco es que haya sido seguro.

El enfoque *castle-and-moat* asume
que los ataques se originan en el exterior de la red,
concentrándose en la protección de su perímetro.
Sin embargo, las amenazas también proceden del interior,
lo que hace que este enfoque sea insuficiente y vulnerable.
Por ejemplo,
si un actor malicioso obtiene acceso a credenciales
(robándolas o comprándolas en la *dark web*),
este enfoque no detectaría ni sería capaz de detener los ataques,
permitiendo al atacante moverse lateralmente y crear caos.
Las empresas que utilizan este enfoque tienen
que emplear otros recursos para defender su perímetro
con herramientas de seguridad extras como *firewalls*,
sistemas de prevención de intrusiones (IPS por sus iniciales en inglés)
y sistemas de detección de intrusiones (IDS por sus iniciales en inglés),
lo cual, sobra decir, tiene un mayor costo monetario.

Se ha hecho absolutamente necesario
que las empresas encuentren otras formas de asegurar sus recursos,
mediante enfoques más eficientes que resuelvan
el defecto fundamental de la confianza inherente
dentro de un perímetro.
La seguridad *zero trust* resuelve ese defecto verificando continuamente
las solicitudes de acceso y concediendo únicamente los permisos mínimos.

## ¿Qué es la seguridad *zero trust*?

Imagina otro edificio.
Este edificio tiene las mismas zonas que el edificio anterior,
pero los residentes en este edificio
solo pueden entrar en sus apartamentos
y solo pueden hacerlo después de haber sido debidamente validados.
Los residentes deben obtener una autorización previa
y pasar una verificación estricta,
igual que cuando entraron en sus apartamentos,
para acceder a cualquier otra zona del edificio.
La arquitectura de seguridad *zero trust* se basa
en esta metodología de supervisión continua
y proporciona un conjunto de principios para diseñar
e implementar redes informáticas seguras.
La idea principal de esta filosofía
es "**nunca confíes, siempre verifica**",
lo que significa que la autenticación
y la autorización son necesarias para cada usuario
o dispositivo que intente acceder a los recursos de la empresa,
independientemente de su ubicación
(oficina de la empresa, oficina desde casa,
*co-workings*, diferentes lugares de trabajo.)
Esto elimina la confianza inherente
o "confianza por defecto" de los modelos antiguos,
al asumir que las amenazas pueden originarse desde cualquier lugar.

La seguridad *zero trust* se basa
en la idea de que no se puede confiar
inherentemente en nada ni en nadie.
Sus principios son fundamentos que,
si se aplican correctamente,
proporcionan una postura empresarial más segura
que tiene la capacidad de afrontar un ataque,
que podría producirse en cualquier momento.
Por eso es importante *zero trust*.
Es una estrategia de ciberseguridad robusta
que contrarresta riesgos como los asociados al robo de credenciales,
el *malware* o *ransomware*,
la ingeniería social y la explotación de vulnerabilidades.
Entonces, ¿de qué principios estamos hablando?

### Principios de *zero trust*

La seguridad *zero trust* debe construirse
sobre una base de principios fundamentales
que se sobreponen a los enfoques tradicionales.
Estos principios son:

- **Acceso con mínimos privilegios**: Se concede a los usuarios
  el nivel mínimo de acceso a las redes
  y sistemas necesario para realizar sus tareas.
  Esto limita el posible alcance de un atacante en caso de obtener acceso.
  Los privilegios siempre pueden elevarse después
  de que los *trust brokers* examinen a fondo cada solicitud.

- **Autenticación y supervisión continuas**: La confirmación continua
  de la identidad garantiza que los usuarios son quienes dicen ser.
  Las eficaces prácticas de IAM son cruciales para verificar
  las solicitudes de acceso y garantizar
  que únicamente las entidades autorizadas
  puedan utilizar recursos específicos.
  Además, la actividad de los usuarios, el tráfico de red,
  la ubicación y el estado de los dispositivos se supervisan
  para detectar anomalías y comportamientos sospechosos.
  Esto permite la detección temprana de posibles violaciones
  y permite una respuesta rápida para contener las amenazas.

- **Microsegmentación**: Esta práctica recomienda segmentar
  las redes en zonas más pequeñas
  para mantener separados los volúmenes de trabajo.
  Es una técnica que limita la "zona de explosión"
  en caso de una intrusión,
  impidiendo que un atacante se desplace lateralmente.

- **Suponer ataques**: Este principio es la presunción de
  que van a producirse ataques
  y promueve a que la empresa dé prioridad a la detección,
  la respuesta y la recuperación rápida para minimizar
  los impactos de las violaciones de seguridad.

### *Zero trust* frente a *castle-and-moat*

<image-block>

!["Zero trust frente a castle and moat"](https://res.cloudinary.com/fluid-attacks/image/upload/v1714427653/airs/es/learn/zero-trust/zt-frente-cam.webp)

Diferencias de zero trust frente a castle-and-moat

</image-block>

### ¿Qué componentes se utilizan en *zero trust*?

En el mercado existen varios componentes para implementar
y establecer la seguridad *zero trust*.
A continuación figuran algunas de las tecnologías y funciones más utilizadas:

- **Autenticación multifactor (MFA)**: Proceso de varios pasos
  para usuarios y dispositivos utilizado
  para la gestión de identidades en el que se necesitan
  al menos dos factores de prueba para acceder.
  Esas pruebas pueden ser contraseñas de un solo uso (OTP),
  *tokens* de seguridad, preguntas de seguridad
  y datos biométricos tales como huellas dactilares.

- **Cifrado**: La información se transforma
  en código mediante este procedimiento
  para impedir el acceso no autorizado.
  La PII (información personal identificable) debe mantenerse cifrada,
  junto con datos sensibles como mapas de red,
  documentos legales e información de *software*.

- **Directorio Activo (AD)**: Se trata de un servicio
  de directorio de Microsoft en el que los administradores pueden controlar
  el acceso a los recursos de red y los permisos.

- **Zero Trust Network Access (ZTNA)**: [ZTNA](../../blog/ztna/)
  es la solución tecnológica
  más utilizada para la seguridad *zero trust*.
  Restringe el acceso a los recursos concretos
  que necesitan los usuarios o dispositivos.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/pruebas-seguridad/"
title="Inicia ahora las pruebas de seguridad de Fluid Attacks"
/>
</div>

## Cómo implementar *zero trust*

Como ya se ha mencionado,
*zero trust* se aleja del enfoque basado en el perímetro
y la confianza inherente, verificando constantemente el acceso
y minimizando los privilegios.
Para las empresas con un modelo tradicional establecido,
la implementación de la arquitectura *zero trust*
puede resultar algo compleja al principio.
Pero adoptando una implementación por etapas,
haciendo un análisis del retorno de la inversión y del presupuesto,
utilizando herramientas de integración
y organizando sesiones de capacitación para los empleados,
el proceso de adopción de *zero trust* puede resultar menos estresante.

A continuación se indican cinco pasos para implementar
la arquitectura de seguridad *zero trust*,
sin olvidar que cada empresa tiene sus propias necesidades:

1. **Definir la superficie de ataque**: Identificar los activos críticos
   (datos y recursos) y trazar los flujos de datos
   (cómo se mueven los datos por la red).

2. **Identificar las solicitudes de red**: Realizar un inventario de usuarios
   y dispositivos que necesitan acceso,
   especificando con certeza quién o qué necesita acceder a qué recurso.

3. **Crear una política de *zero trust***: Definir estándares de seguridad
   y establecer políticas de control de acceso claras y granulares.

4. **Elegir los componentes de *zero trust***: Decidir qué componentes
   de seguridad se van a utilizar en la infraestructura,
   y elegir la solución ideal para la compañía.

5. **Supervisar y mantener la red**: Revisar periódicamente
   los controles de acceso para garantizar que siguen siendo eficaces,
   desarrollar un plan de respuesta a incidentes
   y formar a los usuarios en las buenas prácticas de *zero trust*.

### Modelo de seguridad *zero trust* con Fluid Attacks

Como empresa de ciberseguridad centrada en AppSec,
creemos firmemente que *zero trust* promueve
algunas de las mejores prácticas de seguridad disponibles
y cuyo objetivo final es mantener seguras a las empresas.
Confiamos tanto en ello que seguimos la filosofía *zero trust*
dentro de nuestra empresa y con nuestros clientes.
A aquellos clientes que quieran cumplir con un modelo *zero trust*,
podemos ofrecerles ciertas tácticas que siguen esta filosofía.

Nuestro [CSPM](../../producto/cspm/) (seguridad en la nube)
actúa como un aliado firme del modelo *zero trust*
al evaluar constantemente las configuraciones de la nube.
CSPM puede identificar privilegios excesivos o ilógicos,
por ejemplo cuando una máquina permite
el acceso SSH desde una red no determinada.
Informaríamos de esta situación y ofreceríamos ciertas recomendaciones
(como la inclusión en listas blancas de las IP esperadas)
para garantizar que se cumpla el principio
*zero trust* de mínimo privilegio.

CSPM se complementa con nuestros métodos de prueba,
como las pruebas de seguridad de aplicaciones estáticas
([SAST](../../producto/sast/)),
las pruebas de seguridad de aplicaciones dinámicas
([DAST](../../producto/dast/))
y el análisis de composición de *software* ([SCA](../../producto/sca/)),
para reforzar aún más la postura *zero trust*
ya que garantizan que las vulnerabilidades se descubren con prontitud.

Buscamos políticas de seguridad mal aplicadas
e informamos sobre cómo pueden resultar
en la explotación de vulnerabilidades y, en consecuencia,
afectar a la arquitectura *zero trust* de nuestro cliente.
Podemos ayudar a tu compañía a mantener
un plan de seguridad de alto nivel que aleje a todo
y a todos de los ciberdelincuentes y sus métodos.
También exigimos que determinadas aplicaciones empleen
la autenticación multifactor,
fomentamos la verificación de credenciales
y garantizamos que cada login cumpla las políticas
de uso de contraseñas seguras.

Nuestra solución de [pruebas de seguridad](../../soluciones/pruebas-seguridad/)
puede ser una valiosa herramienta para que
los equipos de seguridad identifiquen
y corrijan vulnerabilidades que podrían reducir la eficacia
del modelo *zero trust*.
Por ejemplo,
cuando revisamos las aplicaciones de nuestros clientes
para detectar vulnerabilidades,
informamos cuando no se ha establecido un proceso
de autorización adecuado o cualquier otra situación
que comprometa el cumplimiento de las políticas de seguridad.
Tras una evaluación exhaustiva,
nuestros *pentesters* te ofrecen recomendaciones
que se alinean con el modelo de seguridad *zero trust*.

Si deseas más información sobre
cómo nuestras soluciones ayudan a tu postura de seguridad,
[contáctanos](../../contactanos/) ahora.
