---
slug: learn/que-es-vulnerabilidad-spring4shell/
title: Spring4Shell
date: 2023-10-06
subtitle: ¿Qué es esta vulnerabilidad de Spring Framework?
category: ataques
tags: vulnerabilidad, riesgo, empresa, software, pruebas de seguridad
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1696602946/learn/spring4shell/cover_sring4shell_1.webp
alt: Foto por Len Cruz en Unsplash
description: Descubre qué es la vulnerabilidad Spring4Shell, cómo podría posiblemente exponer tu sistema a ciberataques y consejos de cómo protegerte contra nuevos problemas de seguridad en Spring Framework.
keywords: Fluid Attacks, Spring Framework, Spring4shell, Ejecucion Remota De Codigo, Rce, Vulnerabilidad, Pentesting, Hacking Etico
author: Wendy Rodriguez
writer: wrodriguez
name: Wendy Rodriguez
about1: Escritor y editor
source: https://unsplash.com/photos/ScEKf8u7y-c
---

En el mundo de _software_,
Spring es un _framework_ (o marco de trabajo)
popular que utilizan los desarrolladores para crear aplicaciones.
Sin embargo, como en cualquier sistema,
puede tener a veces vulnerabilidades o fallas.
La vulnerabilidad Spring4Shell es una de esas.
Es una debilidad dentro de Spring Framework que permite
a los atacantes inyectar comandos o scripts maliciosos en el sistema,
posiblemente consiguiendo acceso no autorizado
o control sobre una aplicación.

## ¿Qué es Spring Framework?

[Spring Framework](https://docs.spring.io/spring-framework/reference/overview.html),
cuya empresa matriz es VMware,
es un marco de [código abierto](../../../blog/choosing-open-source/)
que proporciona un soporte de infraestructura completo
para desarrollar aplicaciones en el lenguaje de programación Java.
Se creó para simplificar el desarrollo de aplicaciones de nivel empresarial
y promover buenas prácticas de programación y patrones de diseño.
Ofrece numerosas funciones y módulos que ayudan a los desarrolladores
a crear aplicaciones robustas y escalables.
Algunas características importantes de Spring Framework son:

- **Inyección de dependencias**: Esta [característica](https://docs.spring.io/spring-framework/reference/core/beans/dependencies/factory-collaborators.html)
  permite que los objetos estén ligeramente acoplados y facilita las pruebas
  y el mantenimiento al reducir las dependencias entre los componentes.

- **Programación orientada a aspectos**: [AOP por su nombre en inglés](https://docs.spring.io/spring-framework/reference/core/aop.html)
  permite a los desarrolladores implementar rutinas transversales,
  por ejemplo, el registro de datos,
  y aplicarlos a varias partes de la aplicación.

- **Modelo vista controlador**: Es un _framework_ basado en HTTP
  y servlets proporcionado por Spring que facilita
  el desarrollo de aplicaciones web.
  Sigue el patrón arquitectónico [MVC](https://docs.spring.io/spring-framework/docs/3.2.x/spring-framework-reference/html/mvc.html)
  para separar las rutinas y proporciona funciones para manejar,
  gestionar vistas, y procesar formularios.

- **Acceso a datos**: Spring proporciona un enfoque coherente y sencillo
  de acceso a datos a través del módulo
  [Spring Data](https://spring.io/projects/spring-data).
  Ofrece soporte para diferentes bases de datos como JDBC, Redis, JPA y NoSQL.

- **Seguridad**: [Seguridad de Spring](https://spring.io/projects/spring-security)
  es un módulo que proporciona mecanismos de autenticación
  y autorización para garantizar la seguridad de la aplicación.
  Ofrece características como autenticación de usuarios, control de
  acceso, y protección contra vulnerabilidades de seguridad comunes.

Estas, entre otras características
simplifican el desarrollo de aplicaciones Java
y fomentan las buenas prácticas de desarrollo de _software_
proporcionando un conjunto completo de herramientas
para aplicaciones tecnológicamente especializadas.

## ¿Qué es la vulnerabilidad Spring4Shell?

Spring Framework experimentó importantes contratiempos en marzo del año pasado.
El primero fue registrada como [CVE-2022-22963](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-22963),
que afectaba a Spring Cloud.
El impacto que tuvo esta vulnerabilidad fue bastante alto porque
era fácil de explotar, sin embargo,
era bastante bajo en términos de disponibilidad,
ya que sólo afectaba a los servicios que ejecutaban
Spring Cloud en la versión 3.1.6 y anteriores (para 3.1.x),
y 3.2.2 y anteriores (para 3.2.x).
Fue corregido poco después de detectarse.
Sin embargo,
un par de días después de la corrección, el 29 de marzo de 2022,
se encontró una vulnerabilidad crítica que afectaba a Spring Framework.
Registrada como [CVE-2022-22965](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-22965),
la vulnerabilidad afectaba a las aplicaciones
Spring MVC y Spring WebFlux utilizando JDK 9 o superior.
La aplicación afectada necesitaba ejecutarse
en Tomcat como un despliegue WAR para ser explotada.
La naturaleza de la vulnerabilidad era general
y había varias formas de explotarla,
permitiendo a los atacantes introducir código malicioso aleatorio
en la aplicación y ejecutarlo.
Llamada Spring4Shell por su parecido
con otra vulnerabilidad de ejecución remota de código (RCE),
[Log4Shell](../que-es-vulnerabilidad-log4shell/);
Spring Framework se vio seriamente comprometido debido a
su uso extensivo en el desarrollo de aplicaciones web.
Spring intentó ser rápido
en [liberar](https://spring.io/blog/2022/03/31/spring-framework-rce-early-announcement)
información conocida sobre la vulnerabilidad y proporcionó
una solución provisional para sus usuarios.
En este artículo hablaremos de la vulnerabilidad Spring4Shell,
ya que hay otras vulnerabilidades descubiertas en el mismo _framework_
pero menos críticas o menos extendidas.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/gestion-vulnerabilidades/"
title="Inicia ahora con la solución de gestión de vulnerabilidades
de Fluid Attacks"
/>
</div>

## Explotación de Spring4Shell

Los atacantes pueden explotar la vulnerabilidad Spring4Shell
enviando una petición especialmente diseñada a un servidor web
que ejecute el _framework_ Spring Core.
Esta petición debe contener ciertos tipos de parámetros vinculantes,
que se utiliza para cargar el código malicioso, y una vez cargado,
puede ser ejecutado por la aplicación, dando al atacante el control sobre esta.
La ejecución remota de código puede lograrse
en versiones impactadas de Spring Framework,
explotando la función de anotación
RequestMapping a través de la petición creada.
Aprovechando esta característica,
un atacante puede acceder y modificar propiedades de clases anidadas
debido a la manera en que Spring Core realiza
la solicitud de vinculación de parámetros mediante serialización.

Según [Microsoft](https://www.microsoft.com/en-us/security/blog/2022/04/04/springshell-rce-vulnerability-guidance-for-protecting-against-and-detecting-cve-2022-22965/),
estas son las características que tienen los sistemas afectados:

- Ejecutan JDK 9.0 o versiones posteriores.
- Spring framework versiones 5.3.0 a 5.3.17, 5.2.0 a 5.2.19,
  y versiones anteriores.
- Apache Tomcat como contenedor de Servlets.
- Empaquetado como un archivo WAR tradicional
  (no como un JAR ejecutable de Spring Boot).
- Dependencia de spring-webmvc o spring-webflux.

## Consecuencias e impacto de Spring4Shell

Un ataque exitoso de vulnerabilidades Spring4Shell
puede tener graves consecuencias
que incluyen el robo de datos sensibles, tales como información de clientes,
datos financieros o propiedad intelectual.
Los atacantes también podrían instalar _malware_ en el sistema de la víctima
o tomar el control del mismo.
Algunos actores de amenazas también podrían penetrar e instalar un _web shell_,
que compromete los servidores web, y utilizarla para lanzar nuevos ataques.
Es de destacar que,
aunque un gran número de instalaciones de Spring eran vulnerables,
no todas las aplicaciones Spring fueron penetradas.

El impacto de Spring4Shell en 2022 fue global,
teniendo su mayor efecto en Europa,
donde el 20% de las organizaciones que utilizaban Spring Framework
sufrieron al menos un intento de explotación.
Por industrias,
los sectores más atacados fueron los proveedores de _software_,
seguidos por los sectores de la educación y la investigación,
y los sectores legal y de seguros.

## Spring4Shell vs. Log4Shell

Estas vulnerabilidades permiten ataques de ejecución de código remoto
pero están presentes en dos _frameworks_ diferentes utilizados
en el desarrollo de aplicaciones Java que sirven a diferentes propósitos.
Ambas se generaron en componentes de código abierto
y se explotaron como vulnerabilidades de día cero
y tienen una puntuación CVSS cualitativa crítica;
sin embargo, presentan diferencias importantes.

[Log4Shell](../../../blog/log4shell/)
es una vulnerabilidad en el marco de registro Log4j,
es decir,
puede utilizarse para registrar mensajes
durante el tiempo de ejecución de la aplicación.
Log4Shell es relativamente fácil de explotar,
puede afectar a una amplia gama de aplicaciones Java,
incluyendo servidores web, servicios en la nube y dispositivos IoT,
y podría llevar a comprometer por completo el sistema.
Por otro lado, Spring4Shell, una vulnerabilidad en Spring Framework,
es más difícil de explotar y afecta a un menor número de aplicaciones.

Para explotar una vulnerabilidad Spring4Shell,
un atacante debe ser capaz de controlar
los valores de ciertos tipos de parámetros.
Esto requiere que el atacante tenga conocimiento del sistema objetivo,
de los tipos de parámetros que son vulnerables,
y de cómo controlar sus valores,
todo lo cual no es un conocimiento al alcance de los atacantes.
Esto lo hace más difícil de explotar que Log4Shell,
que implica aprovechar la forma en que Log4j
interpreta ciertos mensajes de registro.

A pesar de ser más difícil de explotar,
Spring4Shell sigue siendo una vulnerabilidad grave
ya que los desarrolladores que utilizan Spring
deben primero identificar la vulnerabilidad
y luego proceder con un proceso por pasos para solucionarla.
Fluid Attacks tiene sus propios métodos de prueba para
análisis de composición de _software_ ([SCA](../../producto/sca/)),
que pueden escanear y revelar riesgos de ciberseguridad relacionados
con componentes de código abierto
y de terceros, como Log4j y Spring Framework.

## Mitigación y corrección de Spring4Shell

Para solucionar este problema, es necesario actualizar
Spring Framework a una versión superior a 5.2.20 o 5.3.18,
y, si se utiliza Spring Boot, actualizar a una versión superior a 2.6.6.
Es imprescindible actualizar las aplicaciones
que utilicen cualquiera de las versiones afectadas.
Actualizar a una versión revisada modificando los archivos Maven, Gradle,
u otros archivos de gestión de dependencias, y después,
reconstruir y desplegar la aplicación de nuevo.

Si por alguna razón no es posible actualizar la versión de Spring Framework,
el equipo de Spring proporciona soluciones viables
para mitigar la vulnerabilidad,
como actualizar a JDK 8 o deshabilitar la vinculación de ciertos archivos.

## Protección ante vulnerabilidades similares a Spring4Shell

Otras medidas que se pueden tomar para proteger las aplicaciones
de vulnerabilidades como Spring4Shell
u otras dentro de Spring Framework incluyen:

- Utilizar un _firewall_ de aplicaciones web (WAF)
  para filtrar los requisitos de HTTP maliciosos.

- Implementar la validación de _inputs_ para evitar que
  los atacantes inyecten código malicioso en una aplicación.

- Revisar nuevas vulnerabilidades suscribiéndose
  a avisos oficiales de seguridad y listas de correo
  [relacionadas](https://www.vmware.com/security/advisories.html)
  a Spring Framework,
  así como otras fuentes de seguridad relevantes para estar al día.

- [Gestionar vulnerabilidades](../../soluciones/gestion-vulnerabilidades/)
  que podrían ser posibles puertas de entrada a ciberataques mayores.
  Esta amplia perspectiva de las vulnerabilidades
  está orientada a encontrar problemas de día cero y buscar su rápida solución.
  En Fluid Attacks,
  combinamos herramientas de escaneo con _hackers_ éticos
  que mapean vulnerabilidades para encontrar las de mayor impacto.
  Este proceso se apoya en nuestra [plataforma](../../plataforma/),
  la cual proporciona informes que facilitan la gestión de vulnerabilidades.

- Utilizar una herramienta de pruebas de seguridad de aplicaciones estáticas
  (SAST) para escanear aplicaciones buscando vulnerabilidades.
  Fluid Attacks ofrece [SAST](../../producto/sast/), además de otras técnicas,
  la cual analiza y evalúa sistemas,
  con la ventaja añadida de proporcionar información a los desarrolladores,
  buscando vulnerabilidades conocidas de forma sencilla,
  con precisión y rapidez de ejecución a lo largo del SDLC.
  Se realiza tanto en nuestro [plan Essential como en plan Advanced](../../planes/).

- Realizar [pruebas de penetración continuas](../../blog/pruebas-de-penetracion-continuas/)
  en aplicaciones web ayuda a detectar vulnerabilidades y debilidades
  que podrían ser explotadas y, a menudo,
  no pueden ser encontradas por herramientas.
  En Fluid Attacks, vamos un paso más allá con nuestras
  [pruebas de penetración](../../soluciones/pruebas-penetracion-servicio/),
  porque es ejecutado por nuestros _hackers_ éticos certificados
  y altamente cualificados,
  que están equipados para detectar este tipo de vulnerabilidad
  y proporcionan informes detallados que ayudan al proceso de remediación

Las vulnerabilidades Spring4Shell son críticas,
pero no significa que se desaconseje el uso de Spring Framework,
de hecho, a diferencia de otras tecnologías,
la comunidad Spring ha estado trabajando continuamente durante décadas
y tiende a solucionar las fallas reportadas en pocos días.

## Fluid Attacks tiene el dominio sobre la gestión de vulnerabilidades

Es importante que las organizaciones y los desarrolladores
estén al tanto de vulnerabilidades como Spring4Shell;
saber qué aplicaciones utilizan Spring Framework
(y que por lo tanto, podrían estar exponiendo el sistema a ataques)
es estar un paso adelante.
De este modo las organizaciones
y los desarrolladores pueden garantizar la seguridad e integridad
de sus aplicaciones y proteger a sus usuarios finales de posibles ataques.
El primer paso para gestionar vulnerabilidades es disponer de herramientas
que puedan detectarlas, y aquí en Fluid Attacks
puedes contar con nuestro servicio integral,
[Hacking Continuo](../../servicios/hacking-continuo/),
que no sólo incluye
herramientas automatizadas como [SAST](../../producto/sast/)
o [SCA](../../producto/sca/),
sino también pruebas de penetración
realizadas por _hackers_ éticos profesionales que encuentran,
explotan, e informan sobre las vulnerabilidades de tu _software_.
Nuestro [plan Advanced](../../planes/)
ofrece precisamente eso,
una combinación sólida de conocimientos humanos y herramientas de escaneo,
haciendo que el despliegue de tu _software_ sea a prueba de balas.
También puedes [hacer clic aquí](https://app.fluidattacks.com/)
para probar nuestro plan Essential de Hacking Continuo
gratis durante **21 días**.
No dudes en [contactarnos](../../contactanos/)
y comienza a utilizar nuestras soluciones ya.
