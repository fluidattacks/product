---
slug: blog/prevenir-ransomware/
title: Cómo prevenir ataques de ransomware
date: 2024-04-03
subtitle: La mejor ofensa es una buena defensa
category: ataques
tags: ciberseguridad, empresa, tendencia, riesgo, software, ingeniería social
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1712159396/blog/ransomware-prevention/cover_ransomware_prevention_1.webp
alt: Foto por Florian Schmetz en Unsplash
description: La prevención proactiva es la mejor estrategia contra los ataques de ransomware. Por eso hemos recopilado buenas prácticas para prevenir este acto malicioso.
keywords: Ransomware, Buenas Practicas, Prevencion, Vulnerabilidad, Malware, Codigo Malicioso, Ingenieria Social, Hacking Etico, Pentesting
author: Wendy Rodriguez
writer: wrodriguez
name: Wendy Rodriguez
about1: Escritor y editor
source: https://unsplash.com/photos/man-in-black-suit-standing-on-green-floor-G1hIBdjQoAA
---

El aumento de los ataques de *ransomware* ha sido impresionante.
El negocio de *ransomware* ha crecido,
llevando a cabo ataques de forma agresiva e implacable
durante la última década. Las ganancias son buenas:
los ciberdelincuentes obtienen cada año miles,
si no millones, de dólares por el pago de rescates.
Según una [encuesta reciente sobre *ransomware*](https://ransomware.org/ransomware-survey/),
en 2023 se produjeron 4.399 ataques de *ransomware*,
lo que representa un aumento con respecto a 2022,
donde se registraron 2.581 ataques.
La encuesta predice que es más probable que las empresas
sean el objetivo de ataques de *ransomware* en 2024
y en años posteriores,
ya que la tendencia de ataques de *ransomware*
y pagos de rescates casi se duplicaron en 2023.
Dado que esta amenaza no muestra signos de detenerse,
la pregunta no debería ser "en caso de que" sino "cuándo":
Cuándo tu compañía sea víctima de este tipo de ataque malicioso,
¿estarás preparado?

Lo que pasa con el *ransomware* es que,
una vez que ataca,
el sistema afectado nunca vuelve a ser el mismo.
El *ransomware* es casi imposible de eliminar,
y tiende a ser detectado solo después de haber sido ejecutado
(algunos indicios son la lentitud del sistema,
cambios en los nombres o ubicaciones de los archivos,
pérdida de datos, cifrado confuso de archivos
o un anuncio explícito por parte del atacante).
Por eso decimos que más vale prevenir que lamentar,
y **la prevención es fundamental**.
Cuanto mejor comprendas estos ataques, mejor podrás defenderte.
Para empezar,
revisemos las formas en que el *ransomware* se distribuye.

## ¿Cómo se distribuye el *ransomware*?

Buena pregunta. El *ransomware*, al igual que otros
[códigos maliciosos](../../learn/codigo-malicioso/),
se distribuye a través de un punto de entrada
que se explota para obtener acceso al sistema objetivo,
lo que facilita las siguientes fases del ataque.
Los ciberdelincuentes saben que un método sofisticado
y bien estructurado de distribución de *ransomware*
puede conducir a un ataque exitoso y devastador,
y por eso dedican tiempo a perfeccionar
las técnicas de distribución de *software* malicioso.

La táctica más frecuente es el ***phishing***.
Este tipo de [ingeniería social](../../../blog/social-engineering/)
es el pan de cada día de los atacantes porque saben
que la mayor parte del mundo accede a correos electrónicos,
SMS y otros tipos de mensajes.
Los mensajes de
[*phishing*](../../../blog/phishing/)
se camuflan como si procedieran de fuentes legítimas,
por ejemplo, bancos, compañías de tarjetas de crédito
e incluso familiares o colegas,
y contienen archivos adjuntos o enlaces maliciosos que,
al hacer clic, fuerzan la descarga e instalación de *ransomware*.
El *spear phishing*, el [*smishing*](../../../blog/smishing/), el *vishing*
y el *whaling* son algunas de las formas
de *phishing* que todos deberíamos conocer.

Otra táctica utilizada por los atacantes
es el **protocolo de escritorio remoto** (RDP).
[RDP](https://bullwall.com/how-has-rdp-become-a-ransomware-gateway/),
el cual está preinstalado en todos los sistemas operativos Windows actuales,
es utilizado por muchos trabajadores remotos
que necesitan conectarse a los servidores
de su organización para realizar sus tareas.
Los ciberdelincuentes ven en RDP un punto de entrada fácil.
Ya sea explotando vulnerabilidades de seguridad (habituales en RDP),
[descifrando contraseñas](../../../blog/pass-cracking/),
o [rellenando credenciales](../../../blog/credential-stuffing/),
los atacantes obtienen acceso remoto al sistema objetivo
y pueden desplegar allí ransomware con instrucciones de pago.
Los ataques de ransomware RDP han aumentado desde
que el trabajo remoto se hizo necesario
durante las restricciones de COVID-19
y se convirtió en una opción popular para que
las empresas contrataran empleados en todo el mundo.

Una táctica en la que una persona navega por un sitio web
y se produce una descarga involuntaria
se denomina ***drive-by download***.
Los atacantes utilizan sitios web para instalar *software* malicioso
o redirigir a los visitantes a páginas web que ellos controlan,
dejando al usuario expuesto a ciberataques como el *ransomware*.
Este tipo de estrategia es una amenaza inquietante
porque puede ejecutarse incluso sin el consentimiento
o el conocimiento de la víctima.
Los kits de explotación pueden utilizarse en esta táctica
para proporcionar a los atacantes información
sobre aplicaciones, navegadores o extensiones sin parchear
o desactualizados.

Otros ejemplos de técnicas de distribución de *ransomware*
son el ***malvertising*** (o publicidad maliciosa,
es una táctica que intenta difundir *malware* a través de anuncios en línea),
la **piratería de *software*** (programas, aplicaciones, películas,
juegos u otro *software* sin licencia
y usualmente gratuitos pueden contener *malware*)
y los **medios de almacenamiento extraíbles** (las memorias USB
y los discos duros externos pueden utilizarse fácilmente
para infectar cualquier cosa a la que estén conectados).

## Buenas prácticas para prevenir el *ransomware*

La mejor forma de defenderse de los ataques de *ransomware* es,
en primer lugar, intentar evitarlos.
Una estrategia defensiva bien construida puede ayudar
a mitigar o prevenir por completo un ataque de *ransomware*.
Muchos sitios web ofrecen listados o información al respecto,
entre ellos está
CISA (la agencia de ciberseguridad y seguridad de infraestructuras),
quienes crearon una [guía](https://www.cisa.gov/stopransomware/ransomware-guide)
para que las empresas estén mejor preparadas contra estos ataques.
Nosotros también recopilamos las siguientes buenas prácticas,
las cuales consideramos son las más adecuadas
para crear una estrategia de prevención robusta:

- **Mantener copias de seguridad fuera de línea y cifradas**: Regularmente
  haz copias de seguridad de información crítica
  y almacénalas fuera del sistema.
  Un paso más allá sería cifrarlas,
  de modo que sean ilegibles si alguna vez se produce una infiltración.
  Prueba con frecuencia el procedimiento de restauración
  de las copias de seguridad para asegurarte que funcionen.
  Este paso garantiza que la organización disponga
  de una copia intacta con la que se pueda restaurar los datos,
  especialmente en caso de que los atacantes los cifren o los eliminen.

- **Crear un plan de respuesta a incidentes**: Este plan incluye
  políticas y procedimientos para gestionar un ataque de *ransomware*.
  Debe proporcionar instrucciones detalladas
  para la comunicación interna y externa,
  describir formas de contener o impedir que el ataque
  se propague a otras redes,
  y describir protocolos de respuesta a diferentes escenarios
  y pasos para recuperar datos y restaurar sistemas.
  Cualquier persona con acceso a los dispositivos
  de la empresa debe tener a mano una copia impresa del plan,
  que debe permanecer fuera de línea para que solo lo conozca
  el personal autorizado.
  También debe actualizarse constantemente
  y someterse a pruebas periódicas.

- **Implementar un modelo de confianza cero**: Evita el acceso
  no autorizado y limita los privilegios de usuarios,
  concediendo solo los permisos necesarios.
  Esto reduce los posibles daños en caso de que
  los atacantes accedan a una cuenta de usuario.

- **Realizar una evaluación continua de las vulnerabilidades**: Para
  reducir la superficie de ataque,
  debe realizarse un escaneo frecuente de vulnerabilidades
  con herramientas vinculadas a listas como la [CVE](../../../compliance/cve/)
  o la [CWE](../../../compliance/cwe/) con el fin
  de identificar amenazas recientes o conocidas.
  Esto puede formar parte de
  una [solución de gestión de vulnerabilidades](../../soluciones/gestion-vulnerabilidades/),
  en la que deberían participar [*hackers* éticos](../que-es-hacking-etico/)
  para realizar [pruebas de penetración](../pentesting/).
  Los *hackers* éticos tienen un profundo conocimiento
  de las vulnerabilidades y saben cómo explotarlas.
  Como parte de sus evaluaciones,
  deben proporcionar reportes sobre escenarios de explotación
  y sugerencias de remediación.
  La corrección de las vulnerabilidades identificadas
  (especialmente las de mayor gravedad)
  debe hacerse con la mayor brevedad,
  ya que los actores de amenazas las buscan constantemente.
  La gestión y
  la [evaluación de las vulnerabilidades](../evaluacion-de-vulnerabilidades/)
  son fundamentales en un plan de prevención.

- **Aplicar parches y actualizar el *software* constantemente**: Asegúrate
  de aplicar las últimas actualizaciones a los servidores,
  sistemas operativos y cualquier aplicación conectada a Internet.
  Estas actualizaciones suelen contener parches de seguridad
  que corrigen vulnerabilidades que podrían ser aprovechadas
  por los ciberdelincuentes.

- **Separar las redes lógicamente**: La segmentación de la red ayuda
  a contener un ataque de *ransomware* a una sección
  o secciones controlando el acceso o limitando el movimiento lateral.

- **Desactivar macros en Microsoft Office**: Estudia la posibilidad
  de desactivar las macros de los archivos de Microsoft Office
  recibidos por correo electrónico,
  ya que pueden utilizarse para enviar *ransomware*.

- **Implementar políticas de contraseñas**: Establece directrices
  sobre cómo los empleados deben crear,
  utilizar y gestionar las contraseñas que necesitan
  para acceder a los sistemas y datos de la empresa.

- **Habilitar la autenticación multifactor (MFA)**: Exige otras medidas
  aparte del usuario y contraseña para identificar personal.
  MFA utiliza múltiples pasos de verificación,
  lo que añade una capa extra de seguridad
  que protege los sistemas contra personas
  no autorizadas o no deseadas.
  Implementa MFA para acceder a recursos corporativos como aplicaciones,
  cuentas de correo electrónico, VPNs y bases de datos.
  Métodos de autenticación como introducir códigos recibidos
  en llamadas telefónicas o mensajes de texto,
  notificaciones push de aplicaciones autenticadoras
  o datos biométricos como las huellas dactilares son opciones
  de seguridad convenientes para utilizar como MFA.

- **Utilizar sistemas de detección de intrusos (IDS)**: Un IDS
  es una herramienta de supervisión que examina el tráfico
  de la red buscando signos de actividad sospechosa
  o comportamiento irregular que puedan indicar un ataque.
  Un IDS puede detectar actividad de comando y control
  y actividad desconocida o posiblemente maliciosa
  que ocurre antes del despliegue de *ransomware*.

- **Proporcionar entrenamiento para la concientización sobre seguridad**:
  Educa a los empleados sobre los peligros y riesgos del mundo cibernético.
  Facilita la capacitación sobre temas como los ataques maliciosos
  (incluido el *ransomware*), las estafas de *phishing*,
  los adjuntos sospechosos, los ataques de fuerza bruta y otras tácticas.
  Enseña a los empleados a reconocer posibles amenazas
  y a denunciarlas;
  refuerza también la importancia de adherirse a las políticas
  y procedimientos de seguridad.

### Concientización sobre ingeniería social

Las compañías deberían invertir en la concientización
sobre la ingeniería social porque educar a los empleados
sobre las técnicas de manipulación utilizadas
por los atacantes reduce el riesgo de caer víctima de una de ellas.
Los actores maliciosos saben que
el punto más débil de una empresa son sus empleados,
ya que pueden ser persuadidos, empujados,
engañados o manipulados de cualquier forma para
que faciliten información que pueda comprometer la seguridad de la empresa.

Los ciberdelincuentes utilizan estrategias como atraer
al empleado con algo deseable (***baiting***),
correos electrónicos de podrían aprovecharse de la curiosidad
del empleado (***phishing***),
ventanas emergentes de **pretexto** o anuncios dirigidos al empleado,
y la creación de **identidades falsas** para recopilar credenciales,
entre otras muchas técnicas.
Invertir en evaluaciones de ciberseguridad
y capacitar a los empleados para que reconozcan las señales
de alarma es una excelente manera de fomentar una cultura
de seguridad que puede reducir significativamente las probabilidades
de que una empresa sufra un ataque de ransomware.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/gestion-vulnerabilidades/"
title="Inicia ahora con la solución de gestión de vulnerabilidades
de Fluid Attacks"
/>
</div>

## El poder de una defensa proactiva

Como ya comentamos en un [artículo anterior](../10-mayores-ataques-ransomware/),
los ataques de *ransomware* han sido,
son y serán una tremenda amenaza en el mundo cibernético
en el que vivimos y trabajamos.
Adoptando una mentalidad proactiva e implementando medidas
de seguridad como las mencionadas anteriormente,
las empresas pueden fortalecer sus sistemas
contra los ataques de *ransomware*.
Estas medidas también implican mantenerse informado
sobre las amenazas emergentes e invertir en ciberseguridad.

Fluid Attacks ofrece pruebas de seguridad continuas
como una solución integral para protegerse
contra vulnerabilidades explotables.
Una de nuestras soluciones es la gestión de vulnerabilidades,
que utiliza *software* de escaneo automatizado
y [*hacking* ético](../../soluciones/hacking-etico/),
ayudándote a gestionar mejor los problemas
que puedan surgir al desarrollar *software*.
Nuestros *pentesters* son profesionales
[certificados](../../../certifications/) que trabajan continuamente
para encontrar riesgos de ciberseguridad en tus sistemas
y están capacitados para descubrir vulnerabilidades
de día cero a medida que surgen.
Esto permite a tu organización mantener
una postura de seguridad robusta.
Toda la información que encontremos estará a tu disposición
en nuestra [plataforma](../../plataforma/),
donde te proporcionaremos los detalles de cada vulnerabilidad,
como severidad, ubicación, estado y sugerencias de remediación.

La gestión de vulnerabilidades es solo una
de las soluciones que ofrecemos dentro
de nuestro [plan Advanced](../../planes/)
de [Hacking Continuo](../../servicios/hacking-continuo/).
Encuentra más de nuestras soluciones de seguridad
de aplicaciones [aquí](../../soluciones/)
y construye un esquema de seguridad escalonado para proteger tu organización.
[Estamos a un clic](../../contactanos/).
