---
slug: blog/revision-manual-de-codigo/
title: La indispensable revisión manual de código
date: 2022-11-25
subtitle: ¿Usar solo herramientas automatizadas? ¡No te niegues a cambiar de opinión!
category: filosofía
tags: ciberseguridad, pruebas de seguridad, software, empresa, código
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1669424410/blog/manual-code-review/cover_manual_code_review.webp
alt: Foto por Museums Victoria en Unsplash
description: En este artículo, presentamos algunas diferencias entre las revisiones de código automatizadas y manuales, y destacamos estas últimas y los procesos aplicados por los revisores.
keywords: Revision Manual De Codigo, Revision Automatizada De Codigo, Revision Segura De Codigo, Logica Empresarial, Proceso De Revision Manual De Codigo, Revision Manual De Codigo Vs Automatizada, Vulnerabilidades, Hacking Etico, Pentesting
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/WGeO6dW5GZM
---

Cuando nos empeñamos en crear productos en nuestro trabajo diario,
sea cual sea nuestra área, resulta una
y otra vez bastante beneficioso que antes de entregarlos,
antes de su presentación a los usuarios finales,
sean revisados por alguien más.
Recibir retroalimentación de colegas o líderes siempre
ayudará a mejorar nuestro rendimiento y la calidad de nuestros productos.
Esto sin duda aplica para el desarrollo de *software*.
Revisar el código que los desarrolladores construyen para dar forma,
por ejemplo, a aplicaciones web o móviles,
es un proceso fundamental para garantizar su alta calidad y seguridad.
En este artículo,
comparamos las revisiones de código automatizadas
y manuales y nos detenemos especialmente en el método manual.

## ¿Qué es la revisión de código?

La revisión de código es un proceso mediante
el cual se evalúa sistemáticamente un código fuente
para verificar su cumplimiento de estándares específicos.
Este cumplimiento permite que el producto de *software*
se mantenga con una cantidad reducida de fallas
o defectos (estará libre de estos muy de vez en cuando)
que muchas veces son el resultado de un enfoque descuidado
por parte de los desarrolladores.
La revisión de código puede centrarse en la arquitectura,
la funcionalidad y el estilo del *software*,
aunque también puede ocuparse de su seguridad.
De ahí el término
[revisión de código seguro](../revision-codigo-fuente/).
(Tómese como implícito el hecho de que,
cuando aquí hablamos simplemente de "revisión de código",
nos referimos al análisis que involucra la seguridad,
ya que una revisión sin atender a ella termina siendo insuficiente).

La revisión de código se basa en estándares
y requisitos de codificación que, de hecho,
son los que los desarrolladores deben tener en cuenta desde el principio.
Los errores derivados de su incumplimiento,
por pequeños que sean,
pueden pasar una elevada factura
a la organización propietaria del código
y a las personas y entidades relacionadas,
especialmente cuando los delincuentes se aprovechan de ellos.
Hoy en día, principalmente dentro
del [enfoque DevSecOps](../concepto-devsecops/),
se recomienda que la revisión de código se realice al principio
del ciclo de desarrollo de *software* (SDLC)
antes de desplegar el *software* a producción.
La idea es que sea una operación constante para
que los problemas se notifiquen a los desarrolladores cuanto antes
y para evitar costos elevados en el futuro.

## Revisión manual de código vs. automatizada

Tanto las personas expertas como las herramientas automatizadas
pueden realizar revisiones de código.
Actualmente, existe toda una serie de herramientas para la revisión de código.
Y aunque estas pueden variar en características como facilidad de uso,
apariencia y funciones, aquí hacemos una comparación en términos generales.
La revisión automatizada de código es menos costosa
y puede abarcar mucho más código en menos tiempo que la revisión manual.
De hecho, cuando se encarga a un revisor que lea
y revise el código manualmente, línea por línea,
normalmente solo se le asigna un fragmento
o segmento concreto de código.
En el caso de productos grandes, llevaría mucho tiempo
y esfuerzo revisar todo el código manualmente.
Así pues,
esta forma de revisión tiene generalmente menos alcance
que la realizada por máquinas inteligentes,
pero, como veremos, es un procedimiento indispensable dadas
las limitaciones de las herramientas.

Las herramientas automatizadas disponen de una base de datos de estándares
y requisitos predefinidos para la revisión de cumplimiento
y la detección de errores.
Las organizaciones que adquieren las herramientas pueden configurarlas
para que sigan unas normas concretas e ignoren otras
y para que funcionen según uno
o varios lenguajes de programación específicos.
La cuestión es que todo lo que queda fuera de las capacidades
predeterminadas de la herramienta escapa de su radar de detección.
Además, no todo lo que informa una herramienta
representa realmente un problema o falla en el código.
Nos referimos a estos dos últimos obstáculos
como falsos negativos (omisiones)
y falsos positivos (mentiras), respectivamente.
Evitar que estos estén presentes a la hora
de realizar informes de revisión de código está entonces
en manos de los expertos y de su trabajo manual.

Uno de los mayores defectos
de las herramientas de revisión de código es su incapacidad
para detectar los llamados defectos en la lógica empresarial o de negocio.
Las herramientas automatizadas no comprenden
las intenciones humanas ni la lógica empresarial
que hay detrás de un sistema en particular.
La lógica empresarial se refiere a la parte del *software*
que codifica las reglas empresariales
que determinan procesos como la creación,
modificación y almacenamiento de datos.
La lógica empresarial describe múltiples series de actividades
o pasos en los que se establecen relaciones entre
la interfaz del usuario final y la base de datos.
En la mayoría de los casos, los defectos de la lógica empresarial
son propios de la aplicación en cuestión y de su funcionalidad.
Son diversos y no siguen patrones de riesgo definidos
o similares para todos los casos.
Además, no se localizan en una línea de código específica,
sino que afectan a varias áreas de la totalidad de la arquitectura.

Las reglas empresariales dictan las respuestas
o reacciones del *software*
(p. ej., acciones de prevención) ante los distintos escenarios posibles.
Las vulnerabilidades en la lógica de negocio suelen significar
la incapacidad del *software* para detectar
y manejar correctamente algunos escenarios,
ciertos comportamientos extraños o inesperados de los usuarios
que los desarrolladores no tuvieron en cuenta o no previeron.
Ahí es donde los atacantes sacan partido de su astucia.
Por ejemplo,
digamos que la lógica de una aplicación de transferencia de dinero
no restringe los *inputs* del usuario de acuerdo con lo que podría
haberse determinado en las reglas empresariales.
En su lugar, permite la entrada de valores negativos,
lo cual puede conducir a un procedimiento imprevisto como,
imaginémonos algo, extraer dinero del, en lugar de depositarlo al,
supuesto destinatario de la transferencia.
Este tipo de vulnerabilidades suelen calificarse
con una severidad muy alta debido
a los posibles impactos de su explotación.
La revisión manual de código tiene como tarea detectar
y notificar estos problemas de seguridad enormemente peligrosos.

Sin embargo, en términos generales,
más que considerar la sustitución
o el reemplazo de un método por otro,
deberíamos hablar de complementación.
Lo ideal sería utilizar ambos métodos, no solo uno.
La revisión automatizada de código, con su alcance y rapidez,
permite reducir el tiempo y el esfuerzo invertidos
en la revisión manual de código.
Permite al experto concentrarse en fragmentos de código
que pueden tener vulnerabilidades más complejas
y graves que las que puede identificar la máquina.
Por desgracia, hay muchos que, en su ignorancia,
siguen creyendo que una o varias herramientas automatizadas
funcionando en sus organizaciones es suficientemente.
No han prestado atención a comentarios
como los formulados en comunidades como
[OWASP](https://owasp.org/www-pdf-archive/OWASP_Code_Review_Guide_v2.pdf),
en los que se da a entender que la revisión manual de código
debería ser un componente del ciclo de vida seguro de una empresa,
ya que en muchos casos es tan buena, o mejor,
que otros métodos de detección de problemas de seguridad.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/revision-codigo-fuente/"
title="Inicia ahora con la solución de revisión de código seguro
de Fluid Attacks"
/>
</div>

## Centrados en la revisión manual de código

### ¿Quién puede realizar la revisión manual de código?

La revisión manual de código pueden realizarla colegas de los desarrolladores,
ingenieros superiores u otros profesionales,
incluidos proveedores externos como Fluid Attacks con
su solución de [revisión de código seguro](../../soluciones/revision-codigo-fuente/).
Las personas encargadas de una revisión exhaustiva de código
deberían tener amplios conocimientos sobre desarrollo de *software*,
lenguajes de programación, [prácticas de codificación segura](../../../blog/secure-coding-practices/),
[estándares](https://help.fluidattacks.com/portal/en/kb/criteria/compliance) de seguridad,
[requisitos](https://help.fluidattacks.com/portal/en/kb/criteria/requirements)
y [vulnerabilidades](https://help.fluidattacks.com/portal/en/kb/criteria/vulnerabilities/),
entre otras cosas.
Deberían ser capaces
de [pensar y actuar como actores de amenazas](../piensa-como-hacker/)
y tener la virtud de la paciencia y una vasta experiencia en revisiones.
Sin embargo, algunos revisores pueden estar empezando a adquirir experiencia
y no deberían ser excluidos,
sino que simplemente pueden recibir asistencia
de otras personas más experimentadas.
Esta decisión, en cualquier caso,
puede variar en función de las necesidades
y características del código de la organización que solicita el servicio.
Ten en cuenta, como se comenta en
la [Guía de revisión de código de OWASP](https://owasp.org/www-pdf-archive/OWASP_Code_Review_Guide_v2.pdf),
por ejemplo,
que un nuevo módulo de autenticación de inicio
de sesión único en una empresa multimillonaria no será revisado
de forma segura por una persona que simplemente una vez leyó
un artículo sobre codificación segura.

### ¿Cómo hacer una revisión manual de código?

Siempre es recomendable que una revisión manual de código
no sea llevada a cabo por una sola persona.
Lo que inicialmente escapó a los ojos del desarrollador,
en algunos casos, también puede eludir los ojos del revisor.
Aunque con la inclusión de una segunda persona también puede ocurrir lo mismo,
las probabilidades se reducirían sin duda.
Además,
el trabajo realizado por varias personas favorece
la creación de un entorno de colaboración entre los miembros
de los equipos de desarrollo y revisión.
La cooperación puede ocurrir cuando
el revisor no es visto como un vigilante,
sino como un asesor que influye positivamente en la eficacia
de los desarrolladores con comentarios pertinentes.
Los revisores deberían pedir a los desarrolladores
que les envíen pequeños cambios
en el código para un mejor flujo de trabajo.
Y pueden hacer las revisiones en ambientes aislados
de aquellos en los que puedan estar trabajando los desarrolladores
para que no actúen como obstáculos.

Para llevar a cabo revisiones integrales y eficaces,
los expertos deben estar en contexto, saber lo que están evaluando.
Deben conocer y comprender ampliamente las funciones,
propósitos y características del *software* que van a evaluar,
incluidos los lenguajes de programación y las bibliotecas
y otros componentes utilizados y sus interacciones.
A partir de ahí, los revisores deberían plantearse preguntas
como qué puede ocurrir si falla alguna operación del código.
Deben saber qué tipos de usuarios están permitidos
y cuáles son sus permisos.
Deben comprender qué tipos de datos se procesan y de qué manera,
y preguntarse qué ocurriría si se vieran expuestos.
Además, deben modelar las amenazas potenciales,
en parte reconociendo los agentes de amenaza y sus posibles propósitos,
y, dependiendo del tipo de *software*,
anticipar qué vulnerabilidades de seguridad pueden encontrar.

La cantidad de información que debe recopilarse
en una revisión de código dependerá del tamaño de la organización,
de las habilidades de los revisores
y de los riesgos potenciales del código.
Una buena forma de iniciar esta recopilación puede ser establecer contacto
y conversación con los desarrolladores de la aplicación
para recibir al menos alguna información básica.
Las herramientas automatizadas también entran en juego
para proporcionar datos generales antes
de aplicar la revisión manual en áreas específicas del código
(priorizadas, por ejemplo, según el riesgo)
para evaluar el estilo, la interpretación y la seguridad.
Herramientas como los *linters* pueden incluso ser utilizadas
de antemano por los desarrolladores para obtener ayuda
en la identificación de errores sencillos de formato,
como los errores tipográficos.

En la revisión de código, manteniendo como soporte
y guía los estándares establecidos o solicitados por la organización,
incluyendo, por supuesto, las reglas empresariales o de negocio,
los revisores aprenderán primero acerca de la arquitectura
y el estilo del código y, por tanto, de sus errores.
En cuanto a los estándares,
los revisores pueden manejar listas de requisitos de seguridad
adaptados al diseño del *software* que no pueden pasarse por alto,
como los siguientes: validación de *inputs*,
autenticación y gestión de contraseñas,
control de acceso y gestión de sesiones,
criptografía y protección de datos,
gestión de errores y registro,
configuración y control del sistema, entre otros.
Los revisores señalarán la superficie de ataque
y los posibles vectores de ataque,
los controles de seguridad implementados hasta el momento
(¿en qué medida se están protegiendo los activos y las operaciones?),
las vulnerabilidades presentes,
los riesgos estimados y los posibles impactos, etc.
La categorización y calificación de las vulnerabilidades
son bastante útiles ya que permiten su priorización
para la posterior remediación por parte del equipo de desarrolladores.

Por cada error o problema de seguridad detectado en el código,
los revisores deben recomendar una modificación
para cumplir los requisitos previamente definidos.
Si es necesario,
vuelven a establecer contacto con el equipo de desarrollo
para iniciar discusiones
(p. ej., sobre la viabilidad de los cambios sugeridos)
o resolver dudas y dificultades.
Posteriormente, si es necesario,
los revisores proponen recomendaciones o soluciones alternativas
y definen excepciones para llegar a un acuerdo.
Tras las incorporaciones o modificaciones por parte de los desarrolladores,
puede tener lugar una nueva revisión para verificar
que han tenido en cuenta las sugerencias
y que su producto es ahora de mayor calidad y seguridad.

## Revisión de código con Fluid Attacks

En Fluid Attacks,
te ofrecemos nuestra revisión de código seguro
(para más detalles, [lee este artículo](../revision-codigo-fuente/)),
con revisión automatizada y manual de código,
dentro de un servicio integral llamado [Hacking Continuo](../../servicios/hacking-continuo/).
Este servicio no se limita
a [pruebas de seguridad de aplicaciones estáticas](../../producto/sast/)
y [análisis de composición de *software*](../../producto/sca/).
Más allá de la intervención de nuestras herramientas automatizadas
(te invitamos a utilizarlas [gratuitamente durante 21 días](https://app.fluidattacks.com/SignUp)),
las cuales también realizan
[pruebas de seguridad de aplicaciones dinámicas](../../producto/dast/),
empleamos métodos como las [pruebas de penetración manuales](../../soluciones/pruebas-penetracion-servicio/)
y la [ingeniería inversa](../../producto/re/)
con nuestros *pentesters* altamente certificados.
Para más información sobre cómo podemos ayudarte a evitar
que tu organización sea víctima de ciberataques,
no dudes en [contactarnos](../../contactanos/).
