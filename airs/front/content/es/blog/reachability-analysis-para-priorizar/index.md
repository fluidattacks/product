---
slug: blog/reachability-analysis-para-priorizar/
title: Introducción a reachability analysis
date: 2025-01-22
subtitle: Hacia una mejor priorización de vulnerabilidades
category: filosofía
tags: ciberseguridad, empresa, tendencia, pruebas de seguridad, software
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1737544064/blog/reachability-analysis-for-prioritization/cover_reachability_analysis_for_vulnerability_prioritization.webp
alt: Foto por Frankie Mish en Unsplash
description: En este post aprenderás sobre reachability analysis, una técnica de evaluación de software que contribuye a priorizar vulnerabilidades para su remediación.
keywords: Analisis De Composicion Del Software, Sbom, Reachability Analysis Estatico, Reachability Analysis Dinamico, Priorizacion De Vulnerabilidades, Dependencias Directas, Dependencias Transitivas, Hacking Etico, Pentesting
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/a-blurry-image-of-a-persons-hand-with-red-nails-XCgAeHhJa0k
---

"Nos estamos ahogando en un mar de informes de seguridad
—¡Suficiente como para querer desaparecer del mapa!".
Estas palabras podrían atribuirse fácilmente a muchos miembros
de equipos de desarrollo y seguridad
de organizaciones comprometidas con su ciberseguridad.
Y es bastante comprensible.
Incluso los pequeños proyectos pueden recibir cientos de reportes
en cuya comprensión y solución los equipos deben invertir enormes cantidades
de tiempo y esfuerzo.
En consecuencia,
resulta normal que muchas vulnerabilidades permanezcan sin remediarse.
Sin embargo,
dichas vulnerabilidades deberían ser las que *menos* riesgo representen,
no las demás,
como a veces puede pasar.
De ahí **la necesidad de priorizarlas**.

Métricas como la puntuación CVSS
(así como la [CVSSF](../metrica-exposicion-al-riesgo-cvssf/)
por parte de Fluid Attacks)
y la EPSS
han surgido y contribuido a lo largo de los años
al propósito de organizar las vulnerabilidades por orden de importancia
para su gestión y remediación.
Han ayudado a los equipos a evitar retrasos
y a utilizar más eficientemente sus recursos.
Sin embargo,
sigue sin ser suficiente.
Problemas de seguridad de gran prioridad
—según la exposición al riesgo que representan—
**siguen perdiéndose entre el ruido de los informes**.

Este problema se hace patente,
en gran medida,
a partir de las vulnerabilidades correspondientes
a los componentes de *software* de terceros.
Ya hemos visto incontables veces la estimación
de que alrededor del 80% del código fuente de una aplicación moderna promedio
está constituido por paquetes o librerías de código abierto
elaborados por terceros.
De ahí que puedan ser muchísimas las vulnerabilidades correspondientes.
De hecho,
el **uso de *software* con vulnerabilidades conocidas**
ha sido el tipo de problema de seguridad
que más hemos reportado en Fluid Attacks
en los últimos años
(véase [Reporte de ataques 2024](https://fluidattacks.docsend.com/view/x6dm4rv2ciu2taae)).
No obstante,
la mera presencia de componentes de terceros vulnerables en una aplicación
no siempre significa que la aplicación sea vulnerable.

Cuando en una aplicación se llevan a cabo evaluaciones
como **SCA** ([*software composition analysis*](../escaneos-sca/)),
se establece el **SBOM** ([*software bill of materials*](../../learn/que-es-sbom/))
para ver en detalle
no solo los componentes definidos explícitamente como dependencias directas,
sino también las subdependencias o dependencias transitivas,
sobre las cuales los desarrolladores pueden no tener control
o incluso desconocer.
En relación con esos árboles de dependencias
y a partir de herramientas automatizadas y bases de datos masivas,
se establecen correlaciones
y se reportan y priorizan riesgos
vinculados con los componentes de *software* de terceros
empleados en las aplicación,
incluyendo vulnerabilidades, código malicioso y problemas de licencias.
Sin embargo,
normalmente no se analiza
**cómo cada componente vulnerable es usado** en la aplicación,
algunos de los cuales pueden incluso no ejecutarse.
Es aquí cuando entra en juego el "***reachability analysis***"
para contribuir a la priorización de problemas de seguridad.
(En español,
podríamos hablar de "análisis de accesibilidad/alcance"
pero en este *post* preferimos mantener la expresión en inglés).

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/gestion-vulnerabilidades/"
title="Inicia ahora con la solución de gestión de vulnerabilidades
de Fluid Attacks"
/>
</div>

## Entendamos reachability analysis

*Reachability analysis* es una técnica de evaluación
de seguridad de *software*
que **determina si los atacantes pueden explotar una vulnerabilidad
dentro de un componente de terceros o una imagen de contenedor
en una aplicación específica**.
Con un enfoque granular,
*reachability analysis* señala las partes exactas
de los componentes utilizados
por determinadas porciones del código de una aplicación.
Esto implica rastrear las rutas de ejecución
para identificar si determinadas funciones o segmentos de código
de los componentes que contienen vulnerabilidades
se invocan o llaman realmente dentro del código fuente
y el contexto de la aplicación.
Hay que tener presente
que un paquete de *software* puede tener múltiples vulnerabilidades,
pero muchas veces solo un subconjunto de sus funciones
(algunas vulnerables y otras no)
es empleado por la aplicación en cuestión.
Incluso llega a decirse
que [solo del 10%](https://www.darkreading.com/application-security/reachability-analysis-static-security-testing-overload)
al 20% del código importado
suele ser usado por una aplicación específica.

Al comprender cómo se utilizan las funciones,
clases, módulos, etc., vulnerables,
**las organizaciones pueden distinguir entre los riesgos teóricos
y los "del mundo real"**,
reduciendo así el ruido asociado a los informes sobre vulnerabilidades.
Esto significa que,
por ejemplo,
algo que antes se consideraba una vulnerabilidad para una aplicación
puede considerarse ahora un problema irrelevante;
dejarlo como riesgo sería un falso positivo.
Esta precisión es especialmente valiosa
cuando se trata de sistemas de *software* complejos
que dependen de numerosos componentes de terceros.
Al dar prioridad a las vulnerabilidades
que son realmente accesibles por la aplicación,
los equipos de seguridad pueden optimizar sus esfuerzos de remediación
y **reducir la superficie de ataque global**.

*Reachability analysis* puede ir más allá de las dependencias directas
y abarcar las transitivas.
Esto significa que la evaluación se adentra
en la intrincada red de componentes o árbol de dependencias
que conforman una aplicación de *software*,
incluyendo aquellos que se encuentran a varias capas de profundidad.
Al considerar estas relaciones indirectas,
las organizaciones pueden identificar vulnerabilidades
que de otro modo podrían pasar desapercibidas.
Este enfoque integral es esencial para las aplicaciones modernas
que dependen de vastos ecosistemas de componentes de código abierto.

## Métodos de reachability analysis

Los equipos de seguridad suelen emplear escáneres
para realizar *reachability analysis*.
Sin embargo,
la revisión manual por expertos
también puede proporcionar información valiosa.
Existen dos enfoques principales para *reachability analysis*:
el análisis estático y el dinámico.
Cada método ofrece ventajas y retos distintos,
y a menudo se considera que un enfoque combinado es la forma más eficaz
de identificar y mitigar los riesgos.

### Análisis estático

El análisis estático **examina la base de código de una aplicación
sin ejecutarla**.
Analizando el código fuente,
los equipos de seguridad pueden determinar
si se están cargando o invocando librerías vulnerables
desde partes específicas de la aplicación,
estableciendo así los llamados gráficos de llamadas o diagramas similares.
Este método es valioso para la integración temprana
en el ciclo de vida de desarrollo de *software* (SDLC),
permitiendo la identificación de vulnerabilidades alcanzables
antes de que lleguen a producción.

Sin embargo,
el análisis estático tiene **limitaciones**.
Puede pasar por alto llamadas o ejecuciones de elementos vulnerables
que solo se manifiestan durante el tiempo de ejecución,
como las desencadenadas por entradas del usuario
en condiciones ambientales específicas.
Además,
puede tener dificultades para tener en cuenta comportamientos complejos
en tiempo de ejecución
influidos por factores como los ajustes de configuración
y los datos obtenidos de fuentes externas.

### Análisis dinámico

El análisis dinámico **se centra en el comportamiento
en tiempo de ejecución de una aplicación**.
Al evaluar la aplicación mientras se ejecuta,
los equipos de seguridad pueden observar qué componentes vulnerables
se utilizan activamente en entornos específicos
y en condiciones particulares.
Este enfoque es eficaz para reducir los falsos positivos,
ya que puede identificar vulnerabilidades
que en realidad no se explotan en la práctica.
Por ejemplo,
una librería vulnerable puede cargarse en una aplicación,
pero sus funciones vulnerables nunca se llaman
durante el funcionamiento normal.
El análisis dinámico también puede utilizarse a veces
para detectar anomalías de comportamiento,
como accesos no autorizados a archivos o conexiones de red.

Sin embargo,
el análisis dinámico se enfrenta a **desafíos**
como una cobertura limitada
debido a la dificultad de explorar todas las posibles rutas de ejecución,
el elevado costo computacional de evaluar
numerosas combinaciones de entradas de usuario,
la sobrecarga de rendimiento introducida por la instrumentación del análisis
y la gestión de grandes volúmenes de datos generados.

### Combinando los análisis estático y dinámico

Un enfoque combinado
que aproveche tanto el análisis estático como el dinámico
proporciona una comprensión más completa de la postura de seguridad
de una aplicación.
El análisis estático puede servir como evaluación preliminar
para identificar riesgos reales y potenciales.
En cambio,
el análisis dinámico puede validar algunas de estas conclusiones
y aprovechar el contexto de la aplicación en tiempo de ejecución
para detectar riesgos adicionales.
Combinando estos métodos,
los equipos de seguridad pueden priorizar con mayor precisión
los esfuerzos de remediación
y reducir el riesgo de explotación de vulnerabilidades en ciberataques.

## Algunos retos de reachability analysis

Reachability analysis se enfrenta a importantes retos
cuando se aplica a proyectos a gran escala.
A medida que aumentan el tamaño y la complejidad de una base de código,
también lo hacen la demanda de recursos informáticos (y humanos)
y el riesgo de errores u omisiones.
**La necesidad de equilibrar precisión y rendimiento pasa a ser primordial**.
Las herramientas de análisis de gran precisión
pueden resultar extremadamente costosas,
sobre todo en proyectos de gran magnitud.
Por el contrario,
las medidas de reducción de costos
que dan prioridad a la velocidad sobre la precisión
pueden dar lugar a tasas más elevadas de falsos positivos y falsos negativos.
Por otro lado,
la presencia de código ofuscado o propietario en las dependencias de terceros
puede obstaculizar los esfuerzos de análisis.

Asimismo,
la naturaleza dinámica del desarrollo de *software*
plantea retos constantes para *reachability analysis*.
A medida que se actualizan los componentes,
la accesibilidad de las vulnerabilidades puede cambiar,
creando potencialmente nuevos riesgos o invalidando hallazgos anteriores.
Esto subraya **la necesidad de un análisis continuo
y una recalibración periódica de las herramientas de análisis**
para adaptarse a la evolución de las tecnologías y las amenazas.
Es importante tener en cuenta que **un resultado negativo
en cuanto al alcance
no significa definitivamente que una vulnerabilidad no pueda explotarse**.
Siempre existe la posibilidad
de que la herramienta de análisis tenga limitaciones
o de que la vulnerabilidad sea alcanzable en condiciones específicas
aún no detectadas.

## Reachability analysis con Fluid Attacks

En Fluid Attacks,
nuestro objetivo es ayudarte a priorizar
y centrar tus esfuerzos de remediación en las vulnerabilidades
que plantean los riesgos más significativos para tus aplicaciones
e infraestructura.
Aunque *reachability analysis* puede realizarse de forma independiente,
**lo ideal es que forme parte de un amplio marco de ASPM
([*application security posture management*](../../soluciones/aspm/))**,
que incluya RBVM (*risk-based vulnerability management*)
y SSCS (*software supply chain security*).
Así es como lo hacemos en Fluid Attacks
dentro de nuestra solución integral, [Hacking Continuo](../../servicios/hacking-continuo/).
Proporcionamos informes completos sobre vulnerabilidades,
incorporando análisis avanzados de accesibilidad
para ayudarte a comprender qué vulnerabilidades son realmente explotables
en tu contexto específico.

Nosotros integramos *reachability analysis* estático directamente en tu SDLC,
lo que permite la detección y remediación temprana de vulnerabilidades
y evita que se conviertan en problemas más graves y costosos.
Mediante la supervisión continua de tu código base,
podemos identificar los componentes vulnerables y su uso en tus aplicaciones.
Este análisis es una parte esencial de nuestras funciones de SCA y SBOM,
que proporcionan una **visión detallada de tus dependencias
y los riesgos asociados**.
Nuestro equipo de investigadores de seguridad
revisa continuamente bases de datos de avisos de vulnerabilidades
en componentes de terceros
y a partir de sus contextos y condiciones específicas
desarrolla reglas personalizadas
para que nuestra herramienta identifique automáticamente su uso en tu código.
Esto garantiza que nuestro análisis sea preciso y esté actualizado.

Nuestro enfoque en *reachability analysis*
se centra en proporcionar resultados altamente fiables.
**Damos prioridad a las vulnerabilidades que son definitivamente accesibles
dentro de tu aplicación**
en lugar de ofrecer etiquetas ambiguas como "*unreachable*"
(i.e., inaccesible).
Esto significa que si una vulnerabilidad no está marcada como "*reachable*",
no es porque hayamos descartado la posibilidad de que sea explotable,
sino porque la evidencia no es lo suficientemente sólida
como para clasificarla como tal.
Al evitar etiquetas como "*unreachable*",
prevenimos que los usuarios malinterpreten los resultados
y creen una falsa sensación de seguridad.

Dentro de nuestra plataforma,
te ayudamos a priorizar las vulnerabilidades
basándonos en una combinación de factores,
como la accesibilidad, la explotabilidad y los impactos potenciales
en la confidencialidad, la integridad y la disponibilidad.
Por ejemplo,
considera un escenario en el que un componente de terceros
en tu lista de dependencias
tiene vulnerabilidades de altas puntuaciones CVSS
y altas probabilidades de explotación (EPSS).
Aunque estos factores indican vulnerabilidades riesgosas,
***reachability analysis* puede proporcionar un contexto adicional**.
Supón que las funciones vulnerables dentro de este componente
están siendo llamadas en tu aplicación.
En ese caso,
tales vulnerabilidades se priorizan sobre otras similares que,
a pesar de tener puntuaciones CVSS y EPSS altas,
no pueden explotarse directamente en tu código base específico.
Esta priorización garantiza que te centres en las vulnerabilidades
que suponen un riesgo inmediato y procesable.

<div class="imgblock">

![Priorización de vulnerabilidades con reachability](https://res.cloudinary.com/fluid-attacks/image/upload/v1737656845/blog/reachability-analysis-for-prioritization/priorizacion-reachability-analysis-fluid-attacks.webp)

</div>

Así es como mostramos los resultados de *reachability analysis*
dentro de nuestra plataforma:

<div class="imgblock">

![Observa tus dependencias](https://res.cloudinary.com/fluid-attacks/image/upload/v1738177778/docs/find-and-fix-vulnerabilities/test-components-via-sca/see-used-dependencies-fluid-attacks-kb.webp)

</div>

Como ejemplo,
si hacemos clic en la primera dependencia (lodash),
podemos ver su ubicación en el archivo de versionado
que contiene la lista de dependencias (package-lock.json)
y la vulnerabilidad que definimos como "*reachable*":

<div class="imgblock">

![Vulnerabilidad accesible](https://res.cloudinary.com/fluid-attacks/image/upload/v1737581436/docs/find-and-fix-vulnerabilities/test-components-via-sca/know-where-reachable-dependency-is-fluid-attacks-kb.webp)

</div>

Esa misma vulnerabilidad podemos verla marcada entre las demás
asociadas a lodash
cuando hacemos clic en el enlace **View details**
que aparece a la derecha de la tabla principal:

<div class="imgblock">

![Reachable CVE](https://res.cloudinary.com/fluid-attacks/image/upload/v1737580951/docs/find-and-fix-vulnerabilities/test-components-via-sca/know-reachable-cve-fluid-attacks-kb.webp)

</div>

Cuando seguimos el enlace de vulnerabilidad
mostrado en la segunda captura de pantalla,
podemos encontrar cuál de nuestros archivos y en qué ubicación
llama a la función de lodash que es vulnerable
y alcanzable (entre otra información):

<div class="imgblock">

![Archivo asociado con la vulnerabilidad alcanzable](https://res.cloudinary.com/fluid-attacks/image/upload/v1734711728/docs/find-and-fix-vulnerabilities/test-components-via-sca/know-reachable-vulnerability-line-of-code-fluid-attacks-kb.webp)

</div>

Este nivel de granularidad nos permite identificar y abordar rápidamente
la causa raíz de cada problema de seguridad.

Aunque estamos orgullosos de nuestras capacidades actuales,
reconocemos que siempre hay margen de mejora.
Nuestro *roadmap* incluye la ampliación de nuestro *reachability analysis*
para identificar más CVEs
e ir más allá de rastrearlos solo en dependencias directas.
Al invertir continuamente en investigación y desarrollo,
nuestro objetivo es proporcionar a nuestros clientes evaluaciones de seguridad
cada vez más integrales y precisas.

Si deseas aprovechar una **prueba gratuita de 21 días**
de nuestra solución Hacking Continuo en el plan Essential,
por favor sigue [este enlace](https://app.fluidattacks.com/SignUp).

> **Nota:**
> Quiero dar las gracias especialmente a los desarrolladores en seguridad
> Julián Gómez y Luis Saavedra
> por la información que me proporcionaron,
> la cual fue de gran ayuda para crear esta entrada de blog.
