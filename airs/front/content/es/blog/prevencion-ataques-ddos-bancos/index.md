---
slug: blog/prevencion-ataques-ddos-bancos/
title: ¿Sitio no disponible? ¡Que no sea de tu banco!
date: 2024-07-09
subtitle: Desarrolla apps bancarias que resistan ataques DDoS
category: filosofía
tags: ciberseguridad, web, tendencia
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1720557693/blog/ddos-attacks-prevention-banking/cover_banking_ddos.webp
alt: Foto por Sarah Kilian en Unsplash
description: Los bancos están recibiendo la mayoría de los ataques DDoS dirigidos a empresas de servicios financieros, las cuales son atacadas más que antes. ¿Cómo proteger tu app de esta amenaza?
keywords: Ataques Ddos Contra Instituciones Financieras, Estrategias De Mitigacion Ddos, Prevencion Ddos En Bancos, Plan De Prevencion Ddos, Denegacion De Servicio Distribuida En Bancos, Seguridad De Aplicaciones Bancarias, Ciberseguridad De Instituciones Financieras Contra Denegacion De Servicio, Hacking Etico, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Escritor y editor
source: https://unsplash.com/photos/brown-ice-cream-cone-52jRtc2S_VE
---

## ¿Cuál es la tendencia de ataques DDoS contra los bancos?

Los ataques de denegación de servicio distribuidos
(DDoS, por su nombre en inglés)
están afectando fuertemente al sector bancario.
Este tipo de ataque consiste en el envío de una gran cantidad de peticiones
o datos procedentes de distintas fuentes (p. ej., sistemas)
a una página, un servidor o una red,
lo que provoca la degradación de la disponibilidad del servicio
que depende de esa página, ese servidor o esa red.
Según un [reporte](https://www.fsisac.com/hubfs/Knowledge/DDoS/FSISAC_DDoS-HereToStay.pdf)
publicado en marzo de 2024,
los ataques DDoS contra los servicios financieros aumentaron un 154%
entre 2022 y 2023.
Este reporte identificó además a los bancos
como las empresas más afectadas dentro del sector de los servicios financieros
en todo el mundo durante 2023,
representando el 63% de todos los ataques DDoS.

Hay varios factores que pueden estar detrás del aumento de los ataques DDoS.
Por ejemplo,
los recientes picos de hacktivismo en medio de las tensiones geopolíticas,
que incitan a los ciberataques en general,
han supuesto más intentos de DDoS,
a los que, por cierto, las empresas están últimamente más atentas.
Así es:
A medida que hacktivistas (p. ej., algunos grupos prorrusos)
han anunciado su intención de atacar a organizaciones financieras
en Estados Unidos y Europa,
estas han informado de más intentos que en el pasado,
al parecer, en parte, debido a su mayor vigilancia.
Otros factores son el aumento de DDoS como servicio
(es decir,
la posibilidad de alquilar la red de sistemas
utilizada para saturar la página, el servidor o la red objetivos),
que hace que la realización de los ataques sea un asunto menos costoso,
y la búsqueda de beneficios económicos,
que es algo bien común.
Un factor que facilita el éxito de los ataques
es la falta de una protección DDoS adecuada.
Más abajo listamos nuestras recomendaciones relativas a esto último.

Un [ejemplo de ataque DDoS](https://www.bankinfosecurity.com/danish-banks-targets-pro-russian-ddos-hacking-group-a-20902)
en el sector
es el que sufrieron algunos de los principales bancos de Dinamarca
a principios de 2023.
El grupo hacktivista prorruso NoName057(16) llevó a cabo el ataque,
que afectó a los sitios web de nueve instituciones objetivo.
La información reportada dice que los sitios web estuvieron fuera de servicio
durante un breve periodo de tiempo,
pero aun así, la interrupción causó retrasos operativos en las organizaciones.

La amenaza creciente de ataques DDoS debería poner a tu banco en alerta máxima.
Las interrupciones del servicio implican retrasos
e insatisfacción y desconfianza de los clientes.
Además, los ataques DDoS, como se afirma en el reporte mencionado,
pueden formar parte de un ataque con *ransomware*
en el que los atacantes intentan coaccionar a las organizaciones
para que paguen el rescate
amenazando con causar la interrupción del servicio en caso contrario.
Hay mucho en juego,
así que hay que estar preparado para este tipo de ataque.
Veamos primero de qué se trata.

## ¿Qué es un ataque DDoS?

Un ataque de denegación de servicio distribuido
es un intento de desbordar sitios web, servidores o redes
con una avalancha de tráfico específico,
como mensajes, solicitudes de conexión o paquetes malformados.

Un atacante puede obtener el control de un dispositivo
aprovechando una vulnerabilidad no parcheada
o a través de una infección de malware tras ataques de [*phishing*](../../../blog/phishing/)
exitosos,
o incluso por [fuerza bruta](../../../blog/pass-cracking/).
Después,
el atacante construye algo como un ejército (algunos lo llaman *zombie army*)
de dispositivos comprometidos o bots
a través de un canal de comunicación establecido de dispositivos infectados.
A esto se le denomina *botnet*.
Para gestionar el *botnet* de forma remota,
el atacante utiliza un servidor de comando y control, o C&C.
Una vez que éste está listo,
el atacante emite órdenes al *botnet* para que ataque el sistema objetivo,
creando una interrupción que puede afectar al ancho de banda,
la capacidad de procesamiento
o la memoria del objetivo.
Los estragos producidos en el funcionamiento del sistema objetivo
impiden a los usuarios legítimos acceder a él,
ya sea a la velocidad deseada o por completo.

La duración de la no disponibilidad del servicio
debido a ataques DDoS varía bastante.
De hecho, a lo largo de su existencia, ha oscilado entre unos minutos y días.
La denegación de servicio por un ataque distribuido [más larga de la historia](https://securelist.lat/ddos-report-q2-2019/89325/)
duró nada menos que 21 días en 2019.
[Datos recientes de Cloudflare](https://blog.cloudflare.com/es-es/ddos-threat-report-for-2024-q1-es-es/)
muestran que a principios de 2024
casi la mitad de los ataques DDoS duraron más de 10 minutos,
mientras que alrededor de un tercio duró más de una hora.
Estos dos datos se refieren a ataques en la capa de aplicación.
Esta categoría específica se refiere
a cuando el *botnet* satura a las aplicaciones
con un alto volumen de peticiones aparentemente legítimas,
como si se tratara de usuarios finales
que utilizan las funcionalidades de tales aplicaciones.

Dado que los ataques DDoS a nivel de aplicación [han aumentado](https://www.fsisac.com/hubfs/Knowledge/DDoS/FSISAC_DDoS-HereToStay.pdf),
es importante que los líderes de TI de los bancos aborden la seguridad
de las aplicaciones bancarias
en relación con este tipo de ataques.
A continuación,
ofrecemos los requisitos de seguridad que recomendamos para el *software*
que deberían formar parte de tus estrategias de mitigación de DDoS.

<div>
<cta-banner
buttontxt="Leer más"
link="/es/soluciones/pruebas-seguridad/"
title="Inicia ahora las pruebas de seguridad de Fluid Attacks"
/>
</div>

## Prevén DDoS desde el mismo desarrollo de aplicaciones

La industria de la ciberseguridad ofrece soluciones de mitigación de DDoS.
Se trata de herramientas y servicios
para prevenir, detectar y detener los ataques DDoS.
Su objetivo es hacer que tu aplicación sea más resistente
sin necesidad de que la protección esté integrada en la propia aplicación.
Un ejemplo son los cortafuegos de aplicaciones web
(WAFs, por su nombre en inglés),
que puedes desplegar para inspeccionar y bloquear el tráfico entrante
con base en conjuntos de reglas predefinidas,
como un volumen de peticiones alto o patrones anómalos.

Pero seguramente quieres que tu aplicación bancaria pueda defenderse
por sí misma
al momento que fallen las soluciones de mitigación de DDoS.
Esto es clave para tu plan de prevención de DDoS.
Entonces,
nuestras siguientes recomendaciones apuntan a desarrollar aplicaciones seguras
contra ataques DDoS por diseño.

- **Implementar un *rate limit*:** Establecer mecanismos
  para restringir el número de solicitudes de un mismo origen o dirección IP
  durante un periodo determinado.

- **Utilizar la validación de solicitudes:** Validar las peticiones entrantes
  para asegurar que se ajustan a los patrones esperados,
  como en cuanto al tamaño, la frecuencia y los tipos de contenido.

- **Crear listas de permitidos y listas de bloqueados:** Establecer las IP
  a las que no se aplica el *rate limit*
  y las que deben bloquearse
  (p. ej., IP maliciosas que ya han sido denunciadas como tales).

- **Introducir mecanismos CAPTCHA o de desafío-respuesta:**
  Exigir la verificación de que el usuario es humano
  en las páginas para las que se anticipan peticiones sospechosas
  o potencialmente maliciosas.
  Esto puede ayudar a diferenciar entre usuarios legítimos y bots.

- **Permitir la escalabilidad horizontal:** Diseñar las aplicaciones
  para que sean escalables horizontalmente,
  es decir, que puedan distribuir el *workload* entre varias máquinas
  o servidores,
  gestionando así las peticiones y transacciones de los usuarios
  de forma más eficaz,
  reduciendo el riesgo de tiempos de inactividad
  y evitando cuellos de botella en el rendimiento.
  Esta escalabilidad es crucial para soportar una base de usuarios creciente,
  altos volúmenes de transacciones
  y servicios rápidos.

- **Utilizar el equilibrio de cargas:** Desplegar aplicaciones
  con equilibradores de cargas
  que puedan distribuir el tráfico entrante uniformemente
  entre varios servidores.
  Esto ayuda a absorber y distribuir el impacto de los ataques DDoS.

- **Tener arquitectura distribuida y redundancia:** Diseñar aplicaciones
  con arquitecturas distribuidas y componentes redundantes
  que puedan soportar fallos localizados
  y mitigar el impacto de ataques DDoS
  manteniendo la disponibilidad del servicio
  desde ubicaciones o servidores alternativos.

En Fluid Attacks,
te ayudamos a encontrar y remediar las vulnerabilidades de seguridad
de tus aplicaciones bancarias.
Inicia la [prueba gratuita de 21 días](https://app.fluidattacks.com/SignUp)
de Hacking Continuo
para disfrutar de pruebas de seguridad automatizadas
que pueden detectar el incumplimiento de requisitos de seguridad
relacionados con DDoS,
como [establecer un *rate limit*](https://help.fluidattacks.com/portal/en/kb/articles/criteria-requirements-327).
Puedes usar nuestra [plataforma](../../plataforma/)
para gestionar las vulnerabilidades detectadas
y [sincronizarla con VS Code](https://help.fluidattacks.com/portal/en/kb/integrate-with-fluid-attacks/use-ide-extensions/vs-code-extension)
para utilizar las opciones de corrección generadas con IA
que ofrecemos en este último.
Es más,
para mejorar la seguridad contra ataques DDoS,
puedes suscribirte a nuestro plan pago [Advanced](../../planes/),
que añade el análisis de vulnerabilidades
por parte de nuestros *pentesters*.
Estos no solo encontrarán vulnerabilidades que las herramientas no detectan,
sino que también asesorarán a tu equipo de desarrollo
en cuanto a cómo eliminarlas.
¿Este plan te parece mejor?
[Contáctanos](../../contactanos/) para obtener más información.
