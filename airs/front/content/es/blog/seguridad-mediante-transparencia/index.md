---
slug: blog/seguridad-mediante-transparencia/
title: Seguridad mediante transparencia
date: 2024-09-27
subtitle: Sé más seguro aumentando la confianza en tu software
category: opiniones
tags: ciberseguridad, riesgo, empresa, tendencia, código
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1727477788/blog/security-through-transparency/cover_security_through_transparency.webp
alt: Foto por photo nic en Unsplash
description: Ser seguro y ser transparente suelen considerarse opuestos, pero no lo son. Descubre cómo la transparencia puede convertirse en un elemento diferenciador.
keywords: Transparencia, Seguridad, Software De Codigo Abierto, Apertura, Transparencia Como Cultura, Oscuridad, Hacking Etico, Pentesting
author: Juan Díaz
writer: jdiaz
name: Juan Díaz
about1: Security Developer
source: https://unsplash.com/photos/person-holding-black-smartphone-_IL9n-5Ou6c
---

Ser seguro y ser transparente suelen considerarse conceptos opuestos.
Hay prácticas como seguridad por oscuridad
que promueven la falta de información como forma de proteger los sistemas.
Si los atacantes no conocen o no entienden tu *software*,
no pueden explotar sus vulnerabilidades.
Por tanto,
detalles como el código fuente, la arquitectura
y la comunicación entre procesos
son guardados en secreto por muchas organizaciones
con la intención de proteger a sus usuarios y sus datos.

Sin embargo,
organismos como el NIST
(National Institute of Standards and Technology)
y el CWE (Common Weakness Enumeration)
identifican la "[Confianza en la seguridad por oscuridad](https://cwe.mitre.org/data/definitions/656.html)"
como una debilidad.
Técnicas como el *black-box hacking*,
la ingeniería inversa o el abuso de fugas de información
permiten explotar sistemas ocultos por oscuridad,
dejando claro que esta estrategia es ineficaz
y solo proporciona una falsa sensación de seguridad.

Si la oscuridad no es una solución,
¿lo será la transparencia?
Creo que la transparencia responsable aporta más beneficios de seguridad
de los esperados,
así que hablemos de ella:

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/pruebas-seguridad/"
title="Inicia ahora con la solución de pruebas de seguridad
de Fluid Attacks"
/>
</div>

## Las razones detrás de la seguridad

La seguridad no solo consiste en proteger los datos de los usuarios
o los elementos diferenciadores de la empresa.
También se trata de preservar la confianza
que los usuarios tienen en el sistema.
La confianza es el activo más importante e invaluable
que puede tener una compañía
y el más difícil de recuperar una vez se pierde.
La confianza en sí misma puede ser un diferenciador de negocio.

Piensa en las aplicaciones que utilizas a diario
y enumera cuántas de ellas prefieres porque te generan confianza.
Por ejemplo,
tu aplicación de transporte favorita
que elegiste por encima de otras opciones más económicas;
tu aplicación de mensajería favorita
que preferiste por encima de otras con más funciones;
tu aplicación bancaria;
tu aplicación de alojamiento, etc.
Cuando las aplicaciones generan confianza,
reciben usuarios que las usan bastante
y que cuentan a otros usuarios su excelente experiencia con ellas.

Empresas como Uber y Airbnb
comprenden [la importancia de la confianza](https://review.firstround.com/how-modern-marketplaces-like-uber-airbnb-build-trust-to-hit-liquidity/)
e invierten activamente en ella.
En sus aplicaciones,
ofrecen contenidos centrados en el usuario,
proporcionan interfaces atractivas
y añaden funciones sociales
(por ejemplo, compartir información sobre viajes o lugares de alojamiento,
votar en sistemas de *rating*, etc.)
para generar confianza a partir de la experiencia.

En definitiva,
las estrategias para atraer o retener a los usuarios basadas en la experiencia
dependen en gran medida de la comprensión de las desventajas
de otras soluciones en el mercado.
¿Qué podrían hacer las empresas para utilizar la confianza de los usuarios
como elemento diferenciador?

## Efecto de la transparencia

Aunque sabemos que la apertura es un factor clave para generar confianza
entre humanos,
también sirve para generar confianza entre humanos y sistemas.
Piensa por un momento en cómo grandes empresas tecnológicas
altamente maduras como Google, Meta, Amazon o Microsoft
facilitan mucha información sobre su infraestructura,
prácticas y estrategias de seguridad sin aparente ánimo de lucro.

AWS, por ejemplo,
invita a las compañías con buenas prácticas en la nube
a [compartir información sobre su infraestructura](https://aws.amazon.com/architecture/this-is-my-architecture/?tma.sort-by=item.additionalFields.airDate&tma.sort-order=desc&awsf.category=*all&awsf.industry=*all&awsf.language=*all&awsf.show=*all&awsf.product=*all)
y cómo resuelven los problemas a los que se enfrentan.
Es más,
la propia AWS comparte cómo construyó sus servicios
para garantizar la seguridad y fiabilidad a todos sus usuarios.

Por su parte,
Microsoft mantiene una excelente documentación,
la cual incluye fundamentos y explicaciones [de sus decisiones técnicas](https://learn.microsoft.com/en-us/microsoft-365/solutions/?view=o365-worldwide),
aportando así conocimientos y ganándose la confianza de los usuarios.
Las prácticas de seguridad también son material que puede documentarse
dentro de la aplicación
y compartirse con el público,
[como hace WhatsApp](https://faq.whatsapp.com/820124435853543), por ejemplo,
con su cifrado de extremo a extremo.

Por otro lado,
Meta tiene un buen ejemplo de [página de *status*](https://metastatus.com/)
en la que la gente puede estar al tanto de los servicios afectados
en todas las aplicaciones
(por ejemplo, Instagram, Facebook, WhatsApp Business API, etc.).
Esta página informa a los usuarios sobre las interrupciones menores y mayores
y el trabajo en curso para solucionarlas.
Además,
las páginas de status pueden incluir métricas y filtros regionales,
como es el caso de [la de AWS](https://health.aws.amazon.com/health/status).

Los usuarios tienen una impresión positiva y una sensación de seguridad
cuando las empresas son transparentes.
Incluso pueden acabar defendiendo incondicionalmente a las compañías
que se han ganado su confianza.
Las empresas son, al fin y al cabo, agentes sociales
que fortalecen las relaciones con sus clientes.

## La transparencia como cultura

Incluso si una compañía implementa prácticas de transparencia
como las descritas anteriormente,
la confianza se construye tanto externamente con los usuarios
como internamente con el equipo.
En una organización,
[la apertura transforma](https://www.axelerant.com/blog/openness-at-axelerant)
la seguridad psicológica,
las conversaciones y el *feedback* que se recibe.
La apertura en la cultura empresarial garantiza las condiciones adecuadas
para crecer y aprender mucho en lo bueno, en lo malo,
en la salud, en la enfermedad...

Los equipos se convierten en comunidades
en las que la resolución de problemas es un reto de grupo
y no un trabajo individual motivado únicamente por el deseo de logro personal
o el miedo a ser despedido.
Los equipos colaborativos nacen de organizaciones transparentes.

Todavía estoy muy fascinado por la mayor [interrupción de GitLab](https://www.youtube.com/watch?v=tLdRBsuvVKc),
pero sobre todo por la respuesta del equipo.
Un desarrollador borró accidentalmente la base de datos de producción
junto con su copia de seguridad principal.
Los ingenieros de GitLab explicaron la situación
e iniciaron una transmisión en directo para documentar sus esfuerzos
por solucionar el problema.
Fue una situación muy estresante,
pero GitLab es un proyecto de código abierto,
y cada colaborador tiene un fuerte compromiso con la transparencia
y el conocimiento abierto.
Así que decidieron compartir públicamente su interrupción
como estrategia de control de daños.

GitLab sigue siendo uno de los *hosts* más utilizados
para almacenar código después de aquel fracaso.
Nos enseñaron a todos a utilizar la transparencia
para mitigar posibles malas impresiones en los usuarios.
Sin duda,
el esfuerzo en grupo y la apertura a la hora de resolver un incidente
protegen y mantienen la confianza de los usuarios
en los productos y servicios.

## Conclusiones

Entre las muchas formas de mejorar la seguridad y generar confianza,
adoptar una estrategia de transparencia bien ejecutada
puede aportar importantes beneficios a tu compañía.
No se trata solo de tener código abierto
o de compartir conocimientos libremente;
se trata también de entender que la confianza se gana
y se mantiene siendo tan transparente como sea necesario
tanto con tus usuarios como con tu equipo para fomentar el crecimiento.

La seguridad a través de la transparencia es una estrategia viable
que las empresas deberían considerar
para reforzar su posición en el mercado.
Ser abierto y compartir conocimientos siempre merece la pena.
Por eso, en Fluid Attacks,
no solo hablamos de transparencia,
sino que la materializamos.
