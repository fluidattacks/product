---
slug: blog/introduccion-ciberseguridad-sector-aviacion/
title: Asegurando el suelo y los cielos
date: 2024-12-06
subtitle: Introducción a la ciberseguridad del sector de la aviación
category: filosofía
tags: ciberseguridad, empresa, riesgo, tendencia
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1733510698/blog/introduction-cybersecurity-aviation-sector/cover_introduction_cybersecurity_aviation_sector.webp
alt: Foto por Logan Weaver en Unsplash
description: En la industria del transporte, el sector de la aviación, en su masiva transformación digital, se ha enfrentado a retos de seguridad que vale la pena destacar.
keywords: Industria Del Transporte, Industria De La Aviacion, Sector Aeronautico, Ciberseguridad De La Aviacion, Ciberseguridad De Aerolineas, Ciberseguridad De Aviones, Hacking Etico, Pentesting
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/airplane-window-view-of-clouds-during-daytime-hLr0qRmQjnM
---

La industria del transporte,
incluido el sector de la aviación,
ha experimentado una espectacular transformación digital
en las últimas décadas.
Desde los avanzados sistemas de control de vuelo
hasta las operaciones en tierra interconectadas,
casi todos los aspectos de la aviación moderna dependen
en gran medida
de la tecnología digital.
Aunque este avance ha aportado importantes beneficios,
también ha introducido nuevos riesgos en el sector:
las ciberamenazas.

Los ciberataques y las filtraciones de datos ocurren continuamente,
año tras año,
en el sector de la aviación del siglo XXI.
Hace poco más de diez años,
por ejemplo,
Cathay Pacific Airways sufrió una de las violaciones más escandalosas
de este sector.
[Más de nueve millones de clientes](https://www.aviationtoday.com/2024/09/19/cybersecurity-in-the-skies/)
de todo el mundo
vieron afectados sus datos personales en un ataque
que no se [descubrió y reveló hasta 2018](https://www.zdnet.com/article/cathay-pacific-hit-with-500000-fine-for-customer-data-breach/).
¿Qué fue lo que permitió este incidente?
Los sistemas informáticos de la compañía no contaban
con las medidas de ciberseguridad adecuadas,
como autenticación multifactor, parcheado de *software* vulnerable
y cifrado de datos.

Los ciberataques en la industria de la aviación
pueden tener consecuencias de gran alcance,
que van desde interrupciones menores de las aplicaciones
hasta fallos catastróficos y filtraciones de datos.
A medida que los ciberdelincuentes se vuelven cada vez más sofisticados,
es imperativo que las organizaciones de aviación den prioridad
a la ciberseguridad
e implementen sólidos mecanismos de prevención, defensa y respuesta.
Estas empresas deberían comprometerse a mantener su exposición al riesgo
en ciberseguridad al mínimo,
al igual que han tenido que hacer durante tantos años
en lo que respecta a la seguridad física.

Hace unas décadas,
la ciberseguridad no era una preocupación primordial
para [la industria de la aviación](https://www.aviationtoday.com/2024/09/19/cybersecurity-in-the-skies/).
Para garantizar la integridad de los datos
se utilizaban sobre todo simples CRC
(controles o verificaciones de redundancia cíclica)
y mecanismos equivalentes,
sin tener apenas en cuenta a los actores maliciosos.
Sin embargo,
a medida que la tecnología avanzada se fue integrando
en las operaciones de aviación,
el panorama de las amenazas empezó a evolucionar.
Los esfuerzos iniciales en materia de ciberseguridad
se centraron en los sistemas de TI y TO en tierra.
No obstante,
con la creciente conectividad de los sistemas de las aeronaves,
incluidos sensores, lectores biométricos y robots,
la superficie de ataque se ha ampliado significativamente.
Hoy en día,
las ciberamenazas pueden dirigirse a todo,
desde las infraestructuras en tierra hasta los sistemas en vuelo.

Siguiendo las declaraciones de Josh Wheeler,
citadas en el boletín Avionics International,
[destacó que](https://www.aviationtoday.com/2024/09/19/cybersecurity-in-the-skies/)
**la altitud no garantiza la seguridad**;
si la Internet puede acceder a la aeronave,
los datos de la aeronave también son accesibles a la Internet.
Al igual que la ciberseguridad terrestre,
la ciberseguridad de la aviación opera
dentro de una compleja cadena de suministro
en la que intervienen aeropuertos, FBOs, planificadores de viajes,
sistemas de gestión de combustible, empresas de *catering* y otras entidades,
todas las cuales pueden influir en la cibervigilancia general.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/pruebas-penetracion-servicio/"
title="Inicia ahora con las pruebas de penetración como servicio
de Fluid Attacks"
/>
</div>

## Riesgos clave de ciberseguridad para el sector de la aviación

El sector de la aviación debería reconocer
las múltiples y siempre cambiantes ciberamenazas
que se dirigen contra él y sus clientes.
Las organizaciones deberían prevenir, vigilar y mitigar estas amenazas,
las cuales pueden provocar importantes interrupciones y daños.
[Los *hackers* maliciosos suelen atacar](https://www.aerospacetestinginternational.com/features/staying-cybersecure-in-the-aerospace-sector.html)
sistemas críticos
como servidores de historial de vuelos, plataformas de reservas,
sitios web de reserva de boletos, dispositivos de la tripulación de cabina
y sistemas de gestión de vuelos.

### Ransomware

Los ataques con *ransomware* se han convertido en una amenaza omnipresente
[en diversas industrias](../../../blog/citrix-bleed-and-ransomware-attacks/),
pero en un peligro de primer orden para el sector de la aviación.
[Mediante este método](../../../blog/ransomware/),
los ciberdelincuentes cifran sistemas y datos críticos
y luego exigen un rescate para descifrarlos.
Estos ataques pueden interrumpir operaciones,
retrasar vuelos y comprometer información sensible.
[Según ha informado Eurocontrol](https://www.eurocontrol.int/publication/eurocontrol-data-snapshot-39-ransomware-groups-targeting-aviations-supply-chain)
(la Organización Europea para la Seguridad de la Navegación Aérea)
basándose en datos de 2021 y 2022,
se registran aproximadamente **2,5 ataques de *ransomware* por semana**
en organizaciones europeas relacionadas con la aviación.
(Dicho valor podría ser mayor,
pero debemos recordar que a muchas compañías no les gusta admitir
que fueron víctimas de ciberataques).

Un ejemplo de este tipo de incidentes es el caso de Embraer.
Esta empresa brasileña,
entonces uno de los mayores fabricantes de aviones del mundo
por detrás de Airbus y Boeing,
[fue víctima de un ataque de *ransomware*](https://www.zdnet.com/article/hackers-leak-data-from-embraer-worlds-third-largest-airplane-maker/)
a finales de 2020.
Al parecer,
Embraer se negó a negociar con los delincuentes
y a pagar el rescate correspondiente,
por lo que estos decidieron filtrar los archivos privados de la compañía,
incluyendo contratos comerciales, datos de los empleados y código fuente,
en un sitio dentro de la [*dark web*](../../../blog/dark-web/).

También está el caso de los proveedores de piezas de aviones,
[como ASCO](https://www.zdnet.com/article/ransomware-halts-production-for-days-at-major-airplane-parts-manufacturer/),
una conocida empresa internacional en este campo.
En 2019,
ASCO tuvo que interrumpir la producción en fábricas de cuatro países,
enviando temporalmente a casa a más del 70% de sus trabajadores
debido a un ataque de *ransomware* en su planta de Bélgica.
[Más recientemente](https://www.globalair.com/articles/continental-aerospace-latest-to-be-hacked-as-aviation-cyberattacks-rise?id=6999),
Continental Aerospace Technologies,
un fabricante de motores aeronáuticos de Alabama,
sufrió un ciberataque atribuido al ransomware PLAY.
Aunque no se facilitaron detalles del impacto,
se supone que las operaciones de fabricación se vieron interrumpidas
y la propiedad intelectual comprometida.

### Ataques DDoS

Los ataques distribuidos de denegación de servicio (DDoS)
pueden saturar los recursos de red de una organización,
dificultando o imposibilitando el acceso de los usuarios legítimos
a los servicios.
Aunque los ataques DDoS pueden no comprometer directamente datos sensibles,
pueden **interrumpir significativamente operaciones**
como el acceso a sitios web y sistemas de reservas en línea.
Los recientes ataques a sitios web de aeropuertos
ponen de manifiesto el impacto potencial de los ataques DDoS
en el sector de la aviación.

[En 2022](https://www.npr.org/2022/10/10/1127902795/airport-killnet-cyberattack-hacker-russia),
la banda criminal rusa KillNet supuestamente derribó temporalmente
los servicios de varios sitios web de aeropuertos estadounidenses,
aparentemente sin afectar los sistemas internos de los aeropuertos
ni las operaciones de vuelo.
Más recientemente,
hubo un ataque DDoS [contra el aeropuerto John Lennon de Liverpool](https://thecyberexpress.com/liverpool-airport-cyberattack/),
aparentemente perpetrado por un grupo llamado Anonymous Collective.
Al parecer,
esta banda lo hizo en represalia por el apoyo del Reino Unido a Israel
en sus ataques contra Palestina
y consiguió interrumpir el acceso de los usuarios al sitio web del aeropuerto.

Además de atacar los sitios web de las aerolíneas,
los actores de amenazas también pueden afectar sistemas más críticos
con este tipo de ataques.
[Por ejemplo](https://www.zdnet.com/article/hackers-prompt-flight-cancellation-at-polish-airport/),
en 2015, la aerolínea polaca LOT sufrió un ciberataque
que comprometió sus sistemas de gestión de vuelos durante varias horas.
El incidente provocó retrasos y cancelaciones de vuelos,
afectando a aproximadamente 1.400 pasajeros.
Este caso subraya la necesidad crítica
de contar con infraestructuras informáticas resistentes
y planes eficaces de respuesta a incidentes.

### Ataques patrocinados por el Estado

Aunque dentro de este conjunto de riesgos clave
podemos incluir métodos de ataque como los dos descritos anteriormente,
las noticias actuales relativas a las industrias de aviación y aeroespacial
nos invitan a destacarlos por separado.

Los actores estatales suponen una amenaza significativa
para la industria de la aviación.
Estos adversarios avanzados
pueden tener como objetivo infraestructuras críticas,
propiedad intelectual e información sensible.
Lejos de estar motivados económicamente
como los grupos criminales independientes,
estos atacantes pretenden obtener valiosa inteligencia,
interrumpir operaciones o incluso sabotear aeronaves
comprometiendo los sistemas de aviación.
[Por ejemplo](https://www.zdnet.com/article/china-targets-aviation-industry-to-spy-and-steal-industry-secrets/),
algunos grupos de *hacking* chinos,
en particular,
han sido vinculados a ataques selectivos contra el sector de la aviación.
Su objetivo es robar información sensible
para ayudar a sus departamentos gubernamentales y militares,
e incluso a sus mercados de aviación,
a avanzar en sus capacidades tecnológicas.
Sin embargo,
debemos recordar que otros ataques patrocinados por Estados
pueden significar nuevas formas de **ciberterrorismo**.

### Ataques a la cadena de suministro

La industria aeronáutica depende de una compleja cadena de suministro global
en la que participan numerosos fabricantes, proveedores
y prestadores de servicios.
Un ciberataque a un único proveedor de componentes
puede permitir a los atacantes introducir *software* malicioso
y llevar a cabo ataques más complejos contra otros proveedores,
generando importantes consecuencias.
Como sugería la Asociación de Industrias Aeroespaciales
[en un informe reciente](https://www.aia-aerospace.org/publications/supply-chain-cybersecurity-recommendations-report/),
en la aviación, los ataques pueden afectar a casi todo
dentro de la cadena de suministro.
Esto incluye los datos utilizados para diseñar y construir estructuras,
los componentes electrónicos (p. ej., *software* y *firmware*)
y los sistemas utilizados para fabricar componentes electrónicos
y no electrónicos.
Hoy en día,
la importancia de [proteger toda la cadena de suministro](../../../blog/software-supply-chain-security/)
para mitigar estos riesgos es indiscutible.

### Ataques en vuelos

La creciente conectividad de las aeronaves
ha introducido nuevos retos de ciberseguridad.
La conexión Wi-Fi y otras tecnologías inalámbricas durante el vuelo
pueden exponer los sistemas de los aviones a posibles ciberamenazas.
Además, los dispositivos de los pasajeros,
como ordenadores portátiles y teléfonos inteligentes,
pueden servir como vectores de ataque.
En la última década,
han surgido preocupaciones sobre el potencial **hackeo de aeronaves**.
En 2015,
[el investigador de seguridad Chris Roberts](https://www.zdnet.com/article/a-practical-history-of-plane-hacking-beyond-the-hype-and-hysteria/)
supuestamente expuso vulnerabilidades en sistemas de aeronaves comerciales
a través de un aparente "hackeo en pleno vuelo",
haciendo saltar las alarmas sobre la posibilidad
de que actores maliciosos exploten estas debilidades.

Si los *hackers* pueden manipular un carro,
¿por qué no podrían secuestrar un avión?
[George Avetisov comentó](https://www.forbes.com/sites/georgeavetisov/2019/03/19/malware-at-30000-feet-what-the-737-max-says-about-the-state-of-airplane-software-security/)
una vez en Forbes
que si el *software* de automatización de un avión tiene tanta autoridad
que puede ignorar los *inputs* del piloto,
cabe suponer que un *malware* sofisticado podría explotar el mismo problema.
Las puertas reforzadas de la cabina son inútiles
si un atacante puede manipular el *software*
para tomar el control de un avión de forma remota.
Aunque el sector de la aviación ha tomado medidas para hacer frente
a estos riesgos,
sigue siendo un reto importante.

Desde la inmovilización en tierra del avión Boeing 737 Max en [2019](../../../blog/ceo-bugs/),
se ha desatado una intrigante conversación mundial sobre la seguridad aérea.
[La investigación sobre los accidentes](https://www.forbes.com/sites/georgeavetisov/2019/03/19/malware-at-30000-feet-what-the-737-max-says-about-the-state-of-airplane-software-security/)
de los vuelos 302 de Ethiopian Airlines y JT610 de Lion Air
ha desplazado el foco de atención de las medidas de seguridad tradicionales
a los complejos sistemas de *software* que impulsan las aeronaves modernas.
Un área de especial preocupación
es el sistema de aumento de las características de maniobra (MCAS),
una tecnología relativamente nueva
diseñada para mejorar la seguridad de los vuelos.
Sin embargo,
el rápido despliegue de estos sistemas sin las pruebas
y la supervisión adecuadas
plantea interrogantes sobre las posibles vulnerabilidades de ciberseguridad
y el riesgo de interferencias malintencionadas.

Los ataques de [*spoofing*](https://www.reuters.com/technology/cybersecurity/gps-spoofers-hack-time-commercial-airlines-researchers-say-2024-08-10/)
o [interferencia del GPS](https://www.reuters.com/business/aerospace-defense/what-is-gps-jamming-why-it-is-problem-aviation-2024-04-30/),
que pueden manipular los sistemas de navegación de las aeronaves,
suponen hoy en día un riesgo significativo adicional.
Esta técnica consiste en transmitir señales GPS falsas
para perturbar los sistemas de navegación.
Emitiendo desde tierra señales más fuertes que las de los satélites,
los atacantes pueden engañar a los aviones y sus pilotos
haciéndoles creer que se encuentran en una ubicación diferente.
Esto podría [provocar situaciones peligrosas](https://www.dailymail.co.uk/sciencetech/article-13742823/Cyberattack-airline-hack-gps-attack-Russia-Ukraine-Iraq.html),
como colisiones o aterrizajes imprevistos.
Aunque las compañías aéreas son cada vez más conscientes de esta amenaza,
muchas siguen dependiendo en gran medida del GPS para la navegación.
En caso de ataque,
los pilotos pueden verse obligados a utilizar métodos de navegación obsoletos
o menos precisos,
lo que podría comprometer la seguridad del vuelo.

## Algunas medidas prácticas de seguridad

Las organizaciones de aviación deben aplicar estrategias de ciberseguridad
integrales para mitigar los riesgos que plantean las ciberamenazas.
Aunque las medidas de seguridad tradicionales,
como contraseñas seguras, autenticación multifactor, cifrado robusto,
actualización de componentes de *software*, *firewalls*
y *software* antivirus, son esenciales,
se requieren estrategias adicionales
para protegerse contra ataques sofisticados.

- **Segmentación de redes:**
  Al dividir las redes en segmentos más pequeños,
  las organizaciones pueden limitar el impacto potencial de una brecha.

- **Arquitectura de confianza cero:**
  Este modelo de seguridad asume que ningún usuario o dispositivo
  es intrínsecamente fiable,
  por lo que requiere una verificación y autorización estrictas.

- **Intercambio de información sobre amenazas:**
  Colaborar con otras organizaciones para compartir inteligencia sobre amenazas
  puede ayudar a detectar y responder a las amenazas emergentes.

- **Cultura de seguridad sólida:**
  Fomentar una sólida cultura de seguridad entre los empleados es crucial.
  La formación periódica en materia de seguridad puede ayudar a prevenir
  los errores humanos, que suelen ser un factor importante en los ciberataques.

- **Escaneo de vulnerabilidades y *pentesting* frecuentes:**
  Las evaluaciones continuas realizadas por herramientas automatizadas
  y expertos en seguridad o *pentesters*
  pueden identificar vulnerabilidades de seguridad y puntos débiles
  en el *software*
  que deberían remediarse para contribuir a la seguridad de las empresas
  y los usuarios.

La industria de la aviación es un sector digitalizado crítico
que debe priorizar la implementación de medidas de ciberseguridad
para proteger los datos, las operaciones y a los pasajeros.
Este tipo de seguridad debería elevarse a nivel directivo
en este y otros sectores,
subrayando su importancia estratégica.
Al situar la responsabilidad al más alto nivel de la organización,
puedes garantizar que la ciberseguridad reciba la atención
y los recursos necesarios.
Si tu compañía está interesada en recibir la contribución de Fluid Attacks
para madurar en este ámbito mediante pruebas de seguridad rápidas y precisas
a través de métodos automatizados y manuales,
no dudes en [contactarnos](../../contactanos/).
