---
slug: blog/que-es-gestion-de-vulnerabilidades/
title: ¿Qué es la gestión de vulnerabilidades?
date: 2023-02-03
subtitle: Cómo funciona este proceso y qué beneficios conlleva
category: filosofía
tags: ciberseguridad, pruebas de seguridad, pentesting, hacking, empresa
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1675463182/blog/what-is-vulnerability-management/cover_what_is_vulnerability_management.webp
alt: Foto por Marino Linic en Unsplash
description: Aprende sobre el proceso de gestión de vulnerabilidades, sus etapas y los beneficios que tu organización puede obtener implementándolo.
keywords: Gestion De Vulnerabilidades, Proceso De Gestion De Vulnerabilidades, Herramienta De Gestion De Vulnerabilidades, Evaluacion De Vulnerabilidades, Escaneo De Vulnerabilidades, Pruebas De Penetracion, Identificacion De Vulnerabilidades, Hacking Etico, Pentesting
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/d8Agul_A6dE
---

Vulnerabilidades de seguridad.
Siempre hablaremos de ellas.
En Fluid Attacks son parte de nuestra razón de ser.
En nombre de nuestros clientes,
los objetivos iniciales que nos planteamos con ellos son identificar,
describir y notificar estos problemas de seguridad.
Pero nuestra misión no termina ahí.
En [nuestro artículo anterior](../evaluacion-de-vulnerabilidades/),
hablamos principalmente de la **evaluación de vulnerabilidades**,
un procedimiento que cubre los objetivos anteriores.
En esta ocasión, nos centraremos en explicar un término
que ya relacionamos con el anterior:
la **gestión de vulnerabilidades**.
Este concepto abarca una gama más amplia
de actividades relacionadas con las vulnerabilidades,
las cuales también ofrecemos a nuestros clientes.
Antes de entrar en materia,
empecemos contextualizando para quienes no tengan claro
qué es una vulnerabilidad en ciberseguridad.

## ¿Qué es una vulnerabilidad?

Una vulnerabilidad de seguridad se refiere a una debilidad
en un sistema informático que
suele ser el resultado de fallas de _software_,
errores de diseño o configuraciones incorrectas.
Al ser explotada por atacantes, una vulnerabilidad
puede funcionar como una puerta que permite el acceso
no autorizado y el control del sistema para el robo de información
u otros activos o la interrupción de operaciones.

### Diferencias entre una vulnerabilidad, un riesgo y una amenaza

Una **vulnerabilidad** es, por ejemplo,
dejar desbloqueado un punto de acceso
a información confidencial de tu sistema.
Esto no significa necesariamente que se vaya a producir un daño,
pero existe esa posibilidad.
En sí, este es el **riesgo**: una situación peligrosa,
una posibilidad de que ocurra algo malo.
La valoración del riesgo dependerá de la probabilidad
de que se produzca el efecto negativo
y de los gastos que este supondría para sus propietarios
u otras partes implicadas.
En nuestro ejemplo,
la posibilidad es que alguien no autorizado acceda ilegalmente
a la información confidencial para usos ilícitos.

Aunque, en el diccionario,
la palabra "**amenaza**" también tiene entre sus
definiciones ["posibilidad de problema](https://www.oxfordlearnersdictionaries.com/us/definition/english/threat?q=threat),
peligro o desastre" y, en ciberseguridad,
a veces se utiliza indistintamente con la palabra "riesgo",
en nuestro caso, preferimos atribuir
"amenaza" al agente que puede generar el efecto negativo o daño.
Sin embargo, esta última definición también se le da a
"riesgo" en el diccionario. (Es como si prácticamente definieran lo mismo.)
Por eso a veces es mejor hablar explícitamente de
"actor de amenaza" (_threat actor_).
En resumen, si tienes una vulnerabilidad en tu sistema,
existe el riesgo de que un actor de amenaza la explote para perjudicarte.

## ¿Cómo se clasifican y categorizan las vulnerabilidades?

Las vulnerabilidades de seguridad suelen clasificarse
según su severidad (es decir, el nivel de riesgo que representan)
de acuerdo con el [Common Vulnerability Scoring System](https://www.first.org/cvss/)
(CVSS).
Este estándar internacional gratuito y abierto al público,
administrado por la organización sin ánimo de lucro FIRST,
presenta una escala cuantitativa que va de 0,0 a 10,0,
la cual suele dividirse en una escala
cualitativa de calificación de severidad,
como se muestra en la siguiente tabla:

| **Puntuación CVSS** | **Calificación** |
| :-----------------: | :--------------: |
|         0.0         |     Ninguna      |
|      0.1 - 3.9      |       Baja       |
|      4.0 - 6.9      |      Media       |
|      7.0 - 8.9      |       Alta       |
|     9.0 - 10.0      |     Crítica      |

La puntuación CVSS completa se compone de tres grupos de métricas:
Base, Temporal y Ambiental.
La **puntuación Base** representa la severidad
de una vulnerabilidad según sus características inherentes
permanentes a lo largo del tiempo y en diferentes entornos.
Esta métrica aborda principalmente la facilidad
con la que la vulnerabilidad puede ser explotada
y los impactos directos de su explotación.
La **puntuación Temporal** refleja la severidad
de una vulnerabilidad en función de sus características
cambiantes a lo largo del tiempo.
Así, esta métrica tiene en cuenta la existencia
y disponibilidad de código de explotación y parches de remediación.
La **puntuación Ambiental** representa la severidad de
una vulnerabilidad según sus características relevantes
para un entorno de usuario específico.
Esta métrica tiene en cuenta factores como los controles
de seguridad presentes y la importancia relativa
del sistema vulnerable dentro de toda una infraestructura informática.

Aunque es habitual que las organizaciones utilicen
e informen sobre la métrica Base,
es recomendable que los usuarios incluyan en la ecuación
las otras dos métricas para modificar las puntuaciones
Base y revelar severidades de vulnerabilidades completas y más precisas,
ajustadas a sus entornos. Para conocer algunas de
las deficiencias de la métrica CVSS,
te invitamos a leer [este artículo](../../../blog/cvssf-risk-exposure-metric/).

Las vulnerabilidades de seguridad divulgadas públicamente
en todo el mundo suelen catalogarse y describirse en listas
de libre acceso como la [CVE](https://www.cve.org/)
(Common Vulnerabilities and Exposures).
Por otro lado, la [CWE](https://cwe.mitre.org/)
(Common Weakness Enumeration)
enumera y categoriza los distintos tipos
de debilidades en _software_ y _hardware_ que pueden
dar lugar a vulnerabilidades explotables.
La CWE ofrece incluso un Top 25 de las debilidades de _software_ más peligrosas,
similar al [Top 10 de la OWASP](https://owasp.org/www-project-top-ten/).
Sin embargo,
este último destaca específicamente los riesgos de seguridad
más críticos para las aplicaciones _web_
(riesgos asociados a vulnerabilidades de seguridad).

### ¿Cuáles son los diferentes tipos de vulnerabilidades en ciberseguridad?

Tipos de vulnerabilidades hay muchos.
Aunque existen diferentes criterios para categorizar las vulnerabilidades,
en Fluid Attacks, por ejemplo, aplicamos uno similar al utilizado en
[CAPEC](https://capec.mitre.org/)
(Common Attack Pattern Enumeration and Classification)
para organizar los patrones de ataque.
Lo que hacemos es separar los tipos de vulnerabilidades
por mecanismos de ataque frecuentemente utilizados para su explotación,
que en este caso son nueve grupos.
Como la intención aquí no es listar todos los tipos,
lo que ofrecemos a continuación es una lista de los grupos
con tres tipos de vulnerabilidades como ejemplos para cada uno.
Por favor, consulta la
[sección Criteria de nuestra documentación](https://help.fluidattacks.com/portal/en/kb/criteria/vulnerabilities/)
para ver la lista completa.

### Subversión de acceso

- Escalada de privilegios
- Falsificación de solicitud entre sitios
- Acceso no autorizado a archivos

### Recopilación de información

- Uso de _software_ con vulnerabilidades conocidas
  (es decir, _software_ obsoleto o sin parches;
  la vulnerabilidad más común entre los sistemas
  que evaluamos en 2022, según nuestro
  [State of Attacks 2023](https://fluidattacks.docsend.com/view/h5vcqn8eh8pdrguj))
- Información confidencial no cifrada
- Información sensible en el código fuente

### Abuso de funcionalidad

- Archivos no verificables
- Dependencias no actualizables
- Denegación de servicio asimétrica

### Interacciones engañosas

- Falta de comprobación de la integridad de los subrecursos
- Falsificación de solicitud del lado del servidor
- _Token_ generado de forma insegura

### Manipulación de protocolos

- Encabezados HTTP inseguros o no configurados
- Métodos HTTP inseguros habilitados
- Método de autenticación inseguro

### Inyección inesperada

- Inyección SQL
- Ejecución remota de comandos
- Secuencia de comandos entre sitios reflejada

### Técnicas probabilísticas

- Credenciales débiles adivinadas
- Política de credenciales débil
- Falta de protección contra ataques de fuerza bruta

### Manipulación del sistema

- Control inadecuado del tamaño de archivos
- Inyección de registros
- _Sideloaded_

### Manipulación de datos

- Validación insuficiente de autenticidad de datos
- Inclusión de archivos locales
- Lectura fuera de límites

## ¿Cómo protegerse contra las vulnerabilidades?

Las vulnerabilidades de seguridad podrían estar presentes
en nuestros sistemas informáticos sin ningún problema
si no existieran los actores de amenazas.
(De hecho, si ellos no existieran,
seguramente las primeras no se llamarían así.)
Pero esto no es más que un sueño inalcanzable.
Los ciberdelincuentes recorren sin cesar
las redes de información buscando víctimas,
motivados principalmente por el dinero.
Lo que quieren inicialmente es descubrir nuestras vulnerabilidades.
Si estas no existieran, los atacantes no podrían perjudicarnos.
Así que, en última instancia, de lo que buscamos protegernos
es de los actores de amenazas y de sus acciones malintencionadas,
no de las vulnerabilidades. Sin embargo, lo que está a nuestro alcance,
lo que podemos controlar, es la prevención
y el tratamiento de las vulnerabilidades.
Y esto es algo que podemos conseguir
con la **gestión de vulnerabilidades**.
Pero, ¿qué es la gestión de vulnerabilidades en ciberseguridad?

## Proceso de gestión de vulnerabilidades

La gestión de vulnerabilidades es un proceso de
identificación, evaluación, priorización, notificación y tratamiento
de vulnerabilidades de seguridad.
Aunque no tiene por qué considerarse un proceso continuo,
lo ideal sería que así fuese. De hecho, así es como suele ofrecerse.
Pero, ¿por qué tiene que ser un proceso o ciclo continuo?
El entorno cibernético está cambiando constantemente.
Surgen nuevos sistemas, evolucionan, se hacen más complejos y se expanden;
con ellos, aparecen nuevas vulnerabilidades y, por desgracia,
nuevas amenazas. El procedimiento de gestión de vulnerabilidades continuo,
con una repetición estable de las distintas etapas a modo de ciclo,
pretende atender esta situación.
Evaluar los sistemas y sus vulnerabilidades en un único momento,
o solo ocasionalmente, es quedarse atrás
frente a los gigantescos saltos que dan las tecnologías de la información.
Y esto puede significar una alta exposición
al riesgo de sufrir por culpa de ciberataques.

La estrategia de gestión de vulnerabilidades
de seguridad puede formar parte de un programa de seguridad general,
el cual también podría incluir estrategias como la implementación
de defensas y la revisión del tráfico,
la capacitación sobre seguridad para diferentes grupos de empleados,
el desarrollo de planes de respuesta a incidentes, etc.
La gestión de vulnerabilidades es un enfoque preventivo.
Este procedimiento pretende ayudar a reducir
y mantener al mínimo la exposición al riesgo de una organización.
Las herramientas de gestión de vulnerabilidades agilizan
y facilitan las operaciones dentro del ciclo
y a menudo permiten que todo tenga lugar desde un único tablero de gestión.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/gestion-vulnerabilidades/"
title="Empieza ya con la solución Gestión de vulnerabilidades de Fluid Attacks"
/>
</div>

### Diferencia entre evaluación de vulnerabilidades y gestión de vulnerabilidades

Cuando hablamos de gestión de vulnerabilidades,
la evaluación de vulnerabilidades debe entrar necesariamente a la ecuación.
En cambio, esta última puede darse de forma independiente.
La evaluación de vulnerabilidades forma parte del proceso
de gestión de vulnerabilidades porque es cuando se identifican,
clasifican y notifican los problemas de seguridad.
Esta evaluación puede realizarse por parte de herramientas automatizadas
(es decir, escaneo de vulnerabilidades) o humanos
(normalmente a través de [pruebas de penetración](../../soluciones/pruebas-penetracion-servicio/)
o [_hacking_ ético](../../soluciones/hacking-etico/));
aun así, debería existir un complemento entre ambos.
Así, la evaluación de vulnerabilidades se integra
en un ciclo en el que también se priorizan
y abordan las vulnerabilidades.

### ¿Cómo automatizar la gestión de vulnerabilidades?

Al igual que en el caso de la evaluación de vulnerabilidades,
la gestión de vulnerabilidades puede llevarse a cabo
mediante herramientas automatizadas, al menos en parte.
De forma similar a lo que ocurre para la identificación
de problemas de seguridad en los sistemas informáticos,
la contribución de las herramientas para un procedimiento
tan esencial de la gestión de vulnerabilidades
como es la remediación de vulnerabilidades es limitada.
De hecho, esta limitación es mucho más marcada que la primera.

Lo que algunas herramientas automatizadas parecen capaces
de hacer hasta ahora es remediar
vulnerabilidades demasiado básicas y superficiales,
muchas de ellas presentes en componentes
de terceros de código abierto que solo requieren
la identificación y aplicación de parches
o actualizaciones para ser solucionadas.
Por tanto, la intervención humana sigue siendo fundamental.
Es cierto que,
como viene ocurriendo desde hace tiempo
en diferentes fases de la gestión de vulnerabilidades,
las herramientas permiten cada vez más ahorrar tiempo
y esfuerzo para que los expertos en seguridad
y desarrollo puedan centrarse directamente en las vulnerabilidades
más complejas y riesgosas.
Sin embargo, **una correcta gestión de vulnerabilidades
hoy en día no puede automatizarse por completo**.

## Etapas en el ciclo de vida de la gestión de vulnerabilidades

### Roles y responsabilidades de la gestión de vulnerabilidades

En primer lugar, debemos hacer referencia a algunas tareas
a realizar antes de iniciar el ciclo de vida de la gestión de vulnerabilidades.
La organización que pretende implementar esta solución debería
comenzar por definir el alcance del proceso.
Debería tener claro cuáles de sus sistemas
y componentes serán evaluados para la detección de vulnerabilidades.
(Esto puede requerir servicios de inventario de activos.)
En relación con los activos implicados en ese entorno,
la organización en cuestión debería asignarles valores
para determinar cuáles son los más críticos a proteger.

Además, la organización debería definir los **roles y responsabilidades**
para su equipo de gestión de vulnerabilidades, tales como,
quiénes se encargarían de supervisar
las distintas etapas y el ciclo completo, quiénes identificarían,
abordarían o remediarían las vulnerabilidades,
y quiénes revisarían y autorizarían la implementación de estrategias
y la entrega de reportes.
Por tanto, debería también elegir las herramientas
y servicios para la evaluación de vulnerabilidades
y, en general, para todas las etapas de la gestión de vulnerabilidades.
Finalmente,
la organización debería establecer políticas de evaluación
y tratamiento de vulnerabilidades
(p. ej., frecuencia de revisión, parámetros de aceptación temporal y absoluta,
y plazos de remediación).

Aunque no se trata de un modelo estricto,
estos suelen ser los pasos del proceso de gestión de vulnerabilidades:

### Identificación de vulnerabilidades

Esta etapa puede comenzar con el escaneo de vulnerabilidades.
Las herramientas automatizadas escanean los sistemas elegidos
por la organización para detectar si alguno o todos
los problemas de seguridad que tienen en sus bases de datos
de vulnerabilidades conocidas públicamente están presentes en esos sistemas.
Una configuración precisa de los escáneres
y sus escaneos puede ayudar a evitar la interrupción o alteración
de funciones en la tecnología evaluada
y tasas muy altas de falsos positivos.
Después, la intervención humana llega en forma
de [_pentesting_ manual](../que-es-prueba-de-penetracion-manual/),
que cuenta con la ayuda de otras herramientas.
Los llamados _pentesters_,
con una metodología orientada a la evaluación en profundidad
de los sistemas informáticos,
se esfuerzan por detectar todo aquello que se escapa
del radar de las herramientas automatizadas.
A menudo, se trata de vulnerabilidades más severas,
más complejas y previamente desconocidas (de día cero).

<caution-box>

Por cierto,
¿has visto ya nuestra prueba gratuita?
**¡Busca vulnerabilidades en tus aplicaciones de forma gratuita
con nuestras herramientas automatizadas!**
Comienza tu [prueba gratuita de 21 días](https://app.fluidattacks.com/SignUp)
y descubre los beneficios de nuestro [plan Essential](../../planes/)
[Hacking Continuo](../../servicios/hacking-continuo/).

</caution-box>

### Evaluación y priorización de vulnerabilidades

En un punto de esta etapa, los _pentesters_ o profesionales
de la seguridad buscan minimizar las tasas
de falsos positivos y falsos negativos.
Para ello, validan los informes de los escáneres y corrigen sus errores.
Es decir, eliminan de las listas de vulnerabilidades identificadas
aquellos reportes que en realidad no constituyen vulnerabilidades
e incluyen en ellas las vulnerabilidades que las herramientas deberían haber
notificado pero no lo hicieron.
(Estas son listas separadas de las entregadas por las pruebas
de penetración en la etapa anterior).

Tanto los escáneres como los _pentesters_ suelen calificar
sus hallazgos basándose en métricas bien conocidas como el CVSS.
Como hemos dicho antes, este sistema puntúa la severidad
de las vulnerabilidades de 0,0 a 10,0.
Entonces, se pueden priorizar las vulnerabilidades;
se pueden clasificar por orden de importancia según el riesgo que representan.
La ventaja de esta priorización de vulnerabilidades es que,
en el momento del reporte, la organización evaluada
puede saber qué problemas de seguridad debe tratar en primer lugar.

Uno de los factores que influyen en la evaluación
y priorización de vulnerabilidades es su explotabilidad.
En esta fase surgen preguntas como las siguientes:
¿Es explotable esta vulnerabilidad?
¿Es fácil de explotar?
¿Existe ya un _exploit_ para esta vulnerabilidad circulando por la red?
(Un _exploit_ es un código especializado
que permite aprovecharse de un problema de seguridad).
¿Qué consecuencias tendría su explotación?
Los _pentesters_ son cruciales para responder a preguntas como esta última.
A partir de su enfoque ofensivo de la seguridad, es decir,
simulando ataques del "mundo real",
estos expertos se encargan de evaluar la explotabilidad
de vulnerabilidades concretas para aportar pruebas
de sus posibles repercusiones.

### Reporte de vulnerabilidades

Tras la evaluación y validación, cada hallazgo de las herramientas
y los expertos puede notificarse a través de una herramienta
o plataforma de gestión de vulnerabilidades.
Idealmente, cada vulnerabilidad debería poder llegar a esta fase
o, mejor, completar el ciclo de forma independiente.
Lo que queremos decir es que los proveedores de soluciones
no deberían esperar tener una acumulación masiva de vulnerabilidades
antes de reportar su existencia a quienes,
en la siguiente etapa, se encargarán de su tratamiento.
A medida que los equipos reciben
un número reducido de vulnerabilidades priorizadas,
pueden decidir fácilmente qué medidas tomar.
Entre los diversos detalles sobre cada problema de seguridad identificado
y evaluado que pueden facilitarse a la organización sometida
a evaluación figuran los siguientes:
tipo, severidad, ubicación, fecha de identificación, estado actual,
descripción, evidencias de existencia y explotación,
y recomendaciones de tratamiento.

### Tratamiento de vulnerabilidades

Una vez obtenidos los detalles sobre cada vulnerabilidad,
incluidas las recomendaciones de tratamiento,
el equipo responsable de esta etapa debe pasar a actuar.
La opción de tratamiento ideal es la remediación de la vulnerabilidad,
especialmente si la vulnerabilidad implica una exposición de alto riesgo.
La remediación consiste en poner parches o reparar por completo
el problema de seguridad para evitar su explotación.
Hay que tener en cuenta que, a veces,
las sugerencias de herramientas automatizadas
o de personas novatas pueden no ser las mejores.
Por ello, la intervención de personal especializado
en ciberseguridad es una condición indispensable para un tratamiento
adecuado de las vulnerabilidades.

Otra opción de tratamiento,
principalmente en los casos en que por una razón
u otra aún no hay oportunidad de remediarla,
es la mitigación de la vulnerabilidad.
Se trata de reducir la probabilidad de explotación de la vulnerabilidad
(p. ej., aplicando una técnica que dificulte la tarea de un atacante)
y los posibles impactos.
Este puede ser un tratamiento temporal rápido mientras se desarrolla
un parche u otra forma de remediación de la vulnerabilidad.

Por último, existe la opción de la aceptación de la vulnerabilidad,
que puede ser temporal o permanente.
La elección de esta opción puede depender de las políticas internas
de la organización en cuestión.
Un problema de seguridad puede ser aceptado por esta organización
simplemente porque el riesgo que representa es muy bajo,
y los gastos de remediación son mayores que los asociados a su explotación.
En cualquier caso, suele ser aconsejable
que no haya una aceptación permanente y,
sin importar lo bajo que sea el riesgo,
conseguir remediar la vulnerabilidad en un futuro próximo.
Otra cosa es cuando lo que hay que remediar es un problema que,
por ejemplo, se encuentra en una aplicación que la organización planea
deshabilitar a muy corto plazo.
En este caso, la aceptación permanente está más justificada.

### Reevaluación y reporte final de vulnerabilidades

La remediación de vulnerabilidades de seguridad
siempre debería ser verificada para tranquilidad de los interesados.
En esta etapa se realizan reescaneos y reataques
para validar que la solución dada a cada vulnerabilidad fue efectiva,
indicando así su cierre.
Además, se recomienda realizar una reevaluación
para descubrir si han surgido nuevas vulnerabilidades
debido a los tratamientos anteriores.
Una vez realizado esto,
se puede actualizar la información de los reportes.
Por ejemplo,
el estado de seguridad de una vulnerabilidad
puede pasar de "abierta" a "cerrada".

Los informes de vulnerabilidades que se acumulan
en las plataformas de gestión de vulnerabilidades
pueden ser utilizados por los gerentes o administradores de la organización
para evaluar los esfuerzos, logros y progresos.
Una buena plataforma permite la entrega,
personalización y descarga de informes en diferentes formatos
y ajustados tanto para el personal ejecutivo como el técnico.
Cuando la plataforma contiene una amplia variedad de gráficos y cifras,
los equipos de la organización pueden supervisar adecuadamente
las tendencias en la aparición y remediación de vulnerabilidades
y la mitigación de la exposición al riesgo.
Adicionalmente, si la plataforma lo permite,
pueden estar al tanto de su cumplimiento con múltiples requisitos
de estándares internacionales de seguridad.

## ¿Por qué es importante la gestión de vulnerabilidades?

Otra forma de expresar la pregunta anterior podría ser:
**¿Por qué necesitamos la gestión de vulnerabilidades?**
La importancia de la gestión de vulnerabilidades
para una organización radica esencialmente en lo que hemos mencionado antes:
Este es un procedimiento que nos permite mantener el control de un problema.
Aquellas vulnerabilidades,
producto de nuestra ignorancia o descuido,
las cuales los actores de amenazas buscan explotar,
son las que pretendemos detectar y cerrar lo antes posible
o evitar que salgan a la luz pública e incluso que tomen forma.
Traduzcamos la importancia de un ciclo de vida de gestión de vulnerabilidades
completo y continuo en beneficios específicos:

### Beneficios de la gestión de vulnerabilidades

- Identificar, priorizar y remediar vulnerabilidades de seguridad
  antes de que los ciberdelincuentes las exploten.
- Reducir o evitar gastos de tiempo, esfuerzo y dinero,
  especialmente cuando las estrategias de remediación de vulnerabilidades
  se implementan al principio del ciclo de vida de desarrollo de _software_.
- Optimizar la postura de ciberseguridad de la organización,
  teniendo claro cuáles son los activos más valiosos
  y qué tipos de vulnerabilidades deberían evitarse a toda costa debido
  a la exposición al riesgo que representan.
- Mantener una evaluación de los logros y avances dentro de la organización
  en relación con la gestión de vulnerabilidades y riesgos asociados
  y la postura general de ciberseguridad.
- Cumplir plenamente con requisitos de estándares
  internacionales de seguridad y las propias políticas
  de seguridad de la organización.
- Disponer de roles bien definidos y planes de respuesta inmediatos
  y eficaces para cuando surjan vulnerabilidades de severidad alta
  o crítica de manera inesperada.
- Generar confianza en los clientes desde una posición de transparencia
  con la divulgación oportuna de los problemas de seguridad
  y sus soluciones correspondientes.

## Mantente a la vanguardia con el monitoreo continuo de vulnerabilidades

¿Estás interesado en implementar un proceso de gestión de vulnerabilidades?
En Fluid Attacks,
ofrecemos una solución de [gestión de vulnerabilidades](../../soluciones/gestion-vulnerabilidades/)
dentro de nuestro [servicio Hacking Continuo](../../servicios/hacking-continuo/),
la cual podemos integrar en tu ciclo de vida de desarrollo
de _software_ desde el principio.
Conectamos todas las etapas del proceso de gestión de vulnerabilidades
descrito anteriormente desde un único lugar,
es decir, la [plataforma](https://app.fluidattacks.com/) de Fluid Attacks.
Desarrollamos nuestras propias herramientas automatizadas
o escáneres de seguridad, los cuales aplican metodologías
como [SAST](../../producto/sast/),
[DAST](../../producto/dast/)
y [SCA](../../producto/sca/)
para la evaluación continua de vulnerabilidades en tus sistemas.
También contamos con un amplio grupo de expertos en pruebas de penetración que,
con sus habilidades como _hackers_
([consulta aquí](../../../certifications/)
sus múltiples certificaciones),
verifican los hallazgos de los escáneres
e identifican aquellas vulnerabilidades
que estas máquinas no han podido detectar.
Su objetivo aquí es minimizar las tasas
de falsos positivos y falsos negativos.
Cabe destacar que utilizamos la inteligencia artificial para priorizar
la evaluación de aquellas partes de los sistemas
que tienen más probabilidades de contener problemas de seguridad.

Para la calificación de vulnerabilidades, vamos más allá del CVSS,
añadiendo una versión modificada por nosotros: "CVSSF".
Con esta métrica, como puedes conocer en detalle
[en otro artículo del blog](../../../blog/cvssf-risk-exposure-metric/),
superamos varios inconvenientes,
como el que nos hace creer erróneamente
que una vulnerabilidad de severidad 10,0 supone el mismo riesgo
que diez vulnerabilidades de severidad 1,0.
En nuestra plataforma, te reportamos continuamente
las vulnerabilidades a medida que las descubrimos
y evaluamos en las continuas pruebas de seguridad.
Además de proporcionarte muchos detalles sobre cada hallazgo,
incluimos pruebas tangibles de las explotaciones de vulnerabilidades
llevadas a cabo por nuestro [_red team_](../../soluciones/red-team/)
y recomendaciones para su tratamiento.
Asimismo, desde la plataforma,
puedes asignar actividades de remediación a los miembros de tu equipo.
Después de que apliquen las medidas correctivas necesarias
(para las que nuestros expertos pueden asesorarles),
puedes pedirnos tantas veces como quieras que reevaluemos
o volvamos a atacar las vulnerabilidades
para verificar que se cerraron efectivamente.

Utilizando nuestra plataforma,
puedes elegir políticas específicas de tratamiento de vulnerabilidades,
determinar qué requisitos de seguridad cumplir
(que cubren más de 60 estándares de seguridad) y asegurar su cumplimiento.
Adicionalmente, puedes hacer un seguimiento de los tiempos
de remediación de vulnerabilidades y de los cambios en la exposición
al riesgo dentro de tu organización.
Por supuesto, puedes personalizar los informes
y descargarlos en diferentes formatos.
Puedes disfrutar de esto y mucho más dentro de un servicio que,
desde una perspectiva preventiva, busca ayudarte a
**mejorar continuamente la seguridad de tu organización**.
Estando un paso por delante de los ciberdelincuentes,
tu organización puede evitar ser víctima de ciberataques
y ver seriamente afectados sus activos,
operaciones e incluso su reputación.

¿Te interesa nuestro servicio?
No dudes en [contactarnos](../../contactanos/).
