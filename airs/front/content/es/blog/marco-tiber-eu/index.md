---
slug: blog/marco-tiber-eu/
title: ¿Has oído hablar del TIBER-EU?
date: 2022-05-09
subtitle: Una iniciativa ejemplar para evaluar y proteger sistemas
category: filosofía
tags: ciberseguridad, red-team, blue-team, pruebas de seguridad, empresa
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1652118259/blog/tiber-eu-framework/cover_tiber_eu_framework.webp
alt: Foto por Immo Wegmann en Unsplash
description: En este artículo, aprenderás sobre el TIBER-EU, una iniciativa del Banco Central Europeo que evalúa la ciberresiliencia de las entidades europeas.
keywords: TIBER EU, Marco, Red Team, Pruebas Red Team, Escenarios Red Team, Inteligencia De Amenazas, Ciberresiliencia, Hacking Etico, Pentesting
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/Ym2mFGr4GWI
---

[En 1988](https://www.ecb.europa.eu/ecb/history/html/index.es.html),
algunos países europeos decidieron crear una unión económica
que permitiera la libre circulación de capitales entre ellos
y tuviera una autoridad compartida y una política monetaria única.
Años más tarde, definieron y adoptaron una moneda común,
el euro, que surgió junto con
la [eurozona](https://es.wikipedia.org/wiki/Eurozona),
que ahora incluye a 19 de los 27 países miembros de la Unión Europea (UE).
Hoy, la institución que rige esa moneda es el Banco Central Europeo (BCE).
Es sobre una iniciativa relacionada con la ciberseguridad
dentro de esta organización de lo que hablaremos en este artículo del blog.

[Además de](https://www.ecb.europa.eu/ecb/html/index.es.html)
ayudar a mantener estables los precios en la eurozona,
el BCE realiza importantes esfuerzos para contribuir
a la seguridad de la banca europea
[Desde 2014](https://www.ecb.europa.eu/ecb/educational/explainers/tell-me-more/html/anniversary.es.html),
supervisa la solidez y resistencia de los bancos de la zona,
exigiéndoles que realicen ajustes siempre que aparezca alguna irregularidad.
En el contexto digital,
el BCE busca firmemente proteger el dinero de los usuarios frente
a las ciberamenazas y actúa de forma preventiva con la comunidad financiera.
En concreto,
ponen a prueba la seguridad y la ciberresiliencia de sus entidades.
[Como ellos dicen](https://www.ecb.europa.eu/paym/cyber-resilience/html/index.en.html),
la ciberresiliencia se refiere a la capacidad de proteger
los datos y sistemas electrónicos de los ciberataques,
así como de reanudar rápidamente las operaciones comerciales
en caso de que un ataque tenga éxito.
Esta ciberresiliencia la ponen a prueba con la ayuda
de *pentesters* o [*hackers* éticos](../que-es-hacking-etico/),
siguiendo procedimientos que se basan en el [TIBER-EU](https://www.ecb.europa.eu/paym/cyber-resilience/tiber-eu/html/index.en.html).

## ¿Qué es TIBER-EU y cómo funciona?

El TIBER-EU (Threat Intelligence-based Ethical Red Teaming)
es un marco común desarrollado
por los bancos centrales nacionales de la UE y el BCE, publicado en 2018.
Este, orienta a las autoridades, entidades
y proveedores de inteligencia de amenazas
(TI por sus iniciales en inglés)
y [*red teaming*](../../soluciones/red-team/) (RT)
en ciberataques controlados y en la mejora
de la ciberresiliencia de las entidades.
Siguiendo el enfoque de [un *red team*](../ejercicio-red-teaming/),
como el de Fluid Attacks, sus pruebas buscan imitar las tácticas,
técnicas y procedimientos de los atacantes maliciosos.
Pretenden [simular ataques reales](../../../blog/what-is-breach-attack-simulation/)
a los sistemas de sus entidades,
especialmente a sus operaciones críticas,
para determinar sus debilidades y fortalezas e impulsar así
el crecimiento de su nivel de madurez en ciberseguridad.
(Esto nos recuerda
al [SAMM de OWASP](https://fluidattacks.docsend.com/view/4k524b3gviwqubri),
que puedes emplear, por ejemplo,
para evaluar la madurez de
tus [pruebas de seguridad](../../soluciones/pruebas-seguridad/).)

En las pruebas TIBER-EU participan varios equipos.
Del lado de la entidad (generalmente del sector financiero)
que va a ser evaluada están los equipos azul y blanco.
El primero es el que desconoce que va a ser objeto
de ataques simulados destinados a evaluar sus capacidades
de prevención, detección y respuesta.
El segundo es un grupo reducido de personas que conoce acerca del procedimiento
y contribuye a su ejecución. Por otro lado,
están los proveedores de TI y RT.
La primera empresa analiza el espectro de amenazas potenciales
y realiza un reconocimiento de la entidad.
La segunda empresa se encarga
del [*hacking* ético](../../soluciones/hacking-etico/)
o ataques deliberados contra los sistemas de la entidad
y sus operaciones críticas.
Por último, está el equipo cibernético TIBER.
Este grupo pertenece a la autoridad y se encarga de la supervisión
de la prueba para garantizar el cumplimiento de los requisitos del marco.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/red-team/"
title="Inicia ahora con la solución de red teaming de Fluid Attacks"
/>
</div>

El TIBER-EU también gestiona los requisitos para los proveedores de TI y RT.
La entidad a ser examinada debe comprobar que esos requisitos
se cumplen antes de trabajar con aquellas empresas.
Se trata de [estándares de selección](https://www.ecb.europa.eu/pub/pdf/other/ecb.1808tiber_eu_framework.en.pdf)
de los que hablaremos en un próximo artículo.
De momento, conozcamos un poco
el [procedimiento de las pruebas](https://www.ecb.europa.eu/pub/pdf/other/ecb.tiber_eu_framework.en.pdf).
Las autoridades de los países europeos,
en acuerdo con las entidades bajo su responsabilidad,
determinan en qué casos y cuándo llevarlas a cabo.
Para ser reconocido como prueba TIBER-EU,
este proceso debe ser realizado por proveedores externos independientes
y no por los equipos internos de las entidades.
El marco estipula que esta prueba debe dividirse en tres fases:
preparación, prueba y cierre.

<div class="imgblock">

![TIBER-EU Process](https://res.cloudinary.com/fluid-attacks/image/upload/v1652118200/blog/tiber-eu-framework/tiber_eu_process.webp)

<div class="title">

Imagen tomada de [ecb.europa.eu](https://www.ecb.europa.eu/pub/pdf/other/ecb.tiber_eu_framework.en.pdf).

</div>

</div>

Antes de la fase de preparación, TIBER-EU ofrece una fase opcional:
el **panorama genérico de amenazas**.
Este paso consiste en realizar una evaluación genérica
del panorama de amenazas del sector financiero nacional.
Esto implica mapear el rol de la entidad e identificar
a los actuales actores de amenazas de alto nivel
para el sector junto con sus métodos contra este tipo de entidades.
En la **fase de preparación**,
se definen los equipos responsables
de la prueba junto con el alcance de la misma.
La autoridad valida lo anterior
y la entidad contrata a los proveedores de TI y RT.

En la **fase de prueba**,
la empresa de TI elabora un
"Informe de inteligencia sobre amenazas específicas",
presentando escenarios de amenazas e información relevante sobre la entidad.
(El panorama genérico de amenazas de la fase opcional
serviría de base para esta etapa).
La empresa de RT utiliza todo esto para desarrollar escenarios de ataque
y ejecutar ataques controlados contra
determinados sistemas críticos de producción,
personas y procesos que sustentan las funciones críticas de la entidad.

En la **fase de cierre**,
el proveedor de RT ofrece un "informe de prueba del *red team*"
con detalles de los métodos empleados,
así como las hallazgos y evidencias de la prueba.
En función de cada caso,
este informe puede incluir recomendaciones
para que la entidad sometida a prueba mejore en áreas como:
políticas, operaciones, controles o consciencia.
Las personas interesadas revisan y debaten la prueba
y los problemas descubiertos.
Después, la entidad,
que recibe pruebas técnicas detalladas sobre sus puntos débiles
o vulnerabilidades, acuerda y completa un "Plan de remediación".

Para todos los implicados,
debe quedar claro que las pruebas TIBER-EU conllevan riesgos.
Por ejemplo, las pruebas pueden dar lugar a pérdida,
alteración y divulgación de datos,
caída y daños al sistema y casos de denegación de servicio.
Por eso el marco TIBER-EU es estricto
y da prioridad al establecimiento de controles rigurosos
de gestión de riesgos a ser empleados a lo largo de todo el proceso.
El marco establece que,
para que las pruebas sean seguras, deben definirse
y comprenderse adecuadamente los roles
y responsabilidades de todas las partes interesadas.
Además, de acuerdo con lo que hemos mencionado antes,
y de lo que hablaremos más adelante, los proveedores de TI
y RT deben cumplir requisitos específicos.
Y es que se espera garantizar que solo el personal mejor cualificado
realice pruebas tan delicadas en funciones críticas.

Algo fundamental que hace esta sofisticada
y robusta iniciativa es contribuir a proporcionar un nivel
de garantía adecuado de que los activos
y sistemas clave de los servicios financieros están protegidos
contra ataques de agresores técnicamente competentes,
dotados de recursos y de carácter persistente.
Las autoridades europeas confían en las metodologías
de los proveedores de TI y RT para evaluar la seguridad
de sus entidades y reducir los riesgos.
¿Por qué será que tantas organizaciones siguen enfrascadas
en confiar únicamente en herramientas de escaneo automatizadas
y poco precisas?
Ya lo hemos dicho antes:
Para ir un paso adelante de los *hackers* maliciosos,
necesitas a alguien que piense como ellos.
Necesitas *hackers* éticos.
¿Te gustaría contar con la ayuda de los *hackers* éticos de Fluid Attacks?
[¡Contáctanos!](../../contactanos/)
