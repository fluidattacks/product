---
slug: blog/tribu-de-hackers-4/
title: Tribu de hackers red team 4.0
date: 2021-01-08
subtitle: Aprendiendo del experto en red teams 'Tinker Secor'
category: opiniones
tags: ciberseguridad, red-team, hacking, pentesting, blue-team
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1620331134/blog/tribe-of-hackers-4/cover_ip5dcd.webp
alt: Foto por Chase Moyer en Unsplash
description: Este artículo se basa en el libro "Tribe of Hackers Red Team" de Carey y Jin. Aquí compartimos contenido de la entrevista con Tinker Secor.
keywords: Red Team, Seguridad Red Team, Hacking Red Team, Pentesting, Hacking Etico, Blue Team, Conocimiento, Tribu De Hackers
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/ceLRoyy1p9Y
---

**47** es el número de expertos en [*red teaming*](../ejercicio-red-teaming/)
que podemos encontrar en el libro
[*Tribe of Hackers Red Team*](https://www.amazon.com/Tribe-Hackers-Red-Team-Cybersecurity/dp/1119643325)
escrito por Carey y Jin (2019).
Y solo hemos publicado tres artículos al respecto,
cada uno dedicado a un experto en el siguiente orden:
[(1.0) Carey](../tribu-de-hackers-1/),
[(2.0) Donnelly](../tribu-de-hackers-2/),
and [(3.0) Weidman](../tribu-de-hackers-3/).
Entonces,
¿por qué no hacer espacio para un cuarto artículo?
¿O esto se empieza a parecer
a [Rápido y Furioso](https://www.imdb.com/title/tt0232500/?ref_=fn_al_tt_1)?
¡Es broma!

<div class="imgblock">

![FF50](https://res.cloudinary.com/fluid-attacks/image/upload/v1620331132/blog/tribe-of-hackers-4/ff50_cghkzx.webp)

<div class="title">

Imagen tomada de [aquí](https://images-cdn.9gag.com/photo/ap580RB_700b.jpg).

</div>

</div>

Aquí quiero mostrarte otro punto de vista
sobre el *red teaming*
(otro experto respondiendo a las mismas preguntas),
con las correspondientes recomendaciones para cualquiera, eso es todo.
El artículo anterior mostraba lo que podríamos considerar,
para asombro de algunos, un caso "extraño".
Es decir, presentaba las ideas y consejos de una mujer relacionados
con el [*hacking* ético](../que-es-hacking-etico/),
y eso es raro porque, por desgracia,
en la actualidad no es habitual ver a muchas mujeres ejerciendo esta profesión.

Ahora, sería interesante leer (por qué no aprender)
las opiniones y recomendaciones de otro caso "curioso".
En esta ocasión,
una persona que no aparece en el libro en cuestión con su nombre “real”.
Y, al contrario que la mayoría de los expertos entrevistados,
una persona que no muestra foto en su sección.
Sí, al parecer,
es un hombre y utiliza el alias de
"[Tinker Secor](https://twitter.com/tinkersec?lang=en)."

Veamos qué podemos obtener de este sujeto
que sirvió en el Cuerpo de Marines de EE. UU.,
ha trabajado como analista de detección de intrusos
y ahora es un *pentester* de alcance total con experiencia en evaluar
y eludir la seguridad de ambientes lógicos, físicos y sociales.

## Para los que buscan ser diligentes en los *red teams*

Tinker fue reclutado y entrenado
para convertirse en *red analyst* tras adquirir cierta experiencia
con *blue teams* y algo de reputación,
sobre todo dando charlas acerca de operaciones
de defensa en el Cuerpo de Marines.
Pero ya sabemos que no es necesario haber pasado por un equipo azul
para pertenecer a uno rojo.
De hecho, como dice acertadamente Tinker
—cuando se le pregunta por la mejor forma
de conseguir un trabajo en un *red team*—,
es igual que para conseguir cualquier trabajo;
divides tu tiempo entre la adquisición de las habilidades necesarias
y la creación de contactos. ¡Ya está!

¿Qué te recomienda Tinker para desarrollar tus habilidades?
En primer lugar, estudiar lo siguiente:
sistemas, redes, entornos virtuales y nube,
aplicaciones robustas/web/móviles, secuencias de comandos,
ambientes físicos, intercambios sociales, y ataques y defensas básicos.
Muchas cosas que asimilar, ¿no?
Bueno,
esto es lo que propone sobre la práctica:
Participar en retos de *scripting*,
construir un laboratorio virtual dentro de tu portátil
o computador barato e instalar sistemas
y conectarlos entre sí a través de redes,
y hacer ejercicios de capturar la bandera
(CTFs, por su nombre en inglés) en línea o en conferencias.

Conferencias y encuentros;
ese es el tipo de eventos a los que Tinker sugiere asistir
para establecer una red de contactos y buscar trabajo
(más allá de la típica pero nada insignificante aplicación por internet).
Incluso recomienda ser voluntario en este tipo de eventos y, si es posible,
organizar algunos de ellos.
Por supuesto,
¡no olvides unirte a algunos grupos de buena reputación en línea!

<div class="imgblock">

![Old Trope](https://res.cloudinary.com/fluid-attacks/image/upload/v1620331130/blog/tribe-of-hackers-4/oldtrope_w2fgvh.webp)

<div class="title">

Cita de Tinker Secor.

</div>

</div>

## Para los que ya sudan sangre en los *red teams*

Tinker reduce el *red teaming* a “control de calidad".
Tan sencillo como eso. Por lo tanto,
cuando pretendas ofrecer tus servicios a algunos clientes reacios
o sin conocimientos técnicos (que reflejen que no necesitan seguridad),
utiliza alguna evaluación como demostración,
y hazles saber que el *red teaming*
es realmente necesario hoy en día si quieren garantizar la calidad
de sus sistemas no solo para ellos,
sino también para sus clientes o usuarios.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/red-team/"
title="Inicia ahora con la solución de red teaming de Fluid Attacks"
/>
</div>

Un *red team* bien establecido debería poseer
un entendimiento clarísimo de las habilidades particulares
de cada uno de sus miembros.
Como dice Tinker,
es habitual ver en estos grupos a personas
que pueden hacer un poco de todo.
Sin embargo, sobre todo en los grandes proyectos,
los líderes podrían delegar tareas de acuerdo
con las habilidades especiales de los miembros del equipo
y reunirlos para discutir sus actividades de evaluación
y reporte en determinados momentos.

En cuanto a lo que el cliente tiene que saber después
de que el *red team* obtenga resultados en un programa de evaluación,
Tinker sugiere que lo más importante es repasar la metodología del ataque
y mostrar lo que funcionó y lo que no funcionó.
La idea es que el cliente conozca los detalles
de la trayectoria seguida por cada analista junto
con los procedimientos realizados.
Por otra parte,
además de informar sobre las vulnerabilidades,
Tinker recomienda la entrega
de información relacionada con los hallazgos positivos.
Estos incluirían los mecanismos de seguridad que evitaron ataques específicos,
así como los momentos en los que el *blue team* detectó,
respondió a, y contuvo los ataques".

## Para empresas que aspiran estar a la vanguardia en seguridad

Las evaluaciones de garantía de calidad de la seguridad
y las pruebas de penetración pueden
y deberían realizarse en todas las etapas
de un modelo de madurez de seguridad.
Esa es la respuesta que da Tinker a la pregunta
de cuándo introducir un *red team* en el programa
de seguridad de una organización, para que la tengas en cuenta.
(No deberías olvidar el término [DevSecOps](../concepto-devsecops/).)
Después,
si es posible para tu empresa, siguiendo el consejo de Tinker,
sería excelente contar con una persona dedicada
o un equipo que realice continuamente [*hacking* ético](../../soluciones/hacking-etico/)
en tus sistemas.
(¿Has oído hablar de nuestro servicio principal
de [*hacking* continuo](../../servicios/hacking-continuo/)?)

Adicionalmente, Tinker cree que, por el bien de tu compañía,
no deberías emplear únicamente escáneres de vulnerabilidades.
Es mejor mezclar con ellos
las [pruebas de penetración](../../soluciones/pruebas-penetracion-servicio/)
Como él dice,
los dos cubren diferentes áreas
y tienen diferentes puntos fuertes y aplicaciones,
y las empresas deberían emplear ambos.
Según él,
es típico ver cómo las organizaciones implementan
herramientas de escaneo de vulnerabilidades de los mejores proveedores
y las utilizan para detectar problemas de seguridad,
para luego, al cabo de varios meses, remediar solo algunos de ellos,
ignorando por lo general los hallazgos de severidades media y baja.
No existe la creación de programas de gestión
y reparación de vulnerabilidades,
y en los análisis posteriores aparecen listas de vulnerabilidades más extensas,
sobre las que, de nuevo, no se toman las medidas adecuadas.
(Ahora, repito la pregunta para aquellas empresas
que han caído en ese error pero lo reconocen como tal:
¿Han oído hablar de nuestro servicio principal
de [*hacking* continuo](../../servicios/hacking-continuo/)?)

## ¡Eso es todo, amigos!

Por supuesto,
puedes acceder a la información completa de la entrevista en el libro de
[Carey y Jin](https://www.amazon.com/Tribe-Hackers-Red-Team-Cybersecurity/dp/1119643325).
Aquí he compartido solo algunos aspectos destacados
de las respuestas dadas por Tinker Secor,
uno de los **47** expertos en *red teaming* que puedes encontrar allí.
Por otro lado,
si quieres formar parte del equipo de Fluid Attacks,
puedes consultar nuestra página de [Trabaja con nosotros](../../../careers/)
y si necesitas información sobre nuestros servicios
y [soluciones](../../soluciones/) para tu empresa,
[haz clic aquí para contactarnos](../../contactanos/).
