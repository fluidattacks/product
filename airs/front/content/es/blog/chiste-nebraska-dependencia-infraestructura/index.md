---
slug: blog/chiste-nebraska-dependencia-infraestructura/
title: El chiste sobre alguien en Nebraska
date: 2024-03-22
subtitle: Un problema de infraestructura digital que aún muchos ignoran
category: opiniones
tags: software, código, riesgo, vulnerabilidad, ciberseguridad
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1711073708/blog/nebraska-joke-infrastructure-dependency/cover_nebraska_joke_infrastructure_dependency.webp
alt: Foto por Valery Fedotov en Unsplash
description: El chiste aquí mencionado es solo una pequeña muestra de un gran problema de dependencia en la infraestructura digital mundial, sobre el que necesitamos crear conciencia.
keywords: Chiste Nebraska, Dependencia, Infraestructura Digital, Software De Codigo Abierto, Componentes De Software, Xkcd, Sbom, Pentesting, Hacking Etico
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/brown-wooden-blocks-on-black-table-CqX6IhVj2TI
---

Alguna vez leí o escuché en alguna parte algo así como que,
en el mundo digital,
hay muchos problemas fundamentales de estructura y seguridad
a los que no prestamos suficiente atención.
Fue precisamente esta afirmación una de las que hizo ignición
en los recovecos de mi memoria
cuando vi por primera vez el chiste sobre alguien en Nebraska,
en torno al cual gira este blog *post*:

<div class="imgblock">

![Dependency](https://res.cloudinary.com/fluid-attacks/image/upload/v1711074047/blog/nebraska-joke-infrastructure-dependency/dependency-xkcd.webp)

<div class="title">

"Dependency." [Cómic #2347](https://xkcd.com/2347/) en xkcd.

</div>

</div>

## ¿Es un chiste o una alerta? —Ambos

Este chiste forma parte de xkcd,
un webcómic creado por el caricaturista,
ingeniero y escritor estadounidense Randall Munroe.
Lo que el autor intenta mostrar aquí es la **dependencia entre los elementos**
de un sistema o arquitectura digital global
representado como una torre de bloques,
como un Jenga,
los cuales simbolizan los componentes de *software*.
Así,
a medida que ascendemos desde la base de la torre,
los componentes de niveles superiores dependen de más y más
componentes de niveles inferiores.

Curiosamente,
en el tercer nivel de la torre,
de abajo hacia arriba,
encontramos un delgado componente que,
fácilmente podríamos suponer,
si se rompe, o se quita,
significaría la desestabilización y la caída de todo el sistema,
o al menos lo que hay de ahí para arriba
(es decir, "toda la infraestructura digital moderna").
Lo más peculiar y gracioso,
pero al mismo tiempo preocupante,
es que,
además de tratarse de un componente delgado,
Randall se refiere a él como un proyecto mantenido por una persona cualquiera
en un sitio concreto
desde una fecha específica.
Pero, ¿esto qué quiere decir?

En realidad,
no es que nuestros sistemas dependan de lo que hace un individuo específico
por allá en Nebraska.
A lo que este chiste —al mismo tiempo alarma— parece apuntar
es a dar un ejemplo de **una realidad general**
que venimos experimentando desde hace muchos años:
diversos programas o componentes de *software*,
algunos de ellos pequeños,
mantenidos principalmente por motivos personales
por una o pocas personas no famosas o desconocidas,
en cualquier parte del mundo,
soportan gran parte de nuestra infraestructura digital actual.

Si bien la caricatura de Randall representa la realidad mencionada,
cuando revisamos su *caption*,
podemos leer algo como lo siguiente:
Algún día ImageMagick se romperá definitivamente
y tendremos un largo período de lucha
mientras intentamos recomponer la civilización a partir de los escombros.
Este es un ejemplo concreto,
quizá exagerado pero ilustrativo.
**ImageMagick** es un *software* libre y de código abierto
para procesar, crear, mostrar, editar y convertir imágenes rasterizadas,
[creado por allá en 1987](https://en.wikipedia.org/wiki/ImageMagick)
por John Cristy,
el cual ha sido y es bastante usado en diversas aplicaciones,
[especialmente](https://www.jstage.jst.go.jp/article/abas/21/5/21_0220914a/_pdf)
las destinadas a servicios web,
y al que han contribuido a lo largo del tiempo
comunidades de ingenieros y desarrolladores.

Aunque muchas otras librerías y APIs pueden llevar a cabo
las tareas de ImageMagick,
esta parece haberse convertido en el paquete por defecto.
El problema con esta librería es que,
[por allá en 2016](https://ine.com/blog/imagetragick-a-tragick-image-conversion-tale),
se descubrieron cinco vulnerabilidades en ella
(lo cual terminó llevando el nombre de "[ImageTragick](https://imagetragick.com/)"),
[una de las cuales](https://blog.sucuri.net/2016/05/imagemagick-remote-command-execution-vulnerability.html),
al ser explotada,
permitiría la ejecución remota de código
(RCE, sigla en inglés para *remote code execution*).
**Este hallazgo prendió las alarmas**
entre ingenieros y desarrolladores,
significando un alto riesgo potencial
ya que si ImageMagick se estropeaba,
multitud de programas que dependían de ella fallarían,
al menos temporalmente,
mostrando un desequilibrio y colapso en "la torre."

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/gestion-vulnerabilidades/"
title="Inicia ahora con la solución de gestión de vulnerabilidades
de Fluid Attacks"
/>
</div>

## Profundizando en el problema

Hoy en día,
muchos de nosotros sabemos,
aunque seguramente muchos más lo ignoran,
que tanto las más pequeñas como las más grandes compañías y organizaciones
de todo el mundo
emplean componentes de *software* de código abierto de terceros.
**Casi todo el *software* existente depende de este tipo de productos**
desarrollados y mantenidos por individuos y comunidades de desarrolladores.
Pero, ¿por qué?

Una de las razones principales es el ya típico proverbio:
"**No reinventes la rueda**."
Es decir,
si esa obra humana ya resultó suficientemente funcional,
no pierdas el tiempo y dedícate a construir algo nuevo.
En el universo del *software*,
son muchas las personas que crean y comparten código públicamente
y de forma gratuita,
el cual cumple funciones específicas,
algunas de ellas muy básicas.
Así que,
los demás no tienen que construir sus aplicaciones desde cero;
no necesitan desarrollar todos los componentes de sus productos.
Simplemente recurren a lo que ya ha sido creado y compartido por la comunidad,
lo que les permite un desarrollo tecnológico más barato,
sencillo y eficiente.

<div class="imgblock">

![Reinvent the Wheel](https://res.cloudinary.com/fluid-attacks/image/upload/v1711074103/blog/nebraska-joke-infrastructure-dependency/reinvent-the-wheel-xkcd.webp)

<div class="title">

"Reinvent the Wheel." [Cómic #2140](https://xkcd.com/2140/) en xkcd.

</div>

</div>

En otras palabras,
aquí estamos hablando de **reutilización** y **modularidad**,
principios que se vienen utilizando desde hace mucho tiempo
pero que hoy son fundamentales en la ingeniería de *software*.
Así,
cuando un grupo de desarrolladores se propone crear una aplicación,
lo que termina construyendo es una cadena o árbol de dependencias
con múltiples componentes de *software* de código abierto
previamente desarrollados por separado por otros ingenieros,
algunos de ellos de unas pocas líneas de código
que cumplen funciones específicas.

El problema,
como ilustró Randall,
es que muchos de estos proyectos
de componentes de *software* de código abierto
fundamentales para proyectos a gran escala
suelen ser mantenidos por muy pocas personas.
A diferencia de grandes empresas como Linux,
por ejemplo,
muchos **otros proyectos no reciben suficiente atención**.
Quienes los conservan suelen ser expertos en ellos
que a menudo trabajan demasiado
y por poco o ningún dinero,
principalmente como *hobby*,
y sobre quienes recae toda la responsabilidad.
Sin embargo,
por motivos como el cansancio o la búsqueda de mejores oportunidades,
algunos de estos proyectos pueden ser abandonados.
Este inconveniente,
unido a las vulnerabilidades de seguridad que puedan estar allí presentes,
suele suponer un riesgo considerable para la integridad
y seguridad de "todo el sistema".

### Heartbleed y left-pad

Además del caso de ImageMagick,
otros casos ilustrativos de este preocupante problema han sido,
por ejemplo,
los de Heartbleed y left-pad.
**[Heartbleed](../../../blog/my-heart-bleeds/)** es un *bug*
descubierto en 2014 en OpenSSL,
una librería que permite comunicaciones cifradas a través de la Internet,
por ejemplo,
para el comercio electrónico.
Este,
a diferencia de otros,
es un componente de *software* relativamente grande
ya que,
por aquel entonces,
contaba con alrededor de 500 mil líneas de código.
Según se informa en [un sencillo sitio web](https://heartbleed.com/)
dedicado a esta vulnerabilidad,
Heartbleed permitía robar la información protegida,
en condiciones normales,
por el cifrado SSL/TLS utilizado para asegurar la Internet.

Al parecer,
este caso se calificó de irresponsabilidad
por parte del equipo de OpenSSL,
ya que conocían previamente un problema latente
al que no prestaron suficiente atención.
Tardaron cerca de dos años en detectar Heartbleed.
Lo peor es que,
aparentemente,
el mantenimiento del componente afectado dependía solo de dos sujetos,
ambos voluntarios y con exceso de trabajo.
[Se dice que](https://www.jstage.jst.go.jp/article/abas/21/5/21_0220914a/_pdf),
a pesar de lo ocurrido,
las donaciones para este proyecto crecieron muy poco,
pasando de unos 2.000 a 9.000 dólares al año.
(Esta falta de financiación es algo frecuente
en la historia del desarrollo de *software* de código abierto
y algo que queremos destacar en una futura entrada del blog).

<div class="imgblock">

![Heartbleed](https://res.cloudinary.com/fluid-attacks/image/upload/v1711074309/blog/nebraska-joke-infrastructure-dependency/heartbleed-xkcd.webp)

<div class="title">

"Heartbleed." [Cómic #1353](https://xkcd.com/1353/) en xkcd.

</div>

</div>

Para un caso similar más reciente,
[véase el de Log4j](../../../blog/log4shell/),
una librería de código abierto de Apache
en la que se descubrió una vulnerabilidad
que permitía a atacantes remotos
ejecutar código arbitrario en los sistemas expuestos.

En lo que respecta a **[left-pad](https://qz.com/646467/how-one-programmer-broke-the-internet-by-deleting-a-tiny-piece-of-code)**,
resulta que un joven llamado Azer Koçulu había estado desarrollando código
para npm,
un reconocido y ampliamente usado servicio de desarrollo web
para encontrar e instalar paquetes de *software* de código abierto
escritos en JavaScript.
Cuando npm,
en un embrollo que no merece la pena mencionar aquí,
actuó aparentemente en contra de los principios básicos
de la cultura del código abierto
en relación con un paquete en el que Koçulu había estado trabajando,
este optó por abandonarlo,
concretamente borrarlo,
allá por 2016,
lo que provocó colapso en muchos proyectos y sitios web que dependían de él.

Así,
ingenieros y otros individuos empezaron a recibir mensajes de error
al tratar de ejecutar sus aplicaciones y servicios.
Estos requerían de ese paquete llamado left-pad
que npm ya no tenía en su portafolio
y del que algunas personas ni siquiera habían escuchado antes.
Era demasiado extraño que un paquete hubiese desaparecido.
Lo más curioso era que este contaba con solamente 11 líneas de código simple
para cumplir una única función.
La ausencia de esta pequeña librería afectó a muchos otros paquetes
que dependían de ella, directa o indirectamente;
entre estos últimos, React, un componente grande
ampliamente utilizado en diferentes países del mundo,
incluso en la web de Facebook.
Ante estos estragos,
npm se vio obligado a restaurar rápidamente esas 11 líneas de código.

## ¿Cómo deberíamos lidiar con este problema?

¿Que cada quien trabaje en todos los componentes
de sus propios productos de *software*?
Eso no es viable.
Lo que parece más factible en estos momentos
es mantener intacta la cultura del código abierto,
pero **contribuir en mayor medida a su mantenimiento y seguridad**.
Como ya han dicho muchos,
¿por qué las grandes empresas del sistema capitalista
que dependen de estos proyectos públicos y gratuitos
no contribuyen a su financiación o de alguna otra forma?
¿Dónde está el apoyo institucional y gubernamental?
Todo esto sigue siendo insuficiente.

Aparte de ir creando conciencia sobre esta situación,
intentando minimizar la ignorancia al respecto,
y de buscar financiamiento para proyectos de los que todos dependemos,
muchas compañías de diferentes sectores industriales
también podemos contribuir a la seguridad de nuestra infraestructura digital.
¡Ojo!
**El hecho de que no tengamos que reinventar la rueda
no significa que debamos pasarlo por alto**.
¿No podemos acaso aprender sobre ella?
Si bien no tenemos que desarrollar
los componentes de *software* de terceros que utilizamos en nuestros productos,
sí deberíamos conocerlos, revisarlos, actualizarlos, mejorarlos.

Aquí es donde entra en juego
la tan mencionada [lista de materiales de *software*](../../learn/que-es-sbom/)
(SBOM, sigla en inglés para *software bill of materials*).
Utilízala como un inventario organizado
de todos los componentes de terceros utilizados por tu aplicación.
A partir de ahí,
descarta aquellos paquetes que sean innecesarios,
solicita una evaluación de vulnerabilidades de seguridad sobre el resto y,
si es posible,
**contribuye con revisión**, **código o fondos**,
especialmente a aquellos componentes de *software*
que desempeñen un papel crítico para mantener viva tu compañía.

<quote-box>

El estado actual de nuestra infraestructura digital
es uno de los problemas peor comprendidos de nuestro tiempo.
Es fundamental que lo entendamos.
—[Nadia Eghbal](https://www.fordfoundation.org/wp-content/uploads/2016/07/roads-and-bridges-the-unseen-labor-behind-our-digital-infrastructure.pdf)

</quote-box>

Si deseas saber cómo Fluid Attacks puede ayudarte con el SBOM,
el [análisis de composición de *software*](../escaneos-sca/)
([SCA](../../producto/sca/))
y más pruebas de seguridad,
no dudes en [contactarnos](../../contactanos/).
