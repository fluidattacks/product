---
slug: blog/infraestructura-digital-con-poco-soporte/
title: Infraestructura con poco soporte
date: 2024-04-17
subtitle: Te necesitamos, pero no podemos darte dinero
category: opiniones
tags: software, código, riesgo, vulnerabilidad, ciberseguridad
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1713388624/blog/undersupported-digital-infrastructure/cover_undersupported_digital_infrastructure.webp
alt: Foto por Brian Kelly en Unsplash
description: Queremos persistir en la concientización sobre el escaso apoyo que reciben muchos proyectos de software de código abierto, de los que depende casi todo el mundo.
keywords: Infraestructura Digital, Software De Codigo Abierto, Proyectos De Codigo Abierto, Roads And Bridges, Componentes De Software, Poco Apoyo, Poca Financiacion, Pentesting, Hacking Etico
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/brown-wooden-blocks-on-black-table-CqX6IhVj2TI
---

La frase que constituye el subtítulo de este post
fue [alguna vez planteada](https://www.informationweek.com/it-leadership/ntp-s-fate-hinges-on-father-time-)
por el desarrollador de *software* Harlan Stenn,
presidente de la Network Time Foundation.
Con ella,
Harlan nos daba a conocer la postura habitual,
generalmente tácita,
de la gente
frente a **la penosa situación de la infraestructura digital pública**
y muchos de sus componentes de *software* de código abierto,
de los que dependemos todos de una u otra forma.

Hace unos días,
en [el *post* sobre el chiste del sujeto en Nebraska](../chiste-nebraska-dependencia-infraestructura/),
señalamos la enorme dependencia que los gigantes empresariales,
así como las pequeñas y medianas empresas,
los gobiernos nacionales y,
en general,
los usuarios de la Internet
tienen de aquellos componentes o proyectos
que en muchas ocasiones son mantenidos por unos pocos sujetos,
con escasa o nula remuneración.
Como comentábamos allí,
teníamos intención de escribir un *post* sobre este problema
de la poca financiación
—el cual hace parte de un problema más amplio: el poco soporte—
y aquí lo tienes.
Este texto se basa principalmente en el reporte *[Roads and Bridges](https://www.fordfoundation.org/work/learning/research-reports/roads-and-bridges-the-unseen-labor-behind-our-digital-infrastructure/):
The Unseen Labor Behind Our Digital Infrastructure* (2016),
escrito por Nadia Eghbal,
y surge porque trata un problema
al que aún hoy no se le ha prestado suficiente atención.

## Introducción

Sabemos que la comunidad de *software* de código abierto
normalmente trabaja desde un esquema descentralizado,
sin jerarquías
—lo irónico es que la actual economía capitalista digital
se forje y mantenga sobre muchos de sus proyectos libres.
Así,
ninguna organización o autoridad central concede permisos
o determina lo que se va a construir,
mantener y utilizar en la infraestructura.
(Casos atípicos son, por ejemplo, los de IETF y W3C
que establecen ciertos estándares y requisitos
en las piezas más fundamentales o niveles más básicos de la web.)

Desde esta comunidad,
individuos y grupos crean lenguajes de programación,
*frameworks* y librerías,
algunos más complejos que otros,
y los ponen a disposición de quien quiera o necesite usarlos.
Normalmente,
se espera que un nuevo proyecto mejore uno existente
—trayendo nuevas características o resolviendo alguno de sus problemas—
para que se considere útil
o incluso "la mejor opción disponible"
y valga la pena su adopción.
Algunos proyectos resultan ser más preferidos que otros,
y algunos simplemente terminan siendo ignorados.

Los proyectos exitosos van siendo cada vez más conocidos,
implementados y empleados por equipos de desarrolladores
y compañías tecnológicas.
Aparte de su buena funcionalidad,
estos componentes se difunden gracias a factores como la reputación
de los desarrolladores,
el atractivo de los nombres de los productos
y las campañas publicitarias realizadas.
Si bien algunos proyectos de código abierto se originan dentro de una compañía
y/o como parte de un negocio,
aquí nos centramos más que todo en aquellos que parten de individuos
o comunidades independientes.

Los consumidores,
por su parte,
toman provecho de estos bienes públicos
para resolver sus problemas específicos.
Las empresas,
por ejemplo,
recurren a componentes de *software* de código abierto
para construir y soportar sus propios productos y servicios
y con estos ganar dinero.
A medida que más y más gente demanda componentes de *software*,
a menudo mantenidos por unos pocos desarrolladores,
empiezan a aparecer desequilibrios.
Los consumidores envían constantemente solicitudes desmesuradas y escandalosas
a estas personas
sin ofrecerles retribución alguna.

## ¿Por qué generan *software* de código abierto?

Usualmente,
son sujetos voluntarios los que trabajan en el desarrollo
y mantenimiento de proyectos de código abierto.
Lo hacen como un *hobby*,
un arte que aman y les satisface,
y como una forma de resolver sus problemas.
Estos propósitos pueden estar relacionados con el deseo
de influir positivamente en la vida de los demás,
pero, en general, no hablamos de "altruismo".
Muchos desarrolladores y mantenedores de proyectos de código abierto
necesitan pensar en su futura prosperidad económica,
incluso cuando el dinero puede seguir siendo un tema tabú
en la comunidad del código abierto.

El dinero puede seguir viéndose por muchos
como una perversión de la idea original de la comunidad de código abierto,
pero seamos realistas:
Dichosos aquellos pocos que trabajan a tiempo completo como voluntarios
en estos proyectos
y no tienen necesidad de recibir un centavo.
Lo cierto es que una de las intenciones más comunes de los desarrolladores
contribuyentes
es ganarse una reputación.
Suelen tener la intención de ser reconocidos en las comunidades,
demostrar su valía y construir su portafolio de trabajo
(a veces con varias pequeñas contribuciones),
para luego tener la oportunidad de lograr compensaciones
para sostener sus proyectos
o meramente ser contratados en grandes empresas donde sean bien remunerados.

Cuando los desarrolladores se encuentran,
por una u otra razón,
atados a estos proyectos,
van sintiendo la carga pesada,
que a veces,
cuando un proyecto ha sido popular,
es como si se transformara en una "obligación ética" para con el mundo.
Dicha carga refleja injusticia,
sobre todo en la desigualdad de la retribución económica.
Los desarrolladores que mantienen proyectos de código abierto,
a menudo a tiempo completo,
de los que dependen compañías, entidades gubernamentales, y demás,
ganan nada o muy poco
en comparación con quienes forman parte de estas organizaciones.

## Problemas para los desarrolladores y mantenedores

Como comenta Nadia,
cada vez rondan por la web más y más *posts*
y conversaciones públicas
sobre el cansancio y el estrés que sufren estos voluntarios.
Muchas veces,
solo una o dos personas se encargan de la supervivencia de un proyecto.
El apoyo de otras personas suele ser casual o infrecuente.
En base a lo anteriormente mencionado,
algunos colaboradores llegan,
echan una mano durante un tiempo,
son reconocidos y luego, afortunadamente, son contratados por una empresa,
por lo que retiran su apoyo al proyecto sin pensárselo dos veces.

A veces,
los proyectos de código abierto que son antiguos
tienen problemas incluso para encontrar contribuyentes dentro de la comunidad.
La mayoría de los desarrolladores prefieren ponerse a trabajar
en proyectos nuevos y populares
que les llaman la atención
y que puedan servirles más a su propósito de ganar reputación,
y no en aquellos que son sólidos, fundamentales,
"suficientemente consolidados"
y han funcionado bien hasta el momento para los usuarios o consumidores.

Es cierto que algunos desarrolladores y mantenedores de proyectos
de código abierto
logran ingresos por consultoría y apoyo a los consumidores
en el uso de su *software*,
sobre todo cuando los proyectos son ampliamente usados
y la gente está dispuesta a pagar por ellos.
Sin embargo,
los ingresos resultantes no suelen ser suficientes
para cubrir salarios decentes y,
al mismo tiempo,
inversiones en seguridad y servidores,
por ejemplo.
Además,
cuando hay pocas personas implicadas,
la prestación de un servicio puede representar un obstáculo
para el progreso del proyecto,
ya que es necesario invertir mucho tiempo en actividades
distintas al desarrollo y mantenimiento del *software*.

Lo que comienza como un entretenido proyecto de desarrollo de *software*
puede convertirse en un calvario para los desarrolladores
cuando tienen que reparar *bugs* o vulnerabilidades
que les han sido reportados
(algunos derivados de contribuciones de baja calidad),
realizar controles de calidad de las contribuciones
y hacer frente a críticas o comentarios de reclamación.
A veces,
las críticas proceden de los mismos colaboradores
o de individuos que se limitan a señalar los defectos de los proyectos
pero no aportan nada.
Y las reclamaciones de nuevas funcionalidades o consultoría
proceden de los usuarios de ese *software* libre,
a veces incluso de empresas de gran calibre,
bien establecidas y con enormes ingresos,
que descaradamente tienen ciertas expectativas puestas en esos proyectos
de los cuales dependen.

Es aquí donde se hace necesario el apoyo institucional.
Como en el caso de OpenSSL,
algunas personas simpatizantes crean fundaciones y nuevos roles
no orientados tanto hacia lo técnico sino hacia la gestión,
diseño, mercadeo, comunicación y sostenibilidad de estos proyectos.
Algunas organizaciones proporcionan esta asistencia,
pero no se meten con el soporte financiero.
Otras instituciones,
como la Fundación Linux y la Fundación Mozilla,
han obtenido apoyo para proyectos específicos
desde diversas empresas y organizaciones.
Las compañías de *software*, por ejemplo,
a veces contribuyen reportando problemas de seguridad,
sugiriendo cambios y aportando retroalimentación.
No obstante,
la fuerza y la cobertura de este apoyo siguen siendo bastante limitadas.

## Problemas para todos nosotros

Cansados,
agotados de gestionar estos proyectos
(quizá ya con otras prioridades en mente)
y de no obtener retribuciones más que el placer de programar,
estos desarrolladores pueden en cualquier momento abandonar sus proyectos,
a veces con la esperanza de que alguien más lo mantenga en marcha.
Esto puede representar una pérdida de mano de obra cualificada
y una ralentización del crecimiento de la innovación.
De no resultar voluntarios para la manutención,
cada consumidor tendría que buscar componentes de *software*
de código abierto similares
o tomar los proyectos y conservarlos de forma independiente,
entre otras posibles soluciones.

Por otro lado,
ya sea por la carga que recae sobre los desarrolladores
o por la intervención de desarrolladores aficionados
—sin una formación adecuada en ciberseguridad
o sin prestarle mucha atención a ella—
la infraestructura queda vulnerable,
más propensa a sufrir abusos y ataques
que supondrían incidentes como las interrupciones de servicios.
Que un proyecto sea abandonado o sufra violaciones de su seguridad,
seguramente ha de regalarle algo de fama y,
como consecuencia,
tal vez por un tiempo,
algo más de apoyo.
Pero ¿qué hay de aquellos proyectos similares
que siguen siendo impopulares y carecen de apoyo,
pero aún así son pilares de nuestra infraestructura?

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/gestion-vulnerabilidades/"
title="Inicia ahora con la solución de gestión de vulnerabilidades
de Fluid Attacks"
/>
</div>

## Falta de conciencia y apoyo

A menudo,
los desarrolladores o mantenedores no tienen ni idea
de quiénes están utilizando su *software*,
y muchos consumidores no tienen una visibilidad clara
de los componentes de *software* que emplean
o no saben de quiénes dependen esas librerías
ni cuál es su estado o situación.
Adicionalmente,
suelen haber ideas erróneas como la de que esta labor de los desarrolladores
de *software* de código abierto
está bien financiada
y cuenta con equipos amplios y constantemente activos.
De hecho,
los que suelen ser casos excepcionales de financiación suficiente
pueden ser vistos como si fueran la norma.

Ciertamente,
existe patrocinio de empresas de *software* y grupos de desarrollo
y donaciones de fundaciones, instituciones académicas y corporaciones,
pero estas no son suficientes.
Han surgido plataformas para hacer donaciones a los mantenedores
para apoyar su trabajo,
pero, como se menciona en el informe de Nadia, acaba siendo,
en sentido figurado,
dinero para un par de cervezas pero no para pagar la renta.
Confiar en la buena voluntad de la gente en donaciones no recurrentes
no es nada prometedor.

Asimismo,
no es fácil encontrar cooperación entre empresas que compiten en un mercado.
Una compañía puede no querer patrocinar un proyecto
que también beneficia a sus competidores
cuando estos no contribuyen en absoluto.
También hay problemas
cuando un benefactor privado desea privilegios especiales
(sobre todo si se demuestra que es el único
o principal contribuyente financiero)
o cuando hay apoyo desde un gobierno con intereses políticos,
lo que pone en peligro la neutralidad del proyecto.
La intención de preservar dicha neutralidad,
al tiempo que la descentralización,
puede entonces limitar muchos apoyos financieros potenciales.

## Es necesario más apoyo

El mantenimiento de algunos proyectos
es mucho más dispendioso que el de otros.
Cuando son muy pequeños,
el mantenimiento requerido no es significativamente alto.
En otros casos (usualmente proyectos de gran tamaño),
compañías grandes que dependen de ciertos paquetes,
asignan personal propio
para estar pendiente de contribuir en su mantenimiento,
liberando carga a esos individuos originalmente responsables
(incluso llega apoyo monetario),
o también contratan a algunos de los contribuyentes
para trabajar en el proyecto a tiempo completo.
La mayor preocupación se centra en los proyectos lo suficientemente grandes
como para requerir un mantenimiento significativo,
pero no lo suficientemente grandes
como para recibir la atención y el apoyo de las corporaciones.

Actualmente necesitamos un trabajo más colaborativo
para mantener nuestra infraestructura digital.
Deberíamos ver ese apoyo como una responsabilidad compartida.
Pero para ello,
primero necesitamos una comprensión más amplia y precisa de la situación.
A partir de ahí,
podríamos esperar la aparición de más defensores,
sin limitaciones políticas o comerciales,
de este enfoque que no deberíamos permitirnos perder.
La innovación y el progreso global que nos ha aportado este enfoque
no tiene parangón,
por lo que no sería conveniente terminar
en una "sociedad de *software* de código cerrado".

Parte de la solución
podría consistir en intentar hacer un reconocimiento completo,
desde cada empresa u organización,
de todas aquellas librerías, *frameworks*
y otros componentes de *software* de código abierto
que estamos utilizando y de los que dependemos.
A partir de ahí,
podríamos tratar de catalogar esos proyectos
y averiguar cuáles de ellos realmente necesitan apoyo.
En función de lo que definamos
y de nuestras capacidades,
podríamos empezar a proporcionar distintos tipos de recursos.
Incluso podríamos involucrarnos en fundaciones
orientadas a apoyar esos proyectos
y cooperar con nuestros socios que también necesiten de ellos.

Al tiempo que escribo estas palabras,
me van sonando tan idealistas, tan utópicas.
Bien decía Nadia que este problema no es fácil de resolver.
Pero recolectar y compartir datos y métricas
sobre el estado actual de muchos proyectos esenciales
y el impacto que tienen en nuestra economía
puede impulsar a muchos de nosotros que estamos dispuestos ayudar.
Sin embargo,
no todo el mundo quiere contribuir,
y, tal vez,
estropearía aún más las cosas buscar forzarles a que lo hagan.

Tal como hemos podido evidenciar directamente,
o al menos ver en experimentos de psicología social,
hay casos en los que puede haber mucha gente alrededor
de una persona que sufre y necesita ayuda,
y todos nos quedamos sin hacer nada,
normalmente esperando que alguien más dé el primer paso.
En este caso,
sabemos que ya algunos hemos intervenido de una u otra forma.
Aún así,
**necesitamos que el problema y nuestra contribución a su solución
sean visibles para todos**
y así podamos ayudar a evitar tragedias
en nuestra infraestructura digital.
