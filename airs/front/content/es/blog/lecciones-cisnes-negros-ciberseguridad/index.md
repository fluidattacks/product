---
slug: blog/lecciones-cisnes-negros-ciberseguridad/
title: ¡Demuestra que estás a salvo!
date: 2024-04-09
subtitle: Lecciones aprendidas de los cisnes negros
category: opiniones
tags: ciberseguridad, riesgo, empresa, hacking, software
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1712696620/blog/lessons-cybersecurity-black-swans/cover_lessons_cybersecurity_black_swans.webp
alt: Foto por Roy Muz en Unsplash
description: En ciberseguridad también existen los "cisnes negros". En cualquier momento podemos toparnos con uno de ellos, por lo que hay que estar preparados.
keywords: Cisnes Negros, Eventos De Cisne Negro, Prediccion, Sucesos Impredecibles, Sucesos Inesperados, Ciberataques, Inversion En Ciberseguridad, Pentesting, Hacking Etico
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/black-and-white-swan-GxcOkcBBJao
---

No puedes hacerlo.
No puedes demostrar que tú y tu compañía están definitivamente a salvo.
No caigan en el error de suponer que están exentos de peligros
y perjuicios dentro de esta amenazante jungla digital.
No hay manera de garantizar absolutamente que no serán
—es más, digamos mejor, seremos—
dañados o afectados por una amenaza cibernética en un futuro.

Te preguntarás:
"¿Cómo es posible que alguien de una empresa de ciberseguridad
me esté diciendo esto?"
**Honestidad**, **humildad** y **transparencia** son la respuesta.
Estos principios,
entre otros,
rigen nuestros esfuerzos en Fluid Attacks.
Por ello,
sin pelos en la lengua,
reconocemos que ni nosotros ni ninguna otra empresa podemos ni debemos
comprometernos a que tus aplicaciones y otros productos tecnológicos
estarán *definitivamente* a salvo.

Me decidí a escribir este *post*
luego de haber leído un muy corto ensayo del autor Tom Standage
en el libro *[This Will Make You Smarter](https://www.amazon.com/This-Will-Make-You-Smarter/dp/0062109391)*,
editado por John Brockman y publicado en 2012.
El ensayo lleva por nombre
"You Can Show That Something Is Definitely Dangerous
but Not That It's Definitely Safe."
(En la versión en español lo traducen como
"Se puede mostrar que algo es positivamente peligroso
pero no que sea radicalmente seguro.")
De inmediato,
este título despertó mi curiosidad,
y asocié el contenido con lo que había escrito recientemente
sobre la [justificación de la inversión en ciberseguridad](../justificar-la-inversion-en-ciberseguridad/).

Si bien Standage señaló en primer lugar la imposibilidad
de determinar que cierta tecnología no generará daños,
esto es un planteamiento fundamentado en la
**imposibilidad general de probar una realidad negativa**.
Por lo tanto,
esto también aplica para la afirmación que hice al principio de este *post*
y, de hecho, podemos emplearlo para infinidad de proposiciones.
De ahí,
por ejemplo,
podemos decir que no hay forma alguna de probar que un dios
o varios dioses *no* existen.
Así,
para muchos de nosotros,
como humanidad no hayamos encontrado evidencia alguna de su existencia
soportada por la ciencia,
no tenemos razones suficientes para decir que las deidades no existen.
(Aunque sin duda hay refutaciones para posturas como esta,
no pretendo ahondar aquí en discusiones lógico-filosóficas).

Algo similar ocurría hace siglos
cuando se mantenía la creencia,
verdad absoluta para muchos,
de que **los cisnes negros no existían**.
[Tal como afirma Tim Low](https://www.australiangeographic.com.au/topics/wildlife/2016/07/black-swan-the-impossible-bird/),
en la Europa medieval,
curiosamente,
los unicornios tenían más credibilidad que los cisnes negros.
Estos últimos eran mejores símbolos de lo imposible que los primeros.
Fue entonces un acontecimiento totalmente inesperado que,
hacia finales del siglo XVII,
exploradores europeos encontraran cisnes negros en territorio australiano.
De esta forma,
un simple suceso actuó como evidencia suficiente
para refutar dicha proposición de imposibilidad
(la cual no había forma de probar).

Por tanto,
**es mejor que no declaremos algo como imposible**.
El mero hecho de que hasta el momento haya habido un gran número de sucesos
o fenómenos consistentes suele llevarnos a esa inducción errónea.
Como decía Standage,
la ausencia de evidencia no es evidencia de ausencia.
Es preferible mostrarnos escépticos
y hablar de realidades difíciles de predecir.
Sin embargo,
son muchas las personas que desconocen
o simplemente no aplican esta forma de pensar y ver el mundo.
Esto lo dejé claro en el blog *post*
al que hice referencia al principio
cuando formulé algunas palabras adjudicables a los CEOs de muchas compañías:
"Hasta ahora no nos ha pasado nada malo, y seguramente nunca nos pasará."

## Cisnes negros en ciberseguridad

Pasa un día más para una compañía sin ser víctima de un ciberataque,
y este se suma a la prueba de que al día siguiente nada de eso ocurrirá.
Se arraiga firmemente un exceso de confianza.
Si no han recibido ataque alguno luego de un buen tiempo
sin haber invertido en seguridad,
no ven necesaria la inversión.
Si han recibido ataques pero ninguno ha sido catastrófico,
supuestamente gracias a lo que han invertido hasta el momento,
no ven necesario invertir más.
Se han adherido al patrón de realidad experimentado y conocido hasta entonces
y han llegado a afirmar que nada malo o nada peor les ocurrirá.
No obstante,
**los cisnes negros también aparecen en el escenario de la ciberseguridad**.

En diferentes campos de acción humana en las que existen todo tipo de riesgos,
como es el caso de la ciberseguridad,
se usa la metáfora de los cisnes negros.
Los cisnes negros,
en este caso,
suelen hacer referencia a **eventos altamente inesperados e impredecibles**
que pueden ocurrir de forma repentina
y traer consigo consecuencias significativamente graves.
Un ejemplo típico suele ser el del 9/11,
pero en ciberseguridad podemos mencionar
el [ataque del *ransomware* WannaCry](../10-mayores-ataques-ransomware/)
y el [ataque a la cadena de suministro de SolarWinds](../../../blog/solarwinds-attack/),
entre otros.

Resulta interesante que muchos de estos sucesos catastróficos e inesperados,
luego de haberse manifestado,
se consideren en retrospectiva como prevenibles.
Algunas de las personas afectadas acaban diciendo algo así como:
"Si hubiésemos prestado más atención y recogido más datos,
hubiésemos podido evitar que esto sucediera."
Esto me lleva a recordar las siguientes palabras del físico teórico
[Richard Feynman](https://www.inf.fu-berlin.de/lehre/pmo/eng/Feynman-Uncertainty.pdf)
(traducción propia):

<quote-box>

Es necesario y cierto que todas las cosas que decimos en la ciencia,
todas las conclusiones,
son inciertas,
porque solo son conclusiones.
Son conjeturas sobre lo que va a ocurrir,
y no puedes saber lo que va a ocurrir,
porque no has hecho los experimentos más completos.

</quote-box>

**La experimentación**,
elemento sustancial del método científico,
es también de enorme utilidad en la ciberseguridad.
Generar hipótesis y posibles escenarios de riesgo
y ponerlos a prueba de forma continua
puede ser de gran ayuda para prevenir,
así sea algunos,
"eventos de cisne negro."

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/pruebas-penetracion-servicio/"
title="Inicia ahora con las pruebas de penetración como servicio
de Fluid Attacks"
/>
</div>

## Recomendaciones para evitar enfrentarse a cisnes negros

Las amenazas y los ataques en el universo cibernético
aumentan constantemente en número y complejidad.
Ya es una patética costumbre ver noticias de ciberataques exitosos
en casi todos los países e industrias del mundo.
Todos los días observamos efectos devastadores sobre gobiernos,
bancos, entidades de salud, empresas tecnológicas, etc.
El simple hecho de contemplar esta realidad,
aunque no sea en carne propia,
debería ayudarnos a reconocer que **todos podemos ser vulnerables**,
desde el más grande y poderoso hasta el más pequeño y débil.

No esperemos con los brazos cruzados a que algo devastador nos suceda.
**Preparémonos desde ya para lo peor**.
Esforcémonos por identificar múltiples crisis potenciales
y escenarios desastrosos
en los que se verían afectadas nuestra seguridad
y la de nuestros clientes o usuarios.
No importa qué tan improbables sean.
Tratemos al máximo de reducir incertidumbres,
pero sin pretender que podríamos preverlo todo con certeza.
Busquemos con ahínco comprender
y reducir ampliamente nuestras superficies de ataque
y exposición al riesgo.
Y desarrollemos planes sólidos de prevención,
defensa, respuesta y resiliencia.

Por un lado,
podemos forjar asociaciones robustas de contribución
entre empresas u organizaciones,
reconociendo que,
por cuestiones de interconectividad en las infraestructuras digitales,
los ataques sobre una pueden tener efectos dominó sobre otras.
Estos equipos pueden ir identificando y compartiendo
a partir de sus experiencias y las de otros
los riesgos comunes y no comunes
y las mejores prácticas en ciberseguridad.
Compartamos, discretamente, claro está,
conocimientos sobre amenazas potenciales
y análisis posteriores de lo que fueron ciberataques exitosos.
Se trataría de un valioso y constante
**apoyo mutuo a través de lecciones aprendidas**.

Por otro lado,
no nos conformemos con adquirir y desplegar soluciones de seguridad
que solo pueden detectar y/o hacer frente a amenazas previamente conocidas.
(Como lo que sucede a través de evaluaciones con herramientas automatizadas
que se limitan a detectar vulnerabilidades
que forman parte de sus bases de datos.)
No nos mostremos satisfechos con estrategias convencionales.
No nos quedemos sin poner a prueba aquello que dista de lo ordinario.
**Diversifiquemos nuestra inversión en ciberseguridad**
y busquemos todas las vías de acceso que los cibercriminales podrían explotar
o los vectores de ataque que podrían emplear.
(Algo que se puede lograr mediante el [*hacking* ético](../que-es-hacking-etico/).)
Sigamos el ritmo de la apabullante evolución del panorama de amenazas.

**Las amenazas excepcionales deben abordarse con soluciones excepcionales**.
Tal es el caso de la solución todo en uno de Fluid Attacks:
**[Hacking Continuo](../../servicios/hacking-continuo/)**.
Con nuestra multiplicidad de métodos de detección de vulnerabilidades
y capacidades de gestión y remediación de vulnerabilidades,
ayudamos a reducir las incertidumbres.
Si bien somos sensatos al reconocer que no podemos,
ni puede nadie,
demostrarte que tu compañía no será víctima de un ciberataque violento,
sin duda contribuimos a que esas probabilidades disminuyan en gran medida.
¿Tienes alguna duda?
[Ponte en contacto con nosotros](../../contactanos/).
