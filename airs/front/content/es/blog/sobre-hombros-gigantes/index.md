---
slug: blog/sobre-hombros-gigantes/
title: Pararse sobre hombros de gigantes
date: 2018-02-14
category: ataques
subtitle: Sobre el análisis de composición de *software*
tags: pruebas de seguridad, software, vulnerabilidad
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1620331101/blog/stand-shoulders-giants/cover_iiuzyx.webp
alt: Foto por Vincent Riszdorfer en Unsplash
description: "Aquí desarrollamos principalmente una discusión sobre 'A9' del Top 10 de OWASP: Uso de componentes con vulnerabilidades conocidas, en particular bibliotecas de software libre y abierto."
keywords: Composicion De Software, Analisis, Dependencia, Vulnerabilidad, OWASP, Linux, Foss, Hacking Etico, Pentesting
author: Rafael Ballestas
writer: raballestasr
name: Rafael Ballestas
about1: Mathematician
about2: with an itch for CS
source: https://unsplash.com/photos/exKQ01AmzNA
---

En nuestro [último artículo](../../../blog/infinite-monkey-fuzzer/),
difundimos el descubrimiento de una vulnerabilidad en libpng.
Tú podrías decir que esa es solo una pequeña biblioteca con un alcance
muy limitado y solo 556 KiB instalados.
Sin embargo, montones de paquetes dependen de ella.
Para ver cuántos paquetes del repositorio de Arch Linux
dependen de libpng podemos usar el pacgraph
de [Kylee Keen](http://kmkeen.com/pacgraph/):

<div class="imgblock">

!["libpng reverse dependencies"](https://res.cloudinary.com/fluid-attacks/image/upload/c_scale,w_800/v1620331100/blog/stand-shoulders-giants/libpng-pacgraph_edml6c.webp)

<div class="title">

Dependencias inversas de libpng en Arch Linux.

</div>

</div>

¡Más de 14 GiB de *software* dependen de *libpng*!
Y eso solo en los repositorios de Arch Linux,
que no es precisamente la distribución más popular de Linux.
Además,
esta biblioteca es la referencia oficial de PNG y es multiplataforma,
por lo que seguramente muchos otros paquetes
en otros sistemas operativos dependen de ella.

Ahora bien, en el año 2015,
cuando *libpng* aún no había corregido el error *low-high palette*,
todos los programas y bibliotecas anteriores
también eran automáticamente vulnerables al mismo problema.
De hecho esto es lo que le pasó a Equifax
con una vulnerabilidad en Apache Struts.
Al igual que muchos servicios web que utilizan OpenSSL con Heartbleed.

Si esto pudo pasarle a productos tan emblemáticos como
[`Bash`](https://www.gnu.org/software/bash/),
[`Qt`](https://www.qt.io/),
[`TeX`](https://services.math.duke.edu/computing/tex/latex.html) y
[`Xfce`](https://xfce.org/), también podría pasarle a tu organización.
De hecho,
este problema es tan común que forma parte del
Top 10 de [OWASP](../../../compliance/owasp/) de 2017;
lo llaman "A9: Uso componentes con vulnerabilidades conocidas."

Dada la rápida adopción del *software* libre
y de código abierto (FOSS, por su nombre en inglés)
por parte de grandes empresas,
de repente la vulnerabilidad de las dependencias parece ser
un problema [tremendo](https://en.wikipedia.org/wiki/Dependency_hell).
O más bien, como les gustaría señalar a los *yuppies*,
¿una "oportunidad de negocio"?

Desde entonces han aparecido en la escena
de la seguridad muchos proveedores del llamado análisis
de composición de *software* (SCA)
(no lo busques en Google).
Algunos de ellos están respaldados por empresas de larga trayectoria;
la mayoría, no.
De hecho,
este negocio ha tomado tanto impulso que se espera
que crezca más de un 20% cada año de aquí
[al 2022](https://www.prnewswire.com/news-releases/the-software-composition-analysis-market-is-expected-to-grow-from-usd-1540-million-in-2017-to-usd-3984-million-by-2022-at-a-compound-annual-growth-rate-cagr-of-209-300595028.html).

Estas empresas, que tanto deben al FOSS,
tienden a hacerlo quedar mal cuando comercializan su SCA.
Sin embargo, la adopción del FOSS no se está reduciendo.
Como intentaremos demostrar aquí,
no es culpa del FOSS si las aplicaciones
lo utilizan con vulnerabilidades conocidas,
sino de los propietarios de dichas aplicaciones.

[Las aplicaciones de hoy en día](https://pure.qub.ac.uk/en/publications/vulnerability-detection-in-open-source-software-the-cure-and-the-)
utilizan en promedio más de 30 bibliotecas,
lo que representa hasta un 80% de su código.
Piensa en ello
como si tu código fuera solo una fina capa sobre un edificio
de algunas cajas pequeñas y otras más grandes.
Lo que el SCA hace entonces es buscar vulnerabilidades
dentro de esas cajas con información de bases de datos externas,
las cuales pasan a ser consideradas vulnerabilidades en tu propia aplicación:

<div class="imgblock">

!["Dependency building"](https://res.cloudinary.com/fluid-attacks/image/upload/v1620331101/blog/stand-shoulders-giants/depvuln_ztp8tw.webp)

<div class="title">

SCA escanea todos los bloques del edificio de tu app.

</div>

</div>

En lugar de ir desde la supuesta solución hacia el origen del problema,
hagámoslo al revés.

## Lo malo

El FOSS es desarrollado
y utilizado por miles de personas en todo el mundo.
Esto puede ser un arma de doble filo:
Por un lado, según la "Ley de Linus",
la búsqueda de errores y la aplicación de parches
deberían ser más fáciles al haber más implicados.
Por otro lado,
la falta de una guía centralizada favorece la aparición de fallas.

Una diferencia del FOSS con el *software* propietario está en que,
debido a todas sus restricciones,
es "menos probable" que las fallas de este último
se hagan públicas tan pronto como en el caso del primero.
Así que suele esperarse que todas las vulnerabilidades
en el *software* propietario sean de día cero.

Entonces, si el origen del problema no es el FOSS,
¿cuál es? Las principales razones por las que tantas empresas sufren de A9:

- Desconocimiento de las dependencias utilizadas

- Desconocimiento de sus vulnerabilidades

- No escanear continuamente en busca de fallas

- No realizar pruebas de compatibilidad

- Mala configuración de los componentes

En definitiva,
todo se reduce a una falta de comunicación entre el usuario
y la fuente de los componentes.

## Lo bueno

¿Qué puedes hacer? OWASP recomienda las siguientes directrices
[para prevenir A9](https://owasp.org/www-project-top-ten/2017/A9_2017-Using_Components_with_Known_Vulnerabilities.html):

- Recorta dependencias innecesarias, características, componentes, etc.
  Así tendrás menos para revisar.

- Supervisa continuamente los componentes en búsqueda de actualizaciones
  y reportes de vulnerabilidades.

- Obtén solo componentes de fuentes fiables.

- Convierte estas directrices en una política de empresa.

Existen herramientas específicas que
comparan la versión de la dependencia que estás utilizando tanto
con repositorios remotos
(para comprobar si hay actualizaciones)
como con bases de datos de vulnerabilidades
(para averiguar si alguna de tus dependencias
contiene vulnerabilidades reportadas que aún no han sido corregidas).

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/gestion-vulnerabilidades/"
title="Empieza ya con la solución Gestion de vulnerabilidades de Fluid Attacks"
/>
</div>

- Para JavaScript puedes usar
  [*retire.js*](https://github.com/retirejs/retire.js/).

- Los usuarios de Java tienen el *plugin*
  [Versions](http://www.mojohaus.org/versions-maven-plugin/)
  para Maven.

- También para Java y .NET, puedes utilizar la
  herramienta [`OWASP Dependency-Check`](https://www.owasp.org/index.php/OWASP_Dependency_Check).

- Hay un *plugin* de SonarQube de evaluación de
  [dependencias](https://github.com/stevespringett/dependency-check-sonar-plugin/tree/master/examples/single-module-maven).

Ten en cuenta que las herramientas específicas del lenguaje
tienen que estar integradas con el gestor de paquetes apropiado,
como *npm* o *yarn* con *retire*.

Una vista panorámica de cómo el proceso debería integrarse
con tu flujo de desarrollo se representa
en el siguiente diagrama proporcionado por [SourceClear](https://en.wikipedia.org/wiki/SourceClear).

<div class="imgblock">

!["Integración de SCA en el flujo de desarrollo"](https://res.cloudinary.com/fluid-attacks/image/upload/v1620331100/blog/stand-shoulders-giants/source-clear-flow_uplizt.webp)

<div class="title">

Integrando SCA en tu flujo de desarrollo. Vía [SourceClear](https://en.wikipedia.org/wiki/SourceClear).

</div>

</div>

Vemos que cada vez que se añade código,
todo el sistema es escaneado en búsqueda de vulnerabilidades de *software*
de terceros y otros problemas fácilmente identificables
por análisis estático cuando el código no está disponible.
Esto se hace siguiendo este procedimiento:

1. Identificar las dependencias en las que tu *software* se basa.

2. Detectar las versiones de esas dependencias.

3. Comprobar si hay actualizaciones en el repositorio maestro de dependencias.

4. Comprobar una o varias bases de datos de vulnerabilidades, como
   [CVE](https://cve.mitre.org/) y [NVD](https://nvd.nist.gov/)
   o las propias.

5. Reportar los hallazgos.

En realidad, se trata de un proceso sencillo.

Ten en cuenta que la integración no es totalmente automática,
y no debería serlo, ya que estas herramientas podrían dar
(y suelen dar) falsas alarmas,
por lo que son revisadas por especialistas en seguridad.

Internamente, el proceso de escaneo de *software* de terceros
es el mismo tanto para el propietario como para el FOSS,
y es una simple cuestión de consultar las bases de datos
de vulnerabilidades como hemos descrito anteriormente.

Hablando de integración, puede que te preguntes:
¿Qué pasa si mi aplicación se despliega dentro de un contenedor?
"El 30% de las imágenes oficiales de Docker Hub contienen vulnerabilidades
de seguridad de alta prioridad", según [PenTestIt](https://pentestit.com/anchore-open-source-container-inspection-analysis-system/).
Afortunadamente,
hay herramientas que entran en tu contenedor
y realizan SCA dentro de él (y más), como [Anchore](http://pentestit.com/anchore-open-source-container-inspection-analysis-system/)
y
[Dockerscan](http://pentestit.com/dockerscan-docker-security-analysis-suite/).

## Lo feo

Sé que buscaste "análisis de composición de *software*"
cuando te sugerí que no lo hicieras.
Solo sé que lo hiciste. Si no lo hiciste,
¡bien por ti! Esto es lo que te estás perdiendo:

<div class="imgblock">

!["recopilación de proveedores de SCA"](https://res.cloudinary.com/fluid-attacks/image/upload/v1620331099/blog/stand-shoulders-giants/marketing-hype_pedyr7.webp)

<div class="title">

Proveedores de "análisis de composición de *software*".

</div>

</div>

Todos estos líderes de la industria, galardonados,
creadores de avances, oráculos del futuro de la tecnología,
quieren venderte una cosa: análisis de código estático
más las herramientas que hemos comentado antes.

Aunque el análisis estático es una técnica válida,
es solo una técnica.
Escanea código y detecta vulnerabilidades
y prácticas poco saludables,
pero también fomenta la detección tardía
y produce muchos falsos positivos.

Podrías contratar un servicio de este tipo
e incluso intentar complementarlo con herramientas
de análisis dinámico como [*fuzzing*](../../../blog/infinite-monkey-fuzzer/)
y *debuggers*, pero estas tienen sus propios problemas.

Cabe señalar que no sustituyen a la
[revisión de código](../../soluciones/revision-codigo-fuente/)
a la antigua, por humanos.
Al menos por el momento.
Según [Contrast Security](https://cdn2.hubspot.net/hub/203759/file-1100864196-pdf/docs/contrast_-_insecure_libraries_2014.pdf),

<quote-box>

La única forma de afrontar el riesgo de vulnerabilidades
desconocidas en las bibliotecas es que alguien
que entienda de seguridad analice el código fuente.
El análisis estático de las bibliotecas es mejor concebirlo
como una forma de dar señales de dónde pueden estar localizadas
las vulnerabilidades de seguridad en el código,
y no como un sustituto de los expertos.

</quote-box>

En el futuro, es posible que veamos cosas como
[pruebas de seguridad](../../soluciones/pruebas-seguridad/)
distribuidas bajo demanda
y [algoritmos de *machine learning*](https://pure.qub.ac.uk/en/publications/vulnerability-detection-in-open-source-software-the-cure-and-the-)
que utilicen máquinas de vectores de soporte para intentar predecir
qué *commits* tienen más probabilidades de abrir vulnerabilidades,
pero mientras tanto, quédate con lo probado y comprobado.