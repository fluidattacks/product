---
slug: blog/tipos-de-pruebas-de-penetracion/
title: Tipos de pruebas de penetración
date: 2023-01-17
subtitle: Pentesting es un enfoque de seguridad para todo sistema
category: filosofía
tags: pentesting, pruebas de seguridad, hacking, ingeniería social, empresa, ciberseguridad
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1673965755/blog/types-of-penetration-testing/cover_pentesting.webp
alt: Foto por Thomas Griggs en Unsplash
description: Los tipos de pruebas de penetración incluyen aquellas para redes externas e internas, redes inalámbricas, IoT y aplicaciones móviles y *pentesting* basado en ingeniería social. Aprende más aquí.
keywords: Tipos De Pruebas De Penetracion, Pruebas De Penetracion De Redes, Pruebas De Penetracion De Aplicaciones Web, Pruebas De Penetracion Externas, Pruebas De Penetracion De Aplicaciones, Pruebas De Penetracion Internas, Pruebas De Penetracion De Aplicaciones Moviles, Hacking Etico, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Escritor y editor
source: https://unsplash.com/photos/xsGApcVbojU
---

Las [pruebas de penetración](../que-es-prueba-de-penetracion-manual/)
(también conocidas como pruebas de penetración "manuales")
son un enfoque reconocido de pruebas de ciberseguridad
que aprovecha la experiencia de los *hackers* éticos,
o *pentesters*,
para encontrar debilidades y vulnerabilidades complejas.
Se suelen clasificar en diferentes tipos,
los cuales hacen referencia al sistema de información
objetivo de las evaluaciones.
Estas tecnologías difieren ligeramente en el tipo de problemas
que suelen presentar y en las técnicas que utilizan
los atacantes para descubrirlos.
Sin embargo,
los pasos o fases de las pruebas de penetración
en todos los tipos suelen ser los mismos:
planificación, reconocimiento, evaluación de vulnerabilidades,
explotación e informe.
Aprende sobre los diferentes tipos,
los cuales cubrimos
en nuestra [solución de *pentesting*](../../soluciones/pruebas-penetracion-servicio/).

## Pruebas de penetración externas

Este tipo de pruebas de penetración en la red
se centra en la seguridad de los sistemas con acceso a Internet.
Los controles que protegen tecnologías como sitios web,
bases de datos, aplicaciones web y servidores
de protocolos de transferencia de archivos
son aquello que quizá conozcas como "seguridad del perímetro"
dentro de una organización.
El objetivo de las pruebas de penetración en redes externas
es encontrar puntos débiles en estos controles,
así como vulnerabilidades en los propios sistemas.

Este tipo de prueba es muy valiosa porque
implica la simulación de atacantes externos
para ver si la red puede ser penetrada,
lo cual es necesario a medida que las organizaciones
tienen una presencia incrementada en Internet.
Desde esta perspectiva,
las técnicas ofensivas utilizadas por los *hackers* éticos
incluyen la exploración de vulnerabilidades,
la recopilación de información, la fuerza bruta
(p. ej., pulverización de contraseñas, relleno de credenciales)
y la explotación.
Dada la constante evolución de las ciberamenazas,
se aconseja a las organizaciones que soliciten pruebas
de todos los sistemas
(p. ej., pruebas de penetración de aplicaciones web,
pruebas de penetración en la nube)
de forma continua (es decir, **todo el tiempo**).

Cuando a los *pen testers* que sondean la red
desde el exterior no se les da información detallada
ni acceso al código fuente antes de las evaluaciones,
esto se considera un tipo de prueba de penetración de caja negra.
Aunque a lo largo de este texto será evidente
que cualquier tarea de *pentesting* también
podría ser de tipo de caja blanca,
que da acceso inicial al código fuente,
o de tipo caja gris, que da información inicial limitada.
(Estas categorías quedan fuera del alcance de este artículo,
ya que el criterio del que surgen es la información inicialmente disponible,
no el tipo de sistema evaluado).

## Pruebas de penetración internas

En contraste con el tipo anterior de *pentesting*,
éste simula las formas en que se comporta un atacante
tras haber obtenido acceso a la red interna.
Es importante destacar que estas pruebas pueden
dar una idea de las formas en que una persona
con acceso a la red interna podría exponer a riesgos intencionadamente
o involuntariamente a la organización.

Entre las técnicas que los *hackers* éticos
pueden utilizar en las pruebas de penetración interna
se encuentran los ataques de adversario en el medio
(p. ej., el envenenamiento del protocolo
Link-Local Multicast Name Resolution (LLMNR)),
el robo o la falsificación de *tickets* de Kerberos y los ataques IPv6.

[Lee aquí](../../../systems/networks-and-hosts/)
cómo evaluamos continuamente las redes
con pruebas de penetración externas e internas.

## Pruebas de penetración en redes inalámbricas

Las organizaciones pueden aprovechar las pruebas de penetración
para evaluar si los atacantes
pueden comprometer su wifi y acceder a su red.
La búsqueda de vulnerabilidades suele implicar
la evaluación de puntos de acceso,
clientes inalámbricos y protocolos de red inalámbricos
(p. ej., Bluetooth, LoRa, Sigfox).
Los hallazgos más comunes son las debilidades
en el cifrado y las vulnerabilidades de las claves
de acceso wifi protegido (WPA).
Las técnicas que pueden utilizar
los *hackers* éticos incluyen la fuerza bruta,
comprometer los dispositivos inalámbricos
y desplegar puntos de acceso no autorizado dentro de la red.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/pruebas-penetracion-servicio/"
title="Inicia ahora con las pruebas de penetración como servicio
de Fluid Attacks"
/>
</div>

## Pruebas de penetración en IoT

La Internet de las cosas (IoT)
es un sistema que implica la interacción de una gran cantidad
de elementos convencionales diferentes
(p. ej., servicios en la nube, sistemas operativos, aplicaciones)
con los diversos dispositivos inteligentes conectados a la misma red.
En este caso,
se requiere un esfuerzo adicional para controlar
los posibles vectores de ataque.
Estos son diferentes a los de la infraestructura de TI tradicional,
ya que comprometer cualquiera de los dispositivos
o sensores de la IoT puede significar comprometer
toda la infraestructura de la IoT.
Las pruebas de penetración se utilizan para conocer la resistencia
de la IoT corporativa frente a amenazas adversas externas.

En las pruebas de penetración,
los *hackers* éticos pueden realizar inspecciones físicas
de los dispositivos IoT, además del reconocimiento de la red.
También pueden realizar análisis de *firmware*,
incluyendo la evaluación de bibliotecas de terceros
y técnicas de cifrado y ofuscación.
Por lo tanto,
las pruebas de penetración son útiles
para encontrar problemas como configuraciones erróneas
de *firmware* desactualizado y protocolos
y canales de comunicación inseguros.

[Descubre aquí](../../../systems/iot/) cómo ayudamos
a mantener el IoT libre de debilidades y vulnerabilidades de seguridad.

## Pruebas de penetración de aplicaciones móviles

A medida que los teléfonos están cada vez más presentes
en las empresas como medio para realizar operaciones comerciales,
la seguridad de las aplicaciones que se descargan
en ellos se convierte en una necesidad.
Las pruebas de penetración se utilizan
para detectar problemas de seguridad complejos,
como fallos de lógica empresarial,
configuración de despliegue e inyección en aplicaciones
que se ejecutan en sistemas operativos
como Android, iOS y Windows UI.
Este trabajo manual,
combinado con la automatización (escaneo de vulnerabilidades),
aumenta la precisión de las evaluaciones de seguridad,
con un bajo índice de falsos positivos y falsos negativos.

Para este tipo de pruebas,
los *hackers* éticos pueden revisar manualmente el código fuente,
desarrollar ataques personalizados
e incluso realizar ingeniería inversa
para comprobar si las aplicaciones móviles
evaluadas carecen de mecanismos eficaces para ofuscar
el código y evitar la revelación de información.
Los procesos de revisión del código
y de ataque a la app mientras se ejecuta corresponden,
respectivamente,
a las [pruebas de seguridad de aplicaciones estáticas](../../producto/sast/)
(SAST)
y a las [pruebas de seguridad de aplicaciones dinámicas](../../producto/dast/)
(DAST).
Hemos definido las pruebas de seguridad de aplicaciones móviles (MAST),
además de enumerar los principales riesgos
para las aplicaciones móviles, en [otro artículo del blog](../que-es-mast/).

[Aprende aquí](../../../systems/mobile-apps/)
cómo ayudamos a proteger las aplicaciones móviles de forma continua.

## Pruebas de penetración basadas en ingeniería social

Una definición amplia de "sistema de información" incluye a las personas.
En efecto, los seres humanos
recogen, procesan, almacenan y distribuyen información vital
para el funcionamiento de las organizaciones.
En vista de ello,
la ciberseguridad se interesa por las personas
como agentes que pueden prevenir los ciberataques.
Las pruebas de penetración entran en este escenario
como un enfoque para evaluar la resistencia de las organizaciones
a los ataques a través de su personal.

La forma en que las personas son atacadas
es la [ingeniería social](../../../blog/social-engineering/),
que es cuando los atacantes intentan influir en las personas
para que asuman riesgos de ciberseguridad.
Probablemente haya oído hablar del [*phishing*](../../../blog/phishing/),
en el que se envían mensajes
a los empleados de las organizaciones objetivo persuadiéndoles
para que sigan direcciones web fraudulentas,
abran archivos adjuntos o envíen una respuesta.
Ésta y otras técnicas similares
(p. ej., estafas telefónicas)
pueden utilizarse en las pruebas de penetración,
por supuesto,
sin el conocimiento previo de las personas
a las que los *hackers* éticos intentan estafar.
Identificar los puntos débiles en sus respuestas
ayuda a señalar las áreas del elemento humano de ciberseguridad
que necesitan reforzarse con formación
(p. ej., identificar mensajes de *phishing*,
[detectar e informar](../../../blog/human-security-sensor/)
comportamientos inusuales).

En Fluid Attacks,
ofrecemos pruebas con técnicas de ingeniería social en nuestra
[solución de *red teaming*](../../soluciones/red-team/).

## Pruebas de penetración con Fluid Attacks

Fluid Attacks realiza [*pentesting* continuo](../../soluciones/pruebas-penetracion-servicio/)
a lo largo de todo el ciclo de vida de desarrollo de *software* (SDLC).
En este artículo proporcionamos enlaces
a páginas que amplían cómo cubrimos con nuestra solución
los tipos que ofrecen la mayoría de las empresas
de pruebas de penetración.
Si quieres saber más, echa un vistazo
a los [sistemas que evaluamos](../../../systems/).

Recuerda que según muchos estándares,
como los [recientes cambios en la normativa](https://www.federalregister.gov/documents/2021/12/09/2021-25736/standards-for-safeguarding-customer-information)
tras la Ley Gramm Leach Bliley, o GLBA,
las pruebas de penetración deben realizarse con regularidad.
Nosotros te ayudamos a ir más allá del cumplimiento básico
y te ayudamos a asegurar tu *software*
de forma continua conforme lo vas desarrollando.
Nuestro servicio es [Hacking Continuo](../../servicios/hacking-continuo/),
y su [plan](../../planes/) más completo incluye *pentesting*.

Puedes empezar tu [**prueba gratuita de 21 días de Hacking Continuo**](https://app.fluidattacks.com/SignUp),
que incluye solo nuestras pruebas de seguridad automatizadas.
Pruébala y cambiate cuando quieras al plan que incluye pruebas manuales.
