---
slug: blog/que-es-prueba-de-penetracion-manual/
title: ¿Qué es el pentesting manual?
date: 2022-10-20
subtitle: Cómo funciona y en qué se diferencia del "automatizado"
category: filosofía
tags: pentesting, ciberseguridad, pruebas de seguridad, hacking, empresa
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1666278975/blog/what-is-manual-penetration-testing/cover_manual_pentesting.webp
alt: Foto por Ian Dooley en Unsplash
description: Este artículo explica qué son las pruebas de penetración manuales, cómo funcionan para descubrir vulnerabilidades y su diferencia con las llamadas "pruebas de penetración automatizadas".
keywords: Pruebas De Penetracion Manuales, Pruebas De Penetracion Automatizadas, Caja Negra, Caja Blanca, Caja Gris, Pruebas De Penetracion Vs Escaneo De Vulnerabilidad, Pentester, Pentesting, Hacking Etico
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/iD5aVJFCXJg
---

Las [pruebas de penetración](../../soluciones/pruebas-penetracion-servicio/)
o *pentesting*
son un tipo de [pruebas de seguridad](../../soluciones/pruebas-seguridad/)
en las que uno o varios *hackers* éticos atacan un sistema
de información con la autorización de su propietario
para identificar y notificar vulnerabilidades de seguridad.
¿Qué? Sí, lo sabemos.
Seguramente habrás oído que es algo que también se puede realizar
y conseguir mediante herramientas automatizadas específicas.
Normalmente se habla de "pruebas de penetración manuales"
y "pruebas de penetración automatizadas".
Sin embargo, algunos consideramos
y afirmamos que lo que ocurre en estas últimas
no se ajusta a lo que realmente es el *pentesting*
y que este nombre no fue más que fruto de una estrategia de mercadeo.
El *pentesting* manual, como veremos, es el único *pentesting*.

## ¿Pruebas de penetración manuales vs. automatizadas?

Las **pruebas de penetración manuales** (MPT, por su nombre en inglés)
son las realizadas por expertos en seguridad ofensiva
denominados *pentesters* o *hackers* éticos.
En su trabajo,
deben utilizar como soporte herramientas de propiedad privada o pública,
algunas de las cuales pueden ser herramientas automatizadas.
Por otro lado,
las **pruebas de penetración automatizadas** (APT, por su nombre en inglés)
son supuestamente *pentesting* realizado
por herramientas automatizadas.
Una persona con conocimientos básicos de ciberseguridad,
por ejemplo de un equipo informático corriente,
podría poner en marcha este tipo de herramientas sin ningún problema.

Los *pentesters* en **MPT** simulan ataques del mundo real,
empleando diversas tácticas,
técnicas y procedimientos que los *hackers* maliciosos
también podrían utilizar.
Los *pentesters* muestran creatividad en sus formas de atacar.
Pueden dar giros inesperados durante las pruebas,
en función de sus objetivos y resultados.
Las herramientas de **APT**, por el contrario,
siguen un patrón estándar y delimitado.
Se limitan a entregar informes con vulnerabilidades predeterminadas
y conocidas, muchas de las cuales son bastante fáciles de detectar.
Las herramientas no pueden hacer lo que los *pentesters*,
esto es, identificar vulnerabilidades de día cero
(es decir, problemas de seguridad previamente desconocidos).

En **MPT**,
los *pentesters* buscan comprender la estructura
y funcionalidad del objetivo de evaluación
(ToE, por su nombre en inglés).
A partir de ahí,
son capaces de identificar tipos de vulnerabilidades
que no se detectan o apenas se detectan en **APT**.
Por ejemplo,
la detección y verificación de algunos problemas
con el manejo de datos,
como *Cross-site Scripting* (XSS) y *SQL Injection*,
así como fallas en la lógica de negocio
y vulnerabilidades en el control de acceso,
dependen más de **MPT**.
**APT** se centra más en, por ejemplo,
reglas de permisos defectuosas,
actualizaciones faltantes y configuraciones erróneas.
Las respuestas del ToE a determinados *inputs*
pueden parecer válidas a las herramientas automatizadas cuando,
en realidad, son anomalías a los ojos de los *pentesters*.
Por su profundidad y análisis,
MPT puede informar de las vulnerabilidades más complejas
y críticas, las cuales, por cierto,
los *pentesters* pueden explotar para evaluar su impacto.
Algo que las herramientas no consiguen.

Dado que implica a seres humanos en una inmersión profunda,
**MPT** suele llevar más tiempo y ser más costoso.
Las herramientas de **APT** alquiladas
o compradas realizan sus pruebas mucho más rápido,
pero de forma superficial.
Otro problema de estas últimas
es que a menudo informan de falsos positivos.
Parte del tiempo ganado en sus evaluaciones
se pierde con la necesidad de verificar los falsos positivos.
Los desarrolladores acaban pasando horas lidiando con mentiras.
Como es más barato,
**APT** suele aplicarse con más regularidad que MPT.
Sin embargo,
como en PTaaS ([pruebas de penetración como servicio](../que-es-ptaas/)),
**MPT** también puede aplicarse para evaluaciones continuas.

Los informes de algunas herramientas de **APT**
pueden no ser muy detallados
o no proporcionar recomendaciones de soluciones.
Esto es diferente de lo que pueden conseguir los *pentesters*.
Además,
al evaluar más superficies de ataque a través de diversos métodos,
los *pentesters* permiten a los equipos
de desarrollo disponer de resultados
para hacer frente a una gama más amplia de ciberataques.
Cabe señalar que la eficacia de **MPT**
depende de las capacidades de cada *hacker* ético contratado
y puede variar de vez en cuando.
En **APT**, en cambio,
la eficacia está siempre predeterminada.

Fusionar ambas metodologías presentadas
permite obtener beneficios de cada una de ellas.
El trabajo rápido y superficial
de las herramientas automatizadas permite
a los *pentesters* invertir tiempo en evaluaciones complejas.
De hecho,
así es como se suele hacer un *pentesting* adecuado.
Las herramientas contribuyen en una de sus fases
a lo que podríamos decir que realmente hacen:
**escanear vulnerabilidades**.
No existen pruebas de penetración automatizadas.
Eso no existe hasta ahora.
Son los humanos expertos los que hacen el *pentesting*,
dentro del cual,
en una de las fases que veremos más adelante,
pueden recurrir a la ayuda de herramientas automatizadas.

## *Pentesting* de caja negra, blanca y gris

Las pruebas de penetración manuales, es decir,
el *pentesting* propiamente dicho,
pueden clasificarse en función de la información
de que disponen inicialmente los *pentesters*:

### *Pentesting* de caja negra

Como puede ocurrir para un atacante,
en este tipo de *pentesting* los *pentesters*
solo conocen el nombre y la ubicación del objetivo.
No tienen ningún detalle sobre este.
Es decir, desconocen la estructura,
el código fuente y el funcionamiento interno
del sistema que van a atacar.
Por ello, los *pentesters* se ven obligados
a recopilar mucha información y recurrir a métodos
como la fuerza bruta para acceder al ToE.
Esta prueba puede llevar más tiempo que las otras,
en las que los propietarios del sistema dan información
a los *hackers* éticos desde el principio.
En el *pentesting* de caja negra,
los *pentesters* no evalúan el código a menos
que consigan acceder a él por algún motivo.
Lo que hacen es centrarse en los aspectos externos
y el comportamiento del ToE.
Este modo de evaluación, también llamado "prueba y error",
puede llevar a identificar menos vulnerabilidades
en comparación con los otros modos.

### *Pentesting* de caja blanca

En esta modalidad,
también denominada pruebas de caja de cristal o pruebas estructurales,
los *pentesters* tienen un amplio conocimiento del ToE,
así como acceso a su código fuente
y a otros recursos a los que pueden acceder sus desarrolladores.
[La revisión del código](../secure-code-review/) implica el uso de herramientas
que no se emplean en el *pentesting* de caja negra.
Puede ser entonces una valoración más completa
e integral para la posible identificación
de un número más significativo de vulnerabilidades existentes.
En efecto,
aunque en este caso los *pentesters* realizan pruebas estructurales
(dándole quizás una mayor prioridad),
también pueden realizar pruebas funcionales o de negocio,
como en la modalidad de caja negra.

### *Pentesting* de caja gris

En este caso, se mezclan los dos modos anteriores.
Aquí los *pentesters* reciben información parcial sobre el ToE.
Por ejemplo,
solo tienen acceso a elementos internos relevantes
para las pruebas programadas,
como la documentación y la arquitectura,
pero no al código fuente.
En el *pentesting* de caja gris,
la valoración se centra tanto en la funcionalidad
como en la estructura del objetivo,
pero sin ser un método de intrusión en el código.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/pruebas-penetracion-servicio/"
title="Inicia ahora con las pruebas de penetración como servicio
de Fluid Attacks"
/>
</div>

## Cómo realizar pruebas de penetración

No existe un estándar universal para esta actividad,
la cual, de hecho, implica una gran dosis de creatividad
y adaptación a las características del ToE
por parte de los *pentesters*.
No obstante,
suelen seguirse algunos pasos o fases generales
que se presentan a continuación:

### Fase de planificación

Se determina el alcance,
teniendo en cuenta las políticas y normas de seguridad.
Los *pentesters* comienzan a definir estrategias basadas
en sus conocimientos y experiencia.

### Fase de recolección de datos o reconocimiento

Los *pentesters* comienzan a recopilar información sobre el ToE.
Esto dependerá del modo de prueba.
Si bien es cierto que en un *pentesting* de caja blanca
los *pentesters* disponen de una gran cantidad de información,
profundizar en estos recursos permite dejar claro
a qué podría acceder un atacante malicioso en el ToE.
Existen muchas herramientas gratuitas útiles
para recopilar datos sobre, por ejemplo,
versiones de *software* y bases de datos,
tipos de *hardware*
y componentes de terceros utilizados en el ToE.
(Algunas pueden encontrarse gracias
al [OSINT framework](https://osintframework.com/).)
En su intento de acceder al sistema,
los *pentesters* también pueden buscar la forma de obtener
datos como usuarios y contraseñas.

El reconocimiento suele ser inicialmente pasivo y luego activo.
En el primero,
los *pentesters* recopilan información sobre el ToE
sin contacto directo con este.
No hay forma en este proceso de activar algún sistema
de detección de intrusos ni de dejar ningún rastro.
Entran en juego las búsquedas en Google,
el escaneo de redes sociales y sitios como
[netcraft.com](https://www.netcraft.com/)
y [archive.org](https://archive.org/).
En el reconocimiento activo, en cambio,
hay interacción con el ToE.
La recolección de información es más intrusiva,
por lo que existe la posibilidad
de que los *pentesters* sean detectados.
La idea es identificar la tecnología utilizada por el ToE
y los posibles vectores de entrada y ataque.
Aquí, por ejemplo, se suele realizar un escaneo de puertos
para determinar aquellos que están abiertos
y con qué servicios y sistemas operativos están relacionados.
Uno de los escáneres de puertos más reconocidos es [nmap](https://nmap.org/).

En relación con lo anterior,
también existe un proceso denominado enumeración.
En él se examina detalladamente la superficie de ataque.
Los *pentesters* tratan de determinar qué hay en el ToE.
En función del objetivo, es posible conocer,
por ejemplo, los servidores y dispositivos implicados,
así como los usuarios, carpetas y archivos.
Algunas herramientas de enumeración
son [Cain and Abel](https://sectools.org/tool/cain/)
(como vimos hace algún tiempo, también se utiliza para descifrar contraseñas),
[Angry IP Scanner](https://angryip.org/) y [SuperScan](https://sectools.org/tool/superscan/).
Aparte de los escáneres de puertos y las herramientas de enumeración,
los *pentesters* también pueden utilizar *packet sniffers*,
los cuales se utilizan para interceptar y registrar el tráfico de red.

### Fase de evaluación de vulnerabilidades

Los *pentesters* identifican posibles puntos débiles
de seguridad que pueden utilizarse
para acceder al sistema y causar un impacto.
Aquí es donde pueden entrar en acción las llamadas
"pruebas de penetración automatizadas",
es decir, el escaneo de vulnerabilidades.
Las herramientas pueden ser útiles para acelerar
el proceso detectando algunas vulnerabilidades conocidas.
Sin embargo, ya sabemos que la identificación
de los problemas de seguridad de mayor riesgo depende
del trabajo manual de los *pentesters*.
En esta fase, los expertos pueden supervisar
las tendencias actuales para aclarar qué amenazas
son problemáticas para el ToE determinado.
Además, determinan el impacto de las vulnerabilidades identificadas,
teniendo en cuenta factores como el escenario de ataque,
la dificultad de explotación, el efecto sobre la integridad,
confidencialidad y disponibilidad del sistema,
y la influencia sobre los sistemas cercanos.

### Fase de explotación

Aquí hablamos explícitamente de lanzar ataques contra el ToE
y evaluar el impacto real.
Los *pentesters* desarrollan u obtienen *exploits*
(piezas de *software*)
para aprovecharse de las vulnerabilidades identificadas.
Pueden moverse por el ToE y lograr objetivos específicos
que demuestren lo que un *hacker* malicioso podría conseguir
(por ejemplo, robo de datos e interrupción de las operaciones).
En esta fase,
proveedores como Fluid Attacks pueden operar en "modo seguro"
para no afectar a la disponibilidad
del servicio ni a las funcionalidades empresariales
dentro de la organización propietaria del ToE.

### Fase de notificación

Una vez los problemas de seguridad ToE
son identificados y explotados, se reportan.
Los *pentesters* deben entregar hallazgos detallados
tras su interpretación y análisis adecuados.
El informe incluye las ubicaciones y el alcance evaluados,
las metodologías de detección,
las vulnerabilidades identificadas,
las metodologías e intentos
de explotación de las vulnerabilidades
(el trabajo de los *pentesters* debe ser reproducible)
y las recomendaciones de remediación.
Gracias a estas últimas, el propietario,
quien debería disponer de un informe claro
de su exposición al riesgo,
puede tomar medidas correctoras
para mejorar la seguridad de su sistema evaluado.

## Solución *pentesting* de Fluid Attacks

En Fluid Attacks,
las pruebas de penetración son realizadas
por grupos de *pentesters* o *hackers* éticos experimentados
y altamente certificados.
Nuestro *red team* cuenta con miembros con diversas habilidades
que asumen diferentes roles dentro de las pruebas.
Así, por ejemplo,
lo que puede escapar a un grupo puede ser descubierto
por otro desde una perspectiva diferente.
Utilizamos nuestras propias herramientas
de análisis de seguridad para las pruebas
de tipo [SAST](../../producto/sast/), [DAST](../../producto/dast/)
y [SCA](../../producto/sca/).
Sin embargo,
como ocurrió el año pasado
y destacamos en nuestro informe [State of Attacks](https://fluidattacks.docsend.com/view/d7wbu584digrbe9a),
la detección de vulnerabilidades de severidad crítica
puede depender enteramente del trabajo manual de nuestros *pentesters*.

En nuestra solución de *pentesting*,
comprobamos, desde la perspectiva de un atacante,
la seguridad de tus aplicaciones web y móviles,
infraestructuras en la nube,
redes y dispositivos IoT,
entre otros muchos sistemas de información.
En nuestra [plataforma](https://app.fluidattacks.com/),
recibes informes continuos de nuestros hallazgos
con tasas mínimas de falsos positivos y falsos negativos.
Allí obtienes resultados detallados, incluyendo pruebas.
Puedes asignar vulnerabilidades para su remediación a tu personal,
solicitar tantos reataques como sean necesarios
y contar además con el apoyo de nuestros *pentesters*.

El [*pentesting*](../../soluciones/pruebas-penetracion-servicio/)
es una evaluación integral realizada por expertos
que no puede ser reemplazada por el escaneo de vulnerabilidades.
(Si quieres dar el primer paso en seguridad probando
un escaneo con nuestro *software* de escaneo de vulnerabilidades,
solicita nuestra [prueba gratuita de 21 días](https://app.fluidattacks.com/SignUp)).
Es sorprendente la cantidad de personas
que creen que ambas cosas son lo mismo.
Pero sí que hay estafadores en el mercado,
y pueden decirte que hacen pruebas de penetración cuando,
en realidad, lo que aplican es escaneo de vulnerabilidades.
No te dejes engañar.
Si quieres más detalles sobre en qué basar tu elección
del proveedor de *pentesting* adecuado para tu organización,
lee nuestro artículo "[Choosing the Right Pentesting Team](../../../blog/choosing-pentesting-team/)".
Si estás interesado en conocer el modelo PTaaS
(que implica pruebas de penetración continuas)
y sus beneficios para tu empresa,
te invitamos a leer "[Pentesting como servicio](../que-es-ptaas/)".
¿Alguna duda? No dudes en [contactarnos](../../contactanos/).
