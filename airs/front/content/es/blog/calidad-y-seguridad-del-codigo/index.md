---
slug: blog/calidad-y-seguridad-del-codigo/
title: Seguridad como parte de la calidad del código
date: 2022-11-30
subtitle: Abre la puerta a la seguridad como requisito de calidad
category: filosofía
tags: ciberseguridad, pruebas de seguridad, software, empresa, código
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1669830093/blog/code-quality-and-security/cover_code_quality_security.webp
alt: Foto por Dima Pechurin en Unsplash
description: Descubre qué suele entenderse como calidad del código, por qué creemos que este concepto debe incluir la seguridad y algunas recomendaciones para desarrollar código de alta calidad.
keywords: Calidad Y Seguridad Del Codigo, Calidad Del Codigo, Revision Manual De Codigo, Revision Automatizada De Codigo, Revision Segura De Codigo, Estandares, Requisitos, Hacking Etico, Pentesting
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/JUbjYFvCv00
---

Los conceptos de "calidad del código" y "seguridad"
en el desarrollo de *software* parecen no estar tan asociados
como podríamos creer y como debería ser.
La calidad del código debería verse como una medida que incluye
la seguridad entre sus propiedades fundamentales.
La seguridad es una variable que, en caso de mostrar deficiencias,
puede afectar no solo a la privacidad de la información sensible
sino también a la integridad y funcionalidad del producto
o aplicación y a la experiencia del usuario.
En este artículo explicamos qué se entiende habitualmente
por calidad del código y por qué creemos que este concepto
debería incluir la seguridad como una de sus propiedades o requisitos.
También te damos un par de recomendaciones
sobre cómo desarrollar código de alta calidad.

## ¿Qué es la calidad del código?

Cuando buscamos la palabra "calidad" [en el diccionario](https://www.oxfordlearnersdictionaries.com/us/definition/english/quality_1?q=quality),
esta es definida como qué tan bueno o malo es algo.
Por tanto, cuando hablamos de "calidad del código",
nos referimos a una métrica para calificar qué tan bueno
o malo es un conjunto de instrucciones de *software*.
Pero ¿cuán bueno o malo según qué criterios?
Generalmente, la base está en una serie de características o atributos.
Sin embargo, como puede ocurrir, por ejemplo,
con la clasificación del comportamiento humano desde una perspectiva moral,
la calidad del código no es una métrica definitiva
con parámetros objetivos y siempre compartidos,
y está abierta al debate. Depende de la subjetividad,
de lo que las industrias y organizaciones definan a partir de sus necesidades,
requisitos y enfoques específicos.
La calidad del código, por ejemplo,
no necesariamente será vista de la misma manera por quienes construyen
simples juegos para celulares y quienes desarrollan programas
que controlan la maquinaria de enormes empresas eléctricas,
ambos *software* con una criticidad tan distinta.

Aun así,
algunas ideas específicas parecen ser compartidas
por casi todos los proyectos de *software*
que permiten la distinción entre el código de buena y el de mala calidad.
El código de mala o escasa calidad puede carecer de coherencia
y consistencia en el manejo de las convenciones
y estar lleno de errores y complejidad.
Por el contrario,
el código de buena calidad suele considerarse sencillo,
sin errores, bien documentado
y que cumple con la función prevista para sus usuarios finales.
A continuación
se enumeran algunas de esas propiedades clave compartidas
para calificar la calidad del código:

- **Confiabilidad**: Mide la probabilidad
  de que el *software* funcione sin fallas,
  cumpliendo sus propósitos durante un periodo específico.
  Esta propiedad depende del número de errores presentes en el código.

- **Solidez**: Mide la capacidad del *software* para hacer frente
  a comportamientos extraños del usuario y otras condiciones mediante
  mensajes de error comprensibles.
  Esta propiedad está relacionada con la susceptibilidad reducida
  a errores ocultos o a la introducción de nuevos errores.

- **“Testabilidad”**: Mide lo bien que el *software*
  soporta el uso de pruebas que pueden emplearse para, por ejemplo,
  verificar ciertos comportamientos o detectar fallas en su funcionalidad.

- **Legibilidad**: Mide lo legible y comprensible
  que es el código del *software* no solo para sus autores originales,
  sino también para quienes pretendan revisarlo y editarlo.
  Depende del uso de comentarios, notaciones, sangría,
  documentación y simplicidad, entre otras cosas.

- **Mantenibilidad**: Mide la facilidad con la que se puede hacer
  mantenimiento al *software*, es decir, repararlo, actualizarlo y mejorarlo.
  Esta propiedad depende de la estructura, el tamaño,
  la consistencia y la complejidad del código.

- **Portabilidad**: Mide lo utilizable que es el *software*
  en diferentes dispositivos, plataformas u otros ambientes.
  En otras palabras,
  mide la facilidad con que puede transferirse de un medio a otro,
  dependiendo del número de modificaciones necesarias.

- **Reusabilidad**: Mide hasta qué punto las piezas
  de código o los activos del *software* pueden ser replicados
  o reutilizados (incluso para construir sobre ellos)
  por los desarrolladores de otros proyectos o programas.

Hasta aquí, lo problemático es que,
tras recorrer la web y consultar diversas fuentes,
no solemos encontrar ninguna referencia a la seguridad o,
al menos, no se sugiere explícitamente entre estas propiedades clave.

## ¿Qué pasa con la seguridad?

La calidad del código suele asociarse a su rendimiento
y a la experiencia de los usuarios finales
y los desarrolladores que trabajan en él.
Sin embargo,
¿no debería medirse también la calidad del código por su seguridad?
Por otro lado,
¿no depende también la seguridad de lo que llamamos calidad?
Hace casi una década,
un grupo de investigadores compartió las siguientes palabras en
[un estudio](https://resources.sei.cmu.edu/asset_files/TechnicalNote/2014_004_001_428597.pdf)
para el *Software Engineering Institute*:

<quote-box>

Muchas de las enumeraciones de debilidades comunes
(Common Weakness Enumerations, CWE),
como el uso inadecuado de constructos de lenguaje de programación,
los desbordamientos de búfer y las fallas en la validación
de los valores de entrada o *inputs*,
pueden asociarse a prácticas de codificación y desarrollo de baja calidad.
Mejorar la calidad es una condición necesaria para resolver
algunos problemas de seguridad de *software*.

</quote-box>

Los fragmentos de código de mala calidad,
incluso los más pequeños errores,
resultantes de prácticas de codificación inadecuadas,
dan lugar a debilidades y vulnerabilidades de seguridad
que pueden ser explotadas por *hackers* maliciosos
y generar impactos negativos sustanciales en las organizaciones
y los usuarios finales.
Así pues, la seguridad depende de la calidad del producto.
Pero se trata de una calidad diferente que no solo implica propiedades
como las enumeradas anteriormente.
En esta "calidad mejorada del código" se introducen estándares y prácticas que,
de no cumplirse, nos llevan a hablar de inseguridad,
del mismo modo que el incumplimiento
de otros requisitos podría llevarnos a hablar,
por ejemplo, de falta de confiabilidad o de baja legibilidad.
Al introducir estos nuevos estándares,
hemos incluido la seguridad como una propiedad más en la lista anterior.
Por consiguiente, a partir de ahora,
en nuestro discurso, la seguridad del código se tomará
como un factor que interviene en la determinación
de la calidad del código.

Las vulnerabilidades de seguridad en el código
ponen en peligro no solo el valioso rendimiento del *software*,
sino también la privacidad de los datos sensibles
en caso de que se procesen en él.
La no inclusión de la seguridad en el concepto frecuente de calidad
de código puede estar relacionada en parte con esto último.
En el pasado, a diferencia de hoy en día,
abundaban más los productos de *software* que no implicaban
el uso de datos sensibles de la organización
y sus usuarios y clientes o el contacto con amenazas externas.
En aquella época, había una preocupación más importante
por propiedades como la “testabilidad”,
la comprobabilidad y la mantenibilidad.
Hoy, sin embargo, en nuestra economía,
hay mucha más dependencia de Internet y de las aplicaciones web
y móviles que transfieren datos sensibles,
así como un mayor número de dispositivos IoT e interconexiones.
Por eso es cada vez más necesario que la seguridad aparezca también
como una propiedad clave en relación con la calidad del código.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/revision-codigo-fuente/"
title="Inicia ahora con la solución de revisión de código seguro
de Fluid Attacks"
/>
</div>

## Desarrollo de código de alta calidad

Presión para salir a producción rápidamente
con productos de *software* innovadores y competitivos.
Esta carga de trabajo con plazos ajustados es una de las causas habituales
del desarrollo de código de baja calidad.
Aunque alguien podría decir que cada vez es más raro
que no se solucionen fallas de funcionalidad o confiabilidad,
es seguro que muchos desarrolladores siguen ignorando
los estándares de calidad.
Esto es evidente en el caso de los estándares que incorporan la seguridad.
Si algunos profesionales no prestan suficiente atención
a la funcionalidad de lo que construyen,
lo más probable es que presten menos atención a su seguridad.

Peor aún es cuando quienes dejan de lado los estándares
o no promueven su uso son los mismos líderes
o jefes de área de las organizaciones,
centrados principalmente en solicitar rapidez.
Actualmente, la calidad del código, incluida la seguridad,
debe ser vista como una prioridad, empezando por los líderes.
El tiempo invertido en formación sobre estándares,
revisión de código y corrección de errores suele compensarse
con los gastos que se evitan en el futuro.
Cuando no se abordan adecuadamente,
las deficiencias del código pueden acumularse.
Y solucionarlas puede resultar aún más complejo y costoso.
Un código de alta calidad reduce entonces la llamada
"[deuda técnica](https://es.wikipedia.org/wiki/Deuda_t%C3%A9cnica)."

### Consejos para un código de alta calidad

Para desarrollar código limpio o de alta calidad
(incluida la seguridad, tengámoslo en cuenta)
y evitar los llamados "olores de código"
o código de mala calidad y vulnerabilidades de seguridad,
las directrices, convenciones o estándares son imprescindibles.
Estas proporcionan uniformidad,
coherencia entre el trabajo de los distintos miembros del equipo
y estabilidad en el producto.
La calidad del código y
las [buenas prácticas de codificación segura](../practicas-codificacion-segura/)
asociadas a estos estándares son compartidas por diversas comunidades
y pueden difundirse y enseñarse entre equipos colaborativos
de desarrolladores o ingenieros.
Individuos con diferentes especialidades,
habilidades y niveles de experiencia,
incluidos los frecuentemente mencionados
[campeones de seguridad](../../../blog/secdevops-security-champions/),
pueden fomentar un desarrollo seguro y de alta calidad entre sus colegas.

Para garantizar que el producto desarrollado sea de alta calidad,
también es necesario recurrir a las revisiones de código entre pares
o [manuales](../revision-manual-de-codigo/).
Los desarrolladores pueden ver estas evaluaciones
como tareas tediosas y que llevan mucho tiempo,
incluso como obstáculos cuando deberían considerarse objetivos esenciales.
Cuando no son los compañeros y líderes de los desarrolladores,
los proveedores externos pueden encargarse de estas revisiones
para identificar fallas o deficiencias de calidad en el código,
de modo que puedan remediarse con prontitud.
Estas revisiones no deberían dejarse para las fases finales
de los ciclos de vida de desarrollo.
Cuanto antes se detecten los errores, más fácil,
rápida y barata será su remediación.
También es aconsejable que se realicen de forma continua
para que vayan a la par con el trabajo constante de los desarrolladores.

Las revisiones manuales de código deben contar con
la ayuda de herramientas automatizadas,
como las de análisis estático,
principalmente por su amplia cobertura y rapidez,
aunque sus capacidades de detección y precisión son limitadas.
Gracias a estas herramientas,
los revisores expertos pueden centrarse en identificar errores
y problemas de seguridad más complejos
y, a veces, más severos, que quedan fuera del alcance de estas máquinas.
En general, una revisión integral, con humanos expertos
y herramientas automatizadas,
debe tener en cuenta el cumplimiento de propiedades
como las expuestas en este artículo,
junto con [requisitos de seguridad](https://help.fluidattacks.com/portal/en/kb/criteria/requirements)
como los que hemos recopilado y manejamos actualmente en Fluid Attacks.

Estas revisiones también pueden mejorarse cuando se incorporan nuevas técnicas.
Es por ello que en Fluid Attacks,
con nuestra especialidad en pruebas de seguridad,
vamos más allá de la [revisión de código seguro](../revision-codigo-fuente/),
la cual [es solo una parte](../../soluciones/revision-codigo-fuente/)
de nuestro servicio integral [Hacking Continuo](../../servicios/hacking-continuo/).
En este servicio,
realizamos revisiones exhaustivas con técnicas
como [SAST](../../producto/sast/),
[SCA](../../producto/sca/),
[DAST](../../producto/dast/),
[*pentesting* manual](../../soluciones/pruebas-penetracion-servicio/)
e [ingeniería inversa](../../producto/re/).
Además de utilizar nuestras herramientas automatizadas,
Hacking Continuo cuenta con la intervención indispensable
de nuestros *pentesters*,
que piensan y actúan como actores de amenazas,
anticipan riesgos y puntos de entrada
y simulan diferentes escenarios de ataque.
En Fluid Attacks,
contribuimos desde el campo de la seguridad para que tu código
y aplicaciones sean de alta calidad
y mantengan su exposición al riesgo lo más baja posible.

Te invitamos a probar de forma [gratuita durante 21 días](https://app.fluidattacks.com/SignUp)
nuestras evaluaciones a través de nuestras máquinas automatizadas
como introducción al servicio integral que puedes recibir
cuando decidas convertirte en uno de nuestros clientes.
¡[Contáctanos](../../contactanos/)!
