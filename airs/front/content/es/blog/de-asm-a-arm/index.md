---
slug: blog/de-asm-a-arm/
title: De ASM a ARM
date: 2022-11-16
subtitle: Nos adherimos al concepto de gestión de resistencia a ataques
category: filosofía
tags: ciberseguridad, empresa, pruebas de seguridad, hacking, tendencia
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1668649683/blog/from-asm-to-arm/cover_from_asm_to_arm.webp
alt: Foto por Alexander Nikitenko en Unsplash
description: Este artículo del blog explica los conceptos de "gestión de la superficie de ataque" y "gestión de la resistencia a ataques" y nuestra transición de uno a otro.
keywords: Superficie De Ataque, Gestion De La Superficie De Ataque, Asm, Gestion De La Resistencia A Ataques, Arm, Gestion De Vulnerabilidades, Gestion De Exposicion, Hacking Etico, Pentesting
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/H6obC_biCSk
---

En los últimos años,
el concepto de "**gestión de la superficie de ataque**"
(ASM, por su nombre en inglés)
se ha popularizado entre los proveedores de ciberseguridad.
Sin embargo,
hace poco nos dimos cuenta de que lo que suele englobar
este concepto no se ajusta del todo a lo que ofrecemos en la compañía.
De hecho, se queda corto.
Lo que diversas empresas pretenden ofrecer actualmente
en el mercado con la ASM está lejos de lo que nosotros pretendemos.
Nuestra contribución a la ciberseguridad es más profunda.
Por eso hemos decidido adoptar un concepto más nuevo:
"**gestión de la resistencia a ataques**" (ARM, por su nombre en inglés).
La ARM nos permite generar una diferenciación más precisa y
evitar malentendidos.
Abarca mejor lo que hacemos y lo que ofrecemos.
En este artículo te ayudamos a comprender mejor los conceptos ASM y ARM
y nuestra transición de uno a otro.

## Gestión de la superficie de ataque

La superficie de ataque suele referirse
a todos los activos informáticos de una organización
que un atacante podría identificar, a los que podría tener acceso,
explotar y afectar de algún modo.
Independientemente de que muchos de ellos no se consideren vulnerables,
potencialmente lo son.
Por lo tanto,
es preferible considerar la superficie de ataque como absolutamente
todos los activos digitales en relación con la organización.
Algunos de estos activos son internos y otros externos.
Es decir, algunos están dentro de las instalaciones
y la red de la organización
(p. ej., ordenadores y otros dispositivos utilizados por los empleados)
y otros son activos públicos u orientados a la Internet
(p. ej., aplicaciones y sitios web públicos).

La ASM es una solución orientada principalmente a escanear
y supervisar todos estos activos para enumerarlos en favor de su seguridad.
En algunos casos,
la ASM puede centrarse únicamente en la enumeración
de activos internos o externos.
En el informe Innovation Insight for Attack Surface Management de Gartner,
estos dos procesos parecen corresponder a la "gestión
de la superficie de ataque de los ciberactivos"
(CAASM, por su nombre en inglés)
y a la "gestión de la superficie de ataque externa"
(EASM, por su nombre en inglés), respectivamente.
Aunque se refieren a ellos como dos de las tres capacidades de la ASM,
no es el objetivo de este artículo entrar en su categorización.
Lo cierto es que la EASM es la más famosa en la actualidad.
Esta busca dar respuesta a una creciente necesidad de seguridad
ante una mayor exposición al riesgo debido
al mayor uso de superficies externas por parte de las organizaciones.

Hoy en día,
resulta evidente que muchas organizaciones no consiguen
una percepción completa de su superficie de ataque.
Su perímetro no suele estar muy bien definido.
Muchos de sus activos se encuentran dispersos
en diferentes ubicaciones fuera de los límites de su red protegida.
Muchos de ellos son a menudo completamente desconocidos
para las organizaciones.
Algunos activos están inactivos, obsoletos o simplemente descuidados,
sin controles de seguridad,
supervisión o mantenimiento por parte de las organizaciones.
(A veces, por ejemplo, se cree erróneamente que son implícitamente seguros,
como las cadenas de suministro o los recursos en la nube).
Algunos pueden incluso tener fines,
conexiones y propietarios desconocidos.
Por ejemplo,
la llamada "shadow IT" implica que
el personal de una organización adquiere
y utiliza activos sin informar primero a los administradores
o responsables de la seguridad ni recibir su aprobación.

Más allá de la enumeración,
la ASM suele tener como objetivo reportar
y cuantificar los riesgos asociados a los activos identificados,
aparentemente desde la perspectiva de los delincuentes.
(De hecho, esta parece ser la tercera y última capacidad
de la ASM en el informe de Gartner:
"servicios de protección contra riesgos digitales"
(DRPS, por su nombre en inglés)).
Estos riesgos pueden deberse a errores de configuración,
vulnerabilidades y datos expuestos, entre otras cosas,
dentro de los activos y sus interacciones.
Una vez notificados los riesgos,
la ASM trata de priorizarlos para su pronta mitigación
(incluida la eliminación y aplicación de parches a los activos)
por parte de los equipos encargados.
La priorización es fundamental para gestionar adecuadamente
los recursos de una organización, como el tiempo, el esfuerzo y el dinero.

Todo parece perfecto con la ASM,
pero ¿es suficiente para el plan de ciberseguridad de una organización?
Uno de los problemas de la ASM es su limitación al escaneo,
a la simple intervención de herramientas automatizadas que supuestamente
"suplantan" a los atacantes.
Debemos reconocer que la práctica de reconocimiento
de los atacantes no se limita necesariamente
al uso de herramientas automatizadas.
De ahí la necesidad de la intervención humana con procedimientos manuales.
Otra dificultad de la ASM es que, en algunos casos,
se limita a ambientes externos o internos.
Esto deja importantes lagunas de reconocimiento o puntos ciegos
para la organización que solicita el servicio.
[Como afirma Oltsik en CSO](https://www.csoonline.com/article/3648998/look-for-attack-surface-management-to-go-mainstream-in-2022.html),
la categoría emergente de la ASM
se centra únicamente en los activos orientados a la Internet.

Una organización necesita visibilidad de extremo
a extremo de su superficie de ataque
y descubrir todos los activos e interacciones dentro de su ecosistema de TI.
Una ASM profunda no se limita a mirar a nivel de URL,
sino que también enumera la superficie interna.
Una ASM extensa debería enumerar tecnologías como puertos,
servidores, sistemas operativos, dispositivos móviles y de IoT,
componentes de terceros (incluidos los de código abierto),
APIs, microservicios, contenedores, IaC, SaaS, recursos en la nube,
así como datos como direcciones IP, credenciales y código expuesto,
interacciones con socios y terceros
(incluidos los proveedores de la cadena de suministro) y mucho más.
Los ambientes internos y externos son dinámicos
y cambian constantemente, es decir, crecen,
se complejizan y se dispersan con la adición de servicios,
interacciones, _software_, usuarios y dispositivos.
Por lo tanto,
otro problema de la ASM es que a menudo no es un proceso continuo,
con una supervisión permanente de los cambios
en la superficie de ataque a lo largo del tiempo.

Por supuesto,
los proveedores pueden resolver los problemas mencionados
optimizando las soluciones de ASM.
Sin embargo,
procesos tan relevantes en ciberseguridad como
la "[gestión de vulnerabilidades](../que-es-gestion-de-vulnerabilidades/),"
por ejemplo, con la clasificación y priorización de vulnerabilidades
en objetivos específicos de evaluación,
quedan a veces fuera de la oferta de ASM.
De hecho,
Gartner menciona la gestión de vulnerabilidades
como uno de los tres pilares de la llamada "gestión de exposición",
siendo los otros dos la ASM y el componente de "validación".
Sin entrar en sus clasificaciones,
lo que podemos decir es que la ASM siempre necesita complementarse.
Como enfoque único de la ciberseguridad,
puede resultar insuficiente.

Las organizaciones deberían reconocer que sus planes
de ciberseguridad no solo deben enfocarse en la identificación
de sus activos y los riesgos que teóricamente pueden conllevar,
sino también en la práctica de ataques simulados
y la evaluación de sus impactos.
Con la ASM es posible hacerse una idea de cómo
luce la organización evaluada para los atacantes,
qué formas de ataque podrían emplear
y cuáles son algunos de los posibles riesgos.
Con la inclusión de la gestión de vulnerabilidades
es posible responder a preguntas como cuáles son esas vulnerabilidades
de seguridad presentes en la superficie de ataque de la organización,
cuáles son esos activos con vulnerabilidades con más probabilidades
de ser explotadas por actores de amenazas
y cómo puede la organización conseguir remediarlos.
Pero,
¿qué ocurriría si un _hacker_ malicioso
lanzara un ataque contra la organización?
¿Cómo respondería, cuál sería el impacto, y, sería capaz de resistirlo?

## Gestión de la resistencia a ataques

La **ARM** entra entonces en escena.
Además de tratar de enumerar los recursos,
mejorar el conocimiento de una organización
sobre su superficie de ataque,
calificar sus riesgos e incluso ayudar a remediar los problemas
de seguridad detectados en ella,
esta solución es clara al afirmar que ayuda
a aumentar la resistencia de una organización a los ciberataques.
ARM trata de responder a las preguntas:
¿cómo funcionan y cómo deberían funcionar los controles de prevención,
detección y respuesta ante ataques de una organización?
Con esta solución,
una organización puede gestionar y supervisar no solo la superficie de ataque,
sus vulnerabilidades y la exposición al riesgo,
sino también sus pruebas de seguridad.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/pruebas-seguridad/"
title="Inicia ahora las pruebas de seguridad de Fluid Attacks"
/>
</div>

Básicamente,
este nuevo concepto engloba una ASM que,
como decíamos, debe ser integral y constante para estar
al tanto de todos los activos y cambios en los ambientes locales
y externos de una organización.
A esta ASM,
la ARM añade **pruebas de seguridad continuas**,
preferiblemente desde el principio del SDLC,
que también se mantienen al día con las actualizaciones
en la organización evaluada.
En otras palabras,
los activos detectados significan activos añadidos al alcance de las pruebas.
Estas pruebas de seguridad en la ARM
van más allá del uso de herramientas automatizadas,
involucrando el trabajo de _hackers_ éticos
altamente certificados y experimentados.
Estos _hackers_ tienen como objetivo evaluar
todos los vectores de ataque posibles,
adaptándose a los contextos específicos
y a la lógica de negocio de cada organización.

Así, en la ARM,
existe una forma innegable de ver cómo un actor de amenazas
percibe toda la superficie de ataque de una organización
y puede actuar sobre ella.
Siguiendo las tácticas,
técnicas y procedimientos empleados por los delincuentes,
los _hackers_ éticos reconocen los activos
(tanto conocidos como desconocidos para la organización evaluada),
identifican vulnerabilidades en ellos y planifican
y realizan ataques que permiten medir con precisión la exposición al riesgo.
Recordemos por enésima vez que las herramientas automatizadas,
aunque más rápidas y baratas,
se centran únicamente en la detección de vulnerabilidades comunes y conocidas.
Para encontrar los problemas de seguridad
que quedan fuera de las bases de datos de las herramientas,
es decir, aquellos que desconocen,
a menudo más complejos y de mayor severidad, y,
por supuesto, para explotarlos, están los _hackers_ éticos.

En definitiva,
ayudar a tu organización a conocer toda su superficie de ataque,
identificar sus puntos débiles en materia de seguridad
y poner a prueba su respuesta y resistencia a ataques reales
para favorecer luego la mitigación de los riesgos
es lo que pretendemos en Fluid Attacks.
Reafirmando esto,
consecuentemente decidimos cambiar de ASM a ARM en nuestro discurso público.

## La plataforma de Fluid Attacks

En Fluid Attacks te ofrecemos
[Hacking Continuo](../../servicios/hacking-continuo/)
integral con nuestras herramientas automatizadas
y _hackers_ éticos que generan informes en un único lugar:
nuestra [plataforma](https://app.fluidattacks.com/).
En esta plataforma,
dispones no solo de un registro constante de la superficie
de ataque identificada y evaluada de tu organización,
sino también de las vulnerabilidades detectadas en ella
a lo largo del tiempo y de la exposición al riesgo de tu organización.
Allí recibes amplios detalles sobre nuestros hallazgos
que te facilitan la priorización de vulnerabilidades para su remediación.
A partir de la postura de resistencia a ataques,
te entregamos pruebas de la explotación
y el impacto de nuestros ataques simulados y controlados.
También ofrecemos recomendaciones a tu equipo de desarrolladores
para la tarea de remediación,
con la esperanza de que también sirvan de retroalimentación
para sus futuras prácticas de desarrollo.
De hecho, en nuestra plataforma,
puedes asignar procesos de remediación específicos a cada individuo.
De forma ilimitada, puedes solicitar
a nuestros _hackers_ que verifiquen la eficacia
de la remediación mediante reataques.
Y, si es necesario,
puedes solicitarles consultoría o asesoramiento.
También puedes utilizar nuestro agente DevSecOps
para validar el cumplimiento de las políticas de tu organización
antes de desplegar el _software_ a producción.
Puedes hacer un seguimiento de tus progresos en términos de remediación
y mitigación de la exposición al riesgo en tus productos,
así como garantizar el cumplimiento de los requisitos de seguridad
basados en estándares internacionales.

Si estás interesado en probar nuestra plataforma gratuitamente,
te ofrecemos nuestra [prueba gratuita de 21 días](https://app.fluidattacks.com/SignUp)
en la que podrás disfrutar de pruebas de seguridad continuas
con nuestras herramientas automatizadas (plan Essential).
Si, por el contrario,
quieres beneficiarte de inmediato del Hacking Continuo integral
con la intervención de nuestras herramientas
y nuestro [_red team_](../ejercicio-red-teaming/) de _hackers_ éticos
(plan Advanced),
[contáctanos](../../contactanos/).
