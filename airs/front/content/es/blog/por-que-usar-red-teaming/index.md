---
slug: blog/por-que-usar-red-teaming/
title: ¿Por qué motivo usar red teaming?
date: 2023-04-27
subtitle: Reconoce el valor de este método de evaluación de seguridad
category: filosofía
tags: ciberseguridad, red-team, hacking, pruebas de seguridad, empresa, blue-team
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1682619913/blog/why-apply-red-teaming/cover_why_apply_red_teaming.webp
alt: Foto por charlesdeluvio en Unsplash
description: Nos basamos en una pregunta de Tribe of Hackers Red Team (2019) orientada a explicar el valor del red teaming a quienes se muestran reacios a él o lo desconocen.
keywords: Por Que Usar Red Teaming, Que Es Un Red Team, Que Es Red Teaming, Red Team En Ciberseguridad, Definicion Red Teaming, Tribu De Hackers, Pentesting, Hacking Etico
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/DziZIYOGAHc
---

¿Te resistes a utilizar *red teaming*?
¿No sabes qué es y cuál es su aporte valioso
a la ciberseguridad de tu organización?

Este artículo lo construimos a partir
del libro *[Tribe of Hackers Red Team](https://www.amazon.com/gp/product/B07VWHCQMR/ref=dbs_a_def_rwt_bibl_vppi_i2)*
 de Carey y Jin (2019).
Quisimos tomar como principio fundamental precisamente
una de las muchas preguntas que allí se hacen a varios expertos
en *red teaming*: **¿Cómo explicar el valor del *red teaming*
a un cliente u organización reticente o sin conocimientos técnicos?**
Habiendo escrito ya artículos en el blog sobre
las opiniones de cinco profesionales que aparecen en ese libro
(me refiero a [Carey](../tribu-de-hackers-1/),
[Donnelly](../tribu-de-hackers-2/),
[Weidman](../tribu-de-hackers-3/),
[Secor](../tribu-de-hackers-4/),
y [Perez](../tribu-de-hackers-5/)),
descartamos aquí sus respuestas a esa pregunta.
Y lo mismo hicimos con otras que no nos parecieron
suficientemente sustanciosas o pertinentes.
En definitiva, utilizamos las respuestas de 33 expertos.
Esperamos que lo que hemos reunido aquí te ayude a cambiar de opinión
o a ampliar tus horizontes sobre el *red teaming*,
especialmente si tus respuestas a las preguntas
que planteamos al principio fueron afirmativas.

## ¿Está tu organización preparada?

Convencer a una organización del valor del *red teaming*
y persuadirla para que lo implemente en sus programas
de ciberseguridad puede ser un reto engorroso para un proveedor de servicios.
Puede que sea mejor no intentar conseguirlo, al menos no directamente.
Lo ideal es que las organizaciones busquen y recurran a los *red teams*.
Pero si sigue habiendo reticencia
o negligencia con las medidas básicas de prevención,
¿qué podríamos esperar en cuanto a una estrategia más avanzada como esta?
Una organización consciente de la seguridad se informaría al respecto,
principalmente mientras se encuentra en un proceso deliberado de maduración.
Al ser madura en materia de seguridad, recurriría a un *red team*
y a sus servicios sin dilación.
Sin embargo, uno de los obstáculos más comunes es el estancamiento.

Una organización puede no estar preparada y,
por tanto, no querer recurrir al *red teaming* cuando
el pico de su madurez ha sido hasta el momento
la [evaluación de vulnerabilidades](../evaluacion-de-vulnerabilidades/)
o la [gestión de vulnerabilidades](../que-es-gestion-de-vulnerabilidades/).
Algunas incluso llegan a ese punto
confiando exclusivamente en herramientas automatizadas.
¿Aún no han considerado enfoques como
la [gestión de riesgos](../../../blog/cvssf-risk-exposure-metric/)
y la [gestión de la resistencia a ataques](../../../blog/from-asm-to-arm/)?
Quizá sea cierto lo que plantea el experto Chris Gates:
Lo más eficaz es no perder el tiempo;
sinceramente, si tienes que presionar mucho y tratar de convencer,
entonces la organización no va a contar con la mentalidad adecuada
para recibir o implementar un *red team*.

Sin embargo,
sí es posible contribuir a la educación
de las organizaciones en materia de ciberseguridad.
Y esto no debería verse como una pérdida de tiempo.
De hecho, es algo que, por ejemplo,
en Fluid Attacks nos gusta hacer a través de este y otros medios.
Dado que la contratación del *red teaming* acaba siendo una decisión comercial,
es sobre todo a los líderes o directores de las organizaciones
a quienes hay que llegar con mensajes
lo suficientemente claros sobre la importancia de esta solución.
Más aún cuando entre ellos no hay expertos en ciberseguridad
(algo que debería cambiar; [la SEC ya lo ha sugerido](../../../blog/sec-new-regulations/)).

Tal como sostiene Christopher Campbell,
el problema es que la mayoría de la gente no entiende
cuál es la misión principal de un *red team* y, en cambio,
lo compara con otros tipos de pruebas y evaluaciones.
(Si te encuentras entre los que están empezando a aprender sobre el tema,
te invitamos a leer nuestro artículo "[Red Teaming](../ejercicio-red-teaming/)")
Este método no pretende sustituir
al [escaneo de vulnerabilidades](../escaneo-de-vulnerabilidades/)
ni a las [pruebas de penetración](../que-es-prueba-de-penetracion-manual/),
sino *complementarlos* en una fase avanzada
del proceso de madurez de seguridad de una organización.
Los directivos de una organización pueden ser informados
sobre las diferentes características
y beneficios de cada una de estas soluciones
(véase, por ejemplo, [este artículo](../bas-v-pentesting-v-red-teaming/)).
Nosotros podemos contribuir a modificar esa visión que,
como comenta Jake Williams,
suele estar presente en algunos clientes potenciales pero reacios:
El *red teaming* es visto como un escaneo de vulnerabilidades
o un *pentesting* demasiado costoso.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/red-team/"
title="Inicia ahora con la solución de red teaming de Fluid Attacks"
/>
</div>

## ¿Estás tratando de entender las diferencias?

Desde las primeras etapas
del ciclo de vida de desarrollo de *software* (SDLC) de una organización,
los equipos de desarrollo pueden beneficiarse significativamente
de los escaneos y el *pentesting*.
Con técnicas diferentes, estos métodos abordan objetivos diversos.
Las vulnerabilidades conocidas y de día cero se identifican
en el código fuente y las operaciones de *software*,
especialmente en la fase de preproducción.
El *red teaming*, por el contrario,
se dirige más a una tecnología que está, posiblemente,
lista para sus usuarios finales e incluso a un conjunto de sistemas
o a una red en la que intervienen seres humanos.
Sin embargo, es cierto que el *red teaming* se centra más
en los objetivos a alcanzar que en los ámbitos específicos.
Quienes se benefician aquí
son todos los equipos relacionados con la seguridad,
como los de prevención, defensa y respuesta.

En consonancia con lo anterior,
tal como sugiere Brandon McCrillis,
el *red teaming* requiere la capacidad de combinar muchos aspectos
de las auditorías de seguridad tradicionales en un contrato
que traspasa los límites de simplemente "marcar la casilla de cumplimiento".
Y como bien señala Campbell, los *red teams* ponen a prueba
el funcionamiento conjunto de todas tus políticas
y procedimientos en el entorno de producción real,
donde hay personas reales.

En la frase anterior hay un elemento clave de la utilidad del *red teaming*:
la aproximación a la *realidad*. ¿Qué realidad?
La relativa a las ciberamenazas y los actores maliciosos.
Un *red team* se encarga de emular a los ciberdelincuentes,
utilizando sus recursos, técnicas, tácticas y procedimientos,
que podrían perjudicar a una organización.
Como especifica Paul Brager,
el *red teaming* permite a una organización
encontrar brechas potencialmente dañinas
o riesgosas en su postura de seguridad antes
de que los actores maliciosos las exploten,
minimizando el posible impacto en la reputación de la empresa,
los clientes y los accionistas.
Esto forma parte de la tan pregonada por nosotros postura preventiva.
Como plantea Tim MalcomVetter,
el trabajo de un *red team* es preparar a la compañía
ante la realidad de que un atacante puede obrar indebidamente
sin su conocimiento o consentimiento.

Con esta metodología,
a través de un equipo entrenado y supervisado
y con la aprobación previa de las directivas de la organización objetivo,
se trata de evaluar la eficacia
de todo el programa de seguridad de la organización
(prevención, detección, defensa y respuesta)
frente a los ataques, lo más cerca posible de la realidad.
Como expresa David Kennedy,
esto es lo más real que se puede
hacer sin que un adversario te ponga en peligro.
Idealmente, se trata de experimentar uno o más ataques
pero sin esperar a que se produzcan por parte de esos "adversarios reales".
Siguiendo con la analogía de combate utilizada por Chris Nickerson,
con el *red teaming*,
una organización se une a un club de la pelea para ver
lo que se siente realmente al estar en un combate.
Todo el sentido del *red teaming* es desafiar el *status quo*,
no a través de algún tipo de modelo teórico o matemático,
sino para aprender y evolucionar a través de la experiencia.

<div class="imgblock">

![Welcome to the fight club](https://res.cloudinary.com/fluid-attacks/image/upload/v1682619980/blog/why-apply-red-teaming/fight_club_red_teaming.webp)

<div class="title">

Bienvenido al club de la pelea. (Fuente: [memegenerator.com](https://img.memegenerator.net/instances/71384244.jpg).)

</div>

</div>

De forma controlada,
sin poner en riesgo a la organización objetivo,
esta puede enfrentarse a adversarios simulados
que actúan como sus oponentes reales (nada de atacantes hipotéticos).
Los equipos de la organización pueden así aprender de ellos y,
con esa experiencia, estar más preparados ante amenazas y golpes reales.
Volviendo a Nickerson,
este método te preparará para la inevitable lucha que te espera
y te dará confianza en tus habilidades.
También te señalará oportunidades en tu estrategia
que quizá nunca hayas puesto a prueba.
También familiarizado con la analogía de combate,
Robin Wood remata con lo siguiente:
Un boxeador que ha pasado muchas horas
en el cuadrilátero intercambiando golpes con un oponente
estará mucho mejor preparado para una pelea que aquel
que solo ha golpeado un saco de boxeo.

## ¿Encaja con las necesidades de tu organización?

Entonces,
el propósito del *red team* es evaluar las reacciones
reales de los sistemas tecnológicos y humanos de una organización
ante ataques que rozan la realidad.
Así pues, para ser realista,
el *red team* debe conocer y comprender los tipos de amenazas
a los que suele enfrentarse el sector
al que pertenece la organización objetivo.
También se debe hacer una idea de los ataques
e incidentes previos que ha sufrido
o los podría llegar a sufrir esa organización.
Dado que la priorización es una cuestión vital en ciberseguridad,
el *red team* puede averiguar qué es lo que más preocupa
a los dirigentes de la organización y qué activos consideran más importantes.
Como dice McCrillis,
el valor del *red teaming* es entender
y explotar los peores temores de una empresa.
Ya en su ejercicio ofensivo,
el *red team* puede llegar a comprometer cosas que inicialmente
fueron erróneamente infravaloradas por la organización.

El *red team* se enfoca en adaptarse al contexto de la organización objetivo
y en identificar aquellos múltiples problemas de seguridad que,
de ser explotados por criminales,
tendrían impactos significativos y comprometedores para la organización.
Por lo tanto,
el *red team* debe mostrar flexibilidad.
Como sostiene Stephanie Carruthers, normalmente,
las compañías se someten a pruebas de penetración realizadas
por un único consultor que suele tener un conjunto de conocimientos generales.
Un *red team* aporta un grupo de individuos
cuyos conjuntos de habilidades específicas
están alineados con la infraestructura de la empresa.
La formación de cada miembro del equipo debe determinar
su implicación o al menos su grado de participación
(ya que también pueden aprender)
en un proyecto específico con una organización.
Un grupo bien formado de expertos con diferentes conocimientos
y experiencia puede ayudar a descubrir incluso nuevas amenazas
y sugerir cuáles y cómo podrían mejorarse diversos sistemas
y estrategias de la organización.

## Te preocupa el dinero, ¿no?

Es bastante comprensible.
Es posible que muchas organizaciones se muestren reacias
a probar el *red teaming* simplemente por la cuestión del costo.
Puede que ellas ya hayan invertido en buenas prácticas de desarrollo,
herramientas y pruebas de seguridad
y programas de gestión de vulnerabilidades.
Pero podríamos hacerles un par de preguntas:
¿Han sido suficientes sus inversiones?
¿Sus capacidades de prevención, detección,
defensa y respuesta son de suficiente calidad?
Bradley Schaufenbuel sugiere que el *red teaming*
puede verse como una forma relativamente barata de determinar
si tus inversiones pasadas en personas,
procesos y tecnología están proporcionando los resultados esperados
y qué áreas de inversión futura proporcionarán
el mayor beneficio a la organización.

En esta cuestión de dinero,
el éxito de los ejercicios del *red team* puede ayudar a la organización
a comprender cuáles serían los costos
en caso de producirse incidentes específicos.
(Nótese que un buen ejercicio de *[red teaming](../ejercicio-red-teaming/)*
que no tenga "éxito" en alcanzar objetivos precisos
también cuenta como beneficio,
al revelar que los controles y procesos de seguridad
de la organización objetivo son de alta calidad).
Pueden estar en juego grandes cantidades de dinero.
Como dice Kevin Figueroa,
mostrar el costo si la organización se ve comprometida frente
al costo de realizar un trabajo de *red team*
puede cambiar la forma en que abordan la seguridad en una organización.
No esperes a experimentar pérdidas de dinero para entrar en razón.
Ten en cuenta lo que afirma Robert Willis:
Las organizaciones necesitan ser capaces de justificar
el gasto que se destina a todo lo relacionado con la ciberseguridad,
razón por la cual muchas organizaciones
tienen una postura de seguridad horrible.

## ¡Estamos aquí para ayudarte!

En Fluid Attacks,
donde además de *[red teaming](../../soluciones/red-team/)*,
ofrecemos [otras soluciones de ciberseguridad](../../soluciones/),
asumimos como nuestra responsabilidad contribuir
a eliminar la idea errónea de que un *red team* existe
para romper infraestructuras,
culpar a la gente de los problemas de seguridad,
proporcionar largos informes y desaparecer.
Un *red team* fiable
y certificado no solo está ahí para comprometer sistemas
y sacar a la luz brechas y fallas.
Su trabajo también debe incluir la retroalimentación
y la asistencia a las diferentes secciones de la organización objetivo
para ayudarles a mejorar sus programas de seguridad.

No dejes que tu organización
se atasque en el proceso de maduración de la ciberseguridad.
No te sumes a esas personas que, como dice Mary Sawyer,
solo se van a preocupar por la seguridad cuando se vean enfrentadas
con las consecuencias de hacer algo de forma insegura.
Es mejor que tu organización, con la ayuda de un *red team*,
como el de Fluid Attacks,
saque los trapos sucios al sol cuanto antes.
Sigue el consejo de Oddvar Moe:
No sería muy divertido descubrir que un atacante puede
hackear fácilmente nuestro sistema sin ser detectado
y que no se le detiene solo porque no evaluamos adecuadamente
la seguridad de nuestra organización.
[Contáctanos](../../contactanos/)
y empieza a mitigar la exposición al riesgo de tu organización ahora mismo.

<caution-box>

**Precaución:**
No olvides que puedes acceder a las entrevistas completas
con cada uno de los 47 expertos en *red teaming* en
el [libro de Carey y Jin](https://www.amazon.com/Tribe-Hackers-Red-Team-Cybersecurity/dp/1119643325).

</caution-box>
