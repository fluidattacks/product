---
slug: blog/pci-dss-v4-13-nuevos-requisitos/
title: 13 nuevos requisitos de PCI DSS en v4.0
date: 2024-01-12
subtitle: Cumple con los requisitos que entran en vigencia en marzo de 2024
category: política
tags: ciberseguridad, cumplimiento, empresa
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1705073420/blog/pci-dss-v4-new-13-requirements/cover_pci_dss.webp
alt: Foto por Jeremy Perkins en Unsplash
description: Las empresas que almacenan, manejan o transfieren datos de cuentas deberán cumplir con el estándar PCI DSS v4.0 a partir del 31 de marzo. Resumimos sus 13 requisitos nuevos que deben cumplirse para esa fecha.
keywords: Pci Dss, Roles Y Responsabilidades, Controles De Seguridad, Nuevos Requisitos, Cumplimiento Pci Dss, Revision Manual De Codigo, Desarrollar Software Seguro, Hacking Etico, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Escritor y editor
source: https://unsplash.com/photos/brown-deer-on-white-background-BKZawJTA5t4
---

Los nuevos requisitos para obtener la certificación PCI DSS v4.0
incluyen documentar, asignar y comprender
los roles y las responsabilidades
para implementar los controles de seguridad;
verificar el alcance de la evaluación PCI DSS una vez cada 12 meses;
realizar un análisis de riesgos específico
para cada requisito cumplido con el enfoque personalizado;
y respaldar las peticiones de información de los clientes
sobre la responsabilidad y el estado de cumplimiento de los requisitos PCI DSS.
Se espera que se cumplan estos requisitos dentro de un par de meses.

Te damos un poco de contexto.
Las entidades que almacenan, manejan
o transfieren información de titulares de tarjetas están obligadas
a cumplir los requisitos de seguridad
del [Estándar de Seguridad de Datos de Payment Card Industry](https://www.pcisecuritystandards.org/lang/es-es/)
(PCI DSS).
La versión 4.0 del estándar
plantea **64 requisitos técnicos y operativos nuevos**.
Se distribuyen entre los requisitos principales,
que son las 12 categorías cuyos nombres
nos proporcionan una visión general de alto nivel.
En la siguiente imagen puedes ver cómo han cambiado
sus títulos de la v3.2.1 a la v4.0.

<image-block>

!["Comparación entre requisitos PCI DSS v3.2.1 y v4.0"](https://res.cloudinary.com/fluid-attacks/image/upload/v1706642451/airs/es/blogs/im%C3%A1genes%20traducidas/pci-dss-v4-13-nuevos-requisitos/requisitos-pci-dss-comparacion-versiones-fluid-attacks.webp)

Cambios de título en los requisitos principales de PCI DSS de v3.2.1 a v4.0

</image-block>

Las compañías deben empezar a cumplir con **13** de
los nuevos requisitos lo antes posible,
ya que esta versión entrará en vigor el **31 de marzo de 2024**.
Los 51 restantes se consideran buenas prácticas
para empezar a seguir durante el año
y serán obligatorios a partir del 31 de marzo de 2025.

Las sanciones por incumplimiento pueden incluir multas significativas.
Estas oscilan entre 5.000 y 100.000 dólares por mes,
dependiendo del periodo de incumplimiento de la entidad
y del volumen de transacciones.
Además,
las partes interesadas pueden inhabilitar a la entidad
para aceptar pagos con tarjeta.
A ello se añade que el conocimiento público de estos acontecimientos
tendría un efecto negativo en la confianza de los clientes
y en la reputación de la entidad.
Teniendo esto en cuenta,
así como tu propósito de proteger los datos de tu empresa y de tus clientes,
veamos qué hay de nuevo.

## Documentar, asignar y comprender roles y responsabilidades

Diez de los 13 nuevos requisitos
piden **documentar, asignar y comprender los roles y responsabilidades**
para implementar los controles de seguridad.
En el [enfoque prioritario](https://www.pcisecuritystandards.org/lang/es-es/)
presentado formalmente por el estándar,
en caso de que esta sea tu primera vez,
se sugiere que el cumplimiento de este requisito
se deje para después de que se alcancen
otros más básicos en el requisito principal.

En resumen,
estas son nuestras versiones simplificadas de algunos
de los requisitos que tu equipo debe cumplir en primer lugar
y para los cuáles deben estar claros los roles
y las responsabilidades:

- **Construya y mantenga redes y sistemas protegidos:** Resolver
  los problemas de configuraciones riesgosas
  en cuentas predeterminadas de proveedores,
  funciones principales que requieren diferentes niveles de seguridad,
  funcionalidades innecesarias, entre otros.

- **Proteja los datos del tarjetahabiente:** Implementar políticas,
  procedimientos y procesos de retención y eliminación de datos
  de cuentas para mantener el almacenamiento al mínimo;
  utilizar criptografía robusta
  para mantener seguros los números de cuenta principales;
  tomar medidas para proteger las claves criptográficas; etc.

- **Mantenga un programa de gestión de vulnerabilidades:** Desplegar
  soluciones contra programas maliciosos;
  desarrollar *software* basado en los estándares de la industria
  y las buenas prácticas;
  [realizar análisis de código](../../producto/sast/)
  buscando vulnerabilidades y remediarlas antes de ponerlo en producción;
  si se hace la [revisión manual de código](../revision-manual-de-codigo/),
  que sea realizada por personas con amplios conocimientos
  sobre [técnicas de revisión de código](../revision-codigo-fuente/)
  y [prácticas de codificación segura](../practicas-codificacion-segura/); etc.

- **Implemente medidas sólidas de control de acceso:** Definir
  y asignar el acceso adecuadamente; utilizar un sistema de control de acceso;
  implementar MFA;
  utilizar criptografía robusta para hacer ilegibles
  todos los factores de autenticación;
  restringir el acceso físico a los sistemas mediante controles de entrada
  adecuados a las instalaciones; etc.

- **Monitorear y verificar las redes regularmente:** Habilitar,
  revisar y proteger los registros de auditoría
  (es decir, el registro cronológico de las actividades del sistema);
  realizar [escaneos de vulnerabilidad](../escaneo-de-vulnerabilidades/);
  efectuar [pruebas de penetración internas y externas](../tipos-de-pruebas-de-penetracion/)
  al menos una vez cada 12 meses; etc.

- **Mantenga una política de protección informática:** Documentar e implementar
  políticas de uso aceptable para tecnologías de usuario final;
  documentar y validar el alcance de la evaluación PCI DSS;
  monitorear el cumplimiento de PCI DSS
  de proveedores de servicios de terceros; etc.

<caution-box>

PCI DSS v4.0 introduce la categoría "datos de cuenta",
en vez de solo "datos del titular de la tarjeta",
para referirse al número de cuenta principal
(PAN, por sus siglas en inglés)
y a cualquier otro elemento de dos categorías:
datos del titular de la tarjeta
y datos confidenciales de autenticación.
Los primeros incluyen el PAN,
el nombre del titular de la tarjeta,
la fecha de caducidad y el código de servicio;
los últimos incluyen los datos de pista completos,
el código de verificación de la tarjeta y los PINs/bloques de PIN.

</caution-box>

Más adelante,
tal y como nosotros lo entendemos,
tu equipo puede trabajar en la documentación propiamente
dicha dentro de las políticas y procedimientos
o en un archivo específico.
Sin embargo,
el consejo de priorizar solo nos parece adecuado
en cuanto a crear la documentación,
ya que los roles y responsabilidades deben asignarse formalmente
y entenderse para que los controles
de seguridad en sí sean puestos en marcha.

Otro consejo que se da en el documento es la creación
de una matriz de asignación de responsabilidades (matriz RACI).
Se denomina así porque indica quién está a cargo,
quién es responsable,
quién es consultado y quién informado.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/revision-codigo-fuente/"
title="Inicia ahora con la solución de revisión de código seguro
de Fluid Attacks"
/>
</div>

## Lo nuevo para mantener una política de seguridad de la información

Los tres nuevos requisitos restantes que deben cumplirse en 2024,
están relacionados con la política
y los programas organizacionales
para respaldar la seguridad de la información.

De estos requisitos,
el que habría que cumplir primero es el
de **documentar y confirmar el alcance de la revisión de PCI DSS
al menos una vez cada 12 meses**.
Esto significa que tu equipo tiene que verificar que las ubicaciones
y los flujos de datos de cuentas
y los componentes del sistema
(p. ej., *software*, componentes en la nube, dispositivos de red)
que deben protegerse se añaden al alcance de la evaluación.

El consejo es crear una hoja de cálculo y registrar lo siguiente:

- dónde se almacenan los datos, por qué y durante cuánto tiempo;

- qué datos se almacenan (es decir, elementos del titular de la tarjeta
  y/o datos confidenciales de autenticación)

- cómo se protegen los datos;

- cómo se registra el acceso a los datos.

Además,
tu equipo debe identificar no solo sus sistemas y redes internos,
sino también todas las conexiones externas de terceros.

Otro nuevo requisito es
**realizar un análisis de riesgos específico para cada requisito**
cumplido con el enfoque personalizado.
Para ser claros,
las entidades que buscan la certificación
pueden seguir el enfoque definido o el personalizado.
Mientras que el enfoque definido significa aplicar
los controles de seguridad tal y como los describe el estándar,
el enfoque personalizado implica alcanzar los objetivos
de los requisitos sin necesariamente aplicar las tecnologías
o las operaciones exactamente enunciadas en el estándar.
Para acogerse a este último,
una entidad [debe](https://www.pcisecuritystandards.org/lang/es-es/)
"demostrar un enfoque sólido de la gestión de riesgos en materia de seguridad"
(p. ej., disponer de un departamento dedicado a la gestión de riesgos).

Si tu equipo cumple los requisitos de PCI DSS
con el enfoque personalizado,
tendrás que hacer un análisis de riesgos para cada uno de esos elementos
al menos una vez cada 12 meses.
Es decir,
tendrán que definir con detalle cuál sería el efecto
en la seguridad si no se cumpliera el requisito
y decir cómo los controles que ustedes aplican proporcionan
la protección necesaria.
Deberá figurar la firma de un miembro de la dirección ejecutiva
para certificar que ha revisado y aprobado la estrategia.
En el documento hay una plantilla de muestra
que te ayudará a conocer los elementos de los análisis
de los que debes informar.

El último nuevo requisito
que entra en vigencia este año está dirigido únicamente
a los proveedores de servicios externos
(TPSPs por sus siglas en inglés; es decir,
empresas para las que aplica PCI DSS pero que no son una marca para pagos),
como las **pasarelas de pago**.
En primer lugar, es necesario aclarar una cosa.
Una entidad que dependa de TPSPs necesita supervisar
el cumplimiento de la PCI DSS por parte de estos últimos
y mantener información sobre los requisitos que debe cumplir cada uno.

Así pues, el requisito para los TPSPs
es **respaldar las solicitudes de información de los clientes
sobre la responsabilidad y el estado de cumplimiento
de los requisitos de PCI DSS**.
Esto ayudará a la entidad que busca la conformidad
a alcanzar los requisitos mencionados anteriormente.

Los TPSPs pueden proporcionar al cliente
su Certificado de Cumplimiento PCI DSS
(AOC por sus siglas en inglés).
Y en caso de que los TPSPs no estén certificados,
al menos pueden proporcionar evidencias específicas
de su cumplimiento de los requisitos
de PCI DSS al evaluador del cliente.

## Fluid Attacks analiza tu cumplimiento de los requisitos PCI DSS

Ponemos a prueba tu *software* para verificar
el [cumplimiento de PCI DSS](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-pci).
Nuestro [plan estrella](../../planes/) ofrece
escaneo de vulnerabilidades con múltiples técnicas,
revisión manual de código por parte de nuestros *pentesters*,
gestión de vulnerabilidades en nuestra plataforma
y remediación de vulnerabilidades asistida por expertos e IA.
Te ayudamos a proteger tu *software* desde el principio del desarrollo
y antes de lanzarlo a producción o entregarlo a los clientes,
lo que está acorde con el requisito 6 de PCI DSS.

Si quieres evaluar nuestras herramientas primero,
[inicia una prueba gratuita](https://app.fluidattacks.com/).
