---
slug: blog/riesgos-seguridad-aplicaciones-web/
title: Riesgos de seguridad para aplicaciones web
date: 2024-08-16
subtitle: Ataques complejos basados en la web y medidas proactivas
category: ataques
tags: ciberseguridad, empresa, tendencia, riesgo, software
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1724177154/blog/web-apps-sec-threats/web-app-sec-threats_two.webp
alt: Foto por The Average Tech Guy en Unsplash
description: Descubre los riesgos clave de seguridad de las aplicaciones web y cómo los atacantes los explotan. También infórmate de estrategias eficaces que los desarrolladores pueden utilizar y proteger sus aplicaciones.
keywords: Seguridad De Aplicaciones Web, Amenazas De Ciberseguridad, Riesgos De Seguridad De Aplicaciones, Amenazas De Aplicaciones Web, Estrategias De Prevencion, Pruebas De Seguridad, Hacking Etico, Pentesting
author: Wendy Rodriguez
writer: wrodriguez
name: Wendy Rodriguez
about1: Escritor y editor
source: https://unsplash.com/photos/black-android-smartphone-displaying-home-screen-DsmDqiYduaU
---

En la actualidad, las aplicaciones web ofrecen una amplia gama de usos,
desde páginas de *e-commerce* hasta redes sociales
y herramientas de productividad, entre muchos otros.
Además de su amplia accesibilidad,
las aplicaciones web son fáciles de actualizar,
rentables y capaces de manejar a muchos usuarios simultáneamente.
Dado que estas suelen almacenar información sensible,
como datos personales, detalles financieros y propiedad intelectual,
son objetivos principales de ciberataques.

Los ciberdelincuentes explotan las vulnerabilidades de las aplicaciones web,
lo que puede provocar pérdidas financieras,
daños a la reputación y consecuencias legales para las empresas.
Para contrarrestar estas amenazas,
los desarrolladores aplican varios controles o estrategias de seguridad.

## Seguridad de las aplicaciones web

La seguridad de las aplicaciones web consiste en proteger los sitios web,
los servicios web y las API de los ciberataques.
Esta protección puede lograrse mediante una combinación de tecnologías
y estrategias destinadas a prevenir distintas amenazas.

A medida que los ataques basados en la web se vuelven más sofisticados,
la protección de los datos confidenciales se vuelve
una prioridad para las empresas y los desarrolladores.
En este *post* exploramos siete amenazas críticas actuales
para la seguridad de las aplicaciones web
y proporcionamos consejos sobre cómo puedes prevenirlas.

## Amenazas desafiantes y estrategias preventivas

### 1- Ataques de inyección

Los ataques de inyección se producen cuando se inserta código malicioso
en una aplicación a través de varios puntos de entrada,
como formularios, cadenas de consulta o *cookies*.
Una vez ejecutado por la aplicación,
este código puede permitir a los atacantes robar datos,
alterar el comportamiento de la aplicación u obtener acceso no autorizado.

Existen cuatro tipos principales de ataques de inyección.
**[Inyección SQL](../../../blog/sql-injection/)**
ocurre cuando los atacantes insertan código SQL malicioso
en los campos de entrada.
La aplicación ejecuta este código como parte de una consulta a la base de datos,
lo que permite a los atacantes manipular datos,
saltarse la autenticación o eliminar información.
Otra técnica es la **inyección de comandos del sistema operativo**,
con la cual se engaña a una aplicación
para que ejecute comandos arbitrarios del sistema operativo,
dando a los atacantes un mayor control sobre el sistema.
La **inyección LDAP** se dirige a servidores
de protocolo ligero de acceso a directorios (LDAP),
y los atacantes pueden inyectar código malicioso
para modificar las consultas en este protocolo
y obtener acceso no autorizado o manipular la información del directorio.
Por último,
la **inyección NoSQL**, que se produce cuando los atacantes
consiguen inyectar texto arbitrario en consultas NoSQL,
se dirige a bases de datos NoSQL (como MongoDB y ArangoDB).

[En 2023](https://www.cisa.gov/sites/default/files/2023-06/aa23-158a-stopransomware-cl0p-ransomware-gang-exploits-moveit-vulnerability_5_0.pdf),
la banda de *ransomware* Cl0p explotó una vulnerabilidad
de inyección SQL de día cero en
la aplicación MOVEit Transfer ([CVE-2023-34362](https://nvd.nist.gov/vuln/detail/CVE-2023-34362)).
Esta violación permitió el acceso no autorizado a la base de datos,
lo que condujo al robo de datos confidenciales en múltiples organizaciones.
La banda Cl0p utilizó los datos robados para extorsiones,
amenazando con hacerlos públicos a menos que se pagara el rescate.

Estrategias preventivas incluyen:

- Desinfectar y examinar cuidadosamente todas las entradas
  del usuario para evitar la inyección de código dañino.

- Utilizar consultas con parámetros para las interacciones con la base de datos,
  las cuales separan los datos del código SQL,
  reduciendo el riesgo de inyección.

- Realizar pruebas de seguridad rutinarias,
  incluidas pruebas de penetración y evaluaciones de vulnerabilidad,
  para identificar y corregir las vulnerabilidades de inyección antes
  de que los atacantes puedan explotarlas.

### 2 - Secuencias de comandos en sitios cruzados (XSS)

XSS es una vulnerabilidad que permite a los atacantes
inyectar JavaScript malicioso en páginas web vistas por otros usuarios.
Esto puede conducir al robo de *cookies*,
*tokens* de sesión y otra información sensible.
XSS se produce cuando un sitio vulnerable incluye
datos no fiables sin la validación adecuada,
lo que permite que el *script* del atacante
se ejecute en el sistema de un usuario.

Los ataques XSS se presentan de varias formas.
En el **XSS reflejado**, el *script* malicioso se inyecta en una URL
y se refleja al usuario en la respuesta.
En el **XSS almacenado**, el *script* dañino se guarda en el servidor,
por ejemplo en un mensaje o comentario de un foro,
y se ejecuta más tarde cuando otro usuario visualiza el contenido.
El **XSS basado en DOM** se ejecuta por completo en el navegador del cliente,
sin interacción directa con el servidor.

[En 2020](https://www.securityweek.com/vulnerability-whatsapp-desktop-app-exposed-user-files/),
se descubrió una importante vulnerabilidad XSS en WhatsApp Web.
Permitía a los atacantes enviar mensajes especialmente diseñados que,
al ser abiertos por el usuario,
ejecutaban código malicioso y permitían a los atacantes leer archivos
del sistema local del usuario.
Esta vulnerabilidad afectaba a WhatsApp Desktop anterior a la versión 0.3.9309
y a las versiones de WhatsApp para iPhone anteriores a la 2.20.10.

Estrategias preventivas incluyen:

- Validar y desinfectar siempre la entrada del usuario
  para evitar la inyección de *scripts* maliciosos.

- Cifrar correctamente la salida cuando se muestren datos
  proporcionados por el usuario.

- Realizar pruebas de seguridad periódicas,
  incluidos [escaneo de vulnerabilidades](../escaneo-de-vulnerabilidades/)
  y [pruebas de penetración](../pentesting/),
  para descubrir posibles puntos débiles.

### 3 - Fallas de autenticación y gestión de sesiones

Estos errores se producen cuando los mecanismos de una aplicación
para gestionar la autenticación
y las sesiones de los usuarios están mal implementados,
lo que puede llevar a omitir la autenticación o a apropiarse de la sesión.

Los atacantes utilizan varios enfoques para abusar de estas vulnerabilidades.
Pueden **explotar políticas de contraseñas débiles**,
como las que permiten contraseñas de menos de ocho caracteres
o que no imponen el uso de múltiples tipos de caracteres
para aumentar la complejidad.
Los atacantes pueden explotar sistemas
que se saltan los controles de acceso
cuando **no se aplican limitaciones basadas en roles** o no se aplica
una política de "denegación por defecto".
Otra forma es a través de la **gestión de errores de sesión**,
lo que a menudo implica una mala gestión de las *cookies*,
tiempos de espera inadecuados y falta de regeneración de sesión.
También se puede utilizar el **relleno de credenciales** para acceder
a las sesiones de los usuarios.

[A principios de 2023](https://www.idstrong.com/sentinel/norton-lifelock-data-breach/#:~:text=Over%206%2C000%20accounts%20were%20believed,mailing%20addresses%2C%20and%20phone%20numbers.),
Norton LifeLock sufrió una violación de datos
debido a ataques de relleno de credenciales.
Los ciberdelincuentes obtuvieron acceso no autorizado
a las cuentas de los clientes,
exponiendo datos personales sensibles como nombres, direcciones,
números de teléfono y contraseñas almacenadas
en el gestor de contraseñas de Norton.

Estrategias preventivas incluyen:

- Imponer requisitos de contraseñas seguras,
  incluida la longitud, los tipos de caracteres y la caducidad.

- Implementar autenticación multifactor
  y algoritmos *hash* para contraseñas seguras.

- Gestionar las sesiones protegiendo las *cookies* de sesión,
  utilizando HTTP seguro (HTTPS) y renovando regularmente los ID de sesión.

### 4 - Referencias directas inseguras a objetos (IDOR)

Las vulnerabilidades IDOR
se explotan a menudo a través de la manipulación de URL,
donde un actor malicioso aprovecha un identificador predecible
debido a verificaciones insuficientes de autenticación y control de acceso.
Sin las verificaciones adecuadas, el atacante podría ver registros privados,
modificar o borrar datos y tomar el control de las cuentas.

Aunque IDOR se considera un único tipo de vulnerabilidad,
puede aparecer de diferentes formas.
La **escalada horizontal de privilegios** se produce cuando
un atacante accede a datos o realiza acciones destinadas
a otros usuarios dentro del mismo nivel de privilegios.
La **escalada de privilegios vertical** implica acceder a datos
o funciones destinados a usuarios con permisos de mayor nivel.

[En 2019](https://www.forbes.com/sites/ajdellinger/2019/05/26/understanding-the-first-american-financial-data-leak-how-did-it-happen-and-what-does-it-mean/),
First American Financial Corp
sufrió una filtración masiva de datos debido a una vulnerabilidad IDOR,
exponiendo casi 885 millones de registros sensibles
relacionados con transacciones hipotecarias.
Con solo alterar los dígitos de una URL,
cualquier persona con un navegador web podía acceder a estos documentos,
que incluían números de cuentas bancarias y de seguridad social,
así como imágenes de permisos de conducir y otros registros financieros.
Esta vulnerabilidad se descubrió mediante pruebas de penetración manuales.

Estrategias preventivas incluyen:

- Validar la entrada del usuario para evitar que los atacantes exploten
  las referencias directas a objetos.
  Asegurarse que las entradas coinciden con los tipos,
  longitudes y formatos esperados.

- Aplicar controles de acceso y gestión de sesiones eficaces,
  como restringir el acceso a los recursos
  y datos únicamente a los usuarios autorizados.
  Revisar las *cheat sheets* de OWASP sobre [autorización](https://cheatsheetseries.owasp.org/cheatsheets/Authorization_Cheat_Sheet.html)
  y [autenticación](https://cheatsheetseries.owasp.org/cheatsheets/Authentication_Cheat_Sheet.html)
  puede ayudar a orientar las buenas prácticas.

- Realizar pruebas de penetración integrales,
  incluidas pruebas de seguridad de aplicaciones dinámicas
  ([DAST](../../producto/dast/)),
  para revelar posibles vulnerabilidades IDOR.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/pruebas-seguridad/"
title="Inicia ahora las pruebas de seguridad de Fluid Attacks"
/>
</div>

### 5 - Configuraciones de seguridad incorrectas

Entre los puntos vulnerables más comunes se encuentran estos errores,
que se producen cuando no se optimizan los ajustes de seguridad
o se despliegan servicios con opciones predeterminadas inseguras.
Esto puede ir desde mantener contraseñas de fábrica en un dispositivo,
hasta configurar un *firewall* de forma incorrecta.

Los atacantes explotan estas configuraciones erróneas de varias maneras.
Un método común es **aprovecharse de las credenciales predeterminadas**,
ya que muchos dispositivos vienen con nombres de usuario
y contraseñas preestablecidos que a menudo no se modifican.
Las **configuraciones débiles** son otro blanco,
como la configuración incorrecta del *firewall*,
los protocolos obsoletos y el cifrado deficiente.
El ***software* obsoleto** supone un riesgo importante,
ya que los sistemas sin parches son vulnerables a amenazas conocidas.
Las **configuraciones incorrectas en la nube**,
como los ajustes inadecuados en los depósitos de almacenamiento
o las máquinas virtuales,
también pueden exponer los datos y recursos a accesos no autorizados.

[En 2021](https://www.zenity.io/blog/research/the-microsoft-power-apps-portal-data-leak-revisited-are-you-safe-now/),
la filtración de datos de Microsoft Power Apps
expuso información confidencial debido a una configuración
incorrecta del portal.
Power Apps, una plataforma para crear aplicaciones personalizadas,
sufrió una falla crítica donde la configuración destinada
a usuarios autenticados con permisos específicos se configuró
para ser accesible públicamente sin los controles de seguridad adecuados.

Estrategias preventivas incluyen:

- Revisar periódicamente las configuraciones de seguridad
  en todos los sistemas y aplicaciones,
  asegurándose de que se ajustan a buenas prácticas
  y a requisitos de conformidad.

- Establecer y mantener un proceso riguroso de gestión de parches
  para mantener todo el *software* y los sistemas actualizados
  con las revisiones de seguridad más recientes.

- Poner en práctica pruebas de seguridad sistemáticas
  con evaluaciones diseñadas para identificar errores de configuración
  y otros puntos débiles de seguridad antes de que puedan ser explotados.

### 6 - Falsificación de solicitud entre sitios (CSRF)

CSRF es una vulnerabilidad
que permite engañar a los usuarios para que realicen acciones
no deseadas en una aplicación web.
Cuando un usuario hace clic en un enlace malicioso o envía un formulario,
la acción se ejecuta en su nombre,
provocando una posible pérdida de datos o un acceso no autorizado.

Los ataques CSRF suelen llevarse a cabo
mediante la **creación de enlaces o *scripts* maliciosos** que,
al pulsar sobre ellos, envían automáticamente solicitudes falsificadas
a una aplicación vulnerable.
Los atacantes también pueden falsificar las solicitudes
de envío doble de *cookies* (**DSC**) para eludir las protecciones establecidas,
aprovechándose de la forma en que se gestionan las *cookies*.
A menudo se emplean técnicas de **ingeniería social** para persuadir
a los usuarios a interactuar con enlaces o archivos adjuntos dañinos.

[En 2008](https://www.darkreading.com/cyber-risk/csrf-flaws-found-on-major-websites),
un notable ataque CSRF explotó vulnerabilidades en la aplicación web de YouTube.
Los atacantes crearon solicitudes maliciosas que,
cuando un usuario conectado visitaba una página web manipulada
o hacía clic en un enlace,
desencadenaban acciones no deseadas en su cuenta de YouTube,
como añadir vídeos a su lista de favoritos sin su consentimiento.

Estrategias preventivas incluyen:

- Implementar una protección basada en *tokens*,
  en la que se genera un *token* anti-CSRF único para cada sesión
  y se valida en el servidor con cada solicitud.

- Realizar escaneos exhaustivos de vulnerabilidades
  para evaluar las defensas tanto del lado
  del cliente como del lado del servidor,
  asegurándose de que las protecciones contra CSRF
  se implementen y funcionen eficazmente.

### 7 - Insuficiente registro y monitoreo

El registro y monitoreo insuficiente se refiere a prácticas inadecuadas
para rastrear y revisar eventos del sistema.
Esto puede implicar no registrar actividades críticas
como intentos fallidos de inicio de sesión
o descuidar el monitoreo de registros y alertas
para detectar comportamientos sospechosos.

Los atacantes se aprovechan de esta brecha de varias maneras.
Cuando una aplicación
web **funciona sin activar registros de eventos** importantes,
los actores maliciosos pueden pasar desapercibidos
durante largos periodos de tiempo.
Pueden **manipular o borrar registros** para ocultar su rastro.
**Explotar sistemas con capacidades de registro limitadas o inexistentes**
también les permite aprovechar los puntos ciegos
en los que sus acciones no quedan registradas.
La **falta de monitoreo en tiempo real** empeora estos riesgos
al retrasar la detección de amenazas en curso, dejando los sistemas expuestos.

Un ejemplo destacado es la filtración de datos de Target [en 2013](https://coverlink.com/cyber-liability-insurance/target-data-breach/)
en la que los atacantes se infiltraron en la red a través
de un portal de terceros e instalaron *malware*
en los sistemas de los puntos de venta.
Un factor importante que contribuyó a esta violación
fueron las prácticas inadecuadas de registro y supervisión de Target,
que permitieron a los atacantes robar millones
de números de tarjetas de crédito.

Estrategias preventivas incluyen:

- Asegurarse que todas las actividades críticas
  del sistema se registran minuciosamente,
  incluidas las fallas de autenticación, los intentos de acceso,
  los cambios en la configuración del sistema y los mensajes de error.

- Supervisar continuamente los registros
  y las actividades del sistema en tiempo real para detectar
  y responder rápidamente a comportamientos sospechosos y anomalías.

- Desarrollar y mantener un plan integral de respuesta a incidentes
  que describa los procedimientos para detectar,
  gestionar y mitigar los incidentes de seguridad.
  Verificar que el plan incluye funciones y responsabilidades,
  estrategias de comunicación y protocolos de respuesta.

Como habrás notado,
la estrategia de prevención que más se repite en todas
las secciones son las [**pruebas de seguridad**](../../soluciones/pruebas-seguridad/),
que por fortuna, nosotros ofrecemos.
Nuestra solución de [Hacking Continuo](../../servicios/hacking-continuo/)
incluye pruebas de penetración,
[gestión de vulnerabilidades](../../soluciones/gestion-vulnerabilidades/)
y [revisión de código seguro](../../soluciones/revision-codigo-fuente/),
por nombrar algunas.
Todas ellas son medidas esenciales para identificar
y abordar diversas vulnerabilidades, como las inyecciones,
ataques XSS, autenticación deficiente, etc.
Las pruebas de seguridad continuas,
efectuadas con herramientas automatizadas y técnicas manuales,
permiten descubrir de forma proactiva los puntos débiles
de tus aplicaciones web antes
de que los atacantes puedan aprovecharse de ellos.

Permite que Fluid Attacks trabaje para ti en la lucha
contra estas y otras amenazas a la seguridad.
[Contáctanos ahora](../../contactanos/).
