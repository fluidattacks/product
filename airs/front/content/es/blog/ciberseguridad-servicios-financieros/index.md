---
slug: blog/ciberseguridad-servicios-financieros/
title: Ciberseguridad en servicios financieros
date: 2024-05-10
subtitle: ¿Es tu servicio financiero tan seguro como crees?
category: filosofía
tags: ciberseguridad, empresa, vulnerabilidad, riesgo, cumplimiento
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1715352321/blog/financial-services-cybersecurity/cover_cybersecurity_finance_sector.webp
alt: FOto por Claudio Schwarz en Unsplash
description: Las organizaciones de servicios financieros son objetivos codiciados por ciberdelincuentes. Conoce por qué, qué amenazas acechan y cómo podemos ayudarte.
keywords: Servicios Financieros, Industria De Servicios Financieros, Ciberseguridad De Servicios Financieros, Ciberamenazas, Medidas De Ciberseguridad, Sector Financiero, Hacking Etico, Pentesting
author: Wendy Rodriguez
writer: wrodriguez
name: Wendy Rodriguez
about1: Escritor y editor
source: https://unsplash.com/photos/person-holding-fan-of-20-euro-bill-7YKBxf7qYQY
---

Nuestra economía depende del sector financiero.
Bancos, cooperativas de crédito, fondos de pensiones,
compañías de seguros, empresas de inversión
y otros servicios financieros tienen acceso a nuestros datos más sensibles:
información personal identificable (PII por sus siglas en inglés),
detalles de cuentas, datos transaccionales y empresariales.
Debido a su sencillez y prontitud,
las transacciones financieras en línea se han convertido
en algo habitual entre los consumidores y los servicios,
pero entre las ventajas también surgen inconvenientes
que deben ser abordados por las entidades financieras que los ofrecen.

El reporte [Verizon Data Breach Investigation](https://www.verizon.com/business/resources/reports/dbir/2024/industries-intro/financial-and-insurance-data-breaches/)
de 2024 muestra que el sector financiero
registró 3.348 incidentes en 2023,
con 1.115 violaciones confirmadas en Estados Unidos.
El mayor porcentaje de los datos comprometidos pertenecían
a los clientes de la entidad financiera,
lo que significa que son ellos quienes afrontan los mayores riesgos.
Este reporte también expuso que la industria financiera
fue el segundo sector que recibió más ataques el año pasado.
Estos datos destacan el riesgo constante al que se enfrentan
a diario las industrias de servicios financieros.

A lo largo de los años,
los bancos han protegido el efectivo tangible
y otros activos físicos mediante bóvedas sólidas,
con controles de personal y guardias de seguridad.
Ahora deben hacer lo mismo con sus activos digitales.
Hoy en día, cuando los delincuentes
no son solo ladrones enmascarados con pistolas,
sino también actores de amenazas en línea con métodos ingeniosos,
los servicios financieros siguen teniendo
la difícil tarea de impedirles el acceso.
Ahí es donde la ciberseguridad puede ayudar.
Existen medidas, soluciones y enfoques que pueden implementarse
para proteger los activos contra el constante bombardeo de ciberataques
que enfrenta el sector financiero.

## ¿Qué provoca los ciberataques a los servicios financieros?

Los servicios financieros son un objetivo primordial
para los ciberdelincuentes debido a varios factores
que los hacen muy atractivos.
El **beneficio**, ya sea financiero o informativo, es uno de esos factores.
Las instituciones financieras poseen una mina de oro
de información valiosa
(detalles de cuentas, números de la seguridad social, etc.)
que, si son robados, podrían venderse en la *dark web*
por un alto precio o utilizarse para cometer otros delitos.
Además,
dado que estas organizaciones dependen
principalmente de su infraestructura digital,
los atacantes pueden interrumpir las operaciones
y exigir grandes pagos de rescate,
a los que muchas organizaciones acceden para volver a su actividad normal.

Las instituciones financieras tienen ecosistemas complejos,
con numerosos actores interconectados,
como clientes, proveedores y terceros,
los cuales tienen acceso a plataformas en línea, aplicaciones móviles,
redes de comercio o sistemas globales de transferencia bancaria.
La enorme complejidad e interconectividad
de las infraestructuras financieras ofrecen
una **amplia superficie de ataque** llena
de oportunidades para los ciberataques,
algo que motiva a los ciberdelincuentes.

Otra razón es el **impacto**.
Un ciberataque puede dañar gravemente la reputación de una organización,
debilitar la confianza de los usuarios
e incluso desestabilizar las economías.
El impacto sistémico hace
que las entidades financieras sean objetivos especialmente codiciados
por los ciberdelincuentes que buscan causar interrupciones
y caos general.

Todas estas razones permiten a las organizaciones financieras
comprender por qué son blanco constante de ataques
y por qué necesitan una postura de seguridad robusta.
Cumplir las normas de seguridad establecidas
(de lo que hablaremos más adelante)
es extremadamente importante,
pero no es la única característica que debe tenerse en cuenta.
Examinemos primero las amenazas que afrontan las entidades financieras.

## Ciberamenazas comunes en el sector de servicios financieros

Las entidades financieras se enfrentan
a un sinfín de ciberamenazas por los motivos anteriormente expuestos.
Cada vez que se produce un incidente,
la entidad corre el riesgo de sufrir importantes daños.
Como ya hemos explicado,
el usuario final es el principal objetivo de los delincuentes,
sin embargo, la propia entidad financiera sufre pérdidas en sus operaciones,
demandas judiciales y daños a su reputación
como consecuencia de los ciberataques.
Siempre es mejor saber cómo actúan los ciberdelincuentes,
por lo que a continuación se exponen algunas de las amenazas
más comunes a las que está expuesto el sector financiero:

- **Ataques a la cadena de suministro**: En este ataque,
  los ciberdelincuentes penetran en los componentes
  de un proveedor externo o un tercero para acceder
  a los sistemas de sus clientes.
  Los atacantes pueden obtener acceso remoto
  a los sistemas de las instituciones financieras,
  robar datos sensibles o desplazarse lateralmente por diferentes entornos.
  Un ejemplo infame de ello son los ataques
  del [grupo Lazarus en Corea del Sur](https://www.welivesecurity.com/2020/11/16/lazarus-supply-chain-attack-south-korea/)
  a través de una [cadena de suministro](../../../blog/software-supply-chain-security/)
  que afectaba al *software* utilizado por bancos
  y organismos gubernamentales de este país.
  En Fluid Attacks disponemos de las herramientas
  que pueden reforzar la seguridad de tu cadena de suministro de *software*:
  SCA ([análisis de composición de *software*](../../producto/sca/))
  y SBOMs ([lista de materiales de *software*](../../learn/que-es-sbom/)).
  Con SCA,
  se realiza un análisis del código para identificar
  los componentes de código abierto y sus dependencias,
  y así encontrar riesgos asociados a componentes de terceros
  e informar y recomendar soluciones para mitigarlos.
  Con la actualización continua de los SBOM,
  se obtiene una imagen clara de todo lo que compone tu *software*.
  Nuestro SCA tiene la capacidad de producir SBOMs precisos
  y completos para ti en diferentes formatos estandarizados
  (CycloneDX y SPDX).
  Consulta [aquí](https://help.fluidattacks.com/portal/en/kb/articles/analyze-your-supply-chain-security)
  más información sobre este proceso.

- **Ataques de *phishing***: Estos correos o mensajes engañosos,
  que suplantan la identidad de entidades o personas reales,
  pueden contener *malware* que se descarga en el dispositivo
  del receptor con solo hacer clic en un enlace malicioso,
  o pueden enviarse con la intención de engañar al destinatario
  para que revele información sensible, como sus credenciales de acceso.
  Esta táctica puede utilizarse tanto en empleados de empresas como en clientes.
  Algunos ejemplos son la estafa por [*phishing* del banco OCBC de 2021](https://www.channelnewsasia.com/singapore/ocbc-phishing-scam-more-losses-victims-reported-2469086),
  que provocó pérdidas de 13,7 millones de dólares
  y tenía como objetivo a los clientes de esta entidad de Singapur,
  y la campaña de [*spear-phishing* de PerSwaysion](https://www.zdnet.com/article/spear-phishing-campaign-compromises-executives-at-150-companies/),
  que vulneró las cuentas de correo electrónico de altos ejecutivos financieros.

- **Ingeniería social**: Esta táctica consiste en manipular a las personas
  para que divulguen información sensible
  o realicen acciones que comprometan la seguridad.
  Los [ataques de *phishing*](../../../blog/phishing/) son
  una forma de [ingeniería social](../../../blog/social-engineering/),
  pero los atacantes también pueden utilizar las redes sociales,
  las llamadas telefónicas o incluso hacerse pasar
  por personal de la empresa para engañar a los empleados.
  Algunos ejemplos son los *hackers* que engañaron
  a un empleado de la red [interbancaria chilena Redbanc](https://www.finextra.com/newsarticle/33223/fake-job-interview-dupes-chilean-atm-network-employee-into-downloading-malware)
  para que descargara un *malware*,
  dándoles acceso a información sensible,
  y la [campaña Dyre Wolf de 2015](https://securityintelligence.com/dyre-wolf/)
  que utilizó ingeniería social avanzada
  para robar alrededor de 1 millón de dólares a empresas.

- **Ransomware**: Este peligro latente cifra los datos críticos
  y bloquea los sistemas operativos hasta que se paga un rescate,
  interrumpiendo las operaciones y causando pérdidas.
  El [*ransomware*](../../../blog/ransomware/) puede distribuirse
  de diferentes formas.
  Algunos ejemplos son el ataque de *ransomware* de 2021
  que afectó a la [aseguradora estadounidense CNA Financial](https://www.cpomagazine.com/cyber-security/cyber-insurance-firm-suffers-sophisticated-ransomware-cyber-attack-data-obtained-may-help-hackers-better-target-firms-customers/)
  e interrumpió los servicios de empleados y clientes de la empresa,
  y el ataque de *ransomware* al que se enfrentó
  la [aseguradora israelí Shirbit](https://www.calcalistech.com/ctech/articles/0,7340,L-3903077,00.html),
  cuya negación a pagar el rescate dio lugar
  a cuatro demandas colectivas de clientes afectados
  por la filtración de datos que ascendían a 360 millones de dólares.

- **Ataques de denegación de servicio (DDoS)**: Los ataques DDoS saturan
  un servidor con tráfico de Internet falso,
  haciendo que deje de estar disponible para los usuarios legítimos
  e interrumpiendo el servicio de la institución financiera.
  Estos ataques tan comunes pueden comprometer la banca en línea,
  las cuentas de los clientes, los portales de los empleados, etc.
  Algunos ejemplos son los ataques DDoS de
  [2022 a la Bolsa de Moscú y Sberbank](https://www.forbes.com/sites/thomasbrewster/2022/02/28/moscow-exchange-and-sberbank-websites-knocked-offline-was-ukraines-cyber-army-responsible/?sh=6d8246d677ca)
  que sacaron de servicio sus sitios web,
  y los ataques DDoS a [entidades financieras holandesas](https://nltimes.nl/2018/05/28/ddos-attacks-target-dutch-banks)
  (ABN Amro, ING y Rabobank) de 2018
  que dejaron caídos sus servicios de banca en línea
  y aplicaciones móviles.

- **Amenazas internas**: Esta forma de riesgo puede deberse
  a diferentes motivos: A veces,
  empleados descontentos o codiciosos pueden hacer un uso indebido
  de información sensible;
  otras veces,
  los datos pueden ser filtrados accidentalmente
  por empleados descuidados.
  Algunos ejemplos son el incidente de fraude
  en [cajeros automáticos de Bank of America en 2010](https://www.wired.com/2010/04/bank-of-america-hack/),
  en el que un empleado instaló *malware* en 100 cajeros
  y robó miles de dólares,
  y el [empleado de Scotiabank](https://globalnews.ca/news/7201479/scotiabank-suspicious-employee-activity/)
  quien puso en peligro varias cuentas de clientes
  al acceder a ellas sin una razón válida.

Es absolutamente crucial identificar y comprender estas
y otras tácticas (como los ataques de día cero o contra las API)
para desarrollar estrategias más eficaces
y completas contra las amenazas en constante evolución.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/pruebas-seguridad/"
title="Inicia ahora las pruebas de seguridad de Fluid Attacks"
/>
</div>

## Medidas de seguridad para servicios financieros

Los servicios financieros deben aplicar
una amplia gama de medidas de ciberseguridad
para proteger sus activos de los riesgos que acechan.
Necesitan una estrategia de defensa de múltiples capas
para combatir eficazmente los ciberataques,
y debería abordar varios aspectos de la ciberseguridad,
como prevención, detección, reacción y recuperación.
Las siguientes son algunas medidas esenciales que deben tenerse en cuenta:

### Ajustarse a las regulaciones

Los organismos gubernamentales imponen regulaciones estrictas,
y algunas no son simples requisitos,
sino leyes estatales o federales,
por lo que su cumplimiento no es negociable.
Su objetivo es garantizar que las entidades financieras protejan
los datos de los clientes y los protejan contra el fraude financiero,
buscando en general preservar la privacidad de los clientes
y mantener la confianza entre entidades y clientes.

El primer paso es identificar los estándares de cumplimiento
específicos que se aplican a las entidades.
Dependiendo de factores como la ubicación,
los servicios ofrecidos y el tamaño de la institución,
se aplicarán diferentes normativas. Estos estándares incluyen:

- **Ley Sarbanes-Oxley (SOX)**: [Esta ley](https://sarbanes-oxley-act.com/)
  de 2002 es una ley federal que impone requisitos de información financiera
  y divulgación a todas las empresas estadounidenses que cotizan en bolsa
  y tiene como objetivo proteger a los inversores
  y prevenir el fraude contable.

- **Ley Gramm-Leach-Bliley (GLBA)**: [Esta ley](https://www.ftc.gov/business-guidance/privacy-security/gramm-leach-bliley-act)
  de 1999, también conocida como
  Ley de modernización de servicios financieros de 1999,
  es una ley federal estadounidense que obliga a las instituciones
  financieras a proteger la información financiera de los clientes
  y a implementar un programa de seguridad de datos.

- **Consejo de estándares de seguridad de la industria de tarjetas
  de pago (PCI DSS)**: [Este conjunto](https://www.pcisecuritystandards.org/)
  de políticas y procedimientos rige la seguridad
  de los datos de los titulares de tarjetas,
  como números de tarjetas de crédito,
  fechas de caducidad y códigos de seguridad.
  Los controles de seguridad PCI DSS ayudan a las empresas
  a minimizar el riesgo de filtración de datos,
  fraude y usurpación de identidad.

- **Departamento de servicios financieros de Nueva York (NYDFS)**:
  [Estas regulaciones](https://www.dfs.ny.gov/) de ciberseguridad
  tienen como objetivo garantizar que las instituciones financieras
  protejan los datos de sus clientes
  y los sistemas de información contra ataques.

- **Reglamento general de protección de datos (GDPR)**:
  [Esta legislación](https://gdpr-info.eu/) de la Unión Europea
  exige la protección de los datos personales
  y obliga a aplicar normas estrictas en materia de tratamiento,
  almacenamiento y transferencia de datos a las organizaciones
  que manejan información personal de residentes en la UE.

Una vez identificados los estándares pertinentes,
las instituciones financieras deben definir
los pasos específicos necesarios para lograr
y mantener el cumplimiento de cada estándar.
Una vez implementados, deben realizar evaluaciones
y auditorías periódicas para asegurarse de que no existen vacíos
o deficiencias en las políticas y procedimientos establecidos.
Además, es fundamental contar con programas de formación y concientización
para educar a los empleados sobre estándares de cumplimiento,
requisitos regulatorios y buenas prácticas.
También se recomienda adoptar un enfoque proactivo utilizando
herramientas automatizadas para verificar el cumplimiento.
Por nuestra parte,
Fluid Attacks ofrece análisis continuos basados en los estándares del sector.
Nuestra herramienta [SAST](../../producto/sast/)
realiza escaneos que informan de varios incumplimientos en tu *software*.
Adherirse y cumplir con las regulaciones es esencial
para preservar la confianza de los socios y clientes de la organización,
así como para evitar sanciones cuantiosas.

### Utilizar soluciones que den prioridad a la seguridad

Existen varias soluciones o medidas que una institución financiera
puede adoptar para mantener segura su información:

- **Autenticación multifactor (MFA):** Este tipo de autenticación
  concede acceso a los recursos únicamente después
  de que el usuario proporcione dos o más factores de verificación.
  La implementación de MFA refuerza la autenticación del usuario
  y evita el acceso no autorizado a datos y sistemas sensibles.

- **Encriptación de datos:** Los datos confidenciales gestionados
  por las instituciones financieras deben estar siempre encriptados
  tanto en estado inactivo como en tránsito,
  para garantizar que, incluso si son interceptados por actores de amenazas,
  permanezcan ilegibles e inutilizables.

- **Seguridad de los puntos finales:** Los puntos finales,
  como computadoras para escritorio, portátiles,
  teléfonos inteligentes y servidores,
  deben protegerse con antivirus u otro *software* que supervise
  los dispositivos para detectar actividades irregulares,
  intentos de acceso no autorizado, detección de *malware* y otras amenazas.

- **Control de parches:** Las entidades financieras deben dar prioridad
  a aplicar a tiempo los parches de seguridad
  que los proveedores de *software* publican periódicamente
  para corregir las vulnerabilidades de sus productos.

### Implementar metodologías y planes de seguridad

Responder a un ciberataque mientras está en marcha podría
ser como intentar sacar agua a baldes de un barco que se hunde.
Creemos que la prevención y la planificación son mejores que el pánico,
el bloqueo de los sistemas, el pago de rescates,
la pérdida de clientes, etc. Una forma de tomar medidas
antes de sufrir un incidente es tener un enfoque general
proactivo para gestionar la seguridad,
que es lo que hace la **arquitectura de *zero trust***.
El [modelo de seguridad *zero trust*](../../learn/seguridad-zero-trust/)
asume que ningún usuario o dispositivo de la red es inherentemente confiable.
Eso significa que cada solicitud de acceso,
independientemente de su origen,
se verifica estrictamente antes de conceder el acceso
a información o sistemas sensibles.
Esta filosofía se hace realidad con [ZTNA](../ztna/)
(Zero Trust Network Access)
que ayuda a mitigar el riesgo de amenazas internas
e intentos de acceso no autorizados.

Otra forma de adelantarse a los ciberataques
es realizar **capacitaciones de concientización sobre seguridad**.
Los empleados son a menudo la primera línea de defensa contra los ciberataques.
Los programas de formación periódicos sobre seguridad pueden enseñar
al personal a identificar estafas de *phishing*
u otras tácticas de ingeniería social y a emplear buenas prácticas,
como el uso de contraseñas seguras,
para mantener la seguridad de los sistemas.
Esto permite a los empleados tomar decisiones informadas
y evitar ser víctimas de ataques maliciosos.

Como la ciberseguridad nunca está 100% blindada,
siempre se recomienda desarrollar y mantener
un **plan integral de respuesta a incidentes** que
describa los procedimientos para la
detección, evaluación, contención y mitigación de incidentes.
Se recomienda que el plan establezca funciones
y responsabilidades claras para los miembros del equipo,
que se realicen simulacros y actualizaciones con regularidad
y que se guarde fuera de línea.

### Cómo Fluid Attacks ayuda al sector financiero

En Fluid Attacks,
la seguridad de las aplicaciones es nuestra prioridad.
Para el sector de servicios financieros,
nos posicionamos como una solución "todo en uno" que contribuye
a la seguridad a través de diferentes estrategias.

- **Desarrollo de *software* seguro**: Motivamos a nuestros clientes
  para que sigan las buenas prácticas de codificación segura,
  y realizamos [pruebas de seguridad](../../soluciones/pruebas-seguridad/)
  exhaustivas a lo largo del ciclo de vida de desarrollo
  de tu *software* (SDLC) con nuestras herramientas automatizadas
  y nuestro equipo de *pentesters* [certificados](../../../certifications/)
  para identificar vulnerabilidades en tus aplicaciones e infraestructuras.

- **Identificación de vulnerabilidades**: Nuestras soluciones,
  como el [*pentesting*](../../soluciones/pruebas-penetracion-servicio/)
  y el [*hacking* ético](../../soluciones/hacking-etico/),
  tienen como objetivo encontrar vulnerabilidades,
  las cuales son la puerta que necesitan los ciberdelincuentes
  para penetrar en tus sistemas.
  Utilizando herramientas de alto nivel,
  métodos y su valiosa experiencia, nuestros *pentesters*
  atacan tu sistema (con tu permiso) de la misma forma
  que lo haría un actor malicioso.
  Con sus hallazgos,
  que recibirás a través de nuestra plataforma,
  podrás tomar una decisión informada sobre cómo proceder.

- **Gestión de vulnerabilidades**: Una vez identificadas
  las vulnerabilidades de tu *software*,
  las cuales están detalladas en [nuestra plataforma](../../plataforma/),
  pueden ser priorizadas, revisadas por tu equipo y asignadas al mismo,
  y una vez finalizado este proceso,
  puedes solicitar reataques para verificar su remediación.
  Desde la misma plataforma,
  puedes hacer un seguimiento de cómo va el proceso de mitigación
  o reducción de la exposición al riesgo.
  Además,
  ofrecemos recomendaciones de remediación y soporte
  a través de IA generativa y llamadas virtuales con *hackers*.

Todo lo anterior forma parte
de nuestra solución [Hacking Continuo](../../servicios/hacking-continuo/),
que ayuda a [nuestros clientes](../../clientes/) de servicios financieros
y otros sectores a mejorar su postura de ciberseguridad
para ofrecer productos o servicios
de alta calidad a sus usuarios finales.
Uno de nuestros clientes de servicios financieros,
Protección, nos considera "un aliado importante"
en su búsqueda constante de un servicio más seguro.
Lee más sobre su historia de éxito [aquí](https://fluidattacks.docsend.com/view/3nqybjhs59s8pppe).
No dudes en [contactarnos](../../contactanos/) para ver cómo
podemos ayudar a tu organización.
