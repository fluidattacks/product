---
slug: blog/ztna/
title: Zero Trust Network Access
date: 2024-05-02
subtitle: Poniendo en práctica el modelo zero trust
category: filosofía
tags: ciberseguridad, empresa, tendencia, riesgo, software
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1714609501/blog/zero-trust-network-access/cover_ztna.webp
alt: Foto por mitchell kavan en Unsplash
description: ZTNA, un elemento del modelo zero trust, permite que la gestión de accesos sea sencilla y segura para todos los implicados.
keywords: Zero Trust Network Access, Ztna, Modelo De Seguridad Zero Trust, Ztna Vs Vpn, Vpn, Diferencia Entre Ztna Y Vpn, Hacking Etico, Pentesting
author: Wendy Rodriguez
writer: wrodriguez
name: Wendy Rodriguez
about1: Content Writer and Editor
source: https://unsplash.com/photos/man-in-black-shirt-standing-on-blue-light-yoegh5378Cw
---

La [filosofía de *zero trust*](../../learn/seguridad-zero-trust/)
(o confianza cero)
es una propuesta ideal para la ciberseguridad.
Ha tenido un crecimiento exponencial en los últimos años
debido a los principios y componentes en los que se basa.
Uno de esos componentes es el Zero Trust Network Access (ZTNA),
el cual es *zero trust* aplicado a redes.
ZTNA, o acceso a la red de confianza cero,
sirve como medio alternativo para proteger la red de una organización,
alejándose de los modelos de "confianza por defecto"
tan comúnmente utilizados en los modelos tradicionales
como el de *castle and moat*,
y creando así una postura más segura y flexible
que apoya los esfuerzos en la lucha contra la ciberdelincuencia.

El modelo de *castle and moat* utiliza redes privadas,
como VPNs, para controlar el acceso remoto.
Estos actúan como túneles para los usuarios autorizados fuera de la red,
y una vez dentro,
los usuarios acceden a los recursos que necesitan para realizar sus tareas.
Sin embargo,
las VPNs tienen sus limitaciones
y graves inconvenientes en materia de seguridad,
los cuales exploraremos en este artículo.
ZTNA, por otro lado,
hace que el acceso a los recursos de la compañía sea mucho más seguro,
mejorando así su postura frente a la seguridad.

## Zero Trust Network Access

ZTNA es una solución de TI
que se centra en los controles de acceso
y la autenticación de usuarios/dispositivos
para proporcionar un abordaje seguro y granular a los recursos.
ZTNA se ocupa principalmente de proteger el acceso a recursos
como aplicaciones, sistemas, servicios y datos dentro de una red,
desde cualquier ubicación remota y desde cualquier dispositivo.
Esta solución está establecida
para denegar el acceso a menos que se autorice explícitamente.
Las soluciones de ZTNA utilizan normalmente funciones como
la microsegmentación de aplicaciones,
controles de acceso basados en identidad,
configuración de políticas de acceso
y túneles cifrados para proteger el acceso a aplicaciones y datos,
sin importar la ubicación del usuario de la red.

### ¿Cómo funciona ZTNA?

Así es como generalmente funciona el Zero Trust Network Access:

1. Cuando un usuario o dispositivo intenta acceder a un recurso de red,
   la solución de ZTNA verifica su identidad
   mediante mecanismos de autenticación como usuarios y contraseñas,
   autenticación multifactor (MFA) o autenticación biométrica.

2. La solución de ZTNA evalúa en primer lugar la postura de seguridad
   del dispositivo del usuario, que puede incluir la búsqueda de parches,
   de actualizaciones y de otras mejoras.

3. Después, la solución de ZTNA aplica políticas de acceso
   basadas en información contextualizada,
   como las funciones del usuario, el tipo de dispositivo que accede,
   el grado de confidencialidad del recurso solicitado
   y la hora de la solicitud,
   para acceder a una aplicación o sistema específico.

4. Una vez que el usuario o dispositivo está autenticado y autorizado
   la solución de ZTNA crea un túnel seguro y cifrado
   entre él y el recurso al que intenta acceder.
   Esto garantiza que los datos transmitidos estén protegidos
   de ser interceptados o manipulados.

### Beneficios de ZTNA

En el mundo de hoy que se basa en la nube,
ZTNA ofrece varios beneficios a las empresas que buscan
un método más seguro para la gestión de accesos.
Estas son algunos de los beneficios principales:

- Autenticación de identidad de usuario/dispositivo
  como la autenticación basada en el conocimiento (credenciales personales),
  identificación biométrica y autenticación multifactor.
  Todo ello es fundamental para mantener la seguridad de los recursos.

- ZTNA permite el acceso remoto seguro desde cualquier lugar
  o dispositivo siempre que se apruebe la verificación.
  Esta flexibilidad es ideal teniendo en cuenta
  el creciente número de trabajadores remotos.

- La postura de seguridad de una empresa mejora siguiendo
  la filosofía *zero trust* de "nunca confíes, siempre verifica".
  Ayuda a las organizaciones a reducir la superficie de ataque
  y mitigar el riesgo de ataques internos y externos.

- Las soluciones de ZTNA basadas en la nube son fáciles de implementar
  y reducen el riesgo de permisos de acceso por error.

- ZTNA ofrece flexibilidad en caso de que un usuario/dispositivo
  necesite nuevas autorizaciones.

- ZTNA mejora la trazabilidad y la visibilidad de la actividad
  de los usuarios/dispositivos e intentos de acceso a través
  de los registros de tráfico,
  que es una información valiosa para la supervisión
  y auditoría con fines de seguridad.

- ZTNA ofrece un control más granular sobre el acceso a la red en comparación
  con las VPN (redes privadas virtuales)
  o VDI (infraestructuras de escritorios virtuales).
  Este enfoque granular minimiza el riesgo de acceso no autorizado.

- ZTNA permite la validación de la postura del dispositivo
  (sistema operativo, *firewall*, antivirus, IP de origen, cifrado de disco),
  contribuyendo a la estrategia integral de ciberseguridad.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/pruebas-seguridad/"
title="Inicia ahora las pruebas de seguridad de Fluid Attacks"
/>
</div>

## ZTNA vs. VPN

Una VPN actúa como un túnel entre dos redes
o terminales a través de una red intermedia (p. ej., Internet).
Las VPN se utilizan sobre todo por trabajadores remotos que necesitan acceso
a la red de la empresa para realizar sus tareas.
Una vez que los usuarios han proporcionado sus credenciales
pueden moverse por la red libremente
y se les confía el acceso a los recursos de la empresa.
Entre las ventajas de las VPN figuran la flexibilidad del acceso remoto,
el cifrado de datos y la facilidad de instalación.
Las necesidades y presupuestos de cada organización son diferentes,
pero conviene tener en cuenta algunos aspectos a la hora de confiar en las VPN.

Las VPN ofrecen una seguridad de datos fiable,
pero su naturaleza de confianza por defecto
las convierte en un blanco fácil para los *hackers*,
ya que conseguir credenciales mediante ingeniería social
o por fuerza bruta se ha convertido en un juego para ellos.
Los datos viajan cifrados a través de un túnel,
lo que protege los datos,
pero las VPN no garantizan que los dispositivos finales
o las redes conectadas mediante el túnel sean seguros.
Además, cifrar y enrutar los datos a través de servidores VPN
puede reducir la velocidad del tráfico de Internet.
El tiempo de actividad no siempre está garantizado;
fallos de *hardware* o errores de *software*
pueden interferir en la productividad y crear interrupciones.
Las VPN suelen ser la solución menos cara para el acceso remoto;
sin embargo, los complementos como las soluciones de seguridad adicionales,
licencias y mantenimiento aumentan el gasto total.

### ¿Cuál es la diferencia entre ZTNA y VPN?

<image-block>

!["ZTNA vs. VPN"](https://res.cloudinary.com/fluid-attacks/image/upload/v1714679187/airs/es/blogs/im%C3%A1genes%20traducidas/ztna/ztna_vpn_es.webp)

ZTNA vs. VPN

</image-block>

Las soluciones para el acceso seguro a la red incluyen tanto ZTNA como VPN.
Aunque ambas soportan el acceso remoto,
ZTNA tiene algunas ventajas significativas sobre VPN,
sobre todo en términos de establecer fiabilidad y control de acceso.
Las VPN no verifican de forma innata la postura de seguridad del terminal,
es necesario instalar y actualizar constantemente *software* adicional.
ZTNA realiza una verificación de terminal a terminal como paso inicial,
y no establecerá una conexión a menos que
que se haya superado la verificación de seguridad del dispositivo.

Mientras que las VPN se basan en las buenas prácticas de gestión de recursos
y contraseñas del usuario,
el concepto de ZTNA de acceso granular
le permite a una empresa controlar los recursos
que un usuario puede ver y utilizar.
En los casos en que sea necesario acceder a entornos segregados
se debe configurar y gestionar por separado otro VPN.
Esto no solo es menos pragmático,
sino que exponen más sistemas a posibles amenazas.
Por otro lado,
las soluciones de ZTNA ofrecen un enfoque basado en el usuario y más seguro,
ya que limitan el acceso a únicamente las aplicaciones autorizadas,
ofreciendo un acceso granular y
reduciendo la probabilidad de que los atacantes se desplacen lateralmente.

Monitorear el tráfico o la actividad de los usuarios con fines de auditoría
no es posible con las VPN,
lo que las hace inútiles si una empresa quiere hacer un seguimiento de quién
o qué se conecta a sus recursos.
La mayoría de los proveedores de ZTNA
están equipados con una especie de gestor de agentes
que proporciona información como el ID del usuario,
método de autenticación, estado del dispositivo, ubicación GPS y mucho más.

Tras una autenticación exitosa,
las VPN tradicionales conceden acceso a redes enteras.
Esto permite utilizar todos los recursos y,
en caso de una violación, ofrece una gran superficie de ataque.
El modelo de *zero trust* defiende el principio de "mínimo privilegio",
que, si se aplica correctamente,
limita la superficie de ataque en caso de violación.
Con las VPN, los usuarios podrían tener que instalar *software*,
configurar ajustes y solucionar problemas de conexión,
todo lo cual requiere la presencia de un equipo informático.
Por otro lado, tras la configuración inicial,
las soluciones de ZTNA suelen estar basadas en la nube,
por lo que requieren una configuración mínima,
independientemente de la ubicación o el dispositivo.
El ritmo de trabajo se agiliza y se reducen las posibles interrupciones.

### De VPN a ZTNA

En general,
ZTNA suele considerarse la solución más moderna y segura para el acceso remoto,
independientemente del entorno (en la nube o *on-premises*).
Las VPN siguen siendo una opción viable para algunos casos,
especialmente para organizaciones pequeñas con necesidades de red sencillas,
pero como ya se ha dicho, necesitan una protección adicional
tanto dentro como fuera del perímetro de la red.
Las compañías que se han dado cuenta de que la seguridad no se negocia
están migrando a ZTNA.
Con total honestidad, hacer el cambio no es tarea fácil,
pero una vez hecho, valdrá la pena.

El cambio de VPN a ZTNA requiere un plan de migración
cuidadoso y por fases,
teniendo en cuenta que las pruebas deben hacerse antes del despliegue,
así como cuestiones presupuestarias
y una formación clara a los usuarios para abordar las posibles inquietudes.
Las políticas de seguridad deben revisarse y, en su mayor parte
cambiarse para reflejar el enfoque de *zero trust*.
En el caso de que ZTNA vaya a integrarse con infraestructuras ya existentes,
el proveedor de ZTNA elegido debería presentar
un plan de trabajo para lograr la unificación sin complicaciones.
Debe realizarse un seguimiento que incluya el rendimiento del sistema,
las necesidades de escalabilidad
y la actividad de los usuarios tras el despliegue para identificar
y abordar los problemas que surjan.

Existen algunos conceptos equivocados
que los usuarios de VPN pueden tener sobre *zero trust* o ZTNA,
así que hablemos de estos.

- "Para implementar *zero trust* hay que trasladarse a la nube". No es cierto.
  Las soluciones de ZTNA pueden funcionar tanto en las instalaciones
  como en la nube sin comprometer el rendimiento.

- "ZTNA es únicamente una solución de acceso remoto". No es cierto.
  ZTNA se utiliza principalmente en el acceso remoto al trabajo,
  pero *zero trust* resuelve un problema de seguridad
  (acceder a recursos con la debida autenticación y autorización)
  que tienen todas las empresas, incluidos los trabajos presenciales.

- "Cambiar a ZTNA requiere actualizaciones costosas de la infraestructura".
  No es cierto.
  Muchas soluciones de ZTNA están diseñadas para funcionar
  con la infraestructura existente de una empresa,
  lo que reduce la necesidad de actualizaciones.
  Esto convierte a ZTNA en una solución rentable a largo plazo.

- "*Zero trust* protege contra todas las amenazas". No es cierto.
  Aunque *zero trust* y ZTNA tienen el poder y la capacidad de
  mejorar la postura de seguridad,
  son parte de una estrategia de ciberseguridad más grande que trabaja
  con otras metodologías y herramientas para proteger a una empresa.

## Uso de ZTNA por Fluid Attacks

[Violaciones de datos](../10-mayores-violaciones-de-datos/),
[ataques de *ransomware*](../10-mayores-ataques-ransomware/)
y otros incidentes de ciberseguridad suelen ser el resultado
de prácticas de seguridad inadecuadas.
ZTNA, bajo la filosofía de [*zero trust*](../../learn/seguridad-zero-trust/),
enfatiza la necesidad y el valor de una estrategia de ciberseguridad consciente.
Para Fluid Attacks,
ZTNA es esencial para realizar
la solución [Hacking Continuo](../../servicios/hacking-continuo/).
Utilizamos ZTNA de Cloudflare
como túneles seguros para conectarnos a las redes de nuestros clientes,
protegiendo nuestra interacción,
que es una de nuestras mayores prioridades como empresa.

Al conectarse a través de nuestro ZTNA, nuestros clientes se benefician de:

- Aprovechar el principio del menor privilegio estableciendo
  a qué recursos podremos acceder dentro de su red.
- Nuestra [plataforma](../../plataforma/) proporciona registros
  de trazabilidad de navegación por túneles,
  los cuales pueden ser utilizados para auditorías de red.
- Requerimos una autenticación de identidad exhaustiva que cumpla
  con políticas de contraseñas seguras y procesos de autenticación multifactor.
- Los dispositivos conectados pasan por un proceso de verificación minucioso
  que incluye la verificación de las últimas versiones del sistema operativo,
  actualizaciones y parches.
- También hacemos cumplir el requisito
  de un *firewall* activado y un disco duro encriptado.

No pedimos a nuestros clientes
que dejen de utilizar sus métodos de acceso remoto.
Solo cuando se conectan con nosotros
queremos que utilicen nuestra solución ZTNA establecida.
Nos ceñimos a la filosofía
de *zero trust* de "nunca confíes, siempre verifica"
y la ponemos en práctica cuando nuestros clientes nos dan acceso
a su código o aplicaciones.
Sabemos lo importante que es un modelo de seguridad robusto,
y queremos que te beneficies de él,
así que [contáctanos ahora](../../contactanos/).
