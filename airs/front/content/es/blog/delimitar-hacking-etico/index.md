---
slug: blog/delimitar-hacking-etico/
title: Delimitar el hacking ético
date: 2018-01-09
category: filosofía
subtitle: Cómo definir el alcance de tus objetivos
tags: hacking, pentesting, pruebas de seguridad
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1620330847/blog/delimit-ethical-hacking/cover_l6krrp.webp
alt: Person working on the computer while looking at cellphone
description: Cuando se descubren fallas de seguridad mediante el hacking ético, es importante delimitar el alcance de los objetivos. Aquí hablaremos de ello.
keywords: Seguridad, Hacking, Pentesting, Aplicacion, Infraestructura, Codigo, Hacking Etico
author: Felipe Gómez
writer: fgomez
name: Felipe Gómez Arango
about1: Fluid Attacks Account Manager, Bachelor of Business Management
about2: Passionate about technology and security
source: https://unsplash.com/photos/SpVHcbuKi6E
---

Uno de los principales problemas con los que se encuentra una organización
cuando necesita realizar un hackeo ético es establecer los límites del mismo.

Delimitar el alcance del *hacking* ético por tiempo
es un error común ya que no se puede saber cuando el *hacking*,
el cual se mide únicamente por esfuerzo, ha terminado.
Tampoco se puede saber si los resultados
han sido satisfactorios o si por el contrario
ha sido una gran pérdida de tiempo y de recursos que no
ha aportado ningún conocimiento valioso a la organización.

Hay dos objetivos a evaluar en el *hacking* ético,
la infraestructura y la aplicación.
Estos dos se pueden evaluar en un entorno desplegado o en uno de desarrollo,
analizando el código fuente.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/hacking-etico/"
title="Inicia ahora con la solución de hacking ético de Fluid Attacks"
/>
</div>

Si lo que quiere la organización es identificar vulnerabilidades
en sus aplicaciones (web/móvil)
y servicios web en base a las necesidades y contexto de su negocio,
y así generar el mayor impacto comercial posible,
se debe realizar *hacking* ético de aplicaciones.

Si lo que la organización desea es detectar fallas de seguridad
directamente en el desarrollo, identificando malas prácticas
de programación y errores intencionales en el código fuente
que puedan afectar el correcto funcionamiento del sistema,
se debe realizar un análisis de código fuente.

Finalmente,
si lo que se necesita son ataques a la infraestructura subyacente
de los sistemas (Servicios de Red/OS),
buscando explotar vulnerabilidades específicas de la tecnología implementada,
se deberá realizar *hacking* ético de la infraestructura.

Cuando se ha decidido el tipo de ataque que se va a realizar,
es necesario determinar el objetivo de la evaluación
o ToE basándose en tres elementos.

- **Número de puertos**, si lo que se va a evaluar es la infraestructura.

- **Número de campos de entrada**, si el objetivo del ataque es la aplicación.

- **Líneas de código**, si lo que se quiere determinar son
  los riesgos asociados al desarrollo.

Una vez estos alcances se fijan y se aclaran,
se tiene la seguridad de que todo lo relacionado
con esa tecnología será atacado, a diferencia de limitaciones
que se basan en el tiempo de ejecución con herramientas automatizadas
que solo explotan un pequeño porcentaje de
las [vulnerabilidades reportadas](../../../blog/what-is-vulnerability-management/).

En Fluid Attacks, nuestra propuesta es cumplir con el alcance prometido,
nunca en términos de tiempo. Nuestro *hacking* ético
finaliza cuando se ha evaluado el objetivo completo de la evaluación.
