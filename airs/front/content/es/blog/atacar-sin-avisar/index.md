---
slug: blog/atacar-sin-avisar/
title: Atacar sin avisar
date: 2022-06-21
category: opiniones
subtitle: Cinco políticas para poner a prueba la seguridad de tu organización
tags: ciberseguridad, empresa, red-team, blue-team, cumplimiento
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1655830173/blog/attacking-without-announcing/cover_attacking_without_announcing.webp
alt: Foto por Geran de Klerk en Unsplash
description: Queremos orientarte sobre políticas de gestión que sugerimos utilizar para que respondas con exactitud cuán segura está tu información.
keywords: Red Team, Red Team Contra Blue Team, Seguridad Red Team, Seguridad Blue Team, Politica, Pruebas De Seguridad Red Team Blue Team, Buenas Practicas, Hacking Etico, Pentesting
author: Rafael Alvarez
writer: ralvarez
name: Rafael Alvarez
about1: Cofundador y CTO de Fluid Attacks
about2: Ingeniero informático
source: https://unsplash.com/photos/9yvADFNcXOc
---

Hablamos mucho de los beneficios de la conectividad extrema
y de la disponibilidad de la información,
pero muy poco de cómo están protegidos los datos de nuestra empresa,
de nuestros clientes e incluso los personales.
Aquí queremos guiarte sobre algunas políticas
de gestión que te sugerimos aprovechar para determinar
con gran exactitud qué tan segura es tu información y tus sistemas
y qué tan efectivas son tus medidas de defensa.
También queremos informarte de las consecuencias de no aplicar estas políticas.

Gracias a nuestra experiencia,
sabemos que los directivos de las empresas suelen asumir
que comprar más tecnología resolvería todos sus problemas de seguridad.
En realidad, esa "solución" podría empeorar la situación,
porque una tecnología implementada,
construida o configurada de forma incorrecta es el origen de todas
las vulnerabilidades de seguridad.

Por otra parte, para las empresas modernas,
proteger su información haciéndola inaccesible
o manteniéndola en papel ya no es viable.
En un mundo donde la transformación digital es la norma,
es imprescindible facilitar más información al cliente.
Los beneficios de esta transformación van desde la mejora de los tiempos
y costos de las transacciones hasta el aumento de la satisfacción del cliente
y la reducción o eliminación de las ventanas de mantenimiento.
Operaciones que antes solo eran posibles
de forma presencial en horario de oficina,
ahora son posibles en cualquier lugar, 24 horas al día,
siete días a la semana, durante todo el año.

Sin embargo, la transformación digital puede hacernos reflexionar
sobre algunos riesgos y plantearnos preguntas como las siguientes:
¿Puede el consumidor modificar el precio del producto antes de pagarlo?
¿Puede un empleado conocer los cambios salariales de sus compañeros?
¿Pueden los miembros del sindicato leer
las actas del consejo de administración?
¿Puede un invitado obtener las contraseñas de los administradores de red?
¿Puede alguien conectarse a la red de la empresa,
encender un micrófono en el ordenador del administrador
y escuchar las conversaciones?
¿Puede un cliente modificar la página web de nuestra empresa?
¿Podemos consultar el historial médico de otra persona en internet?

## Protege tu organización

La pregunta "¿cuán seguros son los sistemas
y la información de mi organización?"
se responde realizando ciberataques éticos reales.
Hay varios nombres para esto:
[*hacking* ético](../../soluciones/hacking-etico/),
[pruebas de penetración](../../soluciones/pruebas-penetracion-servicio/),
[*red teaming*](../../soluciones/red-team/),
entre otros.
De este enfoque se derivan cinco políticas de gestión:

1. **Ataques continuos:**
   Los ataques a los sistemas de tu organización deben realizarse
   para encontrar vulnerabilidades que permitan a atacantes malintencionados
   tomar el control de tus operaciones y activos de información.
   La palabra "continuos" significa que estos ataques deben seguir
   una frecuencia específica y estable (trimestral, semestral, etc.).
   Cuando esta política no está estipulada,
   las organizaciones tienden a detener nuevos ataques con la excusa de
   no poder solucionar las vulnerabilidades encontradas en el ciclo anterior.
   Una vez que tu organización aplique sabiamente esta política,
   podrás dar un paso más hacia la siguiente.

2. **Ataques de conocimiento cero:**
   No tiene sentido que los atacantes
   ([*red team*](../ejercicio-red-teaming/))
   realicen pruebas de seguridad cuando los defensores (*blue team*)
   conocen las horas y los lugares de sus intrusiones.
   Es absurdo que en estos ataques los miembros del *red team*
   informen de los avances o soliciten permisos a los miembros del *blue team*,
   al personal de la organización vinculado a los proveedores
   de *software/hardware* de defensa o a los jefes implicados.
   Para conocer con certeza el nivel de seguridad de tu empresa,
   estos ejercicios deben acercarse lo más posible a la realidad.
   En la vida real, los atacantes malintencionados no notificarán cuándo,
   cómo y dónde podrían atacar, qué técnicas podrían utilizar,
   cuál es su nivel de penetración,
   qué equipos poseen y qué información han divulgado.
   Por ello,
   tu organización debe tener privilegios restringidos
   para adquirir información sobre las pruebas de seguridad.
   Solo debe saberlo cierto personal.
   Esto se conoce como política de conocimiento cero.

   ![Red team vs blue team](https://res.cloudinary.com/fluid-attacks/image/upload/v1655835394/blog/attacking-without-announcing/red_blue.webp)

   <small>Ejercicios de seguridad: red team vs. blue team (imagen tomada de [aquí](https://hdwallpaperim.com/wp-content/uploads/2017/08/25/126305-Red_vs._Blue-Halo.jpg)).</small>

    Esta política implica que los responsables de la seguridad
    de tu organización no deben ser quienes organicen
    y coordinen las pruebas de *hacking* ético.
    Al conocer los ataques de antemano,
    podrían mostrar tendencias a prepararse para ellos de forma poco realista,
    tratar de limitar su alcance a las zonas fuertes
    y evitar revelar vulnerabilidades críticas a sus responsables
    para no poner en peligro sus cargos actuales.
    Y aunque ahora está de moda tener equipos mixtos,
    una combinación de atacantes y defensores,
    hay que mantener un objetivo claramente definido:
    conocer con precisión el nivel de seguridad.
    Estos equipos mixtos pueden contaminar los resultados
    de las pruebas debido a un conflicto
    de intereses en el diseño organizativo de la empresa.
    Proceder sobre la base de esta política
    te proporciona una ventaja excepcional:
    conocer las capacidades reales de detección
    y reacción de tu organización en caso de ataque.
    Si el *blue team* no sabe si el atacante
    es un *hacker* de sombrero blanco (es decir,
    un *hacker* del *red team*) o un *hacker* de sombrero negro
    (es decir, un *hacker* malicioso),
    siempre estará en estado de alerta
    y responderá según los procedimientos definidos:
    bloqueo, notificación, gestión de incidentes, etc.

   <cta-banner
   buttontxt="Más información"
   link="/es/soluciones/red-team/"
   title="Inicia ahora con la solución de red teaming de Fluid Attacks"
   />

3. **Respuesta implacable:**
   Reaccionar de forma implacable ante cada detección,
   independientemente de las intenciones de los *hackers*.
   Esta política te permite mantener
   bien aceitado el motor de respuesta a incidentes,
   evaluar la calidad del *red team* contratado
   y la eficiencia de tus inversiones en defensa,
   y también te ayuda a lograr reducciones de gastos
   o aplicar sanciones que hagan que los ejercicios de ataque
   se compensen tras cierto tiempo de frecuencia.

   ![Information protection](https://res.cloudinary.com/fluid-attacks/image/upload/v1655836793/blog/attacking-without-announcing/speedbump.webp)

   <small>Protección continua de la información empresarial (imagen tomada de [aquí](https://i.pinimg.com/originals/ac/18/ef/ac18ef59a3b313573e9bf62696c552ea.gif)).</small>

4. **Intrusión total:**
   Esta política es la consecuencia directa de las dos anteriores.
   El *red team* debe disponer de una autorización completa en papel
   y correo electrónico y de todas las formas de protección legal
   de la más alta autoridad de la empresa
   (es decir, el CEO o el director)
   para aplicar cualquier táctica ofensiva con el fin de obtener información,
   modificar datos, acceder a los puestos de trabajo o desconectar servicios.
   Todo debe estar permitido para garantizar la máxima rigurosidad
   y comprometer la seguridad al más alto nivel.
   Si no se pone en práctica esta política,
   los *hackers* éticos o de sombrero blanco
   que hayas contratado tendrán las manos atadas
   y estarán limitados en su identificación de vulnerabilidades.
   Tendrán restringidas las posibilidades de explorar los caminos
   por los que podrían moverse los atacantes malintencionados
   y de detectar los problemas de seguridad que deberías remediar.
   Al final,
   si no encuentran nada significativo en las pruebas de penetración,
   seguramente se deberá a las limitaciones que impusiste al *red team*.
   En consecuencia,
   tu incertidumbre sobre la seguridad de tu organización podría aumentar o,
   para empeorar las cosas, podrías pensar,
   equivocadamente, que todo está protegido.

5. **Coherencia:**
   Si preguntas a los directivos "Entre disponibilidad y confidencialidad,
   ¿qué es lo más importante?"
   la mayoría de las veces, la respuesta será "ambas".
   Pero si les preguntas
   ¿Apagaría sus servidores ante la presencia de un atacante?".
   Responder "sí"
   situaría la confidencialidad por encima de la disponibilidad.
   Sin embargo,
   la respuesta típica es que mantendrían sus servidores en funcionamiento
   e intentarían lidiar con el atacante.
   Es habitual que en la lista de prioridades
   las organizaciones sitúen la disponibilidad por encima
   de la confidencialidad y la integridad.
   Aunque para ellas la disponibilidad
   es el elemento más importante de la tríada,
   resulta paradójico que muchas no autoricen a los *red teams*
   a evaluar sus capacidades defensivas contra ataques
   DoS (denegación de servicio).
   En este caso, la invitación es la siguiente:
   convierte tus restricciones en motivaciones
   para recibir ataques de un *red team*.
   De este modo,
   puedes comprobar con la ayuda de un aliado lo vulnerable
   que es tu empresa a los atacantes maliciosos.

## Conclusión

Aplicando estas simples políticas,
**ataques continuos**, **ataques de conocimiento cero**,
**respuesta implacable**, **intrusión total** y **coherencia**,
puedes averiguar cuán seguros son realmente tus sistemas,
mejorar su seguridad a un ritmo vertiginoso y ahorrar dinero.
No tienes que comprar tecnologías que generan informes
de vulnerabilidades enormes e incomprensibles,
muchos de ellos con falsos positivos y falta de contexto sobre
el impacto real de las vulnerabilidades en tu organización.

¿Te gustaría evaluar la seguridad de tus sistemas con
la ayuda del *red team* más grande de las Américas?
¡No dudes en [contactarnos](../../contactanos/)!
