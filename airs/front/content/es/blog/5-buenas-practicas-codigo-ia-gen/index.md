---
slug: blog/5-buenas-practicas-codigo-ia-gen/
title: Tips para desarrollar software con IA
date: 2023-12-29
subtitle: 5 buenas prácticas para escribir código usando la IA generativa
category: desarrollo
tags: ciberseguridad, código, pruebas de seguridad, tendencia
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1703875585/blog/5-best-practices-coding-with-gen-ai/cover_coding_with_gen_ai.webp
alt: Foto por Takahiro Sakamoto en Unsplash
description: Las herramientas de IA generativa son un aliado para que los desarrolladores escriban código eficientemente. Compartimos cinco buenas prácticas para desarrollar software seguro utilizando dichas herramientas.
keywords: IA Generativa, Herramientas De IA Generativa, Codigo Generado Por IA, Buenas Practicas, Pruebas De Seguridad, Cheat Sheet, Copilot, Hacking Etico, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Escritor y editor
source: https://unsplash.com/photos/five-flying-jet-planes-on-sky-at-daytime-IMued0tpO1s
---

Cuando se utiliza IA generativa para escribir código,
las buenas prácticas a seguir incluyen siempre revisar el resultado,
realizarle pruebas de seguridad,
no alimentar la herramienta con datos de clientes
o datos corporativos sensibles,
asegurarse de estar utilizando una herramienta aprobada por tu empresa
y tratar de no infringir las licencias de código abierto.
Dado que la IA generativa ha llegado para quedarse
como un aliado poderoso para la eficiencia de los proyectos de desarrollo,
es importante comprobar que se implemente de forma segura.

<image-block>

!["Cheat sheet Fluid Attacks - Buenas prácticas para desarrollar con IA"](https://res.cloudinary.com/fluid-attacks/image/upload/v1705958977/airs/es/blogs/im%C3%A1genes%20traducidas/5-buenas-practicas-codigo-ia-generativa/5-buenas-practicas-para-desarrollar-con-IA-2.webp)

*Cheat sheet*: Cinco buenas prácticas para desarrollar con IA generativa
de forma segura

</image-block>

## Revisar la respuesta tú mismo

Al igual que ocurre con los componentes de *software* de terceros
y de código abierto, la IA ofrece una forma de agilizar los proyectos
de desarrollo de *software*.
Por ejemplo,
puedes utilizar GitHub Copilot para escribir código rápidamente
gracias a que autocompleta las funciones de *software*
que deseas basándose en tus indicaciones.
Una investigación de GitHub sugiere que la IA
puede [aumentar tu eficiencia en un 55%](https://github.blog/2022-09-07-research-quantifying-github-copilots-impact-on-developer-productivity-and-happiness/#:~:text=developers%20who%20used%20GitHub%20Copilot).

La IA generativa también tiene la capacidad de permitir
que personas con conocimientos técnicos escasos desarrollen *software*.
Es más,
[se ha sugerido](https://www.techtarget.com/searchitoperations/news/366563975/DevSecOps-pros-prep-for-GenAI-upheavals-in-2024)
que la IA podría realizar las tareas de los desarrolladores júnior y,
aun así, servirles como herramienta de aprendizaje.
Además, se espera que un número significativo de compañías utilicen
la IA generativa para aliviar la carga de trabajo de los desarrolladores
y asignarles otros proyectos más importantes.

Ten por seguro que la IA generativa es una herramienta de apoyo a tu trabajo.
En su estado actual, no va a sustituirte.
De hecho,
siempre debes esperar que haya errores en el código generado por IA.
Siempre **deben** realizarse revisiones.
Examina bien el código, teniendo en cuenta las
[prácticas de codificación seguras](../practicas-codificacion-segura/).
En caso de duda,
pide a compañeros expertos que revisen el código generado
por la IA y te den su opinión antes de hacer *commit*.

## Realizar pruebas de seguridad al código generado por IA

La seguridad es un gran problema del código generado por IA.
Afortunadamente, existen herramientas
que pueden ayudarte a identificar sus vulnerabilidades.
Hemos enumerado algunas que son libres
y de código abierto (FOSS) en nuestro [artículo principal sobre pruebas de seguridad](../fundamentos-pruebas-de-seguridad/),
incluida [nuestra propia herramienta](https://help.fluidattacks.com/portal/en/kb/articles/configure-the-tests-by-the-standalone-scanner).
Si la aplicación que estás codificando está lista para ejecutarse,
puedes evaluarla con [pruebas de seguridad de aplicaciones dinámicas](../../producto/dast/)
(DAST)
además de hacerlo con [pruebas de seguridad de aplicaciones estáticas](../../producto/sast/)
(SAST).
Las primeras atacarán tu aplicación interactuando con ella "desde fuera".

También ayuda mucho contar con [*pentesters* o *hackers* éticos](../que-es-hacking-etico/)
que revisen el código y la aplicación en ejecución,
ya que el [escaneo de vulnerabilidades](../escaneo-de-vulnerabilidades/),
un proceso completamente automatizado,
es conocido por producir informes con altas tasas
de [falsos positivos](../../../blog/impacts-of-false-positives/)
y pasar por alto problemas de seguridad reales.
Estos *hackers*,
también conocidos como analistas de seguridad,
pueden encontrar vulnerabilidades verdaderas que,
de ser explotadas,
tendrían un terrible impacto en la disponibilidad,
confidencialidad e integridad de la información.

Una vez conocidas las vulnerabilidades,
hay que [remediarlas](../../../blog/vulnerability-remediation-process/).
De nuevo, puedes utilizar la IA generativa para arreglar el código.
De hecho,
recientemente hemos desplegado la función [Autofix](https://help.fluidattacks.com/portal/en/kb/articles/fix-code-automatically-with-gen-ai)
de nuestra extensión en VS Code.
Puedes obtener una **sugerencia** de corrección de código con un solo clic.
Pero incluso entonces tendrás que tener cuidado con el resultado,
ya que esta función aprovecha la IA generativa
y no puede escapar de sus limitaciones.
Por lo tanto,
debes revisar la sugerencia de corrección y someterla a pruebas de seguridad.

Si te interesa una prueba gratuita de
[pruebas de seguridad](../../soluciones/pruebas-seguridad/)
con nuestra herramienta automatizada,
haz clic [aquí](https://app.fluidattacks.com/SignUp) para empezar.

## Utilizar herramientas de IA aprobadas por tu compañía

Es importante que las organizaciones identifiquen
y supervisen el uso de la IA generativa en caso de que la permitan.
Tendrán que redactar políticas
y directrices empresariales sobre el uso de la IA
con la ayuda de sus equipos jurídicos y de propiedad intelectual.
Dado que la IA puede cometer errores de la misma forma
que lo han hecho los humanos al escribir código,
la gobernanza existente puede servir de base.
En un artículo específico del blog damos consejos sobre
lo que se debe incluir en
una [política de IA generativa](../6-items-principales-politica-ia-gen/)
Estas son algunas medidas que las organizaciones deben adoptar:

- Establecer las herramientas de IA que identifican como seguras,
  siendo conscientes de las limitaciones de esta tecnología.

- Educar a sus empleados sobre el uso de las tecnologías de IA generativa
  y las políticas relacionadas con ella.

- Establecer las soluciones de pruebas de seguridad que se utilizarán
  para mantener seguro el código generado por IA.

- Monitorear para detectar cualquier infracción de la propiedad intelectual.

Deberás estar al tanto de los requisitos de tu supervisor
o de otras personas de tu compañía
(p. ej., líder, jefe de producto, CTO, gerencia)
para informarles que estás utilizando herramientas de IA generativa.
Si no estás seguro de si puedes utilizarlas
o cuáles de ellas son adecuadas,
pregunta.

<div>
<cta-banner
buttontxt="Leer más"
link="/es/soluciones/pruebas-seguridad/"
title="Inicia ahora las pruebas de seguridad de Fluid Attacks"
/>
</div>

## No usar datos de clientes o propiedad intelectual en motores públicos de GPT

No envíes datos confidenciales de tu compañía
o de sus clientes que puedan ser ingresados en una nube
que está mucho más allá de tu control.
Básicamente,
el motor GPT se basaría en estos datos y acabaría sugiriéndolos
a usuarios ajenos a tu compañía.
Además,
se recomienda a los desarrolladores
de todo el mundo no introducir datos sensibles en los motores de GPT,
ya que se han producido fugas de datos.
(Contando algunas relacionadas con ChatGPT:
Samsung [expuso tres veces sus propios secretos](https://www.theregister.com/2023/04/06/samsung_reportedly_leaked_its_own/);
la explotación de una librería de código abierto defectuosa dio
a los atacantes [acceso al historial de chat](https://securityintelligence.com/articles/chatgpt-confirms-data-breach/)
de otros usuarios;
más de [100.000 cuentas fueron robadas](https://www.bleepingcomputer.com/news/security/over-100-000-chatgpt-accounts-stolen-via-info-stealing-malware/)
una vez.)
Por lo tanto,
ten en cuenta cualquier política de tu organización
que detalle cómo utilizar las herramientas de IA generativa
para el desarrollo.

## Tener cuidado con el *copyright* del código abierto con que se entrenó la IA

¿Existe la posibilidad de que estés copiando
el trabajo de otra persona al utilizar los resultados de la IA generativa?
Los problemas de violación de derechos de autor
(cuando el resultado copia textualmente elementos protegidos
por derechos de autor de *software* existente)
y las violaciones de licencias de código abierto
(p. ej., no proporcionar los avisos y declaraciones de atribución requeridos)
son nuevos y deben analizarse caso por caso
(ya existe una compleja
[demanda contra Copilot](https://www.theregister.com/2023/05/12/github_microsoft_openai_copilot/)).

Es cierto que Copilot, por ejemplo,
se ha entrenado con tantos repositorios de GitHub
que el código que sugiere puede no parecerse exactamente
a ningún código concreto protegido por derechos de autor
o que requiera créditos según su licencia de código abierto.
Aun así, existe un riesgo de infracción, y no se mitiga fácilmente,
ya que Copilot elimina en sus sugerencias la información
sobre derechos de autor,
gestión y licencia que exigen algunas licencias de código abierto.

Lo que te recomendamos es que pienses si podrías obtener
las funciones que necesitas utilizando
librerías de código abierto conscientemente.
Puedes seguir nuestros consejos para
[elegir bien el *software* de código abierto](../../../blog/choosing-open-source/).
Por cierto,
existen herramientas que te ayudarán a averiguar
si hay algún problema con el código de terceros
que introduzcas en tu proyecto.
Al realizar [análisis de composición de *software*](../../producto/sca/) (SCA),
estas herramientas identifican los componentes
que tienen vulnerabilidades conocidas
y plantean posibles conflictos de licencia.
Al [probar nuestra herramienta de forma gratuita](https://app.fluidattacks.com/SignUp)
obtendrás estos análisis, así como SAST y DAST.
Empieza ahora y no olvides [descargar](https://help.fluidattacks.com/portal/en/kb/articles/install-the-vs-code-extension)
nuestro plugin en VS Code para localizar fácilmente el código vulnerable
y obtener sugerencias de corrección de código a través de IA generativa.
