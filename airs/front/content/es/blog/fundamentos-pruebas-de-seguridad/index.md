---
slug: blog/fundamentos-pruebas-de-seguridad/
title: Los fundamentos de las pruebas de seguridad
date: 2023-11-01
subtitle: Conoce tipos, herramientas, técnicas, principios y mucho más
category: filosofía
tags: ciberseguridad, pruebas de seguridad, software, código
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1698867448/blog/security-testing-fundamentals/cover_security_testing.webp
alt: Foto por Erzsébet Vehofsics en Unsplash
description: Explicamos qué son las pruebas de seguridad y te contamos todos sus aspectos básicos. Entre ellos, cómo realizarlas para encontrar vulnerabilidades en aplicaciones de software y otros sistemas.
keywords: Pruebas De Seguridad, Desarrollo De Software, Seguridad De Aplicaciones, Vulnerabilidades, Herramientas De Pruebas De Seguridad, Codigo Fuente, Escaneo De Seguridad, Hacking Etico, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Escritor y editor
source: https://unsplash.com/photos/an-owl-sitting-on-top-of-a-wooden-post-Ql0D1hQeKB8
---

En nuestra vida cotidiana,
confiamos la protección de nuestra información personal
y otros datos sensibles a los sistemas informáticos
(y, por supuesto, a sus propietarios).
Estos datos están constantemente en peligro,
ya que los ciberdelincuentes (_hackers_ de sombrero negro/maliciosos)
quieren acceder a esos sistemas
y encontrar la forma de sacar provecho de ello.
Por eso es tan importante someter cada sistema a pruebas de seguridad.
Básicamente,
esto significa evaluar cada sistema para comprobar que su diseño
y configuración permiten la disponibilidad,
confidencialidad e integridad de los datos.
Tanto si eres propietario de una empresa,
desarrollador de _software_ o simplemente un individuo
que valora su privacidad en línea,
es esencial comprender las pruebas de seguridad.
Este artículo del blog presentará los fundamentos de las pruebas de seguridad:
qué son, qué tipos, herramientas y técnicas existen,
sus principios y mucho más.

## ¿Qué son las pruebas de seguridad?

Un sistema puede ser evaluado durante
todo su ciclo de vida de desarrollo de _software_ (SDLC),
es decir, durante todas las fases que van desde que se diseña
y se trabaja en él hasta que se pone a disposición
de los usuarios finales y le hace mantenimiento.
Lo que un humano o una máquina (en adelante, "herramienta")
evalúa varía dependiendo de lo que se esté enfocando.

Tomemos, por ejemplo,
una prueba automatizada que se ejecuta a medida
que el código fuente del sistema sufre modificaciones
y verifica que las líneas individuales
de código no superen una determinada longitud de caracteres.
Corregir las líneas que la prueba señala como no conformes
puede dar como resultado un código más fácil de leer.
En lo que se centra esta prueba es el formato.

Las pruebas de seguridad son pruebas de _software_ enfocadas
a verificar que los sistemas evaluados no tengan fallas,
ni de diseño ni de configuración,
que puedan permitir situaciones en las que el servicio
o la información no estén disponibles para los usuarios,
personas no autorizadas puedan acceder a datos
de cualquier grado de sensibilidad,
o puedan manipular la información.
Además,
las pruebas de seguridad implican la clasificación
y el informe de los hallazgos y la entrega de recomendaciones
para eliminar las vulnerabilidades.
Hay que tener en cuenta que tanto los humanos como las máquinas
pueden realizar pruebas de seguridad.

## Objetivos principales de las pruebas de seguridad

De acuerdo con la definición ofrecida anteriormente,
es fácil ver que las pruebas de seguridad pretenden lograr lo siguiente:

- Identificación de activos digitales

- Identificación y clasificación de vulnerabilidades de seguridad

- Descubrimiento de los posibles impactos
  de las vulnerabilidades si se explotan

- Reportes de los hallazgos para su remediación

- Recomendaciones para eliminar las vulnerabilidades

En definitiva,
si tuviéramos que condensar todo lo anterior para responder a cuál
es el único objetivo de las pruebas de seguridad, diríamos:
averiguar detalles sobre el estado de seguridad de un sistema de información.

## ¿Por qué son importantes las pruebas de seguridad?

Cuando nos preguntamos por la importancia de algo,
la pregunta puede plantearse así:
"¿Es importante tener ese algo?"
Qué diferencia conlleva tener consecuencias evidentes que indiquen éxito,
como el cumplimiento de los estándares y guías de seguridad que lo exijan,
una mayor calidad de _software_,
ya que la seguridad desempeña un papel importante en ella,
y la confianza de los usuarios.

Puede darse el caso en que algunas empresas consideren
estos logros como permanentes,
o al menos bastante duraderos,
por lo que podrían restar importancia a seguir realizando pruebas de seguridad.
Sin embargo, el panorama de las amenazas evoluciona continuamente.
Los _hackers_ maliciosos idean métodos perfeccionados
en sus ataques implacables a sistemas de todo el mundo.
[Realizar pruebas de seguridad continuamente](../../servicios/hacking-continuo/)
marca la diferencia.
Basta un incidente de ciberseguridad para darse cuenta de ello.

Los costos de los ciberataques en la actualidad
son más devastadores que nunca.
El hecho de que uno de los mayores amplificadores de costos
sea el [incumplimiento de las normativas](https://www.ibm.com/reports/data-breach)
resalta por qué son importantes las pruebas de seguridad.
En 2023,
el costo promedio global de una filtración
de datos batió el récord del 2022.
Ahora se sitúa en 4,45 millones de dólares.
Y los ataques no se limitan a las empresas grandes.
Las organizaciones de todo el mundo con menos de 500 empleados
reportaron impactos que costaron un promedio de 3,31 millones de dólares.
Para las pequeñas y medianas empresas,
sufrir las consecuencias de un ataque puede significar
fácilmente el fin de sus operaciones.

## Caso de estudio: Una costosa lección del poder de las pruebas de seguridad

Marriott International,
una de las principales cadenas hoteleras del mundo,
desafortunadamente fue objeto de
dos importantes filtraciones de datos en 2014 y 2020,
que dejaron al descubierto la información personal
de millones de huéspedes.
Estos incidentes sirven como un duro recordatorio del rol crítico
que cumplen las pruebas de seguridad para proteger datos sensibles
y prevenir brechas costosas de seguridad.

<image-block>

!["Estudio de caso: Marriott Int"](https://res.cloudinary.com/fluid-attacks/image/upload/v1707841025/airs/es/blogs/im%C3%A1genes%20traducidas/fundamentos-pruebas-de-seguridad/estudio-caso-marriott-int-pruebas-seguridad-fluid-attacks.png.webp)

Estudio de caso: Filtraciones de datos de Marriott International

</image-block>

## Los principios clave de las pruebas de seguridad

Ya que las pruebas de seguridad son una parte tan importante
de tu estrategia preventiva,
es recomendable que te esfuerces por aplicar lo que algunos
han denominado como los principios de las pruebas de seguridad.
Son los siguientes:

- **Cobertura:** Las pruebas de seguridad deben utilizar múltiples técnicas
  (como la evaluación del código fuente junto con la inspección
  desde el exterior del sistema en ejecución),
  evaluar varios requisitos de seguridad y abarcar todo el SDLC.

- **Pruebas realistas:** Las pruebas de seguridad deben incluir
  simulaciones de ataques realistas.

- **Continuidad:** Las pruebas de seguridad deben realizarse
  de forma continua incluso cuando la tecnología evaluada
  no experimente cambios frecuentes.

- **Colaboración:** Las pruebas de seguridad deben ser una actividad
  de colaboración entre los equipos de desarrollo, operaciones y seguridad.

## Tipos de pruebas de seguridad

Es curioso:
si uno navega por internet buscando los tipos de pruebas de seguridad,
se encuentra con un popurrí de artículos que en realidad
están en categorías de niveles diferentes.
Para no cometer el mismo error,
seguiremos lo que [hemos hecho antes en este blog](../tipos-de-pruebas-de-penetracion/),
que es presentar los tipos según los sistemas en evaluación.
Estamos de acuerdo solo en parte con las fuentes que hemos visto.
Además, solo presentaremos una selección de los tipos
de pruebas de seguridad más conocidas.

### Pruebas de seguridad de aplicaciones web

Cuando se evalúan aplicaciones web, los
[escáneres web](../escaneo-vulnerabilidad-aplicacion-web/)
y los humanos se centran especialmente en encontrar
las vulnerabilidades vinculadas a la lista
de [OWASP del top 10](../../../blog/owasp-top-10-2021/)
de riesgos principales para las aplicaciones web.
Por eso verifican el cumplimiento de lo siguiente:

- La aplicación utiliza versiones estables,
  examinadas y actualizadas de componentes de terceros.

- La aplicación descarta _inputs_ no seguros.

- La aplicación incluye cabeceras HTTP.

- La aplicación utiliza mecanismos preexistentes
  y actualizados para implementar funciones criptográficas.

- La aplicación define usuarios con privilegios.

- La aplicación requiere contraseñas y frases de acceso largas.

- La aplicación controla redirecciones.

Revisamos todo esto y mucho más para ayudarte
a [proteger tus aplicaciones web](../../../systems/web-apps/).

### Pruebas de seguridad de aplicaciones móviles

Al igual que las pruebas de aplicaciones web,
las [pruebas de aplicaciones móviles](../que-es-mast/)
a menudo siguen a OWASP como autoridad principal.
En este caso, se trata del OWASP Mobile Top 10.
Por consiguiente, el
[escaneo de vulnerabilidades](../escaneo-de-vulnerabilidades/)
y la [revisión manual](../que-es-prueba-de-penetracion-manual/)
comprueban si la aplicación cumple con lo siguiente:

- Tiene las funcionalidades inseguras desactivadas o controladas de otro modo.

- No expone los IDs de sesión en las URLs
  y los mensajes presentados al usuario.

- Mantiene los protocolos de comunicación (por ejemplo, Bluetooth) ocultos,
  protegidos con credenciales o desactivados.

- Solicita autenticación multifactor.

- Encripta la información sensible.

- Establece roles con distintos niveles de privilegio
  para acceder a los recursos.

- No tiene funciones, métodos o clases repetidas.

- No permite la inclusión de parámetros en nombres de directorios
  o rutas de archivos.

- Oculta los datos si no está enfocada.

- No tiene archivos inverificables
  (es decir, no incluidos en auditorías) en su código fuente.

Revisamos esto y mucho más para ayudarte
a [proteger tus aplicaciones móviles](../../../systems/mobile-apps/).

### Pruebas de seguridad de redes

El objetivo de este tipo de pruebas de seguridad
es detectar puntos débiles en los controles de seguridad de sitios web,
bases de datos, aplicaciones web y protocolos de transferencia de archivos.
A continuación se enumeran algunos de los requisitos evaluados:

- Los _firewalls_ filtran correctamente el tráfico,
  bloqueando el acceso no autorizado.

- Los sistemas de detección y prevención de intrusiones identifican,
  detienen, registran e informan eficazmente de posibles incidentes.

- Los controles de acceso de los usuarios están establecidos y son seguros.

- Los segmentos de red están aislados y configurados de forma segura.

- Los mecanismos de cifrado de datos están instalados y son seguros.

- Los equilibradores de carga
  y los _proxies_ están configurados de forma segura.

- La red es resistente a los ataques distribuidos de denegación de servicio
  (DDoS, por sus iniciales en inglés).

- Los dispositivos y sistemas de red están actualizados.

### Pruebas de API

Las interfaces de programación de aplicaciones
(API, por sus siglas en inglés)
sirven de pasarela para el intercambio de datos
entre sistemas de _software_ y aplicaciones.
Los productos de _software_ pueden estar
expuestos a ciberataques a través de sus APIs.
Por tanto, estas también deben someterse a pruebas.
A continuación se indican algunos requisitos de seguridad
en los que se centran las evaluaciones:

- Los mecanismos de autenticación
  (por ejemplo, _tokens_ de API, OAuth) se implementan correctamente.

- Las reglas de autorización están implementadas y son seguras.

- Los controles de privacidad de datos protegen la información
  durante la transmisión y el almacenamiento.

- El _input_ se valida para evitar la inyección de código malicioso.

- La API controla los dominios que pueden acceder a ella.

- Los mensajes de error no exponen información sensible.

- Los _endpoints_ de carga de archivos son seguros.

Revisamos esto y mucho más para ayudarte a [proteger tus APIs](../../../systems/apis/).

### Pruebas de seguridad de la infraestructura en la nube

La infraestructura en la nube
permite a los proyectos de desarrollo de _software_ construir
sus productos sobre arquitecturas personalizadas y modernas.
Al ser altamente escalable,
estos equipos pueden aumentar el uso de servidores
en la nube en sus aplicaciones para satisfacer la necesidad de rapidez
y eficacia de los usuarios.
Estos son algunos de los requisitos verificados en las pruebas:

- Las configuraciones de los recursos en la nube se atienen
  a las buenas prácticas de seguridad.

- Las políticas de gestión de identidades y accesos permiten autenticar
  y autorizar correctamente a los usuarios.

- Los procedimientos de gestión de claves protegen eficazmente
  las claves de cifrado y las credenciales.

- Las medidas adoptadas impiden el acceso no autorizado
  a las consolas de gestión de la nube.

- Las APIs utilizadas en el ambiente de la nube son seguras.

Revisamos esto y más para ayudarte
a [proteger tu infraestructura en la nube](../../../systems/cloud-infrastructure/).

## Otros términos relacionados con las pruebas de seguridad

Nos gustaría definir aquí algunos de los términos
que se han asociado a las pruebas de seguridad,
incluso a veces como "tipos de pruebas de seguridad",
pero que, en nuestra opinión,
podrían considerarse o bien un proceso más amplio
que incluye pruebas de seguridad,
un nombre elegante para la misma actividad o una actividad
dentro de las pruebas de seguridad.

### Evaluación de riesgos de ciberseguridad

Las evaluaciones de riesgos de ciberseguridad
se centran en cuantificar el impacto
y la probabilidad de posibles escenarios de riesgo.
El resultado deseado de una evaluación de riesgos
es un plan estructurado de gestión de riesgos que pueda guiar
a la organización para tomar decisiones sobre mejoras de seguridad,
asignación de recursos y mejora de sus activos digitales en general.
El plan debería incluir pruebas de seguridad como práctica habitual.

### Auditorías de seguridad

Las auditorías de seguridad
son exámenes periódicos de los sistemas de información,
las redes y la infraestructura física de una organización.
La auditoría se centra en las políticas
y procedimientos de seguridad establecidos para ver
cómo se adhieren a los estándares y buenas prácticas de la industria,
así como a los requisitos legales.
No la realizan necesariamente auditores o consultores independientes,
sino también auditores internos.

### Evaluación de la postura de seguridad

Las evaluaciones de la postura de seguridad
comprueban el estado actual de los controles técnicos
de seguridad de una organización.
Se realizan de forma periódica (trimestral o anualmente,
pero lo ideal es que sea continuamente)
para proporcionar información sobre la preparación de la empresa
para resistir ciberataques.
El resultado deseado es un informe actualizado
y detallado sobre las vulnerabilidades de seguridad encontradas,
junto con recomendaciones sobre priorización.
Suena a lo mismo que pruebas de seguridad, ¿no?

### Escaneo de vulnerabilidades

El [escaneo de vulnerabilidades](../escaneo-de-vulnerabilidades/)
es una actividad que se centra en encontrar fallas de seguridad
en el _software_ utilizando herramientas automatizadas.
Idealmente, se realiza de forma continua, examinando cada cambio.
Aunque en realidad debería hacerse independientemente
de si un proyecto de _software_ realiza cambios en su producto con regularidad,
dada la constante evolución de las ciberamenazas
y el descubrimiento de vulnerabilidades existentes desde hace mucho tiempo.
Básicamente, son pruebas de seguridad realizadas mediante herramientas.

### Hacking ético

El [_hacking_ ético](../../soluciones/hacking-etico/)
designa el trabajo del [_hacker_ ético](../etica-del-hacking/).
Y lo que hace el _hacker_ ético es utilizar todas
sus habilidades para encontrar las debilidades
de seguridad en los sistemas cuyos propietarios
han dado permiso para atacar.
El término se ha utilizado indistintamente
con “[_pentesting_](../que-es-prueba-de-penetracion-manual/)”
y “[_red teaming_](../ejercicio-red-teaming/).”
Estos no son más que metodologías en el ámbito del _hacking_ ético.
El primero
suele asociarse a un objetivo más general que el de _red teaming_.
Es más,
el _red teaming_ se ha vinculado en gran medida
a ejercicios en los que solo algunas personas de la organización atacada
conocen la autorización de los ataques.
Por cierto,
los ataques en estas dos metodologías de _hacking_ ético pueden incluir
[ingeniería social](../../../blog/social-engineering/).
Esta última técnica se refiere
al uso de la manipulación psicológica para persuadir
a la gente a revelar información sensible
o realizar acciones arriesgadas
(como visitar sitios web sospechosos o descargar archivos peligrosos).

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/pruebas-seguridad/"
title="Inicia ahora las pruebas de seguridad de Fluid Attacks"
/>
</div>

## Técnicas de pruebas de seguridad

Hay distintas formas de enfocar las pruebas de seguridad
de un producto de _software_.
Las pruebas se centran en un nivel determinado de análisis
de un sistema dependiendo, por ejemplo,
de si se concede un acceso inicial al código fuente
o a la documentación del sistema (pruebas de caja blanca)
o no (pruebas de caja negra).
Lo que puede dar más profundidad a las pruebas de seguridad
es utilizar el método manual junto con el automatizado.
Es decir,
permitir que _hackers_ éticos revisen el código
o intenten _hackear_ ellos mismos el sistema.
Las siguientes son las técnicas que seguramente
oirás hablar más en el sector de la ciberseguridad.

### Pruebas de seguridad de aplicaciones estáticas (SAST)

[SAST](../../producto/sast/) es un tipo de prueba de caja blanca
y también se conoce como análisis estático
o análisis estático de código fuente.
Cuando lo realiza una [herramienta SAST](../herramientas-devsecops/),
el programa escanea el código para encontrar
cualquier cosa que coincida con errores conocidos
que ha almacenado en una base de datos.
Para ello utiliza funciones sofisticadas
(p. ej., reconocimiento de patrones, análisis de flujo de datos,
análisis de flujo de control).
Cuando se hace manualmente, el analista de seguridad,
que conoce el contexto del sistema evaluado,
puede encontrar problemas de seguridad que la herramienta no podría detectar.

### Pruebas de seguridad de aplicaciones dinámicas (DAST)

Si te preguntas por el uso de las palabras "estática"
y "dinámica" en esta técnica y en la anterior,
te explicamos su significado:
"estática" significa que el sistema se evalúa sin ejecutarse,
mientras que "dinámica" significa que el sistema
se evalúa mientras se está ejecutando.
[DAST](../../producto/dast/) es un tipo de prueba de caja negra
y también se conoce como análisis dinámico.
Evalúa las aplicaciones
en funcionamiento que ya están en una máquina virtual,
un servidor web o un contenedor básicamente atacándolas.
Ya sea por una herramienta DAST o por un _hacker_,
la técnica funciona enviando vectores de ataque,
como cadenas de código a los puntos finales de la aplicación
para intentar desencadenar comportamientos inesperados e indeseables.
Al igual que con SAST,
realizar DAST manualmente junto con los escaneos
de la herramienta puede ayudar a reducir significativamente
la tasa de falsos negativos.

### Análisis de composición de _software_ (SCA)

El _software_ no tiene por qué ser completamente original.
En realidad, sería engorroso (e incluso una decisión peligrosa)
reinventar la rueda mientras existen componentes
de _software_ de código abierto seguros,
ampliamente utilizados y respaldados por la comunidad
para conseguir la funcionalidad deseada en tu producto de _software_.
Por lo general, esto se sigue,
y hasta tal punto que los proyectos de desarrollo de _software_
pueden perder la pista de los componentes de código abierto en uso y,
por tanto, desconocer su estado de seguridad
y cuáles necesitan actualización o sustitución.
El [SCA](../../producto/sca/) aborda estos problemas.

El análisis de la composición de _software_
es otro tipo de prueba de caja blanca.
Las [herramientas de SCA](../herramientas-devsecops/)
identifican los componentes de _software_ de terceros utilizados
en el producto de _software_ evaluado y elaboran reportes sobre ellos,
informando de su estado de seguridad
y si existen conflictos de licencia
(p. ej., una biblioteca exige que los proyectos de _software_
que los utilicen pongan su código fuente a libre disposición
y la organización no lo desea).
Un inventario de los componentes encontrados
es lo que actualmente se conoce como una lista de materiales de _software_
(SBOM, por su nombre en inglés).

### La seguridad en la nube (CSPM)

Dado que las desconfiguraciones de los servicios en la nube son
[errores comunes y peligrosos](../../../blog/microsoft-38tb-data-leak/),
es importante llevar a cabo
[evaluaciones de vulnerabilidades](../evaluacion-de-vulnerabilidades/)
en los sistemas e infraestructuras basados en la nube para identificar,
priorizar y remediar este tipo de problemas.
Los escaneos [CSPM](../../../blog/what-is-cspm/)
son entonces el camino a seguir.
Implican pruebas tanto de caja blanca como de caja negra dirigidas
a _scripts_ de infraestructura como código (IaC),
imágenes de contenedores y ambientes de ejecución
para verificar el cumplimiento de los requisitos de seguridad.
Estos últimos incluyen las políticas personalizadas de la organización
sobre su uso de los servicios en la nube.

### Pentesting manual (MPT)

Pasamos ahora a las técnicas que las herramientas no pueden realizar.
Pertenecen al campo del [_hacking_ ético](../que-es-hacking-etico/),
que se refiere al trabajo de analistas de seguridad altamente capacitados para,
esencialmente, identificar y explotar problemas de seguridad
en los sistemas para obtener acceso a ellos
con el permiso de los responsables de los sistemas.
[MPT](../que-es-prueba-de-penetracion-manual/) son pruebas de seguridad
en las que los _hackers_ éticos crean ataques personalizados
para eludir las defensas, simulando las tácticas,
técnicas y procedimientos que caracterizan los ataques
de los ciberdelincuentes.
Pueden ser pruebas de caja blanca y de caja negra
y reciben muchos nombres, como "_pen testing_", "_pentesting_",
"_pentesting_ manual" o simplemente "pruebas de penetración".
En cualquier caso,
todos estos términos apuntan a un enfoque poderoso,
ya que encuentra más [exposición al riesgo](https://fluidattacks.docsend.com/view/h5vcqn8eh8pdrguj)
que el escaneo,
y esta mayor proporción corresponde a menudo
a las vulnerabilidades más complejas.
Esto se debe en parte a que las herramientas se ven limitadas
a la hora de encadenar una vulnerabilidad detectada
con alguna otra para lograr un descubrimiento
de mayor impacto en comparación con el que se consigue explotando
únicamente la vulnerabilidad inicial.
La automatización aún no supera a los humanos
cuando se trata de simular ciberataques.

### Revisión de código seguro (SCR)

[SCR](../revision-codigo-fuente/)
es cuando el código fuente de una aplicación o cualquier
otro sistema se somete al escrutinio de _hackers_ éticos
o _pentesters_ para identificar vulnerabilidades de seguridad.
Esta técnica de pruebas de caja blanca es un trabajo manual que,
en comparación con SAST automatizadas,
puede detectar vulnerabilidades más complejas y críticas para el negocio.
Esto, debido al conocimiento que tienen
los _hackers_ éticos de la lógica del negocio.

### Ingeniería inversa (RE)

La última técnica de pruebas de caja negra,
y de hecho la última de esta lista,
que creemos que verás mencionada en otros lugares con más frecuencia es la
[ingeniería inversa de _software_](../ingenieria-inversa/).
Como su nombre indica,
consiste en observar productos de _software_ desde fuera
para descifrar su código fuente
(es decir, irse hacia atrás en los pasos del desarrollo)
y puede centrarse en funciones de seguridad específicas y muy importantes,
como los algoritmos de cifrado, cuya resistencia
a ser hackeados es de gran interés
para los proyectos de desarrollo de _software_.

## Herramientas de pruebas de seguridad

Como ya mencionamos,
las pruebas de seguridad pueden ser realizadas por herramientas,
automáticamente, y por humanos, manualmente.
Los productos de _software_ existentes
para las pruebas de seguridad automatizadas
(también conocidas como [escaneo de vulnerabilidades](../escaneo-de-vulnerabilidades/))
son demasiados para enumerarlos en este artículo.
Sin embargo,
podemos ofrecer una lista de herramientas que son de código abierto
y libre (FOSS) y que siguen muchas de las buenas prácticas recopiladas
por Open Source Security Foundation (OpenSSF).

- [**Machine de Fluid Attacks:**](https://help.fluidattacks.com/portal/en/kb/articles/configure-the-tests-by-the-standalone-scanner)
  Se trata de una aplicación de interfaz de comandos
  [recomendada](../escaneo-estatico-aprobado-por-casa/)
  por el marco [Cloud Application Security Assessment](https://appdefensealliance.dev/casa/tier-2/ast-guide/static-scan)
  (CASA) de Google que ha conseguido
  [un 100% en verdaderos positivos y un 0% en falsos positivos](../../../blog/owasp-benchmark-fluid-attacks/)
  en el OWASP Benchmark versión 1.2.
  y una [insignia de oro en las buenas prácticas de OpenSSF](../../../blog/openssf-gold-badge-for-universe/).
  Realiza SAST, DAST, CSPM y SCA automatizados en código fuente escrito
  en cualquiera de los múltiples lenguajes de programación.

- [**OWASP® Zed Attack Proxy (ZAP):**](https://www.zaproxy.org/)
  Es un [escáner de vulnerabilidades de aplicaciones web](../escaneo-vulnerabilidad-aplicacion-web/)
  [recomendado por CASA](https://appdefensealliance.dev/casa/tier-2/ast-guide/dynamic-scan)
  que realiza DAST.
  Tiene una [insignia de aprobación en las buenas prácticas de OpenSSF](https://www.bestpractices.dev/es/projects?q=zed%20attack).

- [**Horusec:**](https://horusec.io/site/)
  Es una herramienta para realizar SAST que soporta múltiples lenguajes
  de programación y ha obtenido
  una [insignia de aprobación en las buenas prácticas de OpenSSF](https://www.bestpractices.dev/es/projects/5146).

- [**SpotBugs:**](https://spotbugs.github.io/)
  Es una herramienta para realizar análisis estáticos
  en aplicaciones web escritas en Java y apps de Android.
  [Según un estudio del 2021](https://arxiv.org/pdf/2112.04037.pdf),
  su porcentaje de detección es del 53,8%.
  Ha implementado muchas de las buenas prácticas de OpenSSF,
  pero [aún no ha obtenido la insignia de aprobación](https://www.bestpractices.dev/es/projects/3224).

- [**Flawfinder:**](https://dwheeler.com/flawfinder/)
  Es una herramienta para escanear código fuente escrito en C y C++.
  Tiene una [insignia de aprobación en las buenas prácticas de OpenSSF](https://www.bestpractices.dev/es/projects/323).

- [**Railroader:**](https://railroader.org/)
  Es una herramienta para analizar el código
  de aplicaciones desarrolladas en el marco de trabajo Rails,
  que se utiliza para programar aplicaciones web en el lenguaje Ruby.
  Tiene una [insignia de aprobación en las buenas prácticas de OpenSSF](https://www.bestpractices.dev/es/projects/2514).

También hay muchas herramientas para analizar aplicaciones de iOS y Android.
OWASP las ha recopilado [aquí](https://github.com/OWASP/owasp-mastg/tree/master/tools).
Sus criterios han sido que las herramientas sean FOSS,
capaces de analizar aplicaciones recientes,
actualizadas regularmente y que cuenten con un fuerte respaldo de la comunidad.

## El impacto de los falsos positivos y negativos en las pruebas de seguridad

Aunque de distintas maneras,
tanto los falsos positivos como los falsos negativos
pueden restarle eficacia a las pruebas de seguridad.

Los falsos positivos pueden provocar interrupciones en la construcción,
creando retrasos y contratiempos innecesarios.
Además, la repetición de falsos positivos
puede hacer que los desarrolladores pierdan
la confianza en las herramientas o métodos de seguridad,
lo que podría llevarles
a [ignorar advertencias legítimas en el futuro](../../../blog/impacts-of-false-positives/).
Investigar alarmas falsas también consume tiempo y recursos valiosos,
estancando la creación de nuevo material.

Por su parte,
los falsos negativos pueden generar una falsa sensación de seguridad
y, además,
no identificar las vulnerabilidades reales
puede dejar al _software_ vulnerable a ser explotado por los atacantes.
Lograr encontrar una solución que garantice
minimizar los falsos positivos y los falsos negativos por igual
es crucial para mantener una postura de seguridad robusta.

## Cómo realizar pruebas de seguridad

Las pruebas de seguridad deben ser una actividad planificada
en todo proyecto de desarrollo de _software_.
De acuerdo con la [metodología DevSecOps](../concepto-devsecops/),
los desarrolladores deben ser conscientes de la seguridad
de los productos de _software_ desde el principio del proyecto
y actuar siempre proactivamente a su favor.
Esto incluye saber que podrían tener
que rehacer rápidamente el código que han escrito cuando
se descubra que tiene vulnerabilidades.
Por este motivo,
agradecerán que los productos utilizados
para las pruebas de seguridad sean precisos
(muestren tasas bajas índices de falsos positivos y falsos negativos).

El proyecto necesita identificar primero las herramientas
y los humanos que evaluarán el producto de _software_ en cada fase del SDLC.
Hemos escrito extensamente en otro artículo
sobre [qué técnicas utilizar](../herramientas-devsecops/)
en cada fase temprana del SDLC.
Básicamente,
las técnicas de prueba de caja blanca se pueden aprovechar
cuando los desarrolladores acaban de empezar a escribir código.
Más tarde,
cuando un artefacto está listo para ser desplegado en ambientes de prueba,
se pueden utilizar técnicas de prueba de caja negra.
Se recomienda una combinación de pruebas automatizadas y manuales.

El proyecto también debe establecer criterios
y políticas sobre cuánta exposición al riesgo debe ser capaz
de tolerar y durante cuánto tiempo.
De este modo,
pueden priorizar la remediación de un determinado grupo de vulnerabilidades,
basándose en los resultados de las pruebas de seguridad
sobre la exposición al riesgo asociada a ellas.
Deben realizarse pruebas automatizadas
en los _pipelines_ de integración continua
y despliegue continuo para que los _builds_ sean entregados
a los usuarios finales cuando dichos _builds_ no cumplan
con las políticas del proyecto.

La remediación debe realizarse poco después
de que se detecten los problemas de seguridad.
De este modo,
se dispondrá de un breve periodo de tiempo durante
el cual podría producirse un ataque.
Después,
cuando se considere que se ha eliminado el problema de seguridad
o se haya encontrado una solución,
el proyecto debería volver a realizar pruebas de seguridad
para verificar que se ha mitigado eficazmente la exposición al riesgo.

## Cómo realizamos las pruebas de seguridad

Ofrecemos Hacking Continuo para que los proyectos
de desarrollo de _software_ protejan sus productos
de forma continua desde las primeras fases del SDLC.
Realizamos SAST, DAST, SCA, CSPM, MPT (o PTaaS), SCR y RE
para proporcionar información precisa
y detallada sobre la exposición al riesgo del producto de _software_.
[Ofrecemos dos planes](../../planes/).
En el plan Essential,
las pruebas de seguridad las realiza únicamente nuestra herramienta.
La herramienta presenta unos índices
muy bajos de falsos positivos y falsos negativos,
está recomendada por el marco CASA
y ha obtenido la insignia de oro de las buenas prácticas de OpenSSF.
En el plan Advanced,
las pruebas de seguridad las realizan nuestras herramientas
y nuestro equipo de *pentesters*,
el cual cuenta con [varias certificaciones en seguridad ofensiva](../../../certifications/).

Desde el principio,
el proyecto puede visualizar el mapeo de la superficie de ataque
y los resultados de las pruebas de seguridad en nuestra plataforma.
También puede gestionar los problemas de seguridad encontrados.
A través de nuestra extensión para el IDE en VS Code,
los desarrolladores pueden ver cómodamente las líneas de código inseguras.
Con el clic de un botón,
pueden obtener guías creadas
por IA generativas para remediar con éxito las vulnerabilidades.
Si están suscritos a nuestro plan estrella, plan Advanced,
los desarrolladores pueden hablar con nuestros *pentesters*
para recibir su ayuda.
En ambos planes,
ofrecemos reataques para verificar
que los problemas se han resuelto realmente.

## Beneficios de las pruebas de seguridad con Fluid Attacks

Cuando los proyectos de desarrollo de _software_
eligen Fluid Attacks para proteger sus productos,
obtienen los siguientes beneficios:

- Pruebas de seguridad continuas con varias técnicas

- Informes de vulnerabilidades exhaustivos y continuos

- Tasas mínimas de falsos positivos

- Gestión de vulnerabilidades desde una única plataforma

- Detección de vulnerabilidades complejas y un estado preciso
  y actualizado de la exposición al riesgo

- Orientación para la remediación mediante IA generativa y ayuda de expertos

Te invitamos a iniciar una [prueba gratuita](https://app.fluidattacks.com/SignUp)
de escaneo de seguridad con nuestra herramienta automatizada certificada.
Puedes cambiar en cualquier momento al plan que incluye técnicas manuales.
