---
slug: blog/que-es-hacking-etico/
title: ¿Qué es el hacking ético?
date: 2022-04-11
subtitle: Conceptos clave, cómo funciona y por qué es importante
category: filosofía
tags: ciberseguridad, hacking, pentesting, red-team, pruebas de seguridad, vulnerabilidad
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1649710011/blog/what-is-ethical-hacking/cover_ethical_hacking.webp
alt: Foto por Bruno Kelzer en Unsplash
description: Aprende qué es el *hacking* ético y algunos conceptos clave relacionados, cómo trabajan los hackers éticos y cuáles son los beneficios de esta metodología de evaluación.
keywords: Hacking Etico, Red Team, Sombrero Blanco, Sombrero Negro, Hacker, Vulnerabilidad, Ciberataque, Introduccion, Pentesting
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/J1sd2zy87rc
---

Como ya sabrás,
Fluid Attacks es una empresa especializada
en [_hacking_ ético](../../soluciones/hacking-etico/).
Somos un gran [_red team_](../../soluciones/red-team/),
un equipo de seguridad ofensivo con la misión de detectar vulnerabilidades
de seguridad en [sistemas informáticos](../../../systems/).
Como hace poco nos dimos cuenta de que no teníamos un artículo de blog
informativo e introductorio sobre lo que es el _hacking_ ético,
decidimos crearlo.
Este artículo está dirigido principalmente a quienes se inician
en el tema y desean contar con una introducción.
Está basado parcialmente en un reciente taller impartido por nuestro
Vicepresidente de Hacking [Andres Roldan](https://co.linkedin.com/in/andres-roldan),
a un grupo de periodistas.

## Conceptos clave del _hacking_ ético

### ¿Qué es la ciberseguridad?

[Se calcula que](https://datareportal.com/global-digital-overview)
casi cinco mil millones de personas utilizan actualmente la Internet,
lo que corresponde a cerca del 63% de la población mundial.
Además, alrededor del 92% de estos usuarios, en algún momento,
prácticamente desde cualquier lugar,
tienen acceso a la red a través de dispositivos móviles.
No cabe duda de que nos encontramos
en un mundo digital altamente conectado en el que,
al igual que en la realidad " material ",
las amenazas existen [desde el primer momento](../../../blog/first-cyberattack/).
Frente a las amenazas constantes,
la **ciberseguridad** se ha hecho necesaria.
[Gartner](https://www.gartner.com/en/information-technology/glossary/cybersecurity),
parcialmente acertado,
define este término como "la combinación de personas, políticas,
procesos y tecnologías empleadas por una empresa
para proteger sus activos cibernéticos".
(Digo "parcialmente" porque también es cierto que,
como usuario individual, se puede acceder a la ciberseguridad).
Pero,
¿contra qué deben protegerse los ciberactivos? —Ciberataques.

### ¿Qué son los ciberataques?

[Son asaltos](https://www.checkpoint.com/es/cyber-hub/what-is-cyber-attack/)
llevados a cabo por **ciberdelincuentes** que atacan uno
o varios sistemas informáticos desde uno o varios computadores.
Los ciberataques pueden desactivar los sistemas de las víctimas,
robar sus datos o utilizarlos como puntos de partida para otros asaltos.
Según un [informe de seguridad de IBM](https://www.ibm.com/security/data-breach/threat-intelligence/),
entre los principales tipos de ciberataques (tácticas)
del año pasado figuran los siguientes:
[_ransomware_](../../../blog/ransomware/),
acceso no autorizado a servidores,
[amenaza a correos electrónicos empresariales](../../../blog/cost-cybercrime-ii/),
robo de datos y captura de credenciales.
Y entre las técnicas más utilizadas para lograr estos objetivos
se encuentran las siguientes:
[_phishing_](../../../blog/phishing/),
explotación de vulnerabilidades,
robo de credenciales,
[fuerza bruta](../../../blog/pass-cracking/) y acceso remoto.

### ¿Qué es _hacking_?

Los ciberataques pueden considerarse una parte del _hacking_,
el cual es principalmente el proceso de identificar
y explotar problemas de seguridad en los sistemas
para lograr acceder a ellos sin autorización.
Esta actividad la llevan a cabo los _hackers_,
quienes suelen dividirse en dos tipos o grupos.

### ¿Cuáles son los tipos de _hackers_?

Muchos de los ciberdelincuentes
que perpetran los asaltos son los llamados **_hackers_ maliciosos**,
actores de amenazas o **_hackers_ de sombrero negro**.
Entre sus principales motivaciones está la idea
de obtener alguna recompensa económica.
También pueden atacar simplemente para expresar su desacuerdo
con las decisiones de gobiernos o empresas.
Además, hay ataques derivados del simple deseo de los _hackers_
de asumir riesgos y lograr reconocimiento en determinados grupos de personas.
A veces,
los ciberdelincuentes son incluso contratados por empresas deshonestas
para arruinar proyectos y afectar la reputación de sus rivales.
Algo parecido ocurre entre gobiernos
(p. ej., [la ciberguerra entre Rusia y Ucrania](../../../blog/timeline-new-cyberwar/)).
(Si quieres saber más sobre cómo piensan los _hackers_,
[lee este artículo del blog](../../../blog/thinking-like-hacker/).)
En todo caso,
en un universo en el que podemos experimentar montones
de estímulos contrarios, es de esperar que haya **sombreros blancos**
si hay sombreros negros.
Es decir, si hay _hackers_ maliciosos, también hay **_hackers_ éticos**.

### ¿Qué es un _hacker_ ético?

Un _hacker_ ético o _hacker_ de sombrero blanco
es un experto en ciberseguridad ofensiva
que ejecuta ciberataques principalmente a favor de la seguridad de los demás.
Si todavía tiene en mente la pregunta
¿En qué se diferencian los _hackers_ éticos de los _hackers maliciosos_?,
te invitamos a leer nuestro
artículo [“Ética del _hacking_”](../etica-del-hacking/).
Allí explicamos cómo ambos grupos
pueden compartir cualidades como la curiosidad,
la astucia y la paciencia, pero son muy diferentes
cuando se trata de sus propósitos
y los efectos o consecuencias de sus acciones.

### ¿Cómo convertirse en un _hacker_ ético?

[Ya tenemos un artículo en el blog](../../../blog/how-to-become-an-ethical-hacker/)
en el que respondemos a esta pregunta.
Allí te contamos sobre las habilidades técnicas
y no técnicas que deberías poseer para ser un _hacker_ ético
y sobre las [certificaciones](../../../certifications/)
que deberías obtener para mejorar tus competencias y reconocimiento.

## Definición de _hacking_ ético

**El _hacking_ ético es quizás
la mejor manera de hacer frente al _hacking_ malicioso**.
Los _hackers_ éticos examinan y atacan sistemas para descubrir,
y a veces explotar, sus vulnerabilidades copiando las tácticas,
técnicas y procedimientos de los actores de amenazas.
La gran diferencia es que el ataque se efectúa
con el consentimiento del propietario del sistema,
quien será responsable de remediar
las vulnerabilidades de seguridad notificadas.
El _hacking_ ético es, por tanto,
una metodología de evaluación de sistemas informáticos
en la que las pruebas pretenden simular con la mayor precisión posible
los métodos de ataque de los ciberdelincuentes,
pero no para hacer daño,
sino para permitir a las organizaciones superar sus debilidades
y optimizar su protección de operaciones y activos.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/hacking-etico/"
title="Empieza ya con la solución Hacking Ético de Fluid Attacks"
/>
</div>

En el _hacking_ ético,
los expertos deben mantenerse al día sobre la existencia
y el uso de herramientas de _hacking_,
así como sobre las tendencias de ataque utilizadas por los adversarios.
En sus informes,
los _hackers_ éticos proporcionan información
sobre las vulnerabilidades identificadas, incluyendo su grado de criticidad.
Para ello siguen marcos públicos como [CVE](../../../compliance/cve/)
Y [CVSS](https://www.first.org/cvss/).
También aportan pruebas de la explotación de vulnerabilidades
y de qué activos de información pueden verse comprometidos en un ataque.
Más allá de encontrar vulnerabilidades conocidas,
los _hackers_ éticos también pueden realizar investigaciones
para descubrir y registrar vulnerabilidades de día cero,
es decir, nuevas amenazas desconocidas hasta el momento.

## _Hacking_ ético vs. _pentesting_

El _hacking_ ético es un concepto bastante amplio que,
aunque a menudo se utiliza indistintamente con términos
como [_pentesting_](../que-es-prueba-de-penetracion-manual/)
y _red teaming_,
sería más exacto decir que engloba ambas metodologías.
Vamos a aclarar esto.

Hemos dicho una y otra vez que las pruebas de penetración
son un tipo de pruebas de seguridad
en las que la intervención humana es esencial.
(A pesar de que muchos proveedores afirman
que realizan un tipo de "_pentesting_ automatizado".)
Las personas capacitadas para realizar _pentesting_, es decir,
para comprender la infraestructura
y la funcionalidad de distintos sistemas informáticos,
reconocer y detectar sus vulnerabilidades de seguridad y,
si es necesario,
aprovecharlas para proporcionar muestras tangibles de posibles impactos,
pueden ser identificadas como _hackers_.
Por lo tanto,
un _hacker_ ético que hace _pentesting_ está haciendo _hacking_ ético.

Lo que a veces sugieren algunas personas
como diferencia entre el _hacking_ ético
y las pruebas de penetración es la amplitud del alcance de sus pruebas.
Se dice que el _pentesting_ está más orientado
a evaluar sistemas específicos con métodos predefinidos
para verificar que cumplen con requisitos de estándares
de seguridad para determinados activos,
mientras que el _hacking_ ético tiene
como objetivo una cobertura más amplia,
y sus métodos pueden no estar predefinidos.
(Esto es similar a [la comparación que ya hicimos](../ejercicio-red-teaming/)
entre _red teaming_ y _pentesting_).
Sin embargo,
ambos procedimientos son tareas realizadas por _hackers_ éticos.
Así que, volviendo a la afirmación inicial,
ambos pueden considerarse _hacking_ ético.
En definitiva,
el _pentesting_ es una forma de _hacking_ ético.

## Tipos de _hacking_ ético

Entonces, en relación con el apartado anterior,
es curioso encontrar en internet que algunas compañías
o medios de comunicación nos dicen que los tipos de _hacking_ ético
varían según los sistemas que se pretendan evaluar
(p. ej., aplicaciones web, redes, IoT).
¿No es esto lo que debería hacerse en su lugar
para separar los tipos de _pentesting_
(procedimiento enfocado en sistemas específicos)?
De hecho, eso es algo que ya hizo Chavarría
[en otro de los artículos de nuestro blog](../tipos-de-pruebas-de-penetracion/).
(Si quieres entender la diferencia entre _pentesting_ blanco, gris y negro,
[lee este artículo](../que-es-prueba-de-penetracion-manual/).)
Aquí, podemos decir que entre los tipos de _hacking_ ético,
podríamos incluir las pruebas de penetración y el _red teaming_,
pero la intención es no enredar las cosas.

### 5 fases del _hacking_ ético

Para que se lleve a cabo el proceso de _hacking_ ético,
el propietario de los sistemas debe definir
y aprobar previamente una superficie de ataque
y un objetivo de evaluación
(es decir, una parte o la totalidad de la superficie de ataque).
Los objetivos pueden ser aplicaciones [web](../../../systems/web-apps/)
o [móviles](../../../systems/mobile-apps/),
[APIs y microservicios](../../../systems/apis/),
[clientes robustos](../../../systems/thick-clients/),
[infraestructura en la nube](../../../systems/cloud-infrastructure/),
[redes y _hosts_](../../../systems/networks-and-hosts/),
[dispositivos IoT](../../../systems/iot/)
y tecnología operativa.
La metodología de _hacking_ ético comúnmente utilizada
puede dividirse en fases de
reconocimiento, enumeración, análisis, explotación y reporte.

1. **Fase de reconocimiento:**
   En la **fase de reconocimiento pasivo**,
   los _hackers_ éticos recopilan información de fuentes externas
   sin interactuar directamente con el objetivo.
   Emplean, por ejemplo, técnicas de recopilación de
   [Open Source Intelligence](../../../blog/social-engineering/)
   (es decir, información disponible públicamente)
   Pueden recurrir a buscadores web comunes como Google y Bing
   para descubrir detalles relevantes sobre el objetivo.
   Debido a las características de esta fase,
   hay pocas posibilidades de que los _hackers_ sean detectados.

   En la **fase de reconocimiento activo**,
   los _hackers_ éticos
   ya tienen contacto directo con el objetivo.
   Identifican fuentes de información y tecnología pertenecientes
   a la organización propietaria del sistema en evaluación.
   Interactúan con los servicios,
   sistemas e incluso personal de la organización
   para recoger datos y definir vectores de ataque.
   Las posibilidades de que los _hackers_ sean descubiertos
   aumentan considerablemente si comparamos esta fase con la anterior.

2. **Fase de enumeración:**
   En esta fase,
   los _hackers_ éticos se proponen bosquejar el estado de seguridad
   del objetivo y prepararse para el ataque.
   Identifican sus puntos fuertes y débiles y comienzan
   a prever los posibles impactos que pueden derivarse del asalto.
   Según las características particulares del objetivo,
   los _hackers_ preparan un arsenal especial para él.

3. **Fase de análisis:**
   En esta fase,
   los _hackers_ éticos se encargan de determinar
   el impacto exacto de atacar cada una
   de las vulnerabilidades que han identificado.
   Evalúan cada escenario y vector de ataque,
   así como las dificultades de explotación.
   Tienen en cuenta el daño a la integridad,
   confidencialidad y disponibilidad del objetivo en cada caso.
   Además, los _hackers_ examinan el posible impacto
   en los sistemas cercanos al objetivo.

4. **Fase de explotación:**
   Según Roldán,
   es en esta fase donde el _hacking_ ético
   se diferencia del funcionamiento de las
   herramientas automatizadas de pruebas de seguridad.
   La herramienta se limita a identificar vulnerabilidades,
   mientras que el _hacker_ ético pretende explotarlas
   para alcanzar objetivos de alto valor
   dentro de su objetivo de evaluación.
   De este modo,
   los _hackers_ pueden identificar los efectos reales que un ciberdelincuente
   podría conseguir explotando estas vulnerabilidades.

5. **Fase de reporte:**
   Una vez finalizada la explotación,
   los _hackers_ éticos tienen que presentar los resultados
   a todas las partes interesadas.
   Uno de los entregables de los _hackers_ es un resumen ejecutivo,
   gracias al cual los directivos de la organización propietaria
   del objetivo pueden comprender fácilmente los riesgos identificados.
   A partir de este informe,
   pueden gestionar los procesos para mitigar los riesgos.
   Otro entregable es un resumen técnico para que los desarrolladores
   u otros profesionales puedan comprender en detalle
   cada vulnerabilidad y proceder a su remediación.

## Herramientas de _hacking_ ético

En [nuestro artículo principal sobre _red teaming_](../ejercicio-red-teaming/),
ya habíamos proporcionado una lista de 23 herramientas utilizadas
por los _hackers_ para las diferentes fases de esa forma de evaluación,
la cual, como dijimos, puede ser vista como un tipo de _hacking_ ético.
Ya que puedes acceder a esa lista, en este caso,
queremos complementarla con algunas otras herramientas
utilizadas por nuestros _hackers_ éticos
en nuestro [plan Advanced](../../planes/)
de [Hacking Continuo](../../servicios/hacking-continuo/).

- **Amass:**
  [Amass](https://github.com/owasp-amass/amass)
  es un [proyecto OWASP](https://owasp.org/www-project-amass/)
  para realizar mapeos de red de superficies de ataque
  y encontrar activos externos.
- **Apktool:**
  [Apktool](https://apktool.org/)
  es una [herramienta de código abierto](https://github.com/ibotpeaches/apktool/)
  para realizar [ingeniería inversa](../ingenieria-inversa/)
  en archivos APK (Android Package Kit).
- **bettercap:**
  [bettercap](https://www.bettercap.org/)
  es una [herramienta de código abierto](https://github.com/bettercap/bettercap)
  para realizar reconocimientos y atacar redes WiFi y Ethernet,
  dispositivos inalámbricos HID (dispositivos de interfaz humana)
  y dispositivos BLE (Bluetooth Low Energy).
- **Ciphey:**
  [Ciphey](https://github.com/Ciphey/Ciphey)
  es una herramienta automatizada de código abierto
  para descifrar cifrados (sin conocer las claves),
  decodificar codificaciones y descifrar _hashes_.
- **Covenant:**
  [Covenant](https://github.com/cobbr/Covenant)
  es un marco colaborativo de mando y control .NET para destacar
  las superficies de ataque y facilitar el uso de técnicas ofensivas.
- **DNSRecon:**
  [DNSRecon](https://github.com/darkoperator/dnsrecon)
  es una herramienta de código abierto para el reconocimiento de dominios.
- **Frida:**
  [Frida](https://frida.re/)
  es un conjunto de herramientas de instrumentación dinámica
  de código abierto que se utiliza a menudo para comprender el comportamiento
  interno y las comunicaciones de red de las aplicaciones móviles.
- **Fuff:**
  [Fuff](https://github.com/ffuf/ffuf)
  ("_fuzz faster u fool_") es una herramienta de código abierto escrita
  en Go para realizar _fuzzing_ (es decir, enviar automáticamente
  datos aleatorios a una aplicación para encontrar errores
  o comportamientos inesperados).
- **Ghidra:**
  [Ghidra](https://ghidra-sre.org/)
  es un [marco de código abierto](https://github.com/NationalSecurityAgency/ghidra)
  de ingeniería inversa de _software_ desarrollado por la
  National Security Agency Research Directorate.
- **Gitleaks:**
  [Gitleaks](https://gitleaks.io/)
  es una [herramienta SAST de código abierto](https://github.com/gitleaks/gitleaks)
  para escanear repositorios, archivos y directorios Git
  y detectar secretos codificados.
- **Hopper:**
  [Hopper](https://www.hopperapp.com/)
  es una herramienta de ingeniería inversa para Mac
  y Linux que permite desensamblar, descompilar y depurar aplicaciones.
- **HTTP Toolkit:**
  [HTTP Toolkit](https://httptoolkit.com/)
  es una [herramienta de código abierto](https://github.com/httptoolkit/httptoolkit)
  para interceptar, inspeccionar y reescribir tráfico HTTP(S).
- **Hydra:**
  [Hydra](https://github.com/vanhauser-thc/thc-hydra)
  es una herramienta paralela de código abierto para descifrar contraseñas
  ([con “fuerza bruta”](https://www.techtarget.com/searchsecurity/tutorial/How-to-use-the-Hydra-password-cracking-tool))
  que admite varios protocolos de ataque.
- **Interactsh:**
  [Interactsh](https://app.interactsh.com/#/)
  es una [herramienta de código abierto](https://github.com/projectdiscovery/interactsh)
  para detectar interacciones fuera de banda
  o vulnerabilidades que causan interacciones externas.
- **JMeter:**
  [Apache JMeter](https://jmeter.apache.org/)
  es una [herramienta de código abierto](https://github.com/apache/jmeter)
  para realizar pruebas de carga y analizar
  y medir el rendimiento de aplicaciones y servicios.
- **John the Ripper:**
  [John the Ripper](https://www.openwall.com/john/)
  es una [herramienta de código abierto](https://github.com/openwall/john)
  para [descifrar contraseñas](../../../blog/pass-cracking/)
  que soporta múltiples tipos de _hash_ y cifrado.
- **MobSF:**
  [MobSF](https://mobsf.github.io/Mobile-Security-Framework-MobSF/)
  (Mobile Security Framework)
  es una herramienta automatizada de [código abierto](https://github.com/MobSF/Mobile-Security-Framework-MobSF)
  para realizar análisis estáticos y dinámicos en aplicaciones móviles.
- **Pacu:**
  [Pacu](https://rhinosecuritylabs.com/aws/pacu-open-source-aws-exploitation-framework/)
  es un [marco de explotación de código abierto](https://github.com/RhinoSecurityLabs/pacu)
  para evaluar la seguridad de los entornos de Amazon Web Services (AWS).
- **ZAP:**
  [ZAP](https://www.zaproxy.org/)
  (Zed Attack Proxy)
  es una [herramienta de código abierto](https://github.com/zaproxy/zaproxy)
  para detectar automáticamente vulnerabilidades
  de seguridad en aplicaciones web.

## La importancia del _hacking_ ético

Aunque la importancia del _hacking_ ético
puede que ya te resulte evidente por lo que hemos comentado hasta ahora,
podemos dejarla mucho más clara mostrando algunos beneficios específicos
que esta metodología de pruebas de seguridad puede aportar a tu organización.

### Beneficios del _hacking_ ético

- El esfuerzo de los _hackers_ éticos permite un acercamiento
  significativo a la realidad. Ellos simulan con precisión lo que
  los _hackers_ de sombrero negro o actores de amenazas pueden hacer
  contra tus sistemas (incluso utilizando técnicas como la ingeniería social).
  Esto implica el descubrimiento detallado de diferentes debilidades,
  vulnerabilidades o puntos de entrada que
  los ciberdelincuentes podrían explotar,
  y que deben remediarse lo antes posible.

- Los _hackers_ éticos no solo revelan la ubicación del problema de seguridad,
  sino que también pueden proporcionarte evidencia precisa
  de cómo podría explotarse y qué impacto tendría en tus operaciones,
  activos o información sensible.

- El _hacking_ ético suele tener un alcance de detección de vulnerabilidades
  más amplio que el [escaneo de vulnerabilidades](../escaneo-de-vulnerabilidades/).
  Los _hackers_ pueden analizar la lógica empresarial y correlacionar
  vulnerabilidades para identificar problemas de seguridad complejos y graves.

- Los _hackers_ éticos también pueden contribuir con su experiencia
  en ciberseguridad para revisar y verificar
  los informes de herramientas automatizadas,
  las cuales a menudo pueden enviar falsos positivos.

- Cuando el _hacking_ ético es una actividad continua paralela
  al desarrollo de _software_ de tu organización,
  tus equipos pueden recibir reportes oportunos que ayudan
  a mejorar las tasas de remediación de vulnerabilidades,
  reducir gastos y desplegar productos seguros
  sin retrasos a los usuarios finales.

### ¿Cuáles son algunas limitaciones del _hacking_ ético?

La limitación más notable del _hacking_ ético es la velocidad.
Los _hackers_, por muy experimentados que sean,
no trabajan al ritmo de una máquina automatizada o un escáner.
Aunque las pruebas de seguridad apoyadas únicamente
en herramientas automatizadas no son _hacking_ ético,
un enfoque integral de _hacking_ ético,
como el que ofrecemos en Fluid Attacks,
puede implicar la intervención conjunta de humanos y escáneres.
Esto permite aprovechar la rapidez
de la tecnología para detectar vulnerabilidades conocidas y la astucia
y precisión de los _hackers_ para identificar problemas
de seguridad más complejos, riesgosos e incluso desconocidos hasta el momento.

### ¿Para quién es recomendable el _hacking_ ético?

Las entidades financieras son las que más contratan
los servicios de empresas de _hacking_ ético,
debido principalmente a las regulaciones que lo exigen.
Sin embargo,
es recomendable que cualquier organización con presencia
en la Internet o que desarrolle productos digitales
ponga a prueba la seguridad de sus sistemas con _hacking_ ético,
con el fin de evitar que se produzcan ciberataques exitosos contra ellos.

Sigue [este enlace](../../soluciones/hacking-etico/)
si tú y tu empresa están interesados en conocer detalles
sobre la solución Hacking Ético de Fluid Attacks.
Pero si lo que te gustaría es llevar un sombrero blanco
y ser un _hacker_ ético en nuestro _red team_,
sigue [este otro](../../../careers/).
Para más detalles sobre cada caso,
no dudes en [contactarnos](../../contactanos/).
