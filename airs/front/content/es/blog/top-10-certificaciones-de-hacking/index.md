---
slug: blog/top-10-certificaciones-de-hacking/
title: Top 10 de certificaciones de hacking
date: 2024-02-06
subtitle: Nuestra selección de los retos más difíciles para los hackers éticos
category: opiniones
tags: ciberseguridad, formación, hacking, red-team, pentesting
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1707230827/blog/top-10-hacking-certifications/cover_certifications.webp
alt: Foto por James Orr en Unsplash
description: Compartimos, las que para nosotros son, las 10 certificaciones de *hacking* ético más exigentes, para que puedas elegir tu próximo reto e incluso trazar un camino que ayude a tu carrera profesional.
keywords: Osee, Osce, Gxpn, Certificaciones Hacking Etico, Certificaciones Para Hackers Eticos, Red Team, Precio, Hacking Etico, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Escritor y editor
source: https://unsplash.com/photos/a-close-up-of-a-persons-wrist-with-a-watch-on-it-E99lndzGTYQ
---

Las [certificaciones](../../../certifications/)
de _hacking_ ético más exigentes son,
en orden descendente, Offensive Security Exploitation Expert,
Offensive Security Certified Expert 3
y GIAC Exploit Researcher and Advanced Penetration Tester.
Así lo afirma nuestro Vicepresidente de Hacking,
quien cuenta con más de 20 años de experiencia en ciberseguridad
y posee más de 25 certificaciones.
En este artículo del blog,
describimos brevemente no solo esas tres,
sino también las 10 certificaciones más exigentes.

### 10- Burp Suite Certified Practitioner (BSCP)

Empezamos esta lista con la única certificación
en ella que pone a prueba principalmente tu uso
de un conjunto de herramientas específico.
[BSCP](https://portswigger.net/web-security/certification)
de la academia de seguridad web de PortSwigger exige
un uso hábil de Burp Suite Professional para encontrar
y explotar vulnerabilidades.
Además de la certificación,
la academia ofrece material y laboratorios en línea gratuitos.

El examen BSCP requiere trabajar hasta cuatro horas
para analizar dos aplicaciones web e identificar
y aprovechar vulnerabilidades como XSS, inyección SQL y SSRF.
Menos mal que hay un examen de práctica oficial disponible,
ya que el real [requiere una ejecución impecable en un tiempo muy limitado](https://kentosec.com/2023/04/09/burp-suite-certified-practitioner-bscp-review-and-tips/).
Incluso [los _pen testers_ experimentados pueden fallar](https://javy26.medium.com/burp-suite-certified-practitioner-review-89ffa886bb7d)
varias veces.

Los comentarios habituales
sobre BSCP son que el material de formación gratuito es
muy útil, completo y organizado.

Con un costo de solo 99 USD al momento de escribir este artículo,
BSCP está disponible por un precio accesible.
Sin embargo, es necesario tener una suscripción a Burp Suite Professional,
que cuesta 449 USD al año.

### 9 - Certified Professional Penetration Tester (eCPPT)

Esta es una certificación emitida por INE Security,
antes eLearnSecurity (de ahí la "e" minúscula en el nombre de sus ofertas),
una de las principales compañías en certificación.
Este emisor cuenta con un repertorio variado
de formaciones prácticas y exámenes,
entre los que se encuentran los que permiten obtener una certificación
de nivel inicial (eJPT) y otra más difícil (eWPTX)
que esta que se encuentra en el lugar 9.

El [eCPPT](https://security.ine.com/certifications/ecppt-certification/)
se otorga tras demostrar correctamente los puntos débiles
de una red corporativa y presentar un reporte exhaustivo
de pruebas de penetración.
Este último debe incluir recomendaciones para reforzar dicha red.
Los objetivos del examen incluyen aplicaciones web
y sistemas Windows y Linux.

Algunos poseedores del eCPPT [han comentado](https://www.reddit.com/r/eLearnSecurity/comments/180ftdv/ecppt_the_good_very_good_and_the_bad/)
la falta de Active Directory en esta certificación,
así como de tendencias actuales.
Esta se beneficiaría de una renovación.
Lo positivo es que los poseedores
pueden coincidir (p. ej., [aquí](https://heberjulio65.medium.com/ecpptv2-the-good-very-good-and-the-ugly-f3f7fd2ab9ab)
y [aquí](https://www.reddit.com/r/eLearnSecurity/comments/v73ox4/passed_ecpptv2_review_and_tips/))
en que el material de la formación
te prepara perfectamente para el examen,
por lo que sin duda recomiendan obtenerlo.
Otras características destacables son que te dan mucho tiempo
para completar la práctica y los reportes
(7 días de plazo para cada uno),
y que la experiencia es bastante realista.

Un punto extra es que eCPPT figura
como una de las certificaciones que acreditan
[la calidad de los proveedores de _red teaming_](../../../blog/tiber-eu-providers/)
en Europa.
Así que, si estás pensando en trabajar en Europa,
eCPPT es una buena acreditación para aumentar tus posibilidades de empleo.

El precio del eCPPT era de 400 USD en el momento de redactar este documento.

### 8 - Offensive Security Certified Professional (OSCP)

OffSec es sin duda el principal emisor de certificaciones de _hacking_,
ya que ambas certificaciones en las posiciones 1 y 2 de esta lista son suyas.
Su certificación [OSCP](https://www.offsec.com/courses/pen-200/)
se otorga tras completar el curso Penetration Testing with Kali Linux
y un examen de 24 horas.
[Hemos mencionado OSCP](../../../blog/how-to-become-an-ethical-hacker/)
en otro lugar como una de las certificaciones a obtener
para que puedas avanzar en tus habilidades
y mostrar a los empleadores tu compromiso con el _hacking_.

Tu [objetivo en el examen OSCP](../../../blog/oscp-journey/) es obtener acceso
de administrador a las máquinas de la red.
En un artículo anterior,
además de una [crítica a las certificaciones CEH](../../../blog/certified-ethical-hacker-ceh/),
puedes encontrar algunas características de OSCP,
entre ellas su dificultad.
Este examen exige un gran conocimiento
de herramientas para realizar escaneos,
enumeraciones y escalamiento de privilegios.
Una habilidad para redactar reportes
con pruebas suficientes también es de mucha ayuda.

El precio de OSCP es un poco más alto que el promedio,
1.649 USD en el momento de escribir este artículo.

### 7 - Certified Red Team Expert (CRTE)

Con el 7º puesto presentamos a otro de los principales emisores
de certificaciones de _hacking_:
Altered Security (anteriormente, Pentester Academy).
Este ofrece principalmente certificaciones en [_red teaming_](../ejercicio-red-teaming/).
Y, según la información en su [página web](https://www.alteredsecurity.com/),
han formado a más de 10.000 personas en más de 400 organizaciones.
Entre sus clientes, figuran el Departamento de Justicia
y la Agencia de Seguridad Nacional de los Estados Unidos.

[CRTE](https://www.alteredsecurity.com/redteamlab) se enfoca en entender
y explotar amenazas a Windows Active Directory.
Tu objetivo en el laboratorio sería abusar
de las características del dominio para pasar
de una cuenta de usuario no administrador
en el dominio a administrador de varios entornos.
El examen de 48 horas requiere que enumeres un entorno
de Windows desconocido y construyas cuidadosamente rutas de ataque.
Un antiguo miembro de nuestro equipo ha destacado
este objetivo como
una [diferencia entre CRTE y OSCP](../../../blog/new-red-team-expert/).
A saber,
este último se centra en buscar vulnerabilidades públicas,
mientras que CRTE se centra en (a menudo pasadas por alto)
configuraciones erróneas en componentes de un directorio activo.

Al momento de escribir este _post_,
el precio de los paquetes CRTE que incluyen un intento de examen,
acceso sin caducidad a materiales
y acceso temporal al laboratorio oscilaba entre 299 y 699 USD.
La cifra depende de la duración del acceso al laboratorio.

### 6 - Certified Red Team Master (CRTM)

Esta es otra certificación de Altered Security.
Su laboratorio presenta una simulación
de la red de una institución financiera en la que hay
que burlar muchos mecanismos de defensa recomendados
(p. ej., LAPS, WDAC, ASR, CLM).
El examen de 48 horas no solo consiste en atacar,
sino también en defender.
Tienes que comprometer varios entornos tratando
de que no salten las alarmas.
Y tienes que aplicar controles de seguridad o buenas prácticas
a un entorno al que tienes acceso desde el principio.
Además,
debes entregar todas las pruebas de tu trabajo en un reporte detallado.

Los paquetes [CRTM](https://www.alteredsecurity.com/gcb)
cuestan más que los CRTE,
desde 399 hasta 749 USD en el momento de escribir este artículo.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/hacking-etico/"
title="Inicia ahora con la solución de *hacking* ético de Fluid Attacks"
/>
</div>

### 5 - Red Team Lead

Nuestro VP de Hacking, Andrés Roldán,
ha publicado [una reseña de Red Team Lead](../../../blog/crtl-review/)
con mucho detalle (y ha dado algunos consejos para el examen).
Por lo tanto, seremos breves aquí.

El emisor es Zero-Point Security,
quien ofrece cursos de _red teaming_ y programación.
El curso que conduce a [Red Team Lead](https://eu.badgr.com/public/badges/gwM0NmzISLyqmcqDScDX3w)
es Red Team Ops II, que se centra en tácticas avanzadas,
como saltarse los controles de puntos finales de Windows
de las empresas modernas.

Esta certificación se distingue de las demás por sus capítulos
sobre la reducción de la superficie de ataque
y el control de aplicaciones en Windows Defender.
El examen requiere que obtengas al menos cinco de seis banderas
en un conjunto determinado de máquinas de un entorno
AD (el número de banderas y los criterios de aprobación
han cambiado desde el momento de la reseña).
Tienes ocho días para completarlo y no puedes dedicarle más de
96 horas (el tiempo también ha aumentado desde la reseña).
Este no te pide ningún reporte.

Red Team Lead exige mucha habilidad,
como señala la reseña,
no solo en evasión defensiva y tácticas OPSEC avanzadas,
sino también enumeración AD, pivoteo, movimiento lateral,
suplantación de usuarios, ataques Kerberos
y uso de Cobalt Strike.
Los consejos para tener en cuenta son concentrar un gran esfuerzo
en la enumeración y el reconocimiento en tu desempeño
y practicar mucho en el laboratorio, entre otros.

En el momento de escribir este artículo,
el curso Red Team Ops II cuesta £399 (unos 500 USD).
Las versiones con laboratorio cuestan entre £429 (538 USD) y £489 (613 USD).
Todas las versiones, incluso únicamente el curso,
incluyen un intento de examen gratuito.

### 4 - Web application Penetration Tester eXtreme (eWPTX)

Habíamos anticipado otra certificación de INE Security en este top 10.
Se trata de la certificación de [_pentesting_](../pentesting/)
de aplicaciones web más exigente que ofrece este emisor.

Para completar [eWPTX](https://security.ine.com/certifications/ewptx-certification/)
debes utilizar metodologías avanzadas y tener habilidad
para crear _exploits_ que las herramientas modernas
no podrían alcanzar.
([El relato](https://infosecjunky.com/my-journey-on-ewptxv2/)
de un poseedor de eWPTX dice que los escáneres
ni siquiera podían encontrar las vulnerabilidades).
Además,
el emisor vuelve a poner mucho énfasis en la calidad
del reporte de _pentesting_.

Una [reseña](https://grumpz.net/pass-the-ewptxv2-exam-on-your-first-attempt-in-2023)
recomienda entrenar a fondo para abordar los puntos débiles
que puedas tener en Java Deserialization RCEs,
Server Side Template Injections,
PHP Object Injections, uso avanzado de SQLmap
o la capacidad de encadenar vulnerabilidades.
Luego pasa a listar recursos gratuitos
para mejorar en las vulnerabilidades mencionadas, y más.
Al igual que con eCPPT,
te dan 7 días para completar el examen y otros 7 para reportar.

Un inconveniente es que algunas personas
que lo han realizado informan (p. ej., [aquí](https://www.reddit.com/r/eLearnSecurity/comments/yffgw3/just_passed_ewptx/),
[aquí](https://www.reddit.com/r/eLearnSecurity/comments/13al5f2/comment/jj7hszf/)
y [aquí](https://www.reddit.com/r/netsecstudents/comments/n2qw4y/ineelearnsecuritys_ewptxv2_review/))
de que el examen puede estar "lleno de errores"
y no funcionar correctamente,
lo que puede resultar desconcertante.
Encontrarás el consejo general de reiniciar el ambiente cuando parezca
que las cosas no funcionan.

El eWPTX cuesta 400 USD al momento de escribir este artículo.

### 3 - GIAC Exploit Researcher and Advanced Penetration Tester (GXPN)

Finalmente entramos al top 3.
Esta certificación es la creación de GIAC Certifications
(anteriormente, Global Information Assurance Certification).
Es decir,
el organismo de certificación fundado en 1999
por el prestigioso SANS Institute conocido como el pionero
en certificaciones técnicas InfoSec.
Este destaca por haber emitido alrededor de 174.000 certificaciones.

[GXPN](https://www.giac.org/certifications/exploit-researcher-advanced-penetration-tester-gxpn/)
consiste en ser capaz de escribir
y personalizar herramientas de ataque que te ayuden
a aprovechar vulnerabilidades de Windows y Linux.
Los objetivos son varios,
incluidos los sistemas de control de acceso a la red,
la gestión de memoria y las implementaciones criptográficas.

El material para el examen es [diverso y complejo](https://medium.com/@e.s.velez/my-gxpn-certification-experience-ca95f0e33d90),
por lo que debes ser bastante organizado
para estudiarlo todo adecuadamente.
El examen, como puede comprobarse
en los muy recomendables tests de práctica,
requiere que poseas una estrategia de gestión del tiempo perfeccionada.
Tienes que distribuir tu muy limitado tiempo (3 horas)
entre las preguntas teóricas y las prácticas
y pensar qué debes hacer más rápido.

El precio de GXPN se acercaba a la cifra máxima de 1.299 USD
en el momento de redactar este texto.

### 2 - Offensive Security Certified Expert 3 (OSCE3)

En segundo lugar hay una certificación
que en realidad requiere que obtengas tres certificaciones de OffSec.
Puedes pensar que esto es una especie de trampa por nuestra parte,
metiéndolas todas en un solo sitio.
Pero en cierto modo no lo es.
Piénsalo como una certificación que requiere superar
tres exámenes de nivel experto (cada uno de hasta 48 horas).

Una de las certificaciones requeridas
es OffSec Experienced Penetration Tester ([OSEP](https://www.offsec.com/courses/pen-300/)).
El objetivo es eludir los mecanismos de seguridad diseñados
para bloquear ataques en Windows y Linux.
Puede considerarse como el siguiente paso tras haber obtenido el OSCP.

Otra certificación requerida
es OffSec Web Expert ([OSWE](https://www.offsec.com/courses/web-300/)).
Esta requiere explotar aplicaciones web “front-facing”.
Tienes que demostrar tu habilidad
para analizar manualmente el código fuente “descompilado”,
encontrar errores que los escáneres no pueden encontrar,
combinar vulnerabilidades lógicas
para crear una prueba de concepto, etc.

Y la tercera certificación requerida
es OffSec Exploit Developer ([OSED](https://www.offsec.com/courses/exp-301/)).
Esta requiere escribir Windows _shellcode_ y otros _exploits_ personalizados.
Además, como dice [la cuenta de uno de los poseedores](https://spaceraccoon.dev/rop-and-roll-exp-301-offensive-security-exploit-development-osed-review-and/)
tendrías que sentirte cómodo con el código ensamblador
de 32 bits y la ingeniería inversa.

Al obtener las tres certificaciones,
se obtiene automáticamente la [OSCE3](https://help.offsec.com/hc/en-us/articles/4403282452628-OSCE-FAQ).

OSCE3 se creó después de retirar la certificación OSCE,
la cual hace casi tres años [habíamos colocado como segunda](../../../blog/certificates-comparison-ii/)
tras la que ocupa el primer lugar en esta misma lista.

Cada una de estas certificaciones con sus correspondientes cursos
tiene un precio de 1.649 USD.
Esta puede ser fácilmente la segunda certificación más cara de este top 10.

### 1 - OffSec Exploitation Expert (OSEE)

Como habíamos anticipado,
el primer puesto es para [OSEE](https://www.offsec.com/courses/exp-401/).
Nuestro Vicepresidente de Hacking dijo
sobre el curso que lleva a esta certificación
que es la formación en explotación de Windows más avanzada,
difícil y sorprendente del mercado.
En cuanto al examen de 72 horas,
lo calificó como agotador.
Para leer su reseña,
visita su _post_ "[OSEE, un viaje inesperado](../../../blog/osee-review/)."

El curso te enseña a realizar un _exploit_
de cadena completa e implica, en palabras de Roldán,
utilizar técnicas increíbles.
Los consejos para tener éxito incluyen leer el material de principio a fin,
completar tantas secciones extras de cada módulo como puedas,
repetir todo lo anterior una y otra vez,
y unirte al [servidor OffSec Discord](https://discord.gg/offsec)
para disfrutar de algo de apoyo de la comunidad y del personal de OffSec.
Para conocer los temas del curso, consulta la reseña.

El examen OSEE requiere que completes dos retos:
escapar de un entorno de pruebas (“sandbox”)
y explotar Windows Kernel.
Después, tienes 24 horas para subir un reporte.
Como se argumenta en la reseña,
los retos abordan adecuadamente el material del curso
y el tiempo que se da para completarlos es suficiente.
Si hay alguna crítica,
es que las versiones actuales del sistema operativo Windows
pueden proteger contra las técnicas aprendidas en el curso,
y por lo tanto depende de ti averiguar
cómo explotar estas versiones fuera del curso.

El curso de Advanced Windows Exploitation
en Black Hat para Oriente Medio y África costaba 12.375 USD,
y en Londres costaba £9.385 (unos 11.794 USD)
en el momento de escribir esto.
¡Es mucho! Pero según Roldán,
lo que aprendes durante el curso vale cada centavo.

El equipo de _hackers_ de Fluid Attacks
ha obtenido todas estas certificaciones.
Si estás aquí para descubrir lo que nuestro equipo
puede ser capaz de hacer para ayudarte a asegurar
tu _software_ a través de
nuestro [plan Advanced de Hacking Continuo](../../planes/),
esperamos que este artículo te haya sido útil.
Si tienes alguna duda,[contáctanos](../../contactanos/).
