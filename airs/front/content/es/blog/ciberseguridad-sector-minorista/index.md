---
slug: blog/ciberseguridad-sector-minorista/
title: Ciberseguridad en el sector minorista
date: 2024-10-04
subtitle: Retos, amenazas y buenas prácticas para los comerciantes
category: filosofía
tags: ciberseguridad, empresa, riesgo, tendencia, devsecops
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1728064166/blog/retail-sector-cybersecurity/cover_retail_sector_cybersecurity.webp
alt: Foto por Anima Visual en Unsplash
description: La industria minorista es parte de la transformación digital global, por lo que está constantemente expuesta a amenazas de seguridad ante las cuales debería tener una postura preventiva.
keywords: Ciberseguridad En La Industria Minorista, Amenazas Seguridad Minorista, Ciberataques Sector Minorista, Retos Seguridad Minorista, Estadísticas De Ciberseguridad En El Sector Minorista, Buenas Practicas Comerciantes, Comercio Electronico, Hacking Etico, Pentesting
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/a-close-up-of-a-sign-on-a-window-PS_-DwoNUJ4
---

El sector minorista ("retail" en inglés) está en constante crecimiento,
impulsado por el incesante ritmo de la transformación digital.
Desde las tiendas de comestibles y artículos para el hogar
hasta los minoristas en línea que venden productos electrónicos,
ropa y artículos de lujo,
este sector adopta la tecnología moderna
para mejorar y agilizar las operaciones,
tales como los procesos de pago y la gestión de inventario y personal.
Este crecimiento se ha hecho [evidente en las cifras](https://www.forbes.com/sites/johnkoetsier/2023/01/28/e-commerce-retail-just-passed-1-trillion-for-the-first-time-ever/)
de la modalidad de comercio electrónico o *e-commerce*,
que complementa a o es independiente de la modalidad de venta física:
las ventas en línea solo en los Estados Unidos superaron el billón de dólares
a finales de 2022,
más del doble que unos pocos años antes.

Aunque este auge digital presenta increíbles oportunidades
para los minoristas,
también conlleva importantes retos de ciberseguridad.
Con las compras en línea,
el sector minorista se ha convertido en un objetivo prioritario
para los ciberdelincuentes,
atraídos por las enormes cantidades de datos confidenciales de los clientes,
incluida la información de pago, como números de tarjetas de crédito,
direcciones de envío y números de teléfono,
que los minoristas almacenan y procesan a diario.
En este blog,
exploraremos las amenazas de ciberseguridad
a las que se enfrenta el sector minorista
y describiremos las mejores prácticas para proteger tu negocio
y a tus clientes
dentro de este panorama digital en constante evolución.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/devsecops/"
title="Empieza ya con la solución DevSecOps de Fluid Attacks"
/>
</div>

## La creciente superficie de ataque en el comercio minorista

Varios factores contribuyen a la propensión del sector minorista
a recibir y ser vulnerable a ciberataques:

- **Dependencias de terceros:**
  Los comercios minoristas suelen depender
  de los productos y servicios de terceros para el alojamiento web,
  el procesamiento de pagos, las campañas publicitarias,
  la atención al cliente o la gestión de inventarios.
  Aunque estas asociaciones ofrecen eficiencia y ahorro de costos,
  también introducen riesgos potenciales para la seguridad.
  Una vulnerabilidad en un sistema de terceros
  puede proporcionar un punto de entrada
  para que los atacantes se infiltren en la red de una empresa minorista.

- **Operaciones omnicanal:**
  El entorno minorista actual es un ecosistema complejo
  que abarca tiendas físicas, sitios web de comercio electrónico,
  aplicaciones móviles y otros puntos de contacto digital.
  Esto crea una superficie de ataque compleja,
  que requiere medidas de seguridad sólidas en todos los canales.
  Una sola vulnerabilidad en un área
  (p. ej., dispositivos de punto final del personal
  que pueden no cumplir suficientes requisitos de seguridad)
  puede tener consecuencias en cascada,
  comprometiendo toda la red.

- **Retos de las franquicias:**
  Aunque operan hasta cierto punto de forma independiente,
  los franquiciados siguen formando parte de una marca más prominente
  o franquiciador.
  Estas empresas, a menudo más pequeñas, pueden tener recursos limitados
  para invertir en medidas de ciberseguridad robustas,
  lo que las hace más susceptibles a vulnerabilidades
  que los atacantes podrían explotar.
  El hecho de que un franquiciado se vea comprometido
  puede tener repercusiones negativas en la reputación del franquiciador.

- **Picos estacionales y problemas de personal:**
  Las temporadas altas de compras, como los días de festividades,
  a menudo someten a los minoristas a una inmensa presión
  y les obligan a contratar personal temporal
  que puede no estar adecuadamente formado
  en las mejores prácticas de ciberseguridad
  o no ser suficientemente fiable.
  Esto puede dar lugar a errores involuntarios
  (p. ej., manejar mal los datos de los consumidores
  o ser víctima de estafas de [*phishing*](../../../blog/phishing/)),
  errores de configuración del sistema
  (p. ej., configurar incorrectamente los ajustes de seguridad
  del sistema de punto de venta)
  o incluso amenazas internas.

- **El auge de las tarjetas regalo:**
  Las tarjetas regalo se han hecho cada vez más populares porque,
  mientras proporcionan a los minoristas
  una valiosa fuente de ingresos por adelantado,
  impulsan el conocimiento de la marca
  y a menudo conducen a un mayor gasto del consumidor
  más allá del valor inicial de la tarjeta,
  por desgracia también ofrecen anonimato a los ciberdelincuentes.
  Los atacantes pueden comprometer tarjetas de crédito
  u otros métodos de pago,
  emplearlos para comprar tarjetas regalo de alto valor,
  y luego usar o vender esas tarjetas para pasar desapercibidos.

## Estadísticas sobre ciberseguridad en el sector minorista

Diversos estudios han resaltado la alarmante tasa de ciberataques
dirigidos al sector minorista:

- En 2020, [se informó que](https://www.trustwave.com/hubfs/Web/Library/Documents_pdf/D_16791_2020-trustwave-global-security-report.pdf)
  el 24% de todos los ciberataques a nivel mundial
  (estudio en realidad limitado a unos pocos países)
  estaban dirigidos a minoristas,
  superando incluso al sector financiero y de seguros.
  La mitad de los incidentes de todos los sectores
  se atribuyeron a técnicas de [ingeniería social](../../../blog/social-engineering/).

- [Datos más recientes](https://cloud.google.com/security/resources/m-trends)
  (2023) muestran que el comercio minorista y la hostelería
  ocupan el cuarto lugar, con un 8,6%,
  entre las industrias más atacadas,
  mientras que los sectores financiero (17,3%),
  de servicios empresariales y profesionales,
  y de alta tecnología ocupan los tres primeros puestos.
  En esta investigación,
  los *exploits* fueron el principal vector de infección inicial (38%),
  seguidos de los ataques tipo *phishing* (17%).

- [Hallazgos de IBM sugieren](https://www.ibm.com/account/reg/us-en/signup?formid=urx-52629)
  un cambio en las tácticas de los atacantes,
  con un énfasis creciente en la adquisición de credenciales válidas
  para iniciar sesión en lugar de explotar vulnerabilidades
  o realizar campañas de *phishing*
  (los fallos de identificación y autenticación son cada vez más frecuentes).

- Según el mismo estudio, el año pasado,
  el sector minorista-mayorista ocupó el quinto lugar (10,7%)
  entre los sectores más atacados a escala mundial,
  con la industria manufacturera (25,7%) y la de finanzas y seguros (18,2%)
  a la cabeza.
  En el sector en cuestión,
  el 50% de los incidentes implicaron [*malware*](../../learn/codigo-malicioso/),
  principalmente [*ransomware*](../../../blog/ransomware/).
  El uso de cuentas válidas fue el vector de ataque inicial más común,
  con un 43%, seguido del phishing y la explotación de aplicaciones públicas,
  cada uno con alrededor del 29%.

- Por su parte, [Check Point](https://blog.checkpoint.com/research/check-point-research-reports-highest-increase-of-global-cyber-attacks-seen-in-last-two-years-a-30-increase-in-q2-2024-global-cyber-attacks/),
  en los datos mundiales de este año,
  señala al comercio minorista/mayorista como el tercer sector más atacado
  con *ransomware*,
  siendo el manufacturero el primero y el sanitario el segundo.
  Sin embargo, en ataques globales, ocupa el noveno lugar.

- El costo promedio de la violación de datos en el sector minorista
  [se estima en 2,9 millones de dólares](https://www.trustwave.com/en-us/resources/blogs/spiderlabs-blog/the-2023-retail-services-sector-threat-landscape-a-trustwave-threat-intelligence-briefing/),
  cifra inferior a la media general industrial de 4,4 millones de dólares.
  Sin embargo,
  el daño reputacional de una violación puede ser muy significativo,
  algo que puede magnificar esos costos en los futuros próximo y lejano.

Independientemente del rigor de estos estudios,
de la veracidad de sus datos
y de hasta qué punto representan una realidad más amplia,
o de qué tanto pueden ajustarse a tu contexto específico,
no cabe duda de que el sector minorista se encuentra entre los más atractivos
para los ciberdelincuentes
y de que la explotación de vulnerabilidades, la ingeniería social
y el *malware* son amenazas cibernéticas constantes.
Los comerciantes minoristas deben permanecer vigilantes
y adaptar sus estrategias de seguridad
para hacer frente a las amenazas emergentes.

## Amenazas para la ciberseguridad en el comercio minorista

Profundicemos en algunas de las amenazas de ciberseguridad más frecuentes
a las que se enfrentan las empresas minoristas:

- **Explotación de vulnerabilidades:**
  Aprovecharse de las vulnerabilidades de seguridad en el *software*,
  incluidas las de aplicaciones o componentes de terceros y dispositivos IoT,
  sigue siendo un vector de ataque común.
  Muchos minoristas ([quizás el 50%](https://www.forbes.com/sites/dennismitzner/2022/09/14/self-checkouts-iot-and-the-rise-of-retail-cyber-security-threats/))
  no protegen adecuadamente sus dispositivos IoT,
  como los sistemas de punto de venta y las estanterías inteligentes,
  lo que los hace vulnerables.
  En el desarrollo de *software* surgen problemas en la cadena de suministro,
  donde los paquetes, librerías y dependencias,
  que a menudo pasan desapercibidos,
  constituyen debilidades y pueden servir como puntos de entrada
  para los atacantes.
  Las configuraciones erróneas de la nube también
  pueden provocar filtraciones de datos e interrupciones de servicios.
  Los ataques DDoS (denegación de servicio distribuida),
  de [inyección SQL](../../../blog/sql-injection/)
  y MitM ([*man-in-the-middle*](../../learn/que-es-ataque-man-in-the-middle/))
  se encuentran entre los más mencionados
  como explotación de vulnerabilidades en el sector minorista.

- **Ataques phishing:**
  Este método de ingeniería social sigue siendo una amenaza importante,
  empleando técnicas cada vez más sofisticadas
  para engañar a empleados y clientes.
  Con la ayuda de la IA generativa,
  los correos electrónicos y mensajes de *phishing*
  pueden elaborarse para ser increíblemente convincentes,
  lo que hace más difícil desenmascararlos
  basándose en señales de alarma tradicionales,
  como la mala gramática o los errores de comunicación.
  El objetivo de estos ataques es robar información confidencial
  o instalar *malware*,
  lo que provoca nuevos ataques y daños importantes.

- **Ransomware:** Los ataques de *ransomware*,
  cuyo vector de infección inicial suele ser el *phishing*,
  pueden paralizar las operaciones de los comercios,
  como el ofrecimiento de productos en línea o las transacciones de pago,
  al cifrar sistemas y datos críticos.
  Los atacantes exigen un rescate a cambio de la clave de descifrado,
  lo que suele ejercer una enorme presión sobre los comercios minoristas
  para que paguen a fin de restablecer sus operaciones.

- **Deepfakes:**
  La tecnología [*deepfake*](../../../blog/what-trends-to-expect-for-2023/#deepfake)
  es cada vez más sofisticada,
  permitiendo a los actores de amenazas crear vídeos o grabaciones de audio
  falsos muy realistas.
  Esto supone una amenaza importante para los minoristas,
  ya que los atacantes pueden utilizar *deepfakes*
  para hacerse pasar por clientes o empleados,
  con la posibilidad de acceder a información confidencial
  o facilitar transacciones fraudulentas.

- **Relleno de credenciales:**
  Se trata de un tipo de [ataque de fuerza bruta](../../../blog/pass-cracking/#brute-force)
  en el que los ciberdelincuentes se apoyan en *bots* automatizados
  para utilizar credenciales robadas
  buscando obtener acceso no autorizado a cuentas de usuario.
  El hecho de que muchas personas reutilicen contraseñas
  en múltiples plataformas o aplicaciones
  facilita a los atacantes comprometer las cuentas
  mediante el relleno de credenciales ("[credential stuffing](../../../blog/relleno-credenciales/)")
  y, en consecuencia, robar datos de clientes,
  realizar compras fraudulentas o lanzar otros ataques.

- **Skimming digital:**
  El *skimming* digital implica el uso de código malicioso
  inyectado en sitios web de comercio electrónico
  para robar información de tarjetas de crédito
  durante las transacciones en línea.
  Esta amenaza es especialmente preocupante para los minoristas en línea,
  ya que puede comprometer grandes cantidades de datos de pago.

- **Amenazas internas:**
  Los empleados o contratistas externos con acceso a información sensible
  pueden suponer una amenaza importante.
  Las amenazas internas pueden involucrar actividades maliciosas intencionadas
  o errores no intencionados
  que provocan filtraciones de datos o incidentes de seguridad.

## Buenas prácticas de ciberseguridad para el comercio minorista

Para combatir estas amenazas y proteger su negocio y a sus clientes,
los minoristas deben adoptar un enfoque proactivo e integral
de la ciberseguridad.
Estas son algunas de las mejores prácticas esenciales:

- **Hacking continuo:**
  Si eres un minorista centrado en el comercio electrónico,
  deberías evaluar constantemente la seguridad de tus aplicaciones
  y otros sistemas.
  Las [herramientas de análisis](../escaneo-de-vulnerabilidades/)
  estático y dinámico son muy útiles para detectar vulnerabilidades conocidas.
  Sin embargo,
  estas pruebas deben complementarse con [*pentesting* manual](../que-es-prueba-de-penetracion-manual/)
  a cargo de expertos en seguridad
  para reducir al mínimo los falsos positivos y los falsos negativos.
  Luego, podrías remediar las vulnerabilidades por orden de prioridad,
  en función del riesgo que suponen para tu empresa.

- **Cumplimiento de las normas del sector:**
  Los minoristas que aceptan pagos con tarjeta de crédito o débito
  deben cumplir la norma [PCI DSS](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-pci)
  (Payment Card Industry Data Security Standard),
  la cual rige el almacenamiento, procesamiento y transmisión de datos
  de tarjetas de pago.
  Además, los minoristas que procesan datos de nativos o residentes de la UE
  deben adherirse al [GDPR](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-gdpr)
  (Reglamento General de Protección de Datos),
  y los que manejan datos de residentes de California
  deben cumplir con el [CCPA](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-ccpa)
  (California Consumer Privacy Act).
  El cumplimiento de estos estándares
  te ayuda a prevenir filtraciones de datos,
  evitar multas y mantener la confianza de tus clientes;
  otras normas relevantes a tener en cuenta son [HIPAA](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-hipaa),
  [NIST](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-nist800171)
  e [ISO 27001](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-iso27001).

- **Aplicar medidas de seguridad esenciales:**
  Despliega cortafuegos ("firewalls") que actúen como barreras
  entre Internet y tu red,
  y utiliza programas antivirus para escanear periódicamente el sistema
  y detectar y eliminar archivos maliciosos.
  Cifra los datos confidenciales, tanto en tránsito como en reposo,
  para protegerlos de accesos no autorizados.
  Haz copias de seguridad periódicas de tu sitio web,
  sistemas de punto de venta y otras aplicaciones críticas
  para garantizar la continuidad del negocio
  en caso de un ciberataque o fallo del sistema.

- **Segmentación de redes:**
  Segmenta tu red para evitar que una brecha de seguridad en un área
  comprometa todo el sistema.
  Esto significa dividir tu red en secciones más pequeñas y aisladas,
  casi como creando habitaciones separadas dentro de tu casa.
  Si un ladrón entra en una habitación,
  no puede acceder inmediatamente a toda la casa.
  Esto limita el impacto de los ataques y ayuda a contener los daños.

- **Seguridad de puntos finales:**
  Restringe el acceso a datos confidenciales en puntos finales
  como terminales de punto de venta y ordenadores de empleados.
  Aplica la autenticación multifactor,
  incluida la biométrica o los códigos de un solo uso,
  para impedir que los atacantes obtengan acceso no autorizado.
  Sigue el principio del mínimo privilegio,
  concediendo a los empleados solo el acceso que necesiten
  para realizar sus tareas.

- **Gestión de riesgos de terceros:**
  Investiga a fondo a los proveedores externos
  y asegúrate de que cumplen las normas de seguridad pertinentes.
  Evalúa periódicamente sus prácticas de seguridad
  y actualiza o parcha los sistemas según sea necesario.
  Mantén un inventario de todos los sistemas de terceros
  y sus riesgos asociados.
  Esto debe abarcar tanto el *hardware* como el SaaS
  (*software* como servicio),
  así como los componentes y dependencias
  para el desarrollo de tus propios productos.

- **Formación sobre ciberseguridad:**
  Implementa programas completos de concienciación sobre ciberseguridad
  para todos los empleados, incluido el personal temporal.
  La formación debe abarcar temas como el *phishing*,
  la seguridad de las contraseñas, la ingeniería social
  y los hábitos de navegación segura.
  Adapta el entrenamiento a roles y responsabilidades específicos,
  proporcionando instrucción especializada a los empleados
  que manejan datos confidenciales u operan sistemas críticos.
  Por ejemplo,
  los cajeros pueden necesitar formación específica sobre el uso seguro
  de los sistemas de punto de venta,
  mientras que los desarrolladores deben conocer
  las [prácticas de programación segura](../practicas-codificacion-segura/)
  y los riesgos para la [seguridad de la cadena de suministro](../../../blog/software-supply-chain-security/).

Mediante la aplicación de estas buenas prácticas,
los minoristas pueden mejorar significativamente su postura de ciberseguridad,
proteger su negocio y a sus clientes,
y mantener una sólida reputación en el mercado digital.
Recuerda que la ciberseguridad es un proceso continuo que requiere supervisión,
adaptación y mejoras constantes para adelantarse a las amenazas en evolución.

## Ciberseguridad para el comercio minorista con Fluid Attacks

Si eres un *retailer* o comerciante minorista —no importa el tamaño—
consciente del panorama actual de riesgos y amenazas en el mundo digital
y de la necesidad de proteger a tu empresa y a tus usuarios,
Fluid Attacks está aquí para ayudarte.

Desde un enfoque primordialmente preventivo,
te ofrecemos evaluaciones de seguridad continuas
mediante herramientas automatizadas con [SAST](../../producto/sast/),
[DAST](../../producto/dast/), [SCA](../../producto/sca/)
y [CSPM](../../producto/cspm/),
así como evaluaciones manuales por parte de [nuestros *pentesters*](../../../certifications/)
que implican [PTaaS](../../producto/ptaas/),
[revisión de código seguro](../../soluciones/revision-codigo-fuente/)
e [ingeniería inversa](../../producto/re/).
También te proporcionamos una [plataforma](../../plataforma/)
para la [gestión de vulnerabilidades](../que-es-gestion-de-vulnerabilidades/)
basada en riesgos.
A partir de ahí,
puedes conocer las vulnerabilidades de tu *software*
(incluidos los problemas de la cadena de suministro),
recibir asistencia de [expertos](https://help.fluidattacks.com/portal/en/kb/articles/talk-to-a-hacker)
y basada en [GenAI](https://help.fluidattacks.com/portal/en/kb/find-security-vulnerabilities/fix-code-with-gen-ai)
para la remediación,
y verificar que estás libre de riesgos conocidos
cada vez que planees desplegar tus aplicaciones.

[Ponte en contacto](../../contactanos/)
con nosotros para obtener más información
sobre cómo nuestras soluciones de seguridad pueden ayudarte
a construir y mantener la confianza de tus clientes.
Para empezar con una prueba gratuita, sigue [este enlace](https://app.fluidattacks.com/SignUp).
