---
slug: blog/gestion-resistencia-ataques-psirts/
title: Seguridad excepcional de productos
date: 2022-09-22
subtitle: Cómo la gestión de resistencia a ataques puede ayudar a PSIRTs
category: filosofía
tags: ciberseguridad, empresa
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1663883952/blog/attack-resistance-management-psirts/cover_psirt.webp
alt: Foto por Marek Piwnicki en Unsplash
description: Describimos cómo la gestión de resistencia a ataques puede ayudar a mejorar la madurez operativa de los equipos de respuesta a incidentes de seguridad de productos en el descubrimiento, la clasificación y la remediación de vulnerabilidades.
keywords: Gestion De Resistencia A Ataques, Que Es Psirt, Equipo De Respuesta A Incidentes De Seguridad De Productos, Hackers Eticos, Descubrimiento De Vulnerabilidades, Triaje De Vulnerabilidades, Remediacion De Vulnerabilidades, Hacking Etico, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Escritor y editor
source: https://unsplash.com/photos/zetUhDVYejc
---

Los proveedores de soluciones de *software* necesitan
que su tecnología sea segura para sus usuarios.
Y punto.
Cuando se descubre que no es segura, tienen que activar un proceso fiable
que les permita solucionar los problemas.
Más concretamente, en este proceso,
su equipo de respuesta a incidentes de seguridad de productos
(PSIRT, por su nombre en inglés)
tiene que gestionar los informes, clasificarlos,
orquestar la remediación y dirigirse a las partes interesadas a través
de avisos de seguridad.
Sobre el papel, esto parece tan sencillo como su objetivo final.
Sin embargo,
los PSIRTs se enfrentan a varios retos que afectan su pronta respuesta.
Destacaremos estos retos y cómo puede ayudar
la gestión de resistencia a ataques (ARM, por sus siglas en inglés),
un proceso continuo para mapear la superficie de ataques,
evaluar manualmente su resistencia a ataques y mejorarla.

## ¿Qué es PSIRT?

En pro de la claridad,
comencemos este artículo explicando qué son los [PSIRTs](https://www.first.org/standards/frameworks/psirts/psirt_services_framework_v1.1).
Como su nombre indica,
PSIRTs son equipos creados en las organizaciones para responder a,
y a veces encontrar, amenazas y fallas en sus **productos**.
Por lo tanto,
su función es diferente a la de los equipos de respuesta
a incidentes informáticos (CSIRT, por su nombre en inglés).
Estos últimos toman medidas para evaluar la seguridad de los **sistemas**
de su organización y solucionar los problemas que detectan.

Con el respaldo de los directivos,
los PSIRTs crean políticas
sobre [cómo gestionar las vulnerabilidades](../que-es-gestion-de-vulnerabilidades/)
notificadas en sus productos por fuentes internas o externas.
Esta legitimación permite al equipo orquestar acciones
para exigir la remediación constante de las vulnerabilidades
e involucrar a los equipos jurídico y de comunicación,
ventas y soporte en las labores para garantizar la seguridad.

## ¿Qué es la gestión de resistencia a ataques?

También en pro de la claridad,
definamos la gestión de resistencia a ataques.
Estamos hablando de una solución que aborda las deficiencias
en la ciberseguridad de las organizaciones en lo que respecta
al conocimiento de la totalidad de sus activos digitales
y su superficie de ataque, la frecuencia de las pruebas de seguridad,
la precisión para realizar dichas pruebas
y la habilidad de los equipos de desarrollo y seguridad.

Por consiguiente,
ARM implica la realización continua de pruebas de seguridad manuales
de toda la superficie de ataque para determinar su exposición al riesgo
y su resistencia a los ataques.
A lo largo de este proceso,
la remediación se realiza lo antes posible
con el apoyo técnico de [*pentesters* o *hackers* éticos](../que-es-hacking-etico/),
y los miembros del personal de las organizaciones mejoran continuamente
sus prácticas de desarrollo seguro.

## Cómo ARM ayuda a los PSIRTs en la detección de vulnerabilidades

El más básico [nivel de madurez operacional](https://www.first.org/standards/frameworks/psirts/psirt_maturity_document)
de un PSIRT requiere que establezca los canales
y la modalidad en que recibirá reportes de vulnerabilidades
en los productos de su organización.
(Hablamos sobre esto extensamente [aquí](../../../blog/iso-iec-29147/)).
Y, sin duda, **los reportes llegan**.
¿Cuántos y cuáles son sus fuentes?
La respuesta a ambas puede ser "¡muchos!".
Además,
estos informes pueden referirse a una vulnerabilidad
en una o varias versiones anteriores del producto.
Incluso puede que ya se haya solucionado cuando el PSIRT recibe el informe.
Para rematar,
si las fuentes de los reportes no usan un formato estándar
y legible por máquinas para comunicar vulnerabilidades
(p. ej., [OASIS SARIF](../../../blog/oasis-sarif/)),
el equipo tiene que gastar más tiempo analizando casos.

No tener ARM implementado puede crear un escenario caótico
en el que el progreso de descubrimiento
de vulnerabilidades del PSIRT se ralentiza.
Cuando ARM está presente, aumenta la madurez operativa del equipo.
En esta área, esta madurez se refleja en lo siguiente:

- Todas las superficies de ataque de los productos de la organización
  (p. ej., web, aplicaciones móviles y basadas en la nube,
  dispositivos IoT, servidores de correo electrónico)
  se conocen y supervisan constantemente a medida que evoluciona el *software*.

- Las vulnerabilidades se buscan de forma constante
  y proactiva desde el inicio del ciclo
  de vida de desarrollo de *software* (SDLC),
  en lugar de hacerlo de forma reactiva.
  Es decir,
  la organización no tiene que esperar a que otros,
  como investigadores o PSIRTs externos, las encuentren.
  Esta prueba de seguridad constante ofrece
  la oportunidad de plantear escenarios hipotéticos.

- Se hace inventario constante del *software* de terceros
  y de código abierto en una lista de materiales de *software* (SBOM)
  y se monitorean estos componentes para detectar alertas de seguridad.

- El historial de vulnerabilidades
  (p. ej., ubicación, autor, fecha del informe, estatus)
  se registra en su totalidad.

- Se dispone de información suficiente para generar reportes
  que midan el éxito del equipo con métricas tales
  como vulnerabilidades descubiertas
  (y su correspondiente exposición al riesgo) y tiempo de respuesta.

El resultado es que los PSIRTs que implementan ARM
son capaces de determinar más rápidamente si las vulnerabilidades
reportadas por fuentes externas ya habían sido identificadas.
Si es así,
estos equipos pueden buscar fácilmente cuándo se crearon
las vulnerabilidades y a qué versiones de la tecnología afectan.
Además, les resulta más fácil averiguar si los sistemas
y versiones afectadas han sido corregidas.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/gestion-vulnerabilidades/"
title="Inicia ahora con la solución de gestión de vulnerabilidades
de Fluid Attacks"
/>
</div>

## Cómo ayuda ARM a los PSIRTs en el triaje de vulnerabilidades

Otro reto para los PSIRTs es hacer triaje de las vulnerabilidades.
Esto implica determinar la validez del reporte.
En el nivel básico,
estos equipos necesitan establecer lo que se considera una vulnerabilidad,
cómo se deben priorizar las vulnerabilidades
y qué tan capaz es su personal para entender la forma
en que funciona el problema.

Sin ARM,
es mucho más difícil saber si los problemas notificados
pueden reproducirse dentro del ambiente del proveedor.
Además, una experiencia técnica (posiblemente) inferior,
o el uso de solo herramientas automatizadas,
hace que sea difícil definir con precisión
si el informe no es un falso positivo y,
si se demuestra que no lo es,
qué nivel de riesgo representa la vulnerabilidad.
Además,
como las simulaciones de ataques no se realizan constantemente,
lo que significa que los equipos pasan menos tiempo
clasificando y analizando vulnerabilidades,
los estándares de priorización se aprenden más despacio.

Un ARM idóneo garantiza a los PSIRTs lo siguiente en cuanto
a triaje y análisis:

- Los *hackers* éticos certificados examinan el sistema
  para detectar vulnerabilidades complejas y diseñar *exploits*.
  Así, los conocimientos técnicos de estos profesionales permiten
  una cualificación más precisa de las vulnerabilidades y su impacto
  (p. ej., exposición al riesgo correspondiente),
  así como una reproducción exacta de las mismas.

- De acuerdo con lo anterior,
  los *hackers* éticos aportan conocimientos para mejorar
  los criterios de cualificación de vulnerabilidades.

- Las pruebas de seguridad constantes
  e integrales mantienen afinado el proceso de triaje,
  lo que permite ganar tiempo
  para adquirir conocimientos sobre la priorización.

En pocas palabras,
ARM permite a los PSIRTs validar,
entender y reproducir vulnerabilidades con mayor precisión,
alcanzando así un nivel de madurez más avanzado.

## Cómo ARM ayuda a los PSIRTs en la remediación de vulnerabilidades

En un nivel básico,
los PSIRTs tienen que determinar como lidiar,
si es que lo hacen, con el riesgo que acompaña a las vulnerabilidades
confirmadas en los productos de sus organizaciones.
La remediación es más difícil cuando no hay un ARM instaurado,
ya que una búsqueda manual proactiva de vulnerabilidades
y una cultura de remediación fuerte pueden no estar establecidas.
Además,
la falta de experiencia del personal
y la ausencia de ayuda por parte de *hackers* expertos
puede dar lugar a un proceso de remediación más difícil
y propenso a errores.

Estas son algunas de las formas en las que ARM puede mejorar
el nivel de madurez de los PSIRT en lo que respecta a remediación:

- Los *hackers* éticos apoyan el proceso con recomendaciones y asesoramiento.

- El equipo puede orquestar la remediación en una fase temprana del SDLC
  (es decir, antes del lanzamiento del producto),
  y al hacerlo constantemente pueden lograr una cultura de remediación sólida.

- Los reportes constantes y detallados
  de exposiciones a riesgos remediadas representadas
  por vulnerabilidades pueden ayudar a la organización a saber
  qué tan fuerte es su compromiso con la seguridad
  y si está proporcionando una solución dentro de los tiempos especificados
  en el acuerdo de nivel de servicio, entre otras métricas de éxito.

En un nivel de madurez avanzado,
a través del constante apoyo de *hackers* éticos de clase mundial sobre ARM,
los PSIRTs pueden remediar vulnerabilidades rápida y efectivamente,
en forma temprana y a través de todo el SDLC.
Además,
pueden acceder fácilmente a datos pertinentes para informar
a las partes interesadas de cómo evoluciona la seguridad de los productos.

## Gestión de resistencia a ataques con Fluid Attacks

Fluid Attacks ofrece gestión de resistencia a ataques
como parte de su solución [Hacking Continuo](../../servicios/hacking-continuo/)
En ella,
los *hackers* éticos de Fluid Attacks evalúan la seguridad
de los productos de sus organizaciones clientes
de forma continua a lo largo de todo el SDLC.
Estas gestionan la superficie de ataque de cada uno
de sus productos en la [plataforma](https://app.fluidattacks.com/)
de Fluid Attacks,
donde los resultados de las pruebas de seguridad están fácilmente disponibles,
mostrando detalles minuciosos,
historial y evidencia de las vulnerabilidades encontradas.
A través de esta plataforma,
los PSIRTs pueden asignar la remediación de vulnerabilidades
a su personal y acceder a métricas útiles
sobre lo bien que están reduciendo la exposición al riesgo en sus productos.
Esta plataforma también ofrece opciones de apoyo que conectan
a los PSIRTs con *hackers* que pueden ayudar a la remediación con mayor detalle.
Y todo eso es una pequeña muestra de lo que ofrece la plataforma.

¿Te interesa proteger tu producto con la mejor solución?
[Contáctanos](../../contactanos/).
Si simplemente deseas explorar la plataforma de Fluid Attacks,
obtén la [prueba gratuita de 21 días](https://app.fluidattacks.com/SignUp)
de plan Essential de Hacking Continuo y
observa los resultados de las pruebas
de seguridad automatizadas realizadas para evaluar tu producto,
junto con muchas de las funciones descritas anteriormente.
