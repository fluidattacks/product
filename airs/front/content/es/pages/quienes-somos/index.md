---
slug: quienes-somos/
title: Quiénes somos
description: Fluid Attacks es una empresa de ciberseguridad que ofrece productos y servicios de hacking ético y pentesting.
keywords: Fluid Attacks, Quienes Somos, Compañia, Hacking Etico, Pentesting, Ciberseguridad
banner: about-us-bg
---

Desde **2001**,
Fluid Attacks ha apostado por crecer como equipo
y desarrollar tecnología propia
para **contribuir a la ciberseguridad mundial**.

## Nuestra misión

### Ayudar a las organizaciones a desarrollar y desplegar *software* seguro

Nuestro **[Hacking Continuo](../servicios/hacking-continuo/)**
es una **solución todo en uno**
que combina nuestras propias herramientas,
IA y equipo de *hacking* certificado
a lo largo de todo el ciclo de vida de desarrollo de *software*
para identificar vulnerabilidades de seguridad con precisión,
manteniendo al mínimo los falsos positivos y falsos negativos.

Además de informar con prontitud sobre la exposición al riesgo,
ayudamos a nuestros clientes a través de nuestra **[plataforma](https://app.fluidattacks.com/)**
a comprender, gestionar y solucionar fácilmente los problemas
de seguridad de su *software* y,
de este modo,
lograr **altas tasas de remediación**
y garantizar productos seguros y de alta calidad a sus usuarios finales.

## Principios

### Fluid Attacks está plenamente comprometido con las prácticas éticas

**Honestidad:**
Esta virtud es primordial en todas nuestras interacciones comerciales;
es algo que ofrecemos a terceros y,
en consecuencia,
esperamos a cambio.

**Trabajo en equipo:**
Conseguimos nuestros resultados mediante esfuerzos de colaboración.
Por lo tanto,
defendemos un trato digno y respetuoso para todos los miembros del equipo.

**Discipline:**
Cumplimos todos nuestros compromisos con inquebrantable dedicación,
perseverancia y total orientación a la excelencia
en nombre de todos nuestros clientes.

### Cómo aplicamos nuestros principios en relación con nuestros clientes

He aquí algunos ejemplos:

- Solo realizamos *hacking* ético con la autorización previa del cliente
  propietario del objetivo de evaluación.

- Informamos al cliente de todos los problemas de seguridad
  que identificamos en su *software*.

- No revelamos información sobre las vulnerabilidades detectadas
  a terceros no autorizados dentro o fuera de Fluid Attacks.

- Si cometemos un error,
  informamos al cliente,
  hacemos todo lo necesario para solucionarlo
  y asumimos toda la responsabilidad por cualquier impacto negativo.

- Tras completar un proyecto,
  buscamos eliminar todo tipo de infecciones de los sistemas del cliente
  que formaron parte de nuestras pruebas.

- No guardamos registros de las vulnerabilidades identificadas
  en el *software* del cliente
  una vez cerrado el proyecto.

## Fluid Attacks es una CNA

Somos una **CVE Numbering Authority** (CNA)
y un laboratorio CVE [entre el top 10 a nivel mundial](https://github.com/fluidattacks/awesome-cvelabs).
A través de nuestro equipo de investigación,
identificamos y [divulgamos problemas de seguridad](../../advisories/)
en *software* de código abierto.

## Prueba gratuita

Busca vulnerabilidades en tus aplicaciones de forma gratuita
con las pruebas de seguridad automatizadas de Fluid Attacks.
Comienza tu [prueba gratuita de 21 días](https://app.fluidattacks.com/SignUp)
y descubre las ventajas del [plan Essential de Hacking Continuo](../planes/).
Si prefieres el plan Advanced,
el cual incluye la experiencia del equipo de *hacking* de Fluid Attacks,
llena [este formulario de contacto](../contactanos/).
