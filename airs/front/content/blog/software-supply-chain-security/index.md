---
slug: software-supply-chain-security/
title: Software Supply Chain Security
date: 2023-08-25
subtitle: Towards an approach that engages more than SCA and SBOM
category: philosophy
tags: software, code, security-testing, trend, company
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1692987683/blog/software-supply-chain-security/cover_software_supply_chain_security.webp
alt: Photo by Google DeepMind on Unsplash
description: Understand what a comprehensive software supply chain security approach should aim at and why it is crucial to implement it during your company's SDLCs.
keywords: Software Supply Chain Security, Sscs, Software Supply Chain Attacks, Software Composition Analysis, Sca, Software Bill Of Materials, Sbom, Pentesting, Ethical Hacking
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Content Writer and Editor
source: https://unsplash.com/photos/Krw-2KP7bOE
---

Holistic or comprehensive approaches are
what the cybersecurity industry is increasingly aiming at
with its products, solutions and services.
Indeed,
we should ask ourselves,
why do we focus on limited-scope examination of our software products
when even the remotest of its parts,
when compromised,
can end up damaging the whole?
It's precisely in recent years that
one such approach has been gaining more and more momentum and acceptance
for software development and deployment.
We're talking about "software supply chain security" (SSCS),
possibly a dominant standard in the near future.

Before going on to explain this concept,
let's answer a couple of questions.

## What is a software supply chain?

As we know,
a chain is a series of interconnected objects.
When we talk about a software supply chain,
we refer to a network of components,
tools and processes
(also involving people and organizations)
engaged in the software development lifecycle (SDLC).
All these chain elements provide some structure,
function or other valuable input to give shape,
functionality and quality to the software product in question.

Nowadays,
software development doesn't usually happen from scratch.
On the contrary,
it depends to a large extent on the work of others
(on which a certain amount of trust is placed):
packages, libraries and other software components,
whether open-source or commercial.
[In a previous post](../sca-scans/),
we pointed out that,
in fact,
open-source components constitute more than 70% of the total code
of the average application.
The benefits of this way of working for a company
are mainly higher development speed,
lower production cost and shorter time to market.

All those third-party software components are integrated,
analyzed and monitored with the help of tools and people
and have specific relationships with each other,
which are called dependencies,
giving rise to the supply chains.
Both components and dependencies may have licensing issues,
configuration problems and vulnerabilities
that can affect the final product's security.
While these individual difficulties can be easily solved,
when it comes to very complex chains,
many of their elements may go unnoticed
or be completely unknown to those who have resorted to them.
But how does this happen?

Let's say you chose an open-source component
to fulfill a particular functionality in your software product.
That component,
in turn,
may be dependent on other pieces of code,
which in turn may be connected to others,
and so on.
Hence,
the so-called dependency trees.
Being aware of that first relationship of your software with the component
and not recognizing all the other elements of the tree
can generate a false sense of security.
The broader and more complex the dependency trees are,
the more entry points there may be for attackers
to access them and compromise the supply chain.

## What are the supply chain attacks?

If an attacker identifies and exploits a vulnerability in a component
that is not directly related to
or is two or more levels away from
your application
—let's say by analogy that the issue is in a branch
that sprouted distant from the main trunk of the tree—
it can still affect your application.
Moreover,
all those organizations also using that component as part of their products
can be compromised.
Once the vulnerability is exploited,
whether or not with the use of malware,
the threat actor can gain access to systems,
steal sensitive information,
alter or disrupt operations
or take control of all or almost all of the systems involved.
In other words,
an attack on a single point can affect a whole series of elements
within the chain.
These are supply chain attacks.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/vulnerability-management/"
title="Get started with Fluid Attacks' Vulnerability Management solution
right now"
/>
</div>

Attackers have traditionally focused on exploiting known vulnerabilities
that many organizations ignore
and, therefore, have not patched or remediated.
Once a vulnerability in an open-source component is publicly reported,
criminals desperately seek to attack those systems
that have not yet been patched.
[More recently](https://www.cisa.gov/sites/default/files/publications/ESF_SECURING_THE_SOFTWARE_SUPPLY_CHAIN_DEVELOPERS.PDF),
without waiting for vulnerability disclosure,
threat actors have been increasingly aiming to inject malware
into widely used third-party components and their updates
so that they affect vast numbers of organizations and individuals
when distributed.

Related to the above,
these criminals have sought to compromise software development tools.
They have even ventured to hack into developers' accounts
—including trusted contributors to open-source projects—
on code-sharing platforms such as GitLab and GitHub
and then going on to infect or otherwise corrupt the projects they work on
and ultimately affect everyone
who makes use of those software products.

Situations like the aforementioned took place
in the widely publicized supply chain attacks of [SolarWinds](../solarwinds-attack/)
and [Log4j](../log4shell/).
The SolarWinds attack occurred in 2020,
specifically through malware injection
into an update of its Orion platform.
This ended up affecting thousands of companies and U.S. government agencies
that installed the infected update.
In the case of Log4j
—an Apache library that appeared to be used in millions of apps worldwide—
a vulnerability was identified in 2021
that had apparently existed for more than seven years.
By exploiting this security flaw,
attackers could make applications perform actions that allowed,
for example,
data leakage or the installation of malicious code.

Based on cases such as the above,
initiatives have been created and strengthened
to motivate the implementation of security measures
related to software supply chains.
The Biden Administration,
for instance,
issued an [Executive Order](https://www.whitehouse.gov/briefing-room/presidential-actions/2021/05/12/executive-order-on-improving-the-nations-cybersecurity/)
in May 2021
to improve the nation's cybersecurity,
including areas such as supply chain security.
In February 2022,
NIST released the first version
of the "[Secure Software Development Framework](https://csrc.nist.gov/pubs/sp/800/218/final)"
(SSDF),
and in August,
CISA published the "[Securing the Software Supply Chain](https://www.cisa.gov/sites/default/files/publications/ESF_SECURING_THE_SOFTWARE_SUPPLY_CHAIN_DEVELOPERS.PDF):
Recommended Practices Guide For Developers."
While,
in terms of requirements,
these regulations only apply to those organizations
doing business with the federal government,
what is required there will surely come to be seen as a responsibility
and even a necessity in the private sector
as well.

## What is software supply chain security?

The more we use supply chains,
the more attack attempts will increase,
and the more we will have to worry about their security.
So far,
talk of software supply chain security (SSCS) invites many to think
only of processes such as software composition analysis (SCA),
sometimes including software bill of materials (SBOM),
procedures currently growing in adoption
among security-minded companies.
However,
a security approach with only these two elements
for an entire supply chain
would be insufficient.
SSCS is not just SCA with SBOM.

The SCA focuses on recognizing and evaluating open-source components
and their dependencies.
In the recognition part,
the SBOM,
a detailed inventory or listing of all such components and dependencies,
can come into play.
In the evaluation part,
license conflicts are reviewed,
and known vulnerabilities are identified
by matching with a database.
However,
the SBOM can also provide information
beyond those open-source components.
And it becomes even more helpful
when it updates its reports whenever changes occur
during the SDLC.

Although both processes significantly contribute
to a software product's security,
they only address part of what we initially noted
as the software supply chain.
SSCS,
as a comprehensive approach,
should include all the necessary measures
to ensure that all operations,
tools and components involved in software development and deployment
are secure.
So,
it is not only about verifying and analyzing third-party components,
but also the proprietary code,
infrastructure,
tools and people involved in the development,
and even more.

As a quick checklist,
these are some of the aspects to be taken into account in SSCS
during the different phases of the SDLC:

- Keep source code's version control and history of changes.
- Have adequate processes for verifying the identity of the developers.
- Do peer review of code quality
  and include [secure code review by an external agent](../../solutions/secure-code-review/)
  to identify security vulnerabilities.
- Introduce static ([SAST](../../product/sast/))
  and dynamic ([DAST](../../product/dast/))
  code assessment methods.
- Carry out [SCA and SBOMs](../../product/sca/)
  that allow clear visualization of the entire dependency tree
  and its possible flaws.
- Keep a dynamic SBOM throughout the SDLC.
- Always verify the provenance of components
  (automate this as much as possible).
- Perform binary analysis of the code and its dependencies
  to detect potential vulnerabilities.
- Evaluate the behavior of dependencies
  to discover if there are compromised interactions.
- Configure rules for dependencies
  and prevent using those not complying with them.
- Verify supplier policies and procedures
  to ensure they align with best practices and standards.
- Apply audit logs and tool access controls
  for both internal and external parties.
- Evaluate partners and other external individuals
  on factors such as credibility, track record,
  and reputation in the marketplace.
- Keep isolated and secure build environments.
- Review infrastructure configurations (now usually in the cloud)
  and verify the provenance of container images.
- Validate that deployments to production meet strict security requirements.
- Perform high-level assessment procedures
  such as [manual penetration testing](../../product/mpt/)
  and [red teaming](../../solutions/red-teaming/).
- Beyond vulnerability detection and remediation,
  act in favor of malware identification and removal.
- Implement measures for intrusion prevention and detection
  (perform continuous analysis of suspicious behavior).
- Define a sufficiently robust incident management and response plan
  that clarifies what everyone must do and in what order
  when an attack occurs.
- Maintain continuous visibility
  into compliance with security standards requirements,
  ranging from initial implementation of controls,
  scanning at different stages,
  and verification of policies before going into production.
- As part of the [DevSecOps culture](../../solutions/devsecops/),
  in addition to technical controls,
  there must be a governance framework
  from which all parties are educated on best security practices.

If you'd like to know how we at Fluid Attacks can help your company
follow many of the above procedures
for the security of your software supply chains
from our [comprehensive Continuous Hacking service](../../services/continuous-hacking/),
please do not hesitate to [contact us](../../contact-us/).
