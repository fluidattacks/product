---
slug: transparency-for-supply-chain-security/
title: Commitment Should Show
date: 2024-07-17
subtitle: Transparency for fewer supply chain attacks
category: philosophy
tags: cybersecurity, risk
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1721239722/blog/transparency-for-supply-chain-security/cover_transparency.webp
alt: Photo by Wilhelm Gunkel on Unsplash
description: It should be easy to learn how committed the most used OSS libraries are to security. And that transparency we ask for we should practice ourselves.
keywords: Supply Chain Attacks Prevention, Transparency In Cybersecurity, Software Supply Chain Security, Polyfill, Most Used Open Source Libraries, Software Security Practices, Ethical Hacking, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Content Writer and Editor
source: https://unsplash.com/photos/white-and-black-diamond-shape-illustration-3VQ4AfOKCVc
---

## Heard about the latest supply chain attack?

It has happened again.
A software library was recently found
to be working as an open door for attacks
to a massive amount of products
that have it within their foundation.
We have a name for this: supply chain attack.
We're referring this time to the exploitation of polyfill.js.
To those learning about this news just now,
the domain that software products use to embed this library,
with the purpose to make modern functions available on older browsers,
has been sending malicious JS code to such products,
which has resulted in users being redirected to unsolicited websites.
Around three weeks after the news broke out,
more than [80 thousand web pages](https://publicwww.com/websites/%22cdn.polyfill.io%22/)
still use the library.
They use it even as the original author [urged projects to remove polyfill.js](https://www.theregister.com/2024/06/25/polyfillio_china_crisis/).
What's more,
it's in use even as this same person [says](https://community.fastly.com/t/new-options-for-polyfill-io-users/2540)
modern browsers can take care of what the library was made for.

## How committed to security are the most used libraries?

Now,
we want to talk about an event prior to the above attacks,
as we think it's key to understanding a major concern
surrounding supply chain attacks.
So,
polyfill.js had changed ownership in February, 2024.
And upon [realization of the disparity](https://web.archive.org/web/20240229113710/https://github.com/polyfillpolyfill/polyfill-service/issues/2834)
between the creator's comment
("[...] remove [the library] IMMEDIATELY")
and that of another important person previously involved in the project
("I've been in discussions with Funnull for many months
and they will be the new maintainers and operators
of the [library's] GitHub repo [...]"),
users started to have questions.
What to do?
And just who are Funnull?
Indeed,
this last question has led to worrying discoveries.
[Reportedly](https://www.theregister.com/2024/06/25/polyfillio_china_crisis/),
while the firm says it's from the U.S.,
its site is in Mandarin,
the addresses where it claims to have offices are bogus,
and its actual location might be in the Philippines.
Browsing its website,
[people have noticed](https://dev.to/schalkneethling/to-polyfill-or-not-to-polyfillio-5ggd)
that the firm fleetingly mentions a privacy policy
which no one can locate,
and states that users themselves should regularly check the service terms
to learn of any change to the latter.
The ownership change was done carelessly,
without transparent communication
regarding who the new owners and what the implications for users shall be.

The above begs the question:
Who are the ones
maintaining the most common building blocks of modern software?
You know the joke of it being just one unsung hero(ine) taking care
of the one building block
which, if tackled, would cause the whole edifice above it to collapse.
[A previous post](../nebraska-joke-infrastructure-dependency/) argues
that this, though a funny joke, is kind of close to reality.
The Linux Foundation
and The Laboratory for Innovation Science at Harvard
[found](https://www.linuxfoundation.org/press/press-release/the-linux-foundation-and-harvards-lab-for-innovation-science-release-census-of-most-widely-used-open-source-application-libraries)
that for about a quarter of 49 of the most used free and open-source libraries
**just one developer** had contributed
to more than 80% of the given library's lines of code.
And how committed to security are these
and the other most used projects?
The aforementioned research shows
that many of them are maintained from individual accounts
instead of organizational accounts.
The former have less granularity
in regards to controlling permissions and publishing
than the latter.
Further,
one more observation that can be made is
that the vast majority of the most commonly used libraries do not participate
in the Open Source Security Foundation's [Best Practices Badge](https://www.bestpractices.dev/en)
program.
This program stems from The Linux Foundation
and is an opportunity for development projects
to openly share their degree of compliance with many security requirements.
While their not participating in it does not necessarily mean
that they do not indeed follow the expected best practices,
there is room for doubt.
Honestly,
given that our software,
as it is widely known,
is **made up largely of open-source indirect dependencies**
(i.e., packages used by the packages we use in our project),
it's only logical we want to know how safe these dependencies are.
We need transparency,
i.e., projects being open and honest
about their security practices, risks and incidents,
if we want to be able to counter supply chain attacks.
What's more,
we ourselves,
granted that our project may be part of some other project(s),
need to start being transparent, or maintain or refine our transparency.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/vulnerability-management/"
title="Get started with Fluid Attacks' Vulnerability Management solution
right now"
/>
</div>

## Are we doing our part?

Here is some basic information that we should show publicly
and ask all other software development projects to follow suit:

- **Availability:** Make public the rate of requests processed successfully
  in a relevant time interval.

- **Response time:** Make public how much time it takes to respond
  to a relevant amount of requests (e.g., 99% of requests).

- **Source code:** Make source code public,
  as we have argued time and again
  that [publishing our source code doesn't make us unsafe](../oss-security/),
  on the contrary,
  it encourages enhanced security
  due to higher expectations on devs and higher community scrutiny.

- **Incidents:** Reveal the timeline,
  cause,
  impact,
  solution,
  of every problem;
  and as for having suffered a successful cyberattack,
  be truthful and thorough in the report.

- **Security vulnerabilities:** Make public the flaws
  that have been detected in the project,
  especially those that are unmanaged.

<caution-box>

Here are links to the above information for Fluid Attacks:
[Availability, response time](https://availability.fluidattacks.com/),
[source code](https://gitlab.com/fluidattacks/universe/),
[incidents and security vulnerabilities](https://status.fluidattacks.com/history).

</caution-box>

At Fluid Attacks,
we are convinced that if we exhibit what we're doing,
if everyone and their mother can see the above information about our project,
we are bound to do better.
And this carefulness will surely benefit the quality of software supply chains.

Luckily,
there shall be no escaping the need to achieve transparency,
as current and wildly important initiatives and regulations are pushing for it.
If they haven't got to our region,
they still exert pressure for similar measures
to be taken by the rest of the world,
if eventually.
Here are some examples:

- The [rules by the U.S. Securities and Exchange Commission](../sec-new-regulations/)
  (SEC) already require publicly traded companies
  to disclose incidents promptly,
  describe their risk management
  and inform of their management's cybersecurity expertise.
  CISOs can be sure to be legally liable
  for their firm's cybersecurity posture.
  [We talked elsewhere](../trend-forecast-for-2024/#sec-rules-and-greater-liability-of-cisos)
  about how even before these rules,
  a couple of CISOs got in big trouble
  for lying about the state of cybersecurity of their firms.

- The EU's [Cyber Resilience Act](../cyber-resilience-act/)
  is expected to enter into force soon.
  This legal framework will require **all** products
  with digital elements in the EU
  be secured throughout the software development lifecycle (SDLC).
  Monetary noncompliance penalties are already defined.

- [Cybersecurity labeling for IoT](../cybersecurity-labeling-for-iot/)
  (i.e., issuing approval labels to devices
  that comply with key cybersecurity requirements)
  is expected anytime soon in the U.S.,
  as it has been implemented in Europe.
  What's more,
  we can expect labeling to expand to products other than IoT gadgets,
  as it has done in Germany now.
  Such programs are great,
  as they help the consumer quickly learn
  whether secure software development practices are followed,
  and which, and choose the more secure product.

- For some time now,
  the generation of software bills of materials ([SBOMs](../../learn/what-is-software-bill-of-materials/))
  in specific standard formats
  has been a key measure to detect problems
  that expose projects to supply chain attacks.

- The software supply chain security ([SSCS](../software-supply-chain-security/))
  approach has emerged,
  which basically goes beyond SBOMs
  and software composition analysis ([SCA](../sca-scans/)) scans
  to secure even vendor choice,
  non-third-party source code throughout the SDLC,
  development practices and more.

- The Secure Supply Chain Consumption Framework ([S2C2F](https://github.com/ossf/s2c2f)),
  a project also stemming from The Linux Foundation,
  gives an idea
  of how to consume open-source software dependencies
  into developers' workflows.

All this may seem overwhelming,
but clearly it's only fair
that each of us acts in favor of fellow developers',
and ultimately end users',
surety of our security practices proactively preventing successful attacks.
We can help you secure your software
by offering you the best AppSec techniques and tools
to [manage vulnerabilities](../../solutions/vulnerability-management/)
across the SDLC,
not only related to your code
but also to the status of the third-party code you use.
[Contact us](../../contact-us/).
