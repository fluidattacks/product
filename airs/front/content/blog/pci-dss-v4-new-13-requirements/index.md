---
slug: pci-dss-v4-new-13-requirements/
title: 13 New PCI DSS Requirements in v4.0
date: 2024-01-12
subtitle: Comply with the new requirements due for March 2024
category: politics
tags: cybersecurity, compliance, company
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1705073420/blog/pci-dss-v4-new-13-requirements/cover_pci_dss.webp
alt: Photo by Jeremy Perkins on Unsplash
description: Companies that store, handle or transfer account data must comply with PCI DSS v4.0 from March 31. We summarize its 13 new requirements to be met on that date.
keywords: Pci Dss, Roles And Responsibilities, Security Controls, New Requirements, Pci Dss Compliance, Manual Code Review, Develop Secure Software, Ethical Hacking, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Content Writer and Editor
source: https://unsplash.com/photos/brown-deer-on-white-background-BKZawJTA5t4
---

The new requirements to get the PCI DSS v4.0 certification include
documenting, assigning and understanding roles and responsibilities
for implementing security controls;
verifying the scope of the PCI DSS assessment once every 12 months;
performing a targeted risk analysis for every requirement met
with the customized approach;
and supporting customers' requests for information
on PCI DSS requirement responsibility and compliance status.
Compliance with these requirements is expected in a couple of months.

Let us give you a little bit of context.
Entities that store, handle or transfer cardholder information are required
to comply with the security requirements
of the [Payment Card Industry Data Security Standard](https://www.pcisecuritystandards.org/document_library/)
(PCI DSS).
Version 4.0 of the standard poses **64 new technical
and operational requirements**.
These are distributed among the principal requirements,
which are the 12 categories
whose names provide us with a high-level overview.
In the following image,
you can see how their titles changed from v3.2.1 to v4.0.

<image-block>

!["PCI DSS requirements v3.2.1 vs. v4.0 comparison"](https://res.cloudinary.com/fluid-attacks/image/upload/v1705073549/blog/pci-dss-v4-new-13-requirements/pci-dss-requirements-version-comparison-fluid-attacks.webp)

Title changes in PCI DSS principal requirements from v3.2.1 to v4.0

</image-block>

Companies need to start complying with **13** of the new requirements ASAP,
as this version will take effect on **March 31, 2024**.
The remaining 51 are considered best practices
to start following during the year
and will be mandatory starting March 31, 2025.

The penalties due to noncompliance may include sizable fines.
These range from USD 5,000 to 100,000 each month
depending on the firm's period of noncompliance and volume of transactions.
Also,
stakeholders may seize the firm's ability to accept card payment.
Moreover,
public knowledge of these events would have a detrimental effect
on customer trust and the firm's reputation.
With this in mind,
as well as your purpose to keep your firm's and clients' data safe,
let's look at what's new.

## Documenting, assigning and understanding roles and responsibilities

Ten out of the 13 new requirements ask for
**documenting, assigning and understanding the roles and responsibilities**
for implementing security controls.
In the [prioritized approach](https://www.pcisecuritystandards.org/document_library/)
formally presented by the standard,
in case this is your first rodeo,
it is suggested that fulfillment of this requirement be left
for after other more basic ones in the principal requirement are achieved.

To summarize,
these are our simplified versions of some of the requirements
that your team should meet first
and for which roles and responsibilities should be clear:

- **Build and maintain a secure network and systems:**
  Resolve the issues of risky configurations in vendor default accounts,
  primary functions requiring different security levels,
  unnecessary functionalities, among others.

- **Protect account data:**
  Implement data retention and disposal policies, procedures and processes
  to keep storage of account data to a minimum;
  use strong cryptography
  to keep primary account numbers safe;
  take measures to protect cryptographic keys; etc.

- **Maintain a vulnerability management program:**
  Deploy antimalware solutions;
  develop software based on industry standards and best practices;
  [analyze code](../../product/sast/) for vulnerabilities
  and remediate them prior to releasing it into production;
  if conducting [manual code review](../manual-code-review/),
  have it done by individuals with vast knowledge
  about [code-review techniques](../secure-code-review/)
  and [secure coding practices](../secure-coding-practices/); etc.

- **Implement strong access control measures:**
  Define and assign access appropriately;
  use an access control system;
  implement MFA;
  use strong cryptography to render all authentication factors unreadable;
  restrict physical access to systems
  through appropriate facility entry controls; etc.

- **Regularly monitor and test networks:**
  Enable, review and protect audit logs
  (i.e., the chronological record of system activities);
  conduct [vulnerability scans](../vulnerability-scan/);
  have [internal and external penetration testing](../types-of-penetration-testing/)
  performed at least once every 12 months; etc.

- **Maintain an information security policy:**
  Document and implement acceptable use policies for end-user technologies;
  document and validate the scope of the PCI DSS review;
  monitor third-party service providers' PCI DSS compliance; etc.

<caution-box>

PCI DSS v4.0 introduces the category "account data,"
instead of just "cardholder data,"
to refer to the primary account number (PAN)
and any other elements of two categories:
cardholder data and sensitive authentication data.
The former includes the PAN, cardholder name,
expiration date
and service code;
the latter includes full track data,
card verification code
and PINs/PIN blocks.

</caution-box>

Later,
as we grasp it,
your team can work on the actual documentation
within policies and procedures or a dedicated file.
However,
to us,
the prioritization advice seems fitting only for creating the documentation,
since roles and responsibilities should be assigned formally
and understood
for the actual security controls to be put in place.

Further advice given in the file is the creation of a RACI matrix.
It is so called as it displays who is responsible,
accountable,
consulted
and informed.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/secure-code-review/"
title="Get started with Fluid Attacks' Secure Code Review solution right now"
/>
</div>

## New for maintaining an information security policy

The three new remaining requirements due in 2024
are linked to the organizational policy and programs
to support information security.

Out of these requirements,
the one to meet first is
**documenting and confirming the scope of the PCI DSS review
at least once every 12 months**.
This means your team has to verify
that the locations and flows of account data
and the system components (e.g., software, cloud components, network devices)
to be protected
are added to the scope of the review.

The advice is to create a spreadsheet and record

- where the data is stored, why and for how long;

- what data is stored
  (i.e., elements of cardholder and/or sensitive authentication data);

- how the data is secured;

- how access to the data is logged.

Moreover,
your team should identify not only your internal systems and networks
but also all connections from any third party.

One more new requirement is
**performing a targeted risk analysis for every requirement** met
with the customized approach.
To be clear,
entities pursuing certification can follow either the defined
or the customized approach.
Whereas the defined approach means implementing the security controls
just as the standard describes them,
the customized approach involves reaching the objectives of requirements
without necessarily applying the exact technologies or operations
spelled out in the standard.
To qualify for the latter,
an entity [must](https://www.pcisecuritystandards.org/document_library/)
"demonstrate a robust risk-management approach to security"
(e.g., having a dedicated risk management department).

If your team meets PCI DSS requirements with the customized approach,
then you will have to do a risk analysis for each of those items
at least once every 12 months.
That is,
you will need to define in detail
what would be the effect on security if the requirement is not met
and say how the controls you apply provide the needed protection.
The signature of a member of executive management must be present
to certify that they reviewed and approved the strategy.
There is a sample template in the document
to help you know the elements of the analyses you need to report.

The last new requirement due this year is directed only
at third-party service providers (TPSPs;
i.e., businesses for which PCI DSS applies
but who are not a payment brand),
such as **payment gateways**.
Some clarity is needed first.
An entity depending on TPSPs needs to monitor the latter's PCI DSS compliance
and maintain information on which requirements each has to meet.

The requirement is then for TPSPs to
**support customers' requests for information
on PCI DSS requirement responsibility and compliance status**.
This will help the entity pursuing compliance
meet the requirements mentioned above.

TPSPs can provide the customer with their PCI DSS Attestation of Compliance
(AOC).
And when TPSPs are not certified,
at least they may provide the customer's assessor with specific evidence
of their compliance with PCI DSS requirements.

## Fluid Attacks analyzes your compliance with PCI DSS requirements

We test your software to verify [PCI DSS compliance](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-pci).
Our [flagship plan](../../plans/) offers vulnerability scanning
with multiple techniques,
manual code review by our pentesters,
vulnerability management on our platform,
and expert and AI-assisted vulnerability remediation.
We help you secure your software from the beginning of development
and before you release it into production or to customers,
which is in line with PCI DSS requirement 6.

If you want to test our tools first,
[start a free trial](https://app.fluidattacks.com/).
