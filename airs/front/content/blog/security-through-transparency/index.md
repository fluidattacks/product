---
slug: security-through-transparency/
title: Security Through Transparency
date: 2024-09-27
subtitle: Be more secure by increasing trust in your software
category: opinions
tags: cybersecurity, risk, company, trend, code
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1727477788/blog/security-through-transparency/cover_security_through_transparency.webp
alt: Photo by photo nic on Unsplash
description: Being secure and being transparent are commonly seen as opposites, but they aren't. Learn how openness can become a business differentiator.
keywords: Transparency, Security, Open Source Software, Openness, Transparency As A Culture, Obscurity, Ethical Hacking, Pentesting
author: Juan Díaz
writer: jdiaz
name: Juan Díaz
about1: Security Developer
source: https://unsplash.com/photos/person-holding-black-smartphone-_IL9n-5Ou6c
---

Being secure and being transparent are commonly seen as opposite concepts.
There are practices like security through obscurity
that promote the lack of information as a way to protect systems.
If attackers don't know or understand your software system,
they cannot exploit its security vulnerabilities.
Therefore,
details such as source code, architecture, and inter-process communication
are kept secret by many organizations
with the intention of protecting their users and their data.

However,
authoritative bodies such as NIST
(National Institute of Standards and Technology)
and CWE (Common Weakness Enumeration)
identify "[Reliance on security through obscurity](https://cwe.mitre.org/data/definitions/656.html)"
as a significant weakness.
Techniques such as black-box hacking, reverse engineering,
or the abuse of information leaks
allow the exploitation of systems hidden by obscurity,
making it clear that this strategy is ineffective
and only provides a false sense of security.

If obscurity is not a solution,
then transparency is?
I think that responsible transparency brings more security advantages
than expected,
so let's talk about it:

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/security-testing/"
title="Get started with Fluid Attacks' Security Testing solution right now"
/>
</div>

## The reasons behind security

Security is not only about protecting user data or business differentiators.
It is also about preserving the trust users have in your system.
Trust is the most important and invaluable asset
that a company can have
and the most difficult to regain once lost.
Confidence, itself, could be a business differentiator.

Think about the applications you use daily
and list how many of them you prefer because you trust them.
For instance,
your favorite transportation app,
which you chose over cheaper options;
your favorite messaging app,
which you preferred over some with more features;
your banking app;
your homestay app, etc.
When applications build trust,
they receive users who consolidate their use
and tell other users about their pleasant experience with them.

Organizations like Uber and Airbnb
understand [the importance of trust](https://review.firstround.com/how-modern-marketplaces-like-uber-airbnb-build-trust-to-hit-liquidity/)
and actively invest in it.
In their apps,
they offer user-centric content,
provide beautiful interfaces,
and add social features
(e.g., sharing information about trips or homestay locations,
voting in rating systems, etc.)
to build trust from the enjoyable experience.

Ultimately,
strategies to attract or retain users based on experience
depend heavily on understanding the cons of other solutions in the market.
What could companies do to use users’ trust as a differentiator?

## Transparency effect

While we know that openness is a key factor in building trust between humans,
it also serves to build trust between humans and systems.
Think for a moment about how highly mature big tech companies
like Google, Meta, Amazon, or Microsoft
are providing a lot of information about their infrastructure,
practices, and security strategies without apparent profit motive.

AWS, for example, invites companies with good cloud practices
[to share information about their infrastructure](https://aws.amazon.com/architecture/this-is-my-architecture/?tma.sort-by=item.additionalFields.airDate&tma.sort-order=desc&awsf.category=*all&awsf.industry=*all&awsf.language=*all&awsf.show=*all&awsf.product=*all)
and how they solve the problems they face.
What's more,
AWS itself shares how it built its services
to guarantee security and reliability for all its users.

Meanwhile,
Microsoft maintains excellent documentation,
which includes rationales and explanations [for its technical decisions](https://learn.microsoft.com/en-us/microsoft-365/solutions/?view=o365-worldwide),
thus providing knowledge and earning users' trust.
Security practices are also material
that can be in-app documented and shared with the public,
[as WhatsApp, for instance, does](https://faq.whatsapp.com/820124435853543)
with its end-to-end encryption.

On the other hand,
Meta has a good example of a [status page](https://metastatus.com/)
where people can be aware of affected services across apps
(e.g., Instagram, Facebook, WhatsApp Business API, etc.).
This page informs users about minor and major disruptions
and the work in progress to fix them.
Additionally,
status pages can include metrics and regional filters,
as is the case with [AWS status page](https://health.aws.amazon.com/health/status).

Users have a positive impression and a sense of security
when companies are transparent.
Even they may end up defending wholeheartedly companies
that have earned their trust.
Companies are,
at the end of the day,
social agents that strengthen relationships with their customers.

## Transparency as a culture

Even if a company implements transparency practices
such as those described above,
trust is built both externally with the users and internally with the team.
In an organization, [openness transforms](https://www.axelerant.com/blog/openness-at-axelerant)
the psychological safety, conversations and feedback to be received.
Openness in the business culture grants the appropriate conditions
to grow and learn a lot in better, in worse, in sickness, in health…

Teams turn into communities
where solving problems is a group challenge
rather than individual work
motivated merely by the desire for personal achievement
or the fear of being fired.
Collaborative teams are born of transparent organizations.

I'm still very fascinated by the biggest [GitLab outage](https://www.youtube.com/watch?v=tLdRBsuvVKc),
but mostly by the team's response.
A developer accidentally deleted the production database
along with its primary backup.
GitLab engineers explained the situation
and began a live stream to document their efforts in fixing the issue.
It's a very stressful situation,
but GitLab is an open-source project,
and each contributor has a strong commitment to transparency
and open knowledge.
So, they chose to share their outage publicly as a damage control strategy.

GitLab is still one of the most widely used hosts for storing code
after that failure.
They taught all of us to use transparency
to mitigate potential bad impressions on users.
Undoubtedly, group effort and openness in resolving an incident
protect and maintain users' confidence in products and services.

## Conclusions

Among the many ways to enhance security and build trust,
adopting a well-executed transparency strategy
can provide significant benefits to your business.
It's not just about being open source or sharing knowledge freely;
it's also about understanding that trust is earned and maintained
by being as transparent as necessary
with both your users and your team to foster growth.

Security through transparency is a viable strategy
that businesses should consider to strengthen their market position.
Being open and sharing knowledge is always worth it.
That's why at Fluid Attacks, we don't just talk about transparency,
we embody it.
