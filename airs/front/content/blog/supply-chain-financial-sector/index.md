---
slug: supply-chain-financial-sector/
title: Safe Supply Chain in Financial Orgs
date: 2024-06-14
subtitle: Software supply chain management in financial services
category: philosophy
tags: cybersecurity, company, trend, risk, software
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1718314726/blog/supply_chain_finance_sector/cover_finance_supply_chain.webp
alt: Photo by FlyD on Unsplash
description: The financial sector's software supply chain is a difficult undertaking to manage. But it is a possible task. We discuss threats, trends and provide advice.
keywords: Supply Chain, Financial Institutions, Supply Chain Attacks Financial Institutions, Third Party Vendor, Supply Chain Security, Threats And Trends, Supply Chain Risk Management Recommendations, Ethical Hacking, Pentesting
author: Wendy Rodriguez
writer: wrodriguez
name: Wendy Rodriguez
about1: Content Writer and Editor
source: https://unsplash.com/photos/pink-padlock-on-silver-chain-IMbquw-IQhg
---

As we’ve seen in our previous [blog post](../top-financial-data-breaches/),
financial institutions have long been a priced target for cyberattacks.
And now, an emerging threat has been on the rise: supply chain attacks.
These attacks don’t target the financial institution directly,
but rather infiltrate its software vendors
or other partners within the complex web
of suppliers that make up the modern financial ecosystem.
Once inside,
attackers cause a ripple effect faced by both the affected vendor
and the entity using that service.
Both companies may experience data breaches,
malware installation, operational difficulties
and possibly even future regulatory fines
A successful supply chain attack can also damage
the trust of customers and partners alike.

Last year,
the landscape of the threats in the software supply chain showed
how the level of sophistication of these attacks is changing,
making them more dangerous.
In 2023,
sophisticated attacks targeting the banking sector were uncovered.
In both attacks,
threat actors uploaded malicious packages to NPM,
the largest software registry in the world.
In the first attack,
the package included a payload made to secretly intercept login information
and send it to a remote address.
In the second attack, the packages contained scripts that, upon installation,
identified the operating system of the victim
and decoded relevant encrypted files from the packages.
Afterwards,
the files were used to download a malicious binary onto the victim’s system.
The threat actor leveraged the Havoc Framework,
which is an advanced post-exploitation command and control framework.
This framework enables attackers to manage
and evade attacks by bypassing the security measures such as Windows Defender.
It’s important to mention that the attacker posed as a bank employee
with a fake LinkedIn page associated with the bank,
which led to security experts researching the incident
to consider that the whole situation could be a penetration test.
This demonstrates a clever strategy that attackers can use to trick researchers.

These attacks underscore the sophistication
in which malicious actors are carrying out their attacks.
Methods are evolving to exploit vulnerabilities in the supply chain,
making it imperative for security measures
to also evolve into a stronger front.
We’ll discuss some best practices
and recommendations for those involved with protecting financial institutions.
But first, let’s remember what a supply chain attack is.

## What is a supply chain attack?

A supply chain attack is a kind of cyberattack
that targets weak links in an company’s network,
exploiting vulnerabilities in vendors and third-party suppliers.
Threat actors gain access to these systems and use them
to launch attacks on the final target,
the financial institution in this case.
Supply chain attacks are dangerous
because they can be very difficult to detect.
Organizations may not even be aware that their vendors
have been compromised or they may not even have clear visibility
of third-party components in their software.

These attacks target trusted suppliers
who provide software or services that are crucial
to the organization’s supply chain.
This could be a supplier, vendor,
a customer or a third-party software library
that the company is dependent upon.
Attackers can tamper with software during the manufacturing
or distribution process.
They could target a trusted vendor
and gain access through a weakness they find.
They can steal credentials of the suppliers,
or even track vulnerabilities
and seek out the organizations that use the vulnerable vendor.

### Why is the financial industry vulnerable to supply chain attacks?

Aside from being a high-value target
because of the [sensitive information](../protecting-data-financial-services/)
they access and handle,
financial institutions are particularly vulnerable.
They extensively use third-party services
for their online platforms.
They also use third-party services for cloud-storage,
data processing or other crucial functions.
These complex software ecosystems means a greater number of entry points,
an expanded attack surface, making
[**supply chain security**](../software-supply-chain-security/)
a critical priority.

## Trends and threats

As previously stated,
supply chain attacks are becoming more sophisticated
and are now targeting new areas with combined tactics.
Last year, key trends included:

- **Multi-package attacks:** Attackers split malicious actions
  across multiple packages to evade detection.

- **Commit fraud:** Cybercriminals were seen acting
  like legitimate and well-known sources while introducing their malicious code.

- **Abandoned digital assets:** Attackers exploited old assets,
  like deserted AWS buckets, to introduce malicious code and make
  them seem like a trusting delivery mechanism.

- **Good old social engineering:** Social media was used to establish trust;
  case (1) fake developer profiles that tricked users into using malicious
  open-source packages,
  and case (2) reused Proof of Concepts (PoCs) to create misleading scripts
  for recently disclosed vulnerabilities.

Software supply chain attacks are not declining in 2024;
on the contrary, they’re expected to evolve.
This leads to enhanced challenges and risks for companies,
specifically the well sought-after financial sector.
The threats highlighted by [SecurityWeek’s Cyber Insights](https://www.securityweek.com/cyber-insights-2024-supply-chain/)
are the following:

- State-sponsored attacks that look to destabilize economies

- Concentration risks within extended supply chains,
  including reliance on a single supplier or fourth parties

- Sophisticated supply chain attacks that leverage AI/ML

- Joint ventures among technically skilled criminal groups
  to target supply chains

- Open-source software (OSS) corruption with malicious packages
  on public repositories

### What are supply chain cyber risks?

Due to their complex ecosystems,
financial institutions face a diverse range of risks
from different entry points.
According to a study on trends in supply chain cyber risk management
from 2022 to 2023,
these are possible cyber risks and their probable entry points:

<image-block>

!["Examples of anticipated cyber risks"](https://res.cloudinary.com/fluid-attacks/image/upload/v1718315064/blog/supply_chain_finance_sector/image_study_supply_chain_risks.webp)

Taken from the study conducted by [PwC](https://www.pwc.com/jp/en/knowledge/thoughtleadership/assets/pdf/financial-supply-chain-cyber-risk.pdf).

</image-block>

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/vulnerability-management/"
title="Get started with Fluid Attacks' Vulnerability Management
solution right now"
/>
</div>

## Supply chain cyber risk management recommendations

Thanks to regulatory authorities,
companies are pushed to have guidelines that provide
specific conditions they must comply with
when using a vendor service.
For example, PCI DSS published a document
called “Information Supplement: Third-Party Security Assurance”.
This document outlines the specific PCI DSS requirements
applicable to providers.
It provides guidance to both the supplier
and the company acquiring the service.
It also goes through many controls both companies need to have,
like secure access control and network security.
A responsible use of vendors is also encouraged
by other laws and regulations.
The California Consumer Privacy Act (CCPA)
and the General Data Protection Regulations (GDPR) among others.
These laws mandate companies make sure the suppliers they work with adhere
to the applicable regulations.

The following recommendations for software supply chain security
came from financial institutions experts around the world.
They discuss the stages and the advice a person
in charge of supply chain security should consider
when choosing and dealing with a supplier.

- Pre-selection stage: security risk pre-screening

    - Collect and evaluate data that is openly available
      to the public in order to conduct security risk assessments
      on the supplier and its products.
      Using external risk evaluation services could make this procedure faster.

- Contracting stage: security risks assessment

    - Assign technical/security staff members to the assessment team,
      and base the evaluation on current threat scenarios.
    - Prepare for the possibility of incidents and document the extent
      of duties and deadlines for reporting such events
      in a service level agreement.

- Security risk management with subcontractors

    - Examine the risk management procedures of subcontractors
      who handle high-risk systems in an in-person visit.
    - If work is subcontracted to a fourth party,
      insist that security management be handled by the original subcontractor.
      Require all parties further in the chain implement security
      at the same extent as your own company does.

- Software management

    - When new products are installed,
      software [configuration management](../../learn/what-is-configuration-management/)
      needs to be performed
      and closely linked with [vulnerability management](../what-is-vulnerability-management/).
    - Utilize software bills of materials ([SBOMs](../../learn/what-is-software-bill-of-materials/))
      to oversee resources,
      improve efficiency and security.
    - Make use of management tools for open source software
      to simplify the process of choosing it
      and identifying dependencies.

- Hardware management

    - Manage assets meticulously, and create a mechanism that
      makes it possible for firmware updates to happen swiftly.
    - Carry out security tests based on threat scenarios.

- Reporting to senior management

    - Write reports in “business” language and base them on how
      the expenses will affect revenue, client satisfaction,
      brand loyalty, and reputation.
    - Regarding cybersecurity risks report,
      consider approaches that are risk-based,
      monetary cost-based, and IT-based to determine
      the route best suited to your company.

### Software supply chain security best practices

Securing the software supply chain is critical
for financial institutions to be better prepared
and protected against cyber threats.

1. **Manage risks from software vendors:** Before onboarding with a vendor,
   ensure they meet your security requirements.
   Perform a comprehensive assessment of their security posture
   (e.g., secure development practices, incident responses,
   access controls, etc.)
   before installing anything and keep monitoring it afterwards.
   Within the contractual clauses,
   require the vendor to meet your specific standards
   and notify you of any security incidents.

2. **Use a software composition analysis (SCA) tool:** Manage third-party
   and open-source components in your software.
   SCA can identify open-source software vulnerabilities,
   which helps you to prioritize patching
   or upgrading vulnerable components before attackers get to exploit them.
   It can also help with identifying license conflicts and generating SBOMs.
   The above is offered by Fluid Attacks’ Continuous Hacking
   with its [SCA technique](../../product/sca/).

3. **Create your own software bill of materials (SBOM):** Create and keep
   a detailed SBOM that lists all software components
   used within your company,
   including open-source libraries and third-party dependencies.
   This helps you identify possible vulnerabilities in your supply chain.
   When developing software,
   a well-kept SBOM throughout the SDLC helps minimize issues in the future.
   There are standard SBOM formats, like CycloneDX and SPDX,
   which helps having high-quality SBOMs.
   Our platform generates SBOMs using these two standard formats,
   which show information, dependencies and vulnerabilities coherently.
   Learn more about Fluid Attacks’ SBOM process [here](https://help.fluidattacks.com/portal/en/kb/articles/analyze-your-supply-chain-security).

4. **Secure the SDLC:** Integrate security practices, like SBOMs,
   throughout the entire SDLC, from code development to deployment.
   [Code reviews](../secure-code-review/),
   [vulnerability scanning](../vulnerability-scan/),
   [penetration testing](../penetration-testing/),
   and, very importantly,
   [vulnerability remediation](../vulnerability-remediation-process/)
   are included as security practices.

5. **Manage vulnerabilities:** Proactively identifying and addressing
   vulnerabilities can help to significantly reduce the risk
   of successful attack targeting weaknesses
   within your development environment,
   including your third-party software components.
   Early detection allows you to patch or update vulnerable components promptly.
   Continuous scans and manual reviews of digital assets
   can help to find potential vulnerabilities before an attacker does.
   Vulnerability management also helps improve vendor management
   (continuous monitoring and scanning help keep the supplier honest).
   It can also help comply with regulations
   along with reducing holes in your attack surface.
   See how you can benefit from Fluid Attacks’
   [vulnerability management solution](../../solutions/vulnerability-management/).

## Testing continuously for supply chain management

Managing software supply risks can be enhanced by all of the above.
But we would suggest going a step further
by testing continuously with pentesters.
Securing your supply chain goes beyond managing vendor risks.
Indeed,
the software supply chain security approach
includes securing proprietary software,
infrastructure and more.

Our AppSec solution includes pentesters or [ethical hackers](../../solutions/ethical-hacking/),
who think like malicious actors.
They know their strategies and use that knowledge
to guide you to defend your systems.
Our pentesters perform manual testing,
working along vulnerability scanning tools
([SAST](../../product/sast/),
[DAST](../../product/dast/),
[SCA](../../product/sca/)
and [CSPM](../../product/cspm/))
and AI to achieve comprehensive examinations that look
for weaknesses and potential issues accurately.
From there, our team and the tools report back
to your team from our platform,
which can be integrated into the VS Code IDE with [our extension](https://help.fluidattacks.com/portal/en/kb/articles/install-the-vs-code-extension)
to further streamline the vulnerability management process for your developers.

This proactive approach enables financial entities,
and any other industry,
to build a robust security posture against supply chain attacks.
Continuous [security testing](../../solutions/security-testing/) through
our solution is done from early stages of the SDLC,
which provides prompt information on vulnerabilities
and you can proactively decide how to proceed.
Let us help your company, [contact us now](../../contact-us/).
