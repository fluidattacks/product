---
slug: ddos-attacks-prevention-banking/
title: Site Crashed? Not Your Bank's!
date: 2024-07-09
subtitle: Develop bank applications that resist DDoS attacks
category: philosophy
tags: cybersecurity, web, trend
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1720557693/blog/ddos-attacks-prevention-banking/cover_banking_ddos.webp
alt: Photo by Sarah Kilian on Unsplash
description: Banks are getting most of the DDoS attacks among financial services firms, which are being targeted more than before. Learn to secure your app from this threat.
keywords: Ddos Attacks Against Financial Institutions, Ddos Mitigation Strategies, Ddos Prevention In Banking, Ddos Prevention Plan, Distributed Denial Of Service In Banking, Banking Application Security, Denial Of Service Financial Institution Cybersecurity, Ethical Hacking, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Content Writer and Editor
source: https://unsplash.com/photos/brown-ice-cream-cone-52jRtc2S_VE
---

## What's this trend of DDoS attacks against banks?

Distributed denial-of-service (DDoS) attacks are hitting the banking industry
hard.
This kind of attack involves the delivery of a high amount of requests or data
from different sources (e.g., systems) to a site, server or network
resulting in degradation of availability of the service
that depends on that site, server or network.
According to a [report](https://www.fsisac.com/hubfs/Knowledge/DDoS/FSISAC_DDoS-HereToStay.pdf)
released in March 2024,
DDoS attacks against financial services rose by 154% between 2022 and 2023.
The report further identified banks as the most impacted firms
within the financial services sector worldwide during 2023,
accounting for 63% of all DDoS attacks.

There are several factors that may be behind the surge in DDoS attacks.
For example,
recent spikes in hacktivism amid geopolitical tension,
which incites cyberattacks overall,
has meant more DDoS attempts,
on which firms are, by the way, keeping a more watchful eye lately.
Indeed,
as hacktivists (e.g., pro-Russian groups) have announced their intent
to hit financial organizations in the US and Europe,
these orgs have reported more attempts than in the past,
it seems, in part, due to their heightened vigilance.
Other factors are the rise of DDoS-for-hire services
(i.e., the possibility to rent the system network
used to overwhelm the target site, server or network),
which make conducting the attacks a less costly affair,
and the all-too-common search for financial gain.
An enabling factor for attack success is the lack of adequate DDoS protection.
We'll get to advice on this later.

One [example of a DDoS attack](https://www.bankinfosecurity.com/danish-banks-targets-pro-russian-ddos-hacking-group-a-20902)
in the sector
is that which some of Denmark's largest banks experienced in early 2023.
The pro-Russian hacktivist group NoName057(16) conducted the attack,
affecting the websites of nine target institutions.
Reports say the websites were down for a short period of time,
but still, the disruption caused operational delays in the organizations.

The rising threat of DDoS attacks should place your bank on high alert.
Service disruptions mean delays and customer dissatisfaction and distrust.
Moreover,
DDoS attacks, as argued in the above report, may be part of a ransomware attack
where threat actors try to coerce orgs to pay the ransom
by promising service disruption otherwise.
Stakes are high,
so you need to get ready for this type of attack.
Let’s first go into what it is.

## What is a DDoS attack?

A distributed denial-of-service attack is an attempt to overwhelm websites,
servers
or networks
with a flood of specific traffic,
like messages, connection requests or malformed packets.

An attacker can gain control of one device
by exploiting an unpatched vulnerability,
or through malware infection after successful [phishing](../phishing/) attacks
or even by [brute force](../pass-cracking/).
Afterwards,
the attacker builds kind of an army ("zombie army," some call it)
of compromised devices or bots
through an established communication channel of infected devices.
This is called a botnet.
To manage the botnet remotely,
the attacker uses a command-and-control, or C&C, server.
Once the latter is ready,
the attacker issues commands to the botnet to attack the target system,
creating a disruption
that can affect the target's bandwidth, processing power or memory.
The havoc produced in the operation of the target
prevents legitimate users from accessing it,
either at the desired speed or altogether.

The duration of service unavailability due to DDoS attacks varies quite a lot.
Indeed, historically, it ranges from a few minutes to days.
[The longest DDoS](https://securelist.com/ddos-report-q2-2019/91934/) ever
lasted an uncommonly 21 days.
This was in 2019.
[Recent data by Cloudflare](https://blog.cloudflare.com/ddos-threat-report-for-2024-q1/)
shows
that in early 2024 almost half of DDoS attacks lasted over 10 minutes,
while about one-third lasted more than one hour.
These two pieces of info are in relation to application-layer attacks.
This specific category refers to when the botnet floods applications
with a high volume of seemingly legitimate requests,
as if it were end users utilizing the applications' functionalities.

As DDoS attacks at the application level [have increased](https://www.fsisac.com/hubfs/Knowledge/DDoS/FSISAC_DDoS-HereToStay.pdf),
it's important for IT leaders in banks
to address the security of the bank applications
in relation to this type of attack.
Below, we give our recommended security requirements for software
which should make part of your DDoS mitigation strategies.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/security-testing/"
title="Get started with Fluid Attacks' Security Testing solution right now"
/>
</div>

## Prevent DDoS right from application development

The cybersecurity industry offers DDoS mitigation solutions.
These are tools and services for preventing,
detecting
and stopping DDoS attacks.
They are meant to make your application more resistant
without you needing to have protection built into the application itself.
An example is web application firewalls (WAFs),
which you deploy to inspect and block incoming traffic
based on predefined rulesets,
such as high request rates or anomalous patterns.

Yet, you should want your banking application to be strong itself
when DDoS mitigation solutions falter.
That is key for your DDoS prevention plan.
Our next recommendations are then directed at developing applications
secure against DDoS attacks by design.

- **Implement rate limiting:** Set up rate-limiting mechanisms
  to restrict the number of requests from a single source or IP address
  over a specified period.

- **Use request validation:** Validate incoming requests
  to ensure they conform to expected patterns,
  such as size, frequency, and content types.

- **Build allowlists and blocklists:** Set the IPs
  for which the rate limit does not apply
  and those that must be blocked
  (e.g., malicious IPs that have already been reported as such).

- **Introduce CAPTCHA or challenge-response mechanisms:**
  Require human verification for pages
  for which suspicious or potentially malicious requests can be expected.
  This can help differentiate between legitimate users and bots.

- **Enable horizontal scaling:** Design applications
  to be horizontally scalable,
  that is,
  able to distribute the workload across multiple machines or servers,
  thus managing user requests and transactions more effectively,
  reducing the risk of downtime
  and avoiding performance bottlenecks.
  This scalability is crucial to support a growing user base,
  high transaction volumes
  and fast services.

- **Use load balancing:** Deploy applications behind load balancers
  that can distribute incoming traffic evenly across multiple servers.
  This helps absorb and distribute the impact of DDoS attacks.

- **Have distributed architecture and redundancy:** Design applications
  with distributed architectures and redundant components
  that can withstand localized failures
  and mitigate the impact of DDoS attacks
  by maintaining service availability from alternative locations or servers.

At Fluid Attacks, we help you find and remediate the security vulnerabilities
in your banking applications.
Start the [21-day free trial](https://app.fluidattacks.com/SignUp)
of Continuous Hacking to enjoy automated security testing
that can detect noncompliance with DDoS-related security requirements,
such as [setting a rate limit](https://help.fluidattacks.com/portal/en/kb/articles/criteria-requirements-327).
You can enjoy using our [platform](../../platform/)
to manage the detected vulnerabilities
and [syncing it with VS Code](https://help.fluidattacks.com/portal/en/kb/integrate-with-fluid-attacks/use-ide-extensions/vs-code-extension)
to use the AI-generated fixes options that we offer in the latter.
What's more,
for enhanced security against DDoS attacks,
you can get our [Advanced](../../plans/) paid plan,
which adds vulnerability analysis by our pentesters.
This team will not only find vulnerabilities that tools will not detect,
but also give expert advice to your development team
on how to eliminate them.
Sounds better?
[Contact us](../../contact-us/) to learn more.
