---
slug: cybersecurity-trends-2023/
title: Cybersecurity Trends 2023
date: 2023-11-09
subtitle: Top cyberattack and prevention trends this year
category: opinions
tags: cybersecurity, trend, company
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1699556415/blog/cybersecurity-trends-2023/cover_cybersecurity_trends_2023.webp
alt: Photo by Gary Bendig on Unsplash
description: Fluid Attacks shares a digest of top trends in cyberattacks and prevention in 2023.
keywords: Artificial Intelligence, Cyberwar, Passwordless, Software Supply Chain Security, Software Supply Chain Attacks, Vulnerabilities, Trends, Ethical Hacking, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Content Writer and Editor
source: https://unsplash.com/photos/gray-and-black-mallard-ducks-flying-during-day-time-WPmPsdX2ySw
---

Ever committed to helping companies develop and deploy secure software,
we share a selection
of what we consider to be the most significant cyberattack
and prevention trends in 2023.

As in previous years,
there were attacks based on exploiting vulnerabilities in software products.
Moreover,
the expression in cyberspace of wars between nations continued.
On the other hand,
artificial intelligence (AI) showed important progress this year,
and cybercriminals took that to their advantage to enhance their attacks.

These challenges for cybersecurity have led to protection trends
throughout the development cycle of technological products.
Such include the use of AI for good
and enhancements in authentication mechanisms.

## Cyberattack trends in 2023

<br />

### Cyberattacks using artificial intelligence

AI was on everyone’s lips this year.
Useful and fascinating as it is,
it has been a cause of concern.
A vivid example is what has happened
with the wildly popular chatbot ChatGPT.
Having reached 100 million monthly active users
just two months after its launch,
it made history as the [fastest-growing consumer application in history](https://www.reuters.com/technology/chatgpt-sets-record-fastest-growing-user-base-analyst-note-2023-02-01/)
before [Meta’s Threads launch](https://www.reuters.com/technology/chatgpt-traffic-slips-again-third-month-row-2023-09-07/).
(The number of ChatGPT users is now on decline, though.)

ChatGPT’s generative AI has been abused to [write malicious code](https://iti.illinois.edu/news/chatgpt-malware).
Some malware criminals write with it can even [evade endpoint detection and response](https://www.csoonline.com/article/575487/chatgpt-creates-mutating-malware-that-evades-detection-by-edr.html)
(EDR) applications.
Remarkably,
AI has also helped criminals step up their [phishing](../phishing/) game.
Namely,
it helps them create more convincing messages
to lure victims to downloading and executing malware.

Early this year, [ChatGPT had a data breach](https://securityintelligence.com/articles/chatgpt-confirms-data-breach/)
due to a vulnerability in an open-source library.
The breach “allowed users to see the chat history of other active users.”
Further,
criminals have [stolen over 100,000 accounts](https://www.bleepingcomputer.com/news/security/over-100-000-chatgpt-accounts-stolen-via-info-stealing-malware/)
on this app using malware.
And the app has suffered recent [outages caused by DDoS attacks](https://www.bleepingcomputer.com/news/security/openai-confirms-ddos-attacks-behind-ongoing-chatgpt-outages).

Below we will see
that the cybersecurity industry has also taken advantage
of the latest trends in AI.

### A new cyberwar

The war between Israel and Hamas,
like that between [Russia and Ukraine](../timeline-new-cyberwar/),
has included cyberattacks by hacktivists.
Some attacks intended to deface websites or make them crash,
with the government and militia sectors as the main target.
For example,
pro-Islamic groups have [hit the Israeli Parliament with DDoS attacks](https://blog.checkpoint.com/security/the-iron-swords-war-cyber-perspectives-from-the-first-10-days-of-the-war-in-israel/).
And pro-Israel groups have [attacked websites of the Gaza government](https://www.securityweek.com/hackers-join-in-on-israel-hamas-war-with-disruptive-cyberattacks/).

Other attacks sought to impact nations supporting either side of the conflict.
Case in point,
an increase in cyberattacks [against some supporters of Israel](https://blog.checkpoint.com/security/evolving-cyber-dynamics-amidst-the-israel-hamas-conflict/),
i.e.,
the U.S., France, India and Italy,
has been linked to the activities of some groups associated with Russia,
Bangladesh,
Iran or other nations.
And unidentified groups have [targeted international organizations](https://www.reuters.com/world/middle-east/hackers-hit-aid-groups-responding-israel-gaza-crisis-2023-10-13/)
that provide humanitarian aid to both Palestinians and Israelis.

### Software supply chain attacks

Cybercriminal groups have managed
to cause large monetary and reputational losses to organizations over the years
when they infect third-party software products
used by the firms or on which the firms’ own products depend.

The largest global attack campaign of this type this year
has been the one carried out by the CL0P ransomware gang
exploiting a flaw in a file transfer tool called MOVEit.
To date,
they have affected more than [2,500 organizations](https://www.emsisoft.com/en/blog/44123/unpacking-the-moveit-breach-statistics-and-analysis/),
compromising the data of nearly 70 million people.

Companies using insecure third-party software are exposed
to consequences of the most costly kind.
This year,
a data leak due to a software supply chain attack cost
an average of [**$4.63M** globally](https://www.ibm.com/reports/data-breach).
This cost surpasses that of attacks due to all other causes,
which averaged $4.26M.

Use of software dependencies with known vulnerabilities
remains a widespread issue.
Our [State of Attacks 2023](https://fluidattacks.docsend.com/view/e988pvaiuew3nikq)
report
shows that about **83%** of the systems we tested this year
used flawed software components.
This type of vulnerability caused the most risk exposure (over 25%) to systems
when aggregated.

### Nation-state threats

According to the [Microsoft Digital Defense Report](https://www.microsoft.com/en-us/security/security-insider/microsoft-digital-defense-report-2023-nation-state-threats),
several groups operating globally,
linked to Russia, China, Iran and North Korea,
carried out attacks
that ranged from spreading false information
to spying or stealing cryptocurrencies.

The government sector was the second most affected,
behind academia.
Notably,
this year there was a diversification in the nations attacked,
especially evidenced by how Iranian groups expanded their attacks
to South East Asia,
Eastern and Southern Europe,
Africa
and Latin America.

In addition to the government sector,
a wide variety of industries have been impacted.
This possibly represents large losses for many organizations,
given that,
for example,
the average cost of a data breach this year was a record [**$4.45M**](https://www.ibm.com/reports/data-breach).

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/security-testing/"
title="Get started with Fluid Attacks' Security Testing solution right now"
/>
</div>

## Cyberattack prevention trends in 2023

<br />

### Artificial intelligence for cybersecurity

As we said above,
AI is also used in favor of protecting information systems.
Researchers over the world (e.g., [Microsoft’s](https://blogs.microsoft.com/on-the-issues/2023/11/02/secure-future-initiative-sfi-cybersecurity-cyberattacks/))
began to use it to analyze vast amounts of data from Internet connections
to more quickly detect and analyze cyberattacks
on organizations anywhere around the world.
At the individual organization level,
some solutions using AI are supporting cybersecurity teams,
which are generally understaffed,
to detect potential breaches in their systems.

In terms of AI’s support to the preventive posture in cybersecurity,
that is,
security applied to systems from the beginning of
and during the entire development lifecycle,
we at Fluid Attacks have contributed to the advancements.
We leverage AI
to create models that inform pentesters
which files of the applications they are evaluating are most likely
to have vulnerabilities.
This way,
they can prioritize those files in their search.
In addition,
we released this year a feature for our [IDE extension](https://help.fluidattacks.com/portal/en/kb/integrate-with-fluid-attacks/use-ide-extensions/vs-code-extension)
to generate step-by-step guides on VS Code
with fixes relevant to the detected vulnerabilities.
This helps devs remediate software flaws more easily and effectively.
Other solutions are using AI
to help eliminate vulnerabilities
by automatically presenting suggested code modifications
for devs to simply accept or reject.

### Software supply chain security

There is a constant threat
of attacks taking advantage of flawed open-source components.
That is what makes it so necessary
to follow the recent trend to secure the software supply chains
as comprehensively as possible.

The approach encompasses not only having an up-to-date list
of those components or products in use
(i.e., a software bill of materials)
and their security status,
but also verifying their provenance
and assessing the suppliers' security policies
and their compliance with industry standards.

We have talked more extensively about [software supply chain security (SSCS)](../software-supply-chain-security/)
in a blog post.
Read it,
as we give you a checklist of some main aspects
you should take into account in SSCS
during the different phases of the software development lifecycle.

### The shift to passwordless

Passkeys are a standard
by Google,
Apple,
Microsoft,
World Wide Web Consortium
and FIDO Alliance.
It consists of a PIN or pattern or biometric factor,
such as face or fingerprint,
to access accounts in various applications.
This alternative has been replacing passwords.

One advantage is their speed.
Google has found that passkeys allow users to [authenticate in half the time](https://security.googleblog.com/2023/05/making-authentication-faster-than-ever.html)
it takes with passwords.
In addition,
passkeys are more secure,
because they are not processed by servers, e.g. Google,
but are stored only on the device.
By giving the right passkey,
it generates a unique digital signature
to confirm the access rights to the application
that is requiring authentication.
And as it is used only in authorized applications,
this new method prevents credentials from being shared on fraudulent sites.

## Secure your applications against cyberattack trends with Fluid Attacks

It is a great mistake
to be unaware of the security vulnerabilities,
and the risk exposure they cause,
in proprietary as well as third-party code.
You need to follow a preventive approach to cybersecurity.
Find out how secure your application and your software supply chains are
and fix all found issues
before the bad guys place eyes on them.
Begin your
[21-day free trial](https://app.fluidattacks.com/SignUp)
now and let us help you develop and deploy secure applications.
