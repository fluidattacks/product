---
slug: in-app-protection-is-not-enough/
title: In-App Protection Is Not Enough
date: 2024-05-21
subtitle: If the essential security layer is flawed, you're toast
category: philosophy
tags: cybersecurity, trend
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1716330108/blog/in-app-protection-is-not-enough/cover_in_app_protection.webp
alt: Photo by Jasmin Egger on Unsplash
description: You must not stop remediating vulnerabilities in your mobile apps just because you fully trust technologies such as RASP or anti-reverse engineering solutions.
keywords: In App Protection, Essential Security, Advanced Security, Mobile Security, Android App Protection, App Security, Security Testing, Ethical Hacking, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Content Writer and Editor
source: https://unsplash.com/photos/white-egg-on-brown-wooden-tray-vWtfT-o-UOA
---

Allow us to start this blog post with a simple petition.
Imagine you bought some stuff, including a box of donuts,
and brought them for a retreat in your country house.
You hurry to store most of the stuff,
as you must take off asap to pick up some of your guests.
Since there is no space left in the fridge,
and you have no time to go and look for the zip bags in your car,
you decide to place the box of donuts inside a plain plastic bag
and leave them on the kitchen counter.
"That should do," you think.
Hours later, you're back with your guests and,
as you look in the kitchen for something to offer them,
you see a signal.
An ant, several, lots.
They are all surrounding the bag with the donuts.
Had you inspected the bag beforehand,
You could have seen a little hole.

"I would have used two bags to protect them better!" a reader might argue.
Possibly.
But,
as anyone who's had an ant problem in their kitchen would attest to,
ants always find their way to where they're not wanted,
if given enough time.
Let's get some things clear, though.
We're not supposing that the zip bag is infallible.
There might be other antagonists at your country house with more strength
to power through piercing the different layers securing the donuts.
Our point is
that combining these solutions might have resulted
in a more successful prevention
than what we saw in this hypothetical.

Doesn't all of the above remind us of the posture
that some have adopted towards cybersecurity?
Adding layer upon layer –each imperfect– as defense,
without turning to the most appropriate solution,
and rather confident that that should be enough.
Is this the case with you and your team?

## Security through obscurity

Within the sheer amount of offers in the cybersecurity market,
there are solutions that integrate controls to protect apps as the latter run.
This is the case of a category that Gartner calls "in-app protection."

In-app protection is the type of solution
that introduces functions in its clients' software products
to make them more resistant to cyberattacks.
Some of those defenses, offered directly or from third parties,
are the following:

- **Runtime application self-protection (RASP):**
  It is a function that monitors the app (e.g., its inputs and outputs)
  and notifies the user or closes the app if it finds an insecure pattern.

- **[Man-in-the-middle](../../learn/what-is-man-in-the-middle-attack/)
  (MITM) attacks prevention:**
  It is a set of functions
  like detection of malicious proxies,
  certificate pinning (see below),
  URL allowlisting/whitelisting, etc.,
  that work to prevent hackers from intercepting communications
  between two parties.

- **Detection of jailbroken or rooted devices:**
  It is a function that notifies or prevents the execution of the app
  in devices where there's access to the root user.

- **Secure certificate pinning:**
  This is a function that forces the use of matching SSL
  for communications between the app and a server.
  This is to avoid hackers detour communications
  to a malicious server with fake certificates.

- **Code obfuscation:**
  It is a function that makes source code hard for a hacker to interpret
  or for an [static code analysis](../sastisfying-app-security/) (SAST)
  tool to scan in search of vulnerabilities.

- **Anti-[reverse engineering](../reverse-engineering/) techniques:**
  It is a function to block the tools hackers use
  for identifying software components,
  functions
  and relationships,
  and ultimately, the product's business logic,
  without initial access to source code.

The above are **additional** security measures for apps.
They are recommended by some,
e.g., the [OWASP Mobile App Security](https://mas.owasp.org/) (MAS) project,
for apps whose owners are organizations with an urgent need
to safeguard their assets and business logic.
However some solutions in the market promise
to alleviate the dev team's workload,
to enable speedy deployments into production,
and, something quite attractive,
to protect apps with no need to write code.
Such offerings may be interpreted by clients
as enough measures to keep apps secure,
as if that was a way to remediate their vulnerabilities.

We need you and your team to learn and understand this mantra:
"Enabling security controls is not remediating".
Precisely,
the security features we mentioned in the previous lines are defenses
that help increase the complexity to exploit a vulnerability
or understand the code.
[Remediation](../vulnerability-remediation-process/), on the other hand,
is the elimination of a characteristic
of a software product's security posture,
configuration
or design
that renders it open to exploitation.

Maybe some of the teams that delegate security wholly to in-app protection
don't need the above explanation.
They are satisfied with the armor they procured for their apps
and don't see remediation as necessary,
while perceiving it as extremely expensive.
But hiding software vulnerabilities behind layers of defenses
without any intention to fix them is an unfortunate decision.

## You are just buying time

Fluid Attacks' pentesters have experience bypassing controls
including the ones mentioned in this post.
That is not necessarily due to RASP misconfigurations,
or insufficient obfuscation,
but rather to hackers being able to always bypass these controls,
if given enough time.
Behind these controls hackers find back-ends
with vulnerabilities ready to be exploited.
Another possibility is they can obtain previous software versions
with no in-app protection
and launch their attacks against them.

Excessive confidence in security controls
may eventually allow for security breaches
which can mean losses of information, money and reputation.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/security-testing/"
title="Get started with Fluid Attacks' Security Testing solution right now"
/>
</div>

## Don't comply only with resilient security

We stress on this:
In-app protection defenses are additional measures.
They do not replace security requirements
your app's source code and configurations need to comply with.
That's the same OWASP MAS says.
The latter project recognizes the importance of security controls
as [**resilient security**](https://mas.owasp.org/MASVS/Intro/03-Using_the_MASVS/#mas-testing-profiles)
and calls them an extra layer that reinforces the other two:
an **essential security** layer and an **advanced security** layer.

Essential security requires your mobile app to use preexisting,
tested
and secure cryptographic mechanisms,
protect network traffic,
use inter-process communication mechanisms securely,
have no vulnerable third-party dependencies,
among other requirements.
All this must be verified continuously
and fixed when an issue of noncompliance is detected.
It matters not how good you think your resilient security is.
We list the [risks to mobile apps](../what-is-mast/) elsewhere
along with the role that security testing plays in identifying them.

Regarding advanced security,
compliance with the corresponding set of requirements are strongly advised
if your app handles high-risk sensitive data.
Some security measures in this layer include
usage of secure authentication mechanisms,
identity pinning,
and secure update mechanisms.

## Remediating is safer and faster each time

We have defined what you should recognize as additional and what is essential.
We know that it's the security of your app's code and configurations
what ultimately stands between a malicious threat actor
and the integrity, confidentiality and availability of your business'
and your end users' information.
Undoubtedly,
an important chunk of your team's efforts needs to focus
on identifying and fixing security flaws
while resilient security measures buy you some time.

Not only is it safer to remediate vulnerabilities,
but it is a task that tends to gain velocity.
When your development team takes responsibility for the app's security
they can acquire knowledge about vulnerabilities and,
through timely feedback, they can [learn to write secure code](../secure-coding-five-steps/).
This translates into fewer instances of at least one type of vulnerability
and less rework and time to remediate.
In fact,
our latest report, [State of Attacks 2023](https://fluidattacks.docsend.com/view/e988pvaiuew3nikq),
we found that,
if a team breaks the build,
preventing vulnerable code from being deployed into production,
they take less time to remediate detected vulnerabilities
than when they do not break the build.

## Fluid Attacks tests your app's security thoroughly and helps fix flaws

As mentioned,
Fluid Attacks has a team of pentesters.
It's a [highly qualified](../../certifications/) team
that evaluates the security of software products continuously
alongside [AppSec testing tools](../../solutions/security-testing/)
developed by Fluid Attacks.
Such a combination results in speed and precision.
Thanks to Fluid Attacks,
you can learn about vulnerabilities in your essential,
advanced
and resilient security,
as well as obtain remediation recommendations from an expert team.

Find vulnerabilities in your app
before a cybercriminal does.
[Contact us](../../contact-us/).
