---
slug: pci-dss-best-practices-2024/
title: PCI DSS Best Practices in 2024
date: 2024-01-26
subtitle: Understanding 51 new PCI DSS requirements made easy
category: opinions
tags: cybersecurity, compliance, company
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1706281168/blog/pci-dss-best-practices-2024/cover_pci_dss_best_practices.webp
alt: Photo by Eduardo Balderas on Unsplash
description: PCI DSS v4.0 brings 51 new requirements that are best practices until March 2025. We share a classification that may help take it all in.
keywords: Pci Dss, Best Practices, Security Controls, New Requirements, Pci Dss Compliance, Vulnerability Scans, Service Providers, Ethical Hacking, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Content Writer and Editor
source: https://unsplash.com/photos/selective-focus-photography-of-person-holding-baseball-bat-UVxd5b-_tw8
---

[PCI DSS v4.0](https://www.pcisecuritystandards.org/document_library/)
brings new requirements to companies that handle account data.
Assessors will consider a minority (13) of those new requirements
in PCI DSS assessments starting this year.
(We summarize them [here](../pci-dss-v4-new-13-requirements/).)
Throughout the year,
the remaining **51** requirements are considered best practices
and will not affect the results of the assessments.
That will be the case until **March 31, 2025**.

Since working one's way through 51 requirements may be overwhelming,
we would like to share them organized into some categories
that, we think, would help to inspect them more easily:

- Deploying automated tests to protect against current threats

- Managing the attack surface and security risks comprehensively
  and preventively

- Defining the frequency of assessments

- Protecting sensitive authentication data (SAD)
  and the primary account number (PAN)

- Implementing appropriate cryptographic mechanisms

- Implementing stronger authentication mechanisms

- Reviewing and enhancing access control in user accounts
  and application and system accounts

- Having stricter incident response programs

- Having updated and more complete security awareness programs

In turn,
we deem these categories as part of three broad areas:
security testing and risk management,
secure development,
and incident response and security awareness programs.

<caution-box>

**Disclaimer:** We list the requirements using a different wording
to the one used in the PCI DSS v4.0 [document](https://www.pcisecuritystandards.org/document_library/).
This has allowed us to summarize those requirements
that are made up of several bullets.
We advise the reader to base their compliance on the PCI DSS v4.0 document
and regard this blog post only as a third party's classification
of the new requirements.

</caution-box>

## The 51 new best practices

<br />

### New about security testing and risk management

Let’s kick things off with the new directives about the detection of threats.
The additions take into account evolving trends.
These include having a secure software supply chain,
threat actors moving onto systems
using malware delivered through removable media,
and the necessity of automation.

The following are our simplified versions of new items
that, in summary,
ask for **deploying automated tests to protect against current threats**:

- Maintain an inventory of custom software and third-party software components
  to facilitate vulnerability and patch management. (6.3.2)

- Perform automated antimalware scans
  when removable electronic media is inserted,
  connected
  or logically mounted. (5.3.3)

- Implement processes and automated mechanisms
  to detect phishing attacks and defend against them. (5.4.1)

- Deploy an automated technical solution
  that continually detects and prevents web-based attacks. (6.4.2)

- Use automated mechanisms to perform audit log review. (10.4.1.1)

- Deploy a mechanism to detect unauthorized changes on HTTP headers
  and contents of payment pages at least once every seven days
  or when required per the targeted analysis. (11.6.1)

- Use intrusion-detection and or intrusion-prevention techniques
  to detect, alert on/prevent and address covert communications
  with command-and-control systems. (11.5.1.1)
  (This one is directed at service providers only.)

From these,
the call to address [phishing](../phishing/) has got to be
one of the most interesting additions.
The standard warns against confusing this security control
with mere awareness training.
Its advice is to deploy antispoofing controls
as well as link scrubbers and server-side antimalware
to block phishing emails and malware.

Regarding requirement 6.3.2, it should come as no surprise,
as the last couple of years has seen an increasing awareness
of the importance of generating a software bill of materials ([SBOM](../../learn/what-is-software-bill-of-materials/)).
We link this requirement to deploying automated tests
as the advice is to use [software composition analysis tools](../../product/sca/),
among others,
to inventory the third-party dependencies
that make up the product one intends to secure
along with details such as security status.

We would like to describe other requirements
as referring to **managing the attack surface and security risks
comprehensively and preventively**:

- Perform vulnerability scans enabled by credentials and sufficient privileges,
  and document the systems where authenticated scanning is not possible.
  (11.3.1.2)

- Manage vulnerabilities that are not considered as high-risk or critical
  per the company's targeted risk analysis. (11.3.1.1)

- Support customer requests to conduct penetration testing
  or for penetration testing results. (11.4.7)
  (This one is directed at service providers.)

- Review the effectiveness of logical separation via penetration testing
  at least once every six months. (A1.1.4)
  (This one is directed at service providers.)

- Support decisions on how frequently requirements must be performed
  with targeted risk analyses reviewed at least once every 12 months. (12.3.1)

- Review hardware and software technologies in use
  at least once every 12 months
  to define whether they meet the security needs
  and are still supported by the vendor,
  and document the course of action to remediate outdated technologies.
  (12.3.4)

- Document and confirm the scope of the PCI DSS review
  at least once every six months and upon significant changes. (12.5.2.1)

- Document and communicate to management
  a review of the impact to PCI DSS scope and applicability of controls
  upon significant changes to organizational structure
  (e.g., company mergers or acquisitions). (12.5.3)

Remarkably,
PCI DSS v4.0 further strengthens its requirements for service providers
in regard to [penetration testing](../penetration-testing/).
Moreover,
this version may support a better [vulnerability remediation](../vulnerability-remediation-process/)
culture,
as it promotes that all vulnerabilities be addressed.
Also,
it's good to see the standard's new requirement of authenticated [scanning](../vulnerability-scan/),
which may make for more thorough security assessments.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/penetration-testing-as-a-service/"
title="Get started with Fluid Attacks' Penetration Testing as a Service
right now"
/>
</div>

And we observed
that other set of new requirements revolve around **defining the frequency
of assessments**:

- Define the frequency of periodic malware scans. (5.3.2.1)

- Define the frequency of evaluations of system components
  identified as not at risk for malware. (5.2.3.1)

- Perform a targeted risk analysis to define the frequency of log reviews
  for system components for which a daily log review is not mandatory.
  (10.4.2.1)

- Perform a targeted risk analysis
  to define the frequency of periodic point-of-interaction device inspections
  and the type of inspections. (9.5.1.2.1)

### New about secure development

We also spotted requirements that could be classified
as development best practices
for **protecting sensitive authentication data (SAD)
and the primary account number (PAN)**:

- Keep to a minimum the storage of SAD prior to completion of authorization.
  (3.2.1)

- Prevent PAN from being copied or relocated by unauthorized personnel
  using remote-access technologies (e.g., virtual desktops). (3.4.2)

- Confirm that certificates used to safeguard PAN
  during transmission over open, public networks are valid
  and not expired or revoked. (4.2.1)

- Maintain an inventory of trusted keys and certificates
  used to protect PAN during transmission. (4.2.1.1)

- Document all payment page scripts
  that are loaded and executed in the consumer's browser
  with their justification
  and implement methods to confirm each script is authorized
  and assure each script's integrity. (6.4.3)

V4.0 makes a call for entities to step up their encryption game
to protect account data.
The advice is to use hashing algorithms such as HMAC, CMAC and GMAC.
The requirements that talk about
**implementing appropriate cryptographic mechanisms**
are the following:

- Document that the use of the same cryptographic keys
  in production and test environments is prevented. (3.6.1.1)
  (This is directed at service providers.)

- Use strong cryptography to encrypt SAD
  that is stored prior to completion of authorization. (3.3.2)

- Use strong cryptography to encrypt SAD. (3.3.3)
  (This one is directed at issuers.)

- Use keyed cryptographic hashes of the entire PAN
  to render PAN unreadable. (3.5.1.1)

- Use disk-level and partition-level encryption on removable electronic media
  (e.g., bulk tape-backups) to render PAN unreadable. (3.5.1.2)

- Inventory cryptographic cipher suites and protocols in use
  at least once every 12 months,
  review their continued viability and devise a course of action
  for when they become deprecated. (12.3.3)

Another cluster could be summarized
as **implementing stronger authentication mechanisms**,
which is quite clear in the indication of making passwords longer
and requiring success of all factors of MFA.
The requirements are the following:

- If authentication for user accounts is through passwords or passphrases,
  they must be at least 12 characters long
  (if the system cannot support this, then the minimum is 8 characters)
  and have both numeric and alphabetic characters. (8.3.6)

- Rotate passwords/passphrases for application and system accounts periodically
  and have them be complex. (8.6.3)

- Require passwords/passphrases be changed at least once every 90 days
  or analyze in real time the security posture of user accounts. (8.3.10.1)
  (This one is directed at service providers.)

- Implement MFA for all access into the cardholder data environment.
  (8.4.2) (Does not apply to user accounts on point-of-sale terminals.)

- Require success of all authentication factors to grant access,
  have them be at least two factors
  and use controls so they cannot be bypassed
  nor are susceptible to replay attacks. (8.5.1)

Yet another bundle of new requirements can be linked
to **reviewing and enhancing access control
in user accounts and application and system accounts**.
A couple of requirements that stand out a lot have their focus
on preventing and restricting interactive login
(i.e., logging into system or application accounts as one would user accounts).
Also,
we see a great improvement with the requirements to review access privileges.

- Limit access of application and system accounts
  to only the systems, applications or processes necessary
  and apply the principle of least privilege. (7.2.5)

- Review periodically application and system accounts
  and related access privileges. (7.2.5.1)

- Prevent interactive login and document when it is permitted,
  requiring explicit approval by management,
  usage only for the time necessary,
  confirming user identity before access
  and allowing attribution to this individual user. (8.6.1)

- Prevent having passwords or passphrases
  for any application and system account
  that can be used for interactive login hard coded in scripts,
  configuration/property files
  or source code. (8.6.2)

- Review at least once every six months all user accounts
  and related access privileges. (7.2.4)

- Implement logical separation
  so that, unless authorized,
  the provider cannot access its customers' environments and vice versa.
  (A1.1.1) (This one is directed at service providers.)

### New for incident response and security awareness programs

We relate a bundle of new items
to **having stricter incident response programs**:

- Detect, alert and promptly address failures
  of critical security control systems. (10.7.2)

- Respond fast, restore security functions and implement controls
  to prevent reoccurrence. (10.7.3)

- Detect, alert and promptly address failures
  of automated audit log review mechanisms and code review tools.
  (A3.3.1) (Directed at entities required by payment brands or acquirers
  to undergo further assessments
  due to, e.g., a history of repeated breaches of account data.)

- Have procedures of incident response
  for when PAN is stored where it is not expected,
  including finding out where it came from
  and how it got where it is. (12.10.7)

- Include
  that alerts from a change and tamper-detection mechanism for payment pages
  are monitored and responded to. (12.10.5)

- Implement secure methods
  for customers to report security incidents and vulnerabilities,
  and address each report. (A1.2.3)
  (This one is directed at service providers.)

- Perform a targeted risk analysis
  to define the frequency of training for incident response personnel.
  (12.10.4.1)

Promoting the enhancement of [vulnerability handling processes](../iso-iec-30111/)
is a notable addition in this version.
Also,
it's a good progress
that it's now all entities, not only service providers,
who must manage failures of their critical security control systems
(e.g., network security controls, IDS/IPS, logical access controls).

Lastly, we would like to refer to the new requirements
that we link
to **having updated and more complete security awareness programs**.
Among them,
we see again the emphasis on preventing successful phishing attacks.

- Include content about the acceptable use of end-user technologies. (12.6.3.2)

- Include phishing and other social engineering-related attacks. (12.6.3.1)

- Review the program at least once every 12 months
  to keep it up-to-date on any new threats or vulnerabilities. (12.6.2)

## We contribute to your software's PCI DSS compliance

Fluid Attacks tests whether your software follows [requirements of PCI DSS](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-pci).
Start using the [all-in-one solution](../../services/continuous-hacking/)
that performs continuous [vulnerability scanning](../vulnerability-scan/)
and [pentesting](../penetration-testing/)
and provides remediation advice
from an [experienced team of pentesters](../../certifications/)
and [through gen AI](https://help.fluidattacks.com/portal/en/kb/articles/fix-code-automatically-with-gen-ai).

For a free trial of vulnerability scanning with Fluid Attacks' automated tool,
click [here](https://app.fluidattacks.com/SignUp).
