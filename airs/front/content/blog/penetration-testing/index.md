---
slug: penetration-testing/
title: 'Penetration Testing: A Guide'
date: 2023-12-19
subtitle: Importance, types, steps, tools of pentesting, and more
category: philosophy
tags: cybersecurity, pentesting, security-testing, hacking, software, code
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1702996448/blog/penetration-testing/cover_pentesting.webp
alt: Photo by Peter Neumann on Unsplash
description: Explore the world of penetration testing and gain understanding of how it bolsters your cybersecurity safeguards against emerging threats.
keywords: Penetration Testing, Pen Testing, Pen Tests, Security Testing, Automated Testing, Tools, Hackers, Ethical Hacking, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Content Writer and Editor
source: https://unsplash.com/photos/white-polar-bear-swimming-JZRlnfsdcj0
---

## Pen testing or penetration testing or manual pen testing

[Penetration testing](../what-is-manual-penetration-testing/)
is an evaluation technique
in which [ethical hackers](../../solutions/ethical-hacking/)
simulate real-world cyberattacks on a system
with its owner's permission to determine its security.

This blog post will provide you with the basics about penetration testing,
along with helpful links to other posts
where we delve deeper into each subject.

## Who performs penetration testing?

Penetration testing is performed by highly skilled individuals
who have experience searching for,
identifying,
exploiting
and reporting vulnerabilities in information systems.
Although the activity of pen testing can be done for good or bad,
the term is universally used
to refer to the legal and well-intentioned practice.
The expert carrying out the tasks in pentesting
aimed ultimately to secure IT systems
can be called “pen tester,”
“[ethical hacker](../hacking-ethics/)”
or “security analyst,”
among other names.
We will use them interchangeably in this post.

Pen testers may or may not have formal education and training.
They may have earned a diploma in computer science or engineering,
or may have completed a study program on cybersecurity.
They may have earned [certifications](../../certifications/)
such as OffSec Certified Professional (OSCP)
or even Offensive Security Exploitation Expert (OSEE).
Or they may not.
The one condition to be a pen tester is effectively proving
to have cybersecurity knowledge by using it creatively
to bypass security defenses.

## How does pen testing differ from automated testing?

While pen testing is done by humans,
automated testing is the use of tools to find out details
on the security status of an IT system
and help to enhance it.
Both kinds of testing are useful,
and we advise to use them at the same time to get the best results,
leveraging the velocity of automation and the accuracy of manual reviews.
Here is a brief comparison between pen testing and automated testing:

- **Types of vulnerabilities:** Pen testing is more fitted
  to find business logic flaws,
  issues with data handling (e.g., those allowing cross-site scripting (XSS)
  and [SQL injection](../sql-injection/) attacks)
  and access control vulnerabilities.
  Automated testing finds known vulnerabilities
  that often have a milder impact if exploited.

- **Accuracy:** Tools report high rates of false positives
  (i.e., they often alert erroneously of vulnerabilities)
  and show high rates of false negatives
  (i.e., they often fail to detect vulnerabilities).
  Contrarily,
  hackers verify that they report real issues
  and, thanks to their skill, can find vulnerabilities that tools cannot.

- **Exploitation:** Hackers, unlike tools, create custom exploits
  (e.g., code strings that allow taking advantage of a vulnerability)
  to find out more accurately what impact the detected issues may have
  on information availability, confidentiality and integrity.
  What is more,
  hackers combine vulnerabilities
  to find out how an adversary could achieve the greatest impact
  on the IT system.

- **Time to start getting results:** Vulnerability scans are faster
  than manual reviews,
  which is why generally a tool will start reporting vulnerabilities
  before a hacker does.

- **Remediation recommendations:**
  The advice of hackers may often be more detailed and effective
  than that given by tools.

By the way, pen testing is also known as “manual pen testing” (MPT).
Indeed, [we have published](../what-is-manual-penetration-testing/) our stance
on "automated penetration testing" (APT),
and it is that it does not really exist yet:
MPT is the only kind of penetration testing,
as only humans can carry out and achieve what is expected
with this security testing technique.

## Difference between vulnerability assessment and penetration testing

While [vulnerability assessment](../vulnerability-assessment/) is the name
given to the systematic evaluation of IT system security
and thus responds to the "what,"
pen testing refers to a specific evaluation methodology
and thus responds to the "how."
There is no use in trying to compare them.
Instead,
comparisons like the one in the above section,
between two vulnerability assessment methodologies,
can be useful.
Again,
we recommend using both penetration testing and automated testing,
as this will help achieve comprehensive vulnerability assessments.

## Why is penetration testing important?

Every organization that has Internet-facing assets
is automatically and constantly exposed to the attacks of cybercriminals.
The activity of malicious hackers,
specially of advanced persistent threats and ransomware gangs,
is growing so strong
that every year the previous cybercrime cost record is broken.
[Throughout 2024](../trend-forecast-for-2024/),
this cost will likely be 302,000 USD per second.
Aware of the risks,
organizations worldwide are implementing [security testing](../security-testing-fundamentals/).
Some of them go merely for tools to test their technology.
And tools are alright but not enough.
Even [having more than 50 tools](../impacts-of-false-positives/)
does not guarantee those organizations
they will feel better prepared to detect and respond to attacks.
What they need is to see the security of their technology
through the eyes of the attacker.

Penetration testing is important
because it subjects technology to realistic attack simulations.
The ethical hackers performing it are up to date
with the changing threat landscape
and are just as capable as malicious hackers
of finding business-critical vulnerabilities,
as they use the same techniques
and know the way of thinking of cybercriminals.
Organizations having pen tests performed in their systems
can make them less vulnerable to attacks.
We see this frequently as our annual reports repeatedly show
that manual reviews discover most of our clients' systems' risk exposure.
For example, in our [State of Attacks 2023](https://fluidattacks.docsend.com/view/e988pvaiuew3nikq),
we present
that it was ethical hackers who reported
**72% of the critical vulnerabilities**
in the systems assessed by them and tools.
Therefore,
pentesting contributes significantly
to organizations having a more realistic view
of their technology cybersecurity.

## How much access is given to pen testers?

As we will see in the next section,
pen testers first come to agreements with their clients
on the terms of the tests.
One factor is the information initially available to hackers.
The different decisions on this regard have been classified
into three categories widely known in the industry.
These categories are even sometimes referred to as types of pentesting.
However, we will not use them in this latter sense.

### Opaque box or black box pentesting

This term is used to refer to the situation
in which the organization does not share with the hacker
the source code, structure and internal workings of the software product
to be attacked.
The security analyst starts their approach, then, as many threat actors might.
They would have to use their skills to gain the basic information they need,
and they would launch their attacks against the running software.
This way,
the pen tests will take longer and likely detect less vulnerabilities
than if conducted under the situations described below.

### Semi-opaque box or gray box pentesting

This is the situation
in which the organization shares some information with the hacker
about the internal workings of the target but withholds the source code.

### Transparent box or white box pentesting

The organization shares the source code with the hacker
as well as other resources that can help testing by giving context.
The security analyst can then add performing [code review](../secure-code-review/)
to their tasks,
which also include attacking the software product as it runs.
This agreement yields the best results,
as the hacker can progress more rapidly
and has a better chance to find every vulnerability.

## Penetration testing stages

Although there is not a universally accepted standard
for how to do penetration testing,
we can name some stages or phases that hackers usually follow.
Keep in mind
that pentesting involves a lot of creativity
and adapts to the characteristics of the specific target of evaluation (ToE).
That is why the descriptions we provide here are meant
to give you a general idea of what this testing technique is like.

### Planning and reconnaissance

The pen testing activity starts with a planning stage.
The organization that owns the software determines the scope of the assessment
with the pen tester,
considering relevant security policies and standards,
as well as what it is willing to share about the software.
These agreements make part of what is known as “rules of engagement” (ROE).
Accordingly,
the pen tester begins to define what strategies to follow
and methods to use based on their knowledge and experience.

What is most usual is for the hacker to start to collect data about the ToE,
as pentesting is a very contextual type of security testing.
Of course,
if they have access to the source code and/or documentation,
those will come up handy.
Either way,
they resort to various means to know the product.
A possible route for them is passive reconnaissance.
The hacker turns to Google, social media and various other sites
to gather intelligence without direct contact with the target.
Active reconnaissance may come after.
It involves direct interaction
to learn what the technology used by the ToE is
and what possible entry and attack vectors there are.

### Scanning and vulnerability assessment

Pen testers may use tools to keep gathering information.
They may scan part of the ToE with free tools
to find out about, for example, its software versions and databases,
third-party components
and hardware types.
These professionals have a wide range of options.
Just look at the [OSINT framework](https://osintframework.com/) to get an idea.
Let's say the hacker uses the tool [Zenmap](https://nmap.org/zenmap/).
This program will help them perform a port scan
to recognize which ports are open
and the services and operating systems to which they are related.
Further,
the hacker may use enumeration tools
(e.g., [Angry IP Scanner](https://angryip.org/),
[Cain and Abel](https://sectools.org/tool/cain/),
[SuperScan](https://sectools.org/tool/superscan/)),
which help them find out the servers, devices, folders, files, users, and more,
within the ToE.

Other tools will help the pentester scan for security vulnerabilities.
If they have (or gained) access to source code already,
they may use a [static application security testing](../../product/sast/)
(SAST) tool
to scan for insecure code.
For example,
using [Gitleaks](https://github.com/gitleaks/gitleaks),
they can learn if there are exposed secrets
and sensitive data in Git repositories.
There is also vulnerability scanning that does not need the source code.
A [dynamic application security testing](../../product/dast/) (DAST) tool
might be useful,
as it would test the running software from the outside,
simulating actual use.
For example,
hackers can choose [Burp Suite Professional](https://portswigger.net/burp/pro),
if the ToE is a web application,
to find flaws and assist them with the exploitation of such flaws.
While automation is great to make progress fast,
it is up to the hacking expert to do their own code review and probing
having the business logic in mind,
as well as actually combining vulnerabilities to achieve a greater impact
than otherwise possible,
thus outsmarting any tool.
Since they will have to report it,
the hacker needs to calculate the impact of each vulnerability
considering factors like attack scenario,
difficulty of exploitation,
privileges required,
effect on the availability,
confidentiality
and integrity of the information resources managed by the software product,
and influence on any nearby systems.

### Gaining access through exploitation

The pen tester is able to come up with custom exploits
or find some already in use
to gain access and reach high-value objectives in their ToE.
They can use a tool like [Metasploit](https://www.metasploit.com/)
to help them develop such exploits
and launch their attacks.
During this stage,
the hacker focuses on XSS and SQL injection attacks,
among others,
with which they can bypass security controls.
Having, for example, hijacked a user account,
they can work to see if it is possible to escalate privileges
(i.e., get permissions beyond what is assigned for the account they are using),
view and transmit sensitive data,
and more.
From this stage,
it is all about demonstrating the impacts of security vulnerabilities.
Although the objective of pen tests is to find vulnerabilities in systems
through highly realistic attacks,
the ethical hacker may ensure that these attacks do not actually halt
or disrupt operations in the target company.
At Fluid Attacks,
for example,
our team of pentesters operates in "safe mode,"
unless otherwise requested by the target company,
so that the latter does not stop generating functionality
while security tests are performed.

### Maintaining access

It is usual in penetration testing engagements
to have hackers identify the ways
in which they can stay the longest within the ToE once they have access.
This is to simulate how threat actors have persistent presence
in their victims' systems.
The security analyst may install software classified as a "backdoor."
Such a program allows them to log remotely into a server or computer
without being detected.
Further,
they may use stealthy, somewhat slow methods
to extract data without being noticed
and may tamper with the logs and files that have records of their activity.

### Analysis and reporting phase

Pen testers are aware of the characteristics of useful reports.
These inform the ToE owners what sections of the ToE were assessed,
what vulnerabilities were found and by which methods,
how they exploited the security issues
(and any video POCs (proofs of concept) may constitute great evidence),
what is the risk exposure each of them represents based on their impact,
how much time they spent inside the ToE
and what remediation recommendations they can give.
With all this information,
the ToE owners should be able to take action
to better secure the systems in question.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/penetration-testing-as-a-service/"
title="Get started with Fluid Attacks' Penetration Testing as a Service
right now"
/>
</div>

## Knowledge during the pen tests

We would like to address some situations that may be agreed upon in the ROE,
regarding the knowledge both parties agree to have during the tests.
The following classification of these situations has been referred to
by other sources
as "penetration testing methods."
We do not use the presented categories in the latter sense.

### Targeted testing

The parties agree to share all their movements.
This helps the organization implement security controls
shortly after being informed of the associated flaws.
Then,
the pentesters can verify whether the organization's efforts were effective
and give advice.

### Blind testing

The parties agree to the company sharing only its name.
The hackers must then proceed
as malicious hackers outside the target organization would.
The organization would learn
whether this black-box testing can find security problems in their technology.

### Double-blind testing

The parties agree to part of the organization's security team not knowing
that penetration testing has been requested.
This will greatly support the operation being as realistic as possible.
The organization's defenders would not know the times and places
of the hackers' intrusions
nor be asked for permissions.
We advise this setting
for [red teaming](../attacking-without-announcing/) operations.

## What are the types of penetration testing?

Penetration testing is classified into different types.
There is no universal consensus on which classification to use.
Like in our post "[Security Testing Fundamentals](../security-testing-fundamentals/),"
we have chosen the one that indicates the target of evaluation.
We have expanded more on the [penetration testing types](../types-of-penetration-testing/)
in another blog post.

### Application pen tests

Ethical hackers can perform pentesting in [web](../../systems/web-apps/)
and [mobile applications](../../systems/mobile-apps/).
This ingenious methodology can detect complex security vulnerabilities
such as injection, business logic and deployment configuration flaws.

The focus on web application penetration testing may be the ten principal risks
identified by Open Worldwide Application Security Project (OWASP),
but may well go beyond that,
depending on what was required by the organization in the planning stage.
Anyway, the following are some security requirements that would be verified.

- The application defines users with privileges,
  securing against broken access control.

- The application uses pre-existing and up-to-date mechanisms
  to implement cryptographic functions,
  securing against cryptographic failures.

- The application discards unsafe inputs and includes HTTP security headers,
  securing against injection and cross-site scripting (XSS) attacks.

- The application uses third-party components in their stable,
  tested
  and up-to-date versions,
  which helps secure against using software with known vulnerabilities.

- The application requires lengthy passwords and passphrases,
  securing against identification and authentication failures.

- The application controls redirects to lead to trusted sites,
  securing against server-side request forgery (SSRF) attacks.

To look for known web vulnerabilities fast,
pen testers may use [website security scanners](../web-app-vulnerability-scanning/)
such as [Burp Scanner](https://portswigger.net/burp/documentation/scanner),
[ffuf](https://github.com/ffuf/ffuf),
[Nuclei](https://github.com/projectdiscovery/nuclei)
and [Vega](https://subgraph.com/vega/).
With those taken care of,
the experts may spend most of their time
looking for the most complex vulnerabilities,
which tools cannot find.

[Mobile app pentesting](../what-is-mast/) can be performed
on operating systems such as iOS, Android and Windows UI.
As with web apps,
the authority is OWASP with its Mobile Top 10 list.
The following are some important security requirements for this type of ToE.

- Requesting multi-factor authentication

- Not exposing session IDs in URLs and messages presented to the user

- Having insecure functionalities disabled or otherwise controlled

- Keeping communication protocols (such as Bluetooth) hidden,
  protected with credentials or turned off

- Encrypting sensitive information

- Having roles with different levels of privilege to access resources

- Not having repeated functions, methods or classes

- Not allowing parameter inclusion in directory names or file paths

- Obfuscating the data if the app is not in focus

- Not having unverifiable files (i.e., not included in audits)
  in the source code

Hackers can take advantage of automation when testing this ToE too.
Some tools to perform both automated SAST and DAST
in multiple OS are [MobSF](https://mobsf.github.io/Mobile-Security-Framework-MobSF/)
and [Objection](https://github.com/sensepost/objection).

### Network pen tests

In this type of assessment,
pen testers simulate attacks
to see if the network can be breached
(external penetration testing or external testing)
and/or find out what damage they are able to cause while inside
(internal penetration testing or internal testing).
While testing the former,
the hacker may try offensive attacks like password spraying
(i.e., testing a default password with multiple usernames looking for a match)
and credential stuffing (i.e., testing access with breached credentials).
Internal pentesting involves different actions,
such as [man-in-the-middle attacks](../../learn/what-is-man-in-the-middle-attack/)
(i.e., intercepting and/or modifying data
exchanged between two parties unbeknownst to them).

The following are some requirements pentesting would verify:

- Firewalls successfully block unauthorized access.

- The network is resilient to distributed denial of service (DDoS) attacks.

- Potential incidents are effectively identified, stopped, logged and reported
  by intrusion detection and prevention systems.

- Secure user access controls are in use.

- The configuration of network segments is secure and these are isolated.

- Secure data encryption mechanisms are in use.

- The configuration of load balancers and proxies is secure.

- Network devices and systems are up to date.

### IoT and hardware pen tests

Smart devices may also be inside the scope of penetration testing.
The following are some security requirements
that hackers may assess in this ToE.

- The device restricts logical access to local and network interfaces
  to only the authorized users, services or components.

- The device protects the data it stores and transmits
  from unauthorized access, disclosure and modification.

- The device has a secure default configuration.

- The device allows the creation of secure username and password pairs.

- The device uses components that are up to date
  and has a secure mechanism for updates.

### Personnel pen tests

In some cases, for example, when doing reconnaissance,
hackers may resort to [social engineering](../social-engineering/).
Basically,
they would use weapons of influence, such as scarcity of time,
to persuade unsuspecting employees to take cybersecurity risks.
Common attacks in these operations include [phishing](../phishing/),
where the hacker impersonates legit organizations or persons through email
to get the targeted person to share information, install malware, etc.

## Types of penetration testing tools

We have referenced throughout this blog post a few tools that hackers use.
We have some lists you can access for reference
in our [main post about red teaming](../red-team-exercise/)
and the one about [ethical hacking](../what-is-ethical-hacking/).
For this section,
we will just mention the classification often used for these tools.

- **Network sniffers:** These are tools hackers use
  when analyzing network security,
  as they allow monitoring data in network traffic
  to learn their source and destination,
  as well as which devices are communicating on the network
  and what protocols and ports are used.
  An example of these tools is [Wireshark](https://www.wireshark.org/).

- **Password crackers:** Hackers may use these tools to recover passwords
  by, for example, decrypting encrypted passwords,
  using brute force (e.g., password spraying)
  and uncovering cached passwords.
  One example of a password cracker is [Cain and Abel](https://sectools.org/tool/cain/).

- **Port scanners:** Hackers may identify open ports on the system
  with the help of these tools.
  As we already mentioned,
  these help recognize the operating systems on the ToE.
  A notable example of this kind of tools is [Nmap](https://nmap.org/).

- **Vulnerability scanners:** Hackers perform [SAST](../../product/sast/),
  [DAST](../../product/dast/)
  and software composition analysis ([SCA](../../product/sca/))
  with these tools.
  This way, they identify vulnerabilities in source code,
  the running application
  and its third-party components,
  respectively.
  A tool that can do all three techniques,
  along with cloud security posture management,
  is Fluid Attacks’ Machine.
  (For more information about these techniques,
  read our post "[How do SAST, SCA and DAST differ?](../differences-between-sast-sca-dast/)")

- **Web proxies:** These are tools hackers use to intercept,
  inspect
  and modify traffic between their browser and an application or web server.
  With these tools' help,
  the hacker can identify any HTML features they can target
  with cross-site request forgery (CSRF) or XSS attacks.
  An example of these tools is [OWASP® ZAP](https://www.zaproxy.org/).

## How often should penetration testing be done?

There is point-in-time and continuous penetration testing.
There are benefits of the latter over the former.
One of them is that,
if assessed continuously,
the organization can get real-time security posture identification.
This is key,
as threats are persistent and evolving,
and therefore a snapshot of the ToE's security can become outdated
pretty quickly.
Another benefit is that it costs less,
as organizations do not have to spend time with every pen test
looking for the right professionals available
and establishing the rules of engagement (ROE).
Continuous pen tests, unlike point-in-time, also allow for adjustable scope,
so that, if hackers discover assets that were unknown before setting the ROE,
they can include them in the tests.
Lastly,
continuous operations allow for ongoing support from experts,
so that those responsible for remediation can have advice
on many of the vulnerabilities found
and not only in the few ones they have to prioritize
during a shorter support period offered by point-in-time pen tests.
(For more on the benefits
and how we overcome the challenges of this modality,
read our post "[Continuous Penetration Testing](../continuous-penetration-testing/).")

Penetration testing is required in some industries.
The standards that mandate it also say how often it should be done.
For example,
the Gramm Leach Bliley Act (GLBA) requires financial institutions
to conduct pentesting annually.
So does the Payment Card Industry Data Security Standard (PCI DSS),
while requiring service providers specifically to do it once every six months
and after changes to segmentation controls/methods.
We expand on the subject in our post "[Penetration Testing Compliance](../penetration-testing-compliance/)."
At Fluid Attacks,
we advise organizations to go beyond mere compliance
and test their applications and other systems continuously.

## What are the benefits of penetration testing with Fluid Attacks

Organizations who choose Fluid Attacks' pentesting benefit from the following:

- Continuous penetration testing that goes along the evolution of the ToE

- Assessments by [highly certified pentesters](../../certifications/)
  (OSEE, eCPTXv2, eWPTXv2, etc.)

- Very low false positives and false negatives rates

- Visual evidence of our pentesters' exploitation of vulnerabilities
  in the ToE

- Unlimited remediation guidance through expert help

The above are among the benefits of our [Continuous Hacking Advanced plan](../../services/continuous-hacking/),
which also includes other manual techniques
([secure code review](../../solutions/secure-code-review/)
and [software reverse engineering](../../product/re/)),
besides our [vulnerability scanning](../vulnerability-scan/).
If you want to test the benefits of continuous security testing first,
by only performing scans to your application,
we invite you to start a [21-day free trial](https://app.fluidattacks.com/SignUp)
of our scanner.
To enjoy more than vulnerability scanning,
you can upgrade to the paid Advanced plan at any moment.
