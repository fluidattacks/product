---
slug: cyber-resilience-act/
title: The Cyber Resilience Act Is Coming!
date: 2023-08-11
subtitle: A brief overview of this recent EU draft regulation
category: politics
tags: cybersecurity, compliance, risk, company
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1691711456/blog/cyber-resilience-act/cover_cyber_resilience_act.webp
alt: Photo by Sara Kurfeß on Unsplash
description: Learn about the draft Cyber Resilience Act, including its purposes, obligations and penalties, before its final version comes.
keywords: Cyber Resilience Act, Cra, Products With Digital Elements, Pde, Economic Operator, European Union, Cybersecurity Risk, Ethical Hacking, Pentesting
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Content Writer and Editor
source: https://unsplash.com/photos/Q3CO1ZOZ6ZI
---

Europe continues ahead of the curve
when it comes to cybersecurity regulations.
A few months ago,
we brought up the renewal of its NIS Directive
([NIS2](../board-training-nis-2/)).
Now,
we will speak about the **Cyber Resilience Act** (CRA),
a regulatory proposal
aimed at the development and marketing of secure products with digital elements
(PDEs).
The CRA would serve as a complement to the NIS2 Directive,
which covers cybersecurity in services
provided by important and essential entities
in and for the European Union (EU).
Both regulations are part of a comprehensive cybersecurity framework
that the EU has been invigorating in recent years.

## PDEs and the objectives of the CRA

[In September 2022](https://digital-strategy.ec.europa.eu/en/library/cyber-resilience-act),
the European Commission published the draft CRA.
In order to strengthen the security of PDEs,
this regulation seeks to impose preventive requirements on their planning,
design, development, commercialization and maintenance.
It also aims to enforce the prompt reporting of exploitation of vulnerabilities
or other cybersecurity incidents
linked to these products.
The CRA would apply to various economic operators:
the manufacturers of PDEs
and those individuals or entities who contract their manufacture
or import or distribute them for use in the EU market.

But what exactly is a PDE?
[According to what is expressed in the draft](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=celex:52022PC0454),
a product with digital elements is "any software or hardware product
and its remote data processing solutions,
including software or hardware components
to be placed on the market separately."
It emphasizes PDEs "whose intended and reasonably foreseeable use
includes a direct or indirect logical or physical data connection
to a device or network."
Therefore,
beyond apps and other software products,
this term can include physical devices or hardware
such as smart TVs and refrigerators,
baby monitors, home cameras, smart watches, etc.

The CRA tries to grapple with several issues related to PDEs:
Currently,
EU legislations apply to the security of only a few.
Many PDEs are then launched on the market
with inadequate levels of cybersecurity.
This leads to the presence of vulnerabilities that,
when exploited,
can sometimes affect not only an organization
but an entire supply chain.
In addition,
some of these products are not properly maintained
and do not provide sufficient information to their users
regarding their security.

The purposes of the CRA are consistent
with the intentions of the [security-by-design and -default guidance](../secure-by-design-and-default/)
we discussed a few months ago.
The EU wants companies involved in or related to the manufacture of PDEs
to put into practice trustworthy security principles
from the beginning and throughout the life cycle of these products,
striving to place and maintain them on the market
with as few vulnerabilities as possible.
Moreover,
it is intended that users can have cybersecurity as an evaluation criterion
when choosing PDEs in the market
for their personal or organizational benefit.

The CRA defines some "critical" PDEs
for which there are specific and stricter rules.
These critical products are split into two classes,
with "class II" representing the highest cybersecurity risk.
They are considered critical
according to the intensity and breadth of the potential impact
of the exploitation of vulnerabilities present in them.
Special attention is given
to whether they operate in sensitive environments
or if they have essential functions where,
for example,
they process confidential data.

"Class I" PDEs include privileged access management software,
password managers, standalone and embedded browsers,
network management systems,
and application configuration management systems,
among many others.
"Class II" PDEs include operating systems for servers,
desktops and mobiles,
general purpose microprocessors, and secure cryptoprocessors,
among many others.
Firewalls, intrusion detection or prevention systems,
as well as routers, modems and switches,
can belong to one class or the other;
those for industrial use,
for instance,
would be in "class II."

Among the PDEs for which the CRA would not apply
since other laws cover them
would be products for medical use or for civil aviation,
motor vehicles, national security or military purposes.
Nor would it apply to cloud services such as software-as-a-service (SaaS)
or to free and open-source software (FOSS)
that were developed or supplied outside of commercial activity.

It would also rule out those PDEs
placed on the market before this regulation comes into force
unless,
since then,
they undergo significant modifications in their design,
functionality, performance or purpose
that affect their level of cybersecurity risk.
What would certainly apply to all PDEs within the scope of this regulation
that are on the market before that effective date
is the reporting of vulnerabilities and incidents.

## Obligations to be met by economic operators

The following requirements must be fulfilled by the manufacturers of the PDEs:

- It's a must
  to carry out continuous risk assessments on products
  to ensure the absence of exploitable security vulnerabilities
  in their design, development and production stages.
  This would minimize risks
  and prevent many incidents and severe impacts.
  Delivery requirements would include,
  for example,
  products with secure default configuration,
  appropriate control mechanisms,
  encryption of sensitive data at rest and in transit,
  and storage and processing of only as much data as necessary.

- As part of the risk assessments,
  the CRA highlights the importance
  of diligently managing the third-party software components
  integrated into the PDEs.
  Hence the requirement
  to undertake inventories of components and dependencies,
  also called software bill of materials (SBOMs).

- Once PDEs are on the market,
  vulnerabilities identified in them should be remediated as soon as possible,
  and users should receive the corresponding patches or updates
  as well as germane descriptions and instructions.
  With regard to incidents,
  users should also be informed of them for quick reaction
  and, if possible, implementation of corrective measures.

- Manufacturers must have and maintain adequate policies and procedures
  for the documentation, handling,
  remediation and disclosure of vulnerabilities in their products
  reported by internal and external sources.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/vulnerability-management/"
title="Get started with Fluid Attacks' Vulnerability Management solution
right now"
/>
</div>

- Vulnerabilities in exploitation or other cybersecurity incidents with PDEs
  must be reported to the European Union Agency for Cybersecurity
  ([ENISA](https://www.enisa.europa.eu/))
  within 24 hours of their identification.
  It is required to include details of the vulnerability
  and, if applicable, how it has been dealt with,
  or details of the incident, its impact, and possible causes.

- As part of raising social awareness,
  PDE users should obtain information
  that goes beyond the functionality of these products
  and involves cybersecurity aspects
  to be borne in mind for their selection and use.

- As explicit evidence of compliance with this regulation,
  each manufacturer must draw up and maintain an EU declaration of conformity.
  (The model structure with the required information
  appears in Annex IV of the CRA.)
  Before their product enters the market,
  they must also draw up technical documentation
  detailing the means used to ensure that
  their PDE complies with the fundamental requirements of this regulation
  (this documentation must be updated whenever necessary; see Annex V).
  Additionally,
  they must undergo conformity assessment processes
  (see Annex VI),
  which may result in the traditional CE marking,
  the only marking that should guarantee compliance with the CRA.
  In the case of critical class II PDEs,
  these assessments should involve an independent,
  competent and authorized third party.

Regarding importers and distributors,
they must only bring to the market PDEs
that comply with the fundamental requirements of this regulation.
Should any of these economic operators discover the existence
of a cybersecurity risk in a PDE
with which they are associated,
they must notify the manufacturer and the relevant authorities.

## Possible penalties for infringement

The Member States shall define the penalties
for infringement of the CRA by economic operators
based on the following limits already established:

- Failure to comply with the essential requirements
  may lead to administrative fines of up to €15M
  or, in the case of an undertaking, up to 2.5% of its total global revenues
  during the preceding financial year,
  whichever is higher.

- As per the above logic,
  such values could be up to €10M or 2% of global revenue
  for cases of non-compliance with other regulatory obligations.

- In cases of erroneous, insufficient or deceptive information
  provided to the relevant authorities,
  such fines could be up to €5M or 1% of global revenue.

When setting the amount of the administrative fine in each infringement case,
the corresponding authority will consider aspects
such as the severity and duration of the violation and its consequences,
as well as the size and market share of the economic operator involved.

## How is it going, and what's next?

This regulation would come into force
a few days after its publication
in the *Official Journal of the European Union*,
after its review and approval by the Council of the EU
and the European Parliament.
It was initially stated that after 24 months from its commencement,
the CRA would be fully applicable in the European market,
but that after the first 12 months,
manufacturers would have to comply with the obligation
to report vulnerabilities and incidents.
However, conditions such as these could change.

In July of this year,
[a press release was published](https://www.consilium.europa.eu/en/press/press-releases/2023/07/19/cyber-resilience-act-member-states-agree-common-position-on-security-requirements-for-digital-products/)
informing about the review of the draft CRA
by the Council of the EU
and mentioning some of the suggested amendments.
These were aimed at the scope of the regulation,
the recipients of the reports
about vulnerability exploitation and other incidents,
the complexity of the declaration of conformity,
support for small enterprises and micro-enterprises,
and,
as we said,
the deadlines for the implementation of the CRA,
among other things.

On this account,
we believe that,
for now,
the most prudent thing to do is to wait for the parties involved
to reach explicit and formal agreements
that will be made public in an official document
in order to be able to fully understand
and begin to adopt the requirements
that will come into effect with this regulation.
