---
slug: top-financial-data-breaches/
title: Top 8 Financial Data Breaches
date: 2024-06-06
subtitle: Consequential data breaches in the financial sector
category: attacks
tags: cybersecurity, company, trend, exploit, software, social-engineering
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1717634219/blog/top-financial-data-breaches/cover_data_breaches_finance.webp
alt: Photo by Robs on Unsplash
description: Find out the harsh lessons that the most breached industry has to teach other companies.
keywords: Financial Data Breaches, Data Breaches Finance Sector, Biggest Data Breaches, Customers Affected, Cybersecurity Risks, Ethical Hacking, Pentesting
author: Wendy Rodriguez
writer: wrodriguez
name: Wendy Rodriguez
about1: Content Writer and Editor
source: https://unsplash.com/photos/a-group-of-people-standing-next-to-each-other-HOrhCnQsxnQ
---

Data breaches are security incidents
in which unauthorized individuals access protected data.
They can happen for a number of reasons,
including malicious cyberattacks,
inadequate security procedures or technical flaws.
When deciding on their target,
malicious attackers seek optimum impact and maximum profit.
They get both in the financial sector, and they know it.
Last year,
[the financial sector was the most breached industry](https://www.marketsmedia.com/finance-most-breached-industry-in-2023/),
surpassing the healthcare and the professional services industries.

However embarrassing or detrimental to their brand,
it’s useful for their counterparts to know
how these finance companies have been hit with data breaches
and what lessons they can teach other organizations,
no matter the sector.
That’s why we have assembled this list,
the top 8 biggest data breaches in the financial sector,
which includes banks,
fintechs and reporting agencies from different countries.
They are ranked by the number of compromised data.
We've included the cause,
the impact and the lessons learned because
we want you to gain as much insight as possible from these companies,
so their past mistakes are not your future mistakes.

Since data breaches reported by financial services
reached an all-time high in the year 2023,
we’d like to highlight one of such incidents
before we address the top 8 of all time.
This one affected an impactful number of customers
from well-known and established banks.
Let’s learn more about it.

## The biggest data breach of 2023

In 2023,
[the biggest data breach](https://cybernews.com/news/ncb-management-services-data-breach/)
faced by a financial institution
was experienced by the debt collection agency NCB Management Services.
They were hit by a data breach attack that apparently ended
in a ransom being paid.
But nevertheless,
around **1.1 million people** had their information compromised.
Customers from Bank of America and Capital One,
among other companies,
had information like their address,
social security number,
account number and account status leaked during the attack on NCB.
The US-based debt collector was made aware of the breach 3 days after the fact.
They later had to send out letters to potential victims,
informing them that attackers had penetrated its systems.
In the letters, NCB assured people that the attackers
had been locked out of its systems and that
it wasn't aware of the potentially accessed information
being distributed or used maliciously.

NCB omitted more details about the nature of the attack it experienced.
But a [class action lawsuit](https://www.classaction.org/news/ncb-management-services-failed-to-protect-consumers-info-from-hackers-class-action-says)
was filed not soon after the attack was announced.
It specified that NCB’s network was unencrypted
and accessible without a password or multi-factor authentication.
The complaint also specifies that
“the company could have prevented the breach
by implementing reasonable cybersecurity measures
or simply destroying the data.”
An unknown insider source says the collections company knew
it had vulnerabilities but did nothing about them.
This lawsuit is still ongoing.

## Top data breaches in the financial industry

### 8. Robinhood - Social engineering

- **When did the breach happen?** November 2021.
- **What caused the breach?** Through social engineering,
  malicious actors tricked a customer support employee
  into giving them access to certain customer support systems.
- **How many customers were affected?** Around 7 million users.
- **What impact did it have?** The breach [this trading app](https://newsroom.aboutrobinhood.com/robinhood-announces-data-security-incident-update/)
  experienced exposed email addresses from about
  5 million users and full names from about another 2 million.
  For approximately 310 users,
  additional details like date of birth and zip code were leaked.
  For approximately 10 users, more extensive information was exposed.
- **What can be learned?** This attack with this specific tactic
  highlights the need for thorough employee training
  that addresses the ever-evolving strategies used by cybercriminals.
  Robinhood promptly and honestly told its users about the security breach,
  informing them of the amount of data that was compromised
  and the steps that were taken to mitigate it.
  That’s an important lesson to take into account:
  transparency and a quick response.
  This fintech app also gave impacted users advice
  on how to safeguard their accounts and private data.

### 7. Korea Credit Bureau - Insider threat

- **When did the breach happen?** Between December 2012 and December 2013,
  discovered early 2014.
- **What caused the breach?** This was an insider threat.
  The culprit was an employee of the firm Korea Credit Bureau (KCB).
  The employee was accused, and later arrested,
  for stealing the data from customers of
  three credit card companies while working for them
  as a temporary consultant.
  This employee allegedly copied customers’ data
  onto an external drive over a long period of time.
  The stolen information was sold to marketing firms,
  and upon discovery, the managers at the latter were arrested.
- **How many customers were affected?** At least 20 million bank
  and credit card users, 40% of South Korea’s population.
- **What impact did it have?** The breach [this credit ratings and risk management](https://www.securityweek.com/20-million-people-fall-victim-south-korea-data-leak/)
  firm experienced exposed customers' social security numbers,
  names, phone numbers, addresses,
  and credit card numbers and expiration dates.
  KCB was directed by the Supreme Court of Korea
  to pay the victims 62.3 billion won
  (approximately $48 million) as well as compensation for delayed damages.
  Following a lawsuit in 2015,
  the breach victims were compensated by the three credit card companies
  when it was determined that the stolen data could not be recovered.
- **What can be learned?** The fact that the data was not encrypted
  contributed to its relative ease to be stolen,
  so data encryption is key for protecting sensitive data.
  The KCB breach serves as a stark reminder
  of the importance of internal security controls.
  Internal threats are extremely dangerous,
  and a robust measure like the [zero trust security model’s](../../learn/zero-trust-security/)
  least privilege principle could’ve prevented this type of situation.

### 6. Experian - Advanced social engineering

- **When did the breach happen?** Between May and August 2020.
- **What caused the breach?** A malicious actor
  posing as a representative of an actual,
  legitimate business client tricked an Experian employee
  into relinquishing sensitive information.
  This was a sophisticated form of social engineering,
  as the threat actor provided the verification information Experian
  requests of its clients.
- **How many customers were affected?** About 24 million customers
  and almost 800,000 businesses.
- **What impact did it have?** The breach, which was specific to South Africa,
  [this credit bureau](https://www.experian.com/data-breach/knowledge-center/reports-guides/2020-2021-data-breach-response-guide)
  experienced exposed data including names, identity numbers, contact details,
  physical addresses, and job information
  (titles, places of employment and start dates).
  Experian said they had recovered the data, however,
  a large data file was discovered on the popular
  data transfer website WeSendIt.
- **What can be learned?** The importance of thorough client verification
  procedures is underscored with this incident.
  The need for employee training focused on cyber threat awareness
  and social engineering techniques is also highlighted in this example,
  as employees are a prime target for criminals.

### 5. CardSystems Solutions, Inc. - Exploited vulnerability

- **When did the breach happen?** June 2005.
- **What caused the breach?** Hackers infiltrated CardSystems’ network,
  exploiting a vulnerability to access the company’s files.
  The threat actors were able to install malicious software,
  which captured credit card information during transaction processing.
  The company serviced at least 100,000 small businesses as clients
  and processed at least $15 billion in credit card transactions annually
  at the time of the incident.
- **How many customers were affected?** More than 40 million
  credit card account users.
- **What impact did it have?** The breach [this third-party payment processor](https://www.nbcnews.com/id/wbna8260050)
  experienced exposed names, banks and account numbers, credit card numbers,
  expiration dates and security codes of millions of users.
  They were mainly MasterCard users,
  but the breach also included Visa and American Express users.
  Part of the victims were exposed or suffered fraudulent charges
  from the stolen credit card numbers.
  CardSystems faced lawsuits and fines, as well as business loss,
  as MasterCard and Visa dropped them as payment processors.
  At the time, it was labeled as the largest security lapse in the U.S.
  and it was a major wake-up call for the payment card industry.
- **What was learned?** After an investigation,
  it was informed that the company was not in compliance
  with industry standards of security.
  If they had been following the rules and requirements,
  they would not have been compromised.
  This incident significantly impacted the financial sector,
  leading to stricter regulations for data security.
  We have discussed the importance of data protection
  and the need for compliance in a [past post](../protecting-data-financial-services/).

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/security-testing/"
title="Get started with Fluid Attacks' Security Testing solution right now"
/>
</div>

### 4. JPMorgan Chase - Exploited vulnerability

- **When did the breach happen?** Discovered July 2014, began a month earlier.
  Disclosed in October 2014.
- **What caused the breach?** Malicious actors exploited vulnerabilities
  in JP Morgan Chase's web applications, in particular a zero-day vulnerability,
  to gain access to servers containing customer data.
  The biggest contributor to the breach was one single server
  that was not updated with two-factor authentication.
- **How many customers were affected?** Approximately 83 million accounts
  (76M individuals + 7M small businesses).
- **What impact did it have?** The breach
  [this bank](https://archive.nytimes.com/dealbook.nytimes.com/2014/10/02/jpmorgan-discovers-further-cyber-security-issues/)
  experienced exposed names, email addresses,
  phone numbers and mailing addresses.
  According to an internal investigation by the company,
  data like social security numbers and website credentials were not accessed,
  nevertheless, the breach exposed customers
  to phishing attacks and identity theft.
- **What was learned?** This breach reinforces the need for companies
  to have strong [vulnerability management](../../solutions/vulnerability-management/)
  practices to identify weaknesses in their systems.
  Even though JP Morgan reportedly spent $250 million
  per year on information security,
  they failed to update one of their servers with MFA,
  so a thorough and meticulous updating system is absolutely necessary.
  We highlighted the need for solutions that prioritize security
  in [this post](../financial-services-cybersecurity/).
  A positive tray needs to be mentioned:
  Their network was segmented,
  which restricted access and helped minimize the impact of the breach.

### 3. Capital One - Lax access controls

- **When did the breach happen?** March 2019.
- **What caused the breach?** A former Amazon Web Services (AWS)
  employee gained access to Capital One’s cloud storage infrastructure
  using unauthorized access credentials.
  The former employee understood the AWS platform,
  so she was able to exploit a misconfigured firewall on that network.
- **How many customers were affected?** Around 100 million people in the U.S.
  and about 6 million in Canada.
- **What impact did it have?** The breach
  [this bank](https://www.capitalone.com/digital/facts2019/)
  experienced exposed social security numbers and U.S. bank account numbers,
  as well as dates of birth, addresses, phone numbers, credit balances,
  transactions and credit scores.
  The hacker was convicted of wire fraud and unauthorized access.
  Capital One was fined $80 million and agreed
  to pay $190 million to affected customers.
- **What was learned?** This attack brings awareness to companies’ need
  to have stricter access controls to prevent unauthorized access
  to sensitive data.
  The importance of proper cloud security configuration is absolute,
  which can be bolstered with continuous evaluations
  and following recommendations by a [CSPM](../../product/cspm/) tool.
  Another issue was the bank’s misconfigured firewall,
  which created a vulnerability that was exploited by the hacker.
  Regular [security testing](../../solutions/security-testing/) and
  [vulnerability scanning](../vulnerability-scan/) are essential
  to identify and address potential vulnerabilities
  in system protection technologies.

### 2. Heartland Payment Systems - SQL injection

- **When did the breach happen?** Late 2008 and discovered/announced
  in January 2009.
- **What caused the breach?** Hackers used a combination of techniques:
  an SQL injection vulnerability exploited
  and malware injected through a webform on Heartland's website.
  Once inside, they installed malware to intercept credit card data in transit.
- **How many customers were affected?** Approximately 130 million individuals.
- **What impact did it have?** The breach
  [this payment processing and technology provider](https://abcnews.go.com/Business/PersonalFinance/story?id=6695611&page=1)
  company experienced
  exposed credit card numbers, cardholder names,
  expiration dates, and security codes.
- **What was learned?** This incident sheds light on the importance
  of strong security measures,
  like testing for and patching vulnerabilities continuously,
  implementing robust internal security measures and encrypting sensitive data.
  Heartland took several measures and fixed potential weaknesses
  in security that would have allowed for more attacks.
  They also eliminated the malware and backdoors
  that the attackers had been using.

### 1. Equifax - Supply chain attack

- **When did the breach happen?** Announced September 2017;
  attack happened in March 2017 and went undetected through July 2017.
- **What caused the breach?** The company was hacked through an exploited
  vulnerability in a web application used for consumers’ complaints.
  The vulnerability ([CVE-2017-5638](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-5638))
  laid in Apache Struts, an open source framework
  for creating web applications written in Java.
  Equifax failed to patch this vulnerability on time;
  the patch had been released six months prior to the attack.
  The exposed application allowed access to a database containing
  usernames and passwords sorted in plain text,
  which permitted attackers to go deeper into the system.
- **How many customers were affected?** Around 147 million customers.
- **What impact did it have?** The breach
  [this credit reporting agency](https://www.forbes.com/sites/thomasbrewster/2017/09/14/equifax-hack-the-result-of-patched-vulnerability/?sh=7d1e319f5cda)
  experienced exposed names, dates of birth, social security numbers,
  driver licenses and credit card numbers.
  Due to inadequate segmentation,
  the attackers were able to move laterally where
  they found unencrypted employees’ credentials,
  which allowed them to keep accessing systems.
  All the while, no one suspected a thing.
  The Federal Trade Commission fined Equifax almost $700 million
  for failing to protect its consumers’ data by not taking basic steps
  that could have prevented the breach.
- **What was learned?** The FTC was right,
  several basic steps could’ve prevented this situation.
  One of those steps was updating its systems
  and looking for released patches on third-party applications.
  Supply chain security becomes non-negotiable when
  a company uses third-party components.
  We explained this topic in [this post](../software-supply-chain-security/).
  Another basic step was data encryption,
  especially credentials for accessing parts of the system,
  as well as sensitive data.
  This part highlights the importance of strong authentication practices,
  which can help prevent unauthorized access.
  Proper and logical network segmentation is also part of those steps
  that could’ve been taken to prevent this breach.

By understanding these examples,
organizations could implement better security measures
to mitigate the risk of data breaches.
They include **using strong passwords, patching software vulnerabilities ASAP,
implementing better access controls,
training employees and customers on cybersecurity best practices,
and having a response plan (for any incident) in place**.

## Fluid Attacks against data breaches

These data breaches, all perpetrated by malicious actors,
prove that there are several ways to penetrate a system
and leverage off bad practices.
An important observation should be mentioned,
the top 5 breaches on this list had to do with software vulnerabilities.
That is why, from Fluid Attacks,
we recommend continuous [security testing](../../solutions/security-testing/),
with both automated tools
and manual methods that involve [pentesters](../../solutions/ethical-hacking/).

[Contact us](../../contact-us/) so we can let you know how
our [Continuous Hacking solution](../../services/continuous-hacking/)
can help your organization.
