---
slug: justify-investment-in-cybersecurity/
title: They'll Wait for the Reality Check
date: 2024-03-06
subtitle: How can we justify the investment in cybersecurity?
category: opinions
tags: cybersecurity, company, risk, software, security-testing
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1709774678/blog/justify-investment-in-cybersecurity/cover_justify_investment_in_cybersecurity.webp
alt: Photo by A S on Unsplash
description: While it can be a difficult task because cybersecurity often does not bring tangible benefits, here are some ideas that help justify investing in it.
keywords: Investment In Cybersecurity, Cybersecurity Investment, Costs And Benefits, Roi, Return Of Investment, Risk Exposure, Pentesting, Ethical Hacking
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Content Writer and Editor
source: https://unsplash.com/photos/shallow-focus-photo-of-white-and-gray-cat-J7rRzjba-kY
---

"Nothing bad has happened to us so far,
and surely nothing will ever happen.
We haven't had to spend millions on cybersecurity
like those other firms.
Poor, silly guys,
they invest over and over again;
they fritter away money on something
that doesn't give them even a hint of tangible proof
that it has served any purpose."

Full of the so-called **optimism bias** is whoever comes up with something
like the above.
"Luck," we could say, is what this person
—let's suppose a CEO—
and their company have banked on so far.
However,
if they hold that posture indefinitely,
sooner or later,
they will likely get a reality check,
a big blow that could prove very costly.
**Investing in cybersecurity pays off**,
but, as discussed in this post,
**it can sometimes be tricky to make a case for it**,
both before and after it's made.
(We exclude here those cases in which investment is already mandatory
for a company to comply with requirements
within its industry sector).

## Dealing with investment in cybersecurity

Usually,
the chief information security officers (**CISOs**)
are tasked with **persuading the directors or managers of their organizations**
to invest in security.
CISOs can be bombarded before the investment by the executive sector
with questions about the relevance of that investment
and estimated returns
and/or,
after the investment,
with requests for proof that it has been successful.
The point is that
responding convincingly to all this is often not a piece of cake.

Here,
the typical pre- and post-investment cost-benefit analyses come into play.
The latter include,
for example,
ROI (return on investment).
ROI is a performance measure
to determine how efficient or profitable an investment turned out to be.
Specifically,
it is a ratio between the return amount and the investment cost.
So,
if that value is positive,
it's because the income was greater than the investment.
The **problem with using ROI** is that
**we don't get tangible returns or monetary income**
from cybersecurity investment,
as might be the case with investments in new product development
and marketing campaigns.

In this area,
it is more appropriate to speak of **returns in terms of benefits**,
which are mainly linked to **loss prevention** and **resource savings**.
Security,
in fact,
is more about protecting the ROI
that can be achieved from other areas of an organization.
Considering and analyzing these benefits
and comparing them to the investment costs
is how it's often suggested to make a case for investment in cybersecurity
in order to convince executives,
but this can be a complex undertaking.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/security-testing/"
title="Get started with Fluid Attacks' Security Testing solution
right now"
/>
</div>

## Before deciding to invest

Cybersecurity involves all those activities aimed at protecting IT systems
—including infrastructure, operations and data—
belonging to an individual or organization
against possible risks, threats and attacks.
**Contributing to that protection is the main benefit**
a company can receive by investing in cybersecurity.
You only have to visit the website of a well-known newspaper or magazine
that covers technology issues
to discover headlines about successful cyberattacks
perpetrated against organizations of all types and sizes around the world.
Not all,
but many of these victims tend to be those
who do not invest "enough" in their security,
and some may have invested almost nothing at all.

Investing almost nothing or not "enough" could mean
for those organizations
disruptions in operations, theft of sensitive data, monetary losses,
damage to their reputation and reliability,
and even bankruptcy.
Let's face it:
Being part of the tech world,
which is rife with threats,
and not paying enough for cybersecurity solutions and products
is truly foolish.
Period.
**Investing guarantees the prevention of nuisances**
like the ones mentioned above.
(If that's not recognized by the leaders of the company you're a part of,
I recommend you look for another job).
But now the question is,
**how would we know the investment will be "enough"?**

A plain and obvious answer to that question would be that
**the solution acquired would eradicate everything
that represents a cybersecurity risk**.
But this wouldn't take into account our finite resources
and difficulties of knowing anything with absolute certainty.
A recommended option is to seek a balance:
The investment doesn't impede or hinder the company's growth
and, at the same time,
doesn't leave it vulnerable to significant damage
that leads to its collapse.
This would be especially true for small and medium-sized companies.
In the case of large companies mainly,
the balance would be that there are no delays in operations,
no waste of money,
and the risk exposure is kept as low as possible.
But **how can we know our risk exposure**,
at least to a large extent?

Cybersecurity risks depend on factors such as the size
and revenue of the organization,
the number of computing devices used,
the industry to which it belongs,
the information it handles and stores,
the number of employees and their security training,
and the number and complexity of the software products or applications
it develops and uses,
among others.
However,
**risk exposure is fundamentally determined by the vulnerabilities**
or weaknesses present in software and humans.
If we could know *all* the security vulnerabilities of an organization,
we would know *all* its risk exposure.
What we generally accomplish is to get close to it.

The **exploitation of vulnerabilities** by cybercriminals
usually results in **losses for a company**.
Vulnerabilities vary in their severity or in the risk exposure they represent
and, therefore, in the damage their exploitation could generate.
**Thinking in terms of possible losses** and quantifying them
can be helpful for comparisons with investment costs.
Thus,
if what we could lose is more significant
than what we would invest,
it would be wise to invest to avoid those losses.
Our investment would be "enough".
But here,
the matter is clouded because determining or quantifying those losses
may be pretty knotty.

Indeed,
these calculations are facilitated
when specific areas of impact by the exploitation of particular vulnerabilities
over determined periods are defined,
such as, for example,
those related to the denial of services.
Nonetheless,
**a judicious pre-investment analysis** would imply doing so
for absolutely all the areas that could be affected,
considering all the vulnerabilities
(or at least those of higher severities)
that could be present in the software
(according to its nature)
and all the possible effects or impacts of their exploitation.
In addition,
the probabilities of these situations occurring should be considered.
Not that this is impossible,
but it would be a mammoth task for experts in data analysis and probabilities
with an undoubtedly monumental amount of data,
for which,
curiously enough,
we could also ask,
**is it worth investing in?**

Some organizations,
unaware of or unwilling to invest in such intricate prediction methods,
resort to **more vague or weak approaches**,
such as, for example,
reviewing the losses suffered by similar companies
in publicly-known security incidents.
However,
the situations and impacts for one company and another
can be influenced by multiple different variables.
The fact that one had to pay X million dollars for a ransomware attack
is not a good reference point
to say that this is what you would face
and then determine whether or not it is worth investing
a certain amount of money in security.

Leaving aside these naïve comparisons
and even the option of estimating the probabilities of certain losses
occurring in your company,
acknowledging that the problem stems from vulnerabilities
existing in your systems
anyway allows you to infer that remediating them will help reduce
those unknown probabilities.
But in order to remediate them,
you first need to identify them.
And to identify them,
**you need to invest in** cybersecurity,
specifically **security testing**.
This first investment should be made without too much thought,
more as an immediate commitment.
Here,
you need to start by investing to get closer to a glimpse of the "enough"
or necessary investment.
It is a matter of trial and error.
Vulnerabilities must be detected as soon as possible;
**investments then can be evaluated according to results**.

## After deciding to invest

Having decided that you won't wait for a reality check,
you then move on to choosing
which security testing products and solutions to acquire and implement
in your company.
According to what we have mentioned,
the investment should not affect your productivity and economic growth
and, at the same time, should keep your risk exposure quite low.
Here is where
**cybersecurity providers' offers and achievements are evaluated**.

The investment in security testing has to be justified
mainly in terms of the coverage, accuracy, speed and depth
of the **vulnerability detection** methods and techniques
they offer you,
as well as the means they provide you with
for **vulnerability prioritization and management**,
and what they allow you to achieve in **remediation times and rates**.
Ask yourself:
Does that solution give us broad visibility into our attack surface
and an understanding of its risk exposure?
Does it allow us to recognize our software components
and discard the useless or unnecessary ones?
Does it save our developers time and effort
in remediating vulnerabilities
with various means of support?
Is it not hindering the deployment of our software into production?

The provided **assessment methods for your systems should be varied**,
targeting, for instance, your static source code ([SAST](../../product/sast/)),
running software ([DAST](../../product/dast/))
and third-party software components and dependencies ([SCA](../../product/sca/)).
In addition,
there should be evaluation by both **automated tools and experts**
(e.g., [ethical hackers](../../solutions/ethical-hacking/)),
as the former often report "security issues" where there are none
(false positives)
or fail to detect some real ones
(false negatives).

It's worth emphasizing that
investing in security testing is utterly pointless
if you're only going to sit back and watch all the vulnerabilities reported
without lifting a finger for remediation.
Remediation must be a bounden duty.
By having the option to prioritize vulnerabilities
according to the risk exposure they represent
and considering your current risk tolerance,
you can then determine what investments of time, effort, and money to make
with your team on remediation.
Remember that **identifying and remediating issues early
in the software development lifecycle
means vastly reduced costs**.
Enjoying this benefit
depends heavily on the integration of automatic security controls
and the support you get from the provider
with guidance and vulnerability remediation alternatives.

You should keep in mind that,
at least for a while,
the cybersecurity investment may be partially justified
by the number of vulnerabilities reported to you
and the risk exposure they pose.
However,
after some time of detections and, of course, remediations,
as well as preventions from your development team
based on the feedback received,
the reports may become less and less numerous,
although they may never stop coming.
Therefore,
**the success of your investment** may be later defined
by getting fewer reports
and seeing **a significant,
steady decrease in your company's risk exposure**,
even under comprehensive and exhaustive vulnerability assessments,
which, in other words,
would represent **high maturity in your cybersecurity posture**.

Do you already know the 21-day free trial of vulnerability scanning
with Fluid Attacks' AppSec testing tool?
[We invite you to try it](https://app.fluidattacks.com/SignUp).
Don't you want to be skeptical about the value of an investment
in security testing?
We invite you to access our all-in-one solution,
[Continuous Hacking](../../services/continuous-hacking/),
and enjoy all its benefits in favor of your company's cybersecurity maturity.
[Contact us](../../contact-us/).
