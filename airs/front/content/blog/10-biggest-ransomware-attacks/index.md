---
slug: 10-biggest-ransomware-attacks/
title: 10 Biggest Ransomware Attacks
date: 2024-03-21
subtitle: Historic events that will inspire you to act
category: attacks
tags: cybersecurity, company, trend, risk, software
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1711043923/blog/10-biggest-ransomware-attacks/cover_biggest_ransomware_attacks_1.webp
alt: Photo by Sebastian Pociecha on Unsplash
description: In this post, we talk about the biggest and most impactful ransomware attacks in history that emphasize the need to take cybersecurity measures.
keywords: Ransomware, Service, Raas, Ransom, Vulnerability, Software, Malware, Malicious Code, Cryptocurrency, Cybersecurity, Ethical Hacking, Pentesting
author: Wendy Rodriguez
writer: wrodriguez
name: Wendy Rodriguez
about1: Content Writer and Editor
source: https://unsplash.com/photos/silhouette-of-people-at-night-8_kfXcUieg4
---

“AIDS Trojan” sounds very menacing, doesn't it?
That’s the name of the malware used in the first
[ransomware](../ransomware/) attack
attempted over 30 years ago.
In 1989, that Trojan, created by biologist Joseph Popp,
was distributed via floppy disks.
It encrypted the names of the files in the computer,
and after the user had booted up their machine 90 times,
a window appeared with instructions on what to do to get a decryption tool.
The message included the amount for the ransom
and the address to which to send the check or money order.
This attempt to extort people through malicious software existed
even before the internet’s advent.
Now that the internet is part of our daily routine,
ransomware has become an ever-present threat.

It’s been reported by cryptocurrency-tracing firm
[Chainalysis](https://www.chainalysis.com/blog/ransomware-2024/)
that 2023 hit a record high in ransomware payments.
Ransomware attack extortions exceeded $1 billion last year,
even though law enforcement agencies like the FBI
(Federal Bureau of Investigation) and
government entities like CISA
(Cybersecurity and Infrastructure Security Agency)
recommend against going forward with the payment.
Ransom payouts are not the only issue when ransomware attacks are executed.
Companies have to deal with impacted clients,
data loss, lost revenue from time offline, tarnished reputation,
lawsuits, and as we’ll see in this post,
some companies never bounce back.
Taking into consideration the ransomware's method of execution,
the consequences of the exploitation,
and responses from the affected organizations,
we have gathered the top ten biggest ransomware attacks
that have been reported up to date.

## 10 - University of California

The University of California in San Francisco,
or UCSF, suffered a ransomware attack in 2020.
This attack affected a number of servers
within the School of Medicine’s IT environment.
[According to BBC News](https://www.bbc.com/news/technology-53214783),
the ransomware used for the attack was **NetWalker**,
which encrypted and extracted all of the data it seized
to be used later in the negotiation phase.
Even though they were able to contain
the attack to the medical school's IT systems
and it had no impact on COVID-19 research or patient care,
UCSF officials stated that they still felt compelled
to pay the ransom due to the encrypted data's critical role
in the School of Medicine's ongoing academic research.
Approximately **$1.14 million** in cryptocurrency
was paid to regain access to the encrypted data.

Throughout their run,
the NetWalker malicious actors targeted different establishments,
including other universities,
and continuously used the same modus operandi.
Via [phishing](../phishing/) or spam emails,
they gained unauthorized access to systems
and encrypted all the data possible along the way.
In this case, the attackers leveraged the stolen data,
posting it on their blog as proof of their actions,
which ended up forcing the university’s hand into paying the ransom.

## 9 - City of Dallas

[In May 2023](https://www.techtarget.com/searchsecurity/news/366553259/Dallas-doles-out-85M-to-remediate-May-ransomware-attack),
this city disclosed an allocation of **$8.5 million**
for restoration and remediation efforts after a group identified
as **Royal** infiltrated and affected fewer
than 200 of the city’s computer systems.
A basic service domain account connected to city servers
was compromised by Royal attackers,
who then used penetration testing technologies
and authorized third-party remote management tools
to execute lateral movement.
It was reported that with previously deployed command-and-control beacons,
the group was able to move through the city’s network weeks
before launching the attack, which encrypted data on city servers.

The attack led to disruptions or delays
in the Dallas Police Department’s website,
online payments for city services,
Dallas Fire-Rescue alerting services,
and the city’s court systems.
Sensitive data was also stolen
(social security numbers and private medical information was compromised)
to which the city’s response was to send letters to those affected.
Needless to say,
the people affected are still seeking some kind
of compensation or solution from the city.

## 8 - Kaseya

The software creator Kaseya
fell victim to an intricate ransomware attack
[in July 2021](https://www.csoonline.com/article/571081/the-kaseya-ransomware-attack-a-timeline.html).
The company is best known for providing MSPs
(managed service providers)
and developing virtual system/server administrators (VSAs).
In the atack, the cybercriminals exploited zero-day vulnerabilities
in the Kaseya VSA [on-premises](../on-premises-cloud-security/) software,
which allowed them to bypass authentication
and distribute ransomware to Kaseya’s clients
that encrypted the files on the affected systems.
The attack led to a widespread disruption of services,
and it was estimated that about 1,500 organizations
across different industries were affected by the ransomware.

The criminal organization **REvil**
(also known as Sodinokibi)
perpetrated this attack and initially asked
for **$70 million** to release a universal decryptor.
Kaseya refused to pay and was quick to react,
disabling its VSA servers and advising all its customers
to shut down their own VSA servers until patches were made available,
which was a couple of days after the attack.
The incident highlighted the growing ransomware trend
that targets managed service providers and,
by effect, their clients.

## 7 - JBS Foods

**REvil** also carried out another major ransomware attack in 2021,
this time against one of the largest meat processing companies in the world,
[JBS Foods](../jbs-revil-cyberattack/).
The attack infiltrated the company’s network
with credentials leaked from a previous attack
([5 TB of data](https://securityscorecard.com/blog/jbs-ransomware-attack-started-in-march/)
was extracted over three months).
Then, REvil deployed ransomware that encrypted data
and disrupted production at several of JBS
meat processing facilities around the world.

As a result,
the company had to shut down its operations
and ultimately caved in to the ransom,
paying **$11 million** in ransom to obtain a decryption key.

## 6 - Kronos

The ransomware attack in December 2021
on the workforce management company Kronos
(formally known as Kronos, now Ultimate Kronos Group or UKG)
targeted its feature Kronos Private Cloud.
This cloud-based service was used by many businesses
to manage matters such as payments,
attendance and overtime data.
As of this post, **the attackers’ identity remains unconfirmed**,
but it is known that they stole client data
and sought a payout from the company.
After UKG complied with the attacker's demands,
it was known that the data breach impacted
over 8,000 organizations which included hospitals,
factories, and small businesses that relied on UKG
for payroll and employee scheduling.
This ransomware attack apparently links
to the Kronos banking Trojan of 2014,
in which the [malicious code](../../learn/malicious-code/) would target
browser sessions to acquire login credentials unlawfully
by using a combination of web injections and keylogging.
The technical details of this attack were never released;
nevertheless,
it was reported that UKG paid an **unknown amount** to the attackers.

The legal fallout of this attack
was felt well after the fact.
The attack resulted in lawsuits from impacted companies
seeking compensation for damages, and in July 2023,
UKG reached a $6 million settlement with affected employees
from those companies

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/vulnerability-management/"
title="Get started with Fluid Attacks' Vulnerability Management
solution right now"
/>
</div>

## 5 - Colonial Pipeline

Deemed a “national security threat” by the government of Joe Biden,
[this](../pipeline-ransomware-darkside/) 2021 ransomware attack
was a disruptive incident that hit fuel supplies along
the East Coast of the United States.
Colonial Pipeline,
one of the largest and most important fuel suppliers in the country,
transports gasoline, diesel, jet fuel and home heating fuel,
from Texas to the Northeast region.

The attack was carried out by the malicious group known as **DarkSide**,
who gained unauthorized access through
an exposed password for a
VPN account ([password](../pass-cracking/) reuse).
The attackers deployed ransomware that encrypted Colonial Pipeline’s
data and demanded a ransom payment in cryptocurrency
in exchange for a decryption key.
The company mitigated the impact by shutting down its systems,
which caused disruptions to fuel supplies,
leading to panic buying and fuel shortage,
as well as a price spike.
The company ended up paying the ransom.
Approximately $4.4 million was paid and the system was restored;
with the help of the Department of Justice,
more than half of the payment was recovered.

## 4 - Travelex

As seen previously in this post,
**REvil** was involved in some of the most lucrative attacks
over the past few years.
In December 2019,
the then world’s leading currency exchange company,
Travelex, was hit with a major attack that exploited
a vulnerability in the company’s Pulse Secure VPN servers.
Sodinokibi ransomware caused the company’s computer system
to be crippled and encrypt the data,
leaving Travelex unable to access its files.
It can’t be said that the fault relies solely
on the provider Pulse Secure.
They had identified
and patched the vulnerability back in April 2019,
but Travelex had failed to implement the patch to its servers,
which left an open window for vulnerability seekers like REvil.

The attack severely and forever damaged Travelex.
Even though the attackers demanded a $6 million ransom,
the company ended up paying **$2.3 million**.
It also managed to regain access to its data.
However, Travelex had issues with the system
and was offline for almost two weeks.
After failing partners like banks and supermarket chains,
and due to the threat of a
General Data Protection Regulation
([GDPR](../../compliance/gdpr/)) investigation,
among other financial struggles,
Travelex was forced to sell in 2020.

## 3 - Costa Rican government

[In April 2022](../conti-gang-attacked-costa-rica/),
a cyberattack on the Costa Rican government
was so ferociously executed that it was declared a “national emergency.”
The attackers penetrated the Ministry of Finance first,
encrypting files and crippling two important systems:
the digital tax service and the customs control IT system.
The **Conti** ransomware group claimed responsibility
for this attack and demanded a **$10 million** ransom
in exchange for giving back the taxpayer’s data
and not attacking other government entities.
However, the Costa Rican government declined to pay the ransom,
leading to several other institutions being affected.
The Ministry of Science, Innovation, Technology & Telecommunications,
and the Ministry of Labor & Social Security,
as well as the Costa Rican Social Security Fund.
Consequently, 672 GB of stolen files were uploaded to Conti's website.

Conti’s attack is believed to be linked
to the second one perpetrated
by the [ransomware-as-a-service](../ransomware-as-a-service/)
operation group **Hive**.
It targeted and affected Costa Rica’s healthcare systems
by forcing them to shut down the Single Digital Health Record
and the Centralized Collection System.
As with the first attack,
the government declined to pay the **$5 million** ransom.
Recovering from the attacks took time and resources,
receiving help from Microsoft and
from governments of the United States, Israel, and Spain
to enable the restoration of Costa Rica's services.

## 2 - Ukrainian government

The **NotPetya** attacks, which occurred in 2017,
affected several countries across the globe,
but one of the attacks allegedly targeted Ukraine
for political motivations.
NotPetya had a couple of similarities to the Petya virus from 2016,
and it employed the same tactics as the infamous WannaCry attack,
exploiting unpatched devices,
spreading through networks and encrypting data.
This attack also overwrote the master boot record (MBR)
with malicious payloads and rendered infected computers inoperable.
However, it wasn't ransomware per se.
The NotPetya messages demanding a ransom were untruthful,
as there was no real possibility of getting
a decryption key even after payments were done.
Without a decryption key, data was irreversibly encrypted,
files unrecoverable and permanent damage was caused.

Ukrainian government agencies, businesses,
and critical infrastructures were disrupted.
Subsequent investigations by government agencies from the United States,
the United Kingdom
and other countries [attributed](https://www.washingtonpost.com/world/national-security/russian-military-was-behind-notpetya-cyberattack-in-ukraine-cia-concludes/2018/01/12/048d8506-f7ca-11e7-b34a-b85626af34ef_story.html)
the attack to the Russian military,
specifically the GRU (Russian military intelligence).
The investigations pointed to the geo-political tensions
between Russia and Ukraine still being seen today.

## 1 - WannaCry

In May 2017,
the WannaCry ransomware attack made headlines as one
of the most widespread and notorious cyberattacks in history
as it impacted organizations and individuals around the world.
Apparently,
the malicious hacker gang **Lazarus Group** perpetrated the attack,
which targeted computers running the Microsoft Windows operating system
and exploited a vulnerability with the hack called EternalBlue,
which had been stolen and leaked by The Shadow Brokers group.
Months before the attack,
Microsoft had released patches to address that vulnerability,
but many organizations and individuals failed to update
and apply the patches, leaving them exposed to the risk.

Around 230,000 computers in more than 150 countries
were affected within days of WannaCry’s release.
The impact was greatly felt by organizations
like Spain’s mobile company Telefónica,
where infected computers displayed a pop-up window demanding payment
through digital currency.
The UK’s National Health Service also fell victim to WannaCry,
hospitals and healthcare facilities were forced
to cancel appointments and divert patients due
to computer systems being locked by the ransomware.
The hackers also exploited smaller targets,
encrypting files from individual computers,
requesting from $300 to $600 in cryptocurrency to release the files.
Cyber risk firm Cyence calculated at the time that
the estimated loss from the hack
was around [**$4 billion**](https://www.cbsnews.com/news/wannacry-ransomware-attacks-wannacry-virus-losses/).

After reading this post,
nobody would blame you for running to your computer,
shutting it off, and never using it again.
These tales of terror, however scary they are,
hopefully serve as examples to raise awareness
and as motivation to take action
and be better prepared for attacks.
For companies,
we recommend our [Continuous Hacking](../../services/continuous-hacking/)
solution,
which combines automated tools,
AI and hacking experts to improve your chances against
the cybersecurity minefield.
[Contact us now!](../../contact-us/)
