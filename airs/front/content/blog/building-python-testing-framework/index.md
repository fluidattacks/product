---
slug: building-python-testing-framework/
title: Building a Python Testing Framework
date: 2025-01-15
subtitle: How we enhance our tests by standardizing them
category: philosophy
tags: cybersecurity, company, software, devsecops
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1736984492/blog/building-python-testing-framework/cover_building_python_testing_framework.webp
alt: Photo by Jr Korpa on Unsplash
description: At Fluid Attacks, we built an internal testing framework to improve our tests in Python. Let's talk about our motivation and results.
keywords: Python Code, Pytest, Python Testing Tool, Testing Framework, Standardization, Unit And Functional Tests, Mocks And Fakers, Ethical Hacking, Pentesting
author: Juan Díaz
writer: jdiaz
name: Juan Díaz
about1: Security Developer
source: https://unsplash.com/photos/a-blurry-image-of-a-bright-orange-and-blue-light-sDd9hIAS_IU
---

[Pytest](https://docs.pytest.org/en/stable/) is the king
of Python testing tools.
With more than 12,000 stars on GitHub,
a very active community, continuous improvement with new releases,
and a lot of forks and plugins to extend its functionality,
**pytest is the most important reference
when we need to test code in Python.**

If you look for Python testing tools or frameworks on the Internet,
you'll find articles
like "[Python Testing Frameworks](https://pytest-with-eric.com/comparisons/python-testing-frameworks/)"
by E. Sales,
"[10 Best Python Testing Frameworks](https://www.geeksforgeeks.org/best-python-testing-frameworks/)"
by GeeksForGeeks,
"[Top 9 Python Testing Frameworks](https://www.learnenough.com/blog/python-unit-testing-frameworks)"
by M. Echout,
and many other blog posts with similar rankings
where pytest is never missing.

The comparison with other tools seems unfair
since pytest is a tool for unit and integration testing,
whereas other frameworks compete in a very specific domain.
[Lettuce](https://github.com/gabrielfalcao/lettuce)
and [Behave](https://behave.readthedocs.io/en/latest/),
for example,
introduce behavior-driven development,
but they're not for all development teams;
[Robot](https://robotframework.org/) works for end-to-end tests and RPA,
but it's not for unit or simple integration tests;
[Testify](https://github.com/Yelp/Testify),
[TestProject](https://github.com/testproject-io),
and others are dead projects…
The [unittest](https://docs.python.org/3/library/unittest.html) module
(a Python built-in module)
and [nose2](https://docs.nose2.io/en/latest/)
could be the biggest competitors to pytest,
but they lack the support, community, and plugins pytest has.

At Fluid Attacks,
we use and recommend pytest.
Still,
we decided to wrap it before using it in our codebase.
In this post,
I want to share why you should try it
and how we achieved a new testing framework
for highly maintainable and readable tests.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/devsecops/"
title="Get started with Fluid Attacks' DevSecOps solution right now"
/>
</div>

## Testing with pytest

A previous blog post,
"[From Flaky to Bulletproof](../fluid-attacks-new-testing-architecture/)"
by D. Salazar,
guides our intention to improve the unit and integration tests.
There,
we recommend the use of a testing module
(a pytest wrapper)
due to a big deal:
**standardization**.

When your development team has a lot of members,
you need to define some rules to speak the same language.
You can add linters and formatters to standardize the code syntax,
but the way programmers write code is more tricky to standardize.

Some libraries are considered "opinionated"
because they enforce standardized usage with specific file structures
and their own methods and classes.
Indeed,
pytest has its own methods and files,
but it is so flexible that it seems "unopinionated."

These are some of the most unwanted features of pytest
when it comes to its flexibility:

- Pytest has an official way to mock methods and classes,
  but you can mock using other libraries (e.g., unittest) without conflicts.

- Pytest fixtures actually do magic.
  You can modify all test behavior with fixtures
  without directly referencing those functions
  and messing up the expected test flows.

- You can put tests wherever,
  even beside the functional code.

- You can use real or mocked services
  because pytest execution is not sandboxed.

If you assemble a growing team
in which everyone has different experiences working with pytest,
you can accumulate tons of technical debt,
unreadable code, and exponentially increasing WTFs/minute.

<div class="imgblock">

![Code quality](https://res.cloudinary.com/fluid-attacks/image/upload/v1736985222/blog/building-python-testing-framework/code-quality.webp)

<div class="title">

From [OSNews](https://www.osnews.com/images/comics/wtfm.jpg).

</div>

</div>

Therefore,
we decided to implement our pytest wrapper,
which includes other testing libraries like moto, freezegun,
or coverage-py to write test code in only one way.

Let's talk about our new guidelines and their benefits:

### Pytest prohibition

<div class="imgblock">

![Pytest prohibition](https://res.cloudinary.com/fluid-attacks/image/upload/v1736985132/blog/building-python-testing-framework/pytest-prohibition.webp)

</div>

Thanks to the [importlinter](https://pypi.org/project/import-linter/) library,
we forbid the use of pytest and unittest
in any place other than our testing framework.
We safeguard against misuse by removing pytest fixtures, unittest mocks,
and any unnecessary features that might be introduced later.
Thus,
the testing module can use pytest
and export the most important tools to be used.

### Wrapped utilities

`pytest.raises` (to catch exceptions),
`pytest.mark.parametrize` (to handle multiple cases per test),
and `freezegun.freeze_time` (to use a fake time to run the test)
are the most common features that we use.
We found a way to wrap them into functions
that can be imported from our testing module,
making them easy to use and allowing us to document examples
of how to implement them.

<div class="imgblock">

![Wrapped utilities](https://res.cloudinary.com/fluid-attacks/image/upload/v1736985133/blog/building-python-testing-framework/wrapped-utilities.webp)

</div>

### Mocked services

Fluid Attacks’ platform uses AWS services.
We decided to mock them with moto
to achieve very simple and fast tests
thanks to in-memory simulation of services like DynamoDB and S3.
However,
moto requires some boilerplate code to run and ensure isolation among tests.
Developers could add complex logic inside tests to mock services
or forget the right way to clean up these mocks
because some of the copy/paste steps miss it.

For that reason,
we included a decorator in our testing module
to start the fake AWS environment.
We also encapsulated all the startup and cleanup
to ensure test isolation and simplify the tests.
Developers only need the [documentation](https://dev.fluidattacks.com/components/integrates/backend/testing/unit-tests/)
to know how to preload data or files for testing
using a standard declarative approach.

<div class="imgblock">

![Procedural and declarative ways](https://res.cloudinary.com/fluid-attacks/image/upload/v1736985132/blog/building-python-testing-framework/mocked-services.webp)

</div>

### Fakers for data objects

Leaving behind the discussion
about the differences between fakers, mocks, stubs, and spies,
we needed to create fake data.
We addressed this by implementing Fakers,
a collection of functions designed to return specific fake objects
based on the data types.

Fakers are simple to implement and can call other fakers
to populate nested fields.
Developers can modify any field when calling them for flexibility.
This approach significantly reduces boilerplate code,
enabling the creation of well-structured test objects
without the need to define every property manually.

### Coverage per module

We took a deep dive into the coverage-py library
to achieve a modular solution.
This powerful tool generates detailed reports on test coverage
for the executed code,
serving as a critical resource for identifying gaps in our test suite.
By analyzing these reports,
we can check areas where additional tests are needed,
and the modular approach helps to focus developers on important tests first.

Also,
the new file structure speaks for itself.
Test files are next to normal files,
giving developers a simple way to check for missing tests and extend them.

### Continuous discussion

If any case requires one of the pytest missing features,
any developer can start a discussion to validate if a new feature is necessary
or if the current tools can handle the case.

We are open to discussions and improvements with the whole team,
prioritizing testability and readability.
Any component must be easily testable,
and any test must be highly readable.
We embody a declarative (explicit decorators in the startup)
and descriptive (tests are divided by Arrange, Act, and Assert sections)
solution for testing.

## Results

Our testing framework allows developers to test
if a user can create a new organization
(collection of groups or projects to be assessed) on our platform:

<div class="imgblock">

![New organization](https://res.cloudinary.com/fluid-attacks/image/upload/v1736985132/blog/building-python-testing-framework/new-organization.webp)

</div>

They can also test if a file was uploaded to Amazon S3:

<div class="imgblock">

![Upload S3](https://res.cloudinary.com/fluid-attacks/image/upload/v1736985133/blog/building-python-testing-framework/upload-s3.webp)

</div>

It was an amazing change
because the amount of WTFs/minute that old tests could generate
was considerable,
even more so when tests had to be maintained
and some mocks could hide potential bugs:

<div class="imgblock">

![AsyncMock](https://res.cloudinary.com/fluid-attacks/image/upload/v1736985132/blog/building-python-testing-framework/asyncmock.webp)

</div>

Thus,
we get a very simple and readable code with our testing framework.
The developers greatly appreciated the new testing experience
and have frequently commented on the ease, speed, and confidence
with which they execute the new tests.
That good experience reduces the **assertion-free tests** problem progressively
and motivates developers to write important tests.
We even focus some weeks on migrating old tests to the new framework
in a hackathon style,
prioritizing our code quality to be faster afterward.

<caution-box>

**Note:**
V. Khorikov in *[Unit Testing: Principles, Practices, and Patterns](https://www.goodreads.com/book/show/48927138-unit-testing?from_search=true&from_srp=true&qid=lT2201XI2F&rank=1)*
called "**assertion-free tests**" those tests that don't verify anything.
Even with asserts at the end,
a test could assert things that are not significant
(e.g., a function was called n times
instead of querying a new entity added directly from the database).

</caution-box>

A continuous testing practice is the first layer of any security strategy.

In this blog post,
I shared our learnings around building a pytest wrapper for standardization
and its huge benefits.
Don't forget that enforcing standardization,
reducing features to the essential ones,
mocking your external services only,
and being open to team discussions and feedback
can improve your testing culture.
The more testable and readable your code is,
the faster you can deliver new value
(and the more confident you'll be in your production releases).

## Special thanks

- D. Salazar,
  for supporting, discussing and implementing the core of this solution
  with me.

- D. Betancur and J. Restrepo,
  for attaching and prioritizing the testing framework development
  to the roadmap.

- The development team,
  for using the testing framework, giving meaningful feedback
  and contributing to its maintenance and growth.
