---
slug: top-10-data-breaches/
title: Top 10 Data Breaches in History
date: 2024-04-11
subtitle: Data breaches that left their mark on time
category: attacks
tags: cybersecurity, company, trend, risk, software, exploit
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1712856640/blog/top-ten-data-breaches/top-ten-data-breaches.webp
alt: Photo by Sean Pollock on Unsplash
description: A look back at these ill-intentioned attacks that made us grateful they weren't directed towards our organizations.
keywords: Data Breach, Breaches, Affected Users, Cybersecurity, Malicious Actors, Company, Ethical Hacking, Pentesting
author: Wendy Rodriguez
writer: wrodriguez
name: Wendy Rodriguez
about1: Content Writer and Editor
source: https://unsplash.com/photos/low-angle-photo-of-city-high-rise-buildings-during-daytime-PhYq704ffdA
---

Have you had to face a data breach?
If not, count your lucky stars.
[Statistics show](https://www.itgovernance.co.uk/blog/list-of-data-breaches-and-cyber-attacks-in-2023)
that the number of data breaches in 2023 reached a record 2,814.
News about those incidents only keep coming, just this past week,
[it was reported](https://www.forbesindia.com/article/news/hit-with-massive-data-breach-boat-loses-data-of-75-million-customers/92483/1)
that the data of **7.5 million** customers
of the Indian electronic company boAt Lifestyle
was breached and exposed on [the dark web](../dark-web/).
PII (personally identifiable information) such as names,
email addresses and phone numbers are now for sale in forums
and could eventually be used for different malicious purposes like phone scams,
phishing emails or blackmail.
These kinds of attacks can pose serious issues
not only for customers but also for the companies that are breached.
The consequences for companies may include financial loss,
reputational damage, customer loss, legal ramifications
and production downtime; all of these things may occur simultaneously.

As it has been proven time and time again,
we learn from hardships.
And these organizations have had to learn some hard lessons
as a result of malicious actors leveraging
their lax cybersecurity practices.
In this post,
we want to look back at the top ten known data breaches of all time,
ranked by the number of users/accounts affected.
Before going forward, let’s keep two things in mind:
(1) in order to save face, affected parties don’t always provide
details on how they were attacked or what was breached,
and (2) the difference between data breach and data leak
(the former involves intentional unauthorized access to data,
while the latter typically involves accidental exposure of sensitive data).

## 10 - LinkedIn

In mid-2012, hackers infiltrated
the [social media](https://www.cbsnews.com/news/linkedin-2012-data-breach-hack-much-worse-than-we-thought-passwords-emails/)
system and stole **117 million** email addresses and passwords from users,
both premium and free.
At first it was believed that 6.5 million users had been affected,
to which LinkedIn did little to nothing to warn them about the incident.
But then came 2016,
and sales of all the stolen data were seen on the dark web.
It was then that the company acknowledged the attack
and put out a statement advising their users to change their passwords,
avoid password reuse and leverage advanced security features
like two-factor authentication.
U.S. premium users
of the employment platform filed a collective lawsuit,
to which the company agreed and compensated a total of $1.25 million
to victims who paid for a subscription between 2006 and 2012.

## 9 - Dubsmash

The [once-trendy video messaging app](https://monitor.mozilla.org/breach-details/Dubsmash)
was affected by a data breach in 2018.
The company disclosed the situation in 2019
after seeing hackers selling the stolen data on the dark web.
Malicious actors infiltrated the app’s system
and accessed user data that included
account holders’ names, usernames, email addresses, geographical locations
and hashed passwords, which is still a security risk since attackers
with enough resources and time could crack these passwords.
**162 million** Dubsmash users’ information was compromised,
to which the company only replied with advice
on what to do in case of a breach.

## 8 - Wattpad

This [popular website](https://www.cbc.ca/news/business/wattpad-data-breach-1.5657724)
for storytelling and publishing was hacked in 2020.
The data breach involved the exposure of account information
belonging to more than **270 million** users.
The database, which included usernames, encrypted passwords,
geographic locations and emails,
was secretly sold on the dark web for 10 bitcoins
(almost $100,000 at the time).
Not much was disclosed about the breach,
but it’s known that it impacted everyone
who joined the website before 2017.
A statement from [the company said](https://support.wattpad.com/hc/en-us/articles/360046141392-FAQs-on-the-Wattpad-Security-Incident-July-2020)
that “out of an abundance of precaution” Wattpad
had reset all user’s passwords.

## 7 - Marriott International

This [leading hospitality chain](https://news.marriott.com/news/2018/11/30/marriott-announces-starwood-guest-reservation-database-security-incident)
became the target of a major data breach that was announced in 2018
and exposed the personal information of millions of guests.
That year,
it became known to the public that hackers
had gained unauthorized access to Starwood Hotels’ systems in 2014,
which Marriott purchased in 2016,
and that they had copied guest data before
the company was even aware of it.
Though some duplicates may have inflated the number,
potentially up to **327 million** guests’ data was compromised.
Exposed information included a variety of personal details,
such as names, mailing addresses, phone numbers, email addresses,
dates of birth, and, in some cases, passport numbers
and payment card details.
The breach was a significant blow to Marriott’s reputation
and a major financial blow that resulted in legal repercussions from customers,
a fine for the violation of British citizens’ privacy rights
under the [GDPR](../../compliance/gdpr/),
and system recovery costs.

## 6 - MySpace

The same sellers that offered the stolen LinkedIn data
in 2016 also claimed to have credentials from an unreported breach.
Back in 2013,
the [formerly-popular website](https://www.mcafee.com/blogs/internet-security/myspace-accounts-hacked/)
MySpace suffered a breach that compromised **360 million** user data.
Still,
either they didn’t know or didn’t make a statement
because it was only until 2016 that the data breach
was exposed by malicious actors looking for gains.
By then, MySpace had been purchased by Time Inc.,
which informed this social network’s users of the breach
and explained that their credentials
could have been used to access other websites.
For MySpace,
which was already struggling
to compete with newer social media platforms,
the revelation of the breach was a major blow
to its reputation and user trust.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/penetration-testing-as-a-service/"
title="Get started with Fluid Attacks' Penetration Testing as a Service
right now"
/>
</div>

## 5 - FriendFinder Networks

This 2016 data breach impacted **412 million** user accounts
across 6 different websites owned by
this [online dating and adult entertainment company](https://wolfesystems.com.au/insights-from-the-2016-adult-friend-finder-breach/).
FriendFinder Networks had inadequate security practices,
like storing passwords in plain text,
which contributed to the scope of the breach.
The 6 databases that were stolen included information like client names,
email addresses and passwords.
This was a major privacy nightmare for users,
who were exposed to identity theft,
extortion attempts and phishing attacks.
The company didn't fare well either,
losing a critical number of customers and its reputation along the way,
not to mention the investigations
and fines from data protection agencies it faced.

## 4 - Yahoo (2014)

This [web services provider](https://www.cnet.com/news/privacy/yahoo-500-million-accounts-hacked-data-breach/#google_vignette)
’s data breach of 2014 was publicly revealed in 2016
and affected a massive **500 million** accounts.
The stolen information included a significant amount of user data like names,
email addresses, phone numbers, dates of birth, hashed passwords
and encrypted and unencrypted security questions and answers.
Even though passwords were encrypted,
hackers were able to crack the hashes over time,
and the security questions that were unencrypted
were used to take over accounts.
It was very concerning that Yahoo waited over two years
to disclose the breach to the public,
giving that time to the attackers
so they could exploit the breached information.
At the time, Verizon Inc. was in the middle
of negotiations to purchase Yahoo’s core internet business.
Due to this breach and the legal measures taken by consumers and authorities,
Verizon was able to purchase Yahoo's for a significantly
lower amount than first planned.
It won’t be the last time we hear from this online company
as we make our way down this list.

## 3 - Aadhaar

The [Indian government’s ID database](https://economictimes.indiatimes.com/tech/technology/aadhar-data-leak-personal-data-of-81-5-crore-indians-on-sale-on-dark-web-report/articleshow/104856898.cms)
was breached in 2018 by malicious actors
who exploited vulnerabilities in the encryption mechanism
and leveraged outdated security protocols to access sensitive data.
The scope of the breach was monumental,
as complete names, addresses, biometric data
and Aadhaar numbers of **815 million** citizens
were stolen and later up for sale on the dark web.
Those affected by the breach were vulnerable to financial fraud,
incursion of privacy and a trust deficit in government initiatives
and digital systems.
As a good example of a positive response from the compromised entity,
the Indian government learned its lessons and implemented
the latest encryption technologies,
stricter access controls and advanced authentication protocols,
thus fortifying the nation's cybersecurity infrastructure.

## 2 - Indonesia SIM card

In 2022,
a massive data breach involved [SIM card registrations](https://restofworld.org/2022/indonesia-hacked-sim-bjorka/)
of **1.3 billion** users from Indonesia.
A hacker named “Bjorka” emerged in a popular dark web forum
selling these SIM card registration profiles
that revealed national identity numbers, phone numbers,
and names of telecommunications providers,
among other information.
It’s important to note that Indonesia’s population
is less than the number of registered cards (275.5 million as of 2022),
which suggests the data might include duplicates or registrations
for people with multiple SIM cards.
The Indonesian government initially downplayed the situation,
denying the extent of the breach.
Unfortunately,
Indonesians are used to their information being exposed
since it’s done so often that they even jokingly call Indonesia
an “open-source country.”
The attacker released a statement claiming he only executed
the breach to show the [“terrible data protection policy”](https://thediplomat.com/2022/09/bjorka-the-online-hacker-trying-to-take-down-the-indonesian-government/)
of the country,
especially if it is run by the government.
This, among many other cybersecurity incidents,
have sparked calls for stricter data protection regulation
and enforcement in the Asian country.

## 1 - Yahoo (2013)

This breach was labeled by the [New York Times](https://www.nytimes.com/2017/10/03/technology/yahoo-hack-3-billion-users.html)
as the “biggest known breach of a company’s computer network” ever.
After the company was purchased by Verizon,
it came to light that Yahoo had been breached in 2013
and that **3 billion** users’ data had been compromised.
This is not to be confused with the 2014 breach
that was acknowledged in 2016.
The company confirmed this breach in 2017,
after new intelligence was obtained and indicated
that all Yahoo user accounts were affected by the 2013 breach.
The attack,
which compromised the same user data as the 2014 breach,
had immense financial repercussions for Yahoo.
The costs for investigating the breach,
enhancing security measures, legal fees, million-dollar settlements
and regulatory fines reached over $150 million.
Reputational damages also emerged,
as the incident raised concerns about the company's ability
to protect user data and keep up with cybersecurity standards;
all of these contributed to a decline in the Yahoo user base.

Something worth stressing is that the majority
of these data breaches were the result of attacks
in which vulnerabilities were spotted
and exploited to gain unauthorized access.
When developing software,
[finding vulnerabilities](../../solutions/vulnerability-management/) early
in the SDLC is far preferable to doing so after a breach.
That’s why we recommend
our [Continuous Hacking](../../services/continuous-hacking/) solution,
which identifies, exploits and discloses vulnerabilities
in your software so you can address them immediately.
[Contact us](../../contact-us/) for more information.
