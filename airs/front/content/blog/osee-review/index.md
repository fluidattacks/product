---
slug: osee-review/
title: OSEE, an Unexpected Journey
date: 2023-09-01
subtitle: An OffSec Exploitation Expert review
category: opinions
tags: cybersecurity, exploit, training, company
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1693846506/blog/osee-review/cover_osee_review.webp
alt: Photo by Michael Dziedzic on Unsplash
description: In this post, we review the EXP-401 course and OSEE certification offered by OffSec.
keywords: OSEE, Exploit Development, Cybersecurity Success, Security Status, Ethical Hacking, Pentesting
author: Andres Roldan
writer: aroldan
name: Andres Roldan
about1: VP of Hacking and Research
source: https://unsplash.com/photos/ir5gC4hlqT0
---

On August 22,
I received this mail stating that
I had successfully passed the OSEE exam:

![OSEE Win](https://res.cloudinary.com/fluid-attacks/image/upload/v1693509765/blog/osee-review/mail1.webp)

That email was the culmination of years of studying,
hard work and pushing my limits beyond
what I could ever imagine.

The road to getting that email started
when I achieved the OSCE certification in 2020.
By then,
I bought a spot
in the Advanced Window Exploitation (AWE) training in London
and had two years of failed attempts to travel
due to the worldwide quarantine by COVID,
UK visa constraints and the passing of the Queen.

I will start this post with a summary of what the OSEE certification is.
Then,
I will dive into the details of the training
and some insights and tips for the exam.

## OffSec Exploitation Expert (OSEE)

OSEE is an OffSec certification
you obtain after completing the course
*EXP-401: Advanced Windows Exploitation* (AWE)
and passing a 72-hour grueling and hard exam.

I know people who have had the certificate since 2009,
but the course has received several updates
since its inception,
with exploits for Adobe Flash Player and the Windows Kernel
in their most recent versions.

The latest update was announced on the OSEE-certified private channel
on OffSec Discord's server
on September 18, 2021,
by [Morten Schenk](https://twitter.com/Blomster81),
one of the current instructors of AWE.

![Latest Update](https://res.cloudinary.com/fluid-attacks/image/upload/v1683239294/blog/osee-review/latestupdate.webp)

Quoting what they said during my training,
"We need to be constantly updating this course;
otherwise, the contents wouldn't be advanced enough."

And that phrase summarizes what AWE is:
the most advanced,
difficult and insane Windows exploitation training
on the market.

The current price for taking the course may vary
according to the location.
For example,
on BlackHat Middle East and Africa,
the current price is $12,375:

![Course Price](https://res.cloudinary.com/fluid-attacks/image/upload/v1683239294/blog/osee-review/price1.webp)

If you're going to take it in London like me,
the current price is £9,435 + VAT:

![Course Price](https://res.cloudinary.com/fluid-attacks/image/upload/v1683239294/blog/osee-review/price2.webp)

## A road to London

One of the main characteristics of the AWE course is that
you must attend in person.

Commonly,
AWE is delivered on BlackHat USA in Las Vegas.
However,
it was 2020 when I finished the [OSCE](../osce-journey/) certification,
and BlackHat USA was going to be 100% virtual
due to the COVID-19 pandemic.
I then look at the OffSec website
and found that there was going to be training in London
in April 2021.
I pulled the trigger,
but as expected,
it was rescheduled several times thanks to the pandemic.
After several iterations
and a delayed UK visa,
I was able to travel on September 2022
to take the course at the QA
(a partner of OffSec)
installations in London.

![Course](https://res.cloudinary.com/fluid-attacks/image/upload/v1683239294/blog/osee-review/course1.webp)

The course was designed to be delivered in five days.
However,
the Monday it was supposed to begin
was a bank holiday in the UK
due to the funeral of Queen Elizabeth II.
This resulted in four intense days of brain-melting training.

## Course prerequisites

As with all OffSec certifications,
it is not required to hold a specific title
before attempting to take any of their courses.
However,
as AWE is an advanced training,
they suggest you should have the following:

- Experience performing dynamic analysis of binaries with WinDbg
  and static analysis with IDA free.
- Hands-on experience of basic exploitation.
  You should at least have written exploits in the past.
- Basic knowledge of Windows x86_64 architecture
  and x64 shellcode creation.
- Familiarity with C/C++ programming and the use of
  Visual Studio.

They also suggest bringing a laptop with a modern CPU,
at least 16GB of RAM and 160GB of free hard drive space.
However,
when I arrived at the classroom,
each student had their own dedicated desktop PC with everything set up.
I'm not really sure if that is what happens at every training,
but keep that in mind.

There is also a pre-training challenge upon registration
called "0h n03z, I @m bR0k3n!".
For this challenge,
you are given a Visual Studio project with a broken shellcode,
and you need to perform dynamic analysis using WinDbg
to fix it and make it pop a calculator.
They conclude,
"Solving this challenge represents the minimal technical prerequisites
required for this course.
If you find this challenge too difficult,
you may want to reconsider your registration for the class."

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/ethical-hacking/"
title="Get started with Fluid Attacks' Ethical Hacking solution right now"
/>
</div>

## EXP-401

The official EXP-401 syllabus can be seen [here](https://www.offsec.com/awe/EXP401_syllabus.pdf).

One of the first slides of the training was this:

![WTF](https://res.cloudinary.com/fluid-attacks/image/upload/v1683239294/blog/osee-review/wtf1.webp)

What could you expect from training that starts like that?
Well,
you will be diving into five modules
where a detailed mindset for creating complex exploits is outlined.

On the first day,
you create a custom x64 reverse shellcode from scratch.
That sounds easy, right?
Well,
that was given in the first 2 hours of the training.
The rest of the day was dedicated
to explaining and completing a guest-to-host exploit
for a User-After-Free vulnerability in the VMWare RPC mechanism
(called backdoor),
bypassing modern exploit mitigations like _WDEG_
(which includes bottom-up heap randomization)
and an insane use of a mix of ROP + COP,
resulting with a reverse shell from the VMWare host.

On the second day,
a Type Confusion vulnerability on Edge (pre-chromium) JS engine
was presented.
Due to the nature of Edge and the security mitigations presented,
the road from the initial Arbitrary Read/Write primitive
to executing arbitrary Win32 APIs
is plagued with controls including CFG, ASLR, ACG, CET and DEP,
which are bypassed using mind-blowing techniques.
To be able to escape from the sandbox,
a CVE is abused,
which finally results in obtaining a reverse shell
just by clicking a button on a web page from the browser.

The third day was the start of Windows Kernel exploitation.
The first few hours were used to explain
how Kernel debugging was set up
and many important Windows internals concepts
relevant for exploitation,
like Memory Paging,
SSDT, intro to Kernel Drivers, and Kernel protections,
among others.
Then,
an LPE exploit was created for a FortiShield driver vulnerability,
creating an Arbitrary Read/Write primitive,
bypassing SMEP by abusing the Page Table Entries (PTE),
restoring execution of the Kernel
and making it version-independent.

And finally,
the fourth day was spent creating an exploit
for a native Windows driver (win32k).
The exploit performs heavy Pool manipulation and
a novel gadget for creating an Arbitrary Read/Write primitive.
Also,
by abusing memory leaks,
a version of the exploit that works from a low-integrity shell
is created.
To push things further,
the exploit is updated to work even with _HVCI_ enabled.

In summary,
the course will teach you to perform a full chain exploit:

- Abuse a Type Confusion vulnerability
  to obtain an Arbitrary Read/Write on a sandboxed Edge process
  and execute arbitrary Win32 APIs.
- Break out of the sandbox by exploiting
  (reversing a C# exploit!)
  another Edge process
  and running arbitrary OS commands in medium integrity.
- Perform a guest-to-host attack exploiting a vulnerability
  in VMWare's proprietary RPC protocol.
- Choose how to elevate privileges on the host OS
  by either exploiting a Fortinet driver
  or creating an exploit for the native *win32k* driver.

All of the above,
by bypassing advanced mitigations
like _WDEG_, CFG, CET, ACG, (k)ASLR,
DEP, EAF, _HVCI_, SMEP, etc.

Now,
the WTF levels are easily understood.

## Post-course hangover

As stated above,
the course was four days long.
Unlike many others,
the course material includes two books with written content,
a ZIP file with all the PoCs created
and the slides used to guide the training.
Several "extra miles" in the course
(hint, hint!)
are there to practice further on the topics presented.

Once the course is finished,
you have up to one year to schedule the exam.
I scheduled mine on August 17, 2023.

The month before,
I was 100% dedicated to reprising the course materials
and reproducing the same exploits:

![Course](https://res.cloudinary.com/fluid-attacks/image/upload/v1683239294/blog/osee-review/studying1.webp)

Actually,
I read the materials twice during that month of study.
The first time,
I resolved all the exercises of the five modules
and some extra miles.
The second time,
I reread all the material
but with a 30,000-foot view
to get the big picture of the contents
and understand the design of the exploits
from a high level.

That approach gave me more confidence
in understanding and digesting the deep level of technical concepts
presented in the course.

With everything set,
the exam day arrived.

## Exam

I booked the exam to start on August 17, 2023, at 7 a.m.
You have 71 hours and 45 minutes of lab time
to complete the challenges
and another 24 hours to create and upload a report.
As the exam is proctored,
you must show up 15 minutes before
for identity checking and other proctoring checks.

The [exam report template](https://offensive-security.com/awe/AWE-Exam-Report.docx)
provides guidelines on what to expect from the exam:

- There are two challenges.
- On the first challenge,
  you must escape a sandbox.
- On the second,
  you need to deal with the Windows Kernel.
- Each challenge can give you 25 or 50 points.
- You must at least achieve 75 points to pass the exam
  (i.e., you must complete at least one and a half challenges).

I can't give more details on the exam,
but I think the challenges are a good way
to evaluate the topics covered on the course,
and the time is enough to complete them
if you know what you're doing.

In my case,
I finished the exam just about an hour before the deadline.
Yes,
it was exhausting.

After a few hours of sleep,
I created the report
and sent it on the night of August 20.
The last time I submitted a report to OffSec was early this year
when I completed the [OSEP](../osep-review/) certification,
and I had to wait up to one week for the results.
However,
just two days after submitting the OSEE report,
on August 22,
I received the aforementioned email
saying that I had passed the challenges
and obtained the OSEE certification.

![OSEE Win](https://res.cloudinary.com/fluid-attacks/image/upload/v1683239294/blog/osee-review/mail1.webp)

## Exam tips

The following are things that worked for me,
but, as always, your mileage may vary:

1. Complete all the exercises in the book.
2. Each module has some extra miles.
   Complete as many extra miles as possible.
   The latest extra mile that I finished was the creation of an exploit
   for CVE-2021-31956:

    ![CVE-2021-31956](https://res.cloudinary.com/fluid-attacks/image/upload/v1693510644/blog/osee-review/poc.gif)

3. Repeat points 1 and 2.
4. I've done [previous work](../windows-kernel-debugging/) on Kernel Debugging.
   You can follow my blog posts
   and replicate the [bypassing of controls](../hevd-smep-bypass/)
   and creating an [exploit](../hevd-privilege-escalation/)
   for [HEVD](https://github.com/hacksysteam/HackSysExtremeVulnerableDriver).
5. The [Browser Exploitation](https://connormcgarr.github.io/type-confusion-part-1/)
   series of [Connor McGarr](https://twitter.com/33y0re)
   is a good way to learn and practice
   the Edge Type Confusion module.
6. Join the [OffSec Discord server](https://discord.gg/offsec).
   The community is awesome,
   and OffSec support personnel can assist you with anything
   related to the course.
7. The VPN connection is not stable.
   As it's a UDP tunnel,
   there can be problems with the _MTU_ size calculation
   (VPN MTU > Link MTU),
   which can lead to packet loss during heavy traffic.
   Follow [this guide](https://www.thegeekpub.com/271035/openvpn-mtu-finding-the-correct-settings/)
   to troubleshoot it.
   In the end,
   I had to add the `mssfix 1387` line
   to my OpenVPN connection file
   to fix those issues.

## Common criticism

First and most notoriously,
attending the course is expensive.
However,
in my opinion,
what you learn during the training and preparation for the exam
is worth every penny.

Also,
as the course name suggests,
the training is about exploitation.
Microsoft has done a great job
since the release of Windows 8
to continuously improve the security of the OS.
So,
even though the course was updated in 2021,
many techniques presented in the training
no longer work in the most current Windows OS versions.
That means that,
although the gained knowledge is more focused
on creating the mindset for making complex and advanced exploits,
some of the techniques and primitives used are no longer valid
and new research is needed to develop an exploit
for a modern Windows OS build
(again,
I believe this can even be an advantage
if you see it from another perspective).

## Conclusions

The OSEE experience was really enriching for me.
Obtaining this certification was a personal dream
I had for a long time,
and it is or was an ultimate goal
for many other fellow offensive security professionals.

But I've always had the same feeling
after getting a certification:
What's next?
