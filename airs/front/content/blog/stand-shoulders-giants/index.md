---
slug: stand-shoulders-giants/
title: Stand on the Shoulders of Giants
date: 2018-02-14
category: attacks
subtitle: About software composition analysis
tags: security-testing, software, vulnerability
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1620331101/blog/stand-shoulders-giants/cover_iiuzyx.webp
alt: Photo by Vincent Riszdorfer on Unsplash
description: "Here we mainly develop a discussion on 'A9' of the OWASP Top 10: Using components with known vulnerabilities, in particular, free and open software libraries."
keywords: Software Composition, Analysis, Dependency, Vulnerability, OWASP, Linux, Foss, Ethical Hacking, Pentesting
author: Rafael Ballestas
writer: raballestasr
name: Rafael Ballestas
about1: Mathematician
about2: with an itch for CS
source: https://unsplash.com/photos/exKQ01AmzNA
---

In our [last post](../infinite-monkey-fuzzer/),
we reproduced the discovery of a vulnerability in libpng.
You might say that is just a small library with a very limited scope
and only 556 KiB installed.
However, many, many packages depend on it.
To see how many packages in the Arch Linux repository depend on libpng,
we can use pacgraph by [Kylee Keen](http://kmkeen.com/pacgraph/):

<div class="imgblock">

!["libpng reverse dependencies"](https://res.cloudinary.com/fluid-attacks/image/upload/c_scale,w_800/v1620331100/blog/stand-shoulders-giants/libpng-pacgraph_edml6c.webp)

<div class="title">

Libpng reverse dependencies in Arch Linux.

</div>

</div>

More than 14 GiB worth of software depends on `libpng`\! And that is
only in the Arch Linux repositories, which is hardly the most popular
Linux distribution. Also, this library is the official PNG reference
library and is cross-platform, so certainly many other packages in other
operating systems depend on it.

Now, back in 2015 when, `libpng` had not yet fixed the low-high palette
bug, all the programs and libraries above were also automatically
vulnerable to the same issue. Actually, this is what happened to
Equifax with a vulnerability in Apache Struts. Same with many web
services that use OpenSSL with Heartbleed.

If this could happen to such flagships as
[`Bash`](https://www.gnu.org/software/bash/),
[`Qt`](https://www.qt.io/),
[`TeX`](https://services.math.duke.edu/computing/tex/latex.html) and
[`Xfce`](https://xfce.org/), it could happen to your organization. In
fact, this problem is so common
that it is part of the 2017 [OWASP](../../compliance/owasp/) Top 10;
they call it "A9: Using components with known vulnerabilities."

Given the rapid adoption of Free and Open Source Software (FOSS) by
large companies, all of a sudden, dependency vulnerability appears to be
one [hell](https://en.wikipedia.org/wiki/Dependency_hell) of a problem.
Or rather,
as yuppies would like to point out,
a "business opportunity"?

Many providers of so-called software composition analysis
(SCA)
(don't google it) have since appeared in the security scene.
Some of them are backed by long-standing companies; most are not.
In fact, this business has gained such momentum that it is expected to grow
more than 20% each year from now
[to 2022](https://www.prnewswire.com/news-releases/the-software-composition-analysis-market-is-expected-to-grow-from-usd-1540-million-in-2017-to-usd-3984-million-by-2022-at-a-compound-annual-growth-rate-cagr-of-209-300595028.html).

These companies,
who owe so much to FOSS, tend to make it look bad when marketing their SCA.
However, FOSS adoption is not slowing down.
As we will try to show here, it is not FOSS's fault
if applications use it with known vulnerabilities
but rather that of the apps' owners.

[Today’s applications](https://pure.qub.ac.uk/en/publications/vulnerability-detection-in-open-source-software-the-cure-and-the-)
use 30+ libraries on average,
representing up to 80% of their code.
Think of it as your code being only a thin layer upon
a building of some tiny, some larger boxes.
What SCA does then is look for vulnerabilities
inside those boxes with information from external databases,
which are then considered as vulnerabilities in your own app:

<div class="imgblock">

!["Dependency building"](https://res.cloudinary.com/fluid-attacks/image/upload/v1620331101/blog/stand-shoulders-giants/depvuln_ztp8tw.webp)

<div class="title">

SCA scans all the blocks in your app building.

</div>

</div>

Instead of going from the alleged solution towards the source of the
problem, let’s do it backwards.

## The bad

FOSS is developed and used by thousands around the world. This can be
a double-edged blade: On the one hand, according to "Linus's Law,"
bug-finding and patching should be easier as more eyes are involved.
On the other hand, the lack of a centralized guide makes room for bugs.

One difference between FOSS and proprietary software is that,
due to all its restrictions, bugs in the latter
are “less likely” to become public as soon as in the case of the former.
So all vulnerabilities in proprietary software
are usually expected to be zero-day.

So if the source of the problem is not FOSS, what is it? The main
reasons why so many companies suffer from A9:

- Not knowing used dependencies

- [Ignorance of their vulnerabilities](../what-is-vulnerability-management/)

- No continuous scanning for bugs

- Not testing for compatibility

- Component misconfiguration

In essence, it all boils down to a lack of communication between the
user and the source of the components.

## The good

So what can you do? `OWASP` recommends the following guidelines
[to prevent A9](https://owasp.org/www-project-top-ten/2017/A9_2017-Using_Components_with_Known_Vulnerabilities.html)

- Trim unnecessary dependencies, features, components etc. That way
  you have less to check.

- Continuously monitor components for updates and vulnerability
  reports.

- Only obtain components from trusted sources.

- Make these guidelines into a company policy.

There are specific tools that compare the version of the dependency
you are using against both remote repositories
(to check for updates) and vulnerability databases
(to find out if any of your dependencies contain reported vulnerabilities
that have not been fixed yet).

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/vulnerability-management/"
title="Get started with Fluid Attacks' Vulnerability Management solution
right now"
/>
</div>

- For `JavaScript` you can use
  [`retire.js`](https://github.com/retirejs/retire.js/).

- `Java` users have the
  [`Versions`](http://www.mojohaus.org/versions-maven-plugin/) plugin
  for `Maven`.

- Also for `Java` and `.NET`, you can use the [`OWASP Dependency-Check`](https://www.owasp.org/index.php/OWASP_Dependency_Check)
  tool.

- There’s a [Dependency
  check](https://github.com/stevespringett/dependency-check-sonar-plugin/tree/master/examples/single-module-maven)
  `SonarQube` plugin.

Note that the language-specific tools have to be integrated with the
appropriate package manager, like `npm` or `yarn` with `retire`.

A bird’s eye view of how the process should integrate with your
development flow is depicted by the following diagram provided by
[SourceClear](https://en.wikipedia.org/wiki/SourceClear).

<div class="imgblock">

!["Integrating SCA into dev flow"](https://res.cloudinary.com/fluid-attacks/image/upload/v1620331100/blog/stand-shoulders-giants/source-clear-flow_uplizt.webp)

<div class="title">

Integrating SCA in your development flow.

</div>

</div>

We see that every time code is added, the whole system gets scanned for
third-party software vulnerabilities and other issues easily identified
by static analysis when code is not available. This is done by following
this procedure:

1. Identify the dependencies on which your software is based.

2. Detect the versions of those dependencies.

3. Check the dependency master repository for updates.

4. Check one or several vulnerability databases, like
   [CVE](https://cve.mitre.org/) and [NVD](https://nvd.nist.gov/) or
   your own.

5. Report the findings.

It is a simple process, really.

Notice that the integration is not fully automatic, and it should not
be, since these tools could (and usually do) raise false alarms, so they
are reviewed by human security experts.

Internally, the process of scanning for third-party software
is the same for both proprietary software and FOSS,
and it is a simple matter of querying
the vulnerability databases as described above.

Speaking of integration, you may wonder:
What if my app is deployed inside a container?
"Over 30% of official images in Docker Hub
contain high priority security vulnerabilities,"
according to [PenTestIt](https://pentestit.com/anchore-open-source-container-inspection-analysis-system/).
Fortunately, there are tools
that go into your container and
perform SCA inside of it (and more), like
[Anchore](http://pentestit.com/anchore-open-source-container-inspection-analysis-system/)
and
[Dockerscan](http://pentestit.com/dockerscan-docker-security-analysis-suite/).

## The ugly

I know you did search for "software composition analysis"
when I suggested you didn't.
I just know you did. If you didn’t, good for you\!
Here’s what you’re missing out on:

<div class="imgblock">

!["SCA providers collage"](https://res.cloudinary.com/fluid-attacks/image/upload/v1620331099/blog/stand-shoulders-giants/marketing-hype_pedyr7.webp)

<div class="title">

"Software composition analysis" providers.

</div>

</div>

All of these industry-leading, award-winning, breakthrough-makers,
oracles of the tech future want to sell you one thing: static code
analysis plus the tools we discussed above.

While static analysis is a technique,
it’s just a technique.
It scans code and detects vulnerabilities and unhealthy practices but also
encourages late detection and produces a lot of false positives.

You could try hiring such a service, and maybe even try to complement it
with dynamic analysis tools like [fuzzing](../infinite-monkey-fuzzer/)
and debuggers, but those have their own issues.

It’s worth noting that these are no replacement
for good old-fashioned human [code reviews](../../solutions/secure-code-review/).
At least at the moment.
According to [Contrast Security](https://cdn2.hubspot.net/hub/203759/file-1100864196-pdf/docs/contrast_-_insecure_libraries_2014.pdf),

<quote-box>

The only way to deal with the risk of unknown vulnerabilities in
libraries is to have someone who understands security analyze the
source code. Static analysis of libraries is best thought of as
providing hints where security vulnerabilities might be located in the
code, not a replacement for experts.

</quote-box>

In the future,
we might see things
like distributed on-demand
[security testing](../../solutions/security-testing/)
and [machine learning algorithms](https://pure.qub.ac.uk/en/publications/vulnerability-detection-in-open-source-software-the-cure-and-the-)
using support vector machines to try to predict which commits are likely to
open vulnerabilities, but in the meantime, stick to the tried-and-true.