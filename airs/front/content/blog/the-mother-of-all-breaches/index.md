---
slug: the-mother-of-all-breaches/
title: The Mother of All Breaches?
date: 2024-01-26
subtitle: Let's rather say a bunch of breaches in a single box
category: opinions
tags: cybersecurity, credential, risk, vulnerability, trend
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1706319423/blog/the-mother-of-all-breaches/cover_the_mother_of_all_breaches.webp
alt: Photo by Ray Hennessy on Unsplash
description: They said they discovered the mother of all data breaches. But it was a false alarm. Let's understand why.
keywords: Data Breach, Mother Of All Breaches, Moab, Diachenko, Cybernews, Leak, Leak Lookup, Pentesting, Ethical Hacking
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Content Writer and Editor
source: https://unsplash.com/photos/flock-of-white-and-black-birds-1cVQMljvsnw
---

Yeah,
that's what some people are saying:
the "**Mother of All Breaches**" (MOAB).
What does that mean?
What happened?
The security researcher [Volodymyr "Bob" Diachenko](https://www.linkedin.com/feed/update/urn:li:activity:7155298570126921730/),
in collaboration with the [Cybernews team](https://cybernews.com/security/billions-passwords-credentials-leaked-mother-of-all-breaches/),
allegedly,
recently discovered a massive data breach
with more than **26 billion records**.
This is more than three times the number of human beings on Earth today.
But has this finding been properly named?

Let's start by highlighting what has been discovered
in this gargantuan amount of data.
Researchers say it is mainly passwords and user data
from applications such as LinkedIn, Twitter/X, Wattpad, Evite, Adobe
and Weibo, among others.
But the first place among all of them goes to Tencent QQ,
a Chinese instant messaging software,
accounting for about 5.8% of the total "MOAB."
This data breach also contains records of government agencies
from the U.S., Germany, Brazil, Turkey, and other countries.

What the research team specifically found in an "open instance"
was a judiciously organized database with nearly **4,000 folders**
taking up around **12 terabytes**.
The thing is that each folder contains records of a separate data breach,
**many of which had already been reported previously**.
So,
although it was the researchers who apparently dubbed it "MOAB,"
this finding looks more like a database of multiple data breaches.
The team even expressed that it is highly probable
that there are duplicates in that database
but that there seems to be new user data included anyway.
Nonetheless,
instead of saying the "mother of all data breaches,"
I think it is more appropriate to call it
"the largest compilation of multiple breaches,"
as curiously Cybernews later referred to it in its own publication.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/penetration-testing-as-a-service/"
title="Get started with Fluid Attacks' Penetration Testing as a Service
right now"
/>
</div>

OK,
but where did all that data come from?
When I first read about this discovery,
there was no statement from a criminal group taking responsibility.
But today,
January 26,
Malwarebytes said in an [updated post](https://www.malwarebytes.com/blog/news/2024/01/the-mother-of-all-breaches-26-billion-records-found-online)
that the source of the dataset was [Leak-Lookup](https://leak-lookup.com/).
Believe it or not,
this is a data breach search engine that
"allows you to search across thousands of data breaches
to stay on top of credentials that may have been compromised."
And precisely,
Leak-Lookup shows on its website a total of about **26 billion records**,
corresponding to **4,176 breaches**.

So,
did the breach
(or should we say the leak?)
occur to a company that has collected
and is collecting data breaches?
(I had forgotten for a time the risk exposure
these data aggregation services pose.)
Affirmative.
In fact,
Leak-Lookup [posted on its X account](https://twitter.com/leaklookup/status/1749951429693919400)
on January 23
(linking to the original Cybernews post of January 22)
that
all that issue was the product of a firewall misconfiguration on their systems
that they had already fixed.
According to Malwarebytes,
the affected company said
the initial access to its dataset was reached in December last year.

Let's face it.
It all sounds bizarre.
Not only the fact that Cybernews talked about it
as the "mother of all breaches"
when they knew it was not
(clickbait strategy?),
but also that they didn't immediately report that
they already knew the source of all that data.
(On January 26,
[Cybernews](https://web.archive.org/web/20240126094954/https://cybernews.com/security/billions-passwords-credentials-leaked-mother-of-all-breaches/)
was still not reporting it.
**Update:** They came to do so on January 29.)
Moreover,
both the discoverer and source of discovery are teams
that collect records of data breaches on the Internet.
(Yes, Cybernews has its [data leak checker](https://cybernews.com/personal-data-leak-check/).)
Leak-Lookup's post even began with the phrase,
"We certainly weren't expecting that publicity."
What does that mean?
I smell a rat here.
But, well, I'd rather not go around speculating,
at least not anymore.

Anyway,
knowing that there is a lot of sensitive data floating around the Internet
—possibly including yours—
that malicious actors can leverage for attacks
such as targeted [phishing](../phishing/),
[credential-stuffing](../credential-stuffing/),
and identity theft,
we invite you to keep in mind the following,
for some perhaps already trite,
recommendations:

- If you want,
  you can start by visiting sites like [Have I Been Pwned](https://haveibeenpwned.com/)
  to check if any of your login information
  or other personal details
  are public because of breaches.
  However,
  this database may not yet be up to date with what are supposed to be
  the new breaches within the "MOAB."

- Whether or not you have seen your accounts registered
  as affected on sites like that,
  change your passwords as soon as possible.
  This is something you should,
  in fact, do frequently,
  say, every month.

- If you are still using simple passwords such as "123456" or "password1"
  for any application or service,
  please change them too.
  But hey,
  it is not just changing them from "password1" to "password2"
  or something similar.
  We'll not get tired of repeating this:
  create passwords or passphrases with more than 12 characters,
  using at least one uppercase letter, one lowercase letter,
  one symbol and one number.

- Don't use the same password across different applications.
  If your trouble is you have many passwords to remember,
  it'd be better you use a password manager such as 1Password.

- Enable two-factor or multi-factor authentication.
  Thanks to this,
  if malicious actors have one of your credentials,
  they should also have access to other passwords
  or even any of your personal devices
  in order to cause damage.
  This strategy is like adding another layer of security
  to your daily IT usage.

Are you a Fluid Attacks customer
and have not yet downloaded our VS Code extension?
[We invite you to do so](https://help.fluidattacks.com/portal/en/kb/articles/install-the-vs-code-extension)
to take advantage of automatic vulnerability remediation
through generative AI.
Not familiar with our solutions?
[Start a 21-day free trial right now](https://app.fluidattacks.com/SignUp).
