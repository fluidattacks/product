---
slug: introduction-cybersecurity-aviation-sector/
title: Securing the Ground and Skies
date: 2024-12-06
subtitle: Introduction to cybersecurity in the aviation sector
category: philosophy
tags: cybersecurity, company, risk, trend
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1733510698/blog/introduction-cybersecurity-aviation-sector/cover_introduction_cybersecurity_aviation_sector.webp
alt: Photo by Logan Weaver on Unsplash
description: As part of the transportation industry, the aviation sector, in its massive digital transformation, has faced cybersecurity challenges worth highlighting.
keywords: Transportation Industry, Aviation Industry, Aviation Sector, Aviation Cybersecurity, Airline Cybersecurity, Aircraft Cybersecurity,  Ethical Hacking, Pentesting
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Content Writer and Editor
source: https://unsplash.com/photos/airplane-window-view-of-clouds-during-daytime-hLr0qRmQjnM
---

The transportation industry,
including the aviation sector,
has undergone a dramatic digital transformation over the past few decades.
From advanced flight control systems
to interconnected ground operations,
almost every aspect of modern aviation relies heavily on digital technology.
While this advancement has brought significant benefits,
it has also introduced new risks to the industry:
cyber threats.

Cyberattacks and data breaches occur continuously,
year after year,
in the 21st-century aviation sector.
Just over ten years ago,
for example,
Cathay Pacific Airways experienced one of the most scandalous breaches
in this sector.
[More than nine million customers](https://www.aviationtoday.com/2024/09/19/cybersecurity-in-the-skies/)
around the world
had their personal data affected in an attack
that was only [discovered and revealed in 2018](https://www.zdnet.com/article/cathay-pacific-hit-with-500000-fine-for-customer-data-breach/).
What made this incident possible?
The company's computer systems did not have adequate cybersecurity measures
such as multi-factor authentication, patching of vulnerable software,
and data encryption.

Cyberattacks on the aviation industry can have far-reaching consequences,
ranging from minor application disruptions
to catastrophic failures and data breaches.
As cybercriminals become increasingly sophisticated,
it is imperative for aviation organizations
to prioritize cybersecurity and implement robust prevention,
defense, and response mechanisms.
These companies should commit to keeping their risk exposure
in cybersecurity to a minimum,
just as it has had to do for so many years concerning physical security.

A few decades ago,
cybersecurity was not a primary concern for [the aviation industry](https://www.aviationtoday.com/2024/09/19/cybersecurity-in-the-skies/).
Simple CRC (cyclic redundancy checks) and equivalent mechanisms
were predominantly used to ensure data integrity,
with little consideration for malicious actors.
However,
as advanced technology became more integrated into aviation operations,
the threat landscape began to evolve.
Initial cybersecurity efforts focused on IT and OT systems on the ground.
Nonetheless,
with the increasing connectivity of aircraft systems,
including sensors, biometric readers, and robots,
the attack surface has expanded significantly.
Today,
cyber threats can target everything from ground-based infrastructure
to in-flight systems.

Following Josh Wheeler's remarks,
quoted in the Avionics International newsletter,
[he highlighted that](https://www.aviationtoday.com/2024/09/19/cybersecurity-in-the-skies/)
**altitude does not guarantee security**;
if the Internet can access the aircraft,
the aircraft's data is also accessible to the Internet.
Just like terrestrial cybersecurity,
aviation cybersecurity operates within a complex supply chain
involving "airports, FBOs, trip planners, fuel management systems, caterers,"
and other entities,
all of which can influence overall cyber vigilance.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/penetration-testing-as-a-service/"
title="Get started with Fluid Attacks' Penetration Testing as a Service
right now"
/>
</div>

## Key cybersecurity risks for the aviation sector

The aviation industry should recognize
the multiple and ever-evolving cyber threats
that target it and its customers.
Organizations should prevent, monitor, and mitigate these threats,
which can lead to significant disruptions and damage.
[Hackers often target](https://www.aerospacetestinginternational.com/features/staying-cybersecure-in-the-aerospace-sector.html)
critical systems such as flight history servers,
reservation platforms, ticket booking websites, cabin crew devices,
and flight management systems.

### Ransomware

Ransomware attacks have become a pervasive threat
[across various industries](../citrix-bleed-and-ransomware-attacks/)
but a leading hazard for the aviation sector.
[Through this method](../ransomware/),
cybercriminals encrypt critical systems and data
and then demand a ransom for decryption.
Such attacks can disrupt operations, delay flights,
and compromise sensitive information.
[As reported by Eurocontrol](https://www.eurocontrol.int/publication/eurocontrol-data-snapshot-39-ransomware-groups-targeting-aviations-supply-chain)
(the European Organisation for the Safety of Air Navigation)
based on data from 2021 and 2022,
approximately **2.5 ransomware attacks per week** are reported
in European aviation-related organizations.
(Such value could be higher,
but we must remember that many companies do not like to admit
they were victims of cyberattacks.)

An example of this kind of incident is Embraer's case.
This Brazilian company,
then one of the world's largest airplane makers behind Airbus and Boeing,
was the [victim of a ransomware attack](https://www.zdnet.com/article/hackers-leak-data-from-embraer-worlds-third-largest-airplane-maker/)
in late 2020.
Embraer apparently refused to negotiate with the criminals
and pay the corresponding ransom,
so they decided to leak the company's private files,
including business contracts, employee details, and source code,
on a site within the [dark web](../dark-web/).

There is also the case of airplane parts suppliers, [such as ASCO](https://www.zdnet.com/article/ransomware-halts-production-for-days-at-major-airplane-parts-manufacturer/),
a well-known international company in this field.
In 2019,
ASCO had to interrupt production in factories in four countries,
temporarily sending home more than 70% of its workers
due to a ransomware attack at its plant in Belgium.
[More recently](https://www.globalair.com/articles/continental-aerospace-latest-to-be-hacked-as-aviation-cyberattacks-rise?id=6999),
Continental Aerospace Technologies,
an aircraft engine manufacturer in Alabama,
was hit by a cyberattack attributed to PLAY ransomware.
Although no impact details were provided,
it is assumed that manufacturing operations were disrupted
and intellectual property compromised.

### DDoS attacks

Distributed Denial-of-Service (DDoS) attacks
can overwhelm an organization's network resources,
making it difficult or impossible for legitimate users to access services.
While DDoS attacks may not directly compromise sensitive data,
they can **significantly disrupt operations** like website access
and online booking systems.
Recent attacks on airport websites
underscore the potential impact of DDoS attacks on the aviation industry.

[In 2022](https://www.npr.org/2022/10/10/1127902795/airport-killnet-cyberattack-hacker-russia),
the Russian gang KillNet allegedly temporarily took down the services
of several U.S. airport websites,
apparently without affecting internal airport systems or flight operations.
More recently,
there was one DDoS attack [against John Lennon Airport in Liverpool](https://thecyberexpress.com/liverpool-airport-cyberattack/),
apparently perpetrated by a group called Anonymous Collective.
This gang appeared to be doing so in retaliation for the UK's support
for Israel in its attacks on Palestine
and managed to disrupt user access to the airport's website.

In addition to attacking airlines' websites,
threat actors can also impact more critical systems with this type of attack.
[For example](https://www.zdnet.com/article/hackers-prompt-flight-cancellation-at-polish-airport/),
in 2015, the Polish airline LOT experienced a cyberattack
that compromised its flight management systems for several hours.
The incident resulted in flight delays and cancellations,
affecting approximately 1,400 passengers.
This case underscores the critical need for resilient IT infrastructure
and effective incident response plans.

### State-sponsored attacks

While within this set of key risks,
we can include attack methods such as the two previously described,
contemporary news regarding the aviation and aerospace industries invites us
to highlight them separately.

Nation-state actors pose a significant threat to the aviation industry.
These advanced adversaries may target critical infrastructure,
intellectual property, and sensitive information.
Far from being financially motivated like independent criminal groups,
these attackers intend to gain valuable intelligence,
disrupt operations, or even sabotage aircraft
by compromising aviation systems.
[For instance](https://www.zdnet.com/article/china-targets-aviation-industry-to-spy-and-steal-industry-secrets/),
some Chinese hacking groups,
in particular,
have been linked to targeted attacks on the aviation sector.
They aim to steal sensitive information to support their government
and military departments,
even their aviation markets,
in advancing technological capabilities.
However,
we should remember that other state-sponsored attacks can signify new forms
of **cyber terrorism**.

### Supply chain attacks

The aviation industry relies on a complex global supply chain
involving numerous manufacturers, suppliers, and service providers.
A cyberattack on a single component supplier can allow attackers
to introduce malicious software and carry out more complex attacks
against other suppliers,
generating significant consequences.
As the Aerospace Industries Association [suggested in a recent report](https://www.aia-aerospace.org/publications/supply-chain-cybersecurity-recommendations-report/),
"Across aviation, attacks can impact nearly everything in the supply chain."
This includes the data used to design and build structures,
the electronic components (e.g., software and firmware),
and the systems used to manufacture
both electronic and non-electronic components.
Nowadays,
the importance of [securing the entire supply chain](../software-supply-chain-security/)
to mitigate these risks is indisputable.

### In-flight attacks

The increasing connectivity of aircraft
has introduced new cybersecurity challenges.
In-flight Wi-Fi and other wireless technologies can expose aircraft systems
to potential cyber threats.
Additionally, passenger devices, such as laptops and smartphones,
can serve as attack vectors.
In the last decade,
concerns have emerged about the potential **aircraft hacking**.
In 2015,
security researcher Chris Roberts
allegedly exposed vulnerabilities in commercial aircraft systems
through a supposed "mid-flight hacking,"
raising alarms about the possibility of malicious actors
exploiting these weaknesses.

If hackers can hotwire a car,
why not hijack a plane?
[George Avetisov once commented in Forbes](https://www.forbes.com/sites/georgeavetisov/2019/03/19/malware-at-30000-feet-what-the-737-max-says-about-the-state-of-airplane-software-security/),
"If an aircraft's automation software has such high authority
that it can ignore pilot inputs,
one can assume that sophisticated malware could exploit the same issue.
Reinforced cockpit doors are useless if an attacker can manipulate software
to take control of an aircraft remotely."
While the aviation industry has taken steps to address these risks,
it remains a significant challenge.

Since the grounding of the Boeing 737 Max aircraft in [2019](../ceo-bugs/),
an intriguing global conversation about aviation safety has been sparked.
[The investigation into the crashes](https://www.forbes.com/sites/georgeavetisov/2019/03/19/malware-at-30000-feet-what-the-737-max-says-about-the-state-of-airplane-software-security/)
of Ethiopian Airlines Flight 302 and Lion Air Flight JT610
has shifted the focus from traditional security measures
to the complex software systems that power modern aircraft.
A particular area of concern
is the Maneuvering Characteristics Augmentation System (MCAS),
a relatively new technology designed to enhance flight safety.
However,
the rapid deployment of such systems without adequate testing and oversight
raises questions about potential cybersecurity vulnerabilities
and the risk of malicious interference.

[GPS spoofing](https://www.reuters.com/technology/cybersecurity/gps-spoofers-hack-time-commercial-airlines-researchers-say-2024-08-10/)
or [jamming](https://www.reuters.com/business/aerospace-defense/what-is-gps-jamming-why-it-is-problem-aviation-2024-04-30/)
attacks,
which can manipulate aircraft navigation systems,
pose an additional significant risk today.
This technique involves transmitting false GPS signals
to disrupt navigation systems.
By broadcasting stronger signals from the ground than those from satellites,
attackers can trick aircraft and pilots into believing
they are in a different location.
This could lead to [dangerous situations](https://www.dailymail.co.uk/sciencetech/article-13742823/Cyberattack-airline-hack-gps-attack-Russia-Ukraine-Iraq.html),
such as collisions or unintended landings.
While airlines are increasingly aware of this threat,
many still rely heavily on GPS for navigation.
In the event of a GPS spoofing attack,
pilots may be forced to rely on outdated or less precise navigation methods,
potentially compromising flight safety.

## Some practical security measures

Aviation organizations must implement comprehensive cybersecurity strategies
to mitigate the risks posed by cyber threats.
While traditional security measures, such as strong passwords,
multi-factor authentication, robust encryption, software components updating,
firewalls, and antivirus software,
are essential,
additional strategies are required to protect against sophisticated attacks.

- **Network segmentation:**
  By dividing networks into smaller segments,
  organizations can limit the potential impact of a breach.

- **Zero-trust architecture:**
  This security model assumes that no user or device is inherently trustworthy,
  requiring strict verification and authorization.

- **Threat intelligence sharing:**
  Collaborating with other organizations to share threat intelligence
  can help detect and respond to emerging threats.

- **Strong security culture:**
  Fostering a strong security culture among employees is crucial.
  Regular security awareness training can help prevent human error,
  which is often a major factor in cyberattacks.

- **Regular vulnerability scanning and pentesting:**
  Continuous assessments by automated tools and security experts or pentesters
  can identify security vulnerabilities and weaknesses in software
  that should be remediated to contribute to companies' and users' safety.

The aviation industry is a critical digitalized sector
that must prioritize implementing cybersecurity measures
to protect data, operations, and passengers.
This kind of security should be elevated to a board-level concern
in this and other industries,
underscoring its strategic importance.
By placing accountability at the highest level of the organization,
you can guarantee that cybersecurity receives the necessary attention
and resources.
If your company is interested in receiving Fluid Attacks' contribution
to mature in this area through fast and accurate security testing
through automated and manual methods,
do not hesitate to [contact us](../../contact-us/).
