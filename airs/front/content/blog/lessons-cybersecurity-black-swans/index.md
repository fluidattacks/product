---
slug: lessons-cybersecurity-black-swans/
title: Prove You're Definitely Safe!
date: 2024-04-09
subtitle: Lessons learned from black swans
category: opinions
tags: cybersecurity, risk, company, hacking, software
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1712696620/blog/lessons-cybersecurity-black-swans/cover_lessons_cybersecurity_black_swans.webp
alt: Photo by Roy Muz on Unsplash
description: There are also "black swans" in cybersecurity. We can run into one of them at any time, so we must be prepared.
keywords: Black Swans, Black Swan Event, Prediction, Unpredictable Events, Unexpected Events, Cyberattacks, Cybersecurity Investment, Pentesting, Ethical Hacking
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Content Writer and Editor
source: https://unsplash.com/photos/black-and-white-swan-GxcOkcBBJao
---

You can't do that.
You can't prove that you and your company are definitely safe.
Don't make the mistake of assuming that
you are exempt from danger and damage
within this threatening digital jungle.
There's no way to absolutely guarantee that you won't
—indeed, let's say better, we won't—
be harmed or affected by a cyber threat in the future.

You may wonder,
"How could someone from a cybersecurity company be telling me this?"
**Honesty**, **humility**, and **transparency** are the answer.
These principles,
among others,
govern our efforts at Fluid Attacks.
That's why,
without mincing words,
we recognize that neither we nor any other company can or should commit
to the fact that your applications and other technology products
will *definitely* be safe.

I made up my mind to write this post
after reading a concise essay by author Tom Standage
in the book *[This Will Make You Smarter](https://www.amazon.com/This-Will-Make-You-Smarter/dp/0062109391)*,
edited by John Brockman and published in 2012.
The essay bears the name "You Can Show That Something Is Definitely Dangerous
but Not That It's Definitely Safe."
Right away,
this title piqued my curiosity,
and I associated the content with what I had recently written
about the [rationale for investing in cybersecurity](../justify-investment-in-cybersecurity/).

While Standage first pointed out the impossibility of determining that
a certain technology will not generate harm,
this is an assertion
based on **the general impossibility of proving a negative**.
Therefore,
this also applies to the statement I made at the beginning of this post,
and, indeed, we can use it for an infinite number of propositions.
Hence,
for example,
we can say there is no way to prove that a god or several gods do *not* exist.
Thus,
for many of us,
even if we as humanity haven't found any evidence of their existence
supported by science,
we don't have sufficient reason to say that deities do not exist.
(While there undoubtedly are refutations for stances like this,
I don't intend to dig into logical-philosophical discussions here).

Something similar happened centuries ago
when the belief was held,
an absolute truth for many,
that **black swans did not exist**.
[As Tim Low states](https://www.australiangeographic.com.au/topics/wildlife/2016/07/black-swan-the-impossible-bird/),
in medieval Europe, curiously, unicorns had more credibility than black swans.
The latter were better symbols of the impossible than the former.
It was then a totally unexpected event
that around the end of the 17th century,
European explorers found black swans in Australian territory.
Thus,
a simple event acted as sufficient evidence
to refute such a proposition of impossibility
(which there was no way to prove).

So,
**it is better we don't declare something as impossible**.
The mere fact
that there have so far been vast numbers of consistent events or phenomena
usually leads us to that erroneous induction.
As Standage said,
"Absence of evidence is not evidence of absence."
It is preferable to be skeptical
and talk about realities that are difficult to predict.
However,
many people do not know or simply do not apply this way of thinking
and seeing the world.
I made this clear in the blog post I referenced at the beginning
when I formulated some words attributable to CEOs of many companies:
"Nothing bad has happened to us so far, and surely nothing will ever happen."

## Black swans in cybersecurity

Another day goes by for a company without being the victim of a cyberattack,
and it adds up to proof that nothing of the sort will happen the next day.
Overconfidence is firmly entrenched.
They do not see the need to invest
if they have not been attacked for some time without investing in security.
If they have received attacks but none have been catastrophic,
supposedly because of what they have invested so far,
they do not see the need to invest further.
They have adhered to the hitherto experienced and known reality pattern
and come to affirm that nothing bad or worse will ever happen.
Nevertheless,
**black swans also appear on the cybersecurity scene**.

In different fields of human action where all kinds of risks exist,
as is the case with cybersecurity,
the metaphor of black swans is used.
Black swans,
in this case,
usually refer to **highly unexpected and unpredictable events**
that can occur suddenly
and bring with them significantly serious consequences.
A typical example is 9/11,
but in cybersecurity,
we can mention the [WannaCry ransomware attack](../10-biggest-ransomware-attacks/)
and the [SolarWinds supply chain attack](../solarwinds-attack/),
among others.

Interestingly,
many of these catastrophic and unexpected events,
after they have manifested themselves,
are viewed in hindsight as preventable.
Some of the people affected may end up saying something like,
"If we had paid more attention and collected more data,
we could have prevented this from happening."
This prompts me to recall the following words of theoretical physicist
[Richard Feynman](https://www.inf.fu-berlin.de/lehre/pmo/eng/Feynman-Uncertainty.pdf):

<quote-box>

It is necessary and true that
all of the things we say in science,
all of the conclusions, are uncertain,
because they are only conclusions.
They are guesses as to what is going to happen,
and you cannot know what will happen,
because you have not made the most complete experiments.

</quote-box>

**Experimentation**,
a substantial element of the scientific method,
is also instrumental in cybersecurity.
Generating hypotheses and possible risk scenarios
and testing them on an ongoing basis
can greatly help prevent,
even if only some,
"black swan events."

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/penetration-testing-as-a-service/"
title="Get started with Fluid Attacks' Penetration Testing as a Service
right now"
/>
</div>

## Recommendations to avoid facing black swans

Threats and attacks in the cyber universe are constantly on the rise
in number and complexity.
It is now a pathetic habit to see news of successful cyberattacks
in almost every country and industry around the world.
Every day,
we notice devastating effects on governments, banks, healthcare entities,
technology companies, etc.
The simple fact of contemplating this reality,
even if not in the flesh,
should help us to recognize that **we can all be vulnerable**,
from the biggest and most powerful to the smallest and weakest.

Let's not wait on the sidelines for something devastating to happen to us.
**Let's prepare for the worst now**.
Let's strive to identify multiple potential crises
and disastrous scenarios
in which our security and that of our customers or users could be affected.
No matter how improbable they may be.
Let's try as much as possible to reduce uncertainties,
but without pretending we could foresee everything with certainty.
Let's strive to understand
and vastly reduce our attack surfaces and risk exposure.
And let's develop sound prevention, defense, response and resilience plans.

On the one hand,
we can forge robust contribution partnerships
between companies or organizations,
recognizing that due to interconnectivity issues in digital infrastructures,
attacks on one can have ripple effects on others.
These teams can identify and share from their experiences
and those of others
common and uncommon risks and best practices in cybersecurity.
Let's share, discreetly, of course, knowledge about potential threats
and post-analysis of what were successful cyberattacks.
This would be a valuable, ongoing **mutual support through lessons learned**.

On the other hand,
let's not be comfortable with acquiring and deploying security solutions
that can only detect and/or address previously known threats.
(Such as what happens through assessments with automated tools
limited to detecting vulnerabilities that are part of their databases).
Let's not be satisfied with conventional strategies.
Let's not remain without testing what is out of the ordinary.
**We should diversify our investment in cybersecurity**
and look for all the entry points that cybercriminals could exploit
or attack vectors they could employ.
(Something that can be achieved through [ethical hacking](../what-is-ethical-hacking/).)
Let's keep up with the overwhelming evolution of the threat landscape.

**Exceptional threats need to be addressed with exceptional solutions**.
Such is the case of Fluid Attacks' all-in-one solution:
**[Continuous Hacking](../../services/continuous-hacking/)**.
With our assortment of vulnerability detection methods
and vulnerability management and remediation capabilities,
we help reduce uncertainties.
While we are prudent to recognize that we cannot,
nor can anyone else,
prove to you that your company will not be the victim of a violent cyberattack,
we certainly help to reduce those odds to a great extent.
Any questions?
[Contact us](../../contact-us/).
