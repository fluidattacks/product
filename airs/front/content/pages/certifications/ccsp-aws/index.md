---
slug: certifications/ccsp-aws/
title: Certified Cloud Security Practitioner – AWS
description: Our team of ethical hackers proudly holds the CCSP-AWS (Certified Cloud Security Practitioner – AWS) certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, Ccsp Aws
certificationlogo: logo-ccsp-aws
alt: Logo CCSP AWS
certification: yes
certificationid: 54
---

[CCSP-AWS](https://secops.group/pentesting-exams/certified-cloud-security-practitioner-aws-ccsp-aws/)
is a certification created by The SecOps Group.
Candidates have to prove their knowledge
on topics related to Amazon Web Services cloud security in a written exam.
Its questions are based on real-world scenarios.
They cover mainly the development of Amazon Web Services infrastructure systems
following security best practices.
