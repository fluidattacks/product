---
slug: certifications/osed/
title: OffSec Exploit Developer
description: Our team of ethical hackers proudly holds the OSED (OffSec Exploit Developer) certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, OSED
certificationlogo: logo-osed
alt: Logo osed
certification: yes
certificationid: 4
---

[OSED](https://www.offsec.com/courses/exp-301/)
is an exploit development certification,
created by OffSec.
It is one of three certifications
that make up the new OSCE3 certification,
along with the OSWE for web application security
and the OSEP for penetration testing.
In an intense 48-hour exam,
professionals prove they can find bugs in a binary application
and build an exploit from scratch,
craft exploits for common security mitigations
and use the technique to bypass data execution.
