---
slug: certifications/osmr/
title: OffSec macOS Researcher
description: Our team of ethical hackers proudly holds the OSMR (OffSec macOS Researcher) certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, OSMR
certificationlogo: logo-osmr
alt: Logo OSMR
certification: yes
certificationid: 9
---

[OSMR](https://www.offsec.com/courses/exp-312/) is a professional certification
in penetration testing for macOS
created by OffSec.
To obtain it,
applicants must complete a course on macOS functionality and security
and a hands-on exam.
This is a complicated 48-hour proctored test
in which complex vulnerabilities must be identified, analyzed, and exploited
within a simulated real-world environment of said operating system.
