---
slug: certifications/ejpt/
title: Junior Penetration Tester
description: Our team of ethical hackers proudly holds the eJPT (Junior Penetration Tester) certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, Ejpt
certificationlogo: logo-ejpt
alt: Logo eJPT
certification: yes
certificationid: 22
---

[eJPT](https://security.ine.com/certifications/ejpt-certification/)
is a certification created by INE Security.
It certifies
that the individual has the essential skills
to conduct [penetration testing](../../blog/penetration-testing/).
The exam is entirely hands-on,
modeled after real-world scenarios.
It demands actions such as vulnerability assessment of networks
and web applications,
manual exploitation of the latter
and using Metasploit to perform attacks.
