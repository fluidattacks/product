---
slug: certifications/htb-cpts/
title: HTB Certified Penetration Testing Specialist
description: Our team of ethical hackers proudly holds the HTB Certified Penetration Testing Specialist certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, Htb Cpts
certificationlogo: logo-htb-cpts
alt: Logo HTB Certified Penetration Testing Specialist
certification: yes
certificationid: 45
---

[HTB CPTS](https://academy.hackthebox.com/preview/certifications/htb-certified-penetration-testing-specialist)
is a certification offered by Hack The Box.
It is focused on challenging candidates' pentesting skills
in situations close to the current threat landscape.
Therefore,
the exam requires performing web,
internal,
external
and Active Directory attacks,
among others,
proving a deep knowledge of tools and tactics.
It also requires creating a report with the vulnerabilities found
and remediation advice.
