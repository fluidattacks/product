---
slug: certifications/casa/
title: Certified API Security Analyst
description: Our team of ethical hackers proudly holds the CASA (Certified API Security Analyst) certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, CASA
certificationlogo: logo-casa
alt: Logo CASA
certification: yes
certificationid: 59
---

[CASA](https://www.apisecuniversity.com/courses/certified-api-security-analyst-exam)
is a certification created by APIsec University.
To earn it,
candidates must demonstrate their knowledge of API security threats,
risks, and best practices.
They must correctly answer more than 80% of an exam
with 100 multiple-choice questions.
It is recommended to previously complete the course
OWASP API Security and Beyond!
