---
slug: certifications/gpen/
title: GIAC Certified Penetration Tester
description: Our team of ethical hackers proudly holds the GPEN (GIAC Certified Penetration Tester) certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, GPEN
certificationlogo: logo-gpen
alt: Logo GPEN
certification: yes
certificationid: 11
---

[GPEN](https://www.giac.org/certifications/penetration-tester-gpen/)
is a certification
issued by GIAC Certifications.
Candidates must pass an exam
proving their advanced knowledge on the phases of [pentesting](../../blog/penetration-testing/),
vulnerability scanning and techniques
including password attacks,
attacks on Azure environments,
Windows privilege escalation attacks
and attacks against Active Directory.
Completing the exam questions requires hands-on skills
and the performance of tasks mimicking reality.
