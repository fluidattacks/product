---
slug: certifications/ecpptv2/
title: Certified Professional Penetration Tester
description: Our team of ethical hackers proudly holds the eCPPTv2 (Certified Professional Penetration Tester) certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, Ecppt
certificationlogo: logo-ecpptv2
alt: Logo eCPPTv2
certification: yes
certificationid: 17
---

[eCPPTv2](https://security.ine.com/certifications/ecppt-certification/)
is a certification created by INE Security.
Candidates must prove their skills
in using different methodologies for a thorough penetration test
in a practical exam modeled after a real-world corporate network.
The targets of the assessments and attacks include Windows and Linux systems
and web applications.
Further,
the individual has to write a report
that includes recommendations for remediation.
