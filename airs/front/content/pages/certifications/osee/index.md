---
slug: certifications/osee/
title: Offensive Security Exploitation Expert
description: Our team of ethical hackers proudly holds the OSEE (Offensive Security Exploitation Expert) certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, OSEE
certificationlogo: logo-osee
alt: Logo OSEE
certification: yes
certificationid: 1
---

[OSEE](https://www.offsec.com/courses/exp-401/)
is the most complicated exploit development certification.
It was created by OffSec.
The exam evaluates
the content of the Advanced Windows Exploitation course (EXP-401),
as well as professionals' lateral thinking
and adaptability to challenges.
They have 72 hours to perform a thorough [pentest](../../blog/penetration-testing/)
on vulnerable software
and report it with sufficient detail,
including the exploit methods employed.
