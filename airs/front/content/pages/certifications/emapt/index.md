---
slug: certifications/emapt/
title: Mobile Application Penetration Tester
description: Our team of ethical hackers proudly holds the eMAPT (Mobile Application Penetration Tester) certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, EMAPT
certificationlogo: logo-emapt
alt: Logo eMAPT
certification: yes
certificationid: 18
---

[eMAPT](https://security.ine.com/certifications/emapt-certification/)
is a certification created by INE Security.
This certification is intended to be achieved
by cybersecurity experts
with advanced mobile application security knowledge.
It evaluates the candidate's skills
to perform an expert-level analysis and penetration test.
To do so,
they must perform manual exploitation,
reverse engineering and decryption
in two Android applications.
