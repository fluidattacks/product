---
slug: certifications/cmpen-android/
title: Certified Mobile Pentester (CMPen) – Android
description: Our team of ethical hackers proudly holds the Certified Mobile Pentester (CMPen) - Android certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, Cmpen Android
certificationlogo: logo-cmpen-android
alt: Logo CMPen Android
certification: yes
certificationid: 51
---

[CMPen - Android](https://secops.group/pentesting-exams/certified-mobile-pentester-cmpen-android/)
is a certification created by The SecOps Group.
Candidates have to prove their knowledge
on key concepts of the security of Android applications
in a practical exam.
Mainly,
this exam requires performing SAST and DAST
to identify various vulnerabilities
(e.g., OWASP Mobile Top 10),
exploiting them
and obtaining flags.
