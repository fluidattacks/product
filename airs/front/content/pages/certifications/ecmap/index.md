---
slug: certifications/ecmap/
title: Certified Malware Analysis Professional
description: Our team of ethical hackers proudly holds the eCMAP (Certified Malware Analysis Professional) certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, ECMAP
certificationlogo: logo-ecmap
alt: Logo eCMAP
certification: yes
certificationid: 21
---

eCMAP is a now retired certification created by INE Security.
It was the most practical and professionally-oriented certification
in malware analysis.
In order to achieve it,
candidates had to analyze a malware sample,
demonstrate its functionality,
write a signature that can be used
to detect the malware in other systems or networks,
and provide a detailed professional report.
