---
slug: certifications/osep/
title: OffSec Experienced Pentester
description: Our team of ethical hackers proudly holds the OSEP (OffSec Experienced Pentester) certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, OSEP
certificationlogo: logo-osep
alt: Logo OSEP
certification: yes
certificationid: 5
---

[OSEP](https://www.offsec.com/courses/pen-300/)
is a certification created by OffSec.
Candidates have to prove
they are skilled in advanced [penetration testing](../../blog/penetration-testing/)
techniques.
The challenges include bypassing security mechanisms
and evading defenses
while executing advanced organized attacks
in a focused manner.
