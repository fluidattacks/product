---
slug: certifications/oscp/
title: OffSec Certified Professional
description: Our team of ethical hackers proudly holds the OSCP (OffSec Certified Professional) certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, OSCP
certificationlogo: logo-oscp
alt: Logo OSCP
certification: yes
certificationid: 7
---

[OSCP](https://www.offsec.com/courses/pen-200/)
is a professional certification in ethical hacking
developed by OffSec.
It is the first fully hands-on offensive information security certification
in the world.
It requires the professionals to prove that
they have a clear understanding of the [penetration testing](../../blog/penetration-testing/)
process and lifecycle
through an arduous 24-hour exam.
