---
slug: certifications/crtl/
title: Red Team Lead
description: Our team of ethical hackers proudly holds the Red Team Lead certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, Red Team Lead
certificationlogo: logo-crtl
alt: Logo CRTL
certification: yes
certificationid: 23
---

[Red Team Lead](https://eu.badgr.com/public/badges/gwM0NmzISLyqmcqDScDX3w)
is a certification created by Zero-Point Security.
To earn it,
candidates must obtain all four flags on a given set of machines
in an AD environment
and submit them for scoring.
They have 72 hours or five days to complete this.
The exam demands skill mainly at building versatile,
resilient and secure C2 infrastructure,
writing offensive tooling
and bypassing security.
