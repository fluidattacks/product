---
slug: certifications/gmob/
title: GIAC Mobile Device Security Analyst
description: Our team of ethical hackers proudly holds the GMOB (GIAC Mobile Device Security Analyst) certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, GMOB
certificationlogo: logo-gmob
alt: Logo GMOB
certification: yes
certificationid: 12
---

[GMOB](https://www.giac.org/certifications/mobile-device-security-analyst-gmob/)
is a certification issued by GIAC Certifications.
Candidates must pass an exam
demonstrating their knowledge of Android and iOS device management,
mobile application security assessment
and risk mitigation in relation to malware and data loss.
This proctored exam consists of 75 questions and a time limit of 2 hours;
the minimum passing score is 71%.
