---
slug: certifications/osce3/
title: OffSec Certified Expert 3
description: Our team of ethical hackers proudly holds the OSCE3 (OffSec Certified Expert 3) certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, OSCE
certificationlogo: logo-osce-3
alt: Logo OSCE3
certification: yes
certificationid: 2
---

[OSCE3](https://help.offensive-security.com/hc/en-us/articles/4403282452628-What-is-OSCE3-)
is a certification created by OffSec.
It is awarded to individuals who have gained the OSED,
OSWE and OSEP certifications.
This means that candidates have to prove
they can build exploits from scratch,
identify and exploit vulnerabilities in web apps,
and conduct [penetration testing](../../blog/penetration-testing/)
against hardened systems,
respectively.
