---
slug: advisories/pires/
title: Online Examination System v1.0 - Multiple Unauthenticated SQL Injections (SQLi)
authors: Andres Roldan
writer: aroldan
codename: Pires
product: Online Examination System v1.0
date: 2023-11-01 12:00 COT
cveid: CVE-2023-45111
severity: 9.8
description: Online Examination System v1.0 - Multiple Unauthenticated SQL Injections (SQLi)
keywords: Fluid Attacks, Security, Vulnerabilities, SQLi, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Online Examination System v1.0 - Multiple Unauthenticated SQL Injections (SQLi) |
| **Code name** | [Pires](https://en.wikipedia.org/wiki/Maria_Jo%C3%A3o_Pires) |
| **Product** | Online Examination System |
| **Vendor** | Projectworlds Pvt. Limited |
| **Affected versions** | Version 1.0 |
| **State** | Public |
| **Release date** | 2023-11-01 |

## Vulnerabilities

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Unauthenticated SQL Injections (SQLi) |
| **Rule** | [146. SQL Injection](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-146) |
| **Remote** | Yes |
| **CVSSv3 Vector** | CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H |
| **CVSSv3 Base Score** | 9.8 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2023-45111](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45111) |

## Description

Online Examination System v1.0 is vulnerable to
an Unauthenticated SQL Injection vulnerability.
The 'email' parameter of the feed.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$ref=@$_GET['q'];
$name = $_POST['name'];
$email = $_POST['email'];
$subject = $_POST['subject'];
...
$feedback = $_POST['feedback'];
$q=mysqli_query($con,"INSERT INTO feedback VALUES  ('$id' , '$name', '$email' , '$subject', '$feedback' , '$date' , '$time')")or die ("Error");
```

## Our security policy

We have reserved the ID CVE-2023-45111
to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Online Examination System v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://projectworlds.in/>

## Timeline

<time-lapse
discovered="2023-10-04"
contacted="2023-10-04"
disclosure="2023-11-01">
</time-lapse>
