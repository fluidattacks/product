---
slug: advisories/perahia/
title: Online Notice Board System v1.0 - SQLi
authors: Andres Roldan
writer: aroldan
codename: perahia
product: Online Notice Board System v1.0
date: 2023-12-11 12:00 COT
cveid: CVE-2023-50743,CVE-2023-50752,CVE-2023-50753
severity: 9.8
description: Online Notice Board System v1.0 - Multiple Unauthenticated SQL Injections (SQLi)
keywords: Fluid Attacks, Security, Vulnerabilities, SQLi, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Online Notice Board System v1.0 - Multiple Unauthenticated SQL Injections (SQLi) |
| **Code name** | [Perahia](https://en.wikipedia.org/wiki/Murray_Perahia) |
| **Product** | Online Notice Board System |
| **Vendor** | Kashipara Group |
| **Affected versions** | Version 1.0 |
| **State** | Public |
| **Release date** | 2024-01-02 |

## Vulnerabilities

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Unauthenticated SQL Injections (SQLi) |
| **Rule** | [146. SQL Injection](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-146) |
| **Remote** | Yes |
| **CVSSv3 Vector** | CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H |
| **CVSSv3 Base Score** | 9.8 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2023-50743](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-50743), [CVE-2023-50752](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-50752), [CVE-2023-50753](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-50753) |

## Description

Online Notice Board System v1.0 is vulnerable to
multiple Unauthenticated SQL Injection vulnerabilities.

## Vulnerabilities

### CVE-2023-50743

The 'dd' parameter of the registration.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
extract($_POST);
...
$dob=$yy."-".$mm."-".$dd;
...
$query="insert into user values('','$n','$e','$pass','$mob','$gen','$hob','$imageName','$dob',now())";
mysqli_query($conn,$query);
```

### CVE-2023-50752

The 'e' parameter of the login.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
extract($_POST);
if(isset($save))
{

    if($e=="" || $p=="")
    {
    $err="<font color='red'>fill all the fileds first</font>";
    }
    else
    {
$pass=md5($p);

$sql=mysqli_query($conn,"select * from user where email='$e' and pass='$pass'");
```

### CVE-2023-50753

The 'dd' parameter of the user/update_profile.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
extract($_POST);
if(isset($update))
{
//dob
$dob=$yy."-".$mm."-".$dd;
//hobbies
$hob=implode(",",$hob);

$query="update user set name='$n',mobile='$mob',gender='$gen',hobbies='$hob',dob='$dob' where email='".$_SESSION['user']."'";
```

## Our security policy

We have reserved the IDs CVE-2023-50743,
CVE-2023-50752 and CVE-2023-50753 to
refer to these issues from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Online Notice Board System v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://www.kashipara.com/>

## Timeline

<time-lapse
discovered="2023-12-11"
contacted="2023-12-11"
disclosure="2024-01-02">
</time-lapse>
