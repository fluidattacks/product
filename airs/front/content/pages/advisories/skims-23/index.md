---
slug: advisories/skims-23/
title: PixelYourSite - Insecure deserialization
authors: Andres Roldan
writer: aroldan
codename: skims-23
product: PixelYourSite 10.1.1.1
date: 2025-01-03 12:00 COT
cveid: CVE-2025-0769
description: PixelYourSite 10.1.1.1 - Insecure deserialization Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, CVE, SAST
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | PixelYourSite- Your smart PIXEL (TAG) and API Manager 10.1.1.1 - Insecure deserialization |
| **Code name** | skims-23 |
| **Product** | PixelYourSite- Your smart PIXEL (TAG) and API Manager |
| **Affected versions** | Version 10.1.1.1 |
| **State** | Public |
| **Release date** | 2025-02-28 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Insecure deserialization |
| **Rule** | [Insecure deserialization](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-096) |
| **Remote** | No |
| **CVSSv4 Vector** | CVSS:4.0/AV:N/AC:H/AT:N/PR:N/UI:N/VC:N/VI:L/VA:L/SC:N/SI:N/SA:N/E:U |
| **Exploit available** | No |
| **CVE ID(s)** | [CVE-2025-0769](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-0769) |

## Description

PixelYourSite- Your smart PIXEL (TAG)
and API Manager
10.1.1.1
was found to be vulnerable.
Unvalidated user input is used directly
in an unserialize function in
myapp/modules/facebook/facebook-server-a
sync-task.php.

## Vulnerability

Skims by Fluid Attacks discovered a
Insecure deserialization
in
PixelYourSite- Your smart PIXEL (TAG)
and API Manager
10.1.1.1.
The following is the output of the tool:

### Skims output

```powershell
  24 |
  25 |         } catch (xception $ex) {
  26 |             error_log($ex);
  27 |         }
  28 |
  29 |         return array();
  30 |     }
  31 |
  32 |     protected function run_action() {
  33 |         try {
> 34 |             $data = unserialize(base64_decode($_POST['data']));
  35 |
  36 |             $events = is_array($data[0]) ? $data[0] : $data ;
  37 |             if (empty($events)) {
  38 |                 return;
  39 |             }
  40 |
  41 |             foreach ($events as $event) {
  42 |                 FacebookServer()->sendEvent($event[""pixelIds""],$event[""event""]);
  43 |             }
  44 |
     ^ Col 0
```

## Our security policy

We have reserved the ID CVE-2025-0769 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: PixelYourSite- Your smart PIXEL (TAG)
  and API Manager 10.1.1.1

## Mitigation

The vendor released the following versions
with the patch: PixelYourSite Free: 10.1.1.2
and PixelYourSite Pro: 11.2.2.3

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team using
[Skims](../../plans/#essential)

## Timeline

<time-lapse
  discovered="2025-01-03"
  contacted="2025-02-27"
  replied="2025-02-27"
  confirmed="2025-02-27"
  patched="2025-02-28"
  disclosure="2025-02-28">
</time-lapse>
