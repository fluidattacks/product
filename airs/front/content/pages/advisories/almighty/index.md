---
slug: advisories/almighty/
title: Dev Blog v1.0 - ATO
authors: Carlos Bello
writer: cbello
codename: almighty
product: Dev Blog v1.0 - ATO
date: 2023-11-15 12:00 COT
cveid: CVE-2023-6144
severity: 9.1
description: Dev Blog             -            Account Takeover
keywords: Fluid Attacks, Security, Vulnerabilities, Account Takeover, Dev Blog, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Dev Blog v1.0 - Account Takeover"
    code="[Almighty](https://es.wikipedia.org/wiki/Almighty)"
    product="Dev Blog"
    affected-versions="v1.0"
    fixed-versions=""
    state="Public"
    release="2023-04-10">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Account Takeover (ATO)"
    rule="[417. Account Takeover](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-417)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:N"
    score="9.1"
    available="Yes"
    id="[CVE-2023-6144](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-6144)">
</vulnerability-table>

## Description

Dev blog v1.0 allows to exploit an account takeover through the "user"
cookie. With this, an attacker can access any user's session just by
knowing their username.

## Vulnerability

An account takeover has been identified in Dev blog, which allows an attacker
to hijack the session of arbitrary users as long as he knows the respective
username of the victim.

## Evidence of exploitation

<iframe src="https://tinyurl.com/ysdf7r6r"
width="835" height="504" frameborder="0" title="XSS-Devblog"
webkitallowfullscreen mozallowfullscreen allowfullscreen controls
loading="lazy"></iframe>

## Our security policy

We have reserved the ID CVE-2023-6144 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Dev Blog v1.0

* Operating System: MacOS

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by [Carlos
Bello](https://www.linkedin.com/in/carlos-andres-bello) from Fluid Attacks'
Offensive Team.

## References

**Vendor page** <https://github.com/Armanidrisi/devblog/>

## Timeline

<time-lapse
  discovered="2023-11-14"
  contacted="2023-11-14"
  replied=""
  confirmed="2023-11-14"
  patched=""
  disclosure="2023-11-15">
</time-lapse>
