---
slug: advisories/skims-8/
title: Ad Inserter - Reflected cross-site scripting (XSS)
authors: Andres Roldan
writer: aroldan
codename: skims-8
product: Ad Inserter - Ad Manager and AdSense Ads 2.8.0
date: 2024-12-06 12:00 COT
cveid: CVE-2025-22623
description: Ad Inserter - Ad Manager and AdSense Ads 2.8.0 - Reflected cross-site scripting (XSS) Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, CVE, SAST
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Ad Inserter - Ad Manager and AdSense Ads 2.8.0 - Reflected cross-site scripting (XSS) |
| **Code name** | skims-8 |
| **Product** | Ad Inserter - Ad Manager and AdSense Ads |
| **Affected versions** | Version 2.8.0 |
| **State** | Public |
| **Release date** | 2024-03-05 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Reflected cross-site scripting (XSS) |
| **Rule** | [Reflected cross-site scripting (XSS)](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-008) |
| **Remote** | No |
| **CVSSv4 Vector** | CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:A/VC:H/VI:L/VA:L/SC:L/SI:L/SA:L/E:U |
| **Exploit available** | No |
| **CVE ID(s)** | [CVE-2025-22623](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-22623) |

## Description

Ad Inserter - Ad Manager and AdSense Ads
2.8.0
was found to be vulnerable.
The web application dynamically
generates web content without
validating the source of the
potentially untrusted data in
myapp/includes/dst/dst.php.

## Vulnerability

Skims by Fluid Attacks discovered a
Reflected cross-site scripting (XSS)
in
Ad Inserter - Ad Manager and AdSense Ads
2.8.0.
The following is the output of the tool:

### Skims output

```powershell
  1534 |         $this->action = self::DST_ACTION_OPTIN_NEWSLETTER;
  1535 |         $this->update (true);
  1536 |       } elseif ($action == '-no') {
  1537 |         $this->set_use_email (false);
  1538 |         $this->action = self::DST_ACTION_OPTIN_NO_NEWSLETTER;
  1539 |         $this->update (true);
  1540 |       }
  1541 |     }
  1542 |
  1543 |     elseif (isset ($_POST [""slug""]) && isset ($_POST [""notice-check""])) {
> 1544 |       echo $_POST [""notice-check""];
  1545 |     }
  1546 |
  1547 |     elseif (isset ($_POST [""slug""]) && isset ($_POST [""values""]) && isset ($_POST ['details'])) {
  1548 |       $values = json_encode (wp_unslash ($_POST ['values']));
  1549 |       $this->set_deactivation_reason ($values);
  1550 |
  1551 |       $details = sanitize_text_field (wp_unslash ($_POST ['details']));
  1552 |       $this->set_deactivation_details ($details);
  1553 |
  1554 |       if (isset ($_POST [""hide_form""])) {
       ^ Col 0
```

## Our security policy

We have reserved the ID CVE-2025-22623 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Ad Inserter - Ad Manager and AdSense Ads
  2.8.0

## Mitigation

The vendor released the version 2.8.1 with a fix
for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team using
[Skims](../../plans/#essential)

## Timeline

<time-lapse
  discovered="2024-12-06"
  contacted="2025-02-11"
  replied="2025-02-12"
  confirmed="2025-02-12"
  patched="2025-03-03"
  disclosure="2025-03-05">
</time-lapse>
