---
slug: advisories/argerich/
title: Online Examination System v1.0 - Multiple Authenticated SQL Injections (SQLi)
authors: Andres Roldan
writer: aroldan
codename: Argerich
product: Online Examination System v1.0
date: 2023-11-02 12:00 COT
cveid: CVE-2023-45115,CVE-2023-45116,CVE-2023-45117,CVE-2023-45118,CVE-2023-45119,CVE-2023-45120,CVE-2023-45121
severity: 8.8
description: Online Examination System v1.0 - Multiple Authenticated SQL Injections (SQLi)
keywords: Fluid Attacks, Security, Vulnerabilities, SQLi, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Online Examination System v1.0 - Multiple Authenticated SQL Injections (SQLi) |
| **Code name** | [Argerich](https://en.wikipedia.org/wiki/Martha_Argerich) |
| **Product** | Online Examination System |
| **Vendor** | Projectworlds Pvt. Limited |
| **Affected versions** | Version 1.0 |
| **State** | Public |
| **Release date** | 2023-11-02 |

## Vulnerabilities

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Authenticated SQL Injections (SQLi) |
| **Rule** | [146. SQL Injection](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-146) |
| **Remote** | Yes |
| **CVSSv3 Vector** | CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H |
| **CVSSv3 Base Score** | 8.8 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2023-45115](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45115), [CVE-2023-45116](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45116), [CVE-2023-45117](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45117), [CVE-2023-45118](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45118), [CVE-2023-45119](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45119), [CVE-2023-45120](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45120), [CVE-2023-45121](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45121) |

## Description

Online Examination System v1.0 is vulnerable to
multiple Authenticated SQL Injection vulnerabilities.

## Vulnerabilities

### CVE-2023-45115

The 'ch' parameter of the /update.php?q=addqns resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$ch=@$_GET['ch'];

for($i=1;$i<=$n;$i++)
 {
 $qid=uniqid();
 $qns=$_POST['qns'.$i];
$q3=mysqli_query($con,"INSERT INTO questions VALUES  ('$eid','$qid','$qns' , '$ch' , '$i')");
```

### CVE-2023-45116

The 'demail' parameter of the /update.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$demail=@$_GET['demail'];
$r1 = mysqli_query($con,"DELETE FROM rank WHERE email='$demail' ") or die('Error');
$r2 = mysqli_query($con,"DELETE FROM history WHERE email='$demail' ") or die('Error');
$result = mysqli_query($con,"DELETE FROM user WHERE email='$demail' ") or die('Error');
```

### CVE-2023-45117

The 'eid' parameter of the /update.php?q=rmquiz resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$eid=@$_GET['eid'];
$result = mysqli_query($con,"SELECT * FROM questions WHERE eid='$eid' ") or die('Error');
```

### CVE-2023-45118

The 'fdid' parameter of the /update.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$id=@$_GET['fdid'];
$result = mysqli_query($con,"DELETE FROM feedback WHERE id='$id' ") or die('Error');
```

### CVE-2023-45119

The 'n' parameter of the /update.php?q=quiz resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$sn=@$_GET['n'];
...
$q=mysqli_query($con,"UPDATE `history` SET `score`=$s,`level`=$sn,`sahi`=$r, date= NOW()  WHERE  email = '$email' AND eid = '$eid'")or die('Error124');
```

### CVE-2023-45120

The 'qid' parameter of the /update.php?q=quiz&step=2
resource does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$qid=@$_GET['qid'];
$q=mysqli_query($con,"SELECT * FROM answer WHERE qid='$qid' " );
```

### CVE-2023-45121

The 'desc' parameter of the /update.php?q=addquiz resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$name = $_POST['name'];
$name= ucwords(strtolower($name));
$total = $_POST['total'];
$sahi = $_POST['right'];
$wrong = $_POST['wrong'];
$time = $_POST['time'];
$tag = $_POST['tag'];
$desc = $_POST['desc'];
$id=uniqid();
$q3=mysqli_query($con,"INSERT INTO quiz VALUES  ('$id','$name' , '$sahi' , '$wrong','$total','$time' ,'$desc','$tag', NOW())");
```

## Our security policy

We have reserved the IDs CVE-2023-45115,
CVE-2023-45116, CVE-2023-45117, CVE-2023-45118,
CVE-2023-45119, CVE-2023-45120 and CVE-2023-45121
to refer to these issues from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Online Examination System v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://projectworlds.in/>

## Timeline

<time-lapse
discovered="2023-10-04"
contacted="2023-10-04"
disclosure="2023-11-02">
</time-lapse>
