---
slug: advisories/uchida/
title: Online Examination System v1.0 - Multiple Open Redirects
authors: Andres Roldan
writer: aroldan
codename: Uchida
product: Online Examination System v1.0
date: 2023-11-01 12:00 COT
cveid: CVE-2023-45201,CVE-2023-45202,CVE-2023-45203
severity: 6.1
description: Online Examination System v1.0 - Multiple Open Redirects
keywords: Fluid Attacks, Security, Vulnerabilities, Advisory, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Online Examination System v1.0 - Multiple Open Redirects |
| **Code name** | [Uchida](https://en.wikipedia.org/wiki/Mitsuko_Uchida) |
| **Product** | Online Examination System |
| **Vendor** | Projectworlds Pvt. Limited |
| **Affected versions** | Version 1.0 |
| **State** | Public |
| **Release date** | 2023-11-01 |

## Vulnerabilities

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Open Redirect |
| **Rule** | [156. Uncontrolled external site redirect](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-156) |
| **Remote** | Yes |
| **CVSSv3 Vector** | CVSS:3.1/AV:N/AC:L/PR:N/UI:R/S:C/C:L/I:L/A:N |
| **CVSSv3 Base Score** | 6.1 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2023-45201](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45201), [CVE-2023-45202](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45202), [CVE-2023-45203](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45203) |

## Description

Online Examination System v1.0 is vulnerable to
multiple Open Redirect vulnerabilities.

## Vulnerabilities

### CVE-2023-45201

The 'q' parameter of the admin.php resource
allows an attacker to redirect a victim user to an
arbitrary web site using a crafted URL:

```php
$ref=@$_GET['q'];
...
if($count==1){
    session_start();
    if(isset($_SESSION['email'])){
    session_unset();}
    $_SESSION["name"] = 'Admin';
    $_SESSION["key"] ='sunny7785068889';
    $_SESSION["email"] = $email;
    header("location:dash.php?q=0");
}
    else header("location:$ref?w=Warning : Access denied");
```

### CVE-2023-45202

The 'q' parameter of the feed.php resource
allows an attacker to redirect a victim user to an
arbitrary web site using a crafted URL:

```php
$ref=@$_GET['q'];
...
header("location:$ref?q=Thank you for your valuable feedback");
```

### CVE-2023-45203

The 'q' parameter of the login.php resource
allows an attacker to redirect a victim user to an
arbitrary web site using a crafted URL:

```php
$ref=@$_GET['q'];
...
if($count==1){
    while($row = mysqli_fetch_array($result)) {
        $name = $row['name'];
    }
    $_SESSION["name"] = $name;
    $_SESSION["email"] = $email;
    header("location:account.php?q=1");
} else
    header("location:$ref?w=Wrong Username or Password");
```

## Our security policy

We have reserved the IDs CVE-2023-45201,
CVE-2023-45202 and CVE-2023-45203
to refer to these issues from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Online Examination System v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://projectworlds.in/>

## Timeline

<time-lapse
discovered="2023-10-05"
contacted="2023-10-05"
disclosure="2023-11-01">
</time-lapse>
