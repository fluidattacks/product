---
slug: advisories/oconnor/
title: Online Bus Booking System v1.0 - Multiple Unauthenticated SQL Injections (SQLi)
authors: Andres Roldan
writer: aroldan
codename: O'Connor
product: Online Bus Booking System v1.0
date: 2023-11-01 12:00 COT
cveid: CVE-2023-45012,CVE-2023-45015,CVE-2023-45018,CVE-2023-45019
severity: 9.8
description: Online Bus Booking System v1.0 - Multiple Unauthenticated SQL Injections (SQLi)
keywords: Fluid Attacks, Security, Vulnerabilities, SQLi, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Online Bus Booking System v1.0 - Multiple Unauthenticated SQL Injections (SQLi) |
| **Code name** | [O'Connor](https://en.wikipedia.org/wiki/Sin%C3%A9ad_O%27Connor) |
| **Product** | Online Bus Booking System |
| **Vendor** | Projectworlds Pvt. Limited |
| **Affected versions** | Version 1.0 |
| **State** | Public |
| **Release date** | 2023-11-01 |

## Vulnerabilities

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Unauthenticated SQL Injections (SQLi) |
| **Rule** | [146. SQL Injection](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-146) |
| **Remote** | Yes |
| **CVSSv3 Vector** | CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H |
| **CVSSv3 Base Score** | 9.8 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2023-45012](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45012), [CVE-2023-45015](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45015), [CVE-2023-45018](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45018), [CVE-2023-45019](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45019) |

## Description

Online Bus Booking System v1.0 is vulnerable to
multiple Unauthenticated SQL Injection vulnerabilities.

## Vulnerabilities

### CVE-2023-45012

The 'user_email' parameter of the bus_info.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$user_email = $_POST['user_email'];
$user_query = $_POST['user_query'];

$query = "INSERT INTO query(query_bus_id, query_user, query_email, query_date, query_content, query_replied) VALUES ('$selected_bus', '$user_name', '$user_email', now(), '$user_query', 'no')";

$query_insert = mysqli_query($connection, $query);
```

### CVE-2023-45015

The 'date' parameter of the search.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$source = $_POST['source'];
$destination = $_POST['destination'];
$date = $_POST['date'];
...
$query = "SELECT * FROM posts WHERE post_via LIKE '%$source%$destination%' AND post_date='$date'";

$search_query = mysqli_query($connection,$query);
```

### CVE-2023-45018

The 'username' parameter of the includes/login.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$username = $_POST['username'];
$password = $_POST['password'];

$query = "SELECT * FROM users WHERE username = '$username'";
$select_user = mysqli_query($connection,$query);
```

### CVE-2023-45019

The 'category' parameter of the category.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
if (isset($_GET['category'])) {
    $cat_type = $_GET['category'];
}

$query = "SELECT *  FROM  posts WHERE post_category_id=$cat_type";
$select_all_bus = mysqli_query($connection,$query);
```

## Our security policy

We have reserved the IDs CVE-2023-45012,
CVE-2023-45015, CVE-2023-45018 and CVE-2023-45019
to refer to these issues from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Online Bus Booking System v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://projectworlds.in/>

## Timeline

<time-lapse
discovered="2023-10-02"
contacted="2023-10-02"
disclosure="2023-11-01">
</time-lapse>
