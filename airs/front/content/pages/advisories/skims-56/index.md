---
slug: advisories/skims-56/
title: ﻿Safelayout Cute Preloader - CSS3 Wordpress Preloader - Reflected cross-site scripting (XSS)
authors: Andres Roldan
writer: aroldan
codename: skims-56
product: ﻿Safelayout Cute Preloader - CSS3 Wordpress Preloader 2.0.96
date: 2025-01-03 12:00 COT
cveid: CVE-2025-0766
description: ﻿Safelayout Cute Preloader - CSS3 Wordpress Preloader 2.0.96 - Reflected cross-site scripting (XSS) Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, CVE, SAST
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | ﻿Safelayout Cute Preloader - CSS3 Wordpress Preloader 2.0.96 - Reflected cross-site scripting (XSS) |
| **Code name** | skims-56 |
| **Product** | ﻿Safelayout Cute Preloader - CSS3 Wordpress Preloader |
| **Affected versions** | Version 2.0.96 |
| **State** | Private |
| **Release date** | 2025-01-03 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Reflected cross-site scripting (XSS) |
| **Rule** | [Reflected cross-site scripting (XSS)](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-008) |
| **Remote** | No |
| **CVSSv4 Vector** | CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:A/VC:H/VI:L/VA:L/SC:L/SI:L/SA:L/E:U |
| **Exploit available** | No |
| **CVE ID(s)** | [CVE-2025-0766](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-0766) |

## Description

﻿Safelayout Cute Preloader - CSS3
Wordpress Preloader
2.0.96
was found to be vulnerable.
The web application dynamically
generates web content without
validating the source of the
potentially untrusted data in
myapp/inc/class-safelayout-preloader-adm
in.php.

## Vulnerability

Skims by Fluid Attacks discovered a
Reflected cross-site scripting (XSS)
in
﻿Safelayout Cute Preloader - CSS3
Wordpress Preloader
2.0.96.
The following is the output of the tool:

### Skims output

```powershell
   293 |
   294 |   // ajax handlers
   295 |   public function add_preloader_meta_box_ajax_handler() {
   296 |    check_ajax_referer( 'slpl_preloader_ajax' );
   297 |    $key = $_POST['key'];
   298 |    if ( $key == 'show' ) {
   299 |     update_option( 'safelayout_preloader_options_meta', true );
   300 |    } else {
   301 |     update_option( 'safelayout_preloader_options_meta', false );
   302 |    }
>  303 |    echo $key;
   304 |
   305 |    wp_die();
   306 |   }
   307 |
   308 |   // ajax handlers for feedback
   309 |   public function preloader_feedback_ajax_handler() {
   310 |    check_ajax_referer( 'slpl_preloader_ajax' );
   311 |    $type = sanitize_text_field( $_POST['type'] );
   312 |    $text = sanitize_text_field( $_POST['text'] );
   313 |    $apiUrl = 'https://safelayout.com/feedback/feedback.php';
       ^ Col 0
```

## Our security policy

We have reserved the ID CVE-2025-0766 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: ﻿Safelayout Cute Preloader - CSS3
Wordpress Preloader
2.0.96

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team using
[Skims](../../plans/#essential)

## Timeline

<time-lapse
  discovered="2025-01-03"
  contacted="2025-01-03"
  replied=""
  confirmed=""
  patched=""
  disclosure="">
</time-lapse>
