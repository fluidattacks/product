---
slug: advisories/skims-70/
title: Different Menu in Different Pages - Insecure deserialization
authors: Andres Roldan
writer: aroldan
codename: skims-70
product: ﻿Different Menu in Different Pages - Control Menu Visibility (All in One) trun
date: 2024-12-06 12:00 COT
cveid: CVE-2025-22627
description: ﻿Different Menu in Different Pages - Control Menu Visibility (All in One) trun - Insecure deserialization Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, CVE, SAST
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | ﻿Different Menu in Different Pages - Control Menu Visibility (All in One) trun - Insecure deserialization |
| **Code name** | skims-70 |
| **Product** | ﻿Different Menu in Different Pages - Control Menu Visibility (All in One) |
| **Affected versions** | Version trun |
| **State** | Private |
| **Release date** | 2024-12-06 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Insecure deserialization |
| **Rule** | [Insecure deserialization](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-096) |
| **Remote** | No |
| **CVSSv4 Vector** | CVSS:4.0/AV:N/AC:H/AT:N/PR:N/UI:N/VC:N/VI:L/VA:L/SC:N/SI:N/SA:N/E:U |
| **Exploit available** | No |
| **CVE ID(s)** | [CVE-2025-22627](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-22627) |

## Description

﻿Different Menu in Different Pages -
Control Menu Visibility (All in One)
trun
was found to be vulnerable.
Unvalidated user input is used directly
in an unserialize function in
myapp/admin/includes/AjaxRequests/save-d
ifferent-menu-conditions.php.

## Vulnerability

Skims by Fluid Attacks discovered a
Insecure deserialization
in
﻿Different Menu in Different Pages -
Control Menu Visibility (All in One)
trun.
The following is the output of the tool:

### Skims output

```powershell
   27 |         if (!current_user_can('administrator')) {
   28 |             echo wp_json_encode(array('success' => false, 'status' => 'not_administrator', 'response' => ''));
   29 |
   30 |             die();
   31 |         }
   32 |
   33 |         $theme_location = sanitize_key($_POST['theme_location']);
   34 |         $assigned_menu = (int) sanitize_key($_POST['assigned_menu']);
   35 |         $name   = sanitize_text_field(serialize($_POST['name'] ));
   36 |
>  37 |         $name = unserialize($name);
   38 |
   39 |         if(is_array($name)){
   40 |
   41 |
   42 |             //if menu has changed
   43 |             $changed    = sanitize_key($_POST['changed']);
   44 |             $changed_menu   = (int) sanitize_key($_POST['changed_menu']);
   45 |             $changed_theme_location = sanitize_key($_POST['changed_theme_location']);
   46 |
   47 |
      ^ Col 0
```

## Our security policy

We have reserved the ID CVE-2025-22627 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: ﻿Different Menu in Different Pages -
Control Menu Visibility (All in One)
trun

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team using
[Skims](../../plans/#essential)

## Timeline

<time-lapse
  discovered="2024-12-06"
  contacted="2025-01-07"
  replied=""
  confirmed=""
  patched=""
  disclosure="">
</time-lapse>
