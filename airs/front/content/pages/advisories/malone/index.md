---
slug: advisories/malone/
title: Browsershot 3.57.3 - Server Side XSS to LFR via HTML
authors: Carlos Bello
writer: cbello
codename: malone
product: Browsershot 3.57.3 - Server Side XSS to LFR via HTML
date: 2022-11-21 13:00 COT
cveid: CVE-2022-43984
severity: 7.5
description: Browsershot 3.57.3 - Server Side XSS to LFR via HTML
keywords: Fluid Attacks, Security, Vulnerabilities, Browsershot, Local File Read, XSS, LFR, PDF Generation
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Browsershot 3.57.3 - Server Side XSS to LFR via HTML"
    code="[Malone](https://en.wikipedia.org/wiki/Post_Malone)"
    product="Browsershot"
    affected-versions="Version 3.57.3"
    fixed-versions=""
    state="Public"
    release="2022-11-21">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Server Side XSS"
    rule="[425. Server Side XSS](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-425)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N"
    score="7.5"
    available="Yes"
    id="[CVE-2022-43984](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-43984)">
</vulnerability-table>

## Description

Browsershot version 3.57.3 allows an external attacker to remotely
obtain arbitrary local files. This is possible because the application
does not validate that the JS content imported from an external source
passed to the `Browsershot::html` method does not contain URLs that use
the `file://` protocol.

## Vulnerability

This vulnerability occurs because the application does not validate that
the JS content imported from an external source passed to the
`Browsershot::html` method does not contain URLs that use the `file://`
protocol.

## Evidence of exploitation

![bypass-LFR-browsershot.gif](https://user-images.githubusercontent.com/51862990/199795560-24b3859a-ec1d-46ed-ac4d-3c8ca098527e.gif)

![bypass-LFR-browsershot.png](https://user-images.githubusercontent.com/51862990/199795469-e6db89bf-cf87-4cda-9082-4e4363753f06.png)

## Our security policy

We have reserved the CVE-2022-43984 to refer to these issues from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Browsershot 3.57.3

* Operating System: GNU/Linux

## Mitigation

An updated version of Browsershot is available at the vendor page.

## Credits

The vulnerability was discovered by [Carlos
Bello](https://www.linkedin.com/in/carlos-andres-bello) from Fluid Attacks'
Offensive Team.

## References

**Vendor page** <https://github.com/spatie/browsershot>

## Timeline

<time-lapse
  discovered="2022-10-25"
  contacted="2022-10-25"
  replied="2022-10-25"
  confirmed="2022-10-25"
  patched=""
  disclosure="2022-11-21">
</time-lapse>
