---
slug: advisories/skims-32/
title: PowerPress Podcasting plugin by Blubrry - XML injection (XXE)
authors: Andres Roldan
writer: aroldan
codename: skims-32
product: PowerPress Podcasting plugin by Blubrry 11.10.
date: 2025-01-03 12:00 COT
cveid: CVE-2025-0775
description: PowerPress Podcasting plugin by Blubrry 11.10. - XML injection (XXE) Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, CVE, SAST
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | PowerPress Podcasting plugin by Blubrry 11.10. - XML injection (XXE) |
| **Code name** | skims-32 |
| **Product** | PowerPress Podcasting plugin by Blubrry |
| **Affected versions** | Version 11.10. |
| **State** | Private |
| **Release date** | 2025-01-03 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | XML injection (XXE) |
| **Rule** | [XML injection (XXE)](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-083) |
| **Remote** | No |
| **CVSSv4 Vector** | CVSS:4.0/AV:N/AT:N/AC:L/PR:L/UI:N/VC:L/VI:L/VA:L/SC:N/SI:N/SA:N/E:U |
| **Exploit available** | No |
| **CVE ID(s)** | [CVE-2025-0775](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-0775) |

## Description

PowerPress Podcasting plugin by Blubrry
11.10.
was found to be vulnerable.
Access to external entities in XML
parsing is enabled in
myapp/getid3/getid3.lib.php.

## Vulnerability

Skims by Fluid Attacks discovered a
XML injection (XXE)
in
PowerPress Podcasting plugin by Blubrry
11.10..
The following is the output of the tool:

### Skims output

```powershell
   530 |    }
   531 |   }
   532 |   return ($returnkey ? $minkey : $minvalue);
   533 |  }
   534 |
   535 |  public static function XML2array($XMLstring) {
   536 |   if (function_exists('simplexml_load_string') && function_exists('libxml_disable_entity_loader')) {
   537 |    // http://websec.io/2012/08/27/Preventing-XEE-in-PHP.html
   538 |    // https://core.trac.wordpress.org/changeset/29378
   539 |    $loader = libxml_disable_entity_loader(true);
>  540 |    $XMLobject = simplexml_load_string($XMLstring, 'SimpleXMLElement', LIBXML_NOENT);
   541 |    $return = self::SimpleXMLelement2array($XMLobject);
   542 |    libxml_disable_entity_loader($loader);
   543 |    return $return;
   544 |   }
   545 |   return false;
   546 |  }
   547 |
   548 |  public static function SimpleXMLelement2array($XMLobject) {
   549 |   if (!is_object($XMLobject) && !is_array($XMLobject)) {
   550 |    return $XMLobject;
       ^ Col 0
```

## Our security policy

We have reserved the ID CVE-2025-0775 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: PowerPress Podcasting plugin by Blubrry
11.10.

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team using
[Skims](../../plans/#essential)

## Timeline

<time-lapse
  discovered="2025-01-03"
  contacted="2025-01-03"
  replied=""
  confirmed=""
  patched=""
  disclosure="">
</time-lapse>
