---
slug: advisories/skims-35/
title: Ni WooCommerce Cost Of Goods - Reflected cross-site scripting (XSS)
authors: Andres Roldan
writer: aroldan
codename: skims-35
product: Ni WooCommerce Cost Of Goods 3.2.8
date: 2025-01-03 12:00 COT
cveid: CVE-2025-0777
description: Ni WooCommerce Cost Of Goods 3.2.8 - Reflected cross-site scripting (XSS) Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, CVE, SAST
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Ni WooCommerce Cost Of Goods 3.2.8 - Reflected cross-site scripting (XSS) |
| **Code name** | skims-35 |
| **Product** | Ni WooCommerce Cost Of Goods |
| **Affected versions** | Version 3.2.8 |
| **State** | Private |
| **Release date** | 2025-01-03 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Reflected cross-site scripting (XSS) |
| **Rule** | [Reflected cross-site scripting (XSS)](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-008) |
| **Remote** | No |
| **CVSSv4 Vector** | CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:A/VC:H/VI:L/VA:L/SC:L/SI:L/SA:L/E:U |
| **Exploit available** | No |
| **CVE ID(s)** | [CVE-2025-0777](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-0777) |

## Description

Ni WooCommerce Cost Of Goods
3.2.8
was found to be vulnerable.
The web application dynamically
generates web content without
validating the source of the
potentially untrusted data in
myapp/include/niwoocog-add-cost-price.ph
p.

## Vulnerability

Skims by Fluid Attacks discovered a
Reflected cross-site scripting (XSS)
in
Ni WooCommerce Cost Of Goods
3.2.8.
The following is the output of the tool:

### Skims output

```powershell
   10 |
   11 | Table();
   12 |
   13 |
   14 |
   15 |
   16 | rice', ""wooreportcog""); ?></h2>
   17 | table-demo"">
   18 | -body"">
   19 | st-form"" method=""post"">
>  20 | en"" name=""page"" value=""<?php echo $_REQUEST['page'] ?>"" />
   21 |
   22 | able->search_box( __( 'Search Product', ""wooreportcog"" ), 'wooreportcog');
   23 | able->display();
   24 |
   25 |
   26 |
   27 |
   28 |
   29 |
   30 |
      ^ Col 41
```

## Our security policy

We have reserved the ID CVE-2025-0777 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Ni WooCommerce Cost Of Goods
3.2.8

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team using
[Skims](../../plans/#essential)

## Timeline

<time-lapse
  discovered="2025-01-03"
  contacted="2025-01-03"
  replied=""
  confirmed=""
  patched=""
  disclosure="">
</time-lapse>
