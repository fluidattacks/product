---
slug: advisories/pollini/
title: Job Portal v1.0 - Multiple SQLi
authors: Andres Roldan
writer: aroldan
codename: pollini
product: Job Portal v1.0
date: 2023-12-06 12:00 COT
cveid: CVE-2023-49677,CVE-2023-49681,CVE-2023-49688,CVE-2023-49689
severity: 9.8
description: Job Portal v1.0 - Multiple Unauthenticated SQL Injections (SQLi)
keywords: Fluid Attacks, Security, Vulnerabilities, SQLi, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Job Portal v1.0 - Multiple Unauthenticated SQL Injections (SQLi) |
| **Code name** | [Pollini](https://es.wikipedia.org/wiki/Maurizio_Pollini) |
| **Product** | Job Portal |
| **Vendor** | Kashipara Group |
| **Affected versions** | Version 1.0 |
| **State** | Public |
| **Release date** | 2023-12-06 |

## Vulnerabilities

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Unauthenticated SQL Injections (SQLi) |
| **Rule** | [146. SQL Injection](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-146) |
| **Remote** | Yes |
| **CVSSv3 Vector** | CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H |
| **CVSSv3 Base Score** | 9.8 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2023-49677](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-49677), [CVE-2023-49681](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-49681), [CVE-2023-49688](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-49688), [CVE-2023-49689](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-49689) |

## Description

Job Portal v1.0 is vulnerable to
multiple Unauthenticated SQL Injection vulnerabilities.

## Vulnerabilities

### CVE-2023-49677

The 'cmbQual' parameter of the Employer/InsertJob.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$txtTitle=$_POST['txtTitle'];
$txtTotal=$_POST['txtTotal'];
$cmbQual=$_POST['cmbQual'];
$txtDesc=$_POST['txtDesc'];
$Name=$_SESSION['Name'];
if($cmbQual=="Other")
{
$cmbQual=$_POST['txtOther'];
}
// Establish Connection with MYSQL
$con = mysqli_connect ("localhost","root","","job");

// Specify the query to Insert Record
$sql = "insert into job_master (CompanyName,JobTitle,Vacancy,MinQualification,Description) values('".$Name."','".$txtTitle."','".$txtTotal."','".$cmbQual."','".$txtDesc."')";
// execute query
mysqli_query ($con,$sql);
```

### CVE-2023-49681

The 'cmbQual' parameter of the Employer/InsertWalkin.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$txtTitle=$_POST['txtTitle'];
$txtTotal=$_POST['txtTotal'];
$cmbQual=$_POST['cmbQual'];
$txtDesc=$_POST['txtDesc'];
$txtDate=$_POST['txtDate'];
$txtTime=$_POST['txtTime'];
$Name=$_SESSION['Name'];
if($cmbQual=="Other")
{
$cmbQual=$_POST['txtOther'];
}
// Establish Connection with MYSQL
$con = mysqli_connect ("localhost","root","","job");

// Specify the query to Insert Record
$sql = "insert into walkin_master (CompanyName,JobTitle,Vacancy,MinQualification,Description,InterviewDate,InterviewTime) values('".$Name."','".$txtTitle."','".$txtTotal."','".$cmbQual."','".$txtDesc."','".$txtDate."','".$txtTime."')";
// execute query
mysqli_query ($con,$sql);
```

### CVE-2023-49688

The 'txtUser' parameter of the login.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$UserName=$_POST['txtUser'];
$Password=$_POST['txtPass'];
$UserType=$_POST['cmbUser'];
if($UserType=="Administrator")
{
$con = mysqli_connect("localhost","root","","job");

$sql = "select * from user_master where UserName='".$UserName."' and Password='".$Password."'";
$result = mysqli_query($con,$sql);
```

### CVE-2023-49689

The 'JobId' parameter of the Employer/DeleteJob.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$Id=$_GET['JobId'];
// Establish Connection with MYSQL
$con = mysqli_connect ("localhost","root","","job");

// Specify the query to Insert Record
$sql = "delete from Job_Master where JobId='".$Id."'";
// execute query
mysqli_query ($con,$sql);
```

## Our security policy

We have reserved the IDs CVE-2023-49677,
CVE-2023-49681, CVE-2023-49688 and CVE-2023-49689
to refer to these issues from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Job Portal v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://www.kashipara.com/>

## Timeline

<time-lapse
discovered="2023-11-29"
contacted="2023-11-29"
disclosure="2023-12-06">
</time-lapse>
