---
slug: advisories/skims-9/
title: WP Activity Log - Insecure deserialization
authors: Andres Roldan
writer: aroldan
codename: skims-9
product: WP Activity Log 5.3.2
date: 2025-01-03 12:00 COT
cveid: CVE-2025-0767
description: WP Activity Log 5.3.2 - Insecure deserialization Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, CVE, SAST
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | WP Activity Log 5.3.2 - Insecure deserialization |
| **Code name** | skims-9 |
| **Product** | WP Activity Log |
| **Affected versions** | Version 5.3.2 |
| **State** | Public |
| **Release date** | 2025-02-27 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Insecure deserialization |
| **Rule** | [Insecure deserialization](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-096) |
| **Remote** | No |
| **CVSSv4 Vector** | CVSS:4.0/AV:N/AC:H/AT:N/PR:N/UI:N/VC:N/VI:L/VA:L/SC:N/SI:N/SA:N/E:U |
| **Exploit available** | No |
| **CVE ID(s)** | [CVE-2025-0767](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-0767) |

## Description

WP Activity Log
5.3.2
was found to be vulnerable.
Unvalidated user input is used directly
in an unserialize function in
myapp/classes/Writers/class-csv-writer.p
hp.

## Vulnerability

Skims by Fluid Attacks discovered a
Insecure deserialization
in
WP Activity Log
5.3.2.
The following is the output of the tool:

### Skims output

```powershell
  228 |   public static function write_csv_ajax() {
  229 |    if ( ! array_key_exists( 'nonce', $_POST ) || ! wp_verify_nonce( $_POST['nonce'], 'wsal-export-csv-nonce' ) ) { // php
  230 |     wp_send_json_error( esc_html_e( 'nonce is not provided or incorrect', 'wp-security-audit-log' ) );
  231 |     die();
  232 |    }
  233 |
  234 |    if ( ! array_key_exists( 'query', $_POST ) ) {
  235 |     wp_send_json_error( esc_html_e( 'query is not provided or incorrect', 'wp-security-audit-log' ) );
  236 |     die();
  237 |    } else {
> 238 |     $query = unserialize( ase64_decode( \sanitize_text_field( \wp_unslash( $_POST['query'] ) ) ) );
  239 |    }
  240 |
  241 |    if ( ! array_key_exists( 'order', $_POST ) ) {
  242 |     wp_send_json_error( esc_html_e( 'order is not provided or incorrect', 'wp-security-audit-log' ) );
  243 |     die();
  244 |    }
  245 |
  246 |    if ( ! array_key_exists( 'step', $_POST ) ) {
  247 |     wp_send_json_error( esc_html_e( 'step is not provided or incorrect', 'wp-security-audit-log' ) );
  248 |     die();
      ^ Col 0
```

## Our security policy

We have reserved the ID CVE-2025-0767 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: WP Activity Log 5.3.2

## Mitigation

The vendor released the version 5.3.3 with a fix
for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team using
[Skims](../../plans/#essential)

## Timeline

<time-lapse
  discovered="2025-01-03"
  contacted="2025-02-24"
  replied=""
  confirmed=""
  patched="2025-03-03"
  disclosure="2025-02-28">
</time-lapse>
