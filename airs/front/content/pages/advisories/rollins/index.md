---
slug: advisories/rollins/
title: Vba32 Antivirus v3.36.0 - DoS
authors: Andres Roldan
writer: aroldan
codename: rollins
product: Vba32 Antivirus v3.36.0
date: 2024-01-16 12:00 COT
cveid: CVE-2024-23441
severity: 5.5
description: Vba32 Antivirus v3.36.0 - Denial of Service (DoS) Advisory
keywords: Fluid Attacks, Security, Vulnerabilities, DoS, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Vba32 Antivirus v3.36.0 - Denial of Service (DoS) |
| **Code name** | [rollins](https://en.wikipedia.org/wiki/Sonny_Rollins) |
| **Product** | Vba32 Antivirus |
| **Vendor** | VirusBlokAda |
| **Affected versions** | Version 3.36.0 |
| **State** | Public |
| **Release date** | 2024-01-29 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Denial of Service (DoS) |
| **Rule** | [002. Asymmetric Denial of Service](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-002) |
| **Remote** | No |
| **CVSSv3 Vector** | CVSS:3.1/AV:L/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H |
| **CVSSv3 Base Score** | 5.5 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2024-23441](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-23441) |

## Description

Vba32 Antivirus v3.36.0 is vulnerable to
a Denial of Service vulnerability by triggering the
0x2220A7 IOCTL code of the Vba32m64.sys driver.

## Vulnerability

The 0x2220A7 IOCTL code of the `Vba32m64.sys`
driver is vulnerable to DoS, leading to a BSOD of
the affected computer caused by a NULL pointer
dereference. If an attacker sends an `nOutBufferSize`
parameter greater or equal than 0x2E, the execution flow will
reach the function `sub_11B34` which doesn't verify the return
address of another routine:

```c
v2 = sub_128A8((__int64)&qword_1D710);
v3 = v2; // [1]
if ( v2 )
{
    v4 = sub_12750(v2);
}
else
{
    v5 = sub_128A8((__int64)&qword_1D600);
    v3 = v5;
    if ( !v5 )
        return 0;
    v4 = sub_1278C(v5, 2i64);
    *(_QWORD *)(v3 + 0x50) = MEMORY[0xFFFFF78000000014] + 0x2FAF080i64;
    sub_12830(&qword_1D650, v3);
}
v6 = (_WORD *)(a1 + 0x2C);
*(_QWORD *)(a1 + 0x10) = *(_QWORD *)(v3 + 0x40); // [2]
```

At [1], the return value is assigned to a variable which is
then dereferenced at [2] without checking for its value,
leading to a NULL pointer dereference:

```bash
rax=0000000000000000 rbx=0000000000000000 rcx=0000000000000001
rdx=0000000000000001 rsi=00000000031ac0f4 rdi=00000000031ac120
rip=fffff80447e41b6e rsp=ffffc9816c7fc730 rbp=0000000000000000
 r8=00000000ffffffff  r9=7ffff80447e4d6b0 r10=7ffffffffffffffc
r11=0000000000000000 r12=0000000000000020 r13=0000000000000030
r14=ffffe20fa72678b0 r15=ffffe20fa0e9b060
iopl=0         nv up ei pl zr na po nc
cs=0010  ss=0018  ds=002b  es=002b  fs=0053  gs=002b             efl=00050246
Vba32m64+0x1b6e:
fffff804`47e41b6e 488b4340        mov     rax,qword ptr [rbx+40h] ds:002b:00000000`00000040=????????????????
```

## Our security policy

We have reserved the ID CVE-2024-23441
to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Vba32 Antivirus v3.36.0
* Operating System: Windows

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://www.anti-virus.by/>

**Product page** <https://www.anti-virus.by/vba32>

## Timeline

<time-lapse
discovered="2024-01-16"
contacted="2024-01-16"
replied="2024-01-18"
disclosure="2024-01-29">
</time-lapse>
