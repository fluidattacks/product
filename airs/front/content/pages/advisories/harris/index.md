---
slug: advisories/harris/
title: Recipes 1.5.10 - Blind SSRF
authors: Carlos Bello
writer: cbello
codename: harris
product: Recipes 1.5.10 - Blind SSRF
date: 2024-02-19 12:00 COT
cveid: CVE-2024-0403
severity: 5.3
description: Recipes 1.5.10 - Blind Server Side Request Forgery
keywords: Fluid Attacks, Security, Vulnerabilities, SRRF, Recipes, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Recipes 1.5.10 - Blind SSRF"
    code="[Harris](https://en.wikipedia.org/wiki/Calvin_Harris)"
    product="Recipes 1.5.10"
    affected-versions="Version 1.5.10"
    fixed-versions=""
    state="Public"
    release="2024-01-16">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Server-side request forgery"
    rule="[100. Server-side request forgery](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-100)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N"
    score="5.3"
    available="No"
    id="[CVE-2024-0403](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-0403)">
</vulnerability-table>

## Description

Recipes version 1.5.10 allows arbitrary HTTP requests to be made
through the server. This is possible because the application is
vulnerable to SSRF.

## Vulnerability

A Server-Side Request Forgery (SSRF) vulnerability has been identified in
Recipes, which could allow an attacker to make arbitrary HTTP requests through
the server.

## Evidence of exploitation

<iframe src="https://shorturl.at/rxUZ2"
width="835" height="504" frameborder="0" title="Blind-SSRF-Recipes-1.5.10"
webkitallowfullscreen mozallowfullscreen allowfullscreen controls
loading="lazy"></iframe>

## Our security policy

We have reserved the ID CVE-2024-0403 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Recipes 1.5.10

* Operating System: MacOS

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by [Carlos
Bello](https://www.linkedin.com/in/carlos-andres-bello) from Fluid Attacks'
Offensive Team.

## References

**Vendor page** <https://github.com/TandoorRecipes/recipes/>

## Timeline

<time-lapse
  discovered="2024-01-10"
  contacted="2024-02-10"
  replied=""
  confirmed=""
  patched=""
  disclosure="2024-02-19">
</time-lapse>
