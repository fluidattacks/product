---
slug: advisories/nergal/
title: Asset Management System v1.0 - Unauthenticated SQL Injection (SQLi)
authors: Andres Roldan
writer: aroldan
codename: Nergal
product: Asset Management System v1.0
date: 2023-09-28 12:00 COT
cveid: CVE-2023-43013
severity: 9.8
description: Asset Management System v1.0 - Unauthenticated SQL Injection (SQLi)
keywords: Fluid Attacks, Security, Vulnerabilities, SQLi, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Asset Management System v1.0 - Unauthenticated SQL Injection (SQLi)"
    code="[Nergal](https://en.wikipedia.org/wiki/Adam_Darski)"
    product="Asset Management System"
    vendor="Projectworlds Pvt. Limited"
    affected-versions="Version 1.0"
    fixed-versions=""
    state="Public"
    release="2023-09-28">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Unauthenticated SQL Injection (SQLi)"
    rule="[146. SQL Injection](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-146)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H"
    score="9.8"
    available="Yes"
    id="[CVE-2023-43013](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43013)">
</vulnerability-table>

## Description

Asset Management System v1.0 is vulnerable to an
unauthenticated SQL Injection vulnerability on the
'email' parameter of index.php page, allowing an
external attacker to dump all the contents of the
database contents and bypass the login control.

## Vulnerability

The 'email' parameter of the index.php resource
does not validate the characters received and
it's sent unfiltered to the database.
The vulnerable function is 'login()' located
at core/functions/user.php:

```php
function login($con,$email,$password){

    $user_id= user_id_from_email($con,$email);
    $password = md5($password);

    $query=mysqli_query($con,"SELECT * FROM `users` WHERE `email`= '$email' AND `password`='$password'");
    $result=mysqli_num_rows($query);
    return ($result==1) ? $user_id :false;

}
```

## Evidence of exploitation

![evidence1](https://res.cloudinary.com/fluid-attacks/image/upload/v1695305268/airs/advisories/nergal/evidence1.webp)

## Our security policy

We have reserved the ID CVE-2023-43013 to refer to this
issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Asset Management System v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks'
Offensive Team.

## References

**Vendor page** <https://projectworlds.in/>

## Timeline

<time-lapse
discovered="2023-09-21"
contacted="2023-09-21"
disclosure="2023-09-28">
</time-lapse>
