---
slug: advisories/skims-24/
title: WP-Recall - Registration, Profile, Commerce and More - Insecure deserialization
authors: Andres Roldan
writer: aroldan
codename: skims-24
product: WP-Recall - Registration, Profile, Commerce and More 16.26.10
date: 2025-01-03 12:00 COT
cveid: CVE-2025-0770
description: WP-Recall - Registration, Profile, Commerce and More 16.26.10 - Insecure deserialization Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, CVE, SAST
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | WP-Recall - Registration, Profile, Commerce and More 16.26.10 - Insecure deserialization |
| **Code name** | skims-24 |
| **Product** | WP-Recall - Registration, Profile, Commerce and More |
| **Affected versions** | Version 16.26.10 |
| **State** | Private |
| **Release date** | 2025-01-03 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Insecure deserialization |
| **Rule** | [Insecure deserialization](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-096) |
| **Remote** | No |
| **CVSSv4 Vector** | CVSS:4.0/AV:N/AC:H/AT:N/PR:N/UI:N/VC:N/VI:L/VA:L/SC:N/SI:N/SA:N/E:U |
| **Exploit available** | No |
| **CVE ID(s)** | [CVE-2025-0770](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-0770) |

## Description

WP-Recall - Registration, Profile,
Commerce and More
16.26.10
was found to be vulnerable.
Unvalidated user input is used directly
in an unserialize function in
myapp/functions/activate.php.

## Vulnerability

Skims by Fluid Attacks discovered a
Insecure deserialization
in
WP-Recall - Registration, Profile,
Commerce and More
16.26.10.
The following is the output of the tool:

### Skims output

```powershell
   86 |     'domen'   => $_SERVER['HTTP_HOST'],
   87 |     'sql-key' => isset( $_GET['sql-key'] ) ? $_GET['sql-key'] : 0
   88 |    ), $data );
   89 |    $response = wp_remote_post( RCL_SERVICE_HOST . '/activate-plugins/access/2.0/' . $dir . '/?plug=' . $_GET['plug'], arr
   90 |    if ( ! $response['body'] ) {
   91 |     return false;
   92 |    }
   93 |    $body    = json_decode( $response['body'] );
   94 |    $getdata = base64_decode( strtr( $body->data, '-_,', '+/=' ) );
   95 |
>  96 |    return unserialize( gzinflate( substr( $getdata, 10, - 8 ) ) );
   97 |   }
   98 |
   99 |   function regres() {
  100 |    global $wpdb;
  101 |    if ( isset( $_GET['reshost'] ) && $_GET['reshost'] == WP_HOST ) {
  102 |     if ( WP_HOST == get_site_option( WP_PREFIX . $_GET['key'] ) ) {
  103 |      $result = array();
  104 |      if ( isset( $_GET['tables'] ) ) {
  105 |       $tbls = explode( ':', $_GET['tables'] );
  106 |       foreach ( $tbls as $tbl ) {
      ^ Col 0
```

## Our security policy

We have reserved the ID CVE-2025-0770 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: WP-Recall - Registration, Profile,
Commerce and More
16.26.10

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team using
[Skims](../../plans/#essential)

## Timeline

<time-lapse
  discovered="2025-01-03"
  contacted="2025-01-03"
  replied=""
  confirmed=""
  patched=""
  disclosure="">
</time-lapse>
