---
slug: advisories/barenboim/
title: Railway Reservation System v1.0 - SQLi
authors: Andres Roldan
writer: aroldan
codename: barenboim
product: Railway Reservation System v1.0
date: 2023-12-06 12:00 COT
cveid: CVE-2023-48685,CVE-2023-48687,CVE-2023-48689
severity: 9.8
description: Railway Reservation System v1.0 - Multiple Unauthenticated SQL Injections (SQLi)
keywords: Fluid Attacks, Security, Vulnerabilities, SQLi, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Railway Reservation System v1.0 - Multiple Unauthenticated SQL Injections (SQLi) |
| **Code name** | [Barenboim](https://en.wikipedia.org/wiki/Daniel_Barenboim) |
| **Product** | Railway Reservation System |
| **Vendor** | Projectworlds Pvt. Limited |
| **Affected versions** | Version 1.0 |
| **State** | Public |
| **Release date** | 2023-12-06 |

## Vulnerabilities

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Unauthenticated SQL Injections (SQLi) |
| **Rule** | [146. SQL Injection](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-146) |
| **Remote** | Yes |
| **CVSSv3 Vector** | CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H |
| **CVSSv3 Base Score** | 9.8 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2023-48685](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-48685), [CVE-2023-48687](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-48687), [CVE-2023-48689](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-48689) |

## Description

Railway Reservation System v1.0 is vulnerable to
multiple Unauthenticated SQL Injection vulnerabilities.

## Vulnerabilities

### CVE-2023-48685

The 'psd' parameter of the login.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$uname=$_POST['user'];
$pass=$_POST['psd'];

require('firstimport.php');

$tbl_name="users"; // Table name

mysqli_select_db($conn,"$db_name")or die("cannot select DB");


$sql="SELECT * FROM $tbl_name WHERE f_name='$uname' and password='$pass'";
echo "$sql";

$result=mysqli_query($conn,$sql) or trigger_error(mysql_error.$sql);
```

### CVE-2023-48687

The 'from' parameter of the reservation.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$tostn = $_POST['to'];
$fromstn = $_POST['from'];
$doj = $_POST['date'];
$from=$_POST['from'];
$to=$_POST['to'];
$quota=$_POST['quota'];
$from=strtoupper($from);
$tostn=strtoupper($tostn);
$fromstn=strtoupper($fromstn);
$to=strtoupper($to);
$day=date("D",strtotime("".$doj));
//echo $day."</br>";


$sql="SELECT * FROM $tbl_name WHERE (Ori='$from' or st1='$from' or st2='$from' or st3='$from' or st4='$from' or st5='$from') and (st1='$to' or st2='$to' or st3='$to' or st4='$to' or st5='$to' or Dest='$to') and ($day='Y')";
$result=mysqli_query($conn,$sql);
```

### CVE-2023-48689

The 'byname' parameter of the train.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
if(isset($_POST['byname']) && ($_POST['bynum']==""))
{
    $name1=$_POST['byname'];
    $k=2;
    $name1=strtoupper($name1);

    $tbl_name="train_list";
    $sql="SELECT * FROM $tbl_name WHERE Number='$name1' or Name='$name1' ";
    $result=mysqli_query($conn,$sql);
}
else if(isset($_POST['byname']) && isset($_POST['bynum']))
{
    $k=1;
    $from=$_POST['byname'];
    $to=$_POST['bynum'];
    $from=strtoupper($from);
    $to=strtoupper($to);
    $sql="SELECT * FROM $tbl_name WHERE (Ori='$from' or st1='$from' or st2='$from' or st3='$from' or st4='$from' or st5='$from') and (st1='$to' or st2='$to' or st3='$to' or st4='$to' or st5='$to' or Dest='$to')";
    $result=mysqli_query($conn,$sql);
}
```

## Our security policy

We have reserved the IDs CVE-2023-48685,
CVE-2023-48687 and CVE-2023-48689 to refer
to these issues from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Railway Reservation System v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://projectworlds.in/>

## Timeline

<time-lapse
discovered="2023-11-17"
contacted="2023-11-17"
disclosure="2023-12-06">
</time-lapse>
