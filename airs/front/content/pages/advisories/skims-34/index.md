---
slug: advisories/skims-34/
title: WordPress Contact Form - Reflected cross-site scripting (XSS)
authors: Andres Roldan
writer: aroldan
codename: skims-34
product: WordPress Contact Form, Drag and Drop Form Builder Plugin - Live Forms
date: 2025-01-03 12:00 COT
cveid: CVE-2025-0776
description: WordPress Contact Form - Reflected cross-site scripting (XSS) Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, CVE, SAST
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | WordPress Contact Form, Drag and Drop Form Builder Plugin - Live Forms  - Reflected cross-site scripting (XSS) |
| **Code name** | skims-34 |
| **Product** | WordPress Contact Form, Drag and Drop Form Builder Plugin - Live Forms |
| **Affected versions** | Version  |
| **State** | Private |
| **Release date** | 2025-01-03 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Reflected cross-site scripting (XSS) |
| **Rule** | [Reflected cross-site scripting (XSS)](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-008) |
| **Remote** | No |
| **CVSSv4 Vector** | CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:A/VC:H/VI:L/VA:L/SC:L/SI:L/SA:L/E:U |
| **Exploit available** | No |
| **CVE ID(s)** | [CVE-2025-0776](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-0776) |

## Description

WordPress Contact Form, Drag and Drop
Form Builder Plugin - Live Forms

was found to be vulnerable.
The web application dynamically
generates web content without
validating the source of the
potentially untrusted data in
myapp/views/showreqs.php.

## Vulnerability

Skims by Fluid Attacks discovered a
Reflected cross-site scripting (XSS)
in
WordPress Contact Form, Drag and Drop
Form Builder Plugin - Live Forms
.
The following is the output of the tool:

### Skims output

```powershell
  195 |
  196 |
  197 |
  198 |
  199 |
  200 |
  201 |
  202 |
  203 |
  204 |
> 205 | ction=stat_req&form_id=<?php echo $_REQUEST['form_id']; ?>&status=' + status, function() {
  206 |
  207 |
  208 |
  209 |
  210 |
  211 |
  212 |
  213 |
  214 |
  215 |
      ^ Col 149
```

## Our security policy

We have reserved the ID CVE-2025-0776 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: WordPress Contact Form, Drag and Drop
  Form Builder Plugin - Live Forms

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team using
[Skims](../../plans/#essential)

## Timeline

<time-lapse
  discovered="2025-01-03"
  contacted="2025-01-03"
  replied=""
  confirmed=""
  patched=""
  disclosure="">
</time-lapse>
