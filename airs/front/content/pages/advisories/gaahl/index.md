---
slug: advisories/gaahl/
title: Asset Management System v1.0 - Authenticated SQL Injection (SQLi)
authors: Andres Roldan
writer: aroldan
codename: Gaahl
product: Asset Management System v1.0
date: 2023-09-28 12:00 COT
cveid: CVE-2023-43014
severity: 8.8
description: Asset Management System v1.0 - Authenticated SQL Injection (SQLi)
keywords: Fluid Attacks, Security, Vulnerabilities, SQLi, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Asset Management System v1.0 - Authenticated SQL Injection (SQLi)"
    code="[Gaahl](https://en.wikipedia.org/wiki/Gaahl)"
    product="Asset Management System"
    vendor="Projectworlds Pvt. Limited"
    affected-versions="Version 1.0"
    fixed-versions=""
    state="Public"
    release="2023-09-28">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Authenticated SQL Injection (SQLi)"
    rule="[146. SQL Injection](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-146)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H"
    score="8.8"
    available="Yes"
    id="[CVE-2023-43014](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43014)">
</vulnerability-table>

## Description

Asset Management System v1.0 is vulnerable to
an Authenticated SQL Injection vulnerability
on the 'first_name' and 'last_name' parameters
of user.php page, allowing an authenticated
attacker to dump all the contents of the database
contents.

## Vulnerability

The 'first_name' and 'last_name' parameters of
the user.php resource does not validate the
characters received and they are sent unfiltered
to the database. The vulnerable function is
'update_profile()'
located at core/functions/user.php:

```php
function update_profile($con,$update_user,$id){

    $update = array();
    $update_user['password']= md5($update_user['password']);
    foreach($update_user as $field=>$data) {

        $update[] = '`' . $field . '`=\'' . $data . '\'';

        mysqli_query($con, "UPDATE `users` SET" . implode(', ', $update) . " WHERE `id`=$id");
    }
}
```

## Evidence of exploitation

![evidence1](https://res.cloudinary.com/fluid-attacks/image/upload/v1695308850/airs/advisories/gaahl/evidence1.webp)

## Our security policy

We have reserved the ID CVE-2023-43014  to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Asset Management System v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://projectworlds.in/>

## Timeline

<time-lapse
discovered="2023-09-21"
contacted="2023-09-21"
disclosure="2023-09-28">
</time-lapse>
