---
slug: advisories/scott/
title: Ulearn a5a7ca20de859051ea0470542844980a66dfc05d - RCE
authors: Carlos Bello
writer: cbello
codename: scott
product: Ulearn a5a7ca20de859051ea0470542844980a66dfc05d
date: 2023-04-10 12:00 COT
cveid: CVE-2023-0670
severity: 9.1
description: Ulearn a5a7ca20de859051ea0470542844980a66dfc05d - RCE
keywords: Fluid Attacks, Security, Vulnerabilities, Ulearn, RCE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Ulearn a5a7ca20de859051ea0470542844980a66dfc05d - RCE"
    code="[Scott](https://en.wikipedia.org/wiki/Travis_Scott)"
    product="Ulearn"
    affected-versions="a5a7ca20de859051ea0470542844980a66dfc05d"
    fixed-versions=""
    state="Public"
    release="2023-04-10">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Insecure file upload"
    rule="[027. Insecure file upload](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-027)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:C/C:H/I:H/A:H"
    score="9.1"
    available="No"
    id="[CVE-2023-0670](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-0670)">
</vulnerability-table>

## Description

Ulearn version a5a7ca20de859051ea0470542844980a66dfc05d allows an attacker
with administrator permissions to obtain remote code execution on the
server through the image upload functionality. This occurs because the
application does not validate that the uploaded image is actually an image.

## Vulnerability

This vulnerability This occurs because the application does not validate
that the uploaded image is actually an image.

## Evidence of exploitation

To exploit this vulnerability, we only need to send the following malicious
PHP code to the server.

![rce-ulearn](https://user-images.githubusercontent.com/51862990/216733146-d08069b5-d082-432a-8647-a8dd133c8ff6.gif)

## Our security policy

We have reserved the ID CVE-2023-0670 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Ulearn a5a7ca20de859051ea0470542844980a66dfc05d

* Operating System: GNU/Linux

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by [Carlos
Bello](https://www.linkedin.com/in/carlos-andres-bello) from Fluid Attacks'
Offensive Team.

## References

**Vendor page** <https://github.com/ulearnpro/ulearn/>

## Timeline

<time-lapse
  discovered="2023-02-03"
  contacted="2023-02-03"
  replied="2023-02-03"
  confirmed=""
  patched=""
  disclosure="2023-04-10">
</time-lapse>
