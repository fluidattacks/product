---
slug: advisories/skims-45/
title: rtMedia for WordPress, BuddyPress and bbPress - XML injection (XXE)
authors: Andres Roldan
writer: aroldan
codename: skims-45
product: rtMedia for WordPress, BuddyPress and bbPress 4.6.2
date: 2024-12-06 12:00 COT
cveid: CVE-2025-22625
description: rtMedia for WordPress, BuddyPress and bbPress 4.6.2 - XML injection (XXE) Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, CVE, SAST
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | rtMedia for WordPress, BuddyPress and bbPress 4.6.2 - XML injection (XXE) |
| **Code name** | skims-45 |
| **Product** | rtMedia for WordPress, BuddyPress and bbPress |
| **Affected versions** | Version 4.6.2 |
| **State** | Private |
| **Release date** | 2024-12-06 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | XML injection (XXE) |
| **Rule** | [XML injection (XXE)](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-083) |
| **Remote** | No |
| **CVSSv4 Vector** | CVSS:4.0/AV:N/AT:N/AC:L/PR:L/UI:N/VC:L/VI:L/VA:L/SC:N/SI:N/SA:N/E:U |
| **Exploit available** | No |
| **CVE ID(s)** | [CVE-2025-22625](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-22625) |

## Description

rtMedia for WordPress, BuddyPress and
bbPress
4.6.2
was found to be vulnerable.
Access to external entities in XML
parsing is enabled in
myapp/lib/getid3/getid3.lib.php.

## Vulnerability

Skims by Fluid Attacks discovered a
XML injection (XXE)
in
rtMedia for WordPress, BuddyPress and
bbPress
4.6.2.
The following is the output of the tool:

### Skims output

```powershell
   716 |  /**
   717 |   * @param string $XMLstring
   718 |   *
   719 |   * @return array|false
   720 |   */
   721 |  public static function XML2array($XMLstring) {
   722 |   if (function_exists('simplexml_load_string') && function_exists('libxml_disable_entity_loader')) {
   723 |    // http://websec.io/2012/08/27/Preventing-XEE-in-PHP.html
   724 |    // https://core.trac.wordpress.org/changeset/29378
   725 |    $loader = libxml_disable_entity_loader(true);
>  726 |    $XMLobject = simplexml_load_string($XMLstring, 'SimpleXMLElement', LIBXML_NOENT);
   727 |    $return = self::SimpleXMLelement2array($XMLobject);
   728 |    libxml_disable_entity_loader($loader);
   729 |    return $return;
   730 |   }
   731 |   return false;
   732 |  }
   733 |
   734 |  /**
   735 |  * @param SimpleXMLElement|array|mixed $XMLobject
   736 |  *
       ^ Col 0
```

## Our security policy

We have reserved the ID CVE-2025-22625 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: rtMedia for WordPress, BuddyPress and
bbPress
4.6.2

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team using
[Skims](../../plans/#essential)

## Timeline

<time-lapse
  discovered="2024-12-06"
  contacted="2025-01-07"
  replied=""
  confirmed=""
  patched=""
  disclosure="">
</time-lapse>
