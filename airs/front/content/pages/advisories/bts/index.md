---
slug: advisories/bts/
title: Os Commerce - Cross Site Scripting Stored (XSS)
authors: Takao Sato
writer: tsato
codename: bts
product: Os Commerce 4.12.56860 - Cross Site Scripting Stored (XSS)
date: 2023-09-29 12:00 COT
cveid: CVE-2023-43702, CVE-2023-43703, CVE-2023-43704, CVE-2023-43705, CVE-2023-43706, CVE-2023-43707, CVE-2023-43708, CVE-2023-43709, CVE-2023-43710, CVE-2023-43711, CVE-2023-43712, CVE-2023-43713, CVE-2023-43714, CVE-2023-43715, CVE-2023-43716, CVE-2023-43717, CVE-2023-43718, CVE-2023-43719, CVE-2023-43720, CVE-2023-43721,CVE-2023-43722, CVE-2023-43723, CVE-2023-43724, CVE-2023-43725, CVE-2023-43726, CVE-2023-43727, CVE-2023-43728, CVE-2023-43729, CVE-2023-43730, CVE-2023-43731, CVE-2023-43732, CVE-2023-43733, CVE-2023-43734, CVE-2023-43735, CVE-2023-43736, CVE-2023-5111, CVE-2023-5112
severity: 8.5
description:  Os Commerce 4.12.56860 - Cross Site Scripting Stored (XSS)
keywords: Fluid Attacks, Security, Vulnerabilities,XSS, Os Commerce, CVE
banner: advisories-bg
advise: yes
template: advisory
---

<summary-table
    name="Os Commerce 4.12.56860 - Cross Site Scripting (XSS)"
    code="[BTS](https://en.wikipedia.org/wiki/BTS)"
    product="Os Commerce"
    affected-versions="4.12.56860"
    fixed-versions=""
    state="Public"
    release="2023-09-29">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Cross Site Scripting Stored"
    rule="[008. Stored cross-site scripting (XSS)](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-010/)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H"
    score="8.1"
    available="Yes"
    id="[CVE-2023-43702](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43702),[CVE-2023-43703](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43703),[CVE-2023-43704](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43704),[CVE-2023-43705](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43705),[CVE-2023-43706](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43706),[CVE-2023-43707](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43707),[CVE-2023-43708](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43708),[CVE-2023-43709](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43709),[CVE-2023-43710](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43710),[CVE-2023-43711](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43711),[CVE-2023-43712](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43712),[CVE-2023-43713](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43713),[CVE-2023-43714](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43714),[CVE-2023-43715](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43715),[CVE-2023-43716](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43716),[CVE-2023-43717](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43717),[CVE-2023-43718](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43718),[CVE-2023-43719](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43719),[CVE-2023-43720](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43720),[CVE-2023-43720](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43720),[CVE-2023-43721](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43721),[CVE-2023-43722](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43722),[CVE-2023-43723](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43723),[CVE-2023-43724](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43724),[CVE-2023-43725](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43725),[CVE-2023-43726](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43726),[CVE-2023-43727](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43727),[CVE-2023-43728](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43728),[CVE-2023-43729](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43729),[CVE-2023-43730](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43730),[CVE-2023-43731](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43731),[CVE-2023-43732](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43732),[CVE-2023-43733](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43733),[CVE-2023-43734](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43734),[CVE-2023-43735](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43735),[CVE-2023-43736](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43736),[CVE-2023-5111](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-5111),[CVE-2023-5112](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-5112)">
</vulnerability-table>

## Description

Os Commerce is an e-commerce platform that enables businesses
to create online stores and manage product listings, orders,
and more. It offers various features to streamline online selling.

## Vulnerability

Os Commerce is currently susceptible to a Cross-Site Scripting (XSS)
vulnerability. This vulnerability allows attackers to inject malicious
scripts into specific parameters of the application,
potentially leading to unauthorized script execution
within a user's web browser.

Exploiting XSS (Cross Site Scripting) vulnerabilities
can have severe consequences for web applications and their users.
This type of vulnerability occurs when input data from users
is not properly validated and sanitized, allowing malicious
actors to inject scripts that can be executed by other users
visiting the same web page.

## Exploitation

In this scenario, we have identified several URLs and their
corresponding vulnerable parameters, each of which can be
manipulated to execute
a common malicious payload:

```html
"><script>alert(13)</script>.
```

In the following endpoints,
the payload is executable, and we
provide the affected URLs and parameters.

Here's a brief explanation of each of the vulnerable URLs and parameters:

1. CVE-2023-43702: /admin/orders/tracking-save -
   Vulnerable Parameter: tracking_number
2. CVE-2023-43703: /admin/editor/show-basket?orders_id=4&
   currentCart=cart%7C1-35025&uprid=29&action=edit_product -
   Vulnerable Parameter: product_info[][name] -
3. CVE-2023-43704: /admin/design/theme-title - Vulnerable Parameter: title
4. CVE-2023-43705: /admin/texts/submit?translation_key=%23%23BILLING_ADDRESS
   %23%23&translation_entity=keys&row=0 - Vulnerable Parameter:
   translation_value[1]
5. CVE-2023-43706: /admin/email/templates-save -
   Vulnerable Parameter: email_templates_key
6. CVE-2023-43707: /admin/catalog-pages/edit?id=0&platform_id=1&parent_id=0 -
   Vulnerable Parameter: CatalogsPageDescriptionForm[1][name]
7. CVE-2023-43708: /admin/modules/save?set=payment -
    Vulnerable Parameter: configuration_title[1](MODULE_PAYMENT_SAGE_PAY_SERVER_TEXT_TITLE)
8. CVE-2023-43709: /admin/modules/save?set=payment -
   Vulnerable Parameter: configuration_title[1](MODULE)
9. CVE-2023-43710: /admin/modules/save?set=shipping -
   Vulnerable Parameter: configuration_title[1][MODULE_SHIPPING_PERCENT_TEXT_TITLE]
10. CVE-2023-43711: /admin/adminmembers/adminsubmit -
    Vulnerable Parameter: admin_firstname
11. CVE-2023-43712: /admin/adminfiles/submit -
    Vulnerable Parameter: access_levels_name
12. CVE-2023-43713: /admin/admin-menu/add-submit -
    Vulnerable Parameter: title
13. CVE-2023-43714: /admin/configuration/saveparam -
    Vulnerable Parameter: SKIP_CART_PAGE_TITLE[1]
14. CVE-2023-43715: /admin/configuration/saveparam -
    Vulnerable Parameter: ENTRY_FIRST_NAME_MIN_LENGTH_TITLE[1]
15. CVE-2023-43716: /admin/configuration/saveparam -
    Vulnerable Parameter: MAX_DISPLAY_NEW_PRODUCTS_TITLE[1]
16. CVE-2023-43717: /admin/configuration/saveparam -
    Vulnerable Parameter: MSEARCH_HIGHLIGHT_ENABLE_TITLE[1]
17. CVE-2023-43718: /admin/configuration/saveparam -
    Vulnerable Parameter: MSEARCH_ENABLE_TITLE[1]
18. CVE-2023-43719: /admin/configuration/saveparam -
    Vulnerable Parameter: SHIPPING_GENDER_TITLE[1]
19. CVE-2023-43720: /admin/configuration/saveparam -
    Vulnerable Parameter: BILLING_GENDER_TITLE[1]
20. CVE-2023-43721: /admin/configuration/saveparam -
    Vulnerable Parameter: PACKING_SLIPS_SUMMARY_TITLE[1]
21. CVE-2023-43722: /admin/orders_status_groups/save?
    orders_status_groups_id=5 -
    Vulnerable Parameter: orders_status_groups_name[1]
22. CVE-2023-43723: /admin/orders_status/save?type_id=2 -
    Vulnerable Parameter: orders_status_name[1]
23. CVE-2023-43724: /admin/orders-comment-template/edit -
    Vulnerable Parameter: derb6zmklgtjuhh2cn5chn2qjbm2st
    gmfa4.oastify.comscription[1][name]
24. CVE-2023-43725: /admin/orders_products_status/save?
    orders_products_status_id=50 -
    Vulnerable Parameter: orders_products_status_name_long[1]
25. CVE-2023-43726: /admin/orders_products_status_manual/save?
    orders_products_status_manual_id=0 -
    Vulnerable Parameter: orders_products_status_manual_name_long[1]
26. CVE-2023-43727: /admin/stock-indication/save?
    stock_indication_id=11 -
    Vulnerable Parameter: stock_indication_text%5B1%5D
27. CVE-2023-43728: /admin/stock-delivery-terms/save?
    stock_delivery_terms_id=1 -
    Vulnerable Parameter: stock_delivery_terms_text%5B1%5
28. CVE-2023-43729: /admin/xsell-types/save?xsell_type_id=0 -
    Vulnerable Parameter: xsell_type_name%5B1%5D
29. CVE-2023-43730: /admin/countries/save?countries_id=0 -
    Vulnerable Parameter: countries_name[1]
30. CVE-2023-43731: /admin/zones/save?zones_id=0 -
    Vulnerable Parameter: zone_name
31. CVE-2023-43732: /admin/tax_classes/save?tax_classes_id=0 -
    Vulnerable Parameter: tax_class_title
32. CVE-2023-43733: /admin/tax_rates/save?tax_rates_id=13 -
    Vulnerable Parameter: company_address
33. CVE-2023-43734: /admin/languages/save?languages_id=2&action=save -
    Vulnerable Parameter: name
34. CVE-2023-43735: /admin/address-formats/index -
    Vulnerable Parameter: formats_titles[7]
35. CVE-2023-5111: /admin/featured-types/save?featured_type_id=0 -
    Vulnerable Parameter: featured_type_name[1]
36. CVE-2023-5112: /admin/specials-types/save?specials_type_id=0 -
    Vulnerable Parameter: specials_type_name[1]

To exploit these vulnerabilities, an attacker would simply need to modify
the respective parameter with the provided payload:

```html
"><script>alert(13)</script>.
```

This payload triggers the execution of malicious scripts when the affected URLs are
accessed.

## Evidence of exploitation

The same behavior repeats across the previously
mentioned URLs and parameters.
You only need to inject the payload
into the affected parameters or fields,
and it will be executed.

<iframe src="https://www.veed.io/embed/6ba8ac32-6062-4985-b08d-20b92bea265b"
width="835" height="504" frameborder="0" title="xss"
webkitallowfullscreen mozallowfullscreen allowfullscreen controls
loading="lazy"></iframe>

## Our security policy

We have reserved those IDs: CVE-2023-43702,
CVE-2023-43703, CVE-2023-43704, CVE-2023-43705,
CVE-2023-43706, CVE-2023-43707, CVE-2023-43708,
CVE-2023-43709, CVE-2023-43710, CVE-2023-43711,
CVE-2023-43712, CVE-2023-43713, CVE-2023-43714,
CVE-2023-43715, CVE-2023-43716, CVE-2023-43717,
CVE-2023-43718, CVE-2023-43719, CVE-2023-43720,
CVE-2023-43721, CVE-2023-43722, CVE-2023-43723,
CVE-2023-43724, CVE-2023-43725, CVE-2023-43726,
CVE-2023-43727, CVE-2023-43728, CVE-2023-43729,
CVE-2023-43730, CVE-2023-43731, CVE-2023-43732,
CVE-2023-43733, CVE-2023-43734, CVE-2023-43735,
CVE-2023-5111, CVE-2023-5112

to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Os Commerce 4.12.56860

* Operating System: Windows

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Takao Sato](https://www.linkedin.com/in/takao-sato/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://www.oscommerce.com>

## Timeline

<time-lapse
  discovered="2023-09-22"
  contacted="2023-09-22"
  replied=""
  confirmed=""
  patched=""
  disclosure="2023-09-29">
</time-lapse>
