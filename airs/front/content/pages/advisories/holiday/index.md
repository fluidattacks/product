---
slug: advisories/holiday/
title: Twister Antivirus v8.17 - DoS
authors: Andres Roldan
writer: aroldan
codename: holiday
product: Twister Antivirus v8.17
date: 2024-02-06 12:00 COT
cveid: CVE-2024-1096
severity: 5.5
description: Twister Antivirus v8.17 - Denial of Service Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, DoS, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Twister Antivirus v8.17 - Denial of Service |
| **Code name** | [Holiday](https://en.wikipedia.org/wiki/Billie_Holiday) |
| **Product** | Twister Antivirus |
| **Vendor** | Filseclab |
| **Affected versions** | Version 8.17 |
| **State** | Public |
| **Release date** | 2024-02-06 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Denial of Service (DoS) |
| **Rule** | [002. Asymmetric Denial of Service](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-002) |
| **Remote** | No |
| **CVSSv3 Vector** | CVSS:3.1/AV:L/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H |
| **CVSSv3 Base Score** | 5.5 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2024-1096](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-1096) |

## Description

Twister Antivirus v8.17 is vulnerable to
a Denial of Service vulnerability by triggering the `0x80112067`,
`0x801120CB 0x801120CC 0x80112044, 0x8011204B, 0x8011204F,`
`0x80112057, 0x8011205B, 0x8011205F, 0x80112063, 0x8011206F,`
`0x80112073, 0x80112077, 0x80112078, 0x8011207C` and `0x80112080`
IOCTL codes of the `fildds.sys` driver.

## Vulnerability

The `0x80112067, 0x801120CB 0x801120CC 0x80112044, 0x8011204B, 0x8011204F,`
`0x80112057, 0x8011205B, 0x8011205F, 0x80112063, 0x8011206F,`
`0x80112073, 0x80112077, 0x80112078, 0x8011207C` and `0x80112080`
IOCTL code of the `fildds.sys` driver allows to perform a
Denial of Service, leading to a BSOD of the affected computer caused
by a NULL pointer dereference.

The resulting debugging session when triggering
those IOCTLs resemble the following:

```c
rax=0000000000000000 rbx=ffffce8e6d6fa060 rcx=fffff80171540000
rdx=0000000000000000 rsi=0000000000000001 rdi=ffffce8e6ea8f140
rip=fffff80171541994 rsp=fffff0056a289ee0 rbp=0000000000000002
 r8=0000000000000000  r9=0000000000000000 r10=fffff80171542cd0
r11=0000000000000000 r12=0000000000000000 r13=0000000000000000
r14=ffffce8e6ea8f140 r15=ffffce8e6dbaada0
iopl=0         nv up ei ng nz na po nc
cs=0010  ss=0018  ds=002b  es=002b  fs=0053  gs=002b             efl=00050286
fildds+0x1994:
fffff801`71541994 8b8000040000    mov     eax,dword ptr [rax+400h] ds:002b:00000000`00000400=????????
Resetting default scope

PROCESS_NAME:  IOCTLBruteForce.exe

STACK_TEXT:
fffff005`6a289ee0 fffff801`71542e65     : ffffce8e`80112080 00000000`00000000 ffffce8e`00000000 00000000`00000000 : fildds+0x1994
fffff005`6a28b780 fffff801`6aed1f35     : ffffce8e`6dbaada0 ffffce8e`6d6fa060 fffff005`6a28bb80 00000000`00000001 : fildds+0x2e65
```

## Our security policy

We have reserved the ID CVE-2024-1096 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Twister Antivirus v8.17
* Operating System: Windows

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <http://www.filseclab.com/en-us/index.htm>

**Product page** <http://www.filseclab.com/en-us/products/twister.htm>

## Timeline

<time-lapse
discovered="2024-02-02"
contacted="2024-02-02"
disclosure="2024-02-06">
</time-lapse>
