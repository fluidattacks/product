---
slug: advisories/armstrong/
title: PeTeReport 0.5 - Stored XSS (Markdown)
authors: Oscar Uribe
writer: ouribe
codename: armstrong
product: PeTeReport 0.5
date: 2022-02-18 10:00 COT
cveid: CVE-2022-25220
severity: 4.8
description: PeTeReport 0.5 - Stored XSS (Markdown)
keywords: Fluid Attacks, Security, Vulnerabilities, PeTeReport, XSS
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="PeTeReport 0.5 - Stored XSS (Markdown)"
    code="[Armstrong](https://en.wikipedia.org/wiki/Louis_Armstrong)"
    product="PeTeReport"
    affected-versions="Version 0.5"
    fixed-versions="Version 0.7"
    state="Public"
    release="2022-02-23">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Stored cross-site scripting (XSS)"
    rule="[010. Stored cross-site scripting (XSS)](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-010)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:H/UI:R/S:C/C:L/I:L/A:N"
    score="4.8"
    available="No"
    id="[CVE-2022-25220](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-25220)">
</vulnerability-table>

## Description

PeteReport **Version 0.5** allows an authenticated admin user
to inject persistent javascript code inside the markdown descriptions
while creating a product, report or finding.

## Proof of Concept

Steps to reproduce

1. Click on 'Add Product'.
2. Insert the following PoC inside the product description.

   ```javascript
      [XSS](javascript:alert(1))
   ```

3. Click on 'Save Product'.
4. If a user visits the product and click on the link in
   the description the javascript code will be rendered.

System Information

* Version: PeteReport Version 0.5.
* Operating System: Docker.
* Web Server: nginx.

## Exploit

There is no exploit for the vulnerability but can be manually exploited.

## Mitigation

An updated version of PeteReport is available at the vendor page.

## Credits

The vulnerability was discovered by [Oscar
Uribe](https://co.linkedin.com/in/oscar-uribe-londo%C3%B1o-0b6534155) from the Offensive
Team of Fluid Attacks.

## References

**Vendor page** <https://github.com/1modm/petereport>

**Issue** <https://github.com/1modm/petereport/issues/35>

## Timeline

<time-lapse
  discovered="2022-02-08"
  contacted="2022-02-08"
  replied="2022-02-09"
  confirmed=""
  patched="2022-02-28"
  disclosure="2022-02-23">
</time-lapse>
