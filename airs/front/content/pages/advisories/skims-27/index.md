---
slug: advisories/skims-27/
title: Jetpack VaultPress - Insecure deserialization
authors: Andres Roldan
writer: aroldan
codename: skims-27
product: Jetpack VaultPress 3.0.
date: 2025-01-03 12:00 COT
cveid: CVE-2025-0772
description: Jetpack VaultPress 3.0. - Insecure deserialization Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, CVE, SAST
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Jetpack VaultPress 3.0. - Insecure deserialization |
| **Code name** | skims-27 |
| **Product** | Jetpack VaultPress |
| **Affected versions** | Version 3.0. |
| **State** | Private |
| **Release date** | 2025-01-03 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Insecure deserialization |
| **Rule** | [Insecure deserialization](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-096) |
| **Remote** | No |
| **CVSSv4 Vector** | CVSS:4.0/AV:N/AC:H/AT:N/PR:N/UI:N/VC:N/VI:L/VA:L/SC:N/SI:N/SA:N/E:U |
| **Exploit available** | No |
| **CVE ID(s)** | [CVE-2025-0772](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-0772) |

## Description

Jetpack VaultPress
3.0.
was found to be vulnerable.
Unvalidated user input is used directly
in an unserialize function in
myapp/vaultpress.php.

## Vulnerability

Skims by Fluid Attacks discovered a
Insecure deserialization
in
Jetpack VaultPress
3.0..
The following is the output of the tool:

### Skims output

```powershell
  1814 |     if ( !$syntax_check )
  1815 |      $this->response( ""Code Failed Syntax Check"" );
  1816 |     $this->response( eval( $code . ';' ) );
  1817 |     die();
  1818 |     break;
  1819 |    case 'catchup:get':
  1820 |     $this->response( $this->ai_ping_get( (int)$_POST['num'], (string)$_POST['order'] ) );
  1821 |     break;
  1822 |    case 'catchup:delete':
  1823 |     if ( isset( $_POST['pings'] ) ) {
> 1824 |      foreach( unserialize( $_POST['pings'] ) as $ping ) {
  1825 |       if ( 0 === strpos( $ping, '_vp_ai_ping_' ) )
  1826 |        delete_option( $ping );
  1827 |      }
  1828 |     }
  1829 |     break;
  1830 |    case 'general:ping':
  1831 |     global $wp_version, $wp_db_version, $manifest_version;
  1832 |     @error_reporting(0);
  1833 |     $http_modules = array();
  1834 |     $httpd = null;
       ^ Col 0
```

## Our security policy

We have reserved the ID CVE-2025-0772 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Jetpack VaultPress
3.0.

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team using
[Skims](../../plans/#essential)

## Timeline

<time-lapse
  discovered="2025-01-03"
  contacted="2025-01-03"
  replied=""
  confirmed=""
  patched=""
  disclosure="">
</time-lapse>
