---
slug: advisories/evans/
title: Travel Website v1.0 - Multiple SQLi
authors: Andres Roldan
writer: aroldan
codename: evans
product: Travel Website v1.0
date: 2024-01-03 12:00 COT
cveid: CVE-2023-50862,CVE-2023-50863,CVE-2023-50864,CVE-2023-50865,CVE-2023-50866,CVE-2023-50867
severity: 9.8
description: Travel Website v1.0 - Multiple Unauthenticated SQL Injections (SQLi)
keywords: Fluid Attacks, Security, Vulnerabilities, SQLi, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Travel Website v1.0 - Multiple Unauthenticated SQL Injections (SQLi) |
| **Code name** | [Evans](https://en.wikipedia.org/wiki/Bill_Evans) |
| **Product** | Travel Website |
| **Vendor** | Kashipara Group |
| **Affected versions** | Version 1.0 |
| **State** | Public |
| **Release date** | 2024-01-03 |

## Vulnerabilities

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Unauthenticated SQL Injections (SQLi) |
| **Rule** | [146. SQL Injection](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-146) |
| **Remote** | Yes |
| **CVSSv3 Vector** | CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H |
| **CVSSv3 Base Score** | 9.8 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2023-50862](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-50862), [CVE-2023-50863](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-50863), [CVE-2023-50864](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-50864), [CVE-2023-50865](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-50865), [CVE-2023-50866](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-50866), [CVE-2023-50867](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-50867) |

## Description

Travel Website v1.0 is vulnerable to
multiple Unauthenticated SQL Injection vulnerabilities.

## Vulnerabilities

### CVE-2023-50862

The 'hotelIDHidden' parameter of the booking.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
<?php elseif($mode=="hotel"): ?>

<div class="col-sm-12 bookingHotel">

<?php

    $hotelID = $_POST["hotelIDHidden"];

    $hotelSQL = "SELECT * FROM `hotels` WHERE hotelID='$hotelID'";
    $hotelQuery = $conn->query($hotelSQL);
```

### CVE-2023-50863

The 'hotelIDHidden' parameter of the generateReceipt.php
resource does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$hotelID=$_POST["hotelIDHidden"];
...
$sql = "SELECT * FROM hotels WHERE hotelID='$hotelID'";
$result = $conn->query($sql);
```

### CVE-2023-50864

The 'hotelId' parameter of the hotelDetails.php
resource does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$hotelID=$_GET["hotelId"];
...
$sql = "SELECT * FROM hotels WHERE hotelID='$hotelID'";
$result = $conn->query($sql);
```

### CVE-2023-50865

The 'city' parameter of the hotelSearch.php
resource does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$city=$_GET["city"];
...
$sql = "SELECT * FROM hotels WHERE city='$city'";
$rowcount = mysqli_num_rows(mysqli_query($conn,$sql));
```

### CVE-2023-50866

The 'username' parameter of the loginAction.php
resource does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$username=$_POST["username"];
...
$getUserDataSQL = "SELECT * FROM `users` WHERE Username='$username'";
$getUserDataQuery = $conn->query($getUserDataSQL);
```

### CVE-2023-50867

The 'username' parameter of the signupAction.php
resource does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$username=$_POST["username"];
...
$checkUserExistSQL = "SELECT * FROM `users` WHERE Username='$username'";
$checkUserExistQuery = $conn->query($checkUserExistSQL);
```

## Our security policy

We have reserved the IDs CVE-2023-50862, CVE-2023-50863,
CVE-2023-50864, CVE-2023-50865, CVE-2023-50866
and CVE-2023-50867 to refer to these issues from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Travel Website v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://www.kashipara.com/>

## Timeline

<time-lapse
discovered="2024-01-03"
contacted="2024-01-03"
disclosure="2024-01-03">
</time-lapse>
