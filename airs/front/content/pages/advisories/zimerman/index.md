---
slug: advisories/zimerman/
title: Billing Software v1.0 - Multiple SQLi
authors: Andres Roldan
writer: aroldan
codename: zimerman
product: Billing Software v1.0
date: 2023-12-06 12:00 COT
cveid: CVE-2023-49622,CVE-2023-49624,CVE-2023-49625,CVE-2023-49633,CVE-2023-49639,CVE-2023-49641,CVE-2023-49658,CVE-2023-49665,CVE-2023-49666
severity: 9.8
description: Billing Software v1.0 - Multiple Unauthenticated SQL Injections (SQLi)
keywords: Fluid Attacks, Security, Vulnerabilities, SQLi, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Billing Software v1.0 - Multiple Unauthenticated SQL Injections (SQLi) |
| **Code name** | [Zimerman](https://en.wikipedia.org/wiki/Krystian_Zimerman) |
| **Product** | Billing Software |
| **Vendor** | Kashipara Group |
| **Affected versions** | Version 1.0 |
| **State** | Public |
| **Release date** | 2023-12-06 |

## Vulnerabilities

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Unauthenticated SQL Injections (SQLi) |
| **Rule** | [146. SQL Injection](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-146) |
| **Remote** | Yes |
| **CVSSv3 Vector** | CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H |
| **CVSSv3 Base Score** | 9.8 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2023-49622](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-49622), [CVE-2023-49624](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-49624), [CVE-2023-49625](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-49625), [CVE-2023-49633](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-49633), [CVE-2023-49639](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-49639), [CVE-2023-49641](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-49641), [CVE-2023-49658](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-49658), [CVE-2023-49665](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-49665), [CVE-2023-49666](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-49666) |

## Description

Billing Software v1.0 is vulnerable to
multiple Unauthenticated SQL Injection vulnerabilities.

## Vulnerabilities

### CVE-2023-49622

The 'itemnameid' parameter of the
material_bill.php?action=itemRelation resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
else if($_POST['action'] == 'itemRelation') {
    $pricetype = $_POST['pricetype'];
    $itemnameid = $_POST['itemnameid'];
    $itemtypeid = $_POST['itemtypeid'];
    if($pricetype == 'item_price_dist') {
    $sql44 = mysqli_query($con,"SELECT IL.item_price_dist FROM ho_item_list AS IL INNER JOIN itemtype AS IT ON IL.item_type = IT.sl_no WHERE IL.status='1' AND IL.sl_no = '$itemnameid' AND IL.item_type = '$itemtypeid'");
```

### CVE-2023-49624

The 'cancelid' parameter of the material_bill.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
else if($_REQUEST['cancelid']) {

$billno = $_REQUEST['cancelid'];
$sqld1 = mysqli_query($con,"DELETE FROM sell_bill_details WHERE bill_no = '$billno'");
$sqld2 = mysqli_query($con,"DELETE FROM sell_item_details WHERE sell_bill_no = '$billno'");
$sqld3 = mysqli_query($con,"DELETE FROM stock_entry WHERE bill_no = '$billno'");
$sqld4 = mysqli_query($con,"DELETE FROM payment WHERE bill_no = '$billno'");
```

### CVE-2023-49625

The 'id' parameter of the partylist_edit_submit.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$type_id = $_POST['id'];
$partyName = $_POST['partyName'];
$Contact = $_POST['Contact'];
$Tin = $_POST['Gstn_no'];
$Pan = $_POST['Pan'];
$cin_no = $_POST['Cin'];
$partyAddress = $_POST['Party_address'];
$bank_details = $_POST['bank_details'];

$sql=mysqli_query($con,"UPDATE  party_details SET party_name='$partyName', contact='$Contact', gstin='$Tin',pan_no='$Pan', cin_no='$cin_no',party_address='$partyAddress',bank_details='$bank_details',status='1' WHERE sl_no='$type_id'");
```

### CVE-2023-49633

The 'buyer_address' parameter of the buyer_detail_submit.php
resource does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$buyer_name=$_POST['buyer_name'] ;
$contact = $_POST['contact'];
$gstin = $_POST['gstn_no'];
$pan_no = $_POST['pan_no'];
$cin_no = $_POST['cin_no'];
$buyer_address = $_POST['buyer_address'];
$status =1;

$qry= mysqli_query($con,"INSERT INTO buyer_details (buyer_name,contact,gstin,pan,cin_no,buyer_address,status) VALUES ('$buyer_name', '$contact','$gstin', '$pan_no','$cin_no','$buyer_address','$status')");
```

### CVE-2023-49639

The 'customer_details' parameter of the buyer_invoice_submit.php
resource does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
<?php
include('config.php');
$customer_details = $_POST['customer_details'];
...
$rowc2 = mysqli_query($con, "SELECT * FROM  `party_details` where sl_no='$customer_details'");
```

### CVE-2023-49641

The 'username' parameter of the loginCheck.php
resource does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
<?php
session_start();
include('config.php');
$username = $_POST['username'];
$password = $_POST['password'];

$sql = mysqli_query($con, "SELECT * FROM ho_role WHERE user_name = '$username' AND password = '$password'");
```

### CVE-2023-49658

The 'bank_details' parameter of the party_submit.php
resource does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$party_name=$_POST['party_name'] ;
$contact = $_POST['contact'];
$cin_no = $_POST['cin_no'];
$pan_no = $_POST['pan_no'];
$party_address = $_POST['party_address'];
$bank_details = $_POST['bank_details'];
$gstn_no = $_POST['gstn_no'];
$adhar_no = $_POST['adhar_no'];
$status =1;

$qry= mysqli_query($con,"INSERT INTO party_details (party_name,contact,gstin,pan_no,cin_no,party_address,bank_details,status) VALUES ('$party_name', '$contact','$gstn_no', '$pan_no','$cin_no','$party_address','$bank_details','$status')");
```

### CVE-2023-49665

The 'quantity[]' parameter of the submit_delivery_list.php
resource does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$quantity = $_POST['quantity'];
...
for($i = 0; $i<count($mat_des); $i++) {
    $sql1 = mysqli_query($con,"INSERT INTO delivery_details(challan_no,offer_no,order_no,dispached,remarks,challan_date,purchase_date,no_of_pkg,net_wt,mat_des,hsn_code,quantity) VALUES ('$challan_no', '$offer_no','$order_no','$dispached','$remarks','$challan_date','$purchase_date','$no_of_pkg','$net_wt', '$mat_des[$i]','$hsn_code[$i]','$quantity[$i]')");

```

### CVE-2023-49666

The 'custmer_details' parameter of the submit_material_list.php
resource does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$custmer_details = $_POST['custmer_details'];
...
$rowc2 = mysqli_query($con, "SELECT * FROM  `party_details` where sl_no='$custmer_details'");
```

## Our security policy

We have reserved the IDs CVE-2023-49622, CVE-2023-49624,
CVE-2023-49625, CVE-2023-49633, CVE-2023-49639,
CVE-2023-49641, CVE-2023-49658, CVE-2023-49665
and CVE-2023-49666
to refer to these issues from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Billing Software v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://www.kashipara.com/>

## Timeline

<time-lapse
discovered="2023-11-24"
contacted="2023-11-24"
disclosure="2023-12-06">
</time-lapse>
