---
slug: advisories/deadmau5/
title: Gridea 0.9.3 - Local File Read
authors: Carlos Bello
writer: cbello
codename: deadmau5
product: Gridea 0.9.3
date: 2022-09-26 15:00 COT
cveid: CVE-2022-40275
severity: 5.5
description: Gridea 0.9.3  -  Local File Read - Insecure or unset HTTP headers
keywords: Fluid Attacks, Security, Vulnerabilities, Gridea, HTTP
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Gridea 0.9.3 - Local File Read"
    code="[Deadmau5](https://en.wikipedia.org/wiki/Deadmau5)"
    product="Gridea"
    affected-versions="Version 0.9.3"
    fixed-versions=""
    state="Public"
    release="2022-09-26">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Insecure or unset HTTP headers - Content-Security-Policy"
    rule="[043. Insecure or unset HTTP headers - Content-Security-Policy](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-043)"
    remote="Yes"
    vector="CVSS:3.1/AV:L/AC:L/PR:N/UI:R/S:U/C:H/I:N/A:N"
    score="5.5"
    available="Yes"
    id="[CVE-2022-40275](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-40275)">
</vulnerability-table>

## Description

Gridea version 0.9.3 allows an external attacker to remotely obtain
arbitrary local files on any client that attempts to view a malicious
markdown file through Gridea. This is possible because the application
does not have a CSP policy (or at least not strict enough) and/or does
not properly validate the contents of markdown files before rendering
them.

## Vulnerability

This vulnerability occurs because the application does not have a CSP policy
(or at least not strict enough) and/or does not properly validate the contents
of markdown files before rendering them. Because of the above, an attacker can
embed malicious JS code in a markdown file and send it to the victim to view and
thus achieve an exfiltration of their local files.

## Exploitation

To exploit this vulnerability, you must send the following file to a user to open
with Gridea. The exploit is triggered when the user presses `CTRL+P` or simply
clicks `preview`.

### exploit.md

```markdown
<img src="1" onerror='fetch("file:///etc/private").then(data => data.text()).then(leak => alert(leak));'/>
```

## Our security policy

We have reserved the CVE-2022-40275 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Gridea 0.9.3

* Operating System: GNU/Linux

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by [Carlos
Bello](https://www.linkedin.com/in/carlos-andres-bello) from Fluid Attacks'
Offensive Team.

## References

**Vendor page** <https://github.com/getgridea/gridea>

## Timeline

<time-lapse
  discovered="2022-09-05"
  contacted="2022-09-05"
  replied=""
  confirmed=""
  patched=""
  disclosure="2022-09-26">
</time-lapse>
