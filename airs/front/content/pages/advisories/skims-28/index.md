---
slug: advisories/skims-28/
title: Auto Featured Image (Auto Post Thumbnail) - Reflected cross-site scripting (XSS)
authors: Andres Roldan
writer: aroldan
codename: skims-28
product: Auto Featured Image (Auto Post Thumbnail) 4.1.
date: 2025-01-03 12:00 COT
cveid: CVE-2025-0773
description: Auto Featured Image (Auto Post Thumbnail) 4.1. - Reflected cross-site scripting (XSS) Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, CVE, SAST
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Auto Featured Image (Auto Post Thumbnail) 4.1. - Reflected cross-site scripting (XSS) |
| **Code name** | skims-28 |
| **Product** | Auto Featured Image (Auto Post Thumbnail) |
| **Affected versions** | Version 4.1. |
| **State** | Private |
| **Release date** | 2025-01-03 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Reflected cross-site scripting (XSS) |
| **Rule** | [Reflected cross-site scripting (XSS)](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-008) |
| **Remote** | No |
| **CVSSv4 Vector** | CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:A/VC:H/VI:L/VA:L/SC:L/SI:L/SA:L/E:U |
| **Exploit available** | No |
| **CVE ID(s)** | [CVE-2025-0773](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-0773) |

## Description

Auto Featured Image (Auto Post
Thumbnail)
4.1.
was found to be vulnerable.
The web application dynamically
generates web content without
validating the source of the
potentially untrusted data in
myapp/includes/class-apt.php.

## Vulnerability

Skims by Fluid Attacks discovered a
Reflected cross-site scripting (XSS)
in
Auto Featured Image (Auto Post
Thumbnail)
4.1..
The following is the output of the tool:

### Skims output

```powershell
   731 |
   732 | erer( 'set_post_thumbnail-' . $post_id );
   733 | T['_ajax_nonce'];
   734 |
   735 | iv class='apt_thumbs' id='wapt_thumbs'>
   736 |   <div class='wapt-grid-item'>
   737 |       <div class=""wapt-image-box-library""
   738 |            data-choose='<?php echo __( 'Choose featured image', 'apt' ); ?>'
   739 |            data-update='<?php echo __( 'Select image', 'apt' ); ?>'
   740 |            data-postid='<?php echo $post_id; ?>'
>  741 |            data-nonce='<?php echo $nonce; ?>'
   742 |            style=""background-color: #a3d2ff;"">
   743 |           <div class=""wapt-item-generated""><?php echo __( 'Set featured image from medialibrary', 'apt' ); ?></div>
   744 |       </div>
   745 |   </div>
   746 | div>
   747 |
   748 |
   749 |
   750 |
   751 | PATH . '/admin/views/pro_column.php';
       ^ Col 18
```

## Our security policy

We have reserved the ID CVE-2025-0773 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Auto Featured Image (Auto Post
Thumbnail)
4.1.

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team using
[Skims](../../plans/#essential)

## Timeline

<time-lapse
  discovered="2025-01-03"
  contacted="2025-01-03"
  replied=""
  confirmed=""
  patched=""
  disclosure="">
</time-lapse>
