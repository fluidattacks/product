---
slug: advisories/gershwin/
title: MSI Afterburner - ACL Bypass
authors: Andres Roldan
writer: aroldan
codename: gershwin
product: MSI Afterburner v4.6.6.16381 Beta 3
date: 2024-05-17 12:00 COT
cveid: CVE-2024-3745
severity: 7.8
description: MSI Afterburner v4.6.6.16381 Beta 3 - ACL Bypass Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, Bypass, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | MSI Afterburner v4.6.6.16381 Beta 3 - ACL Bypass |
| **Code name** | [Gershwin](https://es.wikipedia.org/wiki/George_Gershwin) |
| **Product** | MSI Afterburner |
| **Vendor** | Micro-Star INT'L CO. |
| **Affected versions** | Version 4.6.6.16381 Beta 3 |
| **State** | Public |
| **Release date** | 2024-05-17 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | ACL Bypass |
| **Rule** | [115. Security controls bypass or absence](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-115/) |
| **Remote** | No |
| **CVSSv3 Vector** | CVSS:3.1/AV:L/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H |
| **CVSSv3 Base Score** | 7.8 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2024-3745](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-3745) |

## Description

MSI Afterburner v4.6.6.16381 Beta 3 is vulnerable to
an ACL Bypass vulnerability in the `RTCore64.sys` driver,
which leads to triggering vulnerabilities like
[CVE-2024-1443](https://www.cve.org/CVERecord?id=CVE-2024-1443)
and
[CVE-2024-1460](https://www.cve.org/CVERecord?id=CVE-2024-1460)
from a low privileged user.

## Vulnerability

The `RTCore64.sys` driver allows to perform any IOCTL
operation from a low privileged user.

The creation of the `DeviceObject` is done using the
following code:

```c
RtlInitUnicodeString(&DefaultSDDLString, L"D:P(A;;GA;;;SY)(A;;GA;;;BA)");
  result = WdmlibIoCreateDeviceSecure(
             DriverObject,
             0,
             &DestinationString,
             0x22u,
             0,
             0,
             &DefaultSDDLString,
             0i64,
             &DeviceObject);
```

According to [MSDN](https://learn.microsoft.com/en-us/windows-hardware/drivers/ddi/wdmsec/nf-wdmsec-wdmlibiocreatedevicesecure),
the function signature is:

```c
NTSTATUS WdmlibIoCreateDeviceSecure(
  [in]           PDRIVER_OBJECT   DriverObject,
  [in]           ULONG            DeviceExtensionSize,
  [in, optional] PUNICODE_STRING  DeviceName,
  [in]           DEVICE_TYPE      DeviceType,
  [in]           ULONG            DeviceCharacteristics,
  [in]           BOOLEAN          Exclusive,
  [in]           PCUNICODE_STRING DefaultSDDLString,
  [in, optional] LPCGUID          DeviceClassGuid,
                 PDEVICE_OBJECT   *DeviceObject
);
```

The defined SDDL string limits the access for the
`DeviceObject` to Administrators only:

```powershell
PS C:\Users\aroldan> ConvertFrom-SddlString "D:P(A;;GA;;;SY)(A;;GA;;;BA)" | select -ExpandProperty DiscretionaryAcl
NT AUTHORITY\SYSTEM: AccessAllowed (GenericAll)
BUILTIN\Administrators: AccessAllowed (GenericAll)
PS C:\Users\aroldan>
```

Every `DeviceObject` has an associated name space, which
means that paths like `\Device\DeviceName` and
`\Device\DeviceName\Anything` would be valid targets for
a IOCTL request. However, as the `DeviceCharacteristics`
option is set to `NULL` when creating the `DeviceObject`,
the defined ACL does not propagate to every possible file
under the `DeviceObject` namespace, making it trivial to
bypass the ACL by simply adding a `\` in the end of the
`DeviceName` definition in the IOCTL request:

```powershell
PS C:\Users\admin\Desktop> whoami
desktop-fm7vnok\admin
PS C:\Users\admin\Desktop> whoami /priv

PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                          State
============================= ==================================== ========
SeShutdownPrivilege           Shut down the system                 Disabled
SeChangeNotifyPrivilege       Bypass traverse checking             Enabled
SeUndockPrivilege             Remove computer from docking station Disabled
SeIncreaseWorkingSetPrivilege Increase a process working set       Disabled
SeTimeZonePrivilege           Change the time zone                 Disabled
PS C:\Users\admin\Desktop> .\PoC.exe \\.\RTCore64
[!] Error while creating a handle to the driver: (0) Access is denied.

PS C:\Users\admin\Desktop> .\PoC.exe \\.\RTCore64\
[+] Driver opened. DACL bypassed. Driver handle value: 0x8c.
```

## Our security policy

We have reserved the ID CVE-2024-3745 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: MSI Afterburner v4.6.6.16381 Beta 3
* Operating System: Windows

## Mitigation

The vendor published a the version 4.6.6 Beta 4 Build 16449
fixing this vulnerability:

* https://forums.guru3d.com/threads/msi-ab-rtss-development-news-thread.412822/page-227#post-6231456

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://us.msi.com/>

**Product page** <https://www.msi.com/Landing/afterburner/graphics-cards>

**Patched version** <https://forums.guru3d.com/threads/msi-ab-rtss-development-news-thread.412822/page-227#post-6231456>

## Timeline

<time-lapse
  discovered="2024-04-11"
  contacted="2024-04-11"
  replied="2024-04-13"
  confirmed="2024-04-13"
  patched="2024-05-17"
  disclosure="2024-05-17">
</time-lapse>
