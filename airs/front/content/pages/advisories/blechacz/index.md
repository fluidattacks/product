---
slug: advisories/blechacz/
title: Student Information System v1.0 - SQLi
authors: Andres Roldan
writer: aroldan
codename: blechacz
product: Student Information System v1.0
date: 2023-12-06 12:00 COT
cveid: CVE-2023-5008
severity: 9.8
description: Student Information System v1.0 - Unauthenticated SQL Injection (SQLi)
keywords: Fluid Attacks, Security, Vulnerabilities, SQLi, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Student Information System v1.0 - Unauthenticated SQL Injection (SQLi) |
| **Code name** | [Blechacz](https://en.wikipedia.org/wiki/Rafa%C5%82_Blechacz) |
| **Product** | Student Information System |
| **Vendor** | Kashipara Group |
| **Affected versions** | Version 1.0 |
| **State** | Public |
| **Release date** | 2023-12-06 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Unauthenticated SQL Injection (SQLi) |
| **Rule** | [146. SQL Injection](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-146) |
| **Remote** | Yes |
| **CVSSv3 Vector** | CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H |
| **CVSSv3 Base Score** | 9.8 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2023-5008](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-5008) |

## Description

Student Information System v1.0 is vulnerable to an
unauthenticated SQL Injection vulnerability on the
'regno' parameter of index.php page, allowing an
external attacker to dump all the contents of the
database contents and bypass the login control.

## Vulnerability

The 'regno' parameter of the index.php resource
does not validate the characters received and
it's sent unfiltered to the database.
The vulnerable function is:

```php
$regno=$_POST['regno'];
$password=md5($_POST['password']); // md5 creates 128-bit hash string unique for every unique string
$query=mysqli_query($con,"SELECT * FROM staff WHERE StaffRegno='$regno' and password='$password'");
```

## Our security policy

We have reserved the ID CVE-2023-5008 to refer to this
issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Student Information System v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks'
Offensive Team.

## References

**Vendor page** <https://www.kashipara.com/>

## Timeline

<time-lapse
discovered="2023-11-22"
contacted="2023-11-22"
disclosure="2023-12-06">
</time-lapse>
