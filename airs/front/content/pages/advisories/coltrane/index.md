---
slug: advisories/coltrane/
title: MSI Afterburner v4.6.5.16370 - DoS
authors: Andres Roldan
writer: aroldan
codename: coltrane
product: MSI Afterburner v4.6.5.16370
date: 2024-03-06 12:00 COT
cveid: CVE-2024-1443
severity: 4.4
description: MSI Afterburner v4.6.5.16370 - Denial of Service Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, DoS, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | MSI Afterburner v4.6.5.16370 - Denial of Service |
| **Code name** | [Coltrane](https://en.wikipedia.org/wiki/John_Coltrane) |
| **Product** | MSI Afterburner |
| **Vendor** | Micro-Star INT'L CO. |
| **Affected versions** | Version 4.6.5.16370 |
| **State** | Public |
| **Release date** | 2024-03-06 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Denial of Service (DoS) |
| **Rule** | [002. Asymmetric Denial of Service](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-002/) |
| **Remote** | No |
| **CVSSv3 Vector** | CVSS:3.1/AV:L/AC:L/PR:H/UI:N/S:U/C:N/I:N/A:H |
| **CVSSv3 Base Score** | 4.4 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2024-1443](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-1443) |

## Description

MSI Afterburner v4.6.5.16370 is vulnerable to
a Denial of Service vulnerability by triggering the
`0x80002000` IOCTL code of the `RTCore64.sys` driver.

## Vulnerability

The `0x80002000` IOCTL code of the
`RTCore64.sys` driver allows to perform a Denial of Service,
leading to a BSOD of the affected computer caused by a
NULL pointer dereference. The handle to the driver
can only be obtained from a high integrity process.

The prologue of the vulnerable function `sub_11150` is:

```c
.text:0000000000011150 mov     rax, rsp
.text:0000000000011153 mov     [rax+8], rbx
.text:0000000000011157 mov     [rax+18h], rsi
.text:000000000001115B push    rdi
.text:000000000001115C sub     rsp, 0D0h
.text:0000000000011163 and     [rsp+0D8h+SectionHandle], 0
.text:0000000000011169 and     qword ptr [rax-50h], 0
.text:000000000001116E mov     rdi, rdx     // [1]
.text:0000000000011171 mov     rcx, [rdi]   // [2]
```

At `[1]` value of second parameter on `RDX` register
is assigned to `RDI`. The second parameter is a pointer
to the `SystemBuffer` obtained from the IRP object
(`pIrp->AssociatedIrp.SystemBuffer`)
and it's controlled by the attacker in the `lpInBuffer`
value on the `IOCTL` request call.
At `[2]` the value is dereferenced without checking if
it's a valid memory address, which result in a NULL
pointer dereference when the attacker sends a NULL
`lpInputBuffer` value:

```c
CONTEXT:  fffffc8c97475ce0 -- (.cxr 0xfffffc8c97475ce0)
rax=fffffc8c974767b8 rbx=0000000000000000 rcx=ffffa4027690ed80
rdx=0000000000000000 rsi=0000000000000000 rdi=0000000000000000
rip=fffff8013d1f1171 rsp=fffffc8c974766e0 rbp=0000000000000002
 r8=0000000000000000  r9=0000000000000000 r10=fffff8013d1f143c
r11=0000000000000000 r12=0000000000000000 r13=0000000000000000
r14=ffffa4027a24d5f0 r15=ffffa4027690ed80
iopl=0         nv up ei pl zr na po nc
cs=0010  ss=0018  ds=002b  es=002b  fs=0053  gs=002b             efl=00050246
RTCore64+0x1171:
fffff801`3d1f1171 488b0f          mov     rcx,qword ptr [rdi] ds:002b:00000000`00000000=????????????????
Resetting default scope

PROCESS_NAME:  IOCTLBruteForce.exe

STACK_TEXT:
fffffc8c`974766e0 fffff801`3d1f16cc     : 00000000`00000000 00000000`0000002d 00000000`00000000 fffff801`36edce00 : RTCore64+0x1171
fffffc8c`974767c0 fffff801`364d1f35     : ffffa402`724b03e0 ffffa402`724b03e0 fffffc8c`97476b80 00000000`00000001 : RTCore64+0x16cc
```

## Our security policy

We have reserved the ID CVE-2024-1443 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: MSI Afterburner v4.6.5.16370
* Operating System: Windows

## Mitigation

The vendor published a the version 4.6.6 Beta 4 Build 16449
fixing this vulnerability:

* https://forums.guru3d.com/threads/msi-ab-rtss-development-news-thread.412822/page-227#post-6231456

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://us.msi.com/>

**Product page** <https://www.msi.com/Landing/afterburner/graphics-cards>

**Patched version** <https://forums.guru3d.com/threads/msi-ab-rtss-development-news-thread.412822/page-227#post-6231456>

## Timeline

<time-lapse
discovered="2024-02-08"
contacted="2024-02-23"
patched="2024-05-17"
disclosure="2024-03-06">
</time-lapse>
