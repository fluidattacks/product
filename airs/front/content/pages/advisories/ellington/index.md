---
slug: advisories/ellington/
title: Zemana AntiLogger - Process Termination
authors: Andres Roldan
writer: aroldan
codename: ellington
product: Zemana AntiLogger v2.74.204.664
date: 2024-03-14 12:00 COT
cveid: CVE-2024-1853
severity: 5.5
description: Zemana AntiLogger v2.74.204.664 - Arbitrary Process Termination Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, Leak, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Zemana AntiLogger v2.74.204.664 - Arbitrary Process Termination |
| **Code name** | [Ellington](https://en.wikipedia.org/wiki/Duke_Ellington) |
| **Product** | Zemana AntiLogger |
| **Vendor** | Zemana Ltd. |
| **Affected versions** | Version 2.74.204.664 |
| **State** | Public |
| **Release date** | 2024-03-14 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Arbitrary Process Termination |
| **Rule** | [014. Insecure functionality](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-014/) |
| **Remote** | No |
| **CVSSv3 Vector** | CVSS:3.1/AV:L/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H |
| **CVSSv3 Base Score** | 5.5 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2024-1853](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-1853) |

## Description

Zemana AntiLogger v2.74.204.664 is vulnerable to
an Arbitrary Process Termination vulnerability by
triggering the `0x80002048` IOCTL code of the
`zam64.sys` and `zamguard64.sys` drivers.

## Vulnerability

The `0x80002048` IOCTL code of the
`zam64.sys` and `zamguard64.sys` drivers allow to kill
arbitrary processes on the system where it's installed, by
sending a process ID on the first `DWORD` of the
`lpInBuffer` parameter request call.

In order to perform calls to any IOCTL of the
`zam64.sys` and `zamguard64.sys` driver, a call to the
IOCTL `0x80002010` must be performed
with the current process ID as an authorized IOCTL process
caller:

```c
if ( IoctlCode != 0x80002010 )
{
    if ( IoctlCode + 0x7FFFDFAC > 0x10
    || (CurrentStackLocation = 0x11001i64, !_bittest((const int *)&CurrentStackLocation, IoctlCode + 0x7FFFDFAC)) )
    {
    if ( (unsigned int)sub_140009BE4(CurrentStackLocation, "Main.c") && !(unsigned int)sub_140009BEC(v6, 1i64) )
    {
        v3 = 0xC0000022;
        DnsPrint_RpcZoneInfo(
        7,
        (unsigned int)"Main.c",
        0x1E2,
        (unsigned int)"DeviceIoControlHandler",
        0xC0000022,
        "ProcessID %d is not authorized to send IOCTLs ",
        v6);
        goto LABEL_79;
    }
    }
}
```

The handling decompiled code of the `0x80002048` IOCTL
starts with:

```c
case 0x80002048:
    v3 = sub_14001048C(SystemBuffer);
```

The `sub_14001048C` routine calls `sub_1400133D0`:

```c
__int64 __fastcall sub_14001048C(unsigned int *a1)
{
  return sub_1400133D0(*a1, a1[1], 6i64);
}
```

The `sub_1400133D0` is the vulnerable function:

```c
ProcessHandle = 0i64;
v11 = 0;
v4 = 0xC0000001;
Timeout.QuadPart = 0xFFFFFFFFFF676980ui64;
if ( (unsigned int)sub_140005994((void *)pSystemBuffer, &v11) && v11 )  // [1]
{
    DnsPrint_RpcZoneInfo(
        5,
        (unsigned int)"ProcessHelper\\ProcessHelper.c",
        0x1ED,
        (unsigned int)"ZmnPhTerminateProcessById",
        0,
        "Critical process termination attempt blocked");
    return (unsigned int)v4;
}
v4 = sub_140013268(&ProcessHandle, pSystemBuffer, 1u, 1);  // [2]
if ( v4 >= 0 )
{
    v4 = ZwTerminateProcess(ProcessHandle, 0);  // [3]
```

At `[1]` a check is perform to prevent critical processes
termination. At `[2]` a handle of the process passed as an ID
on the `SystemBuffer` is obtained. At `[3]` that handle is
used as a parameter of the `ZwTerminateProcess` call which
terminates the process.

## Evidence of exploitation

![evidence1](https://res.cloudinary.com/fluid-attacks/image/upload/v1709598354/airs/advisories/gomez/Screen_Recording_2024-02-22_at_11.02.42.gif)

## Our security policy

We have reserved the ID CVE-2024-1853 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Zemana AntiLogger v2.74.204.664
* Operating System: Windows

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://zemana.com/>

**Product page** <https://zemana.com/us/antilogger.html>

## Timeline

<time-lapse
discovered="2024-02-23"
contacted="2024-03-04"
disclosure="2024-03-14">
</time-lapse>
