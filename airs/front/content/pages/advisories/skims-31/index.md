---
slug: advisories/skims-31/
title: Image Hover Effects Ultimate - Reflected cross-site scripting (XSS)
authors: Andres Roldan
writer: aroldan
codename: skims-31
product: Image Hover Effects Ultimate 9.9.4
date: 2025-01-03 12:00 COT
cveid: CVE-2025-0774
description: Image Hover Effects Ultimate 9.9.4 - Reflected cross-site scripting (XSS) Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, CVE, SAST
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Image Hover Effects Ultimate (Image Gallery, Effects, Lightbox, Comparison or Magnifier) 9.9.4 - Reflected cross-site scripting (XSS) |
| **Code name** | skims-31 |
| **Product** | Image Hover Effects Ultimate (Image Gallery, Effects, Lightbox, Comparison or Magnifier) |
| **Affected versions** | Version 9.9.4 |
| **State** | Private |
| **Release date** | 2025-01-03 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Reflected cross-site scripting (XSS) |
| **Rule** | [Reflected cross-site scripting (XSS)](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-008) |
| **Remote** | No |
| **CVSSv4 Vector** | CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:A/VC:H/VI:L/VA:L/SC:L/SI:L/SA:L/E:U |
| **Exploit available** | No |
| **CVE ID(s)** | [CVE-2025-0774](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-0774) |

## Description

Image Hover Effects Ultimate (Image
Gallery, Effects, Lightbox, Comparison
or Magnifier)
9.9.4
was found to be vulnerable.
The web application dynamically
generates web content without
validating the source of the
potentially untrusted data in
myapp/Classes/Support_Recommended.php.

## Vulnerability

Skims by Fluid Attacks discovered a
Reflected cross-site scripting (XSS)
in
Image Hover Effects Ultimate (Image
Gallery, Effects, Lightbox, Comparison
or Magnifier)
9.9.4.
The following is the output of the tool:

### Skims output

```powershell
  137 |     {
  138 |
  139 |         $wpnonce = sanitize_key(wp_unslash($_POST['_wpnonce']));
  140 |         if (!wp_verify_nonce($wpnonce, 'image_hover_ultimate')) :
  141 |             return new \WP_REST_Request('Invalid URL', 422);
  142 |             die();
  143 |         endif;
  144 |
  145 |         $notice = $_POST['notice'];
  146 |         update_option('oxi_image_hover_recommended', $notice);
> 147 |         echo $notice;
  148 |         die();
  149 |     }
  150 |
  151 |
  152 |     public function extension()
  153 |     {
  154 |         $response = get_transient(self::GET_LOCAL_PLUGINS);
  155 |         if (!$response || !is_array($response)) {
  156 |             $URL = self::PLUGINS;
  157 |             $request = wp_remote_request($URL);
      ^ Col 0
```

## Our security policy

We have reserved the ID CVE-2025-0774 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Image Hover Effects Ultimate 9.9.4

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team using
[Skims](../../plans/#essential)

## Timeline

<time-lapse
  discovered="2025-01-03"
  contacted="2025-01-03"
  replied=""
  confirmed=""
  patched=""
  disclosure="">
</time-lapse>
