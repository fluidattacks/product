---
slug: advisories/shagrath/
title: Online Book Store Project v1.0 - Insecure File Upload
authors: Andres Roldan
writer: aroldan
codename: Shagrath
product: Online Book Store Project v1.0
date: 2023-09-28 12:00 COT
cveid: CVE-2023-43740
severity: 9.1
description: Online Book Store Project v1.0 - Insecure File Upload
keywords: Fluid Attacks, Security, Vulnerabilities, CVE, Advisory
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Online Book Store Project v1.0 - Insecure File Upload"
    code="[Shagrath](https://en.wikipedia.org/wiki/Shagrath)"
    product="Online Book Store Project"
    vendor="Projectworlds Pvt. Limited"
    affected-versions="Version 1.0"
    fixed-versions=""
    state="Public"
    release="2023-09-28">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Insecure File Upload"
    rule="[027. Insecure File Upload](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-027/)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:C/C:H/I:H/A:H"
    score="9.1"
    available="Yes"
    id="[CVE-2023-43740](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43740)">
</vulnerability-table>

## Description

Online Book Store Project v1.0 is vulnerable to
an Insecure File Upload vulnerability on the 'image'
parameter of admin_edit.php page, allowing an
authenticated attacker to obtain Remote Code Execution
on the server hosting the application.

## Vulnerability

The 'image' parameter of the admin_edit.php resource
does not validate the contents, extension and type of
the file uploaded as a book image, leading to an
arbitrary file upload which can be abused to obtain
Remote Code Execution. The vulnerable code is located
at edit_book.php:

```php
if(isset($_FILES['image']) && $_FILES['image']['name'] != ""){
    $image = $_FILES['image']['name'];
    $directory_self = str_replace(basename($_SERVER['PHP_SELF']), '', $_SERVER['PHP_SELF']);
    $uploadDirectory = $_SERVER['DOCUMENT_ROOT'] . $directory_self . "bootstrap/img/";
    $uploadDirectory .= $image;
    move_uploaded_file($_FILES['image']['tmp_name'], $uploadDirectory);
}
```

## Evidence of exploitation

![evidence1](https://res.cloudinary.com/fluid-attacks/image/upload/v1695331753/airs/advisories/shagrath/evidence1.webp)

## Our security policy

We have reserved the ID CVE-2023-43740
to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Online Book Store Project v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://projectworlds.in/>

## Timeline

<time-lapse
discovered="2023-09-21"
contacted="2023-09-21"
disclosure="2023-09-28">
</time-lapse>
