---
slug: advisories/adderley/
title: Vba32 Antivirus v3.36.0 - AMR
authors: Andres Roldan
writer: aroldan
codename: adderley
product: Vba32 Antivirus v3.36.0
date: 2024-01-29 12:00 COT
cveid: CVE-2024-23439,CVE-2024-23440
severity: 6.3
description: Vba32 Antivirus v3.36.0 - Arbitrary Memory Read Advisory
keywords: Fluid Attacks, Security, Vulnerabilities, DoS, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Vba32 Antivirus v3.36.0 - Arbitrary Memory Read |
| **Code name** | [Adderley](https://en.wikipedia.org/wiki/Cannonball_Adderley) |
| **Product** | Vba32 Antivirus |
| **Vendor** | VirusBlokAda |
| **Affected versions** | Version 3.36.0 |
| **State** | Public |
| **Release date** | 2024-01-29 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Arbitrary Memory Read |
| **Rule** | [111. Out-of-bounds Read](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-111/) |
| **Remote** | No |
| **CVSSv3 Vector** | CVSS:3.1/AV:L/AC:H/PR:L/UI:N/S:U/C:H/I:N/A:H |
| **CVSSv3 Base Score** | 6.3 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2024-23439](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-23439), [CVE-2024-23440](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-23440) |

## Description

Vba32 Antivirus v3.36.0 is vulnerable to
an Arbitrary Memory Read (AMR) vulnerability by triggering the
0x22201B, 0x22201F, 0x222023, 0x222027 ,0x22202B, 0x22202F,
0x22203F, 0x222057 and 0x22205B IOCTL codes of the
Vba32m64.sys driver.

## CVE-2024-23439

The 0x22201B, 0x22201F, 0x222023, 0x222027 ,0x22202B, 0x22202F,
0x22203F, 0x222057 and 0x22205B IOCTL codes of the `Vba32m64.sys`
driver allows to perform a partial Arbitrary Memory Read. The
attacker can control the address from where to perform the read
by supplying an arbitrary pointer in the `lpInBuffer`
parameter of the IOCTL call, but there's currently not leak of
the result of such read to user-space.
However, the invalid address will cause a BSOD which leads to
a Denial of Service of the affected computer.

```bash
rax=000000000022201b rbx=000000000022201b rcx=4141414141414141
rdx=ffffb001f2328190 rsi=4141414141414141 rdi=ffffb001f2328190
rip=fffff800672b1383 rsp=ffffe38230292750 rbp=000001e1d8b10000
 r8=000000000000000e  r9=ffffb001f24910a0 r10=fffff800672b9724
r11=0000000000000000 r12=0000000000000008 r13=0000000000000038
r14=ffffb001f6eb61e0 r15=ffffb001f24910a0
iopl=0         nv up ei ng nz na po nc
cs=0010  ss=0018  ds=002b  es=002b  fs=0053  gs=002b             efl=00050286
Vba32m64+0x1383:
fffff800`672b1383 0fb701          movzx   eax,word ptr [rcx] ds:002b:41414141`41414141=????
```

## CVE-2024-23440

The 0x22200B IOCTL code of the `Vba32m64.sys`
driver allows to read up to 0x802 of memory from ar arbitrary
user-supplied pointer. The attacker can control the address
from where to perform the read by supplying an arbitrary
pointer in the `lpInBuffer` parameter of the IOCTL call.
The vulnerable IOCTL will copy up to 0x802 bytes to a global
variable of the driver. There's not evidence of the leak of
the result of such read to user-space.
However, the invalid address will cause a BSOD which leads to
a Denial of Service of the affected computer.

```bash
1: kd> dps rdx
00000000`ffff0000  ????????`????????
00000000`ffff0008  ????????`????????
00000000`ffff0010  ????????`????????
00000000`ffff0018  ????????`????????
00000000`ffff0020  ????????`????????
00000000`ffff0028  ????????`????????

rax=000000000022201b rbx=0000000000000000 rcx=fffff80349241530
rdx=000007fdb6daf2d0 rsi=00000000ffff0000 rdi=ffffe70f541c69d0
rip=fffff8034923a0e1 rsp=ffffc80bdb061778 rbp=00000000ffff0000
 r8=0000000000000802  r9=ffffe70f540b60a0 r10=fffff80349239724
r11=fffff80349240d30 r12=0000000000000802 r13=0000000000000802
r14=ffffe70f5819ebc0 r15=ffffe70f540b60a0
iopl=0         nv up ei ng nz na po nc
cs=0010  ss=0018  ds=002b  es=002b  fs=0053  gs=002b             efl=00050286
Vba32m64+0xa0e1:
fffff803`4923a0e1 668b040a        mov     ax,word ptr [rdx+rcx] ds:002b:00000000`ffff0800=????
```

## Our security policy

We have reserved the IDs CVE-2024-23439 and CVE-2024-23440
to refer to these issues from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Vba32 Antivirus v3.36.0
* Operating System: Windows

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://www.anti-virus.by/>

**Product page** <https://www.anti-virus.by/vba32>

## Timeline

<time-lapse
discovered="2024-01-16"
contacted="2024-01-16"
replied="2024-01-18"
disclosure="2024-01-29">
</time-lapse>
