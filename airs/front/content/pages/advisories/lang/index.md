---
slug: advisories/lang/
title: Hotel Management v1.0 - Reflected XSS
authors: Andres Roldan
writer: aroldan
codename: lang
product: Hotel Management v1.0
date: 2023-12-06 12:00 COT
cveid: CVE-2023-49269,CVE-2023-49270,CVE-2023-49271,CVE-2023-49272
severity: 5.4
description: Hotel Management v1.0 - Multiple Reflected Cross-Site Scripting (XSS)
keywords: Fluid Attacks, Security, Vulnerabilities, XSS, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Hotel Management v1.0 - Multiple Reflected Cross-Site Scripting (XSS) |
| **Code name** | [Lang](https://en.wikipedia.org/wiki/Lang_Lang) |
| **Product** | Hotel Management |
| **Vendor** | Kashipara Group |
| **Affected versions** | Version 1.0 |
| **State** | Public |
| **Release date** | 2023-12-06 |

## Vulnerabilities

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Reflected Cross-Site Scripting (XSS) |
| **Rule** | [008. Reflected cross-site scripting](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-008/) |
| **Remote** | Yes |
| **CVSSv3 Vector** | CVSS:3.1/AV:N/AC:L/PR:N/UI:R/S:U/C:N/I:L/A:L |
| **CVSSv3 Base Score** | 5.4 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2023-49269](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-49269), [CVE-2023-49270](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-49270), [CVE-2023-49271](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-49271), [CVE-2023-49272](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-49272)|

## Description

Hotel Management v1.0 is vulnerable to
multiple authenticated Reflected Cross-Site Scripting
vulnerabilities.

## Vulnerabilities

### CVE-2023-49269

The 'adults' parameter of the reservation.php resource
is copied into the HTML document as plain text
between tags. Any input is echoed unmodified in the
application's response. The vulnerable
code is:

```php
if (isset($_GET["check_in_date"])) {
    $check_in_date = $_GET["check_in_date"];
    $check_out_date = $_GET["check_out_date"];
    $no_children = $_GET["children"];
    $no_adults = $_GET["adults"];
}
...
<div class="form-group">
    <label for="no_adults">Adults</label>
    <input type="number" class="form-control" name="no_adults" id="no_adults" value="<?php if (isset($no_adults)) {
    echo $no_adults;
} ?>">
</div>
```

### CVE-2023-49270

The 'check_in_date' parameter of the reservation.php
resource is copied into the HTML document as plain text
between tags. Any input is echoed unmodified in the
application's response. The vulnerable
code is:

```php
if (isset($_GET["check_in_date"])) {
    $check_in_date = $_GET["check_in_date"];
    $check_out_date = $_GET["check_out_date"];
    $no_children = $_GET["children"];
    $no_adults = $_GET["adults"];
}
...
<div class="form-group">
    <span class="form-label">Check In</span>
    <input class="form-control" name="check_in_date" id="check_in_date" type="text" required value="<?php if (isset($check_in_date)) {
echo $check_in_date;
} ?>"/>
</div>
```

### CVE-2023-49271

The 'check_out_date' parameter of the reservation.php resource
is copied into the HTML document as plain text
between tags. Any input is echoed unmodified in the
application's response. The vulnerable
code is:

```php
if (isset($_GET["check_in_date"])) {
    $check_in_date = $_GET["check_in_date"];
    $check_out_date = $_GET["check_out_date"];
    $no_children = $_GET["children"];
    $no_adults = $_GET["adults"];
}
...
<div class="form-group">
    <span class="form-label">Check out</span>
    <input class="form-control" name="check_out_date" id="check_out_date" type="text" required value="<?php if (isset($check_out_date)) {
echo $check_out_date;
} ?>"/>
</div>
```

### CVE-2023-49272

The 'children' parameter of the reservation.php resource
is copied into the HTML document as plain text
between tags. Any input is echoed unmodified in the
application's response. The vulnerable
code is:

```php
if (isset($_GET["check_in_date"])) {
    $check_in_date = $_GET["check_in_date"];
    $check_out_date = $_GET["check_out_date"];
    $no_children = $_GET["children"];
    $no_adults = $_GET["adults"];
}
...
<div class="form-group">
    <label for="no_children">Children</label>
    <input type="number" class="form-control" name="no_children" id="no_children" value="<?php if (isset($no_children)) {
    echo $no_children;
} ?>">
</div>
```

## Our security policy

We have reserved the IDs CVE-2023-49269, CVE-2023-49270,
CVE-2023-49271 and CVE-2023-49272 to refer
to these issues from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Hotel Management v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://www.kashipara.com/>

## Timeline

<time-lapse
discovered="2023-11-23"
contacted="2023-11-23"
disclosure="2023-12-06">
</time-lapse>
