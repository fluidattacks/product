---
slug: advisories/skims-15/
title: Kata Plus - Insecure deserialization
authors: Andres Roldan
writer: aroldan
codename: skims-15
product: Kata Plus 1.5.2
date: 2025-01-03 12:00 COT
cveid: CVE-2025-0768
description: Kata Plus 1.5.2 - Insecure deserialization Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, CVE, SAST
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Kata Plus - Addons for Elementor - Widgets, Extensions and Templates 1.5.2 - Insecure deserialization |
| **Code name** | skims-15 |
| **Product** | Kata Plus - Addons for Elementor - Widgets, Extensions and Templates |
| **Affected versions** | Version 1.5.2 |
| **State** | Private |
| **Release date** | 2025-01-03 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Insecure deserialization |
| **Rule** | [Insecure deserialization](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-096) |
| **Remote** | No |
| **CVSSv4 Vector** | CVSS:4.0/AV:N/AC:H/AT:N/PR:N/UI:N/VC:N/VI:L/VA:L/SC:N/SI:N/SA:N/E:U |
| **Exploit available** | No |
| **CVE ID(s)** | [CVE-2025-0768](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-0768) |

## Description

Kata Plus - Addons for Elementor -
Widgets, Extensions and Templates
1.5.2
was found to be vulnerable.
Unvalidated user input is used directly
in an unserialize function in
myapp/includes/theme-options/plugins/cus
tomizer-export-import/classes/class-cei-
core.php.

## Vulnerability

Skims by Fluid Attacks discovered a
Insecure deserialization
in
Kata Plus - Addons for Elementor -
Widgets, Extensions and Templates
1.5.2.
The following is the output of the tool:

### Skims output

```powershell
  254 |    $cei_error = $file['error'];
  255 |    return;
  256 |   }
  257 |   if ( ! file_exists( $file['file'] ) ) {
  258 |    $cei_error = __( 'Error importing settings! Please try again.', 'kata-plus' );
  259 |    return;
  260 |   }
  261 |
  262 |   // Get the upload data.
  263 |   $raw  = file_get_contents( $file['file'] );
> 264 |   $data = @unserialize( $raw );
  265 |
  266 |   // Remove the uploaded file.
  267 |   unlink( $file['file'] );
  268 |
  269 |   // Data checks.
  270 |   if ( 'array' != gettype( $data ) ) {
  271 |    $cei_error = __( 'Error importing settings! Please check that you uploaded a customizer export file.', 'kata-plus' );
  272 |    return;
  273 |   }
  274 |   if ( ! isset( $data['template'] ) || ! isset( $data['mods'] ) ) {
      ^ Col 0
```

## Our security policy

We have reserved the ID CVE-2025-0768 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Kata Plus - Addons for Elementor -
Widgets, Extensions and Templates
1.5.2

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team using
[Skims](../../plans/#essential)

## Timeline

<time-lapse
  discovered="2025-01-03"
  contacted="2025-01-03"
  replied=""
  confirmed=""
  patched=""
  disclosure="">
</time-lapse>
