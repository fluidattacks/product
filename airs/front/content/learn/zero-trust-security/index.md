---
slug: zero-trust-security/
title: Zero Trust Security Model
date: 2024-04-26
subtitle: Never trust. Always verify.
category: philosophy
tags: cybersecurity, company, trend, risk, software
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1714084288/learn/zero-trust-security/cover_zero_trust_security.webp
alt: Photo by Kai Pilger on Unsplash
description: An overview of the cybersecurity philosophy that inherently says “no” to anyone and anything trying to access a company’s digital resources.
keywords: Zero Trust, Zero Trust Security, Zero Trust Philosophy, Zero Trust Architecture, Security Model, Principles Of Zero Trust, Castle And Moat, Ethical Hacking, Pentesting
author: Wendy Rodriguez
writer: wrodriguez
name: Wendy Rodriguez
about1: Content Writer and Editor
source: https://unsplash.com/photos/closeup-photo-of-street-go-and-stop-signage-displaying-stop-1k3vsv7iIIc
---

Today’s cybercriminals are more cunning and ambitious than ever.
They have the means, the time and the intelligence
to attempt to hack into anything (network or technological asset)
they believe to be vulnerable.
There are always ways to enhance your company’s security posture:
Steps to take in order to reduce the possibilities of harmful cyberattacks.
One of the strategies that has been making waves
is the zero trust philosophy and its model.

This model was coined in [2010](https://www.techtarget.com/whatis/feature/History-and-evolution-of-zero-trust-security)
and has become more and more popular
as a result of the complex and evolving cybersecurity landscape,
the expansion of cloud computing and the necessity for remote access,
and the rise of stricter privacy regulations that call
for more comprehensive security strategies.
[Gartner](https://www.gartner.com/en/publications/zero-trust-principles-to-improve-security-playbook#:~:text=Public%2Dsector%20IT%20leaders%20are,fail%20to%20realize%20the%20benefits.)
predicts that “more than 60% of organizations
will embrace zero-trust principles as a starting place
for security by 2025.”
The goal of the zero trust architecture
is to eliminate inherent trust and implement strong identity
and access management (IAM) controls,
which would lower a company’s cyber risk by reducing access
to only essential resources or data inside its perimeter
and preventing unauthorized access.

Let’s first understand what companies around the world
have been working with so far and why it has made them vulnerable
to some kinds of attacks.

## Good old castle-and-moat

Let’s imagine a building with residential apartments,
social areas, administrative offices and commercial spaces.
Every time a resident enters the building,
they are required to provide proof of residency.
After this is completed,
residents have free rein and can enter any location they like,
even apartments that don’t belong to them.
They are trusted to move without encumbrance.

Traditional network security relies on that “trusted by default” concept,
and it’s called the “castle-and-moat” approach.
Consider the network within a company as a castle
and the network perimeter as a moat,
trusting anyone and anything inside the network.
No one outside it is able to access the data,
but once inside the network, lateral movement becomes easy.
Companies had relied on this approach when all of their data
and resources resided in one physical place,
an on-premises data center,
to which employees accessed through a company-owned device
and they were located in the company’s base office.
However,
several factors have rendered the castle-and-moat approach obsolete.
The 2020 pandemic sped up this process,
making remote work and access necessary.
Cloud services have become an intricate part of modern IT structures,
killing on-premises data centers.
Cyber threats keep evolving,
finding ways to bypass traditional network security.
These factors, among others,
have left the castle-and-moat approach behind,
but it’s not like it was ever completely secure.

The castle-and-moat approach assumes
that attacks originate on the outside of the network,
concentrating on protecting its perimeter.
Nevertheless,
threats do also come from within,
making this approach insufficient and vulnerable.
For example, if a malicious actor gains access to credentials
(stealing them or buying them on the dark web),
this approach wouldn’t detect or be able to stop the attacks,
permitting the attacker to move laterally and create havoc.
Companies that use this approach have
to employ other resources to defend their perimeter
with security tools like firewalls,
intrusion prevention systems (IPS) and intrusion detection systems (IDS),
which, needless to say, has a greater monetary cost.

It has become absolutely necessary for companies
to find other ways to secure their resources,
through more efficient approaches
that solve the fundamental flaw of inherent trust
within a perimeter.
Zero trust security addresses that flaw
by continuously verifying access requests
and granting only minimum permissions.

## What is zero trust security?

Imagine another building.
This building has the same areas as the building above,
but the residents in this building can only enter their apartments
and can do so only after they have been properly validated.
The residents must obtain prior authorization
and pass strict verification,
just as they did when they entered their apartments,
to access any other areas of the building.
The zero trust security architecture
is based on this regular monitoring methodology
and provides a set of principles to design
and implement secure IT networks.
The main idea in this philosophy
is “**never trust, always verify**,”
which means that authentication and authorization are necessary
for every user or device trying to access company resources,
no matter their location
(company’s office, home office, co-workings, work cafes, etc.)
This eliminates the inherent trust
or “trust by default” of legacy models
by assuming that threats could originate from wherever.

Zero trust security relies on the idea
that no one and nothing can be inherently trusted.
Its principles are grounds that, if applied correctly,
provide a more secure company posture
that has the ability to face an attack,
which could happen at any time.
That is why zero trust is important.
It’s a robust cybersecurity strategy that counters risks
like those associated with credential theft, malware or ransomware,
social engineering and vulnerability exploitation.
So, what principles are we talking about?

### Zero trust principles

Zero trust security should be built
on a foundation of core principles that stand
up against traditional approaches.
These principles are:

- **Least-privilege access**: Users are granted the bare minimum level
  of access to networks and systems required to perform their tasks.
  This limits the potential reach an attacker
  might have if access is gained.
  Privileges can always be elevated after trust brokers
  examine each request thoroughly.

- **Continuous authentication and monitoring**: Identity confirmation
  on an ongoing basis ensures that users are who they say they are.
  Strong IAM practices are crucial for verifying access requests
  and ensuring only authorized entities can use specific resources.
  Moreover, user activity, network traffic, location and device health
  are monitored for anomalies and suspicious behavior.
  This allows for early detection of possible breaches
  and enables a quick response to contain threats.

- **Microsegmentation**: This practice advocates segmenting networks
  into smaller zones to keep workloads separate.
  It’s a technique that limits the “blast radius” in case of a breach,
  restricting an attacker from moving laterally.

- **Assume breaches**: This principle is the presumption
  that attacks will happen and promotes
  a company to prioritize detection, response,
  and quick recovery to minimize the impacts of security breaches.

### Zero trust vs castle-and-moat

<image-block>

!["Zero trust vs castle and moat"](https://res.cloudinary.com/fluid-attacks/image/upload/v1714170047/learn/zero-trust-security/zt-vs-cam-one.webp)

Differences between zero trust and castle-and-moat approaches

</image-block>

### Which components are used in zero trust?

Several components are available
in the market to implement and establish zero trust security.
The following are some of the most used technologies and features:

- **Multi-factor authentication (MFA)**: User and device multi-step process
  used for identity management in which at least
  two proving factors are needed to access.
  That evidence can be one-time passwords (OTPs),
  security tokens, security questions,
  and biometrics like fingerprints.

- **Encryption**: Information is transformed
  into code using this procedure to prevent unauthorized access.
  PII (personally identifiable information)
  should be kept encrypted, along with sensitive data like network maps,
  legal documents, and software information.

- **Active Directory (AD)**: This is a Microsoft directory service
  where administrators can control access
  to network resources and permissions.

- **Zero Trust Network Access (ZTNA)**: [ZTNA](../../blog/zero-trust-network-access/)
  is the technology solution
  most commonly used for zero trust security.
  It restricts access to the particular resources that users or devices need.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/security-testing/"
title="Get started with Fluid Attacks' Security Testing solution right now"
/>
</div>

## How to implement zero trust

As aforementioned,
zero trust moves away from the perimeter-based approach
and inherent trust, constantly verifying access
and minimizing privileges.
For companies with an established legacy model,
the zero trust architecture implementation
can be somewhat complex at first.
But by adopting a staged implementation,
doing an ROI and budget analysis,
using integration tools and making training sessions for employees,
the zero trust adoption process can become less stressful.

The following are five steps to implement
the zero trust security model,
keeping in mind every company's demands are unique:

1. **Define the attack surface**: Identify critical assets
   (data and resources) and map data flows
   (how your data moves across your network).

2. **Map out network requests**: Make an inventory of users
   and devices that need access,
   specifying with certainty who
   or what needs access to which resource.

3. **Create a policy for zero trust**: Define security standards
   and establish clear and granular access control policies.

4. **Choose your zero trust components**: Decide which
   zero trust security components
   you’ll be using in your infrastructure,
   choosing the ideal solution for your company.

5. **Monitor and maintain the network**: Regularly review access controls
   to ensure they remain effective,
   develop an incident response plan and educate users
   on zero trust best practices.

### Zero Trust security model with Fluid Attacks

As a cybersecurity company focused on AppSec,
we firmly believe that zero trust promotes
some of the best security practices available
and whose ultimate goal is to keep companies safe.
We trust it so much that we follow the zero trust philosophy
within our company and with our clients.
To those clients who want to comply with
a zero trust model,
we can offer them certain tactics that follow this philosophy.

Our [CSPM](../../product/cspm/) (cloud security posture management)
acts as a strong ally to the zero trust model
by constantly assessing cloud configurations.
CSPM can identify excessive or illogical privileges,
for example when a machine allows SSH access
from an unspecified network.
We would report this situation and provide certain recommendations
(like whitelisting expected IPs)
to guarantee the zero trust principle of least privilege is met.

CSPM is complemented by our testing methods,
such as static application security testing ([SAST](../../product/sast/)),
dynamic application security testing ([DAST](../../product/dast/))
and software composition analysis ([SCA](../../product/sca/)),
to further strengthen the zero trust posture
by ensuring vulnerabilities are being discovered promptly.

We look for security policies that are poorly applied
and report on how they can result in vulnerability exploitation
and, consequently, impact our client’s zero trust architecture.
We can assist your company in maintaining a high-level security plan
that keeps everything and everyone farther away
from cybercriminals and their methods.
We also require that certain applications
employ multi-factor authentication,
promote credential verification and ensure every login session
complies with strong password policies.

Our [security testing](../../solutions/security-testing/)
solution can be a valuable tool
for security teams to identify
and fix vulnerabilities that could reduce
the effectiveness of the zero trust model.
For example, when we check our clients' apps for vulnerabilities,
we report when there isn't a proper authorization process established
or any other situation that compromises their security policies.
After a comprehensive evaluation,
our pentesters provide you with recommendations
that align with the zero trust security model.

If you want more information
on how our solutions help your security posture,
[contact us](../../contact-us/) now.
