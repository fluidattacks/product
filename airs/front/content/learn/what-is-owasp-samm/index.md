---
slug: what-is-owasp-samm/
title: OWASP SAMM
date: 2023-11-17
subtitle: The OWASP framework to achieve security maturity
category: philosophy
tags: risk, web, software, trend, cybersecurity, compliance
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1700256635/learn/owasp%20samm/cover_owasp_samm_1.webp
alt: Photo by Sajad Nori on Unsplash
description: Understand the OWASP SAMM framework to formulate and implement a proactive strategy for software security maturity.
keywords: Fluid Attacks, Owasp, Samm, Open Worldwide Application Security Project, Software Assurance Maturity Model, Compliance, Web, App, Security, Ethical Hacking, Pentesting
author: Wendy Rodriguez
writer: wrodriguez
name: Wendy Rodriguez
about1: Content Writer and Editor
source: https://unsplash.com/photos/person-holding-black-and-white-round-ornament-21mJd5NUGZU
---

## OWASP SAMM

In today’s digital world,
software is an essential part of almost every business.
As a result, it is increasingly important for organizations
to ensure that their software is secure.
OWASP is a renowned community in software security that,
with projects like the OWASP SAMM framework,
provides organizations with a helping hand that aims
to reach the ultimate goal: secure software.

## What is OWASP?

OWASP (Open Worldwide Application Security Project)
is a non-profit organization that focuses
on the improvement of security in software systems.
OWASP provides several resources to help software developers
and security professionals understand
and address common security vulnerabilities in software.
These resources include the [OWASP ASVC](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-owaspmasvs),
a framework of security requirements,
and the [OWASP Top Ten](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-owasprisks/),
a list of the most critical web application security risks,
along with a number of tools and guidelines for security software development.
OWASP also hosts conferences and training events around the world
to promote the importance of software security.

## What is OWASP SAMM?

Another important resource from OWASP
is the OWASP [SAMM](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-osamm/)
(Software Assurance Maturity Model) framework,
which helps organizations formulate and implement software security
strategies tailored to their specific risks,
goals, and constraints.
It provides an effective and measurable way for organizations
to assess their software security programs
and improve their security practices.

## Why is OWASP SAMM important?

OWASP SAMM focuses on defining and improving an organization’s
software security posture by emphasizing a balanced and risk-driven approach.
SAMM is important for organizations that develop software
because it helps them define and prioritize their security efforts
and ensure that their software is secure
throughout the development lifecycle.
Being an open-source framework,
it can be customized to meet the specific needs of different organizations.
This framework can offer guidance for organizations
to create strategies and achieve their maturity goals by assisting in:

- Identifying and assessing software security risks

- Developing and implementing effective software security controls

- Measuring and tracking progress in improving software security

By using SAMM, organizations can adopt a more proactive approach to security,
and therefore, reduce the risk of security breaches
and other software vulnerabilities.

## OWASP SAMM v2

Released in 2020,
SAMM v2 is the latest version of this framework
and includes a number of new features and enhancements:

- A new business function named **Implementation**.
  This function represents a number of core activities
  in the build and deploy domains of an organization.

- A revised [maturity model](https://owaspsamm.org/model/),
  that is more aligned with the latest security standards
  and best practices.

- An updated [SAMM Toolbox](https://owaspsamm.org/assessment/)
  that includes a number of new tools and resources.

- A new [SAMM Benchmark](https://owaspsamm.org/benchmark/)
  initiative that provides information on maturity
  and progress in comparison with that of similar organizations.

SAMM v2 can be used by organizations of all sizes and industries.
The model can be used to assess,
formulate, and implement a software security strategy.
Its benefits include improvement in software security posture
and compliance with security standards,
increased customer confidence, and a reduced risk of breaches.

## OWASP SAMM maturity model

The 15 security practices found in SAMM v2
are grouped within the framework
as five essential areas of business functions.
They are **governance**, **design**, **implementation**,
**verification**, and **operations**,
and they represent the core activities that organizations
should undertake to ensure that their software is secure.

- **Governance**: The governance business function is responsible
  for establishing and maintaining the organization’s
  overall software security policy.
  This includes defining the organization’s risks
  and setting security standards, assigning roles and responsibilities,
  and overseeing the implementation of the organization’s security strategy
  in order to align it with the overall business goal.
  Its key security practices are **strategy and metrics**,
  **policy and compliance**,
  and **education and guidance**.

- **Design**: The design business function is responsible
  for ensuring that software security is considered throughout
  the software development lifecycle (SDLC).
  This includes identifying and assessing security requirements,
  designing secure architectures, and carrying out threat modeling.
  Its key security practices are **threat assessment**,
  **security requirements**,
  and **secure architecture**.

- **Implementation**: The implementation business function is responsible
  for deploying and maintaining secure software.
  This includes building and deploying secure software,
  managing and monitoring security configurations,
  and receiving feedback.
  Its key security practices are **secure build**,
  **secure deployment**, and **defect management**.

- **Verification**: The verification business function is responsible
  for ensuring that software security requirements are met.
  This includes conducting security testing and audits,
  performing code reviews, and verifying that security controls
  are in place and operational.
  Its key security practices are **architecture assessment**,
  **requirements-driven testing**, and **security testing**.

- **Operations**: The operation business function is responsible
  for monitoring and managing security risks throughout
  the SDLC.
  This includes identifying and assessing operational risks,
  implementing security controls, and handling detected incidents.
  Its key security practices are **incident management**,
  **environment management**, and **operational management**.

Every key security practice has two associated activities,
each of which is part of a different stream.
Both streams have achievable goals that can be reached
at increasing levels of maturity.
SAMM's model looks like this:

<div class="imgblock">

![SAMM Overview](https://res.cloudinary.com/fluid-attacks/image/upload/v1700259462/learn/owasp%20samm/samm_overview.webp)

<div class="title">

OWASP SAMM overview(https://owaspsamm.org/release-notes-v2/)
(taken from [here](https://owaspsamm.org//img/pages/SAMM_v2_overview.svg)).

</div>

</div>

### OWASP SAMM maturity levels

OWASP SAMM defines four maturity levels (0 to 3)
for each security practice and business function.
The levels represent the evolution of
an organization’s software security program,
ranging from a null state with minimal security practices
(level 0)
to a well-defined and optimized state (level 3).

- Level 0 - Inactive: no or minimal security practices

- Level 1 - Initial: ad-hoc security practices

- Level 2 - Defined: noticeable increase;
  defined and documented security practices

- Level 3 - Mastery: quantitatively measured and continuously improved
  security practices

The assessment part of this verification process
is a sure-fire way to know where each organization stands.
The OWASP foundation provides assessment tools that
are free and easy to understand,
with a complete [questionnaire](https://docs.google.com/spreadsheets/d/1jmLVltRhuG19AX5cLUcWH1Qox2Uic17rD29gMVG5zDE/view#gid=1716553355)
that goes through each of the business functions and its practices,
delivering a thorough scorecard to be used as a starting point.
Regular assessment and continuous improvement efforts help organizations
progress through the maturity levels
and enhance their overall software security posture.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/security-testing/"
title="Get started with Fluid Attacks' Security Testing solution right now"
/>
</div>

## How to achieve a high maturity level

Achieving the highest maturity level in OWASP SAMM
requires a comprehensive and proactive approach.
It involves establishing a culture of continuous improvement,
embedding security into all aspects of the SDLC,
and leveraging automation and advanced security practices.
Here’s a detailed guide to achieving the highest maturity level:

1. Establish a culture of continuous improvement: Foster a
   security-conscious mindset throughout the organization,
   emphasizing on the importance of security in all aspects
   of software development and deployment.
   Encourage and support continuous learning and training
   while promoting open communication and collaboration between
   security teams and every department involved in the SDLC.

2. Embed security into the SDLC: Integrate security activities
   into all phases of the SDLC,
   implement a threat modeling process to identify, analyze,
   and prioritize potential threats to software,
   and utilize tools like [SAST](../../product/sast/)
   or [DAST](../../product/dast/)
   to identify and remediate vulnerabilities before deployment.
   [Penetration testing](../../blog/importance-pentesting/)
   can assess the security posture by identifying additional weaknesses.
   When this task is executed by certified ethical hackers,
   the result will be a lower rate of false positives.

3. Leverage automation and advanced security practices: Implement
   continuous integration and continuous delivery (CI/CD) pipelines
   with built-in security checks and automated
   [vulnerability scanning](../../blog/vulnerability-scan/).
   Utilize infrastructure as code (IaC) to automate
   and secure infrastructure provisioning and configurations,
   as well as cloud-based security solutions.

4. Continuous monitoring and improvement: Establish continuous monitoring
   of security logs, network traffic, and system behavior
   to detect anomalies and potential vulnerabilities.
   Implement reports that frequently inform on software status,
   incidents, and assessments.

## How Fluid Attacks fits in the OWASP SAMM plan

For us, it’s clear that achieving the highest maturity level
in OWASP SAMM is an ongoing journey, not a destination.
Fluid Attacks can provide different tools and solutions to steer
your organization toward maturity in its security posture.

OWASP SAMM defines
[security testing](../../solutions/security-testing/)
as a critical practice
for verifying the effectiveness of security controls
and identifying vulnerabilities in different systems.
With Fluid Attacks, security testing is guaranteed
in all phases of the SDLC.

Apart from security testing,
we have a [highly certified](../../certifications/)
[ethical hacking](../../solutions/ethical-hacking/) team,
that performs manual [penetration testing](../../solutions/penetration-testing-as-a-service/)
for the discovery and detailed analysis of hidden vulnerabilities.

You can also count with a single [platform](../../platform/)
that provides details on findings,
its interactive nature allows users to be up to date
and plan for rapid remediation.

Start with a [21-day free trial](https://app.fluidattacks.com/SignUp)
of vulnerability assessment through our automated tool.
If you’d like more information,
don’t hesitate to [contact us](../../contact-us/).
