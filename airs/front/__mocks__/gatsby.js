const React = require("react");
const gatsby = jest.requireActual("gatsby");

module.exports = {
  ...gatsby,
  graphql: jest.fn(),
  Link: jest
    .fn()
    .mockImplementation(
      ({
        activeClassName,
        activeStyle,
        getProps,
        innerRef,
        partiallyActive,
        ref,
        replace,
        to,
        ...rest
      }) =>
        React.createElement("a", {
          ...rest,
          href: to,
        }),
    ),
  Slice: jest.fn().mockImplementation(({ alias, ...rest }) =>
    React.createElement("div", {
      ...rest,
      "data-test-slice-alias": alias,
    }),
  ),
  navigate: jest.fn(),
  useStaticQuery: jest.fn().mockReturnValue({
    allCloudinaryMedia: {
      nodes: [
        {
          secure_url: "string",
          cloudinaryData: {
            secure_url: "string",
            height: "number",
            width: "number",
          },
        },
      ],
    },
    allMarkdownRemark: {
      edges: [
        {
          node: {
            fields: {
              slug: "slug",
            },
            frontmatter: {
              alt: "image description",
              author: "Author Name",
              category: "blog category",
              codename: "string",
              date: "2023-11-02",
              slug: "string",
              description: "description",
              image: "test.jgp",
              spanish: null,
              subtitle: "subtitle",
              tags: "éxitos,tag2,tag3",
              title: "Online Examination System v1.0 - Multiple Authenticated SQL Injections (SQLi)",
            },
          },
        },
      ],
    },
  }),
};
