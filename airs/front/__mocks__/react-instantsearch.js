const React = require("react");
const ReactInstantSearch = jest.requireActual("react-instantsearch");

module.exports = {
  ...ReactInstantSearch,
  Hits: jest
    .fn()
    .mockImplementation(({ children }) =>
      React.createElement("div", { "data-testid": "hits" }, children),
    ),
  InstantSearch: jest
    .fn()
    .mockImplementation(({ children }) =>
      React.createElement("div", { "data-testid": "instant-search" }, children),
    ),
  Pagination: jest
    .fn()
    .mockImplementation(({ children }) =>
      React.createElement("div", { "data-testid": "pagination" }, children),
    ),
  PoweredBy: jest
    .fn()
    .mockImplementation(({ children }) =>
      React.createElement("div", { "data-testid": "powered-by" }, children),
    ),
  SearchBox: jest
    .fn()
    .mockImplementation(({ children }) =>
      React.createElement("input", { "data-testid": "search-box" }, children),
    ),
};
