/* eslint-disable camelcase */
/* eslint-disable functional/immutable-data */
import "./static/scss/index.scss";
import "animate.css/animate.min.css";
import "tachyons/css/tachyons.min.css";
import "highlight.js/styles/github.css";
import { Auth0Provider } from "@auth0/auth0-react";
import type { AppState } from "@auth0/auth0-react";
import type { Error } from "@bugsnag/core/types/event";
import Bugsnag from "@bugsnag/js";
import type { Event } from "@bugsnag/js";
import BugsnagPluginReact from "@bugsnag/plugin-react";
import { CoralogixRum } from "@coralogix/browser";
import type { GatsbyBrowser } from "gatsby";
import { navigate } from "gatsby";
import _ from "lodash";
import React, { Fragment } from "react";
import { v4 as uuidv4 } from "uuid";

import {
  checkIfBugsnagEventMustBeIgnored,
  shouldReloadGivenTheEvent,
  verifyEventSeverity,
} from "./src/utils/bugsnag-reports";

const setCookie = (name: string, val: string): void => {
  const date = new Date();
  const value = val;

  date.setFullYear(date.getFullYear() + 1);
  document.cookie = `${name}=${value}; expires=${date.toUTCString()}; path=/`;
};

const getCookie = (name: string): string => {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);

  if (parts.length === 2) {
    return parts.pop()?.split(";").shift() as string;
  }

  return "";
};

const getEnvironment = (): string => {
  if (_.isUndefined(window)) {
    return "development";
  }
  const currentUrl = window.location.hostname;
  const ephemeralDomainRegex = /^web.eph.fluidattacks.com/gu;

  if (currentUrl === "localhost") {
    return "development";
  } else if (ephemeralDomainRegex.test(currentUrl)) {
    return "ephemeral";
  }

  return "production";
};

const shouldPageReload = (
  error: Error,
  event: Event,
  shouldReload: boolean,
): boolean => {
  if (error.errorClass === "ChunkLoadError") {
    event.severity = "warning";
    event.unhandled = false;

    return true;
  }

  return shouldReload;
};

const handleErrorMetadata = (
  error: Error,
  event: Event,
  shouldReload: boolean,
): void => {
  const message = event.context;
  event.context = error.errorMessage;
  error.errorMessage = _.isString(message) ? message : "";
  event.groupingHash = event.context;
  const cloudflareBeaconError = error.stacktrace.some((trace): boolean =>
    trace.file.includes("static.cloudflareinsights.com/beacon.min.js"),
  );
  if (cloudflareBeaconError && event.context === "Illegal invocation") {
    event.severity = "warning";
    event.unhandled = false;
  }

  if (shouldPageReload(error, event, shouldReload)) {
    error.errorMessage += ". Page will be reloaded.";
  }
};

if (getEnvironment() === "production") {
  if (getCookie("fa_user_id") === "") {
    setCookie("fa_user_id", uuidv4());
  } else {
    getCookie("fa_user_id");
  }

  const handleError = (event: Event): boolean => {
    if (checkIfBugsnagEventMustBeIgnored(event)) return false;

    verifyEventSeverity(event);

    const shouldReload = shouldReloadGivenTheEvent(event);
    event.errors.forEach((error): void => {
      handleErrorMetadata(error, event, shouldReload);
    });

    if (shouldReload) location.reload();

    return true;
  };

  Bugsnag.start({
    apiKey: "6d0d7e66955855de59cfff659e6edf31",
    onError: handleError,
    plugins: [new BugsnagPluginReact(React)],
    releaseStage: getEnvironment(),
  });

  CoralogixRum.init({
    application: "airs",
    coralogixDomain: "US2",
    ignoreErrors: [
      /Object.hasOwn is not a function/iu,
      /We couldn't load "\/page-data\/sq\/d\/\d+\.json"/iu,
      /page resources for (?:\/[A-z-]*)* not found\. Not rendering React/iu,
      /^Loading chunk \d+ failed\. \([a-z]+: .+\)$/iu,
    ],
    ignoreUrls: [
      /chrome-extension:/gu,
      /moz-extension:/gu,
      /safari-extension:/gu,
      /safari-web-extension:/gu,
    ],
    public_key: "cxtp_QvrRiad3FYjQrAN6biRzWU2qJ8PhMT",
    user_context: {
      user_id: getCookie("fa_user_id"),
      user_name: getCookie("fa_user_id"),
    },
    version: process.env.CI_COMMIT_SHORT_SHA ?? "",
  });
}

const reactPlugin =
  getEnvironment() === "production" ? Bugsnag.getPlugin("react") : undefined;

const BugsnagErrorBoundary = _.isUndefined(reactPlugin)
  ? Fragment
  : reactPlugin.createErrorBoundary(React);

const onRedirectCallback = (appState?: AppState): void => {
  void navigate(appState?.returnTo ?? "/", { replace: true });
};

export const wrapRootElement: GatsbyBrowser["wrapRootElement"] = ({
  element,
}): JSX.Element => (
  <Auth0Provider
    authorizationParams={{ redirect_uri: window.location.origin }}
    clientId={process.env.GATSBY_AUTH0_CLIENT_ID ?? ""}
    domain={process.env.GATSBY_AUTH0_DOMAIN ?? ""}
    onRedirectCallback={onRedirectCallback}
  >
    <BugsnagErrorBoundary>{element}</BugsnagErrorBoundary>
  </Auth0Provider>
);
