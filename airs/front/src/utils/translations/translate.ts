import Bugsnag from "@bugsnag/js";
import type { TOptions } from "i18next";
import i18next, { t, use } from "i18next";
import { initReactI18next } from "react-i18next";

import pageTexts from "./en.json";
import pageEsTexts from "./es.json";

use(initReactI18next)
  .init({
    fallbackLng: "en",
    interpolation: {
      escapeValue: false,
    },
    lng: "en",
    resources: {
      en: { translation: pageTexts },
      es: { translation: pageEsTexts },
    },
  })
  .catch((error): void => {
    Bugsnag.notify(error);
  });

interface ITranslationFn {
  (key: string[] | string, options?: TOptions): string;
}

const translate: { t: ITranslationFn } = {
  t: (key: string[] | string, options?: TOptions): string => t(key, options),
};

export { i18next, translate };
