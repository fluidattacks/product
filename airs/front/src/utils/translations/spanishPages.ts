const translatedPages = [
  {
    en: "/",
    es: "/es/",
  },
  {
    en: "/blog/",
    es: "/es/blog/",
  },
  {
    en: "/learn/",
    es: "/es/learn/",
  },
  {
    en: "/privacy/",
    es: "/es/privacidad/",
  },
  {
    en: "/product/",
    es: "/es/producto/",
  },
  {
    en: "/terms-use/",
    es: "/es/terminos-uso/",
  },
  {
    en: "/solutions/",
    es: "/es/soluciones/",
  },
  {
    en: "/solutions/devsecops/",
    es: "/es/soluciones/devsecops/",
  },
  {
    en: "/solutions/red-teaming/",
    es: "/es/soluciones/red-team/",
  },
  {
    en: "/solutions/vulnerability-management/",
    es: "/es/soluciones/gestion-vulnerabilidades/",
  },
  {
    en: "/solutions/ethical-hacking/",
    es: "/es/soluciones/hacking-etico/",
  },
  {
    en: "/solutions/security-testing/",
    es: "/es/soluciones/pruebas-seguridad/",
  },
  {
    en: "/solutions/secure-code-review/",
    es: "/es/soluciones/revision-codigo-fuente/",
  },
  {
    en: "/platform/",
    es: "/es/plataforma/",
  },
  {
    en: "/about-us/",
    es: "/es/quienes-somos/",
  },
  {
    en: "/product/sast/",
    es: "/es/producto/sast/",
  },
  {
    en: "/product/dast/",
    es: "/es/producto/dast/",
  },
  {
    en: "/product/mpt/",
    es: "/es/producto/mpt/",
  },
  {
    en: "/product/mast/",
    es: "/es/producto/mast/",
  },
  {
    en: "/product/sca/",
    es: "/es/producto/sca/",
  },
  {
    en: "/product/cspm/",
    es: "/es/producto/cspm/",
  },
  {
    en: "/product/ptaas/",
    es: "/es/producto/ptaas/",
  },
  {
    en: "/product/re/",
    es: "/es/producto/re/",
  },
  {
    en: "/clients/",
    es: "/es/clientes/",
  },
  {
    en: "/cookie/",
    es: "/es/cookie/",
  },
  {
    en: "/thank-you-newsletter/",
    es: "/es/gracias-boletin-informativo/",
  },
  {
    en: "/contact-us/",
    es: "/es/contactanos/",
  },
  {
    en: "/contact-us-demo/",
    es: "/es/contactanos-demo/",
  },
  {
    en: "/contact-us-demo/thank-you/",
    es: "/es/contactanos-demo/gracias/",
  },
  {
    en: "/blog/devsecops-concept/",
    es: "/es/blog/concepto-devsecops/",
  },
  {
    en: "/blog/how-to-implement-devsecops/",
    es: "/es/blog/como-implementar-devsecops/",
  },
  {
    en: "/blog/devsecops-tools/",
    es: "/es/blog/herramientas-devsecops/",
  },
  {
    en: "/blog/devsecops-best-practices/",
    es: "/es/blog/buenas-practicas-devsecops/",
  },
  {
    en: "/blog/aws-devsecops-with-fluid-attacks/",
    es: "/es/blog/aws-devsecops-con-fluid-attacks/",
  },
  {
    en: "/blog/bas-vs-pentesting-vs-red-teaming/",
    es: "/es/blog/bas-v-pentesting-v-red-teaming/",
  },
  {
    en: "/plans/",
    es: "/es/planes/",
  },
  {
    en: "/plans/#advanced",
    es: "/es/planes/#advanced",
  },
  {
    en: "/plans/#essential",
    es: "/es/planes/#essential",
  },
  {
    en: "/blog/casa-approved-static-scanning/",
    es: "/es/blog/escaneo-estatico-aprobado-por-casa/",
  },
  {
    en: "/blog/what-does-a-devsecops-engineer-do/",
    es: "/es/blog/que-hace-un-ingeniero-devsecops/",
  },
  {
    en: "/blog/vulnerability-scan/",
    es: "/es/blog/escaneo-de-vulnerabilidades/",
  },
  {
    en: "/about-us/events/breaking-the-build/",
    es: "/es/quienes-somos/eventos/rompiendo-el-build/",
  },
  {
    en: "/blog/vulnerability-assessment/",
    es: "/es/blog/evaluacion-de-vulnerabilidades/",
  },
  {
    en: "/blog/web-app-vulnerability-scanning/",
    es: "/es/blog/escaneo-vulnerabilidad-aplicacion-web/",
  },
  {
    en: "/blog/differences-between-sast-sca-dast/",
    es: "/es/blog/diferencias-entre-sast-sca-dast/",
  },
  {
    en: "/blog/continuous-penetration-testing/",
    es: "/es/blog/pruebas-de-penetracion-continuas/",
  },
  {
    en: "/blog/azure-devsecops-with-fluid-attacks/",
    es: "/es/blog/azure-devsecops-con-fluid-attacks/",
  },
  {
    en: "/blog/why-is-cloud-devsecops-important/",
    es: "/es/blog/importancia-de-la-nube-devsecops/",
  },
  {
    en: "/product/aspm/",
    es: "/es/producto/aspm/",
  },
  {
    en: "/blog/sastisfying-app-security/",
    es: "/es/blog/seguridad-app-sastisfactoria/",
  },
  {
    en: "/blog/sca-scans/",
    es: "/es/blog/escaneos-sca/",
  },
  {
    en: "/blog/stand-shoulders-giants/",
    es: "/es/blog/sobre-hombros-gigantes/",
  },
  {
    en: "/blog/categories/",
    es: "/es/blog/categorias/",
  },
  {
    en: "/blog/categories/attacks/",
    es: "/es/blog/categorias/ataques/",
  },
  {
    en: "/blog/categories/development/",
    es: "/es/blog/categorias/desarrollo/",
  },
  {
    en: "/blog/categories/philosophy/",
    es: "/es/blog/categorias/filosofia/",
  },
  {
    en: "/blog/categories/politics/",
    es: "/es/blog/categorias/politica/",
  },
  {
    en: "/services/continuous-hacking/",
    es: "/es/servicios/hacking-continuo/",
  },
  {
    en: "/blog/tags/",
    es: "/es/blog/tags/",
  },
  {
    en: "/blog/tags/cloud/",
    es: "/es/blog/tags/nube/",
  },
  {
    en: "/blog/tags/company/",
    es: "/es/blog/tags/empresa/",
  },
  {
    en: "/blog/tags/cybersecurity/",
    es: "/es/blog/tags/ciberseguridad/",
  },
  {
    en: "/blog/tags/devsecops/",
    es: "/es/blog/tags/devsecops/",
  },
  {
    en: "/blog/tags/hacking/",
    es: "/es/blog/tags/hacking/",
  },
  {
    en: "/blog/tags/pentesting/",
    es: "/es/blog/tags/pentesting/",
  },
  {
    en: "/blog/tags/red-team/",
    es: "/es/blog/tags/red-team/",
  },
  {
    en: "/blog/tags/security-testing/",
    es: "/es/blog/tags/pruebas%20de%20seguridad/",
  },
  {
    en: "/blog/tags/software/",
    es: "/es/blog/tags/software/",
  },
  {
    en: "/blog/tags/trend/",
    es: "/es/blog/tags/tendencia/",
  },
  {
    en: "/blog/tags/vulnerability/",
    es: "/es/blog/tags/vulnerabilidad/",
  },
  {
    en: "/blog/what-is-ptaas/",
    es: "/es/blog/que-es-ptaas/",
  },
  {
    en: "/blog/reverse-engineering/",
    es: "/es/blog/ingenieria-inversa/",
  },
  {
    en: "/blog/what-is-mast/",
    es: "/es/blog/que-es-mast/",
  },
  {
    en: "/blog/what-is-manual-penetration-testing/",
    es: "/es/blog/que-es-prueba-de-penetracion-manual/",
  },
  {
    en: "/blog/choosing-pentesting-team/",
    es: "/es/blog/elegir-equipo-de-pentesting/",
  },
  {
    en: "/blog/types-of-penetration-testing/",
    es: "/es/blog/tipos-de-pruebas-de-penetracion/",
  },
  {
    en: "/blog/penetration-testing-compliance/",
    es: "/es/blog/cumplimiento-pruebas-de-penetracion/",
  },
  {
    en: "/blog/importance-pentesting/",
    es: "/es/blog/importancia-pentesting/",
  },
  {
    en: "/blog/what-is-ethical-hacking/",
    es: "/es/blog/que-es-hacking-etico/",
  },
  {
    en: "/blog/thinking-like-hacker/",
    es: "/es/blog/piensa-como-hacker/",
  },
  {
    en: "/blog/hacking-ethics/",
    es: "/es/blog/etica-del-hacking/",
  },
  {
    en: "/solutions/penetration-testing-as-a-service/",
    es: "/es/soluciones/pruebas-penetracion-servicio/",
  },
  {
    en: "/solutions/app-security-posture-management/",
    es: "/es/soluciones/aspm/",
  },
  {
    en: "/blog/secure-code-review/",
    es: "/es/blog/revision-codigo-fuente/",
  },
  {
    en: "/learn/malicious-code/",
    es: "/es/learn/codigo-malicioso/",
  },
  {
    en: "/blog/delimit-ethical-hacking/",
    es: "/es/blog/delimitar-hacking-etico/",
  },
  {
    en: "/blog/red-team-exercise/",
    es: "/es/blog/ejercicio-red-teaming/",
  },
  {
    en: "/blog/what-is-vulnerability-management/",
    es: "/es/blog/que-es-gestion-de-vulnerabilidades/",
  },
  {
    en: "/learn/what-is-application-security-posture-management/",
    es: "/es/learn/que-es-aspm/",
  },
  {
    en: "/learn/what-is-log4shell-vulnerability/",
    es: "/es/learn/que-es-vulnerabilidad-log4shell/",
  },
  {
    en: "/blog/tribe-of-hackers-1/",
    es: "/es/blog/tribu-de-hackers-1/",
  },
  {
    en: "/blog/tribe-of-hackers-2/",
    es: "/es/blog/tribu-de-hackers-2/",
  },
  {
    en: "/blog/tribe-of-hackers-3/",
    es: "/es/blog/tribu-de-hackers-3/",
  },
  {
    en: "/blog/tribe-of-hackers-4/",
    es: "/es/blog/tribu-de-hackers-4/",
  },
  {
    en: "/blog/tribe-of-hackers-5/",
    es: "/es/blog/tribu-de-hackers-5/",
  },
  {
    en: "/learn/what-is-spring4shell-vulnerability/",
    es: "/es/learn/que-es-vulnerabilidad-spring4shell/",
  },
  {
    en: "/blog/why-apply-red-teaming/",
    es: "/es/blog/por-que-usar-red-teaming/",
  },
  {
    en: "/blog/attacking-without-announcing/",
    es: "/es/blog/atacar-sin-avisar/",
  },
  {
    en: "/blog/tiber-eu-framework/",
    es: "/es/blog/marco-tiber-eu/",
  },
  {
    en: "/blog/tiber-eu-providers/",
    es: "/es/blog/proveedores-tiber-eu/",
  },
  {
    en: "/blog/manual-code-review/",
    es: "/es/blog/revision-manual-de-codigo/",
  },
  {
    en: "/blog/code-quality-and-security/",
    es: "/es/blog/calidad-y-seguridad-del-codigo/",
  },
  {
    en: "/blog/secure-coding-practices/",
    es: "/es/blog/practicas-codificacion-segura/",
  },
  {
    en: "/blog/secure-coding-five-steps/",
    es: "/es/blog/codificacion-segura-cinco-pasos/",
  },
  {
    en: "/learn/what-is-configuration-management/",
    es: "/es/learn/que-es-gestion-de-configuracion/",
  },
  {
    en: "/blog/choose-vulnerability-management/",
    es: "/es/blog/elegir-gestion-de-vulnerabilidades/",
  },
  {
    en: "/blog/from-asm-to-arm/",
    es: "/es/blog/de-asm-a-arm/",
  },
  {
    en: "/blog/attack-resistance-management-psirts/",
    es: "/es/blog/gestion-resistencia-ataques-psirts/",
  },
  {
    en: "/blog/cvssf-risk-exposure-metric/",
    es: "/es/blog/metrica-exposicion-al-riesgo-cvssf/",
  },
  {
    en: "/blog/security-testing-fundamentals/",
    es: "/es/blog/fundamentos-pruebas-de-seguridad/",
  },
  {
    en: "/learn/what-is-man-in-the-middle-attack/",
    es: "/es/learn/que-es-ataque-man-in-the-middle/",
  },
  {
    en: "/blog/5-best-practices-coding-with-gen-ai/",
    es: "/es/blog/5-buenas-practicas-codigo-ia-gen/",
  },
  {
    en: "/blog/6-main-items-gen-ai-usage-policy/",
    es: "/es/blog/6-items-principales-politica-ia-gen/",
  },
  {
    en: "/learn/what-is-owasp-samm/",
    es: "/es/learn/que-es-owasp-samm/",
  },
  {
    en: "/learn/what-is-software-bill-of-materials/",
    es: "/es/learn/que-es-sbom/",
  },
  {
    en: "/blog/the-mother-of-all-breaches/",
    es: "/es/blog/la-mayor-de-todas-las-brechas/",
  },
  {
    en: "/blog/penetration-testing/",
    es: "/es/blog/pentesting/",
  },
  {
    en: "/blog/pci-dss-v4-new-13-requirements/",
    es: "/es/blog/pci-dss-v4-13-nuevos-requisitos/",
  },
  {
    en: "/blog/pci-dss-best-practices-2024/",
    es: "/es/blog/pci-dss-buenas-practicas-2024/",
  },
  {
    en: "/blog/adversarial-machine-learning-nist/",
    es: "/es/blog/aprendizaje-automatico-antagonico-nist/",
  },
  {
    en: "/blog/indirect-prompt-injection-llms/",
    es: "/es/blog/inyeccion-indirecta-de-prompts-llms/",
  },
  {
    en: "/blog/rely-on-code-assisted-pentesting/",
    es: "/es/blog/confia-en-pentesting-asistido-por-codigo/",
  },
  {
    en: "/blog/top-10-hacking-certifications/",
    es: "/es/blog/top-10-certificaciones-de-hacking/",
  },
  {
    en: "/blog/on-premises-cloud-security/",
    es: "/es/blog/seguridad-nube-on-premises/",
  },
  {
    en: "/blog/justify-investment-in-cybersecurity/",
    es: "/es/blog/justificar-la-inversion-en-ciberseguridad/",
  },
  {
    en: "/blog/10-biggest-ransomware-attacks/",
    es: "/es/blog/10-mayores-ataques-ransomware/",
  },
  {
    en: "/blog/nebraska-joke-infrastructure-dependency/",
    es: "/es/blog/chiste-nebraska-dependencia-infraestructura/",
  },
  {
    en: "/blog/ransomware-prevention/",
    es: "/es/blog/prevenir-ransomware/",
  },
  {
    en: "/blog/lessons-cybersecurity-black-swans/",
    es: "/es/blog/lecciones-cisnes-negros-ciberseguridad/",
  },
  {
    en: "/blog/top-10-data-breaches/",
    es: "/es/blog/10-mayores-violaciones-de-datos/",
  },
  {
    en: "/blog/undersupported-digital-infrastructure/",
    es: "/es/blog/infraestructura-digital-con-poco-soporte/",
  },
  {
    en: "/learn/zero-trust-security/",
    es: "/es/learn/seguridad-zero-trust/",
  },
  {
    en: "/blog/zero-trust-network-access/",
    es: "/es/blog/ztna/",
  },
  {
    en: "/blog/financial-services-cybersecurity/",
    es: "/es/blog/ciberseguridad-servicios-financieros/",
  },
  {
    en: "/blog/fintech-cybersecurity/",
    es: "/es/blog/ciberseguridad-fintech/",
  },
  {
    en: "/blog/in-app-protection-is-not-enough/",
    es: "/es/blog/in-app-protection-no-es-suficiente/",
  },
  {
    en: "/blog/protecting-data-financial-services/",
    es: "/es/blog/proteccion-datos-servicios-financieros/",
  },
  {
    en: "/blog/top-financial-data-breaches/",
    es: "/es/blog/mayores-violaciones-datos-sector-financiero/",
  },
  {
    en: "/blog/supply-chain-financial-sector/",
    es: "/es/blog/cadena-suministro-sector-financiero/",
  },
  {
    en: "/blog/bank-cybersecurity/",
    es: "/es/blog/ciberseguridad-bancaria/",
  },
  {
    en: "/blog/banking-cybersecurity-regulations/",
    es: "/es/blog/regulaciones-ciberseguridad-bancaria/",
  },
  {
    en: "/blog/ddos-attacks-prevention-banking/",
    es: "/es/blog/prevencion-ataques-ddos-bancos/",
  },
  {
    en: "/blog/transparency-for-supply-chain-security/",
    es: "/es/blog/transparencia-seguridad-cadena-de-suministro/",
  },
  {
    en: "/blog/secure-payment-systems/",
    es: "/es/blog/sistemas-pagos-seguros/",
  },
  {
    en: "/blog/crowdstrike-incident-july-2024/",
    es: "/es/blog/incidente-de-crowdstrike-julio-2024/",
  },
  {
    en: "/blog/data-privacy-open-banking/",
    es: "/es/blog/privacidad-datos-banca-abierta/",
  },
  {
    en: "/blog/saas-security-measures/",
    es: "/es/blog/medidas-seguridad-saas/",
  },
  {
    en: "/blog/api-security-best-practices/",
    es: "/es/blog/seguridad-api-buenas-practicas/",
  },
  {
    en: "/blog/web-application-security-threats/",
    es: "/es/blog/riesgos-seguridad-aplicaciones-web/",
  },
  {
    en: "/blog/cloud-security-assessment/",
    es: "/es/blog/evaluacion-seguridad-nube/",
  },
  {
    en: "/blog/security-through-transparency/",
    es: "/es/blog/seguridad-mediante-transparencia/",
  },
  {
    en: "/blog/retail-sector-cybersecurity/",
    es: "/es/blog/ciberseguridad-sector-minorista/",
  },
  {
    en: "/blog/retail-sector-data-breaches/",
    es: "/es/blog/filtraciones-datos-sector-minorista/",
  },
  {
    en: "/blog/point-of-sale-security/",
    es: "/es/blog/seguridad-punto-de-venta/",
  },
  {
    en: "/blog/fluid-attacks-new-testing-architecture/",
    es: "/es/blog/nueva-arquitectura-de-pruebas-fluid-attacks/",
  },
  {
    en: "/blog/introduction-cybersecurity-aviation-sector/",
    es: "/es/blog/introduccion-ciberseguridad-sector-aviacion/",
  },
  {
    en: "/blog/building-python-testing-framework/",
    es: "/es/blog/creando-framework-para-pruebas-en-python/",
  },
  {
    en: "/blog/reachability-analysis-for-prioritization/",
    es: "/es/blog/reachability-analysis-para-priorizar/",
  },
  {
    en: "/blog/attacks-against-transportation-sector/",
    es: "/es/blog/ataques-al-sector-transporte/",
  },
  {
    en: "/blog/f-scores-accuracy-sla-fluid-attacks/",
    es: "/es/blog/f-scores-sla-exactitud-fluid-attacks/",
  },
];

export { translatedPages };
