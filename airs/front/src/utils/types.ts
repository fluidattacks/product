interface ICloudinaryMedia {
  nodes: {
    secure_url: string;
    cloudinaryData: {
      secure_url: string;
      width: number;
      height: number;
    };
  }[];
}

export type { ICloudinaryMedia };
