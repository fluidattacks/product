import Bugsnag from "@bugsnag/js";
import { changeLanguage } from "i18next";

const capitalizePlainString = (title: string): string => {
  return `${title.charAt(0).toUpperCase()}${title.slice(1).replace("-", " ")}`;
};

const stringToUri = (word: string): string => {
  return word
    .toLowerCase()
    .split(/\s+/gu)
    .join("-")
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/gu, "");
};

const updateLanguage = async (language: string): Promise<void> => {
  await changeLanguage(language).catch((error): void => {
    Bugsnag.notify({
      message: "Error changing language",
      name: (error as Error).name,
    });
  });
};

const kebabCaseToCamelCase = (str: string): string => {
  return str.replace(/-(?:[a-z])/gu, (word): string => word[1].toUpperCase());
};

export {
  capitalizePlainString,
  kebabCaseToCamelCase,
  stringToUri,
  updateLanguage,
};
