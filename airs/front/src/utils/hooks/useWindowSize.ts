import { useEffect, useState } from "react";

interface IWindowSize {
  height: number;
  width: number;
}

const getSize = (): IWindowSize => {
  if (typeof window !== "undefined") {
    const { innerHeight, innerWidth } = window;

    return { height: innerHeight, width: innerWidth };
  }

  return { height: 0, width: 1366 };
};

// Get window size reacting to changes
const useWindowSize = (): IWindowSize => {
  const [size, setSize] = useState<IWindowSize>({ height: 0, width: 1366 });

  useEffect((): VoidFunction => {
    const handleResize = (): void => {
      setSize(getSize());
    };
    handleResize();
    window.addEventListener("resize", handleResize, { passive: true });

    return (): void => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return size;
};

export { useWindowSize };
