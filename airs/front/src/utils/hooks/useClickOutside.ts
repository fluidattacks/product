import { useEffect } from "react";
import type { RefObject } from "react";

export const useClickOutside = (
  ref: RefObject<HTMLDivElement>,
  onClickOutside: VoidFunction,
): void => {
  const isOutside = (element: HTMLElement): boolean =>
    !ref.current || !ref.current.contains(element);

  const onClick = (event: Event): void => {
    if (isOutside(event.target as HTMLElement)) {
      onClickOutside();
    }
  };

  useEffect((): VoidFunction => {
    const events = ["mousedown", "touchstart"];
    events.forEach((event): void => {
      document.addEventListener(event, onClick, { passive: true });
    });

    return (): void => {
      events.forEach((event): void => {
        document.removeEventListener(event, onClick);
      });
    };
  });
};
