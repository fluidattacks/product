import dayjs from "dayjs";
import { useEffect, useState } from "react";
import "dayjs/locale/es";

const useBlogsDate = (
  date: string,
  currentLanguage: string | undefined = undefined,
): string => {
  const [safeDate, setSafeDate] = useState("");
  useEffect((): void => {
    if (currentLanguage === "es")
      setSafeDate(
        dayjs(new Date(date))
          .add(5, "hour")
          .locale("es")
          .format("MMMM D, YYYY"),
      );
    else
      setSafeDate(dayjs(new Date(date)).add(5, "hour").format("MMMM D, YYYY"));
  }, [date, currentLanguage]);

  return safeDate;
};

const useDateYear = (): number => {
  const [safeDate, setSafeDate] = useState(0);

  useEffect((): void => {
    setSafeDate(dayjs().year());
  }, []);

  return safeDate;
};

export { useBlogsDate, useDateYear };
