/* eslint-disable functional/no-try-statements */
import _ from "lodash";
import type React from "react";
import { useState } from "react";

const loadInitialState = <T>(
  key: string,
  defaultValue: T,
  storageProvider: Readonly<Storage>,
): T => {
  const storedState = storageProvider.getItem(key);

  if (_.isNull(storedState)) {
    return defaultValue;
  }

  return JSON.parse(storedState) as T;
};

// Wrapper for React.useState that persists using the Web Storage API
export const useStoredState = <T>(
  key: string,
  defaultValue: T,
): [T, React.Dispatch<React.SetStateAction<T>>] => {
  const loadState =
    typeof window === "undefined"
      ? ({ defaultValue } as T)
      : loadInitialState(key, defaultValue, sessionStorage);
  const [state, setState] = useState<T>(loadState);

  const setAndStore: React.Dispatch<React.SetStateAction<T>> = (
    value: React.SetStateAction<T>,
  ): void => {
    setState((currentState): T => {
      const nextValue = value instanceof Function ? value(currentState) : value;

      try {
        sessionStorage.setItem(key, JSON.stringify(nextValue));
      } catch (exception: unknown) {
        /* Empty */
      }

      return nextValue;
    });
  };

  return [state, setAndStore];
};
