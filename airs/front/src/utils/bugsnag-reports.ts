/* eslint-disable functional/immutable-data */
import type { Breadcrumb, Event, Stackframe } from "@bugsnag/js";

interface IExceptions {
  eventContext: string | null;
  requestMetadataInBreadCrumbs: string | null;
  userAgentOperativeSystems: string[] | null;
}

const bugsnagExceptions: IExceptions[] = [
  /*
   * Discard error caused by chrome extension (adblock)
   * blocking request to url with the word pagead2
   */
  {
    eventContext: null,
    requestMetadataInBreadCrumbs: "pagead2",
    userAgentOperativeSystems: null,
  },
  // Error due to notification of external apps while browsing in the website
  {
    eventContext:
      "ResizeObserver loop completed with undelivered notifications.",
    requestMetadataInBreadCrumbs: null,
    userAgentOperativeSystems: null,
  },
  // Discard error caused by mac and iphone browsers generating cross-origin errors
  {
    eventContext:
      "Blocked a frame with origin 'https://fluidattacks.com' from accessing a cross-origin frame. Protocols, domains, and ports must match.",
    requestMetadataInBreadCrumbs: null,
    userAgentOperativeSystems: ["iPhone", "Macintosh"],
  },
  // The following is a error raise from atatus-spa.min
  {
    eventContext: "Cannot convert a Symbol value to a string",
    requestMetadataInBreadCrumbs: null,
    userAgentOperativeSystems: null,
  },
  {
    eventContext:
      'unhandledrejection handler received a non-error. See "unhandledrejection handler" tab for more detail.',
    requestMetadataInBreadCrumbs: null,
    userAgentOperativeSystems: null,
  },
  {
    eventContext: "Cannot read properties of null (reading 'readyState')",
    requestMetadataInBreadCrumbs: null,
    userAgentOperativeSystems: null,
  },
  {
    eventContext: "Cannot read properties of undefined (reading 'idletime')",
    requestMetadataInBreadCrumbs: null,
    userAgentOperativeSystems: null,
  },
  {
    eventContext: "Cannot read properties of undefined (reading 'page')",
    requestMetadataInBreadCrumbs: null,
    userAgentOperativeSystems: null,
  },
  {
    eventContext:
      "Minified React error #418; visit https://reactjs.org/docs/error-decoder.html?invariant=418 for the full message or use the non-minified dev environment for full errors and additional helpful warnings.",
    requestMetadataInBreadCrumbs: null,
    userAgentOperativeSystems: null,
  },
  {
    eventContext:
      "Minified React error #423; visit https://reactjs.org/docs/error-decoder.html?invariant=423 for the full message or use the non-minified dev environment for full errors and additional helpful warnings.",
    requestMetadataInBreadCrumbs: null,
    userAgentOperativeSystems: null,
  },
  {
    eventContext:
      "Object.hasOwn is not a function. (In 'Object.hasOwn(e,o.from)', 'Object.hasOwn' is undefined)",
    requestMetadataInBreadCrumbs: null,
    userAgentOperativeSystems: null,
  },
  {
    eventContext: "Cannot redefine property: googletag",
    requestMetadataInBreadCrumbs: null,
    userAgentOperativeSystems: null,
  },
  {
    eventContext: "undefined is not an object (evaluating 'a.L')",
    requestMetadataInBreadCrumbs: null,
    userAgentOperativeSystems: null,
  },
  {
    eventContext:
      "Object.hasOwn is not a function. (In 'Object.hasOwn(e,n)', 'Object.hasOwn' is undefined)",
    requestMetadataInBreadCrumbs: null,
    userAgentOperativeSystems: null,
  },
  {
    eventContext: "Object.hasOwn is not a function",
    requestMetadataInBreadCrumbs: null,
    userAgentOperativeSystems: null,
  },
  {
    eventContext:
      "Minified React error #425; visit https://reactjs.org/docs/error-decoder.html?invariant=425 for the full message or use the non-minified dev environment for full errors and additional helpful warnings.",
    requestMetadataInBreadCrumbs: null,
    userAgentOperativeSystems: ["iPhone", "Macintosh"],
  },
];

const shouldDiscardGivenTheContext = (
  event: Event,
  contextExceptionContext: IExceptions["eventContext"],
): boolean => {
  if (contextExceptionContext === null) return true;

  if (event.context === contextExceptionContext) return true;

  return (
    event.errors.find(
      (error): boolean => error.errorMessage === contextExceptionContext,
    ) !== undefined
  );
};

const shouldIgnoreGivenTheUserAgent = (
  event: Event,
  exception: IExceptions,
): boolean => {
  if (exception.userAgentOperativeSystems === null) return true;
  const { userAgent } = event.device;
  if (typeof userAgent !== "string") return false;

  const osUserAgent = /\((?:[^;]+);/u.exec(userAgent);

  if (osUserAgent && osUserAgent.length > 1)
    return exception.userAgentOperativeSystems.includes(osUserAgent[1]);

  return true;
};

const ignoreGivenRequestInBreadCrumbs = (
  eventBreadcrumbs: Event["breadcrumbs"],
  exceptionBreadCrumbs: IExceptions["requestMetadataInBreadCrumbs"],
): boolean => {
  if (exceptionBreadCrumbs === null) return true;

  const callBack = (breadCrumb: Breadcrumb): boolean => {
    if (breadCrumb.type === "request") {
      return String(breadCrumb.metadata.request).includes(exceptionBreadCrumbs);
    }

    return false;
  };

  return eventBreadcrumbs.find(callBack) !== undefined;
};

const checkIfBugsnagEventMustBeIgnored = (event: Event): boolean => {
  const exceptionChecks = bugsnagExceptions.map((exception): boolean => {
    return (
      shouldDiscardGivenTheContext(event, exception.eventContext) &&
      shouldIgnoreGivenTheUserAgent(event, exception) &&
      ignoreGivenRequestInBreadCrumbs(
        event.breadcrumbs,
        exception.requestMetadataInBreadCrumbs,
      )
    );
  });

  const errorMustBeIgnored = exceptionChecks.some(
    (exceptionCheck): boolean => exceptionCheck,
  );

  return errorMustBeIgnored;
};

const shouldReloadGivenTheEvent = (event: Event): boolean => {
  const couldNotLoad = /We couldn't load "\/page-data\/sq\/d\/\d+\.json"/iu;
  const noRendering =
    /page resources for (?:\/[A-z-]*)* not found\. Not rendering React/iu;
  const { originalError } = event;

  if (originalError instanceof Error) {
    if (
      couldNotLoad.test(originalError.message) ||
      noRendering.test(originalError.message)
    ) {
      event.severity = "warning";
      event.unhandled = false;

      return true;
    }
  }

  return false;
};

const verifyEventSeverity = (event: Event): void => {
  const setWarningSeverity = (): void => {
    event.unhandled = false;
    event.severity = "warning";
  };

  const stackTraces = event.errors.map(
    (error): Stackframe[] => error.stacktrace,
  );

  const errorsFiles = stackTraces
    .flat()
    .map((stackFrame): string => stackFrame.file);

  if (
    errorsFiles.find((file): boolean => file.includes("fluidattacks.com")) ===
    undefined
  ) {
    setWarningSeverity();

    return;
  }

  if (
    typeof event.originalError !== "undefined" &&
    (event.originalError as Error).message.startsWith("Minified React error")
  ) {
    if (
      typeof event.request.url === "string" &&
      event.request.url.includes("translate.goog")
    ) {
      setWarningSeverity();
    }
  }
};

export {
  checkIfBugsnagEventMustBeIgnored,
  shouldReloadGivenTheEvent,
  verifyEventSeverity,
};
