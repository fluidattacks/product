import dayjs from "dayjs";
import i18next from "i18next";

import { translate } from "./translations/translate";

const filterPostsByLanguage = (posts: readonly INodes[]): INodes[] => {
  const currentLanguage = i18next.language;

  return posts.filter(
    (post: Readonly<INodes>): boolean =>
      (!post.node.fields.slug.includes("/es/") && currentLanguage === "en") ||
      (post.node.fields.slug.includes("/es/") && currentLanguage === "es"),
  );
};

const dateFilter = (post: Readonly<INodes>, date: string): boolean => {
  if (date === translate.t("blogListDatePeriods.lastMonth")) {
    return dayjs(post.node.frontmatter.date).isAfter(
      dayjs().subtract(1, "months"),
    );
  } else if (date === translate.t("blogListDatePeriods.lastSixMonths")) {
    return dayjs(post.node.frontmatter.date).isAfter(
      dayjs().subtract(6, "months"),
    );
  } else if (date === translate.t("blogListDatePeriods.lastYear")) {
    return dayjs(post.node.frontmatter.date).isAfter(
      dayjs().subtract(1, "year"),
    );
  }

  return true;
};

const authorFilter = (post: Readonly<INodes>, author: string): boolean => {
  return (
    author === translate.t("blogListAuthors.all") ||
    post.node.frontmatter.author === author
  );
};

const tagFilter = (post: Readonly<INodes>, tag: string): boolean => {
  return (
    tag === translate.t("blogListTags.all") ||
    post.node.frontmatter.tags
      .split(", ")
      .some((blogTag): boolean => blogTag === tag)
  );
};

const titleFilter = (post: Readonly<INodes>, title: string): boolean => {
  return (
    title === "" ||
    post.node.frontmatter.title.toLowerCase().includes(title.toLowerCase())
  );
};

const categoryFilter = (post: Readonly<INodes>, category: string): boolean => {
  return (
    category === translate.t("blogListCategories.all") ||
    post.node.frontmatter.category === category
  );
};

interface IFilters {
  author: string;
  category: string;
  date: string;
  tag: string;
  title: string;
}

function filterBlogs(
  filters: Readonly<IFilters>,
  blogs: readonly INodes[],
): INodes[] {
  const { author, category, date, tag, title } = filters;

  return blogs
    .filter((post): boolean => authorFilter(post, author))
    .filter((post): boolean => tagFilter(post, tag))
    .filter((post): boolean => dateFilter(post, date))
    .filter((post): boolean => titleFilter(post, title))
    .filter((post): boolean => categoryFilter(post, category));
}

export { filterPostsByLanguage, filterBlogs, dateFilter };
