import { Event } from "@bugsnag/js";
import { describe, test } from "@jest/globals";
import { render, screen } from "@testing-library/react";
import React from "react";
import "@testing-library/jest-dom";

import {
  checkIfBugsnagEventMustBeIgnored,
  shouldReloadGivenTheEvent,
  verifyEventSeverity,
} from "./bugsnag-reports";

const defaultBugsnagEvent = Event.create(
  null,
  false,
  { severity: "", severityReason: { type: "" }, unhandled: true },
  "",
  2,
);

const shouldReloadBugsnagEvent = Event.create(
  Error(`We couldn't load "/page-data/sq/d/2300059654.json"`),
  false,
  { severity: "", severityReason: { type: "" }, unhandled: true },
  "",
  2,
);

const verifySeverityBugsnagEvent = Event.create(
  Error("Minified React error"),
  false,
  { severity: "", severityReason: { type: "" }, unhandled: true },
  "",
  2,
);

const TestComponent = (
  props: Readonly<{
    bugsnagEvent: Event | undefined;
  }>,
): JSX.Element => {
  const { bugsnagEvent } = props;
  const bugsnagResult =
    bugsnagEvent === undefined
      ? false
      : checkIfBugsnagEventMustBeIgnored(bugsnagEvent);
  const bugsnagReloadResult =
    bugsnagEvent === undefined
      ? false
      : shouldReloadGivenTheEvent(bugsnagEvent);
  if (bugsnagEvent) verifyEventSeverity(bugsnagEvent);

  return (
    <React.Fragment>
      <div>
        {bugsnagResult
          ? "Bugsnag ignore error"
          : "Bugsnag does not ignore error"}
      </div>
      <div>
        {bugsnagReloadResult
          ? "Bugsnag reload given the event"
          : "Bugsnag does not reload given the event"}
      </div>
    </React.Fragment>
  );
};

describe("Bugsnag", (): void => {
  test("Bugsnag should not ignore with default event", (): void => {
    expect.hasAssertions();

    render(<TestComponent bugsnagEvent={defaultBugsnagEvent} />);

    expect(
      screen.queryByText("Bugsnag does not ignore error"),
    ).toBeInTheDocument();

    expect(
      screen.queryByText("Bugsnag does not reload given the event"),
    ).toBeInTheDocument();
  });
  test("Bugsnag should not ignore and reload given the event", (): void => {
    expect.hasAssertions();

    render(<TestComponent bugsnagEvent={shouldReloadBugsnagEvent} />);

    expect(
      screen.queryByText("Bugsnag does not ignore error"),
    ).toBeInTheDocument();

    expect(
      screen.queryByText("Bugsnag reload given the event"),
    ).toBeInTheDocument();
  });
  test("Bugsnag should not ignore and verify severity given the event", (): void => {
    expect.hasAssertions();

    jest.replaceProperty(verifySeverityBugsnagEvent, "errors", [
      {
        errorClass: "Minified React error",
        errorMessage: "Minified React error",
        stacktrace: [
          {
            file: "fluidattacks.com",
          },
        ],
        type: "ReactError",
      },
    ]);

    jest.replaceProperty(verifySeverityBugsnagEvent, "request", {
      url: "translate.goog",
    });

    render(<TestComponent bugsnagEvent={verifySeverityBugsnagEvent} />);

    expect(
      screen.queryByText("Bugsnag does not ignore error"),
    ).toBeInTheDocument();

    expect(
      screen.queryByText("Bugsnag does not reload given the event"),
    ).toBeInTheDocument();
  });

  test("Bugsnag should ignore given the event context", (): void => {
    expect.hasAssertions();
    jest.replaceProperty(
      verifySeverityBugsnagEvent,
      "context",
      "Cannot convert a Symbol value to a string",
    );

    render(<TestComponent bugsnagEvent={verifySeverityBugsnagEvent} />);

    expect(screen.queryByText("Bugsnag ignore error")).toBeInTheDocument();

    expect(
      screen.queryByText("Bugsnag does not reload given the event"),
    ).toBeInTheDocument();
  });
});
