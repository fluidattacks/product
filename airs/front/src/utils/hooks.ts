import { useEffect, useState } from "react";

const useCarrousel = (
  isSelected: boolean,
  actualCycle: number,
): {
  cycle: number;
  numberOfCycles: number;
  progress: number;
} => {
  const delay = 80;
  const numberOfCycles = 6;
  const progressLimit = 100;
  const [cycle, setCycle] = useState(0);
  const [progress, setProgress] = useState(0);

  useEffect((): void => {
    const changeCycle = (): void => {
      setCycle((currentCycle): number =>
        currentCycle === numberOfCycles - 1 ? 0 : currentCycle + 1,
      );
    };

    if (progress === progressLimit) {
      setProgress(0);
      changeCycle();
    }
  }, [progress, numberOfCycles]);

  useEffect((): (() => void) => {
    const timer = setInterval((): void => {
      setProgress((currentProgress): number =>
        currentProgress === progressLimit ? 0 : currentProgress + 1,
      );
    }, delay);

    if (isSelected) {
      setProgress(0);
      setCycle(actualCycle);
    }

    return (): void => {
      clearInterval(timer);
    };
  }, [delay, isSelected, actualCycle]);

  return { cycle, numberOfCycles, progress };
};

const usePagination = (
  itemsToShow: number,
  listOfItems: (JSX.Element | undefined)[],
): {
  currentPage: number;
  endOffset: number;
  handlePageClick: (prop: { selected: number }) => void;
  newOffset: number;
  pageCount: number;
  resetPagination: () => void;
} => {
  const numberOfItemsToShow =
    Math.abs(itemsToShow) > 0 ? Math.abs(itemsToShow) : 1;
  const pageCount = Math.ceil(listOfItems.length / numberOfItemsToShow);
  const totalOfPages = pageCount > 0 ? pageCount : 1;
  const [newOffset, setNewOffset] = useState(0);
  const [endOffset, setEndOffset] = useState(numberOfItemsToShow);
  const [currentPage, setCurrentPage] = useState(0);

  const resetPagination = (): void => {
    setNewOffset(0);
    setEndOffset(numberOfItemsToShow);
    setCurrentPage(0);
  };

  const handlePageClick = (prop: { selected: number }): void => {
    const { selected } = prop;
    if (selected > totalOfPages) return;
    setCurrentPage(selected);
    setNewOffset((selected * numberOfItemsToShow) % listOfItems.length);
    setEndOffset(
      ((selected * numberOfItemsToShow) % listOfItems.length) +
        numberOfItemsToShow,
    );
  };

  return {
    currentPage,
    endOffset,
    handlePageClick,
    newOffset,
    pageCount: totalOfPages,
    resetPagination,
  };
};

export { useCarrousel, usePagination };
