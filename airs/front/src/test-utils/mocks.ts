/* eslint-disable camelcase */
import type { ICloudinaryMedia } from "utils/types";

const exampleQueryData: IQueryData = {
  data: {
    markdownRemark: {
      fields: {
        slug: "/example-slug",
      },
      frontmatter: {
        advise: "Example advise",
        alt: "Example alt",
        author: "John Doe",
        authors: "John Doe, Jane Doe",
        banner: "/path/to/banner.jpg",
        category: "Example Category",
        certificationid: "12345",
        certificationsindex: "yes",
        clientsindex: "Clients Index",
        codename: "example-codename",
        cveid: "CVE-2023-1234",
        date: "2023-01-01",
        defaux: "Example defaux",
        definition: "Example definition",
        description: "Example description",
        encrypted: "Example encrypted",
        headtitle: "Example Head Title",
        identifier: "example-identifier",
        image: "/path/to/image.jpg",
        keywords: "example, keywords",
        modified: "2023-01-02",
        partnersindex: "Partners Index",
        phrase: "Example phrase",
        product: "Example Product",
        slug: "/example-slug",
        subtext: "Example subtext",
        subtitle: "Example subtitle",
        tags: "tag1, tag2",
        template: "example-template",
        title: "Example Title",
        writer: "Example Writer",
      },
      headings: [
        {
          depth: 2,
          value: "head1",
        },
      ],
      html: `
        <div class="parsed-html">
          <script>alert('Hello, World!');</script>
          <h3>Hello, World!</h3>
          <p>This is a paragraph.</p>
        </div>
      `,
      rawMarkdownBody: "This is the raw markdown body.",
      tableOfContents: "",
      timeToRead: 1,
    },
    site: {
      siteMetadata: {
        author: "Site Author",
        description: "Site Description",
        keywords: "site, keywords",
        siteUrl: "https://example.com",
        title: "Site Title",
      },
    },
  },
  pageContext: {
    authorName: "John Doe",
    authorUri: "/authors/john-doe",
    breadcrumb: {
      crumbs: [
        {
          crumbLabel: "Home",
          pathname: "/",
        },
      ],
      location: "Home",
    },
    categoryName: "Example Category",
    categoryUri: "/category/example-category",
    language: "en",
    slug: "/example-slug",
    tagName: "example-tag",
    tagUri: "/tag/example-tag",
  },
};

const allCloudinaryMediaData: ICloudinaryMedia = {
  nodes: [
    {
      cloudinaryData: {
        height: 200,
        secure_url: "url",
        width: 200,
      },
      secure_url: "url",
    },
  ],
};

export { exampleQueryData, allCloudinaryMediaData };
