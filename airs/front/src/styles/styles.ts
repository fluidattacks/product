import { styled } from "styled-components";

const NavbarInnerContainer = styled.div.attrs({
  className: `
    relative
    w-100
    z-5
    mr-auto
    ml-auto
  `,
})`
  max-width: 1432px;
  height: 90px;
`;

const NavbarList = styled.ul.attrs({
  className: `
    list
    ma0
    pa0
    flex
    flex-nowrap
    items-center
  `,
})`
  height: inherit;
`;

const Badge = styled.span.attrs({
  className: `
    f7
    br4
    pv2
    ph3
    ma0
    roboto
  `,
})<{ $bgColor: string; $color: string }>`
  background-color: ${({ $bgColor }): string => $bgColor};
  color: ${({ $color }): string => $color};
`;

const PageArticle = styled.article<{ $bgColor: string }>`
  background-color: ${({ $bgColor }): string => $bgColor};
`;

const ArticleTitle = styled.h1.attrs({
  className: `
    bg-white
    mw-1366
    ph-body
    ma0
    center
    c-fluid-bk
    fw7
    f1
    f-5-l
    poppins
    tc
  `,
})``;

const ArticleContainer = styled.div.attrs({
  className: `
    mw-1366
    ph-body
    center
    roboto
    ph4-l
    ph3
    pt5-l
    pt4
    pb5
  `,
})``;

const CareersFaqContainer = styled.div.attrs({
  className: `
    mw-900
    ph-body
    center
    roboto
    ph4-l
    ph3
    pt5-l
    pt4
    pb5
    poppins-heads
  `,
})`
  h3 {
    margin-bottom: 10px !important;
    margin-top: 10px !important;
  }
`;

const FaqContainer = styled.div.attrs({
  className: `
    ph-body
    center
    roboto
    bg-gray-244
    ph4-l
    ph3
    pt5-l
    pt4
    pb5
    faq-container
  `,
})`
  max-width: 950px;
  color: #5c5c70;

  a {
    color: #5c5c70;
  }
`;

const BannerContainer = styled.div.attrs({
  className: `
    bg-banner-sz
    nt-5
    cover
    h-banner
    justify-center
    items-center
    flex bg-center
  `,
})``;

const FullWidthContainer = styled.div.attrs({
  className: `
    w-100
    center
  `,
})`
  padding-left: 20px!;
`;

const BannerTitle = styled.h1.attrs({
  className: `
    white
    fw7
    f1
    poppins
    tc
    ma0
  `,
})``;

const PageContainer = styled.div.attrs({
  className: `
    roboto
    mw-1366
    ph-body
    center
    c-lightblack
    pv4-l
  `,
})``;

const FlexCenterItemsContainer = styled.div.attrs({
  className: `
    flex
    justify-center
    items-center
  `,
})``;

const CenteredSpacedContainer = styled.div.attrs({
  className: `
    tc
    pv3
  `,
})``;

const BlackH2 = styled.h2.attrs({
  className: `
    poppins
    c-fluid-bk
    fw7
    f2
    tc
  `,
})``;

const NewRegularRedButton = styled.button.attrs({
  className: `
    outline-transparent
    bg-fluid-red
    bc-hovered-red
    hv-bg-fluid-dkred
    hv-bd-fluid-dkred
    pointer
    white
    f5
    dib
    t-all-3-eio
    br2
    bc-fluid-red
    ba
    roboto
    justify-center
    bw1
  `,
})`
  padding: 10px 16px;
`;

const PhantomRegularRedButton = styled.button.attrs({
  className: `
    outline-transparent
    bg-transparent
    bc-hovered-red
    hv-bg-fluid-rd
    pointer
    c-dkred
    f5
    dib
    t-all-3-eio
    br2
    ba
    roboto
    justify-center
    bw1
  `,
})`
  padding: 10px 16px;

  :hover {
    color: white;
  }
`;

const SquaredCardContainer = styled.div.attrs({
  className: `
    counter-container
    bs-btm-h-10
    br3
    dib
    ph4
    mh4-l
    center
    mv0-l
    mv4
  `,
})``;

const BannerSubtitle = styled.h2.attrs({
  className: `
    white
    f5
    fw4
    roboto
    tc
    ma0
  `,
})``;

const SocialMediaLink = styled.button.attrs({
  className: `
    pa2
    ba
    br3
    bg-transparent
    pointer
    bc-fluid-gray
    mh1
  `,
})``;

const MarkedTitle = styled.h1.attrs({
  className: `
    c-fluid-bk
    f1-ns
    f2
    poppins
    ml3
  `,
})``;

const MarkedTitleContainer = styled.div.attrs({
  className: `
    center
    flex
    flex-wrap
    mw-900
  `,
})``;

const RedMark = styled.div.attrs({
  className: `
    bl-red
  `,
})``;

const CardsContainer = styled.div.attrs({
  className: `
    cardgrid
    flex-ns
    flex-wrap-ns
    justify-around
  `,
})``;

const CardContainer = styled.div.attrs({
  className: `
    br3
    bs-btm-h-10
    hv-card
    relative
    dt-ns
    mv3
    mh2
    bg-white
    w-clients-card
  `,
})`
  height: 390px;
`;

const CardHeader = styled.button.attrs({
  className: `
    accordion
    pointer
    pa3
    w-100
    bg-white
    outline-transparent
    bn
    t-all-5
  `,
})``;

const CardReadMore = styled.div.attrs({
  className: `
    absolute
    bottom-0
    right-0
    left-0
    fw3
    fadein
  `,
})``;

const CardBody = styled.div.attrs({
  className: `
    panel
    pv0
    ph2
    bg-white
    t-all-5
    overflow-hidden
    ph4
  `,
})``;

const MenuItem = styled.li.attrs({
  className: `
    ph3
    pv1
    dib
    bg-lightergray
    mh2
    mt2
    br4
    hv-bg-fluid-gray
  `,
})``;

const RadioButton = styled.input.attrs({
  className: `
    op7
    dn
    transparent
  `,
  type: `radio`,
})``;

const RadioLabel = styled.label.attrs({
  className: `
    c-black-gray
    f4-ns
    f5
    roboto
    no-underline
    fw3
    hv-fluid-black
    pointer
  `,
})``;

const IframeContainer = styled.div.attrs({
  className: `
    center
    overflow-x-auto
  `,
})`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100vh;
`;

const CardsContainer1200 = styled.div.attrs({
  className: `
    roboto
    internal
    mw-1200
    center
    ph4-l
    ph3
    pt5-l
    pt4
    pb5
  `,
})``;

const AdvisoriesContainer = styled.div.attrs({
  className: `
    mw-1200
    center
    tc
    pb4
    ph-body
  `,
})``;

const AdvisoryContainer = styled.div.attrs({
  className: `
    roboto
    internal
    internal-advisory
    mw-900
    ml-auto
    mr-auto
    ph4-l
    ph3
    pt5-l
    pt4
    pb5
    poppins-heads
  `,
})``;

const ComplianceContainer = styled.div.attrs({
  className: `
    roboto
    mw-1366
    ph-body
    center
    c-lightblack
    pv5
    compliance-page
    flex
    flex-wrap
    items-center
    justify-center
  `,
})``;

const ErrorSection = styled.section.attrs({
  className: `
    error-bg
    vh-100
    w-100
    cover
    bg-top
    flex
    items-center
    justify-center
  `,
})``;

const ErrorContainer = styled.div.attrs({
  className: `
    flex
    flex-column
    justify-between
  `,
})``;

const ErrorTitle = styled.h1.attrs({
  className: `
    poppins
    c-fluid-bk
    f-error
    fw7
    tc
    lh-solid
    ma0
  `,
})``;

const ErrorDescription = styled.p.attrs({
  className: `
    roboto
    c-fluid-bk
    f2
    fw7
    tc
    ma0
  `,
})``;

const ButtonContainer = styled.h1.attrs({
  className: `
    tc
    mt3
  `,
})``;

const BannerH2Title = styled.h1.attrs({
  className: `
    white
    fw7
    f1
    poppins
    tc
    ma0
  `,
})``;

const MenuList = styled.ul.attrs({
  className: `
    list
    ph0-ns
    ph3
    ma0
    tc
    pv3
    slide-show
  `,
})``;

const SectionContainer = styled.div.attrs({
  className: `
    tc
    pv5
    ph-body
  `,
})``;

const SystemsCardContainer = styled.div.attrs({
  className: `
    tl
    pv5
    ph4
    w-100
    w-50-l
  `,
})``;

export {
  AdvisoriesContainer,
  AdvisoryContainer,
  ArticleContainer,
  ArticleTitle,
  Badge,
  BannerContainer,
  BannerH2Title,
  BannerSubtitle,
  BannerTitle,
  BlackH2,
  ButtonContainer,
  CardBody,
  CardContainer,
  CardHeader,
  CardReadMore,
  CardsContainer,
  CardsContainer1200,
  CareersFaqContainer,
  CenteredSpacedContainer,
  ComplianceContainer,
  ErrorContainer,
  ErrorDescription,
  ErrorSection,
  ErrorTitle,
  FaqContainer,
  FlexCenterItemsContainer,
  FullWidthContainer,
  IframeContainer,
  MarkedTitleContainer,
  MarkedTitle,
  MenuItem,
  MenuList,
  NavbarInnerContainer,
  NavbarList,
  NewRegularRedButton,
  PageArticle,
  PageContainer,
  PhantomRegularRedButton,
  RadioButton,
  RadioLabel,
  RedMark,
  SectionContainer,
  SocialMediaLink,
  SquaredCardContainer,
  SystemsCardContainer,
};
