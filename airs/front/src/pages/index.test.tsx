import "@testing-library/jest-dom";
import { render, screen, waitFor } from "@testing-library/react";
import React from "react";

import Error404Page from "./404";
import BlogIndex from "./blog";
import LearnIndex from "./learn";

import NewHomeIndex from ".";
import { exampleQueryData } from "test-utils/mocks";

describe("Pages", (): void => {
  describe("LearnIndex", (): void => {
    it("renders english content", (): void => {
      expect.hasAssertions();

      const { container } = render(
        <LearnIndex
          data={exampleQueryData.data}
          pageContext={exampleQueryData.pageContext}
        />,
      );

      expect(screen.getByText("learn.title")).toBeInTheDocument();
      expect(container).toBeInTheDocument();
    });
  });

  describe("index", (): void => {
    it("renders NewHomeIndex with correctly mock data", (): void => {
      expect.hasAssertions();

      const { container } = render(
        <NewHomeIndex
          data={exampleQueryData.data}
          pageContext={exampleQueryData.pageContext}
        />,
      );

      expect(container).toBeInTheDocument();
    });
  });

  describe("blog", (): void => {
    it("renders BlogIndex with correctly mock data", (): void => {
      expect.hasAssertions();

      const { container } = render(
        <BlogIndex
          data={exampleQueryData.data}
          pageContext={exampleQueryData.pageContext}
        />,
      );

      expect(container).toBeInTheDocument();
    });
  });

  describe("404", (): void => {
    it("renders Error404Page with correctly mock data", async (): Promise<void> => {
      expect.hasAssertions();

      const { container } = render(<Error404Page />);

      await waitFor((): void => {
        expect(container).toBeInTheDocument();
      });
    });
  });
});
