import React from "react";

import { AirsLink } from "../components/AirsLink";
import { NavbarComponent } from "../scenes/Menu";
import {
  ButtonContainer,
  ErrorContainer,
  ErrorDescription,
  ErrorSection,
  ErrorTitle,
  NewRegularRedButton,
} from "../styles/styles";
import { updateLanguage } from "../utils/utilities";

const Error404Page: React.FC = (): JSX.Element => {
  void updateLanguage("en");

  return (
    <React.Fragment>
      <NavbarComponent />
      <ErrorSection>
        <ErrorContainer>
          <ErrorTitle>{"404"}</ErrorTitle>
          <ErrorDescription>{"Whoops! Nothing Found"}</ErrorDescription>
          <ButtonContainer>
            <AirsLink href={"/"}>
              <NewRegularRedButton>{"Go Home"}</NewRegularRedButton>
            </AirsLink>
          </ButtonContainer>
        </ErrorContainer>
      </ErrorSection>
    </React.Fragment>
  );
};

export default Error404Page;
