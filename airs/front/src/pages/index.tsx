import { graphql } from "gatsby";
import React from "react";

import { Seo } from "../components/Seo";
import { HomePage } from "../scenes/HomePage";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import { updateLanguage } from "../utils/utilities";

const NewHomeIndex: React.FC<IQueryData> = ({
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  return (
    <WebsiteWrapper>
      <HomePage />
    </WebsiteWrapper>
  );
};

export const Head = ({ data, pageContext }: IQueryData): JSX.Element => {
  const { author, description, keywords, siteUrl, title } =
    data.site.siteMetadata;

  const spanishTitle =
    "Soluciones de seguridad para aplicaciones | Fluid Attacks";

  const { language } = pageContext;

  return (
    <React.Fragment>
      <meta
        content={"8hgdyewoknahd41nv3q5miuxx6sazj"}
        key={"facebook-domain-verification"}
        name={"facebook-domain-verification"}
      />
      <Seo
        author={author}
        description={description}
        image={
          "https://res.cloudinary.com/fluid-attacks/image/upload/v1669230787/airs/logo-fluid-2022"
        }
        keywords={keywords}
        path={language === "en" ? siteUrl : siteUrl.concat("/es/")}
        title={language === "en" ? title : spanishTitle}
      />
    </React.Fragment>
  );
};

export const query = graphql`
  query {
    site {
      siteMetadata {
        author
        description
        keywords
        siteUrl
        title
      }
    }
  }
`;

export default NewHomeIndex;
