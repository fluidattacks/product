import "@testing-library/jest-dom";
import { fireEvent, render, screen } from "@testing-library/react";
import React from "react";

import CareersFaqIndex from "./faqTemplate";

import { exampleQueryData } from "test-utils/mocks";

describe("CareersFaqIndex", (): void => {
  it("CareersFaqIndex should render mocked data", (): void => {
    expect.hasAssertions();
    jest.spyOn(window, "alert").mockImplementation(jest.fn());

    render(
      <CareersFaqIndex
        data={exampleQueryData.data}
        pageContext={exampleQueryData.pageContext}
      />,
    );

    expect(screen.getByText("This is a paragraph.")).toBeInTheDocument();
    expect(
      document.getElementsByClassName("parsed-html")[0],
    ).toBeInTheDocument();
    fireEvent.click(
      document.getElementsByClassName("parsed-html")[0] as HTMLElement,
    );
    // Assert that alert was not called because of the sanitization
    expect(window.alert).not.toHaveBeenCalled();
  });
});
