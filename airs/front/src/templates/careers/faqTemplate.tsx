/* eslint-disable @typescript-eslint/prefer-destructuring */
/* eslint-disable react/forbid-component-props */
import { graphql } from "gatsby";
import type { StaticQueryDocument } from "gatsby";
import { decode } from "he";
import parse from "html-react-parser";
import React, { useCallback, useEffect, useState } from "react";
import sanitizeHtml from "sanitize-html";

import { PageHeader } from "../../components/PageHeader";
import { Seo } from "../../components/Seo";
import { WebsiteWrapper } from "../../scenes/WebsiteWrapper";
import { CareersFaqContainer, PageArticle } from "../../styles/styles";
import { updateLanguage } from "../../utils/utilities";

const CareersFaqIndex: React.FC<IQueryData> = ({
  data,
  pageContext,
}): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  const { banner, slug, subtext, subtitle, title } =
    data.markdownRemark.frontmatter;
  const { html } = data.markdownRemark;
  const sanitizedHTML = sanitizeHtml(html, {
    allowedClasses: {
      "*": false,
    },
  });

  const hasBanner = typeof banner === "string";
  const isBrowser = typeof document !== "undefined";

  const itemPerList = 10;

  const [next1, setNext1] = useState(itemPerList);
  const [next2, setNext2] = useState(itemPerList);

  const setArrows = (): void => {
    if (isBrowser)
      document.querySelectorAll("h4").forEach((question): void => {
        question.classList.add("arrow-down");
      });
  };

  const hideSection = useCallback((): void => {
    const numToShow = 10;
    if (isBrowser) {
      document.querySelectorAll(".sect3").forEach((element): void => {
        const h4Element = element.querySelector("h4");
        if (h4Element) {
          const numbersText = h4Element.innerText.split(".")[0];
          const numbers = parseInt(numbersText, 10);
          if (numbers > numToShow) {
            element.classList.add("dn");
          }
        }
      });
    }
  }, [isBrowser]);

  const showSection = useCallback(
    (showMore: string): void => {
      if (!isBrowser) return;

      const selector = showMore === "1" ? ".b1" : ".b2";
      document
        .querySelector(selector)
        ?.querySelectorAll(".sect3")
        .forEach((element): void => {
          const h4Element = element.querySelector("h4");
          if (h4Element) {
            const numbersText = h4Element.innerText.split(".")[0];
            const numbers = parseInt(numbersText, 10);

            if (
              (showMore === "1" &&
                numbers > next1 &&
                numbers <= next1 + itemPerList) ||
              (showMore === "2" &&
                numbers > next2 &&
                numbers <= next2 + itemPerList)
            ) {
              element.classList.remove("dn");
            }
          }
        });

      if (showMore === "1") {
        setNext1(next1 + itemPerList);
      } else {
        setNext2(next2 + itemPerList);
      }
    },
    [isBrowser, next1, next2],
  );

  const handleIsShow = useCallback(
    (isShow: boolean, paragraph: Element): void => {
      if (isShow) {
        paragraph.classList.remove("db");
      } else {
        paragraph.classList.add("db");
      }
    },
    [],
  );

  const setOnclick = (): void => {
    if (isBrowser) {
      document.querySelectorAll(".sect3").forEach((element): void => {
        /* eslint-disable-next-line functional/immutable-data */
        (element as HTMLElement).onclick = (): void => {
          element.querySelectorAll(".paragraph").forEach((paragraph): void => {
            const isShow = Array.from(paragraph.classList).includes("db");
            handleIsShow(isShow, paragraph);
          });
          element.querySelectorAll(".olist").forEach((paragraph): void => {
            const isShow = Array.from(paragraph.classList).includes("db");
            handleIsShow(isShow, paragraph);
          });
          element.querySelectorAll("h4").forEach((question): void => {
            const isShow = Array.from(question.classList).includes("arrow-up");
            if (isShow) {
              question.classList.remove("arrow-up");
              question.classList.add("arrow-down");
            } else {
              question.classList.remove("arrow-down");
              question.classList.add("arrow-up");
            }
          });
        };
      });

      document.querySelectorAll(".sect2").forEach((button): void => {
        /* eslint-disable-next-line functional/immutable-data */
        (button as HTMLElement).onclick = (): void => {
          const showMore = button.classList.contains("show-button-1")
            ? "1"
            : "2";
          showSection(showMore);
        };
      });
    }
  };

  useEffect((): void => {
    hideSection();
  }, [hideSection]);

  useEffect((): void => {
    setArrows();
    setOnclick();
  });

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#f9f9f9"}>
        <PageHeader
          banner={banner}
          pageWithBanner={hasBanner}
          slug={slug}
          subtext={subtext}
          subtitle={subtitle}
          title={decode(title)}
        />
        <CareersFaqContainer className={"internal faq-page"}>
          {parse(sanitizedHTML)}
        </CareersFaqContainer>
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = ({ data }: IQueryData): JSX.Element => {
  const { description, keywords, slug, headtitle } =
    data.markdownRemark.frontmatter;

  return (
    <Seo
      description={description}
      image={
        "https://res.cloudinary.com/fluid-attacks/image/upload/v1669230787/airs/logo-fluid-2022"
      }
      keywords={keywords}
      path={slug}
      title={headtitle}
    />
  );
};

export const query: StaticQueryDocument = graphql`
  query CareersFaqIndex($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      fields {
        slug
      }
      frontmatter {
        banner
        description
        keywords
        slug
        subtext
        subtitle
        title
        headtitle
      }
    }
  }
`;

export default CareersFaqIndex;
