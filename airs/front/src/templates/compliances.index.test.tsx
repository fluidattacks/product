import "@testing-library/jest-dom";
import { render, screen, waitFor } from "@testing-library/react";
import React from "react";

import CompliancesIndex from "./compliancesTemplate";
import ComplianceIndex from "./complianceTemplate";

import { exampleQueryData } from "test-utils/mocks";

describe("compliances", (): void => {
  describe("compliancesTemplate", (): void => {
    it("CompliancesIndex should render compliances", (): void => {
      expect.hasAssertions();

      render(
        <CompliancesIndex
          data={exampleQueryData.data}
          pageContext={exampleQueryData.pageContext}
        />,
      );

      expect(
        screen.getByText(
          exampleQueryData.data.markdownRemark.frontmatter.title,
        ),
      ).toBeInTheDocument();
      expect(screen.getByText("compliances.pci.subtitle")).toBeInTheDocument();
      expect(
        screen.getByText("compliances.owasp.subtitle"),
      ).toBeInTheDocument();
      expect(
        screen.getByText("compliances.hipaa.subtitle"),
      ).toBeInTheDocument();
      expect(screen.getByText("compliances.nist.subtitle")).toBeInTheDocument();
    });
  });

  describe("complianceTemplate", (): void => {
    it("ComplianceIndex should render mocked data", async (): Promise<void> => {
      expect.hasAssertions();

      render(
        <ComplianceIndex
          data={exampleQueryData.data}
          pageContext={exampleQueryData.pageContext}
        />,
      );

      await waitFor((): void => {
        expect(
          document.getElementsByClassName("parsed-html")[0],
        ).toBeInTheDocument();
      });
      expect(
        screen.getByText(
          exampleQueryData.data.markdownRemark.frontmatter.title,
        ),
      ).toBeInTheDocument();
      expect(screen.getByText("Hello, World!")).toBeInTheDocument();
    });
  });
});
