import "@testing-library/jest-dom";
import { render, waitFor } from "@testing-library/react";
import React from "react";

import BlogAuthorTemplate from "./blogAuthorTemplate";
import BlogCategoryTemplate from "./blogCategoryTemplate";
import BlogsIndex from "./blogsTemplate";
import BlogTagTemplate from "./blogTagTemplate";

import { exampleQueryData } from "test-utils/mocks";

describe("blog", (): void => {
  describe("blogAuthorTemplate", (): void => {
    it("blogAuthorTemplate should render mocked data", (): void => {
      expect.hasAssertions();

      const { container } = render(
        <BlogAuthorTemplate
          data={exampleQueryData.data}
          pageContext={exampleQueryData.pageContext}
        />,
      );

      expect(container).toBeInTheDocument();
    });
  });
  describe("blogCategoryTemplate", (): void => {
    it("blogCategoryTemplate should render mocked data", (): void => {
      expect.hasAssertions();

      const { container } = render(
        <BlogCategoryTemplate
          data={exampleQueryData.data}
          pageContext={exampleQueryData.pageContext}
        />,
      );

      expect(container).toBeInTheDocument();
    });
  });

  describe("blogTagTemplate", (): void => {
    it("blogTagTemplate should render mocked data", (): void => {
      expect.hasAssertions();

      const { container } = render(
        <BlogTagTemplate
          data={exampleQueryData.data}
          pageContext={exampleQueryData.pageContext}
        />,
      );

      expect(container).toBeInTheDocument();
    });
  });

  describe("blogsTemplate", (): void => {
    it("blogsTemplate should render mocked data", async (): Promise<void> => {
      expect.hasAssertions();

      const { container } = render(
        <BlogsIndex
          data={exampleQueryData.data}
          pageContext={exampleQueryData.pageContext}
        />,
      );

      await waitFor((): void => {
        expect(container).toBeInTheDocument();
      });
    });
    it("blogsTemplate should render mocked data without headtilte", async (): Promise<void> => {
      expect.hasAssertions();

      const { container } = render(
        <BlogsIndex
          data={exampleQueryData.data}
          pageContext={exampleQueryData.pageContext}
        />,
      );

      await waitFor((): void => {
        expect(container).toBeInTheDocument();
      });
    });
  });
});
