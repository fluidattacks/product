import { graphql } from "gatsby";
import type { StaticQueryDocument } from "gatsby";
import parse from "html-react-parser";
import React from "react";
import sanitizeHtml from "sanitize-html";

import { Seo } from "../components/Seo";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import {
  ComplianceContainer,
  MarkedTitle,
  PageArticle,
  RedMark,
} from "../styles/styles";
import { updateLanguage } from "../utils/utilities";

const SystemIndex: React.FC<IQueryData> = ({
  data,
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  const { title } = data.markdownRemark.frontmatter;
  const { html } = data.markdownRemark;
  const sanitizedHTML = sanitizeHtml(html, {
    allowedClasses: {
      "*": false,
    },
  });

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#f4f4f6"}>
        <ComplianceContainer>
          <RedMark>
            <MarkedTitle>{title}</MarkedTitle>
          </RedMark>
          <div>{parse(sanitizedHTML)}</div>
        </ComplianceContainer>
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = ({ data }: IQueryData): JSX.Element => {
  const { description, headtitle, keywords, slug, title } =
    data.markdownRemark.frontmatter;

  return (
    <Seo
      description={description}
      image={
        "https://res.cloudinary.com/fluid-attacks/image/upload/v1619637249/airs/compliance/cover-internal-compliance_clerwf"
      }
      keywords={keywords}
      path={slug}
      title={
        headtitle
          ? `${headtitle} | Systems | Fluid Attacks`
          : `${title} | Systems | Fluid Attacks`
      }
    />
  );
};

export const query: StaticQueryDocument = graphql`
  query SystemIndex($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      fields {
        slug
      }
      frontmatter {
        description
        keywords
        slug
        title
        headtitle
      }
    }
  }
`;

export default SystemIndex;
