import { render, waitFor } from "@testing-library/react";
import React from "react";

import AdvisoriesIndex from "./advisoriesTemplate";
import AdvisoryIndex from "./advisoryTemplate";

import { exampleQueryData } from "test-utils/mocks";

describe("advisoryTemplate", (): void => {
  it("advisoryTemplate should render mocked data", async (): Promise<void> => {
    expect.hasAssertions();

    const modifiedData = {
      ...exampleQueryData,
      data: {
        ...exampleQueryData.data,
        markdownRemark: {
          ...exampleQueryData.data.markdownRemark,
          frontmatter: {
            ...exampleQueryData.data.markdownRemark.frontmatter,
            headtitle: "",
          },
        },
      },
    };

    const { container } = render(
      <AdvisoryIndex
        data={modifiedData.data}
        pageContext={exampleQueryData.pageContext}
      />,
    );

    await waitFor((): void => {
      expect(container).toBeInTheDocument();
    });
  });
  it("advisoryTemplate should render mocked data without headtitle", async (): Promise<void> => {
    expect.hasAssertions();

    const { container } = render(
      <AdvisoryIndex
        data={exampleQueryData.data}
        pageContext={exampleQueryData.pageContext}
      />,
    );

    await waitFor((): void => {
      expect(container).toBeInTheDocument();
    });
  });
});

describe("advisoriesTemplate", (): void => {
  it("advisoriesTemplate should render mocked data", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <AdvisoriesIndex
        data={exampleQueryData.data}
        pageContext={exampleQueryData.pageContext}
      />,
    );

    expect(container).toBeInTheDocument();
  });
});
