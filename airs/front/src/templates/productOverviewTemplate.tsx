import { graphql } from "gatsby";
import type { StaticQueryDocument } from "gatsby";
import { decode } from "he";
import React from "react";

import { ProductOverviewPage } from "../components/ProductOverviewPage";
import { Seo } from "../components/Seo";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import { updateLanguage } from "../utils/utilities";

const ProductOverview: React.FC<IQueryData> = ({
  data,
  pageContext,
}: IQueryData): JSX.Element => {
  const { description } = data.markdownRemark.frontmatter;
  const { language } = pageContext;
  void updateLanguage(language);

  return (
    <WebsiteWrapper>
      <ProductOverviewPage description={description} />
    </WebsiteWrapper>
  );
};

export const Head = ({ data }: IQueryData): JSX.Element => {
  const { description, keywords, slug, title } =
    data.markdownRemark.frontmatter;

  return (
    <Seo
      description={description}
      image={
        "https://res.cloudinary.com/fluid-attacks/image/upload/v1669230787/airs/logo-fluid-2022"
      }
      keywords={keywords}
      path={slug}
      title={decode(`${title} | Fluid Attacks`)}
    />
  );
};

export const query: StaticQueryDocument = graphql`
  query ProductOverviewBySlug($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      fields {
        slug
      }
      frontmatter {
        description
        keywords
        slug
        title
      }
    }
  }
`;

export default ProductOverview;
