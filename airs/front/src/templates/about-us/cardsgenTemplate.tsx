import { graphql } from "gatsby";
import type { StaticQueryDocument } from "gatsby";
import React from "react";

import { Card } from "../../components/Cards";
import { CertificationsPage } from "../../components/CertificationsPage";
import { ClientsPage } from "../../components/ClientsPage";
import { PartnerPage } from "../../components/PartnerPage";
import { Seo } from "../../components/Seo";
import { updateLanguage } from "../../utils/utilities";

const CardsgenIndex: React.FC<IQueryData> = ({
  data,
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  const { banner, clientsindex, certificationsindex, partnersindex, title } =
    data.markdownRemark.frontmatter;
  const { html } = data.markdownRemark;

  if (partnersindex === "yes") {
    return (
      <Card banner={banner} html={html} title={title}>
        <PartnerPage />
      </Card>
    );
  } else if (clientsindex === "yes") {
    return (
      <Card banner={banner} html={html} title={title}>
        <ClientsPage />
      </Card>
    );
  }

  return (
    <Card banner={banner} html={html} title={title}>
      {certificationsindex === "yes" ? <CertificationsPage /> : undefined}
    </Card>
  );
};

export const Head = ({ data }: IQueryData): JSX.Element => {
  const { clientsindex, description, keywords, partnersindex, slug, title } =
    data.markdownRemark.frontmatter;

  const getMetaImage = (): string => {
    if (partnersindex === "yes") {
      return "https://res.cloudinary.com/fluid-attacks/image/upload/v1619633627/airs/partners/cover-partners_n4sshp";
    } else if (clientsindex === "yes") {
      return "https://res.cloudinary.com/fluid-attacks/image/upload/v1619635918/airs/clients/cover-clients_llnlaw";
    }

    return "https://res.cloudinary.com/fluid-attacks/image/upload/v1619632703/airs/certifications/cover-certifications_dos6xu";
  };

  const metaImage: string = getMetaImage();

  return (
    <Seo
      description={description}
      image={metaImage}
      keywords={keywords}
      path={slug}
      title={`${title} | Fluid Attacks`}
    />
  );
};

export const query: StaticQueryDocument = graphql`
  query CardsgenPages($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      fields {
        slug
      }
      frontmatter {
        banner
        certificationsindex
        clientsindex
        description
        keywords
        slug
        partnersindex
        title
      }
    }
  }
`;

export default CardsgenIndex;
