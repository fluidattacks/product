import "@testing-library/jest-dom";
import { render } from "@testing-library/react";
import { useStaticQuery } from "gatsby";
import React from "react";

import CardsgenTemplate from "./cardsgenTemplate";
import PeopleIndex from "./peopleTemplate";

import { allCloudinaryMediaData, exampleQueryData } from "test-utils/mocks";
import type { ICloudinaryMedia } from "utils/types";

interface IDataCardsgenTemplateMock {
  allCloudinaryMedia: ICloudinaryMedia;
  allMarkdownRemark: {
    edges: [
      {
        node: {
          fields: {
            slug: string;
          };
          frontmatter: {
            alt: string;
            certification: string;
            certificationid: string;
            certificationlogo: string;
            description: string;
            keywords: string;
            slug: string;
            title: string;
          };
        };
      },
    ];
  };
}

const mockUseStaticQuery = jest.spyOn({ useStaticQuery }, `useStaticQuery`);
const mockDataUseStaticQuery: IDataCardsgenTemplateMock = {
  allCloudinaryMedia: allCloudinaryMediaData,
  allMarkdownRemark: {
    edges: [
      {
        node: {
          fields: {
            slug: "",
          },
          frontmatter: {
            alt: "Alt content",
            certification: "Certification content",
            certificationid: "1647436",
            certificationlogo: "certificationLogo content",
            description: "Description content",
            keywords: "keywords content",
            slug: "/",
            title: "Title content",
          },
        },
      },
    ],
  },
};

describe("CardsgenTemplate", (): void => {
  beforeEach((): void => {
    mockUseStaticQuery.mockImplementation(
      (): IDataCardsgenTemplateMock => mockDataUseStaticQuery,
    );
  });
  afterEach((): void => {
    jest.restoreAllMocks();
  });
  it("CardsgenTemplate should render mocked data", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <CardsgenTemplate
        data={exampleQueryData.data}
        pageContext={exampleQueryData.pageContext}
      />,
    );

    expect(container).toBeInTheDocument();
  });
  it("CardsgenTemplate should render mocked data when partnersindex is equal yes", (): void => {
    expect.hasAssertions();

    const modifiedData = {
      ...exampleQueryData,
      data: {
        ...exampleQueryData.data,
        markdownRemark: {
          ...exampleQueryData.data.markdownRemark,
          frontmatter: {
            ...exampleQueryData.data.markdownRemark.frontmatter,
            partnersindex: "yes",
          },
        },
      },
    };

    const { container } = render(
      <CardsgenTemplate
        data={modifiedData.data}
        pageContext={exampleQueryData.pageContext}
      />,
    );

    expect(container).toBeInTheDocument();
  });
  it("CardsgenTemplate should render mocked data when clientsindex is equal yes", (): void => {
    expect.hasAssertions();

    const modifiedData = {
      ...exampleQueryData,
      data: {
        ...exampleQueryData.data,
        markdownRemark: {
          ...exampleQueryData.data.markdownRemark,
          frontmatter: {
            ...exampleQueryData.data.markdownRemark.frontmatter,
            clientsindex: "yes",
          },
        },
      },
    };

    const { container } = render(
      <CardsgenTemplate
        data={modifiedData.data}
        pageContext={exampleQueryData.pageContext}
      />,
    );

    expect(container).toBeInTheDocument();
  });
});

describe("PeopleIndex", (): void => {
  beforeEach((): void => {
    mockUseStaticQuery.mockImplementation(
      (): IDataCardsgenTemplateMock => mockDataUseStaticQuery,
    );
  });
  afterEach((): void => {
    jest.restoreAllMocks();
  });
  it("PeopleIndex should render mocked data", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <PeopleIndex
        data={exampleQueryData.data}
        pageContext={exampleQueryData.pageContext}
      />,
    );

    expect(container).toBeInTheDocument();
  });
});
