import { graphql } from "gatsby";
import type { StaticQueryDocument } from "gatsby";
import React from "react";

import { ComplianceCta } from "../components/ComplianceCta";
import { Seo } from "../components/Seo";
import { Title } from "../components/Typography";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import {
  ComplianceContainer,
  FlexCenterItemsContainer,
  PageArticle,
} from "../styles/styles";
import { useHtml } from "../utils/hooks/useHtml";
import { updateLanguage } from "../utils/utilities";

const components = {
  "cta-banner": ComplianceCta,
};

const ComplianceIndex = ({ data, pageContext }: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  const { title } = data.markdownRemark.frontmatter;
  const { html } = data.markdownRemark;
  const content = useHtml(components, html);

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#f9f9f9"}>
        <FlexCenterItemsContainer>
          <Title
            color={"#2e2e38"}
            level={1}
            mt={4}
            size={"big"}
            textAlign={"center"}
          >
            {title}
          </Title>
        </FlexCenterItemsContainer>
        <ComplianceContainer>{content}</ComplianceContainer>
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = ({ data }: IQueryData): JSX.Element => {
  const { description, headtitle, keywords, slug, title } =
    data.markdownRemark.frontmatter;

  return (
    <Seo
      description={description}
      image={
        "https://res.cloudinary.com/fluid-attacks/image/upload/v1619637249/airs/compliance/cover-internal-compliance_clerwf"
      }
      keywords={keywords}
      path={slug}
      title={
        headtitle
          ? `${headtitle} | Compliances | Fluid Attacks`
          : `${title} | Compliances | Fluid Attacks`
      }
    />
  );
};

export const query: StaticQueryDocument = graphql`
  query ComplianceIndex($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      fields {
        slug
      }
      frontmatter {
        description
        keywords
        slug
        title
        headtitle
      }
    }
  }
`;

export default ComplianceIndex;
