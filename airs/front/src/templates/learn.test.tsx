import { render, waitFor } from "@testing-library/react";
import React from "react";

import LearnPostsIndex from "./learnPostsTemplate";

import { exampleQueryData } from "test-utils/mocks";

describe("learnPostsTemplate", (): void => {
  it("LearnPostsIndex should render mocked data", async (): Promise<void> => {
    expect.hasAssertions();

    const { container } = render(
      <LearnPostsIndex
        data={exampleQueryData.data}
        pageContext={exampleQueryData.pageContext}
      />,
    );

    await waitFor((): void => {
      expect(container).toBeInTheDocument();
    });
  });
  it("LearnPostsIndex should render mocked data without headtitle", async (): Promise<void> => {
    expect.hasAssertions();

    const modifiedData = {
      ...exampleQueryData,
      data: {
        ...exampleQueryData.data,
        markdownRemark: {
          ...exampleQueryData.data.markdownRemark,
          frontmatter: {
            ...exampleQueryData.data.markdownRemark.frontmatter,
            headtitle: "",
          },
        },
      },
    };
    const { container } = render(
      <LearnPostsIndex
        data={modifiedData.data}
        pageContext={exampleQueryData.pageContext}
      />,
    );

    await waitFor((): void => {
      expect(container).toBeInTheDocument();
    });
  });
});
