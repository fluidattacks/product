import "@testing-library/jest-dom";
import { render } from "@testing-library/react";
import React from "react";

import MaskedAdvisoryIndex from "./maskedAdvisoryTemplate";
import MdDefaultPage from "./pageArticle";
import PlansIndex from "./plansTemplate";
import ProductOverview from "./productOverviewTemplate";
import QrEventIndex from "./qrEventTemplate";
import ResourcesIndex from "./resourcesTemplate";
import SubscribeIndex from "./subscribeTemplate";
import ThankYouIndex from "./thankYouTemplate";

import { exampleQueryData } from "test-utils/mocks";

describe("Templates", (): void => {
  const testCases = [
    { Component: ThankYouIndex, name: "ThankYouIndex" },
    { Component: SubscribeIndex, name: "SubscribeIndex" },
    { Component: ResourcesIndex, name: "ResourcesIndex" },
    { Component: QrEventIndex, name: "QrEventIndex" },
    { Component: ProductOverview, name: "ProductOverview" },
    { Component: PlansIndex, name: "PlansIndex" },
    { Component: MdDefaultPage, name: "MdDefaultPage" },
    { Component: MaskedAdvisoryIndex, name: "MaskedAdvisoryIndex" },
  ];

  it.each(testCases)(
    "$name should render mocked data",
    ({ Component }): void => {
      expect.hasAssertions();

      const { container } = render(
        <Component
          data={exampleQueryData.data}
          pageContext={exampleQueryData.pageContext}
        />,
      );

      expect(container).toBeInTheDocument();
    },
  );
});
