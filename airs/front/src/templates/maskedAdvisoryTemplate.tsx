import { useAuth0, withAuthenticationRequired } from "@auth0/auth0-react";
import type { Auth0ContextInterface } from "@auth0/auth0-react";
import { graphql } from "gatsby";
import type { StaticQueryDocument } from "gatsby";
import React from "react";

import { SummaryTable } from "../components/AdvisoryTable/SummaryTable";
import { VulnerabilityTable } from "../components/AdvisoryTable/VulnerabilityTable";
import { LogoutButton } from "../components/LogoutButton";
import { Seo } from "../components/Seo";
import { TimeLapse } from "../components/TimeLapse";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import {
  AdvisoryContainer,
  MarkedTitle,
  MarkedTitleContainer,
  PageArticle,
  RedMark,
} from "../styles/styles";
import { useHtml } from "../utils/hooks/useHtml";
import { updateLanguage } from "../utils/utilities";

const components = {
  "summary-table": SummaryTable,
  "time-lapse": TimeLapse,
  "vulnerability-table": VulnerabilityTable,
};

const MaskedAdvisoryIndex: React.FC<IQueryData> = ({
  data,
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);
  const { html } = data.markdownRemark;
  const content = useHtml(components, html);

  const { title } = data.markdownRemark.frontmatter;
  const { user, isAuthenticated }: Auth0ContextInterface = useAuth0();

  if (!isAuthenticated)
    return (
      <WebsiteWrapper>
        <div>{"Not authenticated"}</div>
      </WebsiteWrapper>
    );

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#f4f4f6"}>
        <p>{`Logged in as: ${user?.email}`}</p>
        <LogoutButton />
        <MarkedTitleContainer>
          <div className={"ph-body"}>
            <RedMark>
              <MarkedTitle>{title}</MarkedTitle>
            </RedMark>
          </div>
        </MarkedTitleContainer>
        <AdvisoryContainer>{content}</AdvisoryContainer>
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = ({ data }: IQueryData): JSX.Element => {
  const { description, keywords, slug, title } =
    data.markdownRemark.frontmatter;

  return (
    <Seo
      description={description}
      image={
        "https://res.cloudinary.com/fluid-attacks/image/upload/v1619634447/airs/bg-advisories_htsqyd"
      }
      keywords={keywords}
      path={slug}
      title={`${title} | Fluid Attacks`}
    />
  );
};

export const query: StaticQueryDocument = graphql`
  query MaskedAdvisoryIndex($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      fields {
        slug
      }
      frontmatter {
        description
        keywords
        slug
        title
      }
    }
  }
`;

export default withAuthenticationRequired(MaskedAdvisoryIndex);
