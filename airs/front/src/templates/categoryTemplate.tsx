/* eslint react/forbid-component-props:0 */
import { graphql } from "gatsby";
import type { StaticQueryDocument } from "gatsby";
import React from "react";
import sanitizeHtml from "sanitize-html";

import { SastPageFooter } from "../components/SastPageFooter";
import { Seo } from "../components/Seo";
import { Text } from "../components/Typography";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import {
  BlackH2,
  ComplianceContainer,
  FlexCenterItemsContainer,
  FullWidthContainer,
  MarkedTitle,
  PageArticle,
  RedMark,
} from "../styles/styles";
import { translate } from "../utils/translations/translate";
import { updateLanguage } from "../utils/utilities";

const CategoryIndex: React.FC<IQueryData> = ({
  data,
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  const { slug, defaux, definition, title } = data.markdownRemark.frontmatter;
  const { html } = data.markdownRemark;
  const sanitizedHTML = sanitizeHtml(html, {
    allowedClasses: {
      "*": false,
    },
  });

  const sastSlug = slug === "product/sast/" || slug === "producto/sast/";

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#f9f9f9"}>
        <ComplianceContainer>
          <RedMark>
            <MarkedTitle>{title}</MarkedTitle>
          </RedMark>
          <Text color={"#2e2e38"} mt={2} size={"medium"} textAlign={"justify"}>
            {definition}
          </Text>
          <Text color={"#2e2e38"} mt={1} size={"medium"}>
            {defaux}
          </Text>
          <FullWidthContainer>
            <BlackH2>{translate.t("products.title") + title}</BlackH2>
            <FlexCenterItemsContainer
              className={"solution-benefits flex-wrap poppins-heads"}
              dangerouslySetInnerHTML={{
                __html: sanitizedHTML,
              }}
            />
          </FullWidthContainer>
          {sastSlug ? <SastPageFooter /> : undefined}
        </ComplianceContainer>
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = ({ data }: IQueryData): JSX.Element => {
  const { description, headtitle, image, keywords, slug, title } =
    data.markdownRemark.frontmatter;

  return (
    <Seo
      description={description}
      image={image.replace(".webp", "")}
      keywords={keywords}
      path={slug}
      title={
        headtitle
          ? `${headtitle} | Products | Fluid Attacks`
          : `${title} | Products | Fluid Attacks`
      }
      type={"service"}
    />
  );
};

export const query: StaticQueryDocument = graphql`
  query CategoryIndex($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      fields {
        slug
      }
      frontmatter {
        description
        image
        defaux
        definition
        keywords
        slug
        title
        headtitle
      }
    }
  }
`;

export default CategoryIndex;
