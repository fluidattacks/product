/* eslint react/forbid-component-props: 0 */
import { graphql } from "gatsby";
import type { StaticQueryDocument } from "gatsby";
import React from "react";

import { AdviseCards } from "../components/AdviseCards";
import { AirsLink } from "../components/AirsLink";
import { Button } from "../components/Button";
import { Seo } from "../components/Seo";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import {
  AdvisoriesContainer,
  BannerContainer,
  BannerSubtitle,
  BannerTitle,
  FullWidthContainer,
  PageArticle,
} from "../styles/styles";
import { translate } from "../utils/translations/translate";
import { updateLanguage } from "../utils/utilities";

const AdvisoriesIndex: React.FC<IQueryData> = ({
  data,
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);
  const { banner, subtitle, title } = data.markdownRemark.frontmatter;

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#dddde3"}>
        <BannerContainer className={banner}>
          <FullWidthContainer>
            <BannerTitle>{title}</BannerTitle>
            <BannerSubtitle>{subtitle}</BannerSubtitle>
          </FullWidthContainer>
        </BannerContainer>
        <AdviseCards />
        <AdvisoriesContainer>
          <h4 className={"f3 c-fluid-bk poppins"}>
            {translate.t("advisories.disclosurePhrase")}
          </h4>
          <AirsLink href={"/advisories/policy"}>
            <Button variant={"primary"}>
              {translate.t("advisories.buttonPhrase")}
            </Button>
          </AirsLink>
        </AdvisoriesContainer>
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = ({ data }: IQueryData): JSX.Element => {
  const { description, keywords, slug, title } =
    data.markdownRemark.frontmatter;

  return (
    <Seo
      description={description}
      image={
        "https://res.cloudinary.com/fluid-attacks/image/upload/v1619634447/airs/bg-advisories_htsqyd"
      }
      keywords={keywords}
      path={slug}
      title={`${title} | Fluid Attacks`}
    />
  );
};

export const query: StaticQueryDocument = graphql`
  query AdvisoriesIndex($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      frontmatter {
        description
        banner
        keywords
        slug
        title
        subtitle
      }
    }
  }
`;

export default AdvisoriesIndex;
