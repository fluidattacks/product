import { Link, graphql } from "gatsby";
import type { StaticQueryDocument } from "gatsby";
import React from "react";

import { CloudImage } from "../components/CloudImage";
import { Seo } from "../components/Seo";
import { Text, Title } from "../components/Typography";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import {
  FlexCenterItemsContainer,
  PageArticle,
  PageContainer,
  PhantomRegularRedButton,
  SystemsCardContainer,
} from "../styles/styles";
import { translate } from "../utils/translations/translate";
import { updateLanguage } from "../utils/utilities";

const SystemsIndex: React.FC<IQueryData> = ({
  data,
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  const { title } = data.markdownRemark.frontmatter;

  const systemData = [
    {
      image: "web-applications",
      link: "/systems/web-apps/",
      paragraph: translate.t("systems.webApps.paragraph"),
      title: translate.t("systems.webApps.title"),
    },
    {
      image: "mobile-apps",
      link: "/systems/mobile-apps/",
      paragraph: translate.t("systems.mobileApps.paragraph"),
      title: translate.t("systems.mobileApps.title"),
    },
    {
      image: "apis",
      link: "/systems/apis/",
      paragraph: translate.t("systems.apis.paragraph"),
      title: translate.t("systems.apis.title"),
    },
    {
      image: "cloud",
      link: "/systems/cloud-infrastructure/",
      paragraph: translate.t("systems.cloud.paragraph"),
      title: translate.t("systems.cloud.title"),
    },
    {
      image: "containers",
      link: "/systems/containers/",
      paragraph: translate.t("systems.containers.paragraph"),
      title: translate.t("systems.containers.title"),
    },
  ];

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#f4f4f6"}>
        <FlexCenterItemsContainer>
          <Title
            color={"#2e2e38"}
            level={1}
            mt={4}
            size={"big"}
            textAlign={"center"}
          >
            {title}
          </Title>
        </FlexCenterItemsContainer>
        {/* eslint-disable-next-line react/forbid-component-props */}
        <PageContainer className={"flex flex-wrap"}>
          {systemData.map((systemCard): JSX.Element => {
            return (
              <SystemsCardContainer key={systemCard.title}>
                <CloudImage
                  alt={title}
                  src={`airs/systems/${systemCard.image}`}
                  styles={"w-100"}
                />
                <Title color={"#2e2e38"} level={3} mb={1} mt={1} size={"small"}>
                  {systemCard.title}
                </Title>
                <Text color={"#5c5c70"} mb={1} size={"medium"}>
                  {systemCard.paragraph}
                </Text>
                <Link to={systemCard.link}>
                  <PhantomRegularRedButton>
                    {"Go to system"}
                  </PhantomRegularRedButton>
                </Link>
              </SystemsCardContainer>
            );
          })}
        </PageContainer>
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = ({ data }: IQueryData): JSX.Element => {
  const { description, keywords, slug, title } =
    data.markdownRemark.frontmatter;

  return (
    <Seo
      description={description}
      image={
        "https://res.cloudinary.com/fluid-attacks/image/upload/v1619630822/airs/solutions/bg-solutions_ylz99o"
      }
      keywords={keywords}
      path={slug}
      title={`${title} | Fluid Attacks`}
    />
  );
};

export const query: StaticQueryDocument = graphql`
  query SystemsIndex($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      fields {
        slug
      }
      frontmatter {
        description
        keywords
        slug
        title
      }
    }
  }
`;

export default SystemsIndex;
