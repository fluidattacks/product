import { graphql } from "gatsby";
import type { StaticQueryDocument } from "gatsby";
import hljs from "highlight.js";
import React, { useEffect } from "react";

import { SummaryTable } from "../components/AdvisoryTable/SummaryTable";
import { VulnerabilityTable } from "../components/AdvisoryTable/VulnerabilityTable";
import { Seo } from "../components/Seo";
import { TimeLapse } from "../components/TimeLapse";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import {
  AdvisoryContainer,
  MarkedTitle,
  MarkedTitleContainer,
  PageArticle,
  RedMark,
} from "../styles/styles";
import { useHtml } from "../utils/hooks/useHtml";
import { updateLanguage } from "../utils/utilities";

const components = {
  "summary-table": SummaryTable,
  "time-lapse": TimeLapse,
  "vulnerability-table": VulnerabilityTable,
};

const AdvisoryIndex: React.FC<IQueryData> = ({
  data,
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);
  const { html } = data.markdownRemark;
  const content = useHtml(components, html);
  const { title } = data.markdownRemark.frontmatter;
  useEffect((): void => {
    const startComment = document.createComment("email_off");
    const endComment = document.createComment("/email_off");
    const bodyElement = document.body;
    bodyElement.insertBefore(startComment, bodyElement.firstChild);
    document.body.appendChild(endComment);
  }, []);

  useEffect((): void => {
    hljs.highlightAll();
  }, []);

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#f4f4f6"}>
        <MarkedTitleContainer>
          <div className={"ph-body"}>
            <RedMark>
              <MarkedTitle>{title}</MarkedTitle>
            </RedMark>
          </div>
        </MarkedTitleContainer>
        <AdvisoryContainer>{content}</AdvisoryContainer>
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = ({ data }: IQueryData): JSX.Element => {
  const { description, headtitle, keywords, slug, title } =
    data.markdownRemark.frontmatter;

  return (
    <Seo
      description={description}
      image={
        "https://res.cloudinary.com/fluid-attacks/image/upload/v1619634447/airs/bg-advisories_htsqyd"
      }
      keywords={keywords}
      path={slug}
      title={
        headtitle
          ? `${headtitle} | Advisories | Fluid Attacks`
          : `${title} | Advisories | Fluid Attacks`
      }
    />
  );
};

export const query: StaticQueryDocument = graphql`
  query AdvisoryIndex($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      fields {
        slug
      }
      frontmatter {
        description
        keywords
        slug
        title
        headtitle
      }
    }
  }
`;

export default AdvisoryIndex;
