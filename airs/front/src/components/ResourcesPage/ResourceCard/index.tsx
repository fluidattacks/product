/* eslint react/forbid-component-props: 0 */
import React from "react";

import type { IResourceCardProps } from "./types";

import { AirsLink } from "../../AirsLink";
import { Button } from "../../Button";
import { CloudImage } from "../../CloudImage";
import {
  ButtonContainer,
  CardContainer,
  CardDescription,
  CardTextContainer,
  CardTitle,
  WebinarLanguage,
} from "../styles";

const ResourcesCard = ({
  buttonText,
  cardType,
  description,
  image,
  language,
  title,
  urlCard,
}: IResourceCardProps): JSX.Element => (
  <CardContainer className={cardType}>
    <CloudImage alt={language} src={image} styles={"br3 br--top"} />
    <CardTextContainer>
      <div className={"pv3"}>
        <WebinarLanguage>{language}</WebinarLanguage>
      </div>
      <CardTitle>{title}</CardTitle>
      <CardDescription>{description}</CardDescription>
      <ButtonContainer>
        <AirsLink decoration={"none"} href={urlCard}>
          <Button display={"block"} variant={"tertiary"}>
            {buttonText}
          </Button>
        </AirsLink>
      </ButtonContainer>
    </CardTextContainer>
  </CardContainer>
);

export { ResourcesCard };
