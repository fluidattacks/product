interface IResourceCardProps {
  buttonText: string;
  cardType: string;
  description: string;
  image: string;
  language: string;
  title: string;
  urlCard: string;
}

export type { IResourceCardProps };
