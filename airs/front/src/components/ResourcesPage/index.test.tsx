import "@testing-library/jest-dom";
import {
  act,
  fireEvent,
  render,
  screen,
  waitFor,
} from "@testing-library/react";
import React from "react";

import { ResourcesMenuElements } from "./ResourcesMenuButtons";

import { ResourcesPage } from ".";

describe("ResourcesPage component", (): void => {
  test("renders bannerTitle correctly", (): void => {
    expect.hasAssertions();

    render(<ResourcesPage bannerTitle={"Test Banner Title"} />);

    expect(screen.getByText("Test Banner Title")).toBeInTheDocument();
  });

  test("filters data correctly when a menu element is clicked", (): void => {
    expect.hasAssertions();

    render(<ResourcesPage bannerTitle={"Test Banner Title"} />);

    expect(
      screen.getByText("resources.cardsText.successStory.successStory4Title"),
    ).toBeInTheDocument();

    fireEvent.click(screen.getByText("webinar"));
  });

  describe("ResourcesMenuElements", (): void => {
    expect.hasAssertions();

    const filterData = jest.fn();

    it("renders a button for each filter", (): void => {
      expect.hasAssertions();

      render(<ResourcesMenuElements filterData={filterData} />);
      expect(screen.getAllByRole("button").length).toBe(6);
    });

    it("calls filterData with type when button clicked", (): void => {
      expect.hasAssertions();

      render(<ResourcesMenuElements filterData={filterData} />);

      const button = screen.getByText("ebook");

      act((): void => {
        button.click();
      });

      expect(filterData).toHaveBeenCalledWith("ebook-card");
    });

    it("calls filterData with type all when button clicked", async (): Promise<void> => {
      expect.hasAssertions();

      render(<ResourcesMenuElements filterData={filterData} />);

      const button = screen.getByText("all");
      const data = [
        "all",
        "ebook",
        "report",
        "success story",
        "webinar",
        "white paper",
      ];
      act((): void => {
        button.click();
      });

      await waitFor((): void => {
        data.forEach((text): void => {
          const dataItems = screen.queryAllByText(text);

          expect(dataItems[0]).toBeInTheDocument();
        });
      });
    });

    it("sets selected state when button clicked", (): void => {
      expect.hasAssertions();

      render(<ResourcesMenuElements filterData={filterData} />);

      const button = screen.getByText("report");

      act((): void => {
        button.click();
      });

      expect(button).toHaveStyle({
        background: expect.stringMatching(/rgb\(27, 153, 139\)/u),
      });
    });
  });
});
