import React from "react";

import { CloudImage } from "../../CloudImage";
import { Col, Text, TextContainer } from "../styles";

interface IProps {
  date: string;
  image: boolean;
  text: string;
}

const TimeItem = ({ date, image, text }: IProps): JSX.Element => (
  <Col>
    {/** The image prop is enable ONLY for storybook purpose */}
    {image ? (
      <img
        alt={"Time-lapse-logo"}
        className={"time-lapse-icon mt3 mr2"}
        src={
          "https://res.cloudinary.com/fluid-attacks/image/upload/v1652469698/airs/icons/red-circle-check.png"
        }
      />
    ) : (
      <CloudImage
        alt={"Time-lapse-logo"}
        src={"/airs/icons/red-circle-check"}
        styles={"time-lapse-icon mt3 mr2"}
      />
    )}
    <TextContainer>
      <Text>{date}</Text>
      <Text>{text}</Text>
    </TextContainer>
  </Col>
);

export { TimeItem };
