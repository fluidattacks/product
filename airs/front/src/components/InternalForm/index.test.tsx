import { render, screen } from "@testing-library/react";
import React from "react";

import { InternalForm } from ".";

describe("InternalForm", (): void => {
  it("renders InternalForm correctly", (): void => {
    expect.hasAssertions();

    const { container } = render(<InternalForm />);

    expect(container).toBeInTheDocument();

    const iframeElement = screen.getByTitle("Contact Us Form");
    expect(iframeElement).toBeInTheDocument();
    expect(iframeElement).toHaveAttribute(
      "sandbox",
      "allow-forms allow-top-navigation allow-same-origin allow-scripts",
    );
    expect(iframeElement).toHaveAttribute(
      "src",
      "https://forms.zohopublic.com/fluidattacks1/form/Internalcontactus/formperma/A8qDPgOuvnpp21GF3c56RaPMQOZcJWMxUTYf6RTKntM",
    );
    expect(iframeElement).toHaveStyle({
      border: "0px",
      height: "850px",
      marginBottom: "-7px",
      width: "100%",
    });
  });
});
