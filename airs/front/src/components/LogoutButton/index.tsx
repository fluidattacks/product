import { useAuth0 } from "@auth0/auth0-react";
import React, { useCallback } from "react";

const LogoutButton: React.FC = (): JSX.Element => {
  const { logout } = useAuth0();

  const handleLogout = useCallback(async (): Promise<void> => {
    await logout({ logoutParams: { returnTo: window.location.origin } });
  }, [logout]);

  return <button onClick={handleLogout}>{"Log Out"}</button>;
};

export { LogoutButton };
