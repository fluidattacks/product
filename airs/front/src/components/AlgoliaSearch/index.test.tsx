import { render, screen } from "@testing-library/react";
import React from "react";

import { Search } from ".";

describe("AlgoliaSearch component", (): void => {
  it("AlgoliaSearch should render mocked data", (): void => {
    expect.hasAssertions();

    render(<Search indices={[{ name: "test-index", title: "test" }]} />);

    // Validate that AlgoliaSearch initialized
    expect(screen.getByTestId("instant-search")).toBeInTheDocument();
    expect(screen.getByTestId("search-box")).toBeInTheDocument();
    expect(screen.getByTestId("hits")).toBeInTheDocument();
    expect(screen.getByTestId("pagination")).toBeInTheDocument();
    expect(screen.getByTestId("powered-by")).toBeInTheDocument();
    expect(screen.getByTestId("hits")).not.toBeVisible();
  });
});
