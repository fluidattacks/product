/* eslint-disable react/forbid-component-props */
import { BsTrash2Fill } from "@react-icons/all-files/bs/BsTrash2Fill";
import React from "react";
import type { IconProps } from "react-instantsearch/dist/es/ui/SearchBox";

const ResetIcon = ({ classNames }: Readonly<IconProps>): JSX.Element => (
  <BsTrash2Fill className={classNames.submitIcon} />
);

export { ResetIcon };
