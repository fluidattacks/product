import { BsChevronDown } from "@react-icons/all-files/bs/BsChevronDown";
import { BsChevronUp } from "@react-icons/all-files/bs/BsChevronUp";
import React from "react";

type TDirection = "down" | "up";

interface IChevronProps {
  direction: TDirection;
}

interface IChevronStates {
  company: TDirection;
  platform: TDirection;
  resources: TDirection;
  services: TDirection;
}

const Chevron = ({ direction }: Readonly<IChevronProps>): JSX.Element => {
  if (direction === "down") {
    return (
      <div style={{ marginLeft: "4px" }}>
        <BsChevronDown size={12} />
      </div>
    );
  }

  return (
    <div style={{ marginLeft: "4px" }}>
      <BsChevronUp size={12} />
    </div>
  );
};

export type { TDirection, IChevronStates };
export { Chevron };
