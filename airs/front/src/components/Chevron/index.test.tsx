import "@testing-library/jest-dom";
import { render } from "@testing-library/react";
import React from "react";

import { Chevron } from ".";

describe("Chevron", (): void => {
  it("Chevron should render up chevron", (): void => {
    expect.hasAssertions();
    const { container } = render(<Chevron direction={"up"} />);
    expect(container).toBeInTheDocument();
  });
  it("Chevron should render down chevron", (): void => {
    expect.hasAssertions();
    const { container } = render(<Chevron direction={"down"} />);
    expect(container).toBeInTheDocument();
  });
});
