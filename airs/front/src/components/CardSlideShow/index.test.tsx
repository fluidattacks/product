import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import React from "react";

import { CardSlideShow } from ".";

const mockINodes: INodes[] = [
  {
    node: {
      fields: {
        slug: "slug",
      },
      frontmatter: {
        alt: "alt",
        author: "author",
        category: "category",
        certification: "certification",
        date: "date",
        description: "description",
        identifier: "identifier",
        image: "image",
        slug: "slug",
        spanish: "spanish",
        subtitle: "subtitle",
        tags: "tags",
        title: "title",
      },
    },
  },
];

describe("CardSlideShow", (): void => {
  it("CardSlideShow should render mocked data with empty nodes", (): void => {
    expect.hasAssertions();
    render(
      <CardSlideShow
        btnText={"Button text"}
        containerDescription={"Container description"}
        containerTitle={"Container title"}
        data={[]}
      />,
    );

    expect(screen.queryByText("Container title")).toBeInTheDocument();
    expect(screen.queryByText("Container description")).toBeInTheDocument();
    expect(screen.queryByText("Button text")).not.toBeInTheDocument();
  });

  it("CardSlideShow should render mocked data with non empty nodes", (): void => {
    expect.hasAssertions();

    render(
      <CardSlideShow
        btnText={"Button text"}
        containerDescription={"Container description"}
        containerTitle={"Container title"}
        data={mockINodes}
      />,
    );

    expect(screen.queryByText("Container title")).toBeInTheDocument();
    expect(screen.queryByText("Container description")).toBeInTheDocument();
    expect(screen.queryByText("Image not found")).toBeInTheDocument();
    expect(screen.queryByText("title")).toBeInTheDocument();
    expect(screen.queryByText("subtitle")).toBeInTheDocument();
    expect(screen.queryByText("Button text")).toBeInTheDocument();
  });
});
