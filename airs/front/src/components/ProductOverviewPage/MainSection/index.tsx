/* eslint react/forbid-component-props: 0 */
import React from "react";

import { Container, MainTextContainer } from "./styles";

import { FlexCenterItemsContainer } from "../../../styles/styles";
import { translate } from "../../../utils/translations/translate";
import { AirsLink } from "../../AirsLink";
import { Button } from "../../Button";
import { Text, Title } from "../../Typography";

interface IProps {
  description: string;
}

const MainSection: React.FC<IProps> = ({
  description,
}: IProps): JSX.Element => {
  return (
    <Container>
      <MainTextContainer>
        <Title color={"#f4f4f6"} level={1} size={"big"} textAlign={"center"}>
          {translate.t("productOverview.title")}
        </Title>
        <Text color={"#f4f4f6"} mb={4} mt={4} size={"big"} textAlign={"center"}>
          {description}
        </Text>
        <FlexCenterItemsContainer className={"flex-wrap"}>
          <AirsLink href={"https://app.fluidattacks.com/SignUp"}>
            <Button className={"mh2 mv3"} variant={"primary"}>
              {translate.t("productOverview.mainButton1")}
            </Button>
          </AirsLink>
          <AirsLink href={"/contact-us/"}>
            <Button className={"mh2"} variant={"darkTertiary"}>
              {translate.t("productOverview.mainButton2")}
            </Button>
          </AirsLink>
        </FlexCenterItemsContainer>
      </MainTextContainer>
    </Container>
  );
};

export { MainSection };
