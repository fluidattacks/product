import React from "react";

import { Container } from "./styles";

import { CloudImage } from "../../../CloudImage";

interface IInteractiveProps {
  image1: string;
}

const InteractiveImage: React.FC<IInteractiveProps> = ({
  image1,
}: IInteractiveProps): JSX.Element => {
  return (
    <Container>
      <CloudImage
        alt={"Image Demo"}
        src={image1}
        styles={"bs-product-image mt4"}
      />
    </Container>
  );
};

export { InteractiveImage };
