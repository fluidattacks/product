import React from "react";

import { ProductCard } from "./ProductCard";
import { CardsContainer, Container, MainTextContainer } from "./styles";

import { translate } from "../../../utils/translations/translate";
import { Text, Title } from "../../Typography";

const CardsSection: React.FC = (): JSX.Element => {
  const data = [
    {
      image: "icon-1",
      text: translate.t("productOverview.cardsSection.card1Description"),
    },
    {
      image: "icon-2",
      text: translate.t("productOverview.cardsSection.card2Description"),
    },
    {
      image: "icon-3",
      text: translate.t("productOverview.cardsSection.card3Description"),
    },
  ];

  return (
    <Container>
      <MainTextContainer>
        <Title color={"#2e2e38"} level={1} size={"small"} textAlign={"center"}>
          {translate.t("productOverview.cardsSection.title")}
        </Title>
        <Text color={"#5c5c70"} mt={2} size={"medium"}>
          {translate.t("productOverview.cardsSection.paragraph")}
        </Text>
      </MainTextContainer>
      <CardsContainer>
        {data.map((card): JSX.Element => {
          return (
            <ProductCard
              image={card.image}
              key={`card-${card.text}`}
              text={card.text}
            />
          );
        })}
      </CardsContainer>
    </Container>
  );
};

export { CardsSection };
