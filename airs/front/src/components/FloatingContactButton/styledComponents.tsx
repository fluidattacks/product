import { styled } from "styled-components";

const FloatButton = styled.div.attrs({
  className: `
    f5
    br3
    dib
    poppins
    pointer
    t-all-3-eio
  `,
})<{
  $background: string;
  $backgroundHover: string;
  $color: string;
  $yPosition: string;
}>`
  top: ${({ $yPosition }): string => $yPosition};
  color: ${({ $color }): string => $color};
  opacity: 1;
  right: -70px;
  height: 100px;
  position: fixed;
  padding: 10px 16px;
  transform: rotate(270deg);
  background-color: ${({ $background }): string => $background};

  &:hover {
    background-color: ${({ $backgroundHover }): string => $backgroundHover};
    opacity: 1;
    right: -50px;
  }
`;

const Dialog = styled.div`
  white-space: pre-line;
  background-color: #e9e9ed;
  border-radius: 4px;
  color: #2e2e38;
  display: flex;
  flex-wrap: wrap;
  font-family: Roboto, sans-serif;
  font-size: 14px;
  padding-left: 15px;
  padding-bottom: 15px;
  padding-right: 15px;
  padding-top: 10px;
  margin-top: 70px;
  height: 85%;
  max-width: 700px;
  width: 90%;
`;

const Container = styled.div.attrs({
  className: "absolute--fill fixed overflow-auto z-999",
})`
  align-items: center;
  background-color: #0008;
  display: flex;
  justify-content: center;
`;

const CloseButtonContainer = styled.div`
  display: flex;
  width: 100%;
  justify-content: end;
  max-height: 30px;
`;

export { Dialog, FloatButton, Container, CloseButtonContainer };
