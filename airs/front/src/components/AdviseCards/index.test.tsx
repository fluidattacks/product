import { render, screen } from "@testing-library/react";
import { useStaticQuery } from "gatsby";
import React from "react";

import { AdviseCards } from ".";

interface IDataAdviseCardsMock {
  allMarkdownRemark: {
    edges: [
      {
        node: {
          fields: {
            slug: string;
          };
          html: string;
          frontmatter: {
            authors: string;
            codename: string;
            cveid: string;
            date: string;
            severity: string;
            slug: string;
            title: string;
          };
        };
      },
    ];
  };
}

const mockUseStaticQuery = jest.spyOn({ useStaticQuery }, `useStaticQuery`);
const mockDataUseStaticQuery: IDataAdviseCardsMock = {
  allMarkdownRemark: {
    edges: [
      {
        node: {
          fields: {
            slug: "",
          },
          frontmatter: {
            authors: "Andres Roldan",
            codename: "Argerich",
            cveid:
              "CVE-2023-45115,CVE-2023-45116,CVE-2023-45117,CVE-2023-45118,CVE-2023-45119,CVE-2023-45120,CVE-2023-45121,CVE-2023-45122,CVE-2023-45123,CVE-2023-45124,CVE-2023-45125,CVE-2023-45126,CVE-2023-45127",
            date: "2023-11-02 12:00 COT",
            severity: "8.2",
            slug: "advisories/argerich/",
            title:
              "Online Examination System v1.0 - Multiple Authenticated SQL Injections (SQLi)",
          },
          html: "",
        },
      },
    ],
  },
};

describe(`AdviseCards`, (): void => {
  beforeEach((): void => {
    mockUseStaticQuery.mockImplementation(
      (): IDataAdviseCardsMock => mockDataUseStaticQuery,
    );
  });

  afterEach((): void => {
    jest.restoreAllMocks();
  });

  test("AdviseCards should show mocked data", (): void => {
    expect.hasAssertions();

    render(<AdviseCards />);

    expect(screen.queryByText("Severity 8.2")).toBeInTheDocument();
    expect(
      screen.queryByText(
        "Online Examination System v1.0 - Multiple Authenticated SQL Injections (SQLi)",
      ),
    ).toBeInTheDocument();
    expect(
      screen.queryByText(
        "CVE-2023-45115,CVE-2023-45116,CVE-2023-45117,CVE-2023-45118,CVE-2023-45119,CVE-2023-45120,CVE-2023-45121,CVE-2023-45122,CVE-2023-45123,CVE-2023-45124,CVE-2023-45125,CVE-2023-45126,CVE-2023-45127",
      ),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("Published: 2023-11-02 12:00 COT"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("Discovered by Andres Roldan"),
    ).toBeInTheDocument();
  });
});
