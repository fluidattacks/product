import parse from "html-react-parser";
import React from "react";
import sanitizeHtml from "sanitize-html";

import {
  ButtonDiv,
  ContentInnerDiv,
  ContentMainDiv,
  InnerDiv,
  MainDiv,
  TitleDiv,
} from "./styles";

import { translate } from "../../utils/translations/translate";
import { AirsLink } from "../AirsLink";
import { Button } from "../Button";

const ThankYouContent = ({
  content,
  title,
}: Readonly<{
  content: string;
  title: string;
}>): JSX.Element => {
  const sanitizedHTML = sanitizeHtml(content, {
    allowedClasses: {
      "*": false,
    },
  });

  return (
    <MainDiv>
      <InnerDiv>
        <TitleDiv>{title}</TitleDiv>
        <ContentMainDiv>
          <ContentInnerDiv>{parse(sanitizedHTML)}</ContentInnerDiv>
          <ButtonDiv>
            <AirsLink href={"/blog/"}>
              <Button variant={"primary"}>
                {translate.t("thankYou.button")}
              </Button>
            </AirsLink>
          </ButtonDiv>
        </ContentMainDiv>
      </InnerDiv>
    </MainDiv>
  );
};

export { ThankYouContent };
