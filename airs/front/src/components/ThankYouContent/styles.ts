import { styled } from "styled-components";

const MainDiv = styled.div.attrs({
  className: `
  vh-100
  thank-you-bg
  items-center
  justify-center
  flex
  bg-center
`,
})``;

const InnerDiv = styled.div.attrs({
  className: `
  w-100
  tc
`,
})``;

const TitleDiv = styled.div.attrs({
  className: `
  poppins
  c-fluid-bk
  f2
  fw7
  tc
  lh-solid
`,
})``;
const ContentMainDiv = styled.div.attrs({
  className: `
  w36-l
  center
`,
})``;

const ContentInnerDiv = styled.div.attrs({
  className: `
  roboto
  f4
  c-fluid-bk
  fw3
  tc
`,
})``;

const ButtonDiv = styled.div.attrs({
  className: `
  dib
  tc
  w-100-s
  w-100-m
  pv3
`,
})``;

export {
  ButtonDiv,
  ContentInnerDiv,
  ContentMainDiv,
  InnerDiv,
  MainDiv,
  TitleDiv,
};
