import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import React from "react";

import { ThankYouContent } from ".";

describe("ThankYouContent", (): void => {
  it("should render ThankYouContent container", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <ThankYouContent
        content={"This is a content"}
        title={
          "Online Examination System v1.0 - Multiple Authenticated SQL Injections (SQLi)"
        }
      />,
    );

    expect(container).toBeInTheDocument();
  });

  it("renders Button with Link to /blog/", (): void => {
    expect.hasAssertions();

    render(<ThankYouContent content={"Test content"} title={"Test title"} />);

    const buttonLink = screen.getByRole("link", { name: "thankYou.button" });

    expect(buttonLink).toBeInTheDocument();
    expect(buttonLink.getAttribute("href")).toBe("/blog/");
  });

  it("sanitizes content prop", (): void => {
    expect.hasAssertions();

    render(
      <ThankYouContent
        content={
          "<div class='parsed-html'> <p> Hello </p> <script>alert(1)</script></div>"
        }
        title={"Title"}
      />,
    );

    expect(screen.getByText("Hello")).toBeInTheDocument();
    expect(screen.getByText("Hello").parentElement).toHaveClass("parsed-html");
    expect(document.getElementsByTagName("script")).toHaveLength(0);
    expect(screen.queryByText("alert(1)")).not.toBeInTheDocument();
  });
});
