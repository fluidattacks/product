import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { useStaticQuery } from "gatsby";
import React from "react";
import { act } from "react-dom/test-utils";

import { CertificationsPage } from ".";
import { allCloudinaryMediaData } from "test-utils/mocks";
import { translate } from "utils/translations/translate";
import type { ICloudinaryMedia } from "utils/types";

interface IDataCertificationPageMock {
  allMarkdownRemark: {
    edges: [
      {
        node: {
          fields: {
            slug: string;
          };
          html: string;
          frontmatter: {
            alt: string;
            description: string;
            keywords: string;
            certification: string;
            certificationid: string;
            certificationlogo: string;
            slug: string;
            title: string;
          };
        };
      },
    ];
  };
  allCloudinaryMedia: ICloudinaryMedia;
}

const mockUseStaticQuery = jest.spyOn({ useStaticQuery }, `useStaticQuery`);
const mockDataUseStaticQuery: IDataCertificationPageMock = {
  allCloudinaryMedia: allCloudinaryMediaData,
  allMarkdownRemark: {
    edges: [
      {
        node: {
          fields: {
            slug: "",
          },
          frontmatter: {
            alt: "Alt content",
            certification: "Certification content",
            certificationid: "Certification id content",
            certificationlogo: "Certification logo content",
            description: "Description content",
            keywords: "Keywords content",
            slug: "Slug content",
            title: "Title content",
          },
          html: "",
        },
      },
    ],
  },
};

describe(`AdviseCards`, (): void => {
  beforeEach((): void => {
    mockUseStaticQuery.mockImplementation(
      (): IDataCertificationPageMock => mockDataUseStaticQuery,
    );
  });

  afterEach((): void => {
    jest.restoreAllMocks();
  });

  it("AdviseCards should render mocked data", (): void => {
    expect.hasAssertions();

    render(<CertificationsPage />);

    expect(screen.queryByText("Image not found")).toBeInTheDocument();
    expect(screen.queryByText("Title content")).toBeInTheDocument();
    expect(screen.queryByText(translate.t("clients.card"))).toBeInTheDocument();
  });
  it("AdviseCards should hide Read More option after click", async (): Promise<void> => {
    expect.hasAssertions();

    render(<CertificationsPage />);

    expect(screen.queryByRole("button")).toBeInTheDocument();

    expect(screen.queryByText(translate.t("clients.card"))).toBeInTheDocument();

    await act(async (): Promise<void> => {
      await userEvent.click(screen.getByRole("button"));
    });

    expect(
      screen.queryByText(translate.t("clients.card")),
    ).not.toBeInTheDocument();
  });
});
