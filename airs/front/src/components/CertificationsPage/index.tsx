import { graphql, useStaticQuery } from "gatsby";
import React from "react";

import { CardsContainer } from "../../styles/styles";
import { DropDownCard } from "../DropDownCard";

const CertificationsPage: React.FC = (): JSX.Element => {
  const data: IData = useStaticQuery(graphql`
    query CertificationQuery {
      allMarkdownRemark(
        filter: { frontmatter: { certification: { eq: "yes" } } }
        sort: { frontmatter: { certificationid: ASC } }
      ) {
        edges {
          node {
            fields {
              slug
            }
            html
            frontmatter {
              alt
              description
              keywords
              certification
              certificationid
              certificationlogo
              slug
              title
            }
          }
        }
      }
    }
  `);

  const { edges: certificationInfo } = data.allMarkdownRemark;

  return (
    <CardsContainer>
      {certificationInfo.map(({ node }): JSX.Element => {
        const { alt, certificationlogo, slug, title } = node.frontmatter;

        return (
          <DropDownCard
            alt={alt}
            cardType={"certifications-cards"}
            haveTitle={true}
            htmlData={node.html}
            key={slug}
            logo={certificationlogo}
            logoPaths={"/airs/about-us/certifications"}
            slug={slug}
            title={title}
          />
        );
      })}
    </CardsContainer>
  );
};

export { CertificationsPage };
