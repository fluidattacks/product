import React from "react";
import { Tooltip } from "react-tooltip";

import {
  ButtonContainer,
  FirstCircle,
  SecondCircle,
  ThirdCircle,
} from "./style";

interface IHotspotButtonHandlers {
  onClick: () => void;
}
interface IHotspotButton extends IHotspotButtonHandlers {
  id: string;
  isRight: boolean;
  tooltipMessage: string;
}

const HotSpotButton = ({
  id,
  isRight,
  onClick,
  tooltipMessage,
}: Readonly<IHotspotButton>): JSX.Element => {
  return (
    <React.Fragment>
      <ButtonContainer
        $isRight={isRight}
        data-background-color={"black"}
        data-class={"roboto"}
        data-effect={"solid"}
        data-tooltip-id={id}
        onClick={onClick}
      >
        <FirstCircle />
        <SecondCircle />
        <ThirdCircle />
      </ButtonContainer>
      {/* eslint-disable-next-line react/forbid-component-props */}
      <Tooltip content={tooltipMessage} id={id} style={{ zIndex: 10 }} />
    </React.Fragment>
  );
};

export type { IHotspotButton };
export { HotSpotButton };
