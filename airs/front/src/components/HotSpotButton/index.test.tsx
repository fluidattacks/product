import "@testing-library/jest-dom";
import { render } from "@testing-library/react";
import React from "react";

import { HotSpotButton } from ".";

const emptyFunction = (): void => {
  // This function does nothing and returns nothing (void).
};

describe("HotSpotButton", (): void => {
  it("HotSpotButton should render container with isRight", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <HotSpotButton
        id={"Id"}
        isRight={true}
        onClick={emptyFunction}
        tooltipMessage={"Tooltip message"}
      />,
    );

    expect(container).toBeInTheDocument();
  });
  it("HotSpotButton should render container with not isRight", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <HotSpotButton
        id={"Id"}
        isRight={false}
        onClick={emptyFunction}
        tooltipMessage={"Tooltip message"}
      />,
    );

    expect(container).toBeInTheDocument();
  });
});
