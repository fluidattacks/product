import { styled } from "styled-components";

const AdvisoryTableDiv = styled.div.attrs({
  className: `
    roboto
    internal
    internal-advisory
    mw-900
  `,
})``;

export { AdvisoryTableDiv };
