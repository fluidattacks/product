import React from "react";

import { Text } from "../../Typography/Text";
import { AdvisoryTableDiv } from "../styles";

interface ISummaryTableProps {
  name: string;
  code: string;
  product: string;
  state: string;
  release: string;
  "affected-versions": string;
  "fixed-versions": string;
}

const SummaryTable = ({
  name,
  code,
  product,
  state,
  release,
  "affected-versions": affectedVersions,
  "fixed-versions": fixedVersions,
}: Readonly<ISummaryTableProps>): JSX.Element => {
  return (
    <AdvisoryTableDiv>
      <table>
        <thead>
          <tr>
            <th />
            <th />
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <strong>{"Name"}</strong>
            </td>
            <td>{name}</td>
          </tr>
          <tr>
            <td>
              <strong>{"Code name"}</strong>
            </td>
            <td>
              <Text color={"#5c5c70"}>{code}</Text>
            </td>
          </tr>
          <tr>
            <td>
              <strong>{"Product"}</strong>
            </td>
            <td>{product}</td>
          </tr>
          {affectedVersions === "" ? (
            <tr data-testid={"empty-affected-versions-row"} />
          ) : (
            <tr>
              <td>
                <strong>{"Affected versions"}</strong>
              </td>
              <td>{affectedVersions}</td>
            </tr>
          )}
          {fixedVersions === "" ? (
            <tr data-testid={"empty-fixed-versions-row"} />
          ) : (
            <tr>
              <td>
                <strong>{"Fixed Versions"}</strong>
              </td>
              <td>{fixedVersions}</td>
            </tr>
          )}
          {state === "" ? (
            <tr data-testid={"empty-state-row"} />
          ) : (
            <tr>
              <td>
                <strong>{"State"}</strong>
              </td>
              <td>{state}</td>
            </tr>
          )}
          {release === "" ? (
            <tr data-testid={"empty-release-row"} />
          ) : (
            <tr>
              <td>
                <strong>{"Release date"}</strong>
              </td>
              <td>{release}</td>
            </tr>
          )}
        </tbody>
      </table>
    </AdvisoryTableDiv>
  );
};

export { SummaryTable };
