import i18next from "i18next";
import React from "react";

import { ExternalLink, InternalLink } from "./styles";
import type { ILinkProps } from "./types";

import { translatedPages } from "../../utils/translations/spanishPages";

interface IAirsLinkProps extends ILinkProps {
  children: React.ReactNode;
  href: string;
}

const AirsLink = ({
  children,
  decoration,
  hovercolor,
  href,
  onClick,
}: Readonly<IAirsLinkProps>): JSX.Element => {
  const allowLinks = [
    "https://status.fluidattacks.com",
    "https://docs.fluidattacks.com",
    "https://try.fluidattacks.com",
    "https://app.fluidattacks.com",
    "https://www.instagram.com/fluidattacks",
    "https://www.facebook.com/Fluid-Attacks-267692397253577/",
    "https://twitter.com/fluidattacks",
    "https://www.youtube.com/c/fluidattacks",
    "https://www.linkedin.com/company/fluidattacks",
    "https://try.fluidattacks.tech",
    "https://fluidattacks.docsend.com",
    "https://help.fluidattacks.com",
    "https://landing.fluidattacks.com",
    "https://availability.fluidattacks.com/",
  ];
  const currentLanguage = i18next.language;
  const spanishHref: { en: string; es: string } | undefined =
    translatedPages.find((page): boolean => page.en === href);
  const locationEs = spanishHref ? spanishHref.es : href;
  const translatedLocation = currentLanguage === "es" ? locationEs : href;
  if (allowLinks.some((link): boolean => href.startsWith(link))) {
    return (
      <ExternalLink
        $decoration={decoration}
        $hovercolor={hovercolor}
        href={href}
        onClick={onClick}
        rel={"noopener"}
        target={"_blank"}
      >
        {children}
      </ExternalLink>
    );
  } else if (href.startsWith("https://") || href.startsWith("http://")) {
    return (
      <ExternalLink
        $decoration={decoration}
        $hovercolor={hovercolor}
        href={href}
        onClick={onClick}
        rel={"nofollow noopener noreferrer"}
        target={"_blank"}
      >
        {children}
      </ExternalLink>
    );
  }

  return (
    <InternalLink
      $decoration={decoration}
      $hovercolor={hovercolor}
      onClick={onClick}
      to={translatedLocation}
    >
      {children}
    </InternalLink>
  );
};

export { AirsLink };
export type { IAirsLinkProps };
