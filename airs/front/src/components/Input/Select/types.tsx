interface ISelectProps {
  label?: string;
  onChange: (selectedOption: string) => void;
  options: string[];
  value?: string;
}

export type { ISelectProps };
