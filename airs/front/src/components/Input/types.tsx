interface IStyledInputProps {
  $bgColor?: string;
  $borderColor?: string;
}

interface IInputHandlers {
  onChange: (value: string) => void;
}
interface IInputProps extends IInputHandlers {
  label?: string;
  placeHolder?: string;
}

export type { IInputProps, IStyledInputProps };
