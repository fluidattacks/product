/* eslint react/forbid-component-props:0 */
import { BsLink45Deg } from "@react-icons/all-files/bs/BsLink45Deg";
import { Link } from "gatsby";
import React, { useCallback } from "react";
import { Highlight } from "react-instantsearch";

import type { IResultProps } from "./types";

export const SearchResults = ({ hit }: Readonly<IResultProps>): JSX.Element => {
  const { slug } = hit;
  const fixedSlug = slug.startsWith("/pages/")
    ? slug.replace("/pages/", "/")
    : slug;
  const closeMenu = useCallback((): void => {
    if (typeof document === "undefined") return;
    document.body.setAttribute("style", "overflow-y: auto;");
  }, []);

  return (
    <Link onClick={closeMenu} to={fixedSlug}>
      <div className={"HitDiv bg-white pv2 ph1 br3 bs-btm-h-5 t-all-3-eio"}>
        <Highlight attribute={"title"} className={"Hit-label"} hit={hit} />
        <BsLink45Deg className={"fr pb4 dib pr3 c-fluid-gray f4"} />
      </div>
    </Link>
  );
};
