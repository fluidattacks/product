import React from "react";

import type { ISeoProps, TType } from "./types";

import { translatedPages } from "../../utils/translations/spanishPages";
import { GatsbyHead } from "../GatsbyHead";
import type { ILinkItem, IMetaItem } from "../GatsbyHead/types";

const Seo = ({
  title,
  date = "",
  dateModified = "",
  description,
  path = "",
  author = "Fluid Attacks",
  keywords = "",
  meta,
  image = "",
  type,
}: ISeoProps): JSX.Element => {
  const isFullPath = path.startsWith("https://");
  const newLocation = `/${
    isFullPath ? path.split("/").slice(3).join("/") : path
  }`;
  const translatedPage = translatedPages.find(
    (page): boolean =>
      page.en === newLocation ||
      page.es === (isFullPath ? newLocation : `/es${newLocation}`),
  );
  const englishLink = translatedPage?.en;
  const spanishLink = translatedPage?.es;

  const siteUrl = isFullPath ? path : `https://fluidattacks.com${newLocation}`;

  const metaData: IMetaItem[] = [
    {
      content: author,
      name: "author",
    },
    {
      content: siteUrl,
      name: "canonical",
    },
    {
      content: description,
      name: "description",
    },
    {
      content: image,
      name: "image",
      property: "og:image",
    },
    {
      content: siteUrl,
      name: "og:url",
    },
    {
      content: "article",
      name: "og:type",
    },
    {
      content: title,
      name: "og:title",
    },
    {
      content: description,
      name: "og:description",
    },
    {
      content: image,
      name: "og:image",
    },
    {
      content: "summary_large_image",
      name: "twitter:card",
    },
    {
      content: author,
      name: "twitter:creator",
    },
    {
      content: title,
      name: "twitter:title",
    },
    {
      content: description,
      name: "twitter:description",
    },
    {
      content: image,
      name: "twitter:image",
    },
    {
      content: keywords,
      name: "keywords",
    },
    ...(meta ?? []),
  ];

  const defaultPage = englishLink === newLocation ? newLocation : spanishLink;
  const defaultPath = defaultPage === "/" ? "" : defaultPage;

  const linkData: ILinkItem[] =
    englishLink !== undefined && spanishLink !== undefined
      ? [
          //  If you see an error on the syntax of hreflang tag ignore it, change this name affects SEO positioning
          {
            href: `https://fluidattacks.com${defaultPath}`,
            hreflang: "x-default",
            rel: "alternate",
          },
          {
            href: `https://fluidattacks.com${englishLink}`,
            hreflang: "en",
            rel: "alternate",
          },
          {
            href: `https://fluidattacks.com${spanishLink}`,
            hreflang: "es",
            rel: "alternate",
          },
        ]
      : [];

  const headScript: Record<TType, React.ReactNode> = {
    blog: (
      <script type={"application/ld+json"}>
        {`
        {
          "@context": "https://schema.org/",
          "@type": "Article",
          "author": "${author}",
          "headline": "${title}",
          "image": "${image}",
          "datePublished": "${date}",
          "publisher": {
            "@type": "Organization",
            "name": "Fluid Attacks",
            "logo": {
              "@type": "ImageObject",
              "url": "https://res.cloudinary.com/fluid-attacks/image/upload/q_auto,f_auto/v1622583388/airs/logo_fluid_attacks_2021_eqop3k.svg"
            },
            "address": "95 3rd St, 2nd Floor",
            "location": "San Francisco, CA, 94107, Estados Unidos"
          },
          "dateModified": "${dateModified}",
          "mainEntityOfPage": "${siteUrl}",
          "description": "${description}"
        }
    `}
      </script>
    ),
    default: undefined,
    post: (
      <script type={"application/ld+json"}>
        {`
    {
      "@context": "https://schema.org/",
      "@type": "Article",
      "author": "${author}",
      "headline": "${title}",
      "image": "${image}",
      "datePublished": "${date}",
      "publisher": {
        "@type": "Organization",
        "name": "Fluid Attacks",
        "logo": {
          "@type": "ImageObject",
          "url": "https://res.cloudinary.com/fluid-attacks/image/upload/q_auto,f_auto/v1622583388/airs/logo_fluid_attacks_2021_eqop3k.svg"
        },
        "address": "95 3rd St, 2nd Floor",
        "location": "San Francisco, CA, 94107, Estados Unidos"
      },
      "dateModified": "${dateModified}",
      "mainEntityOfPage": "${siteUrl}",
      "description": "${description}"
    }
`}
      </script>
    ),

    service: (
      <script type={"application/ld+json"}>{`
        {
          "@context": "https://schema.org/",
          "@type": "Service",
          "name": "${title}",
          "image": "${image}",
          "description": "${description}",
          "brand": {
            "@type": "Brand",
            "name": "Fluid Attacks"
          },
          "offers": {
            "@type": "Offer",
            "url": "${siteUrl}"
          }
        }
    `}</script>
    ),
  };

  return (
    <GatsbyHead linkItems={linkData} metaItems={metaData} title={title}>
      {headScript[type ?? "default"]}
    </GatsbyHead>
  );
};

export { Seo };
