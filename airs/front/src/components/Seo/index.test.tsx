import { render } from "@testing-library/react";
import React from "react";

import { Seo } from ".";

const testCases = ["https://fluidattacks.com", "https://fluidattacks.com/es/"];

describe("Seo", (): void => {
  it.each(testCases)("should contain metainfo %s", (siteUrl): void => {
    expect.hasAssertions();

    const description = "This is a description";
    jest.spyOn(console, "error").mockImplementation((): undefined => undefined);

    render(
      <Seo
        description={description}
        image={"This is an image"}
        keywords={"This is a keyword"}
        path={siteUrl}
        title={"This is a title"}
      />,
    );

    expect(document.getElementsByTagName("title")[0].innerHTML).toBe(
      "This is a title",
    );
    expect(document.getElementsByTagName("meta")).toHaveLength(17);
    expect(
      document
        .querySelectorAll("meta[name='description']")[0]
        .getAttribute("content"),
    ).toBe(description);
    expect(document.getElementsByTagName("link")).toHaveLength(4);
    expect(document.querySelectorAll("link[hreflang='en']")).toHaveLength(1);
    expect(document.querySelectorAll("link[hreflang='es']")).toHaveLength(1);
    expect(
      document
        .querySelectorAll("link[hreflang='x-default']")[0]
        .getAttribute("href"),
    ).toBe(siteUrl);
    jest.restoreAllMocks();
  });
});
