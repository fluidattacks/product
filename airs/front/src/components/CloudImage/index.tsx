/* eslint react/require-default-props: 0 */
import { graphql, useStaticQuery } from "gatsby";
import React from "react";

import { useWindowSize } from "../../utils/hooks/useWindowSize";

interface IImageNode {
  secure_url: string;
  cloudinaryData: {
    height: number;
    secure_url: string;
    width: number;
  };
}

interface IData {
  allCloudinaryMedia: {
    nodes: IImageNode[];
  };
}

const CloudImage: React.FC<{
  alt: string;
  isProfile?: boolean;
  src: string;
  styles?: string;
  width?: number;
  height?: number;
}> = ({ alt, height, isProfile = false, src, styles, width }): JSX.Element => {
  const data: IData = useStaticQuery(graphql`
    query CloudinaryImage {
      allCloudinaryMedia {
        nodes {
          secure_url
          cloudinaryData {
            secure_url
            height
            width
          }
        }
      }
    }
  `);
  const srcBlog = src.endsWith(".webp") ? src.replace(".webp", "") : src;
  const isDesktop = useWindowSize().width > 1200;
  const isContent = srcBlog.includes("/blog/") || srcBlog.includes("/learn/");

  const getHeightSize = (): number | string => {
    if (typeof height === "undefined") {
      if (isContent && isDesktop) {
        return 600;
      }

      return "auto";
    }

    return height;
  };

  const getWidthSize = (): number | string => {
    if (typeof width === "undefined") {
      if (isContent && isDesktop) {
        return 900;
      }

      return "auto";
    }

    return width;
  };

  const defaultImage = (
    <img
      alt={"default"}
      src={
        "https://res.cloudinary.com/fluid-attacks/image/upload/v1671487952/airs/blogs/authors/default"
      }
    />
  );
  const imageElements = data.allCloudinaryMedia.nodes
    .filter(
      (image): boolean =>
        image.secure_url.includes(src) ||
        image.cloudinaryData.secure_url.includes(srcBlog),
    )
    .map(
      (image): JSX.Element => (
        <img
          alt={alt}
          className={styles}
          height={getHeightSize()}
          key={alt}
          loading={"lazy"}
          src={
            image.secure_url.includes(src)
              ? image.secure_url.concat(".webp")
              : image.cloudinaryData.secure_url.replace(".png", ".webp")
          }
          width={getWidthSize()}
        />
      ),
    );
  const preloadImages = data.allCloudinaryMedia.nodes
    .filter(
      (image): boolean =>
        image.secure_url.includes(src) ||
        image.cloudinaryData.secure_url.includes(srcBlog),
    )
    .map(
      (image): JSX.Element => (
        <link
          href={
            image.secure_url.includes(src)
              ? image.secure_url.concat(".webp")
              : image.cloudinaryData.secure_url.replace(".png", ".webp")
          }
          key={alt}
          rel={"preload"}
        />
      ),
    );

  if (imageElements.length < 1 && isProfile) {
    return defaultImage;
  } else if (imageElements.length < 1) {
    return <p>{"Image not found"}</p>;
  }

  return (
    <div
      style={{ display: "flex", flexWrap: "wrap", justifyContent: "center" }}
    >
      {preloadImages}
      {imageElements}
    </div>
  );
};

export { CloudImage };
