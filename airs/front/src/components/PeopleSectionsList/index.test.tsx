import "@testing-library/jest-dom";
import { render } from "@testing-library/react";
import React from "react";

import { PeopleSection } from "./Section";

import { PeopleSectionList } from ".";

describe("PeopleSectionList", (): void => {
  it("renders with the class 'w-100'", (): void => {
    expect.hasAssertions();

    const { container } = render(<PeopleSectionList />);

    expect(container).toBeInTheDocument();
    expect(container.firstChild).toHaveClass("w-100");
  });

  it("renders 4 PeopleSection components", (): void => {
    expect.hasAssertions();

    const { getByText } = render(<PeopleSectionList />);

    expect(getByText("people.fluidAttacks.title")).toBeInTheDocument();
    expect(getByText("people.hackingTeam.title")).toBeInTheDocument();
    expect(getByText("people.product.title")).toBeInTheDocument();
    expect(getByText("people.marketing.title")).toBeInTheDocument();
  });

  it("renders with left aligned image", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <PeopleSection
        description={"Description"}
        imageAlt={"Test Image"}
        imageSide={"left"}
        imageSrc={"/images/image.jpg"}
        title={"Title"}
      />,
    );

    expect(container).toBeInTheDocument();
  });

  it("renders with right aligned image", (): void => {
    const { container } = render(
      <PeopleSection
        description={"Description"}
        imageAlt={"Test Image"}
        imageSide={"right"}
        imageSrc={"/images/image.jpg"}
        title={"Title"}
      />,
    );
    expect(container).toBeInTheDocument();
  });
});
