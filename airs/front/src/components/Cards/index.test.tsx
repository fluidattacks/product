import { fireEvent, render, screen } from "@testing-library/react";
import React from "react";

import { Card } from ".";

describe("Card component", (): void => {
  it("renders correctly", (): void => {
    expect.hasAssertions();
    jest.spyOn(window, "alert").mockImplementation(jest.fn());
    const title = "Test Title";
    render(
      <Card
        banner={"test"}
        html={'<div onclick="alert(1)">Click me</div>'}
        title={title}
      >
        <div>{"Test Children"}</div>
      </Card>,
    );
    expect(screen.getByText(title)).toBeInTheDocument();
    expect(screen.getByText("Test Children")).toBeInTheDocument();
    expect(screen.getByText("Click me")).toBeInTheDocument();
    fireEvent.click(screen.getByText("Click me"));
    // Assert that alert was not called because of the sanitization
    expect(window.alert).not.toHaveBeenCalled();
  });
});
