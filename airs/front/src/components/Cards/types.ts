interface ICardProps {
  children: React.ReactNode;
  title: string;
  html: string;
  banner: string;
}

export type { ICardProps };
