import { styled } from "styled-components";

const WhiteCardContainer = styled.div.attrs({
  className: `
    w-auto
    center
    pv3
    flex
  `,
})``;

const RedParagraph = styled.p.attrs({
  className: `
    c-dkred
    f-375
    mb4
    fw7
    mt0
  `,
})``;

const WhiteParagraph = styled.p.attrs({
  className: `
    white
    f-375
    mb1
    fw7
    mt0
  `,
})``;

const SmallBlackText = styled.p.attrs({
  className: `
    mt0
    c-fluid-bk
    f5
  `,
})``;

const SmallGrayText = styled.p.attrs({
  className: `
    mt0
    c-fluid-gray
    f5
  `,
})``;

export {
  WhiteCardContainer,
  RedParagraph,
  WhiteParagraph,
  SmallBlackText,
  SmallGrayText,
};
