import React from "react";

import type { IHeroProps, IHeroTone, THeroTone } from "./types";

import { useWindowSize } from "../../utils/hooks/useWindowSize";
import { AirsLink } from "../AirsLink";
import { Button } from "../Button";
import { CloudImage } from "../CloudImage";
import { Container } from "../Container";
import { Text, Title } from "../Typography";

const Hero = ({
  button1Link,
  button1Text,
  button2Link,
  button2Text,
  image,
  imageStyles,
  paragraph,
  size = "big",
  sizeMd,
  sizeSm,
  subtitle = "",
  title,
  tone = "light",
  variant = "center",
}: IHeroProps): JSX.Element => {
  const { width } = useWindowSize();

  const tones: Record<THeroTone, IHeroTone> = {
    dark: {
      bgColor: "#25252d",
      button1: "primary",
      button2: "darkTertiary",
      paragraphColor: "#b0b0bf",
      subtitleColor: "#8f8fa3",
      titleColor: "#fafafa",
    },
    darkTransparent: {
      bgColor: "transparent",
      button1: "primary",
      button2: "darkTertiary",
      paragraphColor: "#b0b0bf",
      subtitleColor: "#8f8fa3",
      titleColor: "#fafafa",
    },
    light: {
      bgColor: "#f4f4f6",
      button1: "primary",
      button2: "tertiary",
      paragraphColor: "#65657b",
      subtitleColor: "#8f8fa3",
      titleColor: "#2e2e38",
    },
  };

  if (variant === "center") {
    return (
      <Container
        bgColor={tone === "darkTransparent" ? "unset" : tones[tone].bgColor}
        bgGradient={tone === "darkTransparent" ? tones[tone].bgColor : "unset"}
        ph={4}
        pv={5}
      >
        <Container
          align={"center"}
          center={true}
          display={"flex"}
          justify={"center"}
          maxWidth={"1440px"}
          wrap={"wrap"}
        >
          <Container pb={5} width={"50%"} widthMd={"100%"}>
            {subtitle === "" ? (
              <div />
            ) : (
              <Title
                color={tones[tone].subtitleColor}
                level={2}
                mb={2}
                size={"small"}
              >
                {subtitle}
              </Title>
            )}
            <Title
              color={tones[tone].titleColor}
              level={1}
              mb={3}
              size={size}
              sizeMd={sizeMd}
              sizeSm={sizeSm}
            >
              {title}
            </Title>
            <Text color={tones[tone].paragraphColor} size={"big"}>
              {paragraph}
            </Text>
            <Container
              display={"flex"}
              justify={"start"}
              justifyMd={"center"}
              justifySm={"unset"}
              mv={3}
              wrap={"wrap"}
            >
              <Container pv={1} width={"auto"} widthSm={"100%"}>
                <AirsLink decoration={"none"} href={button1Link}>
                  <Button display={"block"} variant={tones[tone].button1}>
                    {button1Text}
                  </Button>
                </AirsLink>
              </Container>
              <Container
                ph={width > 488 ? 3 : 0}
                pv={1}
                width={"auto"}
                widthSm={"100%"}
              >
                <AirsLink href={button2Link}>
                  <Button display={"block"} variant={tones[tone].button2}>
                    {button2Text}
                  </Button>
                </AirsLink>
              </Container>
            </Container>
          </Container>
          <Container
            display={"flex"}
            justify={"center"}
            width={"50%"}
            widthMd={"100%"}
          >
            {/** The image with https are ONLY for storybook purpose */}
            {image.startsWith("https") ? (
              <img alt={title} className={imageStyles} src={image} />
            ) : (
              <CloudImage alt={title} src={image} styles={imageStyles} />
            )}
          </Container>
        </Container>
      </Container>
    );
  }

  return (
    <Container
      bgColor={tone === "darkTransparent" ? "unset" : tones[tone].bgColor}
      bgGradient={tone === "darkTransparent" ? tones[tone].bgColor : "unset"}
      display={"flex"}
      justify={"end"}
    >
      <Container
        align={"center"}
        display={"flex"}
        justify={"end"}
        maxWidth={"1740px"}
        mr={0}
        wrap={"wrap"}
      >
        <Container
          ph={4}
          phMd={4}
          phSm={4}
          pv={5}
          width={"50%"}
          widthMd={"100%"}
        >
          <Title
            color={tones[tone].titleColor}
            level={1}
            mb={3}
            size={size}
            sizeMd={sizeMd}
            sizeSm={sizeSm}
          >
            {title}
          </Title>
          <Text color={tones[tone].paragraphColor} size={"big"}>
            {paragraph}
          </Text>
          <Container
            display={"flex"}
            justify={"start"}
            justifyMd={"center"}
            justifySm={"unset"}
            mv={3}
            wrap={"wrap"}
          >
            <Container pv={1} width={"auto"} widthSm={"100%"}>
              <AirsLink decoration={"none"} href={button1Link}>
                <Button display={"block"} variant={tones[tone].button1}>
                  {button1Text}
                </Button>
              </AirsLink>
            </Container>
            <Container
              ph={width > 480 ? 3 : 0}
              pv={1}
              width={"auto"}
              widthSm={"100%"}
            >
              <AirsLink href={button2Link}>
                <Button display={"block"} variant={tones[tone].button2}>
                  {button2Text}
                </Button>
              </AirsLink>
            </Container>
          </Container>
        </Container>
        <Container
          display={"flex"}
          justify={"end"}
          width={"50%"}
          widthMd={"100%"}
        >
          {/** The image with https are ONLY for storybook purpose */}
          {image.startsWith("https") ? (
            <img alt={title} src={image} />
          ) : (
            <CloudImage alt={title} src={image} />
          )}
        </Container>
      </Container>
    </Container>
  );
};

export { Hero };
