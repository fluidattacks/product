import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import React from "react";
import { act } from "react-dom/test-utils";

import { InformativeBanner } from ".";

describe("InformativeBanner", (): void => {
  it("InformativeBanner should show mocked data", (): void => {
    expect.hasAssertions();

    render(
      <InformativeBanner
        bgColor={"white"}
        buttonText={"Button text content"}
        title={"Title content"}
        url={"Url content"}
      />,
    );

    expect(screen.queryByText("Title content")).toBeInTheDocument();
    expect(screen.queryByText("Button text content")).toBeInTheDocument();
  });
  it("InformativeBanner should allow click on button", async (): Promise<void> => {
    expect.hasAssertions();

    const { container } = render(
      <InformativeBanner
        bgColor={"white"}
        buttonText={"Button text content"}
        title={"Title content"}
        url={"https://status.fluidattacks.com"}
      />,
    );

    await act(async (): Promise<void> => {
      await userEvent.click(
        screen.getByRole("button", {
          name: "Button text content",
        }),
      );
    });

    expect(container).toBeInTheDocument();
  });
});
