import { styled } from "styled-components";

const BannerList = styled.ul.attrs({
  className: `
    flex
    flex-nowrap
  `,
})`
  margin: 0px;
  list-style-type: none;
  padding: 0px;
  align-items: center;
  justify-content: center;
`;

const InformativeBannerContainer = styled.div.attrs({})<{
  $bgColor: string;
  $isClose: boolean;
}>`
  background-color: ${({ $bgColor }): string => $bgColor};
  display: ${({ $isClose }): string => ($isClose ? "none" : "block")};
`;

const BannerItem = styled.li`
  min-width: max-content;
  align-items: center;
  text-decoration-color: #ffffff;
`;

const CloseContainer = styled.li`
  > svg {
    color: #fcbabe;
    position: absolute;
    right: 2%;
    top: 0;
    padding-top: 8px;
  }
`;

const BannerButton = styled.button`
  background-color: transparent;
  border: none;
  color: #ffffff;
  font-size: 14px;
  font-weight: bold;
  display: flex;
  align-items: center;
  justify-content: center;
  padding-left: 0;
  text-decoration: underline;
  > svg {
    padding-left: 5px;
  }
`;

const InformativeBannerTitle = styled.p`
  min-width: max-content;
  color: white;
  font-size: 14px;
`;

export {
  BannerButton,
  BannerItem,
  BannerList,
  InformativeBannerTitle,
  CloseContainer,
  InformativeBannerContainer,
};
