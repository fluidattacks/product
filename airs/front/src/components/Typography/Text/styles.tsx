import reactMarkdown from "react-markdown";
import { styled } from "styled-components";

import type {
  ISize,
  TSize,
  TStyle,
  TTypographyTransientProps,
  TWeight,
} from "../types";

const fontStyles: Record<TStyle, string> = {
  i: "italic",
  no: "normal",
};

const fontWeights: Record<TWeight, number> = {
  bold: 7,
  regular: 4,
  semibold: 6,
};

const sizes: Record<TSize, ISize> = {
  big: { fontSize: "20px", lineHeight: "28" },
  medium: { fontSize: "16px", lineHeight: "24" },
  small: { fontSize: "14px", lineHeight: "20" },
  smallBold: { fontSize: "14px", lineHeight: "24" },
  xs: { fontSize: "12px", lineHeight: "22" },
  xxs: { fontSize: "12px", lineHeight: "22" },
};

const getSize = (defaultSize: TSize, size?: TSize): string =>
  size === undefined
    ? `font-size: ${sizes[defaultSize].fontSize};`
    : `font-size: ${sizes[size].fontSize};`;

const getLineHeight = (defaultSize: TSize, size?: TSize): string =>
  size === undefined
    ? `line-height: ${sizes[defaultSize].lineHeight}px;`
    : `line-height: ${sizes[size].lineHeight}px;`;

const getClassName = ({
  $mb = 0,
  $ml = 0,
  $mr = 0,
  $mt = 0,
  $weight = "regular",
}: TTypographyTransientProps): string =>
  `fw${fontWeights[$weight]} mb${$mb} ml${$ml} mr${$mr} mt${$mt}`;

const getStyle = ({
  $color,
  $hColor = $color,
  $display = "block",
  $fontStyle = "no",
  $textAlign = "unset",
  $size = "medium",
  $sizeMd,
  $sizeSm,
}: TTypographyTransientProps): string => `
  color: ${$color};
  display: ${$display};
  font-style: ${fontStyles[$fontStyle]};
  text-align: ${$textAlign};
  width: ${$display === "block" ? "100%" : "auto"};
  ${getLineHeight($sizeMd ?? $size, $sizeSm)}
  ${getSize($sizeMd ?? $size, $sizeSm)}
  &:hover {
    color: ${$hColor};
  }
  > p {
    margin: 0;
    word-wrap: break-word;
  }

  @media screen and (min-width: 60em) {
    ${getLineHeight($size)}
    ${getSize($size)}
  }

  @media screen and (min-width: 30em) and (max-width: 60em) {
    ${getLineHeight($size, $sizeMd)}
    ${getSize($size, $sizeMd)}
  }
`;

const StyledText = styled.p.attrs<TTypographyTransientProps>(
  (props): { className: string } => ({ className: getClassName(props) }),
)<TTypographyTransientProps>`
  ${getStyle};
`;

const StyledTextMarkdown = styled(
  reactMarkdown,
).attrs<TTypographyTransientProps>((props): { className: string } => ({
  className: getClassName(props),
}))<TTypographyTransientProps>`
  ${getStyle};
`;

export { StyledTextMarkdown, StyledText };
