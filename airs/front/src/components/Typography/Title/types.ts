import type { ITypographyProps, Nums1To4 } from "../types";

interface ITitleProps extends ITypographyProps {
  id?: string;
  level: Nums1To4;
  children: React.ReactNode;
}

export type { ITitleProps };
