import "@testing-library/jest-dom";
import { render } from "@testing-library/react";
import React from "react";

import type { ITextProps, ITitleProps } from ".";
import { Text, Title } from ".";

const nonStringChildren = <span>{"Non-string content"}</span>;

const propsTitle: ITitleProps = {
  children: nonStringChildren,
  color: "red",
  display: "block",
  level: 1,
  mb: 0,
  textAlign: "center",
};

const props: ITextProps = {
  children: nonStringChildren,
  color: "red",
  display: "block",
  mb: 0,
  textAlign: "center",
};

describe("Styled components", (): void => {
  it("renders Title when children not string", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <Title
        color={propsTitle.color}
        display={propsTitle.display}
        level={propsTitle.level}
        mb={propsTitle.mb}
        textAlign={propsTitle.textAlign}
      >
        {propsTitle.children}
      </Title>,
    );

    expect(container.firstChild).toBeInTheDocument();
    expect(container.firstChild).toContainHTML(
      "<span>Non-string content</span>",
    );
  });

  it("renders Text when children not string", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <Text
        color={props.color}
        display={props.display}
        mb={props.mb}
        textAlign={props.textAlign}
      >
        {props.children}
      </Text>,
    );

    expect(container.firstChild).toBeInTheDocument();
    expect(container.firstChild).toContainHTML(
      "<span>Non-string content</span>",
    );
  });
  it("renders Text when children is string", (): void => {
    expect.hasAssertions();

    const propslocal: ITextProps = {
      children: "string Children",
      color: "red",
      display: "block",
      mb: 0,
      textAlign: "center",
    };

    const { container } = render(
      <Text
        color={propslocal.color}
        display={propslocal.display}
        mb={propslocal.mb}
        textAlign={propslocal.textAlign}
      >
        {propslocal.children}
      </Text>,
    );

    expect(container.firstChild).toBeInTheDocument();
    expect(container.firstChild).toContainHTML("string Children");
  });
});
