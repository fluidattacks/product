import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import React from "react";

import { MessageBanner } from ".";

describe("MessageBanner", (): void => {
  it("render message banner", (): void => {
    render(
      <MessageBanner
        buttonText={"test button"}
        linkButton={"https://fluidattacks.com"}
        message={<b>{"test message"}</b>}
      />,
    );

    expect(screen.getByText("test button")).toBeInTheDocument();
    expect(screen.getByText("test message")).toBeInTheDocument();
    expect(screen.getAllByRole("link", { hidden: true })[0]).toHaveAttribute(
      "href",
      "https://fluidattacks.com",
    );
  });

  it("displays banner correctly in mobile", (): void => {
    expect.hasAssertions();

    jest.replaceProperty(window, "innerWidth", 400);
    sessionStorage.setItem("show", JSON.stringify(true));
    render(
      <MessageBanner
        additionalDescription={"second test message"}
        buttonText={"test button"}
        linkButton={"https://fluidattacks.com"}
        message={<b>{"test message"}</b>}
      />,
    );

    expect(screen.getByRole("button")).toBeInTheDocument();
    expect(screen.getByText("test message")).toBeInTheDocument();
    expect(screen.getByText("second test message")).toBeInTheDocument();
    expect(screen.getAllByRole("link", { hidden: true })[0]).toHaveAttribute(
      "href",
      "https://fluidattacks.com",
    );
    jest.restoreAllMocks();
  });
});
