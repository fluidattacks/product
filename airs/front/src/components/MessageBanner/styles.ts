import { styled } from "styled-components";

const Button = styled.button`
  align-items: center;
  background-color: transparent;
  border: unset;
  cursor: pointer;
  display: flex;
  justify-content: center;
  padding: 0;

  a {
    color: #ffffff;
    font-size: 14px;
    margin: 0;
  }

  &:hover a {
    color: #f9959e;
  }
`;

const CloseIcon = styled.div`
  align-items: center;
  cursor: pointer;
  display: flex;
  height: 12px;
  justify-content: center;
  width: 12px;

  svg {
    color: #ffffff;
    font-size: 14px;

    &:hover {
      color: #f9959e;
    }
  }
`;

export { Button, CloseIcon };
