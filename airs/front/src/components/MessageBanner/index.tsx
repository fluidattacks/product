import React, { useCallback } from "react";
import { FaXmark } from "react-icons/fa6";

import { Button, CloseIcon } from "./styles";
import type { IMessageBannerProps } from "./types";

import { useStoredState } from "../../utils/hooks/useStorageState";
import { AirsLink } from "../AirsLink";
import { Container } from "../Container";
import { Text } from "../Typography";

const MessageBanner = ({
  additionalDescription,
  buttonText,
  message,
  mobileDescription,
  linkButton,
  onClose,
}: IMessageBannerProps): JSX.Element => {
  const [show, setShow] = useStoredState("show", true);
  const handleClose = useCallback((): void => {
    setShow((previousState): boolean => !previousState);
  }, [setShow]);

  return (
    <Container
      align={"center"}
      bgGradient={"90deg, #F32637 0%, #B8075D 100%"}
      display={show ? "flex" : "none"}
      id={"message-banner"}
      justify={"between"}
      ph={4}
      pv={2}
    >
      <Container
        align={"center"}
        display={"flex"}
        gap={"4px"}
        justify={"center"}
        pv={1}
        wrap={"wrap"}
      >
        <Text
          color={"#ffffff"}
          display={"inline-block"}
          size={"small"}
          textAlign={"center"}
        >
          {message}
        </Text>
        {mobileDescription === undefined ? undefined : (
          <Text
            color={"#ffffff"}
            display={"inline-block"}
            size={"small"}
            textAlign={"center"}
          >
            {mobileDescription}
          </Text>
        )}
        <Text
          color={"#ffffff"}
          display={"inline-block"}
          size={"small"}
          textAlign={"center"}
        >
          {additionalDescription}
        </Text>
        <Button>
          {<AirsLink href={linkButton}>{buttonText}</AirsLink>}&nbsp;{"🏅"}
        </Button>
      </Container>
      <CloseIcon onClick={onClose ?? handleClose}>
        <FaXmark />
      </CloseIcon>
    </Container>
  );
};

export { MessageBanner };
