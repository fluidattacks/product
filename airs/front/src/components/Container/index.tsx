import React from "react";

import { StyledContainer } from "./styles";
import type { IContainerProps } from "./types";

const Container = ({
  align,
  bgColor,
  bgGradient,
  borderBottomColor,
  borderTopColor,
  borderColor,
  borderHoverColor,
  br,
  center,
  children,
  classname,
  direction,
  display,
  displayMd,
  displaySm,
  gap,
  height,
  hovercolor,
  hoverShadow,
  id,
  justify,
  justifyMd,
  justifySm,
  leftBar,
  maxWidth,
  mb,
  mh,
  minHeight,
  minWidth,
  minWidthMd,
  minWidthSm,
  ml,
  mr,
  mt,
  mtMd,
  mtSm,
  mv,
  onClick,
  pb,
  ph,
  phMd,
  phSm,
  pl,
  position,
  pr,
  pt,
  pv,
  pvMd,
  pvSm,
  shadow,
  shadowBottom,
  textHoverColor,
  topBar,
  width,
  widthMd,
  widthSm,
  wrap,
}: IContainerProps): JSX.Element => {
  return (
    <StyledContainer
      $align={align}
      $bgColor={bgColor}
      $bgGradient={bgGradient}
      $borderBottomColor={borderBottomColor}
      $borderColor={borderColor}
      $borderHoverColor={borderHoverColor}
      $borderTopColor={borderTopColor}
      $br={br}
      $center={center}
      $classname={classname}
      $direction={direction}
      $display={display}
      $displayMd={displayMd}
      $displaySm={displaySm}
      $gap={gap}
      $height={height}
      $hoverShadow={hoverShadow}
      $hovercolor={hovercolor}
      $id={id}
      $justify={justify}
      $justifyMd={justifyMd}
      $justifySm={justifySm}
      $leftBar={leftBar}
      $maxWidth={maxWidth}
      $mb={mb}
      $mh={mh}
      $minHeight={minHeight}
      $minWidth={minWidth}
      $minWidthMd={minWidthMd}
      $minWidthSm={minWidthSm}
      $ml={ml}
      $mr={mr}
      $mt={mt}
      $mtMd={mtMd}
      $mtSm={mtSm}
      $mv={mv}
      $pb={pb}
      $ph={ph}
      $phMd={phMd}
      $phSm={phSm}
      $pl={pl}
      $pointer={onClick !== undefined}
      $position={position}
      $pr={pr}
      $pt={pt}
      $pv={pv}
      $pvMd={pvMd}
      $pvSm={pvSm}
      $shadow={shadow}
      $shadowBottom={shadowBottom}
      $textHoverColor={textHoverColor}
      $topBar={topBar}
      $width={width}
      $widthMd={widthMd}
      $widthSm={widthSm}
      $wrap={wrap}
      onClick={onClick}
    >
      {children}
    </StyledContainer>
  );
};

export { Container };
