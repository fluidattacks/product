/* eslint react/forbid-component-props: 0 */
import { IoIosArrowDown } from "@react-icons/all-files/io/IoIosArrowDown";
import { IoIosArrowUp } from "@react-icons/all-files/io/IoIosArrowUp";
import parse from "html-react-parser";
import React, { useCallback, useState } from "react";
import sanitizeHtml from "sanitize-html";

import {
  CardBody,
  CardContainer,
  CardHeader,
  CardReadMore,
} from "../../styles/styles";
import { translate } from "../../utils/translations/translate";
import { CloudImage } from "../CloudImage";

interface IProps {
  alt: string;
  cardType: string;
  haveTitle: boolean;
  htmlData: string;
  logo: string;
  logoPaths: string;
  slug: string;
  title: string;
}

const DropDownCard = ({
  alt,
  cardType,
  haveTitle,
  htmlData,
  logo,
  logoPaths,
  slug,
  title,
}: IProps): JSX.Element => {
  const [isTouch, setIsTouch] = useState(false);
  const handleOpenClose = useCallback((): void => {
    setIsTouch((previousState): boolean => !previousState);
  }, []);
  const sanitizedHTML = sanitizeHtml(htmlData, {
    allowedClasses: {
      "*": false,
    },
  });

  return (
    <CardContainer className={cardType} key={slug}>
      <CardHeader onClick={handleOpenClose}>
        <CloudImage alt={alt} src={`${logoPaths}/${logo}`} />
        <br />
        {haveTitle ? (
          <React.Fragment>
            <br />
            <div className={"mb5"}>
              <h4>{title}</h4>
            </div>
          </React.Fragment>
        ) : undefined}
        <CardReadMore>
          {isTouch ? undefined : translate.t("clients.card")}
          <br />
          {isTouch ? (
            <IoIosArrowDown className={"arrow w1 pv3"} />
          ) : (
            <IoIosArrowUp className={"arrow w1 pv3"} />
          )}
        </CardReadMore>
      </CardHeader>
      <CardBody
        style={{
          height: isTouch ? "30rem" : "0",
          marginTop: isTouch ? "-3rem" : "0",
        }}
      >
        <div className={"down-cards"}>{parse(sanitizedHTML)}</div>
      </CardBody>
    </CardContainer>
  );
};
export { DropDownCard };
