import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { useStaticQuery } from "gatsby";
import React from "react";

import { DropDownCard } from ".";
import { allCloudinaryMediaData } from "test-utils/mocks";
import { translate } from "utils/translations/translate";
import type { ICloudinaryMedia } from "utils/types";

interface IDataDropDownCardMock {
  allCloudinaryMedia: ICloudinaryMedia;
}

const mockUseStaticQuery = jest.spyOn({ useStaticQuery }, `useStaticQuery`);
const mockDataUseStaticQuery: IDataDropDownCardMock = {
  allCloudinaryMedia: allCloudinaryMediaData,
};

describe("DropDownCard", (): void => {
  beforeEach((): void => {
    mockUseStaticQuery.mockImplementation(
      (): IDataDropDownCardMock => mockDataUseStaticQuery,
    );
  });
  afterEach((): void => {
    jest.restoreAllMocks();
  });
  it("DropDownCard should render mocked data with haveTitle", (): void => {
    expect.hasAssertions();

    render(
      <DropDownCard
        alt={"Alt content"}
        cardType={"Card type content"}
        haveTitle={true}
        htmlData={"Html data content"}
        key={"Key content"}
        logo={"Partner logo content"}
        logoPaths={"Logo path content"}
        slug={"Slug content"}
        title={"Title content"}
      />,
    );

    expect(screen.queryByText("Image not found")).toBeInTheDocument();
    expect(screen.queryByText("Title content")).toBeInTheDocument();
    expect(screen.queryByText(translate.t("clients.card"))).toBeInTheDocument();
    expect(screen.queryByText("Html data content")).toBeInTheDocument();
  });
});
