import { styled } from "styled-components";

import { AirsLink } from "../AirsLink";

interface ITocComponentsProps {
  $selected: boolean;
}

const TocUl = styled.ul`
  max-width: 310px;
  background-color: #ffffff;
  padding-inline-start: 0px;

  li::marker {
    content: "";
  }
  @media screen and (max-width: 1200px) {
    max-width: 600px;
    width: 60%;
  }
`;

const TocLi = styled.li<ITocComponentsProps>`
  width: 105%;
  border-radius: 8px;
  background-color: ${({ $selected }): string =>
    $selected ? "#fddfe2" : "transparent"};
  margin-top: 2px;
  margin-left: 19px;
  font-size: 16px;

  &:hover {
    background-color: ${({ $selected }): string =>
      $selected ? "#fddfe2" : "#f4f4f6"};
  }
`;

const TocButton = styled.div<ITocComponentsProps>`
  border-left: ${({ $selected }): string =>
    $selected ? "2px solid #bf0b1a" : "2px solid #f4f4f6"};
`;

const TocLiContainer = styled.div`
  width: 100%;
`;

const TocLiButton = styled.div<ITocComponentsProps>`
  display: flex;
  align-items: center;
  color: ${({ $selected }): string => ($selected ? "#bf0b1a" : "#2e2e38")};
  font-weight: ${({ $selected }): string => ($selected ? "700" : "400")};
  width: 100%;
  padding: 10px;
`;

const TocLink = styled(AirsLink)`
  width: 100%;
`;

export { TocButton, TocLi, TocLink, TocLiButton, TocLiContainer, TocUl };
