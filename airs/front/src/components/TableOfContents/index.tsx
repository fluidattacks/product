import React, { useCallback, useState } from "react";

import { TocButton, TocLi, TocLiButton, TocLiContainer, TocUl } from "./styles";

import { useWindowSize } from "../../utils/hooks/useWindowSize";
import { translate } from "../../utils/translations/translate";
import { AirsLink } from "../AirsLink";
import { Container } from "../Container";
import { Title } from "../Typography";

interface ITableOfContentsProps {
  headings: {
    depth: number;
    value: string;
  }[];
}

const TableOfContents = ({ headings }: ITableOfContentsProps): JSX.Element => {
  const [current, setCurrent] = useState("");
  const { width } = useWindowSize();

  const showCurrent = useCallback(
    (href: string): VoidFunction =>
      (): void => {
        setCurrent(href);
      },
    [],
  );

  if (headings.length < 1) {
    return <div />;
  }

  return (
    <Container
      display={"flex"}
      justify={"center"}
      pt={width < 1200 ? 3 : 0}
      wrap={"wrap"}
    >
      <Container height={"20px"} pb={4}>
        <Title color={"#2e2e38"} level={3} size={"small"} textAlign={"center"}>
          {translate.t("tableOfContents.title")}
        </Title>
      </Container>

      <TocUl>
        {headings.map((heading): JSX.Element => {
          const href = heading.value
            .replace(/[^\w\s-]/giu, "")
            .replace(/ /gu, "-")
            .toLowerCase();

          return (
            <TocButton
              $selected={current === href}
              key={heading.value}
              onClick={showCurrent(href)}
            >
              <TocLi $selected={current === href}>
                <TocLiContainer>
                  <AirsLink
                    decoration={"none"}
                    hovercolor={current === href ? "#bf0b1a" : "#2e2e38"}
                    href={`#${href}`}
                  >
                    <TocLiButton $selected={current === href}>
                      {heading.value}
                    </TocLiButton>
                  </AirsLink>
                </TocLiContainer>
              </TocLi>
            </TocButton>
          );
        })}
      </TocUl>
    </Container>
  );
};

export { TableOfContents };
