type Nums1To4 = 1 | 2 | 3 | 4;

interface ITransientProps {
  columns: Nums1To4;
  columnsMd?: Nums1To4;
  columnsSm?: Nums1To4;
  gap: string;
  pv?: string;
  ph?: string;
}
interface IGridProps extends ITransientProps {
  children: React.ReactNode;
}

type TStyledGridProps = TPrefixWithDollar<ITransientProps>;

export type { IGridProps, Nums1To4, TStyledGridProps };
