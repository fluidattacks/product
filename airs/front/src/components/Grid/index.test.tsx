import { render, screen } from "@testing-library/react";
import React from "react";

import { Grid } from ".";

describe("Grid", (): void => {
  it("should render", (): void => {
    expect.hasAssertions();
    render(
      <Grid columns={2} gap={"8px"}>
        <p>{"Grid"}</p>
        <p>{"Grid"}</p>
      </Grid>,
    );
    expect(screen.getByRole("grid")).toBeInTheDocument();
    expect(screen.getAllByText("Grid")).toHaveLength(2);
  });
});
