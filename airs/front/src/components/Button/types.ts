import type {
  ButtonHTMLAttributes,
  MouseEventHandler,
  ReactElement,
  ReactNode,
} from "react";
import type { IconType } from "react-icons";

type TDisplay = "block" | "inline-block" | "inline";
type TSize = "lg" | "md" | "sm";
type TVariant =
  | "darkGhost"
  | "darkSecondary"
  | "darkTertiary"
  | "ghost"
  | "primary"
  | "secondary"
  | "tertiary"
  | "transparent";

interface ITransientProps {
  customSize?: ISize;
  display?: TDisplay;
  selected?: boolean;
  size?: TSize;
  variant?: TVariant;
}
interface IStyledButtonProps extends TPrefixWithDollar<ITransientProps> {
  type: ButtonHTMLAttributes<HTMLButtonElement>["type"];
}

interface ISize {
  fontSize: number;
  ph: number;
  pv: number;
}

interface IVariant {
  bgColor: string;
  bgColorHover: string;
  borderColor: string;
  borderRadius: number;
  borderSize: number;
  color: string;
  colorHover: string;
}

interface IButtonProps
  extends ITransientProps,
    ButtonHTMLAttributes<HTMLButtonElement> {
  children?: ReactNode;
  icon?: ReactElement<IconType>;
  iconSide?: "left" | "right";
  onClick?: MouseEventHandler<HTMLButtonElement>;
}

export type {
  IButtonProps,
  ISize,
  IStyledButtonProps,
  IVariant,
  TDisplay,
  TSize,
  TVariant,
};
