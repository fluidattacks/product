import { render, screen } from "@testing-library/react";
import React from "react";

import { Button } from ".";

describe("Button", (): void => {
  it("should render", (): void => {
    expect.hasAssertions();
    render(
      <Button>
        <p>{"Button"}</p>
      </Button>,
    );
    expect(screen.getByRole("button")).toBeInTheDocument();
    expect(screen.getByText("Button")).toBeInTheDocument();
  });

  it("should render with icon", (): void => {
    expect.hasAssertions();
    render(
      <Button icon={<p>{"Icon"}</p>}>
        <p>{"Button"}</p>
      </Button>,
    );
    expect(screen.getByRole("button")).toBeInTheDocument();
    expect(screen.getByText("Button")).toBeInTheDocument();
    expect(screen.getByText("Icon")).toBeInTheDocument();
  });

  it("should call onClick", (): void => {
    expect.hasAssertions();
    const onClick = jest.fn();
    render(<Button onClick={onClick} />);
    expect(screen.getByRole("button")).toBeInTheDocument();
    screen.getByRole("button").click();
    expect(onClick).toHaveBeenCalledTimes(1);
  });

  it("should by disabled", (): void => {
    expect.hasAssertions();
    render(<Button disabled={true} />);
    expect(screen.getByRole("button")).toBeInTheDocument();
    expect(screen.getByRole("button")).toBeDisabled();
  });
});
