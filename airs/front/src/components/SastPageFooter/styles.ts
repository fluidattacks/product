import { styled } from "styled-components";

const LanguagesListContainer = styled.div.attrs({
  className: `
  mw-1366
  ph-body
  center
  pv5
  v-top
  mb5
`,
})``;

const ListContainer = styled.div.attrs({
  className: `
  center
  mw8
  tc
  roboto
`,
})``;

const ListColumn = styled.ul.attrs({
  className: `
  list
  f5
  dib-l
  ph4
  mv0
  pv0-l
  pv4
  tl
`,
})`
  color: #5c5c70;
`;

const SastParagraph = styled.div.attrs({
  className: `
    center
    roboto
    f3-l
    f4
    lh-2
    pv4
    tc
  `,
})``;

export { LanguagesListContainer, ListColumn, ListContainer, SastParagraph };
