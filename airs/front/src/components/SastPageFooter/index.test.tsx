import { render, screen } from "@testing-library/react";
import React from "react";

import { SastPageFooter } from ".";

describe("SastPageFooter", (): void => {
  test("SastPageFooter renders correctly", (): void => {
    expect.hasAssertions();

    render(<SastPageFooter />);

    expect(
      screen.getByText("sastCategoryParagraph.supportedLanguages"),
    ).toBeInTheDocument();
    expect(screen.getByText("sastCategoryParagraph.bold1")).toBeInTheDocument();
    expect(screen.getByText("C#")).toBeInTheDocument();
    expect(screen.queryByText("Image not found")).toBeInTheDocument();
  });
});
