import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { useStaticQuery } from "gatsby";
import React from "react";

import { PresentationCard } from ".";
import { allCloudinaryMediaData } from "test-utils/mocks";
import type { ICloudinaryMedia } from "utils/types";

interface IDataImageMock {
  allCloudinaryMedia: ICloudinaryMedia;
}

const mockUseStaticQuery = jest.spyOn({ useStaticQuery }, `useStaticQuery`);
const mockDataUseStaticQuery: IDataImageMock = {
  allCloudinaryMedia: allCloudinaryMediaData,
};

describe("ShadowedCard", (): void => {
  beforeEach((): void => {
    mockUseStaticQuery.mockImplementation(
      (): IDataImageMock => mockDataUseStaticQuery,
    );
  });

  afterEach((): void => {
    jest.restoreAllMocks();
  });
  it("should render ShadowedCard", (): void => {
    expect.hasAssertions();

    const { getByText, getAllByAltText } = render(
      <PresentationCard image={"image.png"} text={"text"} />,
    );

    expect(getByText("text")).toBeInTheDocument();
    expect(getByText("text")).toHaveStyle({
      color: expect.stringMatching(/46, 46, 56/u),
    });
    expect(getAllByAltText("default")).toHaveLength(1);
  });

  it("should render PresentationCard with image starting with https", (): void => {
    render(
      <PresentationCard
        image={"https://example.com/image.jpg"}
        text={"text"}
      />,
    );

    const imageElement = screen.getByRole("img", {
      name: "https://example.com/image.jpg",
    });
    expect(imageElement).toHaveAttribute(
      "src",
      "https://example.com/image.jpg",
    );
    expect(imageElement).toHaveAttribute("alt");
  });
});
