interface IFaqProps {
  children?: React.ReactNode;
  title: string;
}

export type { IFaqProps };
