import { render } from "@testing-library/react";
import React from "react";

import type { IHeadProps } from "./types";

import { GatsbyHead } from ".";

describe("Head", (): void => {
  const props: IHeadProps = {
    children: (
      <script>
        {`{
        "@context": "https://schema.org",
        "@type": "Organization",
        "url": "https://fluidattacks.com",
        "name": "Fluid Attacks",
        "contactPoint": {
          "@type": "ContactPoint",
          "telephone": "+57 333 222 4400",
          "contactType": "Customer service"
        }
       }`}
      </script>
    ),
    linkItems: [
      {
        href: "https://fluidattacks.com",
        hreflang: "en",
        rel: "canonical",
      },
      {
        href: "https://fluidattacks.com/es/",
        hreflang: "es",
        rel: "alternate",
      },
    ],
    metaItems: [
      {
        content: "Test content",
        name: "description",
      },
      {
        content: "Test content 2",
        name: "Test name",
      },
    ],
    title: "Test title",
  };
  it("should render Head component with props", (): void => {
    expect.hasAssertions();

    const error = jest
      .spyOn(console, "error")
      .mockImplementation((): undefined => undefined);

    render(
      <GatsbyHead
        linkItems={props.linkItems}
        metaItems={props.metaItems}
        title={props.title}
      >
        {props.children}
      </GatsbyHead>,
    );
    const title = document.getElementsByTagName("title");
    expect(title).toHaveLength(1);
    expect(title[0].innerHTML).toBe("Test title");
    expect(document.getElementsByTagName("link")).toHaveLength(3);
    expect(document.getElementsByTagName("meta")).toHaveLength(4);
    expect(document.getElementsByTagName("script")).toHaveLength(4);
    // The error is due to the use of html tag in head component (it is used to set html attributes)
    expect(error.mock.calls[0][1]).toMatch(/<html>/u);
    jest.restoreAllMocks();
  });
});
