import React from "react";
import {
  FacebookIcon,
  FacebookShareButton,
  LinkedinIcon,
  LinkedinShareButton,
  TwitterShareButton,
  XIcon,
} from "react-share";

import { IconContainer } from "./styles";

import { translate } from "../../utils/translations/translate";
import { Container } from "../Container";
import { Text } from "../Typography";

interface IShareProps {
  slug: string;
}

const ShareSection = ({ slug }: IShareProps): JSX.Element => {
  return (
    <Container
      display={"flex"}
      justify={"center"}
      justifySm={"center"}
      wrap={"wrap"}
    >
      <Container mv={3} width={"auto"} widthSm={"100%"}>
        <Text
          color={"#2e2e38"}
          size={"medium"}
          textAlign={"center"}
          weight={"bold"}
        >
          {translate.t("blog.share.title")}
        </Text>
      </Container>
      <Container display={"flex"} justify={"center"} widthSm={"auto"}>
        <FacebookShareButton
          title={translate.t("blog.share.facebook")}
          url={`https://fluidattacks.com/blog/${slug}`}
        >
          <IconContainer>
            <FacebookIcon
              bgStyle={{
                fill: "#2e2e38",
              }}
              round={true}
              size={50}
            />
          </IconContainer>
        </FacebookShareButton>
      </Container>
      <Container
        display={"flex"}
        justify={"center"}
        phSm={3}
        pv={3}
        pvSm={0}
        widthSm={"auto"}
      >
        <LinkedinShareButton
          title={translate.t("blog.share.linkedin")}
          url={`https://fluidattacks.com/blog/${slug}`}
        >
          <IconContainer>
            <LinkedinIcon
              bgStyle={{
                fill: "#2e2e38",
              }}
              round={true}
              size={50}
            />
          </IconContainer>
        </LinkedinShareButton>
      </Container>
      <Container display={"flex"} justify={"center"} widthSm={"auto"}>
        <TwitterShareButton
          title={translate.t("blog.share.twitter")}
          url={`https://fluidattacks.com/blog/${slug}`}
        >
          <IconContainer>
            <XIcon
              bgStyle={{
                fill: "#2e2e38",
              }}
              round={true}
              size={50}
            />
          </IconContainer>
        </TwitterShareButton>
      </Container>
    </Container>
  );
};

export { ShareSection };
