interface IBlogCta {
  buttontxt: string;
  link: string;
  title: string;
  paragraph?: string;
}

export type { IBlogCta };
