import { BsArrowRightShort } from "@react-icons/all-files/bs/BsArrowRightShort";
import i18next from "i18next";
import React, { useCallback } from "react";

import {
  CardFooterHomeSolutions,
  CardLink,
  MainCoverHome,
  SlideShow,
  SolutionsContainer,
} from "./styles";

import { AirsLink } from "../../../components/AirsLink";
import { Button } from "../../../components/Button";
import { Container } from "../../../components/Container";
import { Text, Title } from "../../../components/Typography";
import { useWindowSize } from "../../../utils/hooks/useWindowSize";
import { translatedPages } from "../../../utils/translations/spanishPages";
import { translate } from "../../../utils/translations/translate";

const SolutionsSection: React.FC = (): JSX.Element => {
  const { width } = useWindowSize();

  const data = [
    {
      description: translate.t("solutions.homeCards.devSecOps.paragraph"),
      title: translate.t("solutions.homeCards.devSecOps.title"),
      urlCard: "/solutions/devsecops/",
    },
    {
      description: translate.t("solutions.homeCards.secureCode.paragraph"),
      title: translate.t("solutions.homeCards.secureCode.title"),
      urlCard: "/solutions/secure-code-review/",
    },
    {
      description: translate.t("solutions.homeCards.redTeaming.paragraph"),
      title: translate.t("solutions.homeCards.redTeaming.title"),
      urlCard: "/solutions/red-teaming/",
    },
    {
      description: translate.t("solutions.homeCards.securityTesting.paragraph"),
      title: translate.t("solutions.homeCards.securityTesting.title"),
      urlCard: "/solutions/security-testing/",
    },
    {
      description: translate.t("solutions.homeCards.ethicalHacking.paragraph"),
      title: translate.t("solutions.homeCards.ethicalHacking.title"),
      urlCard: "/solutions/ethical-hacking/",
    },
    {
      description: translate.t(
        "solutions.homeCards.vulnerabilityManagement.paragraph",
      ),
      title: translate.t("solutions.homeCards.vulnerabilityManagement.title"),
      urlCard: "/solutions/vulnerability-management/",
    },
    {
      description: translate.t(
        "solutions.homeCards.appSecurityPostureManagement.paragraph",
      ),
      title: translate.t(
        "solutions.homeCards.appSecurityPostureManagement.title",
      ),
      urlCard: "/solutions/app-security-posture-management/",
    },
  ];

  const handleClick = useCallback(
    (url: string): VoidFunction =>
      (): void => {
        const currentLanguage = i18next.language;
        const spanishUrl: { es: string } | undefined = translatedPages.find(
          (page): boolean => page.en === url,
        );
        const locationEs = spanishUrl ? spanishUrl.es : url;
        const translatedLocation =
          currentLanguage === "es" ? locationEs.replace("/es", "") : url;
        if (typeof window !== "undefined") {
          const actLocation = window.location.href;
          window.location.assign(
            actLocation.concat(translatedLocation.slice(1)),
          );
        }
      },
    [],
  );

  return (
    <MainCoverHome>
      <Container>
        <Container
          align={"center"}
          center={true}
          display={"flex"}
          justify={"center"}
          maxWidth={"850px"}
          mb={4}
          minHeight={"360px"}
          wrap={"wrap"}
        >
          <Title
            color={"#ffffff"}
            level={1}
            mb={4}
            ml={2}
            mr={2}
            mt={5}
            size={"medium"}
            textAlign={"center"}
          >
            {translate.t("home.solutions.title")}
          </Title>
          <Text color={"#b0b0bf"} mb={4} size={"big"} textAlign={"center"}>
            {translate.t("home.solutions.subtitle")}
          </Text>
          <AirsLink href={"/solutions/"}>
            <Button display={"block"} variant={"primary"}>
              {translate.t("home.solutions.button")}
            </Button>
          </AirsLink>
        </Container>
        <SolutionsContainer $gradientColor={"#fffff"} $maxWidth={width}>
          <SlideShow>
            {[0, 1].map((): JSX.Element[] =>
              data.map(
                (card): JSX.Element => (
                  <Container
                    align={"start"}
                    bgGradient={"#ffffff, #f4f4f6"}
                    borderColor={"#dddde3"}
                    borderHoverColor={"#bf0b1a"}
                    br={2}
                    direction={"column"}
                    display={"flex"}
                    height={"318px"}
                    hovercolor={"#ffe5e7"}
                    key={card.urlCard}
                    onClick={handleClick(card.urlCard)}
                    ph={3}
                    pv={3}
                    width={"338px"}
                  >
                    <CardLink>
                      <Title color={"#bf0b1a"} level={4} size={"xxs"}>
                        {translate.t("home.solutions.cardTitle")}
                      </Title>
                      <Title
                        color={"#2e2e38"}
                        level={4}
                        mb={3}
                        mt={3}
                        size={"small"}
                      >
                        {card.title}
                      </Title>
                      <Text color={"#535365"} size={"medium"}>
                        {card.description}
                      </Text>
                      <CardFooterHomeSolutions id={"link"}>
                        <Container
                          align={"center"}
                          display={"flex"}
                          justify={"end"}
                          pb={2}
                          pt={4}
                          wrap={"wrap"}
                        >
                          <AirsLink
                            decoration={"underline"}
                            href={card.urlCard}
                          >
                            <Text
                              color={"#2e2e38"}
                              size={"small"}
                              weight={"bold"}
                            >
                              {translate.t("home.solutions.cardLink")}
                            </Text>
                          </AirsLink>
                          <BsArrowRightShort size={20} />
                        </Container>
                      </CardFooterHomeSolutions>
                    </CardLink>
                  </Container>
                ),
              ),
            )}
          </SlideShow>
        </SolutionsContainer>
      </Container>
    </MainCoverHome>
  );
};

export { SolutionsSection };
