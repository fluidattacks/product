import { BsArrowRightShort } from "@react-icons/all-files/bs/BsArrowRightShort";
import React from "react";

import { CardFooterHomeReviews, CardLink } from "./styles";

import { AirsLink } from "../../../components/AirsLink";
import { CloudImage } from "../../../components/CloudImage";
import { Container } from "../../../components/Container";
import { Text, Title } from "../../../components/Typography";
import { i18next, translate } from "../../../utils/translations/translate";

const Reviews: React.FC = (): JSX.Element => {
  const lng = i18next.language;

  return (
    <Container
      align={"center"}
      bgColor={"#ffffff"}
      display={"flex"}
      justify={"center"}
      minHeight={"500px"}
      wrap={"wrap"}
    >
      <Container maxWidth={"700px"} minWidth={"300px"} mt={5} ph={4} phMd={4}>
        <Title color={"#2e2e38"} level={1} mb={1} size={"medium"}>
          {translate.t("home.reviews.title")}
        </Title>
        <Text color={"#535365"} mb={4} mt={4} size={"big"}>
          {translate.t("home.reviews.subtitle")}
        </Text>
        <Container align={"center"} display={"flex"} gap={"8px"}>
          <Text
            color={"#2e2e38"}
            display={"inline"}
            size={"big"}
            weight={"bold"}
          >
            {translate.t("home.reviews.public")}
          </Text>
          <AirsLink
            href={
              "https://www.gartner.com/peer-insights/vendor-portal/reviews/published-reviews"
            }
          >
            <Container
              borderColor={"#eaecf0"}
              br={3}
              height={"36px"}
              hovercolor={"#eaecf0"}
              width={"91px"}
            >
              <CloudImage
                alt={"clutch review"}
                src={"airs/home/SuccessReviews/gartner-reviews"}
                styles={"pv2 ph2 w3"}
              />
            </Container>
          </AirsLink>
          <AirsLink href={"https://clutch.co/profile/fluid-attacks"}>
            <Container
              borderColor={"#eaecf0"}
              br={3}
              height={"36px"}
              hovercolor={"#eaecf0"}
              width={"91px"}
            >
              <CloudImage
                alt={"clutch review"}
                src={"airs/home/SuccessReviews/clutch-reviews"}
                styles={"pv2 ph2 w3"}
              />
            </Container>
          </AirsLink>
        </Container>
      </Container>
      <Container
        align={"center"}
        display={"flex"}
        justify={"center"}
        maxWidth={"750px"}
        wrap={"wrap"}
      >
        <Container
          align={"start"}
          bgGradient={"#ffffff, #f4f4f6"}
          borderColor={"#dddde3"}
          br={2}
          direction={"column"}
          display={"flex"}
          height={"357px"}
          hoverShadow={true}
          hovercolor={"#ffffff"}
          mh={3}
          mv={3}
          width={"338px"}
        >
          <CardLink>
            <Title color={"#bf0b1a"} level={4} size={"xxs"}>
              {translate.t("home.reviews.successStory.title")}
            </Title>
            <CloudImage
              alt={"success story 1"}
              src={"airs/home/SuccessReviews/logo-payvalida"}
              styles={"mv3 w-50"}
            />
            <Text color={"#535365"} mb={2} size={"medium"}>
              {translate.t("home.reviews.successStory.description1")}
            </Text>
            <CardFooterHomeReviews id={"link"}>
              <Container
                align={"center"}
                display={"flex"}
                justify={"end"}
                pv={4}
                wrap={"wrap"}
              >
                <AirsLink
                  decoration={"underline"}
                  href={
                    lng === "en"
                      ? "https://fluidattacks.docsend.com/view/gfgsj2xdjfh6kwkt"
                      : "https://fluidattacks.docsend.com/view/furtbbxfz8pe4r9e"
                  }
                >
                  <Text color={"#2e2e38"} mr={1} size={"small"} weight={"bold"}>
                    {translate.t("home.reviews.link")}
                  </Text>
                </AirsLink>
                <BsArrowRightShort size={20} />
              </Container>
            </CardFooterHomeReviews>
          </CardLink>
        </Container>
        <Container
          align={"start"}
          bgGradient={"#ffffff, #f4f4f6"}
          borderColor={"#dddde3"}
          br={2}
          direction={"column"}
          display={"flex"}
          height={"357px"}
          hoverShadow={true}
          hovercolor={"#ffffff"}
          width={"338px"}
        >
          <CardLink>
            <Title color={"#bf0b1a"} level={4} size={"xxs"}>
              {translate.t("home.reviews.successStory.title")}
            </Title>
            <CloudImage
              alt={"success story 2"}
              src={"airs/home/SuccessReviews/logo_proteccion"}
              styles={"mv3 w-50"}
            />
            <Text color={"#535365"} size={"medium"}>
              {translate.t("home.reviews.successStory.description2")}
            </Text>
            <CardFooterHomeReviews id={"link"}>
              <Container
                align={"center"}
                display={"flex"}
                justify={"end"}
                pv={4}
                wrap={"wrap"}
              >
                <AirsLink
                  decoration={"underline"}
                  href={
                    lng === "en"
                      ? "https://fluidattacks.docsend.com/view/v3aj7p3sixmh6ict"
                      : "https://fluidattacks.docsend.com/view/3nqybjhs59s8pppe"
                  }
                >
                  <Text color={"#2e2e38"} mr={1} size={"small"} weight={"bold"}>
                    {translate.t("home.reviews.link")}
                  </Text>
                </AirsLink>
                <BsArrowRightShort size={20} />
              </Container>
            </CardFooterHomeReviews>
          </CardLink>
        </Container>
      </Container>
    </Container>
  );
};

export { Reviews };
