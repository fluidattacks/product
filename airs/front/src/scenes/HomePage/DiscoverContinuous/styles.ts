import { styled } from "styled-components";

const HomeVideoContainer = styled.div<{
  $isVisible: boolean;
  $button: boolean;
}>`
  display: ${({ $isVisible }): string => ($isVisible ? "flex" : "none")};
  justify-content: center;
  align-items: center;
  width: 845px;
  height: 475px;
  @media screen and (max-width: 56em) {
    max-width: 347px;
    max-height: 195px;
  }
  article {
    width: 845px;
    height: 475px;
    display: flex;
    align-items: center;
    justify-content: center;
    background-position: 50%;
    background-size: cover;
    > button {
      display: ${({ $button }): string => ($button ? "block" : "none")};
      width: 95px;
      height: 95px;
      border-width: 0px;
      background-image: url("https://res.cloudinary.com/fluid-attacks/image/upload/v1682098617/airs/home/red-play.png");
      background-color: transparent;
      z-index: 1;
      border-radius: 100%;
      transition: all 0.2s cubic-bezier(0, 0, 0.2, 1);
    }
    > button:hover {
      background-image: url("https://res.cloudinary.com/fluid-attacks/image/upload/v1682098617/airs/home/black-play.png");
    }
    @media screen and (max-width: 56em) {
      max-width: 347px;
      max-height: 195px;
    }
  }
  iframe {
    max-width: 845px;
    width: 845px;
    height: 475px;
    max-height: 475px;
    @media screen and (max-width: 56em) {
      max-width: 347px;
      max-height: 195px;
    }
  }
`;

const HomeImageContainer = styled.div<{ $isVisible: boolean }>`
  display: ${({ $isVisible }): string => ($isVisible ? "none" : "flex")};
  margin-left: 2%;
  margin-right: 2%;
  max-width: 845px;
  max-height: 475px;
`;

const PlayButtonContainer = styled.div.attrs({
  className: `
    fl
    flex
    relative
    pointer
  `,
})`
  width: 70px;
  height: 70px;
  top: 40%;
  left: 45%;
  z-index: 1;

  @media screen and (max-width: 480px) {
    top: 35%;
    left: 40%;
  }
`;

const PlayImageContainer = styled.div.attrs({
  className: `
    dib
    center
    relative
  `,
})`
  > div + img {
    margin-top: -64px;
  }
`;

export {
  HomeVideoContainer,
  HomeImageContainer,
  PlayButtonContainer,
  PlayImageContainer,
};
