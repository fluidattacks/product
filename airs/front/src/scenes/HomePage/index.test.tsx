import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import React from "react";

import { HomePage } from ".";

const data = [
  {
    description: "solutions.homeCards.devSecOps.paragraph",
    title: "solutions.homeCards.devSecOps.title",
  },
  {
    description: "solutions.homeCards.secureCode.paragraph",
    title: "solutions.homeCards.secureCode.title",
  },
  {
    description: "solutions.homeCards.redTeaming.paragraph",
    title: "solutions.homeCards.redTeaming.title",
  },
  {
    description: "solutions.homeCards.securityTesting.paragraph",
    title: "solutions.homeCards.securityTesting.title",
  },
  {
    description: "solutions.homeCards.ethicalHacking.paragraph",
    title: "solutions.homeCards.ethicalHacking.title",
  },
  {
    description: "solutions.homeCards.vulnerabilityManagement.paragraph",
    title: "solutions.homeCards.vulnerabilityManagement.title",
  },
  {
    description: "solutions.homeCards.appSecurityPostureManagement.paragraph",
    title: "solutions.homeCards.appSecurityPostureManagement.title",
  },
];

describe("HomePage", (): void => {
  it("HomePage should render mocked data", (): void => {
    expect.hasAssertions();

    render(<HomePage />);

    expect(screen.queryByText("home.hero.title")).toBeInTheDocument();
    expect(screen.queryByText("home.hero.paragraph")).toBeInTheDocument();

    expect(screen.getByText("home.hero.button1")).toBeInTheDocument();
    expect(screen.getByText("home.hero.button2")).toBeInTheDocument();

    expect(screen.queryByText("home.clients.title")).toBeInTheDocument();

    expect(
      screen.queryByText("home.discoverContinuous.subtitle"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("home.discoverContinuous.title"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("home.discoverContinuous.card1.title"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("home.discoverContinuous.card1.subtitle"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("home.discoverContinuous.card2.title"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("home.discoverContinuous.card2.subtitle"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("home.discoverContinuous.card3.title"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("home.discoverContinuous.card3.subtitle"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("home.discoverContinuous.button"),
    ).toBeInTheDocument();

    expect(
      screen.queryByText("home.continuousCycle.subtitle"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("home.continuousCycle.title"),
    ).toBeInTheDocument();

    expect(screen.queryByText("home.solutions.title")).toBeInTheDocument();
    expect(screen.queryByText("home.solutions.subtitle")).toBeInTheDocument();
    expect(screen.queryByText("home.solutions.button")).toBeInTheDocument();
    expect(screen.queryAllByText("home.solutions.cardTitle")).toHaveLength(
      data.length * 2,
    );
    expect(screen.queryAllByText("home.solutions.cardLink")).toHaveLength(
      data.length * 2,
    );
    data.forEach((item): void => {
      expect(screen.queryAllByText(item.description)).toHaveLength(2);
      expect(screen.queryAllByText(item.title)).toHaveLength(2);
    });

    expect(screen.queryByText("home.reviews.title")).toBeInTheDocument();
    expect(screen.queryByText("home.reviews.subtitle")).toBeInTheDocument();
    expect(screen.queryByText("home.reviews.public")).toBeInTheDocument();
    expect(
      screen.queryAllByText("home.reviews.successStory.title"),
    ).toHaveLength(2);
    expect(
      screen.queryByText("home.reviews.successStory.description1"),
    ).toBeInTheDocument();
    expect(screen.queryAllByText("home.reviews.link")).toHaveLength(2);
    expect(
      screen.queryByText("home.reviews.successStory.description2"),
    ).toBeInTheDocument();

    expect(screen.getByText("blog.ctaButton1")).toBeInTheDocument();
    expect(screen.getByText("blog.ctaButton2")).toBeInTheDocument();
    expect(screen.queryByText("blog.ctaDescription")).toBeInTheDocument();
    expect(screen.queryByText("blog.ctaTitle")).toBeInTheDocument();
  });
});
