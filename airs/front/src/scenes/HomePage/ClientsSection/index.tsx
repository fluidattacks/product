import React from "react";

import { ClientsContainer, Container, SlideShow } from "./styles";

import { CloudImage } from "../../../components/CloudImage";
import { Container as Div } from "../../../components/Container";
import { Title } from "../../../components/Typography";
import { translate } from "../../../utils/translations/translate";

const ClientsSection = (): JSX.Element => {
  const clients = [
    "bancolombia",
    "itau",
    "avianca",
    "oxxo",
    "copa-airlines-updated",
    "consubanco",
    "banco-inter-chile",
    "interbank",
    "banbif",
    "banistmo",
    "gesa-updated",
    "bantrab",
    "lulo-bank",
    "global-bank-updated",
    "banco-general",
  ];

  return (
    <Container $bgColor={"#25252d"}>
      <Title
        color={"#f4f4f6"}
        level={3}
        mb={4}
        size={"small"}
        textAlign={"center"}
      >
        {translate.t("home.clients.title")}
      </Title>
      <ClientsContainer $gradientColor={"#25252d"}>
        <SlideShow>
          {[0, 1].map((re): JSX.Element[] =>
            clients.map(
              (client): JSX.Element => (
                <Div key={`${client}-${re}`} ph={3}>
                  <CloudImage
                    alt={`${client}-${re}`}
                    key={`${client}-${re}`}
                    src={`airs/home/ClientSection/updateLogos/${client}-logo`}
                  />
                </Div>
              ),
            ),
          )}
        </SlideShow>
      </ClientsContainer>
    </Container>
  );
};

export { ClientsSection };
