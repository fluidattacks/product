import React from "react";

import { Header } from "./Header";
import { PostsList } from "./PostsList";

const LearnPage: React.FC = (): JSX.Element => {
  return (
    <React.Fragment>
      <Header />
      <PostsList />
    </React.Fragment>
  );
};

export { LearnPage };
