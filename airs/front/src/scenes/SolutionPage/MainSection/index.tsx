import React from "react";

import { Header2 } from "./components/Header2";
import { Paragraph } from "./components/Paragraph";
import { SolutionCard } from "./components/SolutionCard";
import { SolutionCtaBanner } from "./components/SolutionCtaBanner";
import { SolutionLink } from "./components/SolutionLink";
import { SolutionSlideShow } from "./components/SolutionSlideShow";
import { TextContainer } from "./components/TextContainer";

import { Container } from "../../../components/Container";
import { FaqContainer } from "../../../components/GridContainer/FaqContainer";
import { GridContainer } from "../../../components/GridContainer/GridContainer";
import { SolutionFaq } from "../../../components/SolutionFaq/SolutionFaq";
import { useHtml } from "../../../utils/hooks/useHtml";

interface IMainProps {
  html: string;
}

const components = {
  a: SolutionLink,
  "faq-container": FaqContainer,
  "grid-container": GridContainer,
  h2: Header2,
  p: Paragraph,
  "solution-card": SolutionCard,
  "solution-cta": SolutionCtaBanner,
  "solution-faq": SolutionFaq,
  "solution-slide": SolutionSlideShow,
  "text-container": TextContainer,
};

const MainSection: React.FC<IMainProps> = ({ html }): JSX.Element => {
  const content = useHtml(components, html);

  return <Container bgColor={"#fff"}>{content}</Container>;
};

export { MainSection };
