import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import { useStaticQuery } from "gatsby";
import React from "react";

import { Header2 } from "./MainSection/components/Header2";
import { SolutionCard } from "./MainSection/components/SolutionCard";
import { SolutionCtaBanner } from "./MainSection/components/SolutionCtaBanner";
import { SolutionLink } from "./MainSection/components/SolutionLink";
import { SolutionSlideShow } from "./MainSection/components/SolutionSlideShow";
import { TextContainer } from "./MainSection/components/TextContainer";

import { SolutionPage } from ".";
import { FaqContainer } from "../../components/GridContainer/FaqContainer";
import { GridContainer } from "../../components/GridContainer/GridContainer";
import { SolutionFaq } from "../../components/SolutionFaq/SolutionFaq";
import { allCloudinaryMediaData } from "test-utils/mocks";
import type { ICloudinaryMedia } from "utils/types";

interface IDataSolutionPageMock {
  allCloudinaryMedia: ICloudinaryMedia;
  allMarkdownRemark: {
    edges: [
      {
        node: {
          fields: {
            slug: string;
          };
          frontmatter: {
            alt: string;
            description: string;
            image: string;
            slug: string;
            subtitle: string;
            tags: string;
            title: string;
          };
        };
      },
      {
        node: {
          fields: {
            slug: string;
          };
          frontmatter: {
            alt: string;
            description: string;
            image: string;
            slug: string;
            subtitle: string;
            tags: string;
            title: string;
          };
        };
      },
    ];
  };
}

const mockUseStaticQuery = jest.spyOn({ useStaticQuery }, `useStaticQuery`);
const mockDataUseStaticQuery: IDataSolutionPageMock = {
  allCloudinaryMedia: allCloudinaryMediaData,
  allMarkdownRemark: {
    edges: [
      {
        node: {
          fields: {
            slug: "/es/blog/",
          },
          frontmatter: {
            alt: "alt",
            description: "description",
            image: "/test.jpg",
            slug: "Slug content",
            subtitle: "Subtítulo",
            tags: "éxitos,tag2,tag3",
            title: "Título",
          },
        },
      },
      {
        node: {
          fields: {
            slug: "/es/blog2/",
          },
          frontmatter: {
            alt: "alt2",
            description: "description2",
            image: "/test2.jpg",
            slug: "Slug content2",
            subtitle: "Subtítulo2",
            tags: "éxitos2,tag3,tag4",
            title: "Título2",
          },
        },
      },
    ],
  },
};

describe("SolutionPage", (): void => {
  beforeEach((): void => {
    mockUseStaticQuery.mockImplementation(
      (): IDataSolutionPageMock => mockDataUseStaticQuery,
    );
  });

  afterEach((): void => {
    jest.restoreAllMocks();
  });
  it("renders SolutionPage", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <SolutionPage
        description={"description"}
        html={"<p>Test HTML</p>"}
        image={"/test.jpg"}
        title={"Título"}
      />,
    );

    await waitFor((): void => {
      expect(screen.getByText("description")).toBeInTheDocument();
      expect(screen.getByText("Título")).toBeInTheDocument();
      expect(screen.getByText("Test HTML")).toBeInTheDocument();
    });
  });

  it("renders image prop", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <SolutionPage
        description={"description"}
        html={"<p>Test HTML</p"}
        image={"/test.jpg"}
        title={"Título"}
      />,
    );
    await waitFor((): void => {
      expect(screen.getByRole("img")).toHaveAttribute("src", "/test.jpg");
    });
    expect(
      screen.getByRole("button", { name: "blog.ctaButton1" }),
    ).toBeInTheDocument();
  });

  describe("SolutionLink", (): void => {
    it("renders children", (): void => {
      expect.hasAssertions();

      const { getByText } = render(
        <SolutionLink href={"/test"}>{"Test Child"}</SolutionLink>,
      );

      expect(getByText("Test Child")).toBeInTheDocument();
    });

    it("renders with default props", (): void => {
      expect.hasAssertions();

      const { getByRole } = render(
        <SolutionLink href={"/test"}>{"Link"}</SolutionLink>,
      );

      expect(getByRole("link")).toBeInTheDocument();
    });
  });

  it("renders SolutionSlideShow", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <SolutionSlideShow
        description={"description"}
        solution={"devsecops"}
        title={"Título"}
      />,
    );
    expect(screen.getByText("Título")).toBeInTheDocument();
    expect(screen.getByText("description")).toBeInTheDocument();
    expect(container.textContent).toContain("Títulodescription");
    expect(container.textContent).toContain("Título");
  });

  describe("FaqContainer", (): void => {
    it("should render children", (): void => {
      render(
        <FaqContainer>
          <div>{"Test children"}</div>
        </FaqContainer>,
      );
      expect(screen.getByText("Test children")).toBeInTheDocument();
    });
  });

  describe("GridContainer", (): void => {
    it("should render GridContainer", (): void => {
      render(
        <GridContainer>
          <div>{"Test children"}</div>
        </GridContainer>,
      );
      expect(screen.getByText("Test children")).toBeInTheDocument();
    });
  });

  describe("Header2", (): void => {
    it("should render Header2", (): void => {
      render(
        <Header2 color={""} level={2}>
          <div>{"Test children"}</div>
        </Header2>,
      );
      expect(screen.getByText("Test children")).toBeInTheDocument();
    });
  });

  describe("SolutionFaq", (): void => {
    it("renders title prop", (): void => {
      expect.hasAssertions();

      render(<SolutionFaq title={"title"} />);

      expect(screen.getByText("title")).toBeInTheDocument();
    });

    it("should toggle FAQ content visibility on click", (): void => {
      expect.hasAssertions();

      const { getByText } = render(
        <SolutionFaq title={"title"}>{"Test title"}</SolutionFaq>,
      );

      expect(getByText("title")).toBeInTheDocument();
      expect(getByText("Test title")).toHaveStyle({ display: "none" });

      fireEvent.click(getByText("title"));
      expect(getByText("Test title")).toHaveStyle({ display: "block" });

      fireEvent.click(getByText("title"));
      expect(getByText("Test title")).toHaveStyle({ display: "none" });
    });
  });

  describe("SolutionCtaBanner", (): void => {
    it("renders CtaBanner component with provided props", (): void => {
      expect.hasAssertions();

      const { getByText, getByRole } = render(
        <SolutionCtaBanner
          button1Link={"https://fluidattacks.com"}
          button1Text={"Button 1"}
          paragraph={"Test Paragraph"}
          title={"Test Title"}
        />,
      );
      expect(getByText("Test Title")).toBeInTheDocument();
      expect(getByText("Test Paragraph")).toBeInTheDocument();

      const linkButton = getByRole("link", { name: "blog.ctaButton1" });
      expect(linkButton).toHaveAttribute(
        "href",
        "https://app.fluidattacks.com/SignUp",
      );

      expect(
        getByRole("button", { name: "blog.ctaButton2" }),
      ).toBeInTheDocument();
    });
  });

  describe("SolutionCard", (): void => {
    it("renders SolutionCard", (): void => {
      expect.hasAssertions();

      const { getByText } = render(
        <SolutionCard
          description={"Test Description"}
          descriptionColor={"#000"}
          image={"/test.jpg"}
          title={"Test Title"}
        />,
      );
      expect(getByText("Test Title")).toBeInTheDocument();
      expect(getByText("Test Description")).toBeInTheDocument();
    });
  });
  describe("TextContainer", (): void => {
    it("should render children", (): void => {
      render(<TextContainer>{"Test Content"}</TextContainer>);

      expect(screen.getByText("Test Content")).toBeInTheDocument();
    });
  });
});
