import { render, screen } from "@testing-library/react";
import React from "react";

import { Layout } from ".";

describe("Layout", (): void => {
  jest.mock("../../../utils/hooks/useWindowSize", (): number => 479);

  afterAll((): void => {
    jest.clearAllMocks();
  });
  it("Layout should render mocked data", (): void => {
    expect.hasAssertions();

    render(
      <Layout>
        <div>
          <p>{"Layout"}</p>
        </div>
      </Layout>,
    );

    expect(screen.getByText("Layout")).toBeInTheDocument();
    // Some assertins to verify that the footer is rendered
    expect(screen.getByText("footer.products.title")).toBeInTheDocument();
    expect(screen.getByText("footer.links.privacy")).toBeInTheDocument();
    expect(
      screen.getByText("menu.platform.aSinglePane.platformOverview.title"),
    ).toBeInTheDocument();
  });
});
