import { render } from "@testing-library/react";
import { useStaticQuery } from "gatsby";
import React from "react";

import { WebsiteWrapper } from ".";
import { allCloudinaryMediaData } from "test-utils/mocks";
import type { ICloudinaryMedia } from "utils/types";

interface IDataImageMock {
  allCloudinaryMedia: ICloudinaryMedia;
}

const mockUseStaticQuery = jest.spyOn({ useStaticQuery }, `useStaticQuery`);
const mockDataUseStaticQuery: IDataImageMock = {
  allCloudinaryMedia: allCloudinaryMediaData,
};

describe("WebsiteWrapper", (): void => {
  beforeEach((): void => {
    mockUseStaticQuery.mockImplementation(
      (): IDataImageMock => mockDataUseStaticQuery,
    );
  });

  afterEach((): void => {
    jest.restoreAllMocks();
  });
  it("renders children", (): void => {
    const { getByText } = render(
      <WebsiteWrapper>
        <div>{"Test Children"}</div>
      </WebsiteWrapper>,
    );

    expect(getByText("Test Children")).toBeInTheDocument();
  });
});
