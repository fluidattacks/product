import { FiChevronDown } from "@react-icons/all-files/fi/FiChevronDown";
import { FiGlobe } from "@react-icons/all-files/fi/FiGlobe";
import { navigate } from "gatsby";
import i18next from "i18next";
import React, { useCallback, useState } from "react";

import { LanguagesButton, LanguagesContainer, Switcher } from "./styles";

import { Container } from "../../../components/Container";
import { useWindowLocation } from "../../../utils/hooks/useWindowLocation";
import { translatedPages } from "../../../utils/translations/spanishPages";

export const LanguageSwitcher: React.FC = (): JSX.Element => {
  const [displayLanguages, setDisplayLanguages] = useState(false);
  const handleClickButton = useCallback((): void => {
    setDisplayLanguages((previous): boolean => !previous);
  }, []);
  const location = useWindowLocation();
  const newLocation = `/${location}`;
  const spanishHref = translatedPages.find(
    (page): boolean => page.en === newLocation,
  );
  const englishHref = translatedPages.find(
    (page): boolean => page.es === newLocation,
  );

  const navigateToPage = async (pagePath: string): Promise<void> => {
    await navigate(pagePath, { replace: true });
  };

  const locationEs = spanishHref?.es ?? "";
  const locationEn = englishHref?.en ?? "";
  const languageHandler = useCallback(
    (language: string): VoidFunction =>
      (): void => {
        if (language === "es" && locationEs !== "") {
          void navigateToPage(locationEs);
        } else if (language === "en" && locationEn !== "") {
          void navigateToPage(locationEn);
        }
      },
    [locationEn, locationEs],
  );

  return (
    <Container
      align={"center"}
      display={"flex"}
      justify={"center"}
      maxWidth={"110px"}
      position={"relative"}
      wrap={"wrap"}
    >
      <Switcher onClick={handleClickButton}>
        <FiGlobe size={15} />
        {i18next.language === "en" ? "English" : "Español"}
        <FiChevronDown />
      </Switcher>
      <LanguagesContainer $isShown={displayLanguages}>
        <LanguagesButton
          disabled={i18next.language === "en"}
          onClick={languageHandler("en")}
        >
          {"English"}
        </LanguagesButton>
        <LanguagesButton
          disabled={i18next.language === "es"}
          onClick={languageHandler("es")}
        >
          {"Español"}
        </LanguagesButton>
      </LanguagesContainer>
    </Container>
  );
};
