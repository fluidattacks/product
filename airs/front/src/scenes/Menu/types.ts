type TScreens = "desktop" | "medium" | "mobile";

export type { TScreens };
