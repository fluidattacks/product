import { css, styled } from "styled-components";
import type { RuleSet } from "styled-components";

import type { TScreens } from "./types";

const MenuMobileInnerContainer = styled.div.attrs({
  className: `
  left-0
  top-0
  z-max
  `,
})`
  background-color: #ffffff;
  height: 100%;
`;

const MenuFootContainer = styled.div.attrs({
  className: `
  w-100
  left-0
  bottom-0
  fixed
  fadein
  `,
})<{ $isShown: boolean }>`
  display: ${({ $isShown }): string => ($isShown ? "flex" : "none")};
  background-color: #f4f4f6;
  align-content: center;
  justify-content: center;
  height: 130px;
`;

const ContainerWithSlide = styled.div.attrs({
  className: `
  w-auto
  overflow-y-auto
  `,
})<{ $isShown: boolean; $mobile: boolean }>`
  @keyframes fadeInRight {
    from {
      opacity: 0;
      transform: translateX(100%);
    }
    to {
      opacity: 1;
    }
  }

  animation: fadeInRight 0.4s ease-in-out;
  box-shadow: ${({ $mobile }): string =>
    $mobile ? "" : "0px 10px 7px -2px rgba(0, 0, 0, 0.16)"};
  display: ${({ $isShown }): string => ($isShown ? "block" : "none")};
  margin-bottom: ${({ $mobile }): string =>
    $mobile ? "140px" : "max-content"};

  .content {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }
`;

const SlideMenu = styled.div<{
  $isShown: boolean;
  $status: number;
  $mobile: boolean;
}>`
  @keyframes fadeInLeft {
    from {
      opacity: 0;
      transform: translateX(-100%);
    }
    to {
      opacity: 1;
      transform: translateX(0%);
    }
  }

  animation: ${({ $status }): string =>
    $status > 1 ? "fadeInLeft 0.4s ease-in-out" : ""};
  box-shadow: ${({ $mobile }): string =>
    $mobile ? "" : "0px 10px 7px -2px rgba(0, 0, 0, 0.16)"};
  display: ${({ $isShown }): string => ($isShown ? "block" : "none")};
  height: 280px;
`;

const SearchButton = styled.div`
  width: max-content;
  height: 37px;
  background-color: transparent;
  border-radius: 4px;
  font-size: 14px;
  box-sizing: border-box;
  margin-left: 8px;
  margin-right: 8px;
  padding: 8px 9px 8px 8px;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  z-index: 999999999;

  &:hover {
    background-color: #dddde3;
  }
`;

const ContactButton = styled.div`
  width: max-content;
  height: 37px;
  background-color: #f4f4f6;
  border-radius: 4px;
  border: 1px solid #f4f4f6;
  font-size: 14px;
  color: #2e2e38;
  box-sizing: border-box;
  padding-left: 8px;
  padding-right: 8px;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  z-index: 999999999;

  &:hover {
    background-color: #dddde3;
  }
`;

const breakPoints: Record<TScreens, Record<"max" | "min", string>> = {
  desktop: { max: "10000px", min: "1201px" },
  medium: { max: "1200px", min: "960px" },
  mobile: { max: "959px", min: "10px" },
};

const NavbarContainer = styled.div.attrs({
  className: `
  cssmenu
  lh-solid
  cover
  w-100
  top-0
  z-max
  t-all-5
  `,
})<{ $screen: TScreens }>`
  box-shadow: 0 10px 20px 0 rgba(0, 0, 0, 0.16);
  background-color: #ffffff;
  display: none;
  position: -webkit-sticky;
  position: sticky;

  ${({ $screen }): RuleSet<object> =>
    $screen === "desktop"
      ? css`
          @media screen and (min-width: 1201px) {
            display: block;
          }
        `
      : css`
          @media screen and (min-width: ${breakPoints[$screen]
              .min}) and (max-width: ${breakPoints[$screen].max}) {
            display: block;
          }
        `};
`;

export {
  ContactButton,
  ContainerWithSlide,
  MenuFootContainer,
  MenuMobileInnerContainer,
  NavbarContainer,
  SearchButton,
  SlideMenu,
};
