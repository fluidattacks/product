/* eslint react/forbid-component-props: 0 */
import { FiMenu } from "@react-icons/all-files/fi/FiMenu";
import { IoIosSearch } from "@react-icons/all-files/io/IoIosSearch";
import { Link } from "gatsby";
import React, { createRef, useCallback, useEffect, useState } from "react";
import type { RefObject } from "react";

import { LanguageSwitcher } from "./LanguageSwitcher";
import { DropdownMenu } from "./MobileMenu/DropdownMenu";
import { ContactButton, NavbarContainer, SearchButton } from "./styles";
import type { TScreens } from "./types";
import { contents, customButtonSize, initialChevronStates } from "./utils";

import { AirsLink } from "../../components/AirsLink";
import { Button } from "../../components/Button";
import { Chevron } from "../../components/Chevron";
import { CloudImage } from "../../components/CloudImage";
import { Container } from "../../components/Container";
import { NavbarInnerContainer, NavbarList } from "../../styles/styles";
import { useClickOutside } from "../../utils/hooks/useClickOutside";
import { useWindowSize } from "../../utils/hooks/useWindowSize";
import { i18next, translate } from "../../utils/translations/translate";

export const NavbarComponent: React.FC = (): JSX.Element => {
  const { width } = useWindowSize();
  const [menuStatus, setMenuStatus] = useState(0);
  const [categoryShown, setCategoryShown] = useState(0);
  const [isSecondaryMenuVisible, setIsSecondaryMenuVisible] = useState(true);
  const [menu, setMenu] = useState(false);
  const [chevrons, setChevrons] = useState(initialChevronStates);
  const menuRef: RefObject<HTMLDivElement> = createRef();

  const handleScreen = useCallback((): TScreens => {
    if (width < 1200 && width >= 960) {
      return "medium";
    } else if (width < 960 && width > 10) {
      return "mobile";
    }

    return "desktop";
  }, [width]);

  const screen = handleScreen();
  const customOptionsButtonSize = customButtonSize(16, 8, 8);

  const resetState = useCallback((): void => {
    setCategoryShown(0);
  }, []);

  const listContents = contents(resetState);

  const alternateChevron = useCallback(
    (category: string): void => {
      const alternativeChevron = chevrons[category] === "up" ? "down" : "up";
      setChevrons({ ...initialChevronStates, [category]: alternativeChevron });
    },
    [chevrons],
  );
  const resetChevrons = useCallback((): void => {
    setChevrons(initialChevronStates);
  }, []);

  const handleClick = useCallback((): void => {
    setMenuStatus((previousState): number => (previousState === 0 ? 1 : 0));
    setMenu((previousState): boolean => !previousState);
  }, []);

  const handleClickButton = useCallback(
    (category: string): VoidFunction =>
      (): void => {
        resetState();
        alternateChevron(category);
        if (category === "services" && categoryShown !== 1) {
          setCategoryShown(1);
        } else if (category === "resources" && categoryShown !== 3) {
          setCategoryShown(3);
        } else if (category === "platform" && categoryShown !== 2) {
          setCategoryShown(2);
        } else if (category === "company" && categoryShown !== 4) {
          setCategoryShown(4);
        } else if (category === "search" && categoryShown !== 5) {
          setCategoryShown(5);
        }
      },
    [resetState, alternateChevron, categoryShown],
  );

  useClickOutside(menuRef, (): void => {
    resetState();
    resetChevrons();
    if (menu && screen === "desktop") {
      setMenu(false);
    }
  });

  const listenToScroll = useCallback((): void => {
    const heightToShowMenu = 0;
    const heightToHideMenu = 30;
    if (typeof document !== "undefined") {
      const winScroll =
        document.body.scrollTop || document.documentElement.scrollTop;

      if (winScroll === heightToShowMenu) {
        setIsSecondaryMenuVisible(true);
      } else if (winScroll >= heightToHideMenu) {
        setIsSecondaryMenuVisible(false);
      }
    }
  }, []);

  useEffect((): VoidFunction => {
    window.addEventListener("scroll", listenToScroll, { passive: true });

    return (): void => {
      window.removeEventListener("scroll", listenToScroll);
    };
  }, [listenToScroll, categoryShown]);

  if (screen === "mobile") {
    return (
      <NavbarContainer $screen={"mobile"} id={"navbarMobile"}>
        <NavbarInnerContainer id={"inner_navbar"}>
          <NavbarList className={"roboto"} id={"navbar_list"}>
            <div className={"w-auto flex flex-nowrap"}>
              <li>
                <AirsLink href={"/"}>
                  <Container display={"block"} ph={3} pv={2} width={"150px"}>
                    <CloudImage
                      alt={"Fluid Attacks logo navbar"}
                      src={"airs/menu/Logo"}
                    />
                  </Container>
                </AirsLink>
              </li>
            </div>
            <Container
              align={"center"}
              display={"flex"}
              justify={"center"}
              justifyMd={"end"}
              justifySm={"end"}
              maxWidth={"90%"}
            >
              <LanguageSwitcher />
              <Button onClick={handleClick} variant={"ghost"}>
                <FiMenu size={width > 960 ? 20 : 25} />
              </Button>
            </Container>
          </NavbarList>
        </NavbarInnerContainer>
        {listContents[categoryShown]}
        <DropdownMenu
          display={menu ? "block" : "none"}
          handleClick={handleClick}
          setStatus={setMenuStatus}
          status={menuStatus}
        />
      </NavbarContainer>
    );
  } else if (screen === "medium") {
    return (
      <NavbarContainer $screen={"medium"} id={"navbar"}>
        {isSecondaryMenuVisible ? (
          <Container
            align={"center"}
            bgColor={"#f4f4f6"}
            display={"flex"}
            height={"53px"}
            justify={"center"}
          >
            <Container display={"flex"} justify={"end"} maxWidth={"1120px"}>
              <ContactButton>
                <AirsLink decoration={"none"} href={"/contact-us/"}>
                  {translate.t("menu.buttons.contact")}
                </AirsLink>
              </ContactButton>
              <SearchButton>
                <Container
                  align={"center"}
                  display={"flex"}
                  onClick={handleClickButton("search")}
                >
                  <IoIosSearch size={17} />
                  {translate.t("menu.buttons.search")}
                </Container>
              </SearchButton>
              <LanguageSwitcher />
            </Container>
          </Container>
        ) : undefined}
        <NavbarInnerContainer id={"inner_navbar"}>
          <NavbarList className={"roboto"} id={"navbar_list"}>
            <div className={"w-auto flex flex-nowrap"}>
              <li>
                <Link className={"db tc no-underline"} to={"/"}>
                  <Container display={"block"} ph={3} pv={2} width={"150px"}>
                    <CloudImage
                      alt={"Fluid Attacks logo navbar"}
                      src={"airs/menu/Logo"}
                    />
                  </Container>
                </Link>
              </li>
            </div>
            <Container
              display={"flex"}
              justify={"end"}
              minWidth={"250px"}
              width={"80%"}
              wrap={"nowrap"}
            >
              <AirsLink href={"https://app.fluidattacks.com/"}>
                <Button variant={"tertiary"}>
                  {translate.t("menu.buttons.login")}
                </Button>
              </AirsLink>
              <Container maxWidth={"150px"} ml={3} mr={2}>
                <AirsLink href={"https://app.fluidattacks.com/SignUp"}>
                  <Button variant={"primary"}>
                    {translate.t("menu.buttons.trial")}
                  </Button>
                </AirsLink>
              </Container>
            </Container>
            <Container
              display={"flex"}
              justify={"center"}
              justifyMd={"end"}
              justifySm={"end"}
              maxWidth={"50px"}
            >
              <Button onClick={handleClick} variant={"ghost"}>
                <FiMenu size={width > 960 ? 20 : 25} />
              </Button>
            </Container>
          </NavbarList>
        </NavbarInnerContainer>
        {listContents[categoryShown]}
        <DropdownMenu
          display={menu ? "block" : "none"}
          handleClick={handleClick}
          setStatus={setMenuStatus}
          status={menuStatus}
        />
      </NavbarContainer>
    );
  }

  return (
    <NavbarContainer $screen={"desktop"} id={"navbar"} ref={menuRef}>
      {isSecondaryMenuVisible ? (
        <Container
          align={"center"}
          bgColor={"#f4f4f6"}
          display={"flex"}
          height={"53px"}
          justify={"center"}
        >
          <Container display={"flex"} justify={"end"} maxWidth={"1400px"}>
            <ContactButton>
              <AirsLink decoration={"none"} href={"/contact-us/"}>
                {translate.t("menu.buttons.contact")}
              </AirsLink>
            </ContactButton>
            <SearchButton>
              <Container
                align={"center"}
                display={"flex"}
                onClick={handleClickButton("search")}
              >
                <IoIosSearch size={17} />
                {translate.t("menu.buttons.search")}
              </Container>
            </SearchButton>
            <LanguageSwitcher />
          </Container>
        </Container>
      ) : undefined}
      <NavbarInnerContainer id={"inner_navbar"}>
        <NavbarList className={"roboto"} id={"navbar_list"}>
          <div
            className={"flex flex-nowrap"}
            style={{ height: "81px", width: "158px" }}
          >
            <li>
              <Link
                className={"db tc pa1 no-underline"}
                to={i18next.language === "es" ? "/es/" : "/"}
              >
                <Container display={"block"} ph={3} pv={2} width={"150px"}>
                  <CloudImage
                    alt={"Fluid Attacks logo navbar"}
                    height={54}
                    src={"airs/menu/Logo"}
                    width={118}
                  />
                </Container>
              </Link>
            </li>
          </div>
          <Container center={true} display={"flex"} width={"auto"}>
            <Button
              customSize={customOptionsButtonSize}
              icon={<Chevron direction={chevrons.services} />}
              iconSide={"right"}
              onClick={handleClickButton("services")}
              variant={"ghost"}
            >
              {translate.t("menu.buttons.service")}
            </Button>
            <Button
              customSize={customOptionsButtonSize}
              icon={<Chevron direction={chevrons.platform} />}
              iconSide={"right"}
              onClick={handleClickButton("platform")}
              variant={"ghost"}
            >
              {translate.t("menu.buttons.platform")}
            </Button>
            <AirsLink href={"/plans/"}>
              <Button customSize={customOptionsButtonSize} variant={"ghost"}>
                {translate.t("menu.buttons.plans")}
              </Button>
            </AirsLink>
            <Button
              customSize={customOptionsButtonSize}
              icon={<Chevron direction={chevrons.resources} />}
              iconSide={"right"}
              onClick={handleClickButton("resources")}
              variant={"ghost"}
            >
              {translate.t("menu.buttons.resources")}
            </Button>
            <AirsLink href={"/advisories/"}>
              <Button customSize={customOptionsButtonSize} variant={"ghost"}>
                {translate.t("menu.buttons.advisories")}
              </Button>
            </AirsLink>
            <Button
              customSize={customOptionsButtonSize}
              icon={<Chevron direction={chevrons.company} />}
              iconSide={"right"}
              onClick={handleClickButton("company")}
              variant={"ghost"}
            >
              {translate.t("menu.buttons.company")}
            </Button>
          </Container>
          <Container
            display={"flex"}
            justify={"end"}
            maxWidth={"330px"}
            wrap={"nowrap"}
          >
            <AirsLink href={"https://app.fluidattacks.com/"}>
              <Button variant={"tertiary"}>
                {translate.t("menu.buttons.login")}
              </Button>
            </AirsLink>
            <Container maxWidth={"150px"} ml={3} mr={2}>
              <AirsLink href={"https://app.fluidattacks.com/SignUp"}>
                <Button variant={"primary"}>
                  {translate.t("menu.buttons.trial")}
                </Button>
              </AirsLink>
            </Container>
          </Container>
        </NavbarList>
      </NavbarInnerContainer>
      {listContents[categoryShown]}
    </NavbarContainer>
  );
};
