import React from "react";

import { AirsLink } from "../../../../components/AirsLink";
import { Container } from "../../../../components/Container";
import type { TDisplay } from "../../../../components/Container/types";
import { Grid } from "../../../../components/Grid";
import { Text } from "../../../../components/Typography";
import { useWindowSize } from "../../../../utils/hooks/useWindowSize";
import { translate } from "../../../../utils/translations/translate";

interface IPlatformProps {
  display: TDisplay;
}

const PlatformMenu: React.FC<IPlatformProps> = ({
  display,
}: IPlatformProps): JSX.Element => {
  const { width } = useWindowSize();

  return (
    <Container
      bgColor={"#ffffff"}
      display={display}
      scroll={"y"}
      shadowBottom={width >= 1200}
    >
      <Container
        display={width >= 960 ? "flex" : "inline"}
        height={"max-content"}
        justify={"center"}
        pb={3}
      >
        <Container maxWidth={width >= 1200 ? "460px" : "1440px"} pb={4} ph={4}>
          <Container
            borderBottomColor={"#dddde3"}
            height={"36px"}
            mb={3}
            mt={3}
            pb={3}
          >
            <Text color={"#8f8fa3"} size={"xs"}>
              {translate.t("menu.platform.aSinglePane.title")}
            </Text>
          </Container>
          <Container>
            <AirsLink hovercolor={"#bf0b1a"} href={"/platform/"}>
              <Text color={"#2e2e38"} mb={3} size={"small"} weight={"bold"}>
                {translate.t(
                  "menu.platform.aSinglePane.platformOverview.title",
                )}
              </Text>
            </AirsLink>
            <Text color={"#535365"} mb={3} size={"xs"}>
              {translate.t(
                "menu.platform.aSinglePane.platformOverview.subtitle",
              )}
            </Text>
          </Container>
        </Container>
        <Container maxWidth={width >= 960 ? "460px" : "1440px"} ph={4}>
          <Container
            borderBottomColor={"#dddde3"}
            height={"36px"}
            mt={3}
            pb={3}
          >
            <Text color={"#8f8fa3"} size={"xs"}>
              {translate.t("menu.platform.products.title")}
            </Text>
          </Container>
          <Grid
            columns={width < 960 ? 1 : 3}
            gap={width >= 1200 ? "1rem" : "0rem"}
            ph={"0px"}
          >
            <Container widthSm={"300px"}>
              <AirsLink hovercolor={"#bf0b1a"} href={"/product/sast/"}>
                <Text color={"#2e2e38"} mb={3} size={"small"} weight={"bold"}>
                  {translate.t("menu.platform.products.links.sast")}
                </Text>
              </AirsLink>
              <AirsLink hovercolor={"#bf0b1a"} href={"/product/dast/"}>
                <Text color={"#2e2e38"} mb={3} size={"small"} weight={"bold"}>
                  {translate.t("menu.platform.products.links.dast")}
                </Text>
              </AirsLink>
              <AirsLink hovercolor={"#bf0b1a"} href={"/product/mpt/"}>
                <Text color={"#2e2e38"} mb={3} size={"small"} weight={"bold"}>
                  {translate.t("menu.platform.products.links.mpt")}
                </Text>
              </AirsLink>
              <AirsLink hovercolor={"#bf0b1a"} href={"/product/mast/"}>
                <Text color={"#2e2e38"} mb={3} size={"small"} weight={"bold"}>
                  {translate.t("menu.platform.products.links.mast")}
                </Text>
              </AirsLink>
            </Container>
            <Container>
              <AirsLink hovercolor={"#bf0b1a"} href={"/product/sca/"}>
                <Text color={"#2e2e38"} mb={3} size={"small"} weight={"bold"}>
                  {translate.t("menu.platform.products.links.sca")}
                </Text>
              </AirsLink>
              <AirsLink hovercolor={"#bf0b1a"} href={"/product/cspm/"}>
                <Text color={"#2e2e38"} mb={3} size={"small"} weight={"bold"}>
                  {translate.t("menu.platform.products.links.cspm")}
                </Text>
              </AirsLink>
              <AirsLink hovercolor={"#bf0b1a"} href={"/product/ptaas/"}>
                <Text color={"#2e2e38"} mb={3} size={"small"} weight={"bold"}>
                  {translate.t("menu.platform.products.links.ptaas")}
                </Text>
              </AirsLink>
            </Container>
            <Container>
              <AirsLink hovercolor={"#bf0b1a"} href={"/product/re/"}>
                <Text color={"#2e2e38"} mb={3} size={"small"} weight={"bold"}>
                  {translate.t("menu.platform.products.links.re")}
                </Text>
              </AirsLink>
              <AirsLink hovercolor={"#bf0b1a"} href={"/product/aspm/"}>
                <Text color={"#2e2e38"} mb={3} size={"small"} weight={"bold"}>
                  {translate.t("menu.platform.products.links.aspm")}
                </Text>
              </AirsLink>
              <AirsLink
                hovercolor={"#bf0b1a"}
                href={"/solutions/secure-code-review/"}
              >
                <Text color={"#2e2e38"} mb={3} size={"small"} weight={"bold"}>
                  {translate.t("menu.platform.products.links.scr")}
                </Text>
              </AirsLink>
            </Container>
          </Grid>
        </Container>
      </Container>
    </Container>
  );
};

export { PlatformMenu };
