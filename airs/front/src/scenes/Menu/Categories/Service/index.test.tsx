import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import React from "react";

import { ServiceMenu } from ".";

describe("ServiceMenu", (): void => {
  it("renders correctly data", (): void => {
    expect.hasAssertions();

    render(<ServiceMenu display={"block"} />);

    expect(
      screen.getByText("menu.services.allInOne.title"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("menu.services.allInOne.continuous.subtitle"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("menu.services.solutions.applicationSec.title"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("menu.services.solutions.applicationSec.subtitle"),
    ).toBeInTheDocument();
  });
  it("renders links", (): void => {
    expect.hasAssertions();

    render(<ServiceMenu display={"flex"} />);

    expect(
      screen.getByRole("link", {
        name: "menu.services.allInOne.continuous.title",
      }),
    ).toHaveAttribute("href", "/services/continuous-hacking/");
    expect(
      screen.getByRole("link", {
        name: "menu.services.solutions.applicationSec.title",
      }),
    ).toHaveAttribute("href", "/solutions/");
    expect(
      screen.getByRole("link", {
        name: "menu.services.solutions.compliance.title",
      }),
    ).toHaveAttribute("href", "/compliance/");
  });
});
