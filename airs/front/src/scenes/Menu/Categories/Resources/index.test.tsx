import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import React from "react";

import { ResourcesMenu } from ".";

describe("ResourcesMenu", (): void => {
  it("renders correctly data", (): void => {
    expect.hasAssertions();

    render(<ResourcesMenu display={"block"} />);

    expect(
      screen.getByText("menu.resources.learn.clients.title"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("menu.resources.learn.blog.title"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("menu.resources.learn.blog.subtitle"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("menu.resources.learn.learnMore.subtitle"),
    ).toBeInTheDocument();
  });
  it("renders links", (): void => {
    expect.hasAssertions();

    render(<ResourcesMenu display={"flex"} />);

    expect(
      screen.getByRole("link", { name: "menu.resources.learn.blog.title" }),
    ).toHaveAttribute("href", "/blog/");
    expect(
      screen.getByRole("link", {
        name: "menu.resources.learn.learnMore.title",
      }),
    ).toHaveAttribute("href", "/learn/");
    expect(
      screen.getByRole("link", {
        name: "menu.resources.learn.downloadables.title",
      }),
    ).toHaveAttribute("href", "/resources/");
    expect(
      screen.getByRole("link", {
        name: "menu.resources.help.documentation.title",
      }),
    ).toHaveAttribute("href", "https://help.fluidattacks.com/portal/en/home");
  });
});
