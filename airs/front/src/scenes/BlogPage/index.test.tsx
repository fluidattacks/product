import "@testing-library/jest-dom";
import { render, screen, waitFor } from "@testing-library/react";
import { useStaticQuery } from "gatsby";
import React from "react";

import { BlogPage } from ".";
import { allCloudinaryMediaData } from "test-utils/mocks";
import { translate } from "utils/translations/translate";
import type { ICloudinaryMedia } from "utils/types";

interface IDataBlogPageMock {
  allCloudinaryMedia: ICloudinaryMedia;
  allMarkdownRemark: {
    edges: [
      {
        node: {
          fields: {
            slug: string;
          };
          frontmatter: {
            alt: string;
            description: string;
            image: string;
            slug: string;
            subtitle: string;
            tags: string;
            title: string;
          };
        };
      },
    ];
  };
}

const mockUseStaticQuery = jest.spyOn({ useStaticQuery }, `useStaticQuery`);
const mockDataUseStaticQuery: IDataBlogPageMock = {
  allCloudinaryMedia: allCloudinaryMediaData,
  allMarkdownRemark: {
    edges: [
      {
        node: {
          fields: {
            slug: "",
          },
          frontmatter: {
            alt: "Alt content",
            description: "Description content",
            image: "Image content",
            slug: "Slug content",
            subtitle: "Subtitle content",
            tags: "Tags content",
            title: "Title content",
          },
        },
      },
    ],
  },
};

describe("BlogPage", (): void => {
  beforeEach((): void => {
    mockUseStaticQuery.mockImplementation(
      (): IDataBlogPageMock => mockDataUseStaticQuery,
    );
  });
  afterEach((): void => {
    jest.restoreAllMocks();
  });
  it("BlogPage should render mocked data", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <BlogPage
        author={"Author content"}
        category={"Category content"}
        date={"February 13, 2024"}
        description={"Description content"}
        headings={[{ depth: 2, value: "head1" }]}
        html={"Html content"}
        image={"Image content"}
        language={"en"}
        slug={"Slug content"}
        subtitle={"Subtitle content"}
        tags={"Tags content"}
        timeToRead={5}
        title={"Title content"}
        writer={"Writer content"}
      />,
    );

    await waitFor((): void => {
      expect(
        screen.queryByText(translate.t("loadingState.text")),
      ).not.toBeInTheDocument();
    });

    expect(screen.queryByText("Html content")).toBeInTheDocument();
    expect(screen.queryByText("Category content")).toBeInTheDocument();
    expect(screen.queryByText("Title content")).toBeInTheDocument();
    expect(screen.queryByText("Description content")).toBeInTheDocument();
    expect(screen.queryByText("Author content")).toBeInTheDocument();
    expect(
      screen.queryByText(
        `February 13, 2024 | 5 ${translate.t("blog.infoSection.readTime")}`,
      ),
    ).toBeInTheDocument();

    expect(
      screen.queryByText(translate.t("blog.share.title")),
    ).toBeInTheDocument();
    expect(screen.queryByText("Tags:")).toBeInTheDocument();
    expect(screen.queryByText("Tags content")).toBeInTheDocument();
    expect(
      screen.queryByText(translate.t("blog.subscribeCta.title")),
    ).toBeInTheDocument();
    expect(
      screen.queryByText(translate.t("blog.subscribeCta.paragraph")),
    ).toBeInTheDocument();
    expect(
      screen.queryByText(translate.t("blog.subscribeCta.button")),
    ).toBeInTheDocument();
    expect(
      screen.queryByText(translate.t("blog.slide.title")),
    ).toBeInTheDocument();
    expect(
      screen.queryByText(translate.t("blog.slide.paragraph")),
    ).toBeInTheDocument();
    expect(
      screen.queryByText(translate.t("blog.ctaTitle")),
    ).toBeInTheDocument();
    expect(
      screen.queryByText(translate.t("blog.ctaDescription")),
    ).toBeInTheDocument();
  });
});
