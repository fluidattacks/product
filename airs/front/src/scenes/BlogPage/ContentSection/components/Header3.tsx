import React from "react";

import { Container } from "../../../../components/Container";
import { Title } from "../../../../components/Typography";
import type { ITitleProps } from "../../../../components/Typography";

const Header3: React.FC<ITitleProps> = ({ children }): JSX.Element => {
  if (typeof children === "string") {
    const titleID = children
      .replace(/[^\w\s-]/giu, "")
      .replace(/ /gu, "-")
      .toLowerCase();

    return (
      <div id={titleID} style={{ scrollMarginTop: "6em" }}>
        <Container pv={3}>
          <Title color={"#2e2e38"} level={3} size={"xs"}>
            {children}
          </Title>
        </Container>
      </div>
    );
  }

  return (
    <Container pv={3}>
      <Title color={"#2e2e38"} level={3} size={"xs"}>
        {children}
      </Title>
    </Container>
  );
};

export { Header3 };
