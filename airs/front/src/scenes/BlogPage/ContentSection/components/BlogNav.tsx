import React from "react";

import { Container } from "../../../../components/Container";
import { ShareSection } from "../../../../components/ShareSection";
import { TableOfContents } from "../../../../components/TableOfContents";
import { useWindowSize } from "../../../../utils/hooks/useWindowSize";

interface IBlogNavTypes {
  headings: [
    {
      depth: number;
      value: string;
    },
  ];
  slug: string;
}

const BlogNav: React.FC<IBlogNavTypes> = ({ headings, slug }): JSX.Element => {
  const { width } = useWindowSize();

  return (
    <div style={{ position: "sticky", top: "105px" }}>
      <Container display={width > 1200 ? "flex" : "none"}>
        <TableOfContents headings={headings} />
      </Container>
      <ShareSection slug={slug} />
    </div>
  );
};

export { BlogNav };
