import { styled } from "styled-components";

type TPosition = "first" | "intermediate" | "last";

const TableContainer = styled.div`
  max-width: 1431px;
  overflow-x: auto;
  width: 80%;
  &::-webkit-scrollbar {
    width: 3px;
    border-radius: 15px;
  }

  &::-webkit-scrollbar-track {
    background: #e9e9ed;
  }

  &::-webkit-scrollbar-thumb {
    background: #d2d2da;
  }
`;

const ComparativeTable = styled.table.attrs({ className: "mv4 w-100" })`
  border-spacing: 0;
  min-width: 1050px;
  th:first-child {
    width: 11%;
  }
`;

const HeadColumn = styled.th<{
  $position: TPosition;
}>`
  border-top-left-radius: ${({ $position }): string =>
    $position === "first" ? "20px" : "0px"};
  border-top-right-radius: ${({ $position }): string =>
    $position === "last" ? "20px" : "0px"};
  padding-left: 10px;
  padding-right: 10px;
  background: #2e2e38;
  height: 50px;
  max-width: 80px;
  min-width: 120px;
  text-wrap: stable;
  align-items: center;
`;

const Row = styled.tr`
  height: 50px;
`;

const Cell = styled.td<{
  $isLast: boolean;
  $isBottom: boolean;
}>`
  border-right: ${({ $isLast }): string =>
    $isLast ? "solid 1px #dddde3" : "0px"};
  border-bottom: solid 1px #dddde3;
  border-bottom-right-radius: ${({ $isBottom, $isLast }): string =>
    $isBottom && $isLast ? "20px" : "0px"};
  border-spacing: 0px;
  text-align: center;
  padding-top: 10px;
  height: 50px;
`;
const DescriptionCell = styled.td<{
  $position: TPosition;
}>`
  border-top: ${({ $position }): string =>
    $position === "first" ? "solid 1px #dddde3" : "0px"};
  border-top-left-radius: ${({ $position }): string =>
    $position === "first" ? "20px" : "0px"};
  border-bottom-left-radius: ${({ $position }): string =>
    $position === "last" ? "20px" : "0px"};
  border-left: solid 1px #dddde3;
  border-bottom: solid 1px #dddde3;
  text-align: center;
  color: #535365;
  font-size: 16px;
  padding-left: 16px;
`;

const DescriptionContainer = styled.div`
  max-width: 1431px;
  width: 80%;
  color: #535365;
  display: flex;
  @media (max-width: 1000px) {
    display: grid;
  }
`;

const LeftColumn = styled.div`
  padding-top: 80px;
  flex-grow: 1;
`;

const RightColumn = styled.div`
  padding-top: 30px;
  width: 300px;
  text-align: left;
`;

const LevelDescLeftColumn = styled.div`
  height: 40px;
  text-align: left;
  width: 90px;
  padding-top: 10px;
`;

const LevelDescRightColumn = styled.div`
  height: 40px;
  text-align: left;
  width: 200px;
  padding-top: 8px;
  padding-left: 10px;
`;

export {
  ComparativeTable,
  DescriptionContainer,
  HeadColumn,
  LeftColumn,
  LevelDescLeftColumn,
  LevelDescRightColumn,
  RightColumn,
  Row,
  TableContainer,
  Cell,
  DescriptionCell,
};
