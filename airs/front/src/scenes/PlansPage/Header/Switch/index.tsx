import React, { useCallback } from "react";

import { SwitcherButton, SwitcherWrapper } from "./styles";
import type { ISwitchProps } from "./types";

const Switch = ({
  currentSelection,
  handleSwitch,
  options,
}: Readonly<ISwitchProps>): JSX.Element => {
  const handleClick = useCallback(
    (option: string): VoidFunction =>
      (): void => {
        handleSwitch(option);
      },
    [handleSwitch],
  );

  return (
    <SwitcherWrapper>
      {options.map(
        (option: string): JSX.Element => (
          <SwitcherButton
            $active={option.toLocaleLowerCase().includes(currentSelection)}
            key={option}
            onClick={handleClick(option)}
          >
            {option}
          </SwitcherButton>
        ),
      )}
    </SwitcherWrapper>
  );
};

export { Switch };
