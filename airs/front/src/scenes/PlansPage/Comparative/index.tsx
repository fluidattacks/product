/* eslint react/forbid-component-props: 0 */
import { BsArrowRight } from "@react-icons/all-files/bs/BsArrowRight";
import React from "react";

import {
  Cell,
  ComparativeTable,
  DescriptionCell,
  HeadColumn,
  Row,
  TableContainer,
} from "./styles";

import { AirsLink } from "../../../components/AirsLink";
import { CloudImage } from "../../../components/CloudImage";
import { Container } from "../../../components/Container";
import { Text, Title } from "../../../components/Typography";
import { translate } from "../../../utils/translations/translate";

interface IComparison {
  text: string;
  essential: boolean;
  advanced: boolean;
}

const ComparativePlans: React.FC = (): JSX.Element => {
  const cells: IComparison[] = [
    {
      advanced: true,
      essential: true,
      text: translate.t(
        "plansPage.comparativePlans.characteristics.characteristic1",
      ),
    },
    {
      advanced: true,
      essential: true,
      text: translate.t(
        "plansPage.comparativePlans.characteristics.characteristic2",
      ),
    },
    {
      advanced: true,
      essential: true,
      text: translate.t(
        "plansPage.comparativePlans.characteristics.characteristic3",
      ),
    },
    {
      advanced: true,
      essential: true,
      text: translate.t(
        "plansPage.comparativePlans.characteristics.characteristic4",
      ),
    },
    {
      advanced: true,
      essential: true,
      text: translate.t(
        "plansPage.comparativePlans.characteristics.characteristic5",
      ),
    },
    {
      advanced: true,
      essential: true,
      text: translate.t(
        "plansPage.comparativePlans.characteristics.characteristic6",
      ),
    },
    {
      advanced: true,
      essential: false,
      text: translate.t(
        "plansPage.comparativePlans.characteristics.characteristic7",
      ),
    },
    {
      advanced: true,
      essential: false,
      text: translate.t(
        "plansPage.comparativePlans.characteristics.characteristic8",
      ),
    },
    {
      advanced: true,
      essential: false,
      text: translate.t(
        "plansPage.comparativePlans.characteristics.characteristic9",
      ),
    },
    {
      advanced: true,
      essential: false,
      text: translate.t(
        "plansPage.comparativePlans.characteristics.characteristic10",
      ),
    },
    {
      advanced: true,
      essential: false,
      text: translate.t(
        "plansPage.comparativePlans.characteristics.characteristic11",
      ),
    },
  ];

  return (
    <Container
      align={"center"}
      bgColor={"#ffffff"}
      display={"flex"}
      id={"comparative-plans-table"}
      justify={"center"}
      ph={4}
      pv={5}
      wrap={"wrap"}
    >
      <Title
        color={"#bf0b1a"}
        level={3}
        mb={3}
        size={"small"}
        textAlign={"center"}
      >
        {translate.t("plansPage.comparativePlans.title")}
      </Title>
      <Container maxWidth={"951px"} ph={4}>
        <Title
          color={"#2e2e38"}
          level={1}
          mb={3}
          size={"medium"}
          textAlign={"center"}
        >
          {translate.t("plansPage.comparativePlans.subtitle")}
        </Title>
      </Container>
      <Text color={"#535365"} mb={4} size={"medium"} textAlign={"center"}>
        {translate.t("plansPage.comparativePlans.paragraph")}
      </Text>
      <TableContainer>
        <ComparativeTable>
          <thead>
            <tr>
              <HeadColumn />
              <HeadColumn>
                <Title
                  color={"#2e2e38"}
                  level={3}
                  size={"xs"}
                  textAlign={"center"}
                >
                  {translate.t("plansPage.comparativePlans.essentialTitle")}
                </Title>
                <AirsLink
                  decoration={"underline"}
                  hovercolor={"#bf0b1a"}
                  href={"https://app.fluidattacks.com/SignUp"}
                >
                  <Text color={"#2e2e38"} mt={2}>
                    {translate.t("plansPage.header.button.essential")}
                    <BsArrowRight size={13} style={{ marginLeft: "7px" }} />
                  </Text>
                </AirsLink>
              </HeadColumn>
              <HeadColumn>
                <Title
                  color={"#2e2e38"}
                  level={3}
                  size={"xs"}
                  textAlign={"center"}
                >
                  {translate.t("plansPage.comparativePlans.advancedTitle")}
                </Title>
                <AirsLink
                  decoration={"underline"}
                  hovercolor={"#bf0b1a"}
                  href={"/contact-us/"}
                >
                  <Text color={"#2e2e38"} mt={2}>
                    {translate.t("plansPage.header.button.advanced")}
                    <BsArrowRight size={13} style={{ marginLeft: "7px" }} />
                  </Text>
                </AirsLink>
              </HeadColumn>
            </tr>
          </thead>

          <tbody>
            {[...Array(11).keys()].map(
              (el: number): JSX.Element => (
                <Row key={`row-${el}`}>
                  <DescriptionCell key={`descriptionCell-${el}`}>
                    <Text color={"#535365"}>{cells[el].text}</Text>
                  </DescriptionCell>
                  <Cell key={`cell-${el}`}>
                    <CloudImage
                      alt={"plans-icon"}
                      key={`icon-image${el}`}
                      src={
                        cells[el].essential
                          ? "airs/plans/circle-check"
                          : "airs/plans/circle-xmark"
                      }
                    />
                  </Cell>
                  <Cell>
                    <CloudImage
                      alt={"plans-icon"}
                      key={`icon-image${el}`}
                      src={
                        cells[el].advanced
                          ? "airs/plans/circle-check"
                          : "airs/plans/circle-xmark"
                      }
                    />
                  </Cell>
                </Row>
              ),
            )}
          </tbody>
        </ComparativeTable>
      </TableContainer>
    </Container>
  );
};

export { ComparativePlans };
