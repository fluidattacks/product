import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { useStaticQuery } from "gatsby";
import React from "react";

import { AuthorsPage } from ".";
import { allCloudinaryMediaData } from "test-utils/mocks";
import { translate } from "utils/translations/translate";
import type { ICloudinaryMedia } from "utils/types";

interface IDataAuthorsPageMock {
  allCloudinaryMedia: ICloudinaryMedia;
  allMarkdownRemark: {
    edges: [
      {
        node: {
          fields: {
            slug: string;
          };
          frontmatter: {
            author: string;
            writer: string;
          };
        };
      },
    ];
  };
}

const mockUseStaticQuery = jest.spyOn({ useStaticQuery }, `useStaticQuery`);
const mockDataUseStaticQuery: IDataAuthorsPageMock = {
  allCloudinaryMedia: allCloudinaryMediaData,
  allMarkdownRemark: {
    edges: [
      {
        node: {
          fields: {
            slug: "",
          },
          frontmatter: {
            author: "Author content",
            writer: "Writer content",
          },
        },
      },
    ],
  },
};

describe("AuthorsPage", (): void => {
  beforeEach((): void => {
    mockUseStaticQuery.mockImplementation(
      (): IDataAuthorsPageMock => mockDataUseStaticQuery,
    );
  });
  afterEach((): void => {
    jest.restoreAllMocks();
  });
  it("AuthorsPage should render mocked data", (): void => {
    expect.hasAssertions();

    render(<AuthorsPage />);

    expect(screen.queryByText("Authors")).toBeInTheDocument();
    expect(
      screen.queryByText(translate.t("blog.subscribeCta.title")),
    ).toBeInTheDocument();
    expect(screen.queryByText("Author content")).toBeInTheDocument();
    expect(
      screen.queryByText(translate.t("blog.ctaTitle")),
    ).toBeInTheDocument();
    expect(
      screen.queryByText(translate.t("blog.ctaDescription")),
    ).toBeInTheDocument();
    expect(screen.queryByText("Try for free")).toBeInTheDocument();
    expect(screen.queryByText("Learn more")).toBeInTheDocument();
    expect(screen.queryByText("Image not found")).toBeInTheDocument();
  });
});
