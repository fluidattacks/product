import React from "react";

import { PlanTag, PlansGrid, Tag } from "./styles";

import { AirsLink } from "../../../components/AirsLink";
import { Button } from "../../../components/Button";
import { Container } from "../../../components/Container";
import { Text, Title } from "../../../components/Typography";
import { useWindowSize } from "../../../utils/hooks/useWindowSize";
import { translate } from "../../../utils/translations/translate";

interface IContinuosHackingProps {
  language: string;
}

const ContinuousHackingPlans: React.FC<IContinuosHackingProps> = ({
  language,
}): JSX.Element => {
  const { width } = useWindowSize();

  return (
    <Container
      align={"center"}
      bgColor={"#ffffff"}
      display={"flex"}
      justify={"center"}
      pt={4}
      width={"100%"}
      wrap={"wrap"}
    >
      <Container
        bgColor={"#ffffff"}
        maxWidth={"1210px"}
        mh={width < 480 ? 2 : 0}
      >
        <Title
          color={"#2e2e38"}
          level={2}
          mb={4}
          size={width > 480 ? "big" : "medium"}
          textAlign={"center"}
        >
          {translate.t("continuousHackingPage.plans.title")}
        </Title>
      </Container>
      <PlansGrid>
        <PlanTag>
          {width > 630 ? (
            <Container
              align={"end"}
              display={"flex"}
              justify={"center"}
              pl={3}
              pt={3}
            >
              <Title color={"#2e2e38"} level={2} ml={4} mt={4} size={"medium"}>
                {translate.t("continuousHackingPage.plans.essential.title")}
              </Title>
              <Container pb={2} width={"100%"} widthSm={"0px"}>
                <Tag>{translate.t("plansPage.header.essential.tag")}</Tag>
              </Container>
            </Container>
          ) : (
            <Container
              align={"end"}
              display={"flex"}
              justify={"center"}
              pl={3}
              pt={3}
              wrap={"wrap"}
            >
              <Container ml={width > 380 ? 4 : 2} mv={3} width={"100%"}>
                <Tag>{translate.t("plansPage.header.essential.tag")}</Tag>
              </Container>
              <Container width={"100%"}>
                <Title
                  color={"#2e2e38"}
                  level={2}
                  ml={width > 380 ? 4 : 2}
                  mt={2}
                  size={"medium"}
                >
                  {translate.t("continuousHackingPage.plans.essential.title")}
                </Title>
              </Container>
            </Container>
          )}

          <Container maxWidth={"80%"} pl={3}>
            <Text color={"#1D2939"} ml={4} mt={4} size={"big"}>
              {translate.t("continuousHackingPage.plans.essential.subtitle")}
            </Text>
          </Container>
          <Container
            display={"flex"}
            justify={"start"}
            justifyMd={"center"}
            justifySm={"unset"}
            ml={4}
            mv={width > 960 ? 5 : 4}
            pl={3}
            wrap={"wrap"}
          >
            <Container pv={1} width={"auto"} widthSm={"80%"}>
              <AirsLink
                decoration={"none"}
                href={"https://app.fluidattacks.com/SignUp"}
              >
                <Button display={"block"} variant={"primary"}>
                  {translate.t("continuousHackingPage.plans.essential.button1")}
                </Button>
              </AirsLink>
            </Container>
            <Container ph={3} phSm={0} pv={1} width={"auto"} widthSm={"80%"}>
              <AirsLink decoration={"none"} href={"/plans/#essential"}>
                <Button display={"block"} variant={"tertiary"}>
                  {translate.t("continuousHackingPage.plans.essential.button2")}
                </Button>
              </AirsLink>
            </Container>
          </Container>
        </PlanTag>
        <PlanTag>
          <Container align={"end"} display={"flex"} justify={"center"} pl={3}>
            <Title color={"#2e2e38"} level={2} ml={4} mt={4} size={"medium"}>
              {translate.t("continuousHackingPage.plans.advanced.title")}
            </Title>
          </Container>
          <Container width={"80%"}>
            <Container pl={3}>
              <Text color={"#1D2939"} ml={4} mt={4} size={"big"}>
                {translate.t("continuousHackingPage.plans.advanced.subtitle")}
              </Text>
            </Container>
          </Container>

          <Container
            display={"flex"}
            justify={"start"}
            justifyMd={"center"}
            justifySm={"unset"}
            ml={4}
            mv={4}
            wrap={"wrap"}
          >
            <Container
              ph={3}
              phSm={0}
              pv={language === "es" ? 3 : 1}
              pvMd={3}
              width={"auto"}
              widthSm={"80%"}
            >
              <AirsLink decoration={"none"} href={"/plans/"}>
                <Button display={"block"} variant={"tertiary"}>
                  {translate.t("continuousHackingPage.plans.advanced.button")}
                </Button>
              </AirsLink>
            </Container>
          </Container>
        </PlanTag>
      </PlansGrid>
    </Container>
  );
};

export { ContinuousHackingPlans };
