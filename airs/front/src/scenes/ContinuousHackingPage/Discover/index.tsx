import React, { useLayoutEffect } from "react";

import { ContentCard } from "./ContentCard";
import { DiscoverOnMobile } from "./MobileContent";
import { ContentColumn, ContinuousRow, VectorColumn } from "./styles";
import { Vector } from "./Vector";

import { CloudImage } from "../../../components/CloudImage";
import { Container } from "../../../components/Container";
import { Title } from "../../../components/Typography";
import { useWindowSize } from "../../../utils/hooks/useWindowSize";
import { translate } from "../../../utils/translations/translate";

const DiscoverContinuousHacking: React.FC = (): JSX.Element => {
  const { width } = useWindowSize();
  const cards = [
    {
      icon: "airs/services/continuous-hacking/discover/icon1",
      paragraph: translate.t(
        "continuousHackingPage.discover.content.card1.paragraph",
      ),
      subtitle: translate.t(
        "continuousHackingPage.discover.content.card1.subtitle",
      ),
      title: translate.t("continuousHackingPage.discover.content.card1.title"),
    },
    {
      icon: "airs/services/continuous-hacking/discover/icon2",
      paragraph: translate.t(
        "continuousHackingPage.discover.content.card2.paragraph",
      ),
      subtitle: translate.t(
        "continuousHackingPage.discover.content.card2.subtitle",
      ),
      title: translate.t("continuousHackingPage.discover.content.card2.title"),
    },
    {
      icon: "airs/services/continuous-hacking/discover/icon3",
      paragraph: translate.t(
        "continuousHackingPage.discover.content.card3.paragraph",
      ),
      subtitle: translate.t(
        "continuousHackingPage.discover.content.card3.subtitle",
      ),
      title: translate.t("continuousHackingPage.discover.content.card3.title"),
    },
    {
      icon: "airs/services/continuous-hacking/discover/icon4",
      paragraph: translate.t(
        "continuousHackingPage.discover.content.card4.paragraph",
      ),
      subtitle: translate.t(
        "continuousHackingPage.discover.content.card4.subtitle",
      ),
      title: translate.t("continuousHackingPage.discover.content.card4.title"),
    },
  ];

  useLayoutEffect((): VoidFunction => {
    const onScroll = (): void => {
      const scrollDistance = -document
        .getElementsByClassName("discover-section")[0]
        .getBoundingClientRect().top;
      const progressPercentage =
        document.getElementsByClassName("discover-section")[0].scrollHeight -
        document.documentElement.clientHeight;
      const scrolled = scrollDistance / progressPercentage;
      const path = document.getElementById("vector") as SVGPathElement | null;
      if (!path) {
        return;
      }
      const pathLength = path.getTotalLength();
      const pointOnPath = path.getPointAtLength(pathLength * (scrolled - 0.1));
      const dot = document.getElementById("dot") as SVGElement | null;
      if (scrolled < 0) {
        dot?.setAttribute(
          "transform",
          `translate(${path.getPointAtLength(150).x},${
            path.getPointAtLength(150).y
          })`,
        );
      }

      if (scrolled > 0.17 && scrolled < 0.991) {
        dot?.setAttribute(
          "transform",
          `translate(${pointOnPath.x},${pointOnPath.y})`,
        );
      }
    };
    window.addEventListener("scroll", onScroll, { passive: true });

    return (): void => {
      window.removeEventListener("scroll", onScroll);
    };
  }, []);

  if (width < 1140) {
    return <DiscoverOnMobile />;
  }

  return (
    <Container
      align={"center"}
      bgColor={"#ffffff"}
      classname={"discover-section"}
      display={"flex"}
      justify={"center"}
      wrap={"wrap"}
    >
      <Container maxWidth={"1210px"} mh={4}>
        <Title
          color={"#bf0b1a"}
          level={2}
          mb={4}
          mt={5}
          size={"small"}
          textAlign={"center"}
        >
          {translate.t("continuousHackingPage.discover.subtitle")}
        </Title>
        <Title color={"#2e2e38"} level={2} size={"big"} textAlign={"center"}>
          {translate.t("continuousHackingPage.discover.title")}
        </Title>
      </Container>
      <ContinuousRow>
        <ContentColumn>
          <ContentCard
            icon={cards[0].icon}
            mb={7}
            mt={6}
            paragraph={cards[0].paragraph}
            subtitle={cards[0].subtitle}
            title={cards[0].title}
          />
          <CloudImage
            alt={"discoverImage1"}
            src={"airs/services/continuous-hacking/discover/image2"}
            styles={"mb5"}
          />
          <ContentCard
            icon={cards[2].icon}
            mb={5}
            mt={6}
            paragraph={cards[2].paragraph}
            subtitle={cards[2].subtitle}
            title={cards[2].title}
          />
          <CloudImage
            alt={"discoverImage1"}
            src={"airs/services/continuous-hacking/discover/image4"}
            styles={"mt5"}
          />
        </ContentColumn>
        <VectorColumn>
          <Vector />
        </VectorColumn>
        <ContentColumn>
          <CloudImage
            alt={"discoverImage1"}
            src={"airs/services/continuous-hacking/discover/image1"}
          />
          <ContentCard
            icon={cards[1].icon}
            mb={7}
            mt={7}
            paragraph={cards[1].paragraph}
            subtitle={cards[1].subtitle}
            title={cards[1].title}
          />
          <CloudImage
            alt={"discoverImage1"}
            src={"airs/services/continuous-hacking/discover/image3"}
          />
          <ContentCard
            icon={cards[3].icon}
            mb={1}
            mt={6}
            paragraph={cards[3].paragraph}
            subtitle={cards[3].subtitle}
            title={cards[3].title}
          />
        </ContentColumn>
      </ContinuousRow>
    </Container>
  );
};

export { DiscoverContinuousHacking };
