import i18next from "i18next";
import React from "react";

import { Card } from "./styles";

import { Container } from "../../../components/Container";
import { Grid } from "../../../components/Grid";
import { Hero } from "../../../components/Hero";
import { Text, Title } from "../../../components/Typography";
import { useWindowSize } from "../../../utils/hooks/useWindowSize";
import { translate } from "../../../utils/translations/translate";

const ContinuousHackingHeader: React.FC = (): JSX.Element => {
  const { language } = i18next;
  const { width } = useWindowSize();

  return (
    <Container
      align={"center"}
      bgGradient={"110.83deg, #2E2E38 1.35%, #121216 99.1%"}
      display={"flex"}
      wrap={"wrap"}
    >
      <Hero
        button1Link={"https://app.fluidattacks.com/SignUp"}
        button1Text={translate.t("continuousHackingPage.portrait.hero.button1")}
        button2Link={"/contact-us/"}
        button2Text={translate.t("continuousHackingPage.portrait.hero.button2")}
        image={`airs/${
          language === "es" ? "es/" : ""
        }services/continuous-hacking/header-hero`}
        imageStyles={"w-90"}
        paragraph={translate.t("continuousHackingPage.portrait.hero.paragraph")}
        subtitle={translate.t("continuousHackingPage.portrait.hero.subtitle")}
        title={translate.t("continuousHackingPage.portrait.hero.title")}
        tone={"darkTransparent"}
        variant={"center"}
      />
      <Container align={"center"} display={"flex"} maxWidth={"100%"} mh={4}>
        <Title
          color={"#ffffff"}
          level={1}
          mb={2}
          mt={3}
          size={width < 480 ? "small" : "medium"}
          textAlign={"center"}
        >
          {translate.t("continuousHackingPage.portrait.title")}
        </Title>
      </Container>

      <Text color={"#ffffff"} ml={3} mr={3} size={"big"} textAlign={"center"}>
        {translate.t("continuousHackingPage.portrait.subtitle")}
      </Text>
      <Container
        center={true}
        maxWidth={width >= 960 ? "1400px" : "700px"}
        mt={4}
        pb={5}
        ph={4}
        phMd={0}
      >
        <Grid
          columns={4}
          columnsMd={width > 580 ? 2 : 1}
          columnsSm={1}
          gap={"0.5rem"}
        >
          <Container align={"center"} display={"flex"} justify={"center"}>
            <Card
              bgGradient={"180deg, #3A3A46 0%, #121216 100%"}
              br={4}
              description={translate.t(
                "continuousHackingPage.portrait.card1.subtitle",
              )}
              descriptionColor={"#ffffff"}
              image={"airs/services/continuous-hacking/card1"}
              maxWidth={"310px"}
              title={translate.t("continuousHackingPage.portrait.card1.title")}
              titleColor={"#ffffff"}
              titleSize={"xs"}
            />
          </Container>
          <Container align={"center"} display={"flex"} justify={"center"}>
            <Card
              bgGradient={"180deg, #3A3A46 0%, #121216 100%"}
              br={4}
              description={translate.t(
                "continuousHackingPage.portrait.card2.subtitle",
              )}
              descriptionColor={"#ffffff"}
              image={"airs/services/continuous-hacking/card2"}
              maxWidth={"310px"}
              title={translate.t("continuousHackingPage.portrait.card2.title")}
              titleColor={"#ffffff"}
              titleSize={"xs"}
            />
          </Container>
          <Container align={"center"} display={"flex"} justify={"center"}>
            <Card
              bgGradient={"180deg, #3A3A46 0%, #121216 100%"}
              br={4}
              description={translate.t(
                "continuousHackingPage.portrait.card3.subtitle",
              )}
              descriptionColor={"#ffffff"}
              image={"airs/services/continuous-hacking/card3"}
              maxWidth={"310px"}
              title={translate.t("continuousHackingPage.portrait.card3.title")}
              titleColor={"#ffffff"}
              titleSize={"xs"}
            />
          </Container>
          <Container align={"center"} display={"flex"} justify={"center"}>
            <Card
              bgGradient={"180deg, #3A3A46 0%, #121216 100%"}
              br={4}
              description={translate.t(
                "continuousHackingPage.portrait.card4.subtitle",
              )}
              descriptionColor={"#ffffff"}
              image={"airs/services/continuous-hacking/card4"}
              maxWidth={"310px"}
              title={translate.t("continuousHackingPage.portrait.card4.title")}
              titleColor={"#ffffff"}
              titleSize={"xs"}
            />
          </Container>
        </Grid>
      </Container>
    </Container>
  );
};

export { ContinuousHackingHeader };
