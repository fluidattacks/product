import React from "react";

import { ContinuousHackingClients } from "./Clients";
import { DiscoverContinuousHacking } from "./Discover";
import { ContinuousHackingPlans } from "./Plans";
import { ContinuousHackingHeader } from "./Portrait";
import { ContinuousHackingTools } from "./Tools";

interface IContinuosHackingProps {
  language: string;
}

const ContinuousHackingPage: React.FC<IContinuosHackingProps> = ({
  language,
}): JSX.Element => {
  return (
    <React.Fragment>
      <ContinuousHackingHeader />
      <DiscoverContinuousHacking />
      <ContinuousHackingTools />
      <ContinuousHackingPlans language={language} />
      <ContinuousHackingClients />
    </React.Fragment>
  );
};

export { ContinuousHackingPage };
