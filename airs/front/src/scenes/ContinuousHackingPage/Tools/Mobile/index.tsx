import React from "react";

import { CloudImage } from "../../../../components/CloudImage";
import { Container } from "../../../../components/Container";
import { Text, Title } from "../../../../components/Typography";
import { useWindowSize } from "../../../../utils/hooks/useWindowSize";
import { translate } from "../../../../utils/translations/translate";
import { IconBlock } from "../styles";

const ContinuousHackingToolsMobile: React.FC = (): JSX.Element => {
  const { width } = useWindowSize();

  return (
    <Container
      align={"center"}
      bgColor={"#ffffff"}
      display={"flex"}
      justify={"center"}
      pt={5}
      wrap={"wrap"}
    >
      <Container
        bgColor={"#ffffff"}
        maxWidth={"1210px"}
        mh={width < 480 ? 2 : 0}
      >
        <Title
          color={"#2e2e38"}
          level={2}
          mb={4}
          size={"medium"}
          textAlign={"center"}
        >
          {translate.t("continuousHackingPage.tools.title")}
        </Title>
      </Container>
      <Container
        display={"flex"}
        justify={"center"}
        maxWidth={"1290px"}
        pt={3}
        wrap={"wrap"}
      >
        <Container
          align={"center"}
          display={"flex"}
          justify={"center"}
          width={"40%"}
        >
          <CloudImage
            alt={"tools-image"}
            src={"airs/services/continuous-hacking/tools/small/order-small"}
          />
        </Container>

        <Container width={"50%"}>
          <Container>
            <Title
              color={"#2e2e38"}
              level={2}
              mb={2}
              size={"xxs"}
              textAlign={"start"}
            >
              {translate.t("continuousHackingPage.tools.types.1.title")}
            </Title>
            <Container align={"center"} display={"flex"} mv={2}>
              <IconBlock>
                <CloudImage
                  alt={"tools-image"}
                  src={"airs/services/continuous-hacking/tools/identification1"}
                />
              </IconBlock>
              <Container display={"flex"} wrap={"wrap"}>
                <Text color={"#25252d"} display={"inline-block"} size={"xs"}>
                  {translate.t("continuousHackingPage.tools.types.1.1.title")}
                </Text>
                <Text color={"#40404f"} size={"xs"}>
                  {translate.t(
                    "continuousHackingPage.tools.types.1.1.subtitle",
                  )}
                </Text>
              </Container>
            </Container>
            <Container align={"center"} display={"flex"}>
              <IconBlock>
                <CloudImage
                  alt={"tools-image"}
                  src={"airs/services/continuous-hacking/tools/identification2"}
                />
              </IconBlock>
              <Container display={"flex"} wrap={"wrap"}>
                <Text color={"#25252d"} display={"inline-block"} size={"xs"}>
                  {translate.t("continuousHackingPage.tools.types.1.2")}
                </Text>
              </Container>
            </Container>
            <Container align={"center"} display={"flex"} mv={2}>
              <IconBlock>
                <CloudImage
                  alt={"tools-image"}
                  src={"airs/services/continuous-hacking/tools/identification3"}
                />
              </IconBlock>
              <Container display={"flex"} wrap={"wrap"}>
                <Text color={"#25252d"} display={"inline-block"} size={"xs"}>
                  {translate.t("continuousHackingPage.tools.types.1.3.title")}
                </Text>
                <Text color={"#40404f"} size={"xs"}>
                  {translate.t(
                    "continuousHackingPage.tools.types.1.3.subtitle",
                  )}
                </Text>
              </Container>
            </Container>
          </Container>
          <Container>
            <Title
              color={"#2e2e38"}
              level={2}
              mb={2}
              size={"xxs"}
              textAlign={"start"}
            >
              {translate.t("continuousHackingPage.tools.types.2.title")}
            </Title>
            <Container align={"center"} display={"flex"} mv={2}>
              <IconBlock>
                <CloudImage
                  alt={"tools-image"}
                  src={"airs/services/continuous-hacking/tools/review1"}
                />
              </IconBlock>
              <Container display={"flex"} wrap={"wrap"}>
                <Text color={"#25252d"} display={"inline-block"} size={"xs"}>
                  {translate.t("continuousHackingPage.tools.types.2.1.title")}
                </Text>
                <Text color={"#40404f"} size={"xs"}>
                  {translate.t(
                    "continuousHackingPage.tools.types.2.1.subtitle",
                  )}
                </Text>
              </Container>
            </Container>
            <Container align={"center"} display={"flex"}>
              <IconBlock>
                <CloudImage
                  alt={"tools-image"}
                  src={"airs/services/continuous-hacking/tools/review2"}
                />
              </IconBlock>
              <Container display={"flex"} wrap={"wrap"}>
                <Text color={"#25252d"} display={"inline-block"} size={"xs"}>
                  {translate.t("continuousHackingPage.tools.types.2.2.title")}
                </Text>
                <Text color={"#40404f"} size={"xs"}>
                  {translate.t(
                    "continuousHackingPage.tools.types.2.2.subtitle",
                  )}
                </Text>
              </Container>
            </Container>
            <Container align={"center"} display={"flex"} mv={2}>
              <IconBlock>
                <CloudImage
                  alt={"tools-image"}
                  src={"airs/services/continuous-hacking/tools/review3"}
                />
              </IconBlock>
              <Container display={"flex"} wrap={"wrap"}>
                <Text color={"#25252d"} display={"inline-block"} size={"xs"}>
                  {translate.t("continuousHackingPage.tools.types.2.3")}
                </Text>
              </Container>
            </Container>
          </Container>
          <Container>
            <Title
              color={"#2e2e38"}
              level={2}
              mb={2}
              size={"xxs"}
              textAlign={"start"}
            >
              {translate.t("continuousHackingPage.tools.types.3.title")}
            </Title>
            <Container align={"center"} display={"flex"} mv={2}>
              <IconBlock>
                <CloudImage
                  alt={"tools-image"}
                  src={"airs/services/continuous-hacking/tools/remediation1"}
                />
              </IconBlock>
              <Container display={"flex"} wrap={"wrap"}>
                <Text color={"#25252d"} display={"inline-block"} size={"xs"}>
                  {translate.t("continuousHackingPage.tools.types.3.1")}
                </Text>
              </Container>
            </Container>
            <Container align={"center"} display={"flex"}>
              <IconBlock>
                <CloudImage
                  alt={"tools-image"}
                  src={"airs/services/continuous-hacking/tools/remediation2"}
                />
              </IconBlock>
              <Container display={"flex"} wrap={"wrap"}>
                <Text color={"#25252d"} display={"inline-block"} size={"xs"}>
                  {translate.t("continuousHackingPage.tools.types.3.2")}
                </Text>
              </Container>
            </Container>
            <Container align={"center"} display={"flex"} mv={2}>
              <IconBlock>
                <CloudImage
                  alt={"tools-image"}
                  src={"airs/services/continuous-hacking/tools/remediation3"}
                />
              </IconBlock>
              <Container display={"flex"} wrap={"wrap"}>
                <Text color={"#25252d"} display={"inline-block"} size={"xs"}>
                  {translate.t("continuousHackingPage.tools.types.3.3.title")}
                </Text>
                <Text color={"#40404f"} size={"xs"}>
                  {translate.t(
                    "continuousHackingPage.tools.types.3.3.subtitle",
                  )}
                </Text>
              </Container>
            </Container>
          </Container>
          <Container>
            <Title
              color={"#2e2e38"}
              level={2}
              mb={2}
              size={"xxs"}
              textAlign={"start"}
            >
              {translate.t("continuousHackingPage.tools.types.4.title")}
            </Title>
            <Container align={"center"} display={"flex"} mv={2}>
              <IconBlock>
                <CloudImage
                  alt={"tools-image"}
                  src={"airs/services/continuous-hacking/tools/verification1"}
                />
              </IconBlock>
              <Container display={"flex"} wrap={"wrap"}>
                <Text color={"#25252d"} display={"inline-block"} size={"xs"}>
                  {translate.t("continuousHackingPage.tools.types.4.1")}
                </Text>
              </Container>
            </Container>
            <Container align={"center"} display={"flex"}>
              <IconBlock>
                <CloudImage
                  alt={"tools-image"}
                  src={"airs/services/continuous-hacking/tools/verification2"}
                />
              </IconBlock>
              <Container display={"flex"} wrap={"wrap"}>
                <Text color={"#25252d"} display={"inline-block"} size={"xs"}>
                  {translate.t("continuousHackingPage.tools.types.4.2")}
                </Text>
              </Container>
            </Container>
          </Container>
        </Container>
      </Container>
    </Container>
  );
};

export { ContinuousHackingToolsMobile };
