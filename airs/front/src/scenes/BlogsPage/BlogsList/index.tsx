import { graphql, useStaticQuery } from "gatsby";
import i18next from "i18next";
import React, { useCallback, useState } from "react";

import { Button } from "../../../components/Button";
import { Container } from "../../../components/Container";
import { Grid } from "../../../components/Grid";
import { Select, TextArea } from "../../../components/Input";
import { Pagination } from "../../../components/Pagination";
import { Text } from "../../../components/Typography";
import { VerticalCard } from "../../../components/VerticalCard";
import {
  filterBlogs,
  filterPostsByLanguage,
} from "../../../utils/filter-posts";
import { usePagination } from "../../../utils/hooks";
import { translatedPages } from "../../../utils/translations/spanishPages";
import { translate } from "../../../utils/translations/translate";
import { capitalizePlainString } from "../../../utils/utilities";

export const BlogsList: React.FC = (): JSX.Element => {
  const data: IData = useStaticQuery(graphql`
    query NewBlogsList {
      allMarkdownRemark(
        filter: {
          fields: { slug: { regex: "/blog/" } }
          frontmatter: { image: { ne: null } }
        }
        sort: { frontmatter: { date: DESC } }
      ) {
        edges {
          node {
            fields {
              slug
            }
            frontmatter {
              alt
              author
              category
              date
              slug
              description
              image
              spanish
              subtitle
              tags
              title
            }
          }
        }
      }
    }
  `);

  const currentLanguage = i18next.language;

  const allPosts = data.allMarkdownRemark.edges;

  const selectedLanguagePosts = filterPostsByLanguage(allPosts);

  const categoriesOrder = [
    translate.t("blogCategories.interview.title"),
    translate.t("blogCategories.politics.title"),
    translate.t("blogCategories.development.title"),
    translate.t("blogCategories.opinions.title"),
    translate.t("blogCategories.philosophy.title"),
    translate.t("blogCategories.attacks.title"),
  ];

  const authorsListRaw = selectedLanguagePosts.map(
    (edge): string => edge.node.frontmatter.author,
  );

  const tagsListRaw = selectedLanguagePosts
    .map((edge): string[] => edge.node.frontmatter.tags.split(", "))
    .flat();

  const categoriesListRaw = selectedLanguagePosts.map(
    (edge): string => edge.node.frontmatter.category,
  );

  const authorsList = authorsListRaw.filter(
    (author, index): boolean => authorsListRaw.indexOf(author) === index,
  );

  const tagsList = tagsListRaw.filter(
    (tag, index): boolean => tagsListRaw.indexOf(tag) === index,
  );

  const categoriesList = categoriesListRaw.filter(
    (category, index): boolean => categoriesListRaw.indexOf(category) === index,
  );

  const sortedCategories = [...categoriesList].sort(
    (first, second): number =>
      categoriesOrder.indexOf(second) - categoriesOrder.indexOf(first),
  );

  const sortedAuthors = [...authorsList].sort((first, second): number =>
    first.localeCompare(second),
  );

  const datesList = [
    translate.t("blogListDatePeriods.lastMonth"),
    translate.t("blogListDatePeriods.lastSixMonths"),
    translate.t("blogListDatePeriods.lastYear"),
  ];

  const [selectedAuthor, setSelectedAuthor] = useState(
    translate.t("blogListAuthors.all"),
  );
  const [selectedTag, setSelectedTag] = useState(
    translate.t("blogListTags.all"),
  );
  const [selectedDate, setSelectedDate] = useState("");
  const [selectedTitle, setSelectedTitle] = useState("");
  const [selectedCategory, setSelectedCategory] = useState(
    translate.t("blogListCategories.all"),
  );

  const filteredBlogCards = filterBlogs(
    {
      author: selectedAuthor,
      category: selectedCategory,
      date: selectedDate,
      tag: selectedTag,
      title: selectedTitle,
    },
    selectedLanguagePosts,
  );

  const blogsCards = filteredBlogCards.map((post): JSX.Element | undefined => {
    const { slug } = post.node.fields;
    const { alt, author, date, description, image, spanish, subtitle, title } =
      post.node.frontmatter;

    const translatedBlog = translatedPages.find(
      (page): boolean => page.en === slug,
    )?.en;
    if (slug === translatedBlog && i18next.language === "es") {
      return undefined;
    }
    if (slug.startsWith("/es/") && i18next.language === "en") {
      return undefined;
    }

    return spanish === "yes" ? undefined : (
      <VerticalCard
        alt={alt}
        author={author}
        btnText={slug.startsWith("/es/") ? "Leer *post*" : "Read post"}
        currentLanguage={currentLanguage}
        date={date}
        description={description}
        image={image}
        key={slug}
        link={slug}
        subMinHeight={"56px"}
        subtitle={subtitle}
        title={title}
        titleMinHeight={"64px"}
      />
    );
  });

  const listOfBlogs: (JSX.Element | undefined)[] = blogsCards.filter(
    (post): boolean => {
      return post !== undefined;
    },
  );

  const itemsPerPage = 9;

  const {
    currentPage,
    endOffset,
    handlePageClick,
    newOffset,
    pageCount,
    resetPagination,
  } = usePagination(itemsPerPage, listOfBlogs);

  const changeAuthor = useCallback(
    (author: string): void => {
      setSelectedAuthor(author);
      resetPagination();
    },
    [resetPagination],
  );

  const changeTag = useCallback(
    (tag: string): void => {
      setSelectedTag(tag);
      resetPagination();
    },
    [resetPagination],
  );

  const changeDate = useCallback(
    (date: string): void => {
      setSelectedDate(date);
      resetPagination();
    },
    [resetPagination],
  );

  const changeTitle = useCallback(
    (title: string): void => {
      setSelectedTitle(title);
      resetPagination();
    },
    [resetPagination],
  );

  const changeCategory = useCallback(
    (category: string): VoidFunction => {
      return (): void => {
        setSelectedCategory(category);
        resetPagination();
      };
    },
    [resetPagination],
  );

  return (
    <Container bgColor={"#fff"}>
      <Container center={true} maxWidth={"1440px"} ph={4} pv={5}>
        <Grid columns={4} columnsMd={2} columnsSm={1} gap={"1rem"}>
          <Select
            label={translate.t("blog.filters.author")}
            onChange={changeAuthor}
            options={sortedAuthors}
            value={translate.t("blogListAuthors.all")}
          />
          <Select
            label={translate.t("blog.filters.tag")}
            onChange={changeTag}
            options={tagsList}
            value={translate.t("blogListTags.all")}
          />
          <Select
            label={translate.t("blog.filters.date")}
            onChange={changeDate}
            options={datesList}
            value={translate.t("blogListDatePeriods.all")}
          />
          <TextArea
            label={translate.t("blog.filters.title.text")}
            onChange={changeTitle}
            placeHolder={translate.t("blog.filters.title.placeholder")}
          />
        </Grid>
        <Container display={"flex"} justify={"center"} pv={5} wrap={"wrap"}>
          <Container key={"All"} mh={2} mt={2} width={"auto"}>
            <Button
              onClick={changeCategory(translate.t("blogListCategories.all"))}
              selected={
                selectedCategory === translate.t("blogListCategories.all")
              }
            >
              {translate.t("blog.categories.viewAll")}
            </Button>
          </Container>
          {sortedCategories.map((category: string): JSX.Element => {
            return (
              <Container key={category} mh={2} mt={2} width={"auto"}>
                <Button
                  onClick={changeCategory(category)}
                  selected={selectedCategory === category}
                >
                  {capitalizePlainString(category)}
                </Button>
              </Container>
            );
          })}
        </Container>
        {listOfBlogs.length > 0 ? (
          <React.Fragment>
            <Grid columns={3} columnsMd={2} columnsSm={1} gap={"1rem"}>
              {listOfBlogs.slice(newOffset, endOffset)}
            </Grid>
            <Pagination
              forcePage={currentPage}
              onChange={handlePageClick}
              pageCount={pageCount}
            />
          </React.Fragment>
        ) : (
          <Container>
            <Text color={"#2e2e38"} size={"big"} textAlign={"center"}>
              {translate.t("blog.empty")}
            </Text>
          </Container>
        )}
      </Container>
    </Container>
  );
};
