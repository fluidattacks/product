import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { useStaticQuery } from "gatsby";
import React from "react";

import { BlogsPage } from ".";
import { allCloudinaryMediaData } from "test-utils/mocks";
import type { ICloudinaryMedia } from "utils/types";

interface IDataBlogsPageMock {
  allCloudinaryMedia: ICloudinaryMedia;
  allMarkdownRemark: {
    edges: {
      node: {
        fields: {
          slug: string;
        };
        frontmatter: {
          alt: string;
          author: string;
          category: string;
          date: string;
          slug: string;
          description: string;
          image: string;
          spanish: string;
          subtitle: string;
          tags: string;
          title: string;
        };
      };
    }[];
  };
}

const mockUseStaticQuery = jest.spyOn({ useStaticQuery }, `useStaticQuery`);
const mockDataUseStaticQuery: IDataBlogsPageMock = {
  allCloudinaryMedia: allCloudinaryMediaData,
  allMarkdownRemark: {
    edges: [
      {
        node: {
          fields: {
            slug: "1",
          },
          frontmatter: {
            alt: "Alt content",
            author: "Author content",
            category: "Category content",
            date: "Date content",
            description: "Description content",
            image: "Image content",
            slug: "Slug content",
            spanish: "Spanish content",
            subtitle: "Subtitle content",
            tags: "Tags content",
            title: "First post",
          },
        },
      },
      {
        node: {
          fields: {
            slug: "2",
          },
          frontmatter: {
            alt: "Alt content 2",
            author: "Author content 2",
            category: "Category content 2",
            date: "Date content 2",
            description: "Description content 2",
            image: "Image content 2",
            slug: "Slug content 2",
            spanish: "Spanish content 2",
            subtitle: "Subtitle conten 2",
            tags: "Tags content 2",
            title: "Second post",
          },
        },
      },
    ],
  },
};

describe("BlogsPage", (): void => {
  beforeEach((): void => {
    mockUseStaticQuery.mockImplementation(
      (): IDataBlogsPageMock => mockDataUseStaticQuery,
    );
  });
  afterEach((): void => {
    jest.restoreAllMocks();
  });
  it("BlogsPage should render mocked data", (): void => {
    expect.hasAssertions();

    render(<BlogsPage />);

    expect(screen.queryByText("blog.title")).toBeInTheDocument();
    expect(screen.queryByText("blog.description")).toBeInTheDocument();
    expect(screen.queryByText("blog.subscribeCta.title")).toBeInTheDocument();

    expect(screen.queryByText("blog.filters.author")).toBeInTheDocument();
    expect(screen.queryAllByText("blogListAuthors.all")[0]).toBeInTheDocument();
    expect(screen.queryByText("blog.filters.tag")).toBeInTheDocument();
    expect(screen.queryAllByText("blogListTags.all")[0]).toBeInTheDocument();
    expect(screen.queryByText("blog.filters.date")).toBeInTheDocument();
    expect(
      screen.queryAllByText("blogListDatePeriods.all")[0],
    ).toBeInTheDocument();
    expect(screen.queryByText("blog.filters.title.text")).toBeInTheDocument();
    expect(screen.queryByText("blog.categories.viewAll")).toBeInTheDocument();
    expect(screen.queryByText("Category content")).toBeInTheDocument();
    expect(screen.queryByText("Invalid Date")).toBeInTheDocument();
    expect(screen.queryAllByText("Author content")).toHaveLength(2);
    expect(screen.queryByText("Subtitle content")).toBeInTheDocument();
    expect(screen.queryByText("First post")).toBeInTheDocument();
    expect(screen.queryByText("Description content")).toBeInTheDocument();
    expect(screen.queryAllByText("Read post")).toHaveLength(2);
    expect(screen.queryByText("blog.ctaTitle")).toBeInTheDocument();
    expect(screen.queryByText("blog.ctaDescription")).toBeInTheDocument();
    expect(screen.queryByText("Try for free")).toBeInTheDocument();
    expect(screen.queryByText("Learn more")).toBeInTheDocument();
    expect(screen.queryAllByText("Image not found")).toHaveLength(3);
  });
  it("BlogsPage should allow using filters", async (): Promise<void> => {
    expect.hasAssertions();

    render(<BlogsPage />);

    expect(screen.queryByText("First post")).toBeInTheDocument();
    expect(screen.queryByText("Second post")).toBeInTheDocument();

    const dropdown = screen.getAllByRole("combobox");

    expect(dropdown).toHaveLength(3);

    await userEvent.selectOptions(dropdown[0], "Author content");

    expect(screen.queryByText("First post")).toBeInTheDocument();
    expect(screen.queryByText("Second post")).not.toBeInTheDocument();

    await userEvent.selectOptions(dropdown[0], "Author content 2");

    expect(screen.queryByText("First post")).not.toBeInTheDocument();
    expect(screen.queryByText("Second post")).toBeInTheDocument();

    await userEvent.selectOptions(dropdown[0], "blogListAuthors.all");

    await userEvent.selectOptions(dropdown[1], "Tags content");

    expect(screen.queryByText("First post")).toBeInTheDocument();
    expect(screen.queryByText("Second post")).not.toBeInTheDocument();

    await userEvent.selectOptions(dropdown[1], "Tags content 2");

    expect(screen.queryByText("First post")).not.toBeInTheDocument();
    expect(screen.queryByText("Second post")).toBeInTheDocument();

    await userEvent.selectOptions(dropdown[1], "blogListTags.all");

    await userEvent.selectOptions(dropdown[2], "BlogListDatePeriods.lastMonth");

    expect(screen.queryByText("blog.empty")).toBeInTheDocument();

    await userEvent.selectOptions(dropdown[2], "blogListDatePeriods.all");

    const searchInput = screen.getAllByRole("textbox");

    expect(searchInput).toHaveLength(1);

    await userEvent.type(searchInput[0], "First");

    expect(screen.queryByText("First post")).toBeInTheDocument();
    expect(screen.queryByText("Second post")).not.toBeInTheDocument();

    await userEvent.clear(searchInput[0]);

    await userEvent.click(
      screen.getByRole("button", {
        name: "Category content",
      }),
    );

    expect(screen.queryByText("First post")).toBeInTheDocument();
    expect(screen.queryByText("Second post")).not.toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", {
        name: "Category content 2",
      }),
    );

    expect(screen.queryByText("First post")).not.toBeInTheDocument();
    expect(screen.queryByText("Second post")).toBeInTheDocument();
  });
});
