import { graphql, useStaticQuery } from "gatsby";
import i18next from "i18next";
import React from "react";

import { AirsLink } from "../../../components/AirsLink";
import { Container } from "../../../components/Container";
import { Grid } from "../../../components/Grid";
import { PresentationCard } from "../../../components/PresentationCard";
import { filterPostsByLanguage } from "../../../utils/filter-posts";
import { capitalizePlainString, stringToUri } from "../../../utils/utilities";

const CategoriesList: React.FC = (): JSX.Element => {
  const data: IData = useStaticQuery(graphql`
    query NewCategoriesList {
      allMarkdownRemark(
        filter: {
          fields: { slug: { regex: "/blog/" } }
          frontmatter: { image: { regex: "" } }
        }
        sort: { frontmatter: { date: DESC } }
      ) {
        edges {
          node {
            fields {
              slug
            }
            frontmatter {
              category
            }
          }
        }
      }
    }
  `);

  const currentLanguage = i18next.language;

  const selectedLanguageEdges = filterPostsByLanguage(
    data.allMarkdownRemark.edges,
  );

  const categoriesListRaw = selectedLanguageEdges.map((edge): string =>
    edge.node.frontmatter.category.toLowerCase().normalize("NFD"),
  );

  const categoriesList = categoriesListRaw.filter(
    (category, index): boolean => categoriesListRaw.indexOf(category) === index,
  );

  return (
    <Container ph={4} pv={5}>
      <Container center={true} maxWidth={"1000px"}>
        <Grid columns={3} columnsMd={2} columnsSm={1} gap={"1rem"}>
          {categoriesList.map((category): JSX.Element => {
            const categoryUri = stringToUri(category);

            return (
              <AirsLink
                decoration={"none"}
                href={`${categoryUri}/`}
                key={category}
              >
                <PresentationCard
                  image={
                    currentLanguage === "en"
                      ? `blogs/categories/${categoryUri}`
                      : `es/blogs/categorias/${categoryUri}`
                  }
                  text={capitalizePlainString(category)}
                />
              </AirsLink>
            );
          })}
        </Grid>
      </Container>
    </Container>
  );
};

export { CategoriesList };
