import { render, screen } from "@testing-library/react";
import { useStaticQuery } from "gatsby";
import React from "react";

import { Cta } from "./Cta";
import { Header } from "./Header";
import { TagsList } from "./TagsList";

import { TagsPage } from ".";
import { allCloudinaryMediaData } from "test-utils/mocks";
import type { ICloudinaryMedia } from "utils/types";

interface IDataTagsPageMock {
  allCloudinaryMedia: ICloudinaryMedia;
  allMarkdownRemark: {
    edges: [
      {
        node: {
          fields: {
            slug: string;
          };
          frontmatter: {
            tags: string;
          };
        };
      },
    ];
  };
}

const mockUseStaticQuery = jest.spyOn({ useStaticQuery }, `useStaticQuery`);
const mockDataUseStaticQuery: IDataTagsPageMock = {
  allCloudinaryMedia: allCloudinaryMediaData,
  allMarkdownRemark: {
    edges: [
      {
        node: {
          fields: {
            slug: "",
          },
          frontmatter: {
            tags: "éxitos,tag2,tag3",
          },
        },
      },
    ],
  },
};

describe("TagsPage", (): void => {
  beforeEach((): void => {
    mockUseStaticQuery.mockImplementation(
      (): IDataTagsPageMock => mockDataUseStaticQuery,
    );
  });

  afterEach((): void => {
    jest.restoreAllMocks();
  });
  it("render correctly TagsPage", (): void => {
    render(<TagsPage />);
    expect(screen.getByText("Éxitos,tag2,tag3")).toBeInTheDocument();
    expect(screen.getByText("blog.tags.title")).toBeInTheDocument();
  });

  it("should return mocked data", (): void => {
    expect.hasAssertions();

    render(<TagsList />);

    expect(screen.getByText("Éxitos,tag2,tag3")).toBeInTheDocument();
  });

  it("TagsList renders", (): void => {
    expect.hasAssertions();

    render(<TagsList />);

    expect(screen.getByRole("link")).toBeInTheDocument();
  });

  it("Header renders", (): void => {
    expect.hasAssertions();

    render(<Header />);

    expect(screen.getByRole("heading")).toBeInTheDocument();
  });

  it("Cta renders", (): void => {
    expect.hasAssertions();

    render(<Cta />);

    expect(screen.getByText("Try for free")).toBeInTheDocument();
  });
});
