# shellcheck shell=bash

function main {
  export INTEGRATES_API_TOKEN="test"
  export PYTHONPATH

  PYTHONPATH="${PWD}/melts:${PYTHONPATH}"
  local pytest_args=(
    --cov 'src'
    --cov-branch
    --cov-report 'term'
    --cov-report 'xml:coverage.xml'
    --disable-warnings
    --no-cov-on-fail
    -vvl
  )

  pushd melts || return 1
  pytest "${pytest_args[@]}"
}

main "${@}"
