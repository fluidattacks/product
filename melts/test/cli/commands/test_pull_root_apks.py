from io import (
    BytesIO,
)
from unittest.mock import (
    AsyncMock,
    MagicMock,
    mock_open,
    patch,
)

import pytest
from aiohttp import (
    ClientTimeout,
)

from src.cli.commands.pull_root_apks import (
    download_apk_file,
    pull_root_apks,
)
from test.conftest import (
    SyncCliRunner,
)


@patch("aiohttp.client.ClientSession.get", new_callable=MagicMock)
@patch(
    "src.cli.commands.pull_root_apks.get_resource_download_url",
    new_callable=AsyncMock,
    return_value="https://aws.amazon.com/123456",
)
@pytest.mark.asyncio
async def test_download_apk_file(
    mock_get_download_url: AsyncMock,
    mock_client_get: MagicMock,
) -> None:
    mock_get_response = MagicMock()
    mock_get_response.status = 200
    mock_content = BytesIO(b"apk_file")
    mock_get_response.content.read = AsyncMock(side_effect=mock_content.read)

    mock_client_get.return_value.__aenter__.return_value = mock_get_response

    mock_open_func = mock_open()
    with patch("builtins.open", mock_open_func):
        result = await download_apk_file(
            group_name="example_group",
            apk_file_name="test.apk",
            destination_path="groups/example_group/test_root/fa_apks/test.apk",
        )

    mock_get_download_url.assert_awaited_once()
    mock_client_get.assert_called_once_with(
        "https://aws.amazon.com/123456", timeout=ClientTimeout(total=120)
    )
    mock_open_func.assert_called_once_with("groups/example_group/test_root/fa_apks/test.apk", "wb")
    mock_open_func.return_value.write.assert_called_once_with(b"apk_file")
    assert result


@patch("src.cli.commands.pull_root_apks.LOGGER.error", new_callable=MagicMock)
@patch(
    "src.cli.commands.pull_root_apks.get_resource_download_url",
    new_callable=AsyncMock,
    return_value=None,
)
@pytest.mark.asyncio
async def test_download_apk_file_failed(
    mock_get_download_url: AsyncMock,
    mock_logger: MagicMock,
) -> None:
    result = await download_apk_file(
        group_name="example_group",
        apk_file_name="test.apk",
        destination_path="groups/example_group/test_root/fa_apks/test.apk",
    )

    mock_get_download_url.assert_awaited_once()
    mock_logger.assert_called_once_with(
        "Unable to download APK File %s",
        "test.apk",
    )
    assert not result


@patch("src.cli.commands.pull_root_apks.LOGGER.info", new_callable=MagicMock)
@patch(
    "src.cli.commands.pull_root_apks.download_apk_file",
    new_callable=AsyncMock,
    return_value=("groups/example_group/root_nickname/fa_apks_to_analyze/test.apk"),
)
@patch(
    "src.cli.commands.pull_root_apks.get_group_git_root_env_apks",
    new_callable=AsyncMock,
    return_value={"test.apk"},
)
def test_pull_root_apks(
    mock_api_response: AsyncMock,
    mock_download_file: AsyncMock,
    mock_logger: MagicMock,
    runner: SyncCliRunner,
) -> None:
    result = runner.invoke(pull_root_apks, ["--group", "example_group", "--root", "root_nickname"])
    assert not result.exception
    mock_api_response.assert_awaited_once()
    mock_download_file.assert_awaited_once()
    call_args = mock_download_file.call_args
    assert call_args.kwargs["group_name"] == "example_group"
    assert call_args.kwargs["apk_file_name"] == "test.apk"
    assert call_args.kwargs["destination_path"].endswith(
        "groups/example_group/root_nickname/fa_apks_to_analyze/test.apk"
    )
    assert result.output == ""
    mock_logger.assert_called_once_with(
        "Downloaded APK file in %s",
        "groups/example_group/root_nickname/fa_apks_to_analyze/test.apk",
    )


@patch("src.cli.commands.pull_root_apks.LOGGER.error", new_callable=MagicMock)
@patch(
    "src.cli.commands.pull_root_apks.get_group_git_root_env_apks",
    new_callable=AsyncMock,
    return_value=None,
)
def test_pull_root_apks_failed(
    mock_api_response: AsyncMock,
    mock_logger: MagicMock,
    runner: SyncCliRunner,
) -> None:
    result = runner.invoke(pull_root_apks, ["--group", "example_group", "--root", "root_nickname"])
    assert not result.exception
    mock_api_response.assert_awaited_once()
    assert result.output == ""
    mock_logger.assert_called_once_with(
        "Cannot find any APK environments for root %s",
        "root_nickname",
    )
