from datetime import (
    UTC,
    datetime,
    timedelta,
)
from typing import (
    Any,
)
from unittest.mock import (
    ANY,
    AsyncMock,
    MagicMock,
    patch,
)

import pytest
from more_itertools import (
    mark_ends,
)

from src.cli.commands.pull_repos import (
    _delete_file_repo,
    _repo_already_exists,
    calculate_days_ago,
    download_repo,
    notify_out_of_scope,
    pull_repos,
)
from test.conftest import (
    SyncCliRunner,
)


def test_calculate_days_ago() -> None:
    past_date = datetime.now(UTC) - timedelta(days=3)

    result = calculate_days_ago(past_date)

    assert result == 3


@pytest.mark.parametrize("gitignore", ["*.log", "*.tmp", ""])
def test_notify_out_of_scope(caplog: Any, gitignore: Any) -> None:
    with caplog.at_level("INFO"):
        notify_out_of_scope("fake_nickname", gitignore)

    assert "Please remember the scope for : fake_nickname" in caplog.text
    if gitignore:
        expected_logs = [f"    - {line}" for _, is_last, line in mark_ends(gitignore)]
        assert all(log in caplog.text for log in expected_logs)
    else:
        assert "    - " not in caplog.text


def test_delete_file_repo() -> None:
    with patch("os.path.isfile") as mock_os_path_isfile:
        with patch("os.remove") as mock_os_remove:
            mock_os_path_isfile.return_value = True
            file_path = "/path/to/fake_repo/.git/config"

            _delete_file_repo(file_path)
        mock_os_remove.assert_called_once_with("/path/to/fake_repo/.git/config")


def test_repo_already_exists() -> None:
    with patch("src.cli.commands.pull_repos.get_head_commit") as mock_repo_hash:
        mock_repo_hash.return_value = "00000"
        file_path = "/path/to/fake_repo/.git/config"

        result = _repo_already_exists(
            {
                "cloningStatus": {
                    "commit": "00000",
                    "modifiedDate": "2020-01-01T00:00:00.000+00:00",
                },
                "branch": "dev",
                "nickname": "dev",
            },
            file_path,
        )
        assert result

        result = _repo_already_exists(
            {
                "cloningStatus": {
                    "commit": "0001",
                    "modifiedDate": "2020-01-01T00:00:00.000+00:00",
                },
                "branch": "dev",
                "nickname": "dev",
            },
            file_path,
        )
        assert not result


@patch("aiohttp.client.ClientSession.post", new_callable=MagicMock)
@patch(
    "src.cli.commands.pull_repos.download_repo_from_s3",
    new_callable=AsyncMock,
    return_value=True,
)
@pytest.mark.asyncio
async def test_download_repo_false(
    mock_download_repo: AsyncMock,
    mock_client_post: MagicMock,
) -> None:
    mock_client_post.return_value.__aenter__.return_value.status = 200
    mock_client_post.return_value.__aenter__.return_value.json.return_value = {
        "data": {
            "root": {
                "id": "root_id",
                "nickname": "root_nickname",
                "downloadUrl": "https://example.com/download",
            }
        }
    }
    mock_progress_bar = MagicMock()

    result = await download_repo(
        group_name="example_group",
        root={"id": "1", "nickname": "root1", "gitignore": "gitignore"},
        progress_bar=mock_progress_bar,
    )
    mock_download_repo.assert_awaited_once()
    assert result

    mock_client_post.return_value.__aenter__.return_value.json.return_value = {
        "data": {
            "root": {
                "id": "root_id",
                "nickname": "root_nickname",
                "downloadUrl": None,
            }
        }
    }

    _result = await download_repo(
        group_name="example_group",
        root={"id": "1", "nickname": "root1", "gitignore": "gitignore"},
        progress_bar=mock_progress_bar,
    )
    mock_download_repo.assert_awaited_once()
    assert not _result


@patch("aiohttp.client.ClientSession.post", new_callable=MagicMock)
@patch(
    "src.cli.commands.pull_repos.download_repo_from_s3",
    new_callable=AsyncMock,
    return_value=True,
)
@patch(
    "src.cli.commands.pull_repos._repo_already_exists",
    new_callable=MagicMock,
    return_value=True,
)
@pytest.mark.asyncio
async def test_download_repo_exists(
    mock_repo_already_exists: MagicMock,
    mock_download_repo_from_s3: AsyncMock,
    mock_client_post: MagicMock,
) -> None:
    mock_progress_bar = MagicMock()

    result = await download_repo(
        group_name="example_group",
        root={"id": "1", "nickname": "root1", "gitignore": "gitignore"},
        progress_bar=mock_progress_bar,
    )
    mock_client_post.assert_called_once()
    mock_download_repo_from_s3.assert_not_called()
    mock_repo_already_exists.assert_called_once()
    assert result


@patch("aiohttp.client.ClientSession.post", new_callable=MagicMock)
@patch(
    "src.cli.commands.pull_repos._repo_already_exists",
    new_callable=MagicMock,
    return_value=True,
)
def test_pull_repos_all_roots(
    mock_repo_already_exists: MagicMock,
    mock_client_post: MagicMock,
    runner: SyncCliRunner,
) -> None:
    mock_client_post.return_value.__aenter__.return_value.status = 200
    mock_client_post.return_value.__aenter__.return_value.json.return_value = {
        "data": {
            "group": {
                "gitRoots": {
                    "edges": [
                        {
                            "node": {
                                "id": "1",
                                "branch": "dev",
                                "nickname": "root1",
                                "state": "ACTIVE",
                                "gitignore": ["*.log"],
                                "url": "https://example.com/_git/repo",
                                "cloningStatus": {
                                    "commit": "0000",
                                    "modifiedDate": "2020-01-01T00:00:00",
                                },
                            }
                        },
                    ],
                    "pageInfo": {
                        "hasNextPage": False,
                        "endCursor": "ROOT#123456#GROUP#example_group",
                    },
                },
            },
            "root": {
                "id": "1",
                "nickname": "root1",
                "downloadUrl": "https://example.com/download",
            },
        }
    }

    result = runner.invoke(pull_repos, ["--group", "example_group"])
    assert not result.exception
    mock_repo_already_exists.assert_called()
    assert result.output == ""


@patch("aiohttp.client.ClientSession.post", new_callable=MagicMock)
@patch("src.cli.commands.pull_repos.LOGGER.error", new_callable=MagicMock)
def test_pull_repos_not_uploaded(
    mock_logger: MagicMock,
    mock_client_post: MagicMock,
    runner: SyncCliRunner,
) -> None:
    mock_client_post.return_value.__aenter__.return_value.status = 200
    mock_client_post.return_value.__aenter__.return_value.json.return_value = {
        "data": {
            "group": {
                "gitRoots": {
                    "edges": [
                        {
                            "node": {
                                "id": "1",
                                "branch": "dev",
                                "nickname": "root1",
                                "state": "ACTIVE",
                                "gitignore": ["*.log"],
                                "url": "https://example.com/_git/repo",
                            }
                        },
                    ],
                    "pageInfo": {
                        "hasNextPage": False,
                        "endCursor": "ROOT#123456#GROUP#example_group",
                    },
                },
            },
            "root": {
                "id": "1",
                "nickname": "root1",
                "downloadUrl": None,
            },
        }
    }

    result = runner.invoke(pull_repos, ["--group", "example_group"])
    mock_logger.assert_called_once_with("Cannot find download url for %s repository", ANY)
    assert not result.exception
    assert result.output == ""


@patch("aiohttp.client.ClientSession.post", new_callable=MagicMock)
@patch(
    "src.cli.commands.pull_repos._repo_already_exists",
    new_callable=MagicMock,
    return_value=True,
)
def test_pull_repos_nickname(
    mock_repo_already_exists: MagicMock,
    mock_client_post: MagicMock,
    runner: SyncCliRunner,
) -> None:
    mock_client_post.return_value.__aenter__.return_value.status = 200
    mock_client_post.return_value.__aenter__.return_value.json.return_value = {
        "data": {
            "group": {
                "gitRoots": {
                    "edges": [
                        {
                            "node": {
                                "branch": "dev",
                                "cloningStatus": {
                                    "commit": "0000",
                                    "modifiedDate": "2024-04-30T18:34:26",
                                },
                                "gitignore": [
                                    "test/",
                                ],
                                "id": "123456",
                                "nickname": "root1",
                                "state": "ACTIVE",
                                "url": "ssh://git@gitlab.com:fluidattacks",
                                "downloadUrl": "https://s3.amazonaws.com/",
                            }
                        }
                    ]
                }
            }
        }
    }

    result = runner.invoke(pull_repos, ["--group", "example_group", "--root", "root1"])
    mock_repo_already_exists.assert_called()
    assert not result.exception
    assert result.output == ""


@patch(
    "src.cli.commands.pull_repos.get_group_git_root",
    new_callable=AsyncMock,
    return_value=None,
)
@patch("src.cli.commands.pull_repos.LOGGER.error", new_callable=MagicMock)
def test_pull_repos_nickname_failed(
    mock_logger_error: MagicMock,
    mock_client_post: AsyncMock,
    runner: SyncCliRunner,
) -> None:
    result = runner.invoke(pull_repos, ["--group", "example_group", "--root", "test_root"])
    assert result
    mock_client_post.assert_awaited_once()
    mock_logger_error.assert_called_once_with("Cannot find download url for root %s", "test_root")
    assert result.output == ""
