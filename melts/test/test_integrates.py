from datetime import (
    UTC,
    datetime,
)
from unittest.mock import (
    ANY,
    AsyncMock,
    MagicMock,
    patch,
)

import pytest
from aioresponses import (
    aioresponses,
)

from src.api.integrates import (
    get_git_root_credentials,
    get_git_root_download_url,
    get_git_root_upload_url,
    get_group_git_root,
    get_group_git_root_env_apks,
    get_group_git_roots,
    get_group_git_roots_paginated,
    get_resource_download_url,
    make_graphql_request,
    update_git_root_cloning_status,
)
from src.utils.exceptions import (
    InvalidIntegratesAPIToken,
)


@patch(
    "aiohttp.client.ClientSession.post",
    new_callable=MagicMock,
)
@pytest.mark.asyncio
async def test_make_graphql_request(
    mock_client_post: MagicMock,
) -> None:
    mock_client_post.return_value.__aenter__.return_value.status = 200
    mock_client_post.return_value.__aenter__.return_value.json.return_value = {
        "data": "data",
    }

    result = await make_graphql_request("https://example.com", "query", {}, max_retries=3)
    assert result == {"data": "data"}


@pytest.mark.asyncio
async def test_make_graphql_request_429() -> None:
    url = "http://example.com/graphql"
    query = "your_graphql_query"
    variables = {"key": "value"}

    with aioresponses() as mock:
        mock.post(url, status=429, headers={"Retry-After": "1"})

        with pytest.raises(Exception, match="Failed to make GraphQL request after 2 retries."):
            await make_graphql_request(url, query, variables, max_retries=2)


@patch("aiohttp.client.ClientSession.post", new_callable=MagicMock)
@pytest.mark.asyncio
async def test_get_group_git_roots(
    mock_client_post: MagicMock,
) -> None:
    mock_client_post.return_value.__aenter__.return_value.status = 200
    mock_client_post.return_value.__aenter__.return_value.json.return_value = {
        "data": {
            "group": {
                "roots": [
                    {"id": "1", "branch": "master", "nickname": "root1"},
                    {"id": "2", "branch": "dev", "nickname": "root2"},
                ]
            }
        }
    }

    result = await get_group_git_roots("example_group")

    assert result == [
        {"id": "1", "branch": "master", "nickname": "root1"},
        {"id": "2", "branch": "dev", "nickname": "root2"},
    ]


@patch("aiohttp.client.ClientSession.post", new_callable=MagicMock)
@patch("src.cli.commands.clone.LOGGER.error", new_callable=MagicMock)
@pytest.mark.asyncio
async def test_get_group_git_roots_invalid_token(
    mock_logger_error: MagicMock,
    mock_client_post: MagicMock,
) -> None:
    mock_client_post.return_value.__aenter__.return_value.status = 200
    mock_client_post.return_value.__aenter__.return_value.json.return_value = {
        "data": None,
        "errors": [
            {
                "message": "Login required",
                "locations": [{"line": 3, "column": 17}],
                "path": ["group"],
            }
        ],
    }
    with pytest.raises(InvalidIntegratesAPIToken):
        await get_group_git_roots("example_group")

    mock_logger_error.assert_called_once_with(
        "Login required - Integrates API token is expired or broken"
    )


@patch("aiohttp.client.ClientSession.post", new_callable=MagicMock)
@pytest.mark.asyncio
async def test_get_git_root_download_url(mock_client_post: MagicMock) -> None:
    mock_client_post.return_value.__aenter__.return_value.status = 200
    mock_client_post.return_value.__aenter__.return_value.json.return_value = {
        "data": {
            "root": {
                "id": "root_id",
                "nickname": "root_nickname",
                "downloadUrl": "https://example.com/download",
            }
        }
    }

    result = await get_git_root_download_url("example_group", "root_id")

    assert result == "https://example.com/download"

    mock_client_post.return_value.__aenter__.return_value.status = 200
    mock_client_post.return_value.__aenter__.return_value.json.return_value = {
        "data": {
            "root": {
                "uploadUrl": "https://example.com/download",
                "cloningStatus": {
                    "hasMirrorInS3": True,
                },
            }
        }
    }

    result_upload = await get_git_root_upload_url("example_group", "root_id")

    assert result_upload == ("https://example.com/download", True)


@patch("aiohttp.client.ClientSession.post", new_callable=MagicMock)
@pytest.mark.asyncio
async def test_get_git_root_empty(mock_client_post: MagicMock) -> None:
    mock_client_post.return_value.__aenter__.return_value.status = 200
    mock_client_post.return_value.__aenter__.return_value.json.return_value = {
        "data": {"root": None, "group": None}
    }

    result = await get_git_root_download_url("example_group", "root_id")

    assert not result

    result_upload = await get_git_root_upload_url("example_group", "root_id")

    assert result_upload == (None, None)

    result_creden = await get_git_root_credentials("example_group", "root_id")

    assert not result_creden

    result_groups = await get_group_git_roots("example_group")

    assert result_groups == []


@patch("aiohttp.client.ClientSession.post", new_callable=MagicMock)
@pytest.mark.asyncio
async def test_get_git_root_credentials(mock_client_post: MagicMock) -> None:
    mock_client_post.return_value.__aenter__.return_value.status = 200
    mock_client_post.return_value.__aenter__.return_value.json.return_value = {
        "data": {
            "root": {
                "id": "root_id",
                "nickname": "root_nickname",
                "credentials": {
                    "oauthType": "oauth",
                    "user": "user123",
                    "password": "password123",
                    "isPat": False,
                    "key": None,
                    "token": "token123",
                    "type": "type123",
                },
            }
        }
    }

    result = await get_git_root_credentials("example_group", "root_id")

    assert result == {
        "oauthType": "oauth",
        "user": "user123",
        "password": "password123",
        "isPat": False,
        "key": None,
        "token": "token123",
        "type": "type123",
    }


@patch("aiohttp.client.ClientSession.post", new_callable=MagicMock)
@pytest.mark.asyncio
async def test_update_git_root_cloning_status(
    mock_client_post: MagicMock,
) -> None:
    mock_client_post.return_value.__aenter__.return_value.status = 200
    mock_client_post.return_value.__aenter__.return_value.json.return_value = {
        "data": {
            "updateRootCloningStatus": {"success": True},
            "root": {
                "id": "root_id",
                "nickname": "root_nickname",
                "credentials": {
                    "oauthType": "oauth",
                    "user": "user123",
                    "password": "password123",
                    "isPat": False,
                    "key": None,
                    "token": "token123",
                    "type": "type123",
                },
            },
        }
    }

    await update_git_root_cloning_status(
        group_name="example_group",
        git_root_id="root_id",
        status="success",
        commit="commit_hash",
        commit_date=datetime.now(UTC),
        message="Test message",
    )

    mock_client_post.assert_called_once()


@patch("aiohttp.client.ClientSession.post", new_callable=MagicMock)
@patch("src.api.integrates.LOGGER.warning", new_callable=MagicMock)
@pytest.mark.asyncio
async def test_get_group_git_root(
    mock_logger_error: MagicMock,
    mock_client_post: MagicMock,
) -> None:
    mock_client_post.return_value.__aenter__.return_value.status = 200
    mock_client_post.return_value.__aenter__.return_value.json.return_value = {
        "data": {
            "group": {
                "gitRoots": {
                    "edges": [
                        {
                            "node": {
                                "branch": "qa",
                                "cloningStatus": {
                                    "commit": "0000",
                                    "modifiedDate": "2024-04-29T11:13:21",
                                },
                                "gitignore": [],
                                "id": "123456",
                                "nickname": "test",
                                "state": "ACTIVE",
                                "url": "https://test.com",
                                "downloadUrl": "https://example.com/test.git",
                            }
                        },
                        {
                            "node": {
                                "branch": "dev",
                                "cloningStatus": {
                                    "commit": "0000",
                                    "modifiedDate": "2024-04-29T11:13:21",
                                },
                                "gitignore": [],
                                "id": "789123",
                                "nickname": "test-for-qa",
                                "state": "ACTIVE",
                                "url": "https://prod.com",
                                "downloadUrl": "https://example.com/repo.git",
                            }
                        },
                    ],
                }
            }
        }
    }

    result = await get_group_git_root(
        group_name="example_group",
        git_root_nickname="test",
    )

    mock_client_post.assert_called_once()
    mock_logger_error.assert_not_called()
    assert result == {
        "branch": "qa",
        "cloningStatus": {
            "commit": "0000",
            "modifiedDate": "2024-04-29T11:13:21",
        },
        "gitignore": [],
        "id": "123456",
        "nickname": "test",
        "state": "ACTIVE",
        "url": "https://test.com",
        "downloadUrl": "https://example.com/test.git",
    }


@patch("aiohttp.client.ClientSession.post", new_callable=MagicMock)
@patch("src.api.integrates.LOGGER.warning", new_callable=MagicMock)
@pytest.mark.asyncio
async def test_get_group_git_root_failed(
    mock_logger_error: MagicMock,
    mock_client_post: MagicMock,
) -> None:
    mock_client_post.return_value.__aenter__.return_value.status = 200
    mock_client_post.return_value.__aenter__.return_value.json.return_value = {
        "data": None,
        "errors": [
            {
                "message": "Access denied or group not found",
                "locations": [{"line": 2, "column": 5}],
                "path": ["group"],
            }
        ],
    }

    await get_group_git_root(
        group_name="example_group",
        git_root_nickname="test",
    )

    mock_client_post.assert_called_once()
    mock_logger_error.assert_called_once_with(
        "The group %s does not have a git root with nickname %s", ANY, ANY
    )


@patch("aiohttp.client.ClientSession.post", new_callable=MagicMock)
@patch("src.api.integrates.LOGGER.warning", new_callable=MagicMock)
@pytest.mark.asyncio
async def test_get_group_git_roots_paginated(
    mock_logger_warning: MagicMock,
    mock_client_post: MagicMock,
) -> None:
    mock_client_post.return_value.__aenter__.return_value.status = 200
    mock_client_post.return_value.__aenter__.return_value.json.return_value = {
        "data": {
            "group": {
                "gitRoots": {
                    "edges": [
                        {
                            "node": {
                                "branch": "qa",
                                "cloningStatus": {
                                    "commit": "0000",
                                    "modifiedDate": "2024-04-29T11:13:21",
                                },
                                "gitignore": [],
                                "id": "123456",
                                "nickname": "test",
                                "state": "ACTIVE",
                                "url": "https://test.com",
                            }
                        },
                        {
                            "node": {
                                "branch": "dev",
                                "cloningStatus": {
                                    "commit": "0000",
                                    "modifiedDate": "2024-04-29T11:13:21",
                                },
                                "gitignore": [],
                                "id": "789123",
                                "nickname": "prod",
                                "state": "ACTIVE",
                                "url": "https://prod.com",
                            }
                        },
                    ],
                    "pageInfo": {
                        "hasNextPage": False,
                        "endCursor": "ROOT#789123#GROUP#example_group",
                    },
                }
            }
        }
    }

    result = await get_group_git_roots_paginated(
        "example_group",
    )

    mock_client_post.assert_called_once()
    mock_logger_warning.assert_not_called()
    assert result == [
        {
            "branch": "qa",
            "cloningStatus": {
                "commit": "0000",
                "modifiedDate": "2024-04-29T11:13:21",
            },
            "gitignore": [],
            "id": "123456",
            "nickname": "test",
            "state": "ACTIVE",
            "url": "https://test.com",
        },
        {
            "branch": "dev",
            "cloningStatus": {
                "commit": "0000",
                "modifiedDate": "2024-04-29T11:13:21",
            },
            "gitignore": [],
            "id": "789123",
            "nickname": "prod",
            "state": "ACTIVE",
            "url": "https://prod.com",
        },
    ]


@patch("aiohttp.client.ClientSession.post", new_callable=MagicMock)
@patch("src.api.integrates.LOGGER.warning", new_callable=MagicMock)
@pytest.mark.asyncio
async def test_get_group_git_roots_paginated_failed(
    mock_logger_warning: MagicMock,
    mock_client_post: MagicMock,
) -> None:
    mock_client_post.return_value.__aenter__.return_value.status = 200
    mock_client_post.return_value.__aenter__.return_value.json.return_value = {
        "data": None,
        "errors": [
            {
                "message": "Access denied or group not found",
                "locations": [{"line": 2, "column": 5}],
                "path": ["group"],
            }
        ],
    }

    result = await get_group_git_roots_paginated(
        "group_that_does_not_exist",
    )

    mock_client_post.assert_called_once()
    mock_logger_warning.assert_called_once_with(
        "The group %s does not exist or it does not have any Git Roots",
        "group_that_does_not_exist",
    )
    assert result == []


@patch(
    "src.api.integrates.make_graphql_request",
    new_callable=AsyncMock,
    return_value={
        "data": {
            "group": {
                "gitRoots": {
                    "edges": [
                        {
                            "node": {
                                "gitEnvironmentUrls": [],
                                "id": "urlid1",
                                "nickname": "test_root_git",
                            }
                        },
                        {
                            "node": {
                                "gitEnvironmentUrls": [
                                    {
                                        "url": "test.apk",
                                        "id": "789123",
                                        "include": True,
                                        "urlType": "APK",
                                    },
                                    {
                                        "url": "test_2.apk",
                                        "id": "456238",
                                        "include": True,
                                        "urlType": "APK",
                                    },
                                    {
                                        "url": "https://fa.com/",
                                        "id": "123456",
                                        "include": True,
                                        "urlType": "URL",
                                    },
                                ],
                                "id": "urlid2",
                                "nickname": "test_root",
                            }
                        },
                    ]
                }
            }
        }
    },
)
@patch("src.api.integrates.LOGGER.warning", new_callable=MagicMock)
@pytest.mark.asyncio
async def test_get_group_git_root_env_apks(
    mock_logger_warning: MagicMock,
    mock_api_request: AsyncMock,
) -> None:
    result = await get_group_git_root_env_apks(
        "example_group",
        "test_root",
    )

    mock_api_request.assert_awaited_once()
    mock_logger_warning.assert_not_called()
    assert result == {"test.apk", "test_2.apk"}


@patch(
    "src.api.integrates.make_graphql_request",
    new_callable=AsyncMock,
    return_value={"data": {"group": {"gitRoots": {"edges": []}}}},
)
@patch("src.api.integrates.LOGGER.warning", new_callable=MagicMock)
@pytest.mark.asyncio
async def test_get_group_git_root_env_apks_failed(
    mock_logger_warning: MagicMock,
    mock_api_request: AsyncMock,
) -> None:
    result = await get_group_git_root_env_apks(
        "example_group",
        "root_that_does_not_exist",
    )

    mock_api_request.assert_awaited_once()
    mock_logger_warning.assert_called_with(
        "The group %s does not have a git root with nickname %s",
        "example_group",
        "root_that_does_not_exist",
    )
    assert result is None


@patch(
    "src.api.integrates.make_graphql_request",
    new_callable=AsyncMock,
    return_value={
        "data": {
            "downloadFile": {
                "url": (
                    "https://integrates.s3.amazonaws.com/resources/"
                    "example_group/test.apk?wncowefewhf"
                ),
            }
        }
    },
)
@patch("src.api.integrates.LOGGER.warning", new_callable=MagicMock)
@pytest.mark.asyncio
async def test_get_resource_download_url(
    mock_logger_warning: MagicMock,
    mock_api_request: AsyncMock,
) -> None:
    result = await get_resource_download_url(
        "example_group",
        "test.apk",
    )

    mock_api_request.assert_awaited_once()
    mock_logger_warning.assert_not_called()
    assert result == (
        "https://integrates.s3.amazonaws.com/resources/example_group/test.apk?wncowefewhf"
    )


@patch(
    "src.api.integrates.make_graphql_request",
    new_callable=AsyncMock,
    return_value={
        "data": None,
        "errors": [
            {
                "message": "Access denied",
                "locations": [{"line": 2, "column": 3}],
                "path": ["downloadFile"],
            }
        ],
    },
)
@patch("src.api.integrates.LOGGER.warning", new_callable=MagicMock)
@pytest.mark.asyncio
async def test_get_resource_download_url_failed(
    mock_logger_warning: MagicMock,
    mock_api_request: AsyncMock,
) -> None:
    result = await get_resource_download_url(
        "group_that_does_not_exist",
        "some_file.apk",
    )

    mock_api_request.assert_awaited_once()
    mock_logger_warning.assert_called_with(
        "Failed to fetch download url for file %s in group %s",
        "some_file.apk",
        "group_that_does_not_exist",
    )
    assert result is None
