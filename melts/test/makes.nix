{ makeScript, outputs, projectPath, ... }: {
  jobs."/melts/test" = makeScript {
    name = "melts-test";
    searchPaths = { source = [ outputs."/melts/config/runtime" ]; };
    entrypoint = ./entrypoint.sh;
  };
}
