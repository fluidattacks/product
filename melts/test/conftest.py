import os
import tempfile
from collections.abc import (
    Generator,
)
from typing import (
    Any,
)

import anyio
import pytest
from asyncclick.testing import (
    CliRunner,
)


class SyncCliRunner(CliRunner):
    def invoke(
        self: "SyncCliRunner",
        *args: Any,
        _sync: bool = False,
        **kwargs: Any,
    ) -> Any:
        main_function = super().invoke
        if _sync:
            return main_function(*args, **kwargs)

        async def response() -> Any:
            return await main_function(*args, **kwargs)

        result = anyio.run(response)
        return result


@pytest.fixture(scope="function")
def runner() -> SyncCliRunner:
    return SyncCliRunner()


@pytest.fixture(scope="function")
def clean_working_dir() -> Generator[str, None, None]:
    current_dir = os.getcwd()
    try:
        with tempfile.TemporaryDirectory() as temp_dir:
            os.chdir(temp_dir)
            yield temp_dir
    finally:
        os.chdir(current_dir)


@pytest.fixture(scope="function")
def groups_working_dir() -> Generator[str, None, None]:
    current_dir = os.getcwd()
    try:
        with tempfile.TemporaryDirectory() as temp_dir:
            os.chdir(temp_dir)
            os.makedirs("groups")
            yield temp_dir
    finally:
        os.chdir(current_dir)
