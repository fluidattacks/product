{ makeScript, outputs, ... }: {
  imports = [
    ./config/runtime/makes.nix
    ./coverage/makes.nix
    ./dev/makes.nix
    ./lib/makes.nix
    ./pipeline/makes.nix
    ./test/makes.nix
  ];
  jobs = {
    "/melts" = makeScript {
      name = "melts";
      searchPaths.source = [ outputs."/melts/config/runtime" ];
      entrypoint = ./entrypoint.sh;
    };
    "/melts/lint" = makeScript {
      name = "melts-lint";
      searchPaths.source = [ outputs."/melts/config/runtime" ];
      entrypoint = ''
        pushd melts

        if test -n "''${CI:-}"; then
          ruff format --config ruff.toml --diff
          ruff check --config ruff.toml
        else
          ruff format --config ruff.toml
          ruff check --config ruff.toml --fix
        fi

        mypy --config-file mypy.ini src
        mypy --config-file mypy.ini test
      '';
    };
  };
}
