# shellcheck shell=bash

function clone_services_repository {
  local group="${1}"

  : \
    && CI='true' \
      CI_COMMIT_REF_NAME='trunk' \
      melts --init pull-repos --group "${group}"
}

function get_forces_token {
  local group="${1}"

  melts misc --get-forces-token "${group}"
}
