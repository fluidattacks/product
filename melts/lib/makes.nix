{ makeTemplate, outputs, ... }: {
  jobs."/melts/lib" = makeTemplate {
    name = "melts-lib";
    searchPaths.bin = [ outputs."/melts" ];
    template = ./template.sh;
  };
}
