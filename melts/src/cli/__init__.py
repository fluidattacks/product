import asyncio
import os
import sys
from contextlib import (
    suppress,
)
from pathlib import (
    Path,
)

import asyncclick as click

from src import (
    constants,
)
from src.cli.commands.clone import (
    clone,
)
from src.cli.commands.fingerprint import (
    get_fingerprint,
)
from src.cli.commands.pull_repos import (
    pull_repos,
)
from src.cli.commands.pull_root_apks import (
    pull_root_apks,
)
from src.cli.commands.push_repos import (
    push_repos,
)
from src.logger import (
    LOGGER,
)

API_TOKEN_MSSING_MSG = """
    Environment variable INTEGRATES_API_TOKEN is not set.
    It is required to authenticate your requests against Fluid Attacks' API.
    Check the documentation on how to create one at
    https://help.fluidattacks.com/portal/en/kb/articles/learn-the-basics-of-the-fluid-attacks-api#Authentication_with_Fluid_Attacks__platform_API_Token
"""
GROUPS_DIR_MISSING_MSG = """
    "groups" directory not found in current directory %s.
    Create it manually or execute

    m gitlab:fluidattacks/universe@trunk /melts --init [COMMAND] [OPTS]
"""


@click.group()
@click.option("--init", default=False, is_flag=True)
async def main(init: bool = False) -> None:
    """Main function for the cli"""
    valid_setup = True
    if init:
        with suppress(FileExistsError):
            os.mkdir("groups")

    if constants.API_TOKEN is None:
        valid_setup = False
        LOGGER.error(API_TOKEN_MSSING_MSG)

    current_path = Path(os.getcwd())
    if "groups" not in os.listdir(current_path):
        for parent in current_path.parents:
            if parent.name == "groups":
                LOGGER.info("Moving to %s", str(parent.parent.absolute()))
                os.chdir(parent.parent)
                break
        else:
            valid_setup = False
            LOGGER.error(
                GROUPS_DIR_MISSING_MSG,
                current_path.absolute(),
            )

    if not valid_setup:
        sys.exit(1)


main.add_command(pull_repos)
main.add_command(clone)
main.add_command(get_fingerprint)
main.add_command(push_repos)
main.add_command(pull_root_apks)


if __name__ == "__main__":
    asyncio.run(main.main())
