import os
from collections.abc import Callable
from contextlib import (
    suppress,
)
from datetime import (
    UTC,
    datetime,
)
from pathlib import (
    Path,
)
from typing import (
    Any,
)

import asyncclick as click
from aioextensions import (
    collect,
)
from alive_progress import (
    alive_bar,
)
from fluidattacks_core.git import (
    download_repo_from_s3,
    get_head_commit,
)
from git import (
    GitError,
)
from more_itertools import (
    mark_ends,
)

from src.api.integrates import (
    get_git_root_download_url,
    get_group_git_root,
    get_group_git_roots_paginated,
)
from src.logger import (
    LOGGER,
)

Item = dict[str, Any]


def calculate_days_ago(date: datetime) -> int:
    """
    Return passed days after a provided date

    param: date: provided date to calculate passed days
    """
    passed_days = datetime.now(UTC) - date.replace(tzinfo=UTC)

    return passed_days.days


def notify_out_of_scope(
    nickname: str,
    gitignore: str,
) -> None:
    LOGGER.info("Please remember the scope for : %s", nickname)

    for _, is_last, line in mark_ends(gitignore):
        if is_last:
            LOGGER.info("    - %s\n", line)
        else:
            LOGGER.info("    - %s", line)


def _repo_already_exists(root: Item, repo_path: str) -> bool:
    with suppress(GitError):
        if (
            (commit := root.get("cloningStatus", {}).get("commit"))
            and (branch := root.get("branch"))
            and (local_commit := get_head_commit(Path(repo_path), branch))
            and (local_commit == commit)
        ):
            LOGGER.debug("%s repository already exists", root["nickname"])
            return True
    return False


def _delete_file_repo(file_path: str) -> None:
    if os.path.isfile(file_path):
        os.remove(file_path)


async def download_repo(
    *,
    group_name: str,
    root: Item,
    root_download_url: str | None = None,
    progress_bar: Callable[[], None] | None = None,
) -> bool:
    url = root_download_url or await get_git_root_download_url(group_name, root["id"])
    if not url:
        LOGGER.error("Cannot find download url for %s repository", root["nickname"])
        return False

    repo_path = Path("groups") / group_name / root["nickname"]
    config_file_path = repo_path / ".git" / "config"
    os.makedirs(repo_path.parent, exist_ok=True)

    if _repo_already_exists(root, str(repo_path.absolute())):
        _delete_file_repo(str(config_file_path.absolute()))
        LOGGER.debug("%s _repo_already_exists already exists", root["nickname"])
        return True

    if await download_repo_from_s3(url, repo_path, root["gitignore"]):
        LOGGER.debug("%s download_repo_from_s3 already exists", root["nickname"])
        if progress_bar:
            progress_bar()
        _delete_file_repo(str(config_file_path.absolute()))
        return True

    return False


def notify_repo_results(root: Item, result: bool) -> None:
    if not result:
        LOGGER.info(
            "root %s does not have repos uploaded to s3",
            root["nickname"],
        )
        return

    if root["gitignore"]:
        notify_out_of_scope(root["nickname"], root["gitignore"])

    if date := root.get("cloningStatus", {}).get("modifiedDate"):
        LOGGER.info(
            "Data for %s was uploaded to S3 %i days ago",
            root["nickname"],
            calculate_days_ago(datetime.fromisoformat(date)),
        )


async def download_single_repo(group_name: str, git_root_nickname: str) -> None:
    """Download a repository from the S3 mirror."""
    git_root = await get_group_git_root(group_name, git_root_nickname)

    if not git_root or not git_root["downloadUrl"]:
        LOGGER.error("Cannot find download url for root %s", git_root_nickname)
        return

    LOGGER.info("Downloading root %s from group %s", git_root_nickname, group_name)
    result = await download_repo(
        group_name=group_name,
        root=git_root,
        root_download_url=git_root["downloadUrl"],
    )

    notify_repo_results(git_root, result)


@click.command()
@click.option(
    "--group",
    "group_name",
    required=True,
    help="Specify the name of the group you want to download from.",
)
@click.option(
    "--root",
    "git_root_nickname",
    required=False,
    help=("Download a specific repository. Provide the nickname of the repository."),
)
async def pull_repos(group_name: str, git_root_nickname: str | None) -> None:
    """Download repositories from the S3 mirror."""
    if git_root_nickname:
        return await download_single_repo(group_name, git_root_nickname)

    git_roots = [
        git_root
        for git_root in await get_group_git_roots_paginated(group_name)
        if git_root.get("state") == "ACTIVE"
    ]
    with alive_bar(len(git_roots), enrich_print=False) as progress_bar:
        results: tuple[bool, ...] = await collect(
            [
                download_repo(
                    group_name=group_name,
                    root=root,
                    progress_bar=progress_bar,
                )
                for root in git_roots
            ],
            workers=4,
        )
        for root, result in zip(git_roots, results, strict=False):
            notify_repo_results(root, result)
