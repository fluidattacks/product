import os
import tarfile
import tempfile
from datetime import (
    UTC,
    datetime,
)
from uuid import (
    uuid4,
)

import aiohttp
import asyncclick as click
from aioextensions import (
    collect,
)
from git import (
    GitError,
)
from git.repo import (
    Repo,
)

from src.api.integrates import (
    get_git_root_upload_url,
    get_group_git_roots_paginated,
    update_git_root_cloning_status,
)
from src.logger import (
    LOGGER,
)


async def _upload_repository(group: str, repo_path: str, root: dict, upload_url: str) -> bool:
    uploaded = True
    with tempfile.TemporaryDirectory(
        prefix=f"integrates_clone_{group}_",
        ignore_cleanup_errors=True,
    ) as temp_dir:
        tar_path = os.path.join(temp_dir, f"{uuid4().hex}.tar.gz")
        create_git_root_tar_file(root["nickname"], repo_path, tar_path)

        LOGGER.info("Uploading repository %s...", root["nickname"])
        async with aiohttp.ClientSession(
            timeout=aiohttp.ClientTimeout(total=3600, sock_connect=30, sock_read=600)
        ) as session:
            with open(tar_path, "rb") as handler:
                file_size = handler.seek(0, 2)
                handler.seek(0)
                request = await session.put(
                    upload_url,
                    data=handler,
                    headers={
                        "Content-Type": "application/octet-stream",
                        "Content-Length": str(file_size),
                    },
                )

                if request.status != 200:
                    uploaded = False
                    LOGGER.error(
                        "There was an error uploading repository %s: %s",
                        root["nickname"],
                        await request.text(),
                    )

    return uploaded


def create_git_root_tar_file(
    root_nickname: str, repo_path: str, output_path: str | None = None
) -> bool:
    git_dir = os.path.normpath(f"{repo_path}/.git")
    with tarfile.open(output_path or f"{root_nickname}.tar.gz", "w:gz") as tar_handler:
        if os.path.exists(git_dir):
            tar_handler.add(git_dir, arcname=f"{root_nickname}/.git", recursive=True)
            return True
        return False


async def _push_repo(group_name: str, git_root: dict, force: bool) -> bool:
    upload_url, has_mirror_in_s3 = await get_git_root_upload_url(group_name, git_root["id"])
    if not upload_url:
        LOGGER.error("Could not get presigned URL to upload repository to S3")
        return False

    repo_path = os.path.join(os.getcwd(), "groups", group_name, git_root["nickname"])
    try:
        repo = Repo(repo_path)
    except GitError:
        LOGGER.error("Repository not found at %s", repo_path)
        return False

    current_commit = repo.head.commit.hexsha
    current_commit_date = datetime.fromtimestamp(repo.head.commit.authored_date, tz=UTC)
    last_commit = git_root["cloningStatus"]["commit"]
    last_upload_date = datetime.fromisoformat(git_root["cloningStatus"]["modifiedDate"])
    if (
        current_commit == last_commit
        and (datetime.now(UTC) - last_upload_date).days <= 3
        and has_mirror_in_s3
        and not force
    ):
        await update_git_root_cloning_status(
            group_name=group_name,
            git_root_id=git_root["id"],
            status="OK",
            commit=current_commit,
            commit_date=current_commit_date,
            message=("[Melts] The repository was not cloned as no new changes were detected"),
        )
        LOGGER.info(
            "Repository %s was not uploaded "
            "since no new changes were detected "
            "and the last upload was less than 3 days ago",
            git_root["nickname"],
        )
        return True

    if await _upload_repository(group_name, repo_path, git_root, upload_url):
        await update_git_root_cloning_status(
            group_name=group_name,
            git_root_id=git_root["id"],
            status="OK",
            commit=current_commit,
            commit_date=current_commit_date,
            message="[Melts] Cloned successfully",
        )
        LOGGER.info(
            "Repository %s uploaded successfully",
            git_root["nickname"],
        )
        return True

    return False


@click.command()
@click.option(
    "--group",
    "group_name",
    help="Specify the name of the group you want to upload to.",
    required=True,
)
@click.option(
    "--root",
    "git_root_nickname",
    default=None,
    help=("Upload a specific repository. Provide the nickname of the repository."),
    required=False,
)
@click.option(
    "--force",
    default=False,
    help="Upload repositories even if there are no new changes.",
    is_flag=True,
    required=False,
)
async def push_repos(group_name: str, git_root_nickname: str | None, force: bool) -> bool:
    """Upload repositories to the S3 mirror."""
    git_roots = await get_group_git_roots_paginated(group_name)
    git_roots = [
        git_root
        for git_root in git_roots
        if git_root.get("state") == "ACTIVE"
        and (git_root["nickname"] == git_root_nickname if git_root_nickname is not None else True)
    ]
    await collect(
        (_push_repo(group_name, git_root, force) for git_root in git_roots),
        workers=32,
    )
    return True
