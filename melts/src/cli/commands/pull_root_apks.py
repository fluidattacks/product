import os
from pathlib import (
    Path,
)
from typing import (
    Any,
)

import aiohttp
import asyncclick as click
from aioextensions import (
    collect,
)

from src.api.integrates import (
    get_group_git_root_env_apks,
    get_resource_download_url,
)
from src.logger import (
    LOGGER,
)

Item = dict[str, Any]


async def download_apk_file(
    group_name: str, apk_file_name: str, destination_path: str
) -> str | None:
    download_url = await get_resource_download_url(group_name, apk_file_name)
    if download_url:
        async with aiohttp.ClientSession() as session:
            timeout = aiohttp.ClientTimeout(total=120)
            async with session.get(download_url, timeout=timeout) as response:
                if response.status == 200:
                    with open(destination_path, "wb") as file:
                        while True:
                            chunk = await response.content.read(8192)
                            if not chunk:
                                break
                            file.write(chunk)
                    return destination_path

    LOGGER.error("Unable to download APK File %s", apk_file_name)
    return None


@click.command()
@click.option(
    "--group",
    "group_name",
    required=True,
    help="Specify the name of the group you want to download from.",
)
@click.option(
    "--root",
    "git_root_nickname",
    required=True,
    help=("Provide the nickname of the root to retrieve apk files."),
)
async def pull_root_apks(group_name: str, git_root_nickname: str) -> None:
    """Download APK files from the S3 mirror."""
    root_env_apks = await get_group_git_root_env_apks(group_name, git_root_nickname)
    if not root_env_apks:
        LOGGER.error("Cannot find any APK environments for root %s", git_root_nickname)
        return

    repo_apks_path = Path("groups") / group_name / git_root_nickname / "fa_apks_to_analyze"
    os.makedirs(repo_apks_path, exist_ok=True)

    results_paths: tuple[str | None, ...] = await collect(
        (
            download_apk_file(
                group_name=group_name,
                apk_file_name=apk_file_name,
                destination_path=os.path.join(repo_apks_path, apk_file_name),
            )
            for apk_file_name in root_env_apks
        ),
        workers=8,
    )

    for apk_downloaded_path in results_paths:
        if apk_downloaded_path:
            LOGGER.info("Downloaded APK file in %s", apk_downloaded_path)
