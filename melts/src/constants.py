import os

# Constants
API_TOKEN: str | None = os.environ.get("INTEGRATES_API_TOKEN")
BASE_DIR = os.path.dirname(__file__)
DEBUG: bool = os.environ.get("DEBUG") == "true"
