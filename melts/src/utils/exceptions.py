class CustomBaseException(Exception):
    pass


class RepositoryCredentialsNotFound(CustomBaseException):
    def __init__(self, git_root_nickname: str) -> None:
        header = "Exception - Repository credentials not found"
        data = f"{git_root_nickname}"
        msg = f"{header}: {data}"
        super().__init__(msg)


class GitRootNicknameNotFound(CustomBaseException):
    def __init__(self, git_root_nickname: str) -> None:
        header = "Exception - Git root nickname not found"
        data = f"{git_root_nickname}"
        msg = f"{header}: {data}"
        super().__init__(msg)


class InvalidIntegratesAPIToken(CustomBaseException):
    def __init__(self) -> None:
        header = "Exception - Integrates API token is expired or broken"
        super().__init__(header)
