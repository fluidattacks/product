{ makeSearchPaths, outputs, ... }: {
  dev = {
    melts = {
      source = [
        outputs."/common/dev/global_deps"
        outputs."/melts/config/runtime"
        (makeSearchPaths { pythonPackage = [ "$PWD/melts" ]; })
      ];
    };
  };
}
