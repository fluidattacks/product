{ inputs, makeScript, outputs, ... }: {
  jobs."/melts/coverage" = makeScript {
    entrypoint = ''
      aws_login dev 3600
      sops_export_vars common/secrets/dev.yaml CODECOV_TOKEN

      codecov-cli upload-process --flag melts --dir melts
    '';
    name = "melts-coverage";
    searchPaths = {
      bin = [ inputs.codecov-cli inputs.nixpkgs.git ];
      source = [ outputs."/common/utils/aws" outputs."/common/utils/sops" ];
    };
  };
}
