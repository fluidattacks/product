{ makePythonEnvironment, inputs, makeTemplate, projectPath, ... }:
let
  pythonRequirements = makePythonEnvironment {
    pythonProjectDir = projectPath "/melts";
    pythonVersion = "3.11";
  };
in {
  jobs."/melts/config/runtime" = makeTemplate {
    replace = { __argSrcMelts__ = projectPath "/melts"; };
    name = "melts-config-runtime";
    searchPaths = {
      bin = [
        inputs.nixpkgs.git
        inputs.nixpkgs.gnutar
        inputs.nixpkgs.openssh
        inputs.nixpkgs.python311
      ];
      pythonPackage = [ (projectPath "/melts") ];
      source = [ pythonRequirements ];
    };
    template = ./template.sh;
  };
}
