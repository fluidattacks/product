package org.fluidattacks.plugins.template.inspections

import com.intellij.DynamicBundle
import org.jetbrains.annotations.Nls
import org.jetbrains.annotations.NonNls
import org.jetbrains.annotations.PropertyKey

object InspectionBundle {
    private const val BUNDLE: @NonNls String = "messages.InspectionBundle"

    private val ourInstance =
        DynamicBundle(
            InspectionBundle::class.java,
            BUNDLE,
        )

    fun message(
        key:
            @PropertyKey(resourceBundle = BUNDLE)
            String,
        params: Any,
    ): @Nls String? = ourInstance.getMessage(key, *arrayOf(params))
}
