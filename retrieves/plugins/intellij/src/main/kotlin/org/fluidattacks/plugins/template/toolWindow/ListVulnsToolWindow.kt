package org.fluidattacks.plugins.template.toolWindow

import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.application.readAction
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.project.Project
import com.intellij.openapi.vfs.LocalFileSystem
import com.intellij.psi.PsiManager
import kotlinx.coroutines.runBlocking
import org.fluidattacks.plugins.template.domain.Vulnerability
import org.fluidattacks.plugins.template.services.MyProjectService
import org.fluidattacks.plugins.template.services.TokenService
import org.fluidattacks.plugins.template.views.WelcomePanel
import java.awt.BorderLayout
import javax.swing.JOptionPane
import javax.swing.JPanel

class ListVulnsToolWindow(
    private val project: Project,
) {
    private val logger: Logger = Logger.getInstance(this::class.java)
    private val myToolWindowContent = JPanel(BorderLayout())
    private val welcomePanel = WelcomePanel(this::handleSubmitToken)
    private val vulnerabilityTreePanel = VulnerabilityTreePanel(project, logger)
    private val myProjectService = project.getService(MyProjectService::class.java)

    init {
        setupUI()
        ApplicationManager.getApplication().executeOnPooledThread {
            runBlocking {
                val isValid = myProjectService.validateToken()
                if (isValid) {
                    fetchAndDisplayVulnerabilities()
                }
            }
        }
    }

    fun getContent(): JPanel = myToolWindowContent

    private fun setupUI() {
        myToolWindowContent.add(welcomePanel, BorderLayout.NORTH)
        myToolWindowContent.add(vulnerabilityTreePanel, BorderLayout.CENTER)
        vulnerabilityTreePanel.isVisible = false
    }

    private fun handleSubmitToken() {
        val token = welcomePanel.getToken()
        if (token.isNotEmpty()) {
            val tokenService = project.getService(TokenService::class.java)
            tokenService.setToken(token)
            fetchAndDisplayVulnerabilities()
        }
    }

    private fun filterAndGroupVulnerabilities(vulnerabilities: List<Vulnerability>): Map<String, List<Vulnerability>> {
        val psiManager = PsiManager.getInstance(project)
        val filteredVulnerabilities =
            vulnerabilities.filter { vulnerability ->
                val projectBasePath = project.basePath
                val cleanedFilePath = vulnerability.where.replaceFirst("${vulnerability.rootNickname}/", "")
                val absoluteFilePath = "$projectBasePath/$cleanedFilePath"
                val file = LocalFileSystem.getInstance().findFileByPath(absoluteFilePath)
                try {
                    val lineNumber =
                        if (vulnerability.specific.toInt() > 0) {
                            vulnerability.specific.toInt() - 1
                        } else {
                            vulnerability.specific.toInt()
                        }
                    if (file?.exists() == true) {
                        val lineCount =
                            psiManager
                                .findFile(file)
                                ?.viewProvider
                                ?.document
                                ?.lineCount

                        lineCount != null && lineNumber <= lineCount
                    } else {
                        false
                    }
                } catch (error: NumberFormatException) {
                    // Some vulnerability specifics aren't parsable as Integers
                    false
                }
            }

        val groupedVulns =
            filteredVulnerabilities
                .groupBy {
                    it.finding.title
                }.toList()
                .sortedByDescending { (_, vulns) -> vulns.first().finding.severityScoreV4 }
                .toMap()
        return groupedVulns.mapValues { finding -> finding.value.sortedWith(compareBy({ it.where }, { it.specific })) }
    }

    private fun fetchAndDisplayVulnerabilities() {
        runBlocking {
            try {
                val vulnerabilities = myProjectService.fetchVulnerabilities()
                if (vulnerabilities.isEmpty()) {
                    welcomePanel.isVisible = true
                } else {
                    readAction {
                        welcomePanel.isVisible = false
                        vulnerabilityTreePanel.isVisible = true
                        val groupedVulnerabilities = filterAndGroupVulnerabilities(vulnerabilities)
                        vulnerabilityTreePanel.updateTree(groupedVulnerabilities)
                    }
                }
            } catch (e: Exception) {
                ApplicationManager.getApplication().invokeLater {
                    JOptionPane.showMessageDialog(
                        myToolWindowContent,
                        "Error fetching vulnerabilities: ${e.message}",
                        "Error",
                        JOptionPane.ERROR_MESSAGE,
                    )
                }
            }
        }
    }
}
