package org.fluidattacks.plugins.template.services

import java.util.*

object ConfigUtil {
    private val properties: Properties = Properties()

    init {
        val inputStream = this::class.java.classLoader.getResourceAsStream("config.properties")
        inputStream?.use {
            properties.load(it)
        } ?: throw RuntimeException("Could not load configuration file")
    }

    fun getProperty(key: String): String? = properties.getProperty(key)
}
