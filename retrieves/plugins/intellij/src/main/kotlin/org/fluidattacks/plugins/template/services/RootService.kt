import com.apollographql.apollo3.api.ApolloResponse
import com.intellij.openapi.components.Service
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.InputValidator
import com.intellij.openapi.ui.Messages
import org.fluidattacks.plugins.template.domain.GitRoot
import org.fluidattacks.plugins.template.domain.Group
import org.fluidattacks.plugins.template.services.TokenService
import org.fluidattacks.plugins.template.services.Utils.executeQuery
import org.jetbrains.plugins.template.IntelliJGetGroupRootsQuery
import java.net.URI
import java.net.URISyntaxException
import kotlin.text.Regex

@Service(Service.Level.PROJECT)
class RootService(
    private val project: Project,
) {
    private val logger: Logger = Logger.getInstance(this::class.java)
    private val tokenService = project.getService(TokenService::class.java)

    suspend fun identifyRoots(
        groups: List<Group>,
        gitRemote: String,
        globalRole: String,
        nickname: String,
    ): List<GitRoot> {
        val trimmedGitRemote = normalizeUrl(gitRemote)

        return groups.flatMap { group ->
            val roots =
                if (group.roots.isNotEmpty() || globalRole != "admin") {
                    group.roots
                } else {
                    getGroupGitRootsSimple(group.name)
                }

            roots
                .filter { root ->
                    root.state == "ACTIVE" &&
                        (
                            root.nickname == nickname ||
                                root.groupName == nickname ||
                                root.url.contains(trimmedGitRemote) ||
                                normalizeUrl(root.url) == trimmedGitRemote
                        )
                }.map { root ->
                    root.copy(groupName = group.name, userRole = group.userRole)
                }
        }
    }

    private fun normalizeUrl(url: String): String {
    /*
     * For future reference:
     * Git URLs can come in many different protocols like HTTP, SSH, FTP and
     * Git itself.
     * Most can be split from the repository site (gitlab, github, ...) by a
     * .com/ part (HTTP) or .com: (SSH & Git). So that we can compare the inner
     * path, which is all Retrieves truly cares about.
     *
     * There are a couple of special cases though:
     * - Bitbucket SSH URLs use .org/ (SSH) and .org: (Git)
     * - Azure SSH URLs use .com:v3/
     * - Codecommit HTTPS & SSH URLs use .com/v1/
     * - Some private repositories on premises may have .com.<country>:<port>
     * The regex below tries to capture all these variations.
     */
        return try {
            val urlRegex = Regex("""\.com:v3/|\.com\.[^/]+/|\.(?<topssh>com|org):|\.(?<tophttp>com|org)/""")
            val trimmedUrl = url.trim().split(urlRegex).last()
            // Remove .git but only if it's at the end and the /_git/ part of Azure URLs
            val uri = URI(trimmedUrl.replace(Regex("\\.git$"), "").replace(Regex("//_git//"), "/"))
            URI(uri.scheme, uri.authority, uri.path, null, null).toString().trimEnd('/')
        } catch (e: URISyntaxException) {
            logger.warn("Error normalizing URL: $url", e)
            url
        }
    }

    private suspend fun getGroupGitRootsSimple(groupName: String): List<GitRoot> {
        val token = tokenService.getToken()
        val apolloGroups =
            token?.let {
                executeQuery(
                    token = it,
                    query = IntelliJGetGroupRootsQuery(groupName),
                    handleResponse = { response -> handleResponse(response) },
                )
            } ?: emptyList()
        return apolloGroups
    }

    private fun handleResponse(response: ApolloResponse<IntelliJGetGroupRootsQuery.Data>): List<GitRoot> {
        val roots =
            response.data
                ?.group
                ?.roots
                ?.filter { root ->
                    root.onGitRoot.state == "ACTIVE"
                }?.map { root ->
                    GitRoot(
                        id = root.onGitRoot.id,
                        nickname = root.onGitRoot.nickname,
                        state = root.onGitRoot.state,
                        url = root.onGitRoot.url,
                        groupName = response.data!!.group?.name,
                        userRole = null,
                        vulnerabilitiesConnection = null,
                    )
                } ?: emptyList()

        return roots
    }

    fun showGroupRequiredMessage() {
        Messages.showMessageDialog(
            project,
            "Please open a project related to a registered root in the Fluid Attacks platform",
            "Root Not Found",
            Messages.getWarningIcon(),
        )
    }

    fun showRootRequiredMessage() {
        Messages.showWarningDialog(
            "The extension cannot disambiguate this root, please choose one from the list.",
            "Root Not Set",
        )
    }

    class SelectionValidator(
        private val rootKeys: Array<String>,
    ) : InputValidator {
        override fun checkInput(inputString: String?): Boolean = inputString in rootKeys

        override fun canClose(inputString: String?): Boolean = inputString in rootKeys
    }

    fun showRootSelectionDialog(roots: List<GitRoot>): GitRoot? {
        val rootMap = roots.associateBy { "${it.nickname} of group ${it.groupName}" }
        val rootKeys = rootMap.keys.toTypedArray()
        val selectedValue =
            Messages.showEditableChooseDialog(
                "Select the root to analyze:",
                "Select Root",
                Messages.getQuestionIcon(),
                rootKeys,
                rootKeys.first(),
                SelectionValidator(rootKeys),
            )
        if (selectedValue != null) {
            return rootMap[selectedValue]
        }
        return null
    }
}
