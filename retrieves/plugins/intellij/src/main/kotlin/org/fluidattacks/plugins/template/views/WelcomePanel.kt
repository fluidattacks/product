package org.fluidattacks.plugins.template.views

import com.intellij.ide.BrowserUtil
import com.intellij.ui.components.ActionLink
import org.fluidattacks.plugins.template.services.ConfigUtil
import java.awt.*
import javax.swing.*

class WelcomePanel(
    private val handleSubmit: () -> Unit,
) : JPanel() {
    private val tokenField = JPasswordField(30)
    private val submitButton = JButton("Add Token")
    private val documentationUrl = ConfigUtil.getProperty("documentation.url") ?: ""

    init {
        setupUI()
    }

    private fun setupUI() {
        this.layout = BorderLayout()
        val dimension = Dimension(400, 100)

        val message =
            """
            In order to use the Fluid Attacks extension, enter a valid Fluid Attacks API token.<br><br>
            To generate a new token go to our Platform:Login>User Settings>API token>Add and paste the token here:
            """.trimIndent()

        val messageLabel = JLabel("<html>$message</html>")
        messageLabel.preferredSize = dimension
        this.add(messageLabel, BorderLayout.NORTH)

        val inputPanel = JPanel(FlowLayout(FlowLayout.LEFT))
        inputPanel.add(tokenField)
        inputPanel.add(submitButton)
        inputPanel.preferredSize = Dimension(400, 50)
        this.add(inputPanel, BorderLayout.CENTER)

        val docLinkLabel =
            ActionLink("See the docs") {
                BrowserUtil.browse(documentationUrl)
            }.apply { setExternalLinkIcon() }
        this.add(docLinkLabel, BorderLayout.SOUTH)

        submitButton.addActionListener {
            handleSubmit.invoke()
        }
    }

    override fun getPreferredSize(): Dimension = Dimension(500, 200)

    fun getToken(): String = tokenField.password.concatToString()
}
