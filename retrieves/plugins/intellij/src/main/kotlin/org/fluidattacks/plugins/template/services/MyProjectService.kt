package org.fluidattacks.plugins.template.services

import RootService
import com.apollographql.apollo3.api.ApolloResponse
import com.intellij.ide.util.PropertiesComponent
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.components.Service
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.project.Project
import kotlinx.coroutines.runBlocking
import org.fluidattacks.plugins.template.domain.*
import org.fluidattacks.plugins.template.services.Utils.executeQuery
import org.fluidattacks.plugins.template.services.Utils.getGitRemoteUrl
import org.jetbrains.plugins.template.IntelliJGetRootVulnerabilitiesQuery
import org.jetbrains.plugins.template.IntelliJGetUserGroupsQuery
import org.jetbrains.plugins.template.IntelliJGetUserInfoQuery
import java.nio.file.Paths

@Service(Service.Level.PROJECT)
class MyProjectService(
    private val project: Project,
) {
    private val logger: Logger = Logger.getInstance(this::class.java)
    private val tokenService = project.getService(TokenService::class.java)
    private val rootService = project.getService(RootService::class.java)

    init {
        logger.info("Fluid Attacks initialized for project: ${project.name}")
    }

    suspend fun validateToken(): Boolean {
        logger.info("Validating API token")
        val token = tokenService.getToken()
        if (token == null || token == "" || !isValidToken(token)) {
            logger.info("Failed to retrieve a valid API token")
            return false
        }
        return setGroupAndRoot()
    }

    private suspend fun isValidToken(token: String): Boolean {
        executeQuery(
            token = token,
            query = IntelliJGetUserInfoQuery(),
            handleResponse = { response: ApolloResponse<IntelliJGetUserInfoQuery.Data> -> handleUserResponse(response) },
        ) ?: false
        return true
    }

    private fun handleUserResponse(response: ApolloResponse<IntelliJGetUserInfoQuery.Data>): Boolean {
        val userEmail = response.data?.me?.userEmail
        val userRole = response.data?.me?.role

        if (userEmail != null) {
            PropertiesComponent.getInstance().setValue("user.email", userEmail)
            PropertiesComponent.getInstance().setValue("user.role", userRole)
            return true
        }
        return false
    }

    private fun setGroupAndRoot(): Boolean {
        logger.info("Setting group and root")

        val token = tokenService.getToken() ?: return false
        val currentPath = project.basePath ?: return false
        val localNickname = Paths.get(currentPath).fileName.toString()
        val gitRemote = getGitRemoteUrl(currentPath) ?: ""
        val groupKey = "${project.name}.group"
        val rootNicknameKey = "${project.name}.root.nickname"

        val result =
            runBlocking {
                val groups = fetchUserGroups(token)
                val roots =
                    PropertiesComponent.getInstance().getValue("user.role")?.let {
                        rootService.identifyRoots(
                            groups = groups,
                            gitRemote = gitRemote,
                            globalRole = it,
                            nickname = localNickname,
                        )
                    }

                when {
                    roots?.size == 1 -> {
                        PropertiesComponent.getInstance(project).setValue(groupKey, roots[0].groupName)
                        PropertiesComponent.getInstance(project).setValue(rootNicknameKey, roots[0].nickname)
                        logger.info("Set root ${roots[0].nickname} from group ${roots[0].groupName}")
                        true
                    }
                    roots?.size!! > 1 -> {
                        ApplicationManager.getApplication().invokeAndWait {
                            val selectedRoot = rootService.showRootSelectionDialog(roots)
                            if (selectedRoot != null) {
                                PropertiesComponent.getInstance(project).setValue(groupKey, selectedRoot.groupName)
                                PropertiesComponent
                                    .getInstance(project)
                                    .setValue(rootNicknameKey, selectedRoot.nickname)
                                logger.info("Set root ${selectedRoot.nickname} from group ${selectedRoot.groupName}")
                            } else {
                                rootService.showRootRequiredMessage()
                                logger.warn("Did not select a root from the available list")
                            }
                        }
                        PropertiesComponent.getInstance(project).getValue(rootNicknameKey, "").isNotEmpty()
                    }
                    else -> {
                        ApplicationManager.getApplication().invokeLater {
                            rootService.showGroupRequiredMessage()
                        }
                        logger.warn("Cannot set group and root")
                        logger.info("Local nickname: $localNickname | Local remote URL: $gitRemote")
                        val availableRoots =
                            groups
                                .map { group ->
                                    group.roots.filter { it.state == "ACTIVE" }.map {
                                        group.name
                                        it.nickname
                                        it.url
                                    }
                                }.flatten()
                        logger.info("Available roots: $availableRoots")
                        false
                    }
                }
            }

        return result
    }

    private suspend fun fetchUserGroups(token: String): List<Group> =
        executeQuery(
            token = token,
            query = IntelliJGetUserGroupsQuery(),
            handleResponse = { response: ApolloResponse<IntelliJGetUserGroupsQuery.Data> -> handleGroupsResponse(response) },
        ) ?: emptyList()

    private fun handleGroupsResponse(response: ApolloResponse<IntelliJGetUserGroupsQuery.Data>): List<Group> =
        response.data?.me?.organizations?.flatMap { organization ->
            organization?.groups?.mapNotNull { group ->
                group?.let {
                    Group(
                        name = group.name,
                        userRole = group.userRole,
                        subscription = null,
                        roots =
                            group.roots?.map { root ->
                                root.onGitRoot.let { gitRoot ->
                                    GitRoot(
                                        id = gitRoot.id,
                                        nickname = gitRoot.nickname,
                                        state = gitRoot.state,
                                        url = gitRoot.url,
                                        groupName = group.name,
                                        userRole = group.userRole,
                                        vulnerabilitiesConnection = null,
                                    )
                                }
                            } ?: emptyList(),
                    )
                }
            } ?: emptyList()
        } ?: emptyList()

    suspend fun fetchVulnerabilities(): List<Vulnerability> {
        logger.info("Getting vulnerabilities info")
        val groupKey = "${project.name}.group"
        val rootNicknameKey = "${project.name}.root.nickname"

        val groupName = PropertiesComponent.getInstance(project).getValue(groupKey)
        val rootNickname = PropertiesComponent.getInstance(project).getValue(rootNicknameKey)

        if (groupName != null && rootNickname != null) {
            try {
                val vulnerabilities = mutableListOf<Vulnerability>()
                var cursor = ""

                do {
                    val response =
                        executeQuery(
                            token = tokenService.getToken() ?: throw IllegalStateException("Token is not available"),
                            query = IntelliJGetRootVulnerabilitiesQuery(groupName, cursor, rootNickname, 200),
                            handleResponse = { response: ApolloResponse<IntelliJGetRootVulnerabilitiesQuery.Data> ->
                                handleResponse(
                                    response,
                                )
                            },
                        )
                    if (response?.vulnerabilities != null) {
                        vulnerabilities.addAll(response.vulnerabilities)
                        cursor = response.pageInfo.endCursor ?: ""
                    }
                } while (response?.pageInfo?.hasNextPage == true)
                return vulnerabilities
            } catch (e: Exception) {
                logger.error("Error fetching vulnerabilities", e)
            }
        } else {
            rootService.showGroupRequiredMessage()
            logger.info("Group name or root nickname unavailable")
        }
        return emptyList()
    }

    private fun handleResponse(response: ApolloResponse<IntelliJGetRootVulnerabilitiesQuery.Data>): VulnerabilityQueryResult {
        val vulnerabilities = mutableListOf<Vulnerability>()
        response.data?.group?.vulnerabilities?.edges?.forEach { edge ->
            edge?.node?.let { node ->
                val finding = node.finding
                if (finding != null) {
                    vulnerabilities.add(
                        Vulnerability(
                            id = node.id,
                            specific = node.specific,
                            where = node.where,
                            rootNickname = node.rootNickname,
                            reportDate = node.reportDate,
                            treatmentStatus = node.treatmentStatus.toString(),
                            finding =
                                Finding(
                                    id = finding.id,
                                    description = finding.description,
                                    title = finding.title,
                                    severityScoreV4 = finding.severityScoreV4,
                                ),
                            verification = node.verification,
                        ),
                    )
                }
            }
        }
        return VulnerabilityQueryResult(
            vulnerabilities,
            PageInfo(
                response.data
                    ?.group
                    ?.vulnerabilities
                    ?.pageInfo
                    ?.endCursor,
                response.data
                    ?.group
                    ?.vulnerabilities
                    ?.pageInfo
                    ?.hasNextPage,
            ),
        )
    }
}
