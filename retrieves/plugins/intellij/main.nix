{ inputs, makeScript, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "retrieves-plugins-intellij";
  searchPaths = { bin = [ inputs.nixpkgs.ktlint ]; };
}
