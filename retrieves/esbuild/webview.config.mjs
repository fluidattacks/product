import { build, context } from "esbuild";

const isDev = process.env.GITLAB_CI === undefined;
const buildOptions = {
  bundle: true,
  entryPoints: ["./src/webview/App.tsx"],
  external: ["bufferutil", "utf-8-validate", "vscode"],
  format: "cjs",
  minify: !isDev,
  outfile: "dist/webview.js",
  platform: "browser",
  sourcemap: true,
};

if (isDev) {
  const ctx = await context(buildOptions);

  // eslint-disable-next-line fp/no-mutating-methods
  await ctx.watch();

  await ctx.serve({
    port: 9000,
  });
} else {
  await build(buildOptions);
}
