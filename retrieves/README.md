# Visual Studio Code plugin

Fluid Attacks' extension facilitates the **management of vulnerabilities** in
your code when you have your applications or infrastructure under evaluation
in our all-in-one solution:
[Continuous Hacking](https://fluidattacks.com/services/continuous-hacking/).

Whether you are subscribed to our Essential or
[Advanced plan](https://fluidattacks.com/plans/), you can download this plugin
for free anytime and install it to complement what we offer on
[our platform](https://fluidattacks.com/platform/). This extension
is ideal for your developers from the beginning of the software development
lifecycle as it allows them to achieve and maintain **high vulnerability
remediation rates**.

The following are the **benefits** of using the Fluid Attacks Visual Studio
Code plugin:
- You get a detailed list of the types of vulnerabilities and their specific
cases that we have identified so far in the source code of your products.
- You see the specific lines of code in which a vulnerability detected by our
tools or ethical hackers is present.
- You can automatically generate step-by-step guides for vulnerability
remediation ("Custom fix").
- You can automatically generate instant solution proposals to remediate
vulnerabilities ("Autofix").
- You can request reattacks or reevaluations by our tools or hacking team to
verify the effectiveness of your vulnerability remediation.
![custom fix](https://gitlab.com/fluidattacks/universe/-/raw/trunk/retrieves/docs/get_custom_fix.png)

## Supported languages, platforms and frameworks

Remediation guides and automatic vulnerability fixes are available for the
following languages, platforms and frameworks: AWS, Azure, C-Sharp, Dart,
Docker, Elixir, Go, Java, PHP, Python, Ruby, Scala, Swift, Terraform, and
TypeScript.

## Installation and configuration

Please follow these steps:

 1. Click the Install button here in the Visual Studio Marketplace or go to the
 Extensions section of your Visual Studio Code and search for and install the
 Fluid Attacks plugin there.
 1. Log in to Fluid Attacks' platform.
 1. Generate and copy an API token from our platform (see
 [API Setup](https://help.fluidattacks.com/portal/en/kb/articles/learn-the-basics-of-the-fluid-attacks-api#Authentication_with_Fluid_Attacks__platform_API_Token)).
 1. Go to Settings of the Fluid Attacks extension in Visual Studio Code and
 paste the API token in the corresponding space (you can also add it after
 clicking on the Fluid Attacks extension icon; see
 [IDE Installation](https://help.fluidattacks.com/portal/en/kb/articles/install-the-vs-code-extension)):
 ![API token](https://gitlab.com/fluidattacks/universe/-/raw/trunk/retrieves/docs/add_api_token.png)

 - If there is a problem loading that window to enter the API token, you can
 open Preferences: Open User Settings (JSON) in your editor and add the token
 in the following setting:

  ```json
  {
  "fluidattacks.apiToken": "your-token-here",
  }
  ```

 - You can also provide the token to the editor via an environment variable:

 ```sh
  export FLUID_API_TOKEN="your-token-here"
 ```

 5. After entering the token, close and reopen the editor to apply the changes.

 6. Verify that the installation and configuration of the extension were done
 correctly: The Fluid Attacks icon should be displayed in the left sidebar, and
 red dots will appear in the Explorer, next to the folders in your repository
 with vulnerable files.
![Successful config](https://res.cloudinary.com/fluid-attacks/image/upload/v1692778934/docs/machine/vscode-extension/activation_extension.png)

## Support

Visit Fluid Attacks' [Knowledge Base](https://help.fluidattacks.com/portal/en/kb)
and find everything related to this IDE extension:
- [Introduction](https://help.fluidattacks.com/portal/en/kb/integrate-with-fluid-attacks/use-ide-extensions/vs-code-extension)
- [Installation](https://help.fluidattacks.com/portal/en/kb/articles/install-the-vs-code-extension)
- [Platform](https://help.fluidattacks.com/portal/en/kb/articles/view-vulnerable-lines-use-fix-options-and-more)
- [Custom fix](https://help.fluidattacks.com/portal/en/kb/articles/get-ai-generated-guides-for-remediation)
- [Autofix](https://help.fluidattacks.com/portal/en/kb/articles/fix-code-automatically-with-gen-ai)
- [FAQ](https://help.fluidattacks.com/portal/en/kb/articles/integrations-faq)

If you have any questions,
please contact us at help@fluidattacks.com.
You can also schedule onboarding sessions with our team
[here](https://calendar.google.com/calendar/u/0/appointments/schedules/AcZssZ2risTBon9b0rvtvpNr-rrMzYRIjcxjcaBGOxkAZUOjO8CtA8RBTVpp_4LT-t2UgyPbf4CclXS8).

## Telemetry

We collect error logs to help improve the extension (see our
[Privacy policy](https://fluidattacks.com/privacy/)). However, this plugin
respects the VS Code telemetry settings, so you can opt out of the telemetry
as shown
[here](https://code.visualstudio.com/docs/supporting/faq#_how-to-disable-telemetry-reporting).
