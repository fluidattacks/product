let
  arch = let
    commit = "f24ef3fa9b7ac09c39d86e3a14651be04e0379f2";
    sha256 = "sha256:136jmgd0f4hrv859bx4agjzisbz9vwi0hhvnq1m7kskr6mzqlrgg";
    url =
      "https://gitlab.com/fluidattacks/universe/-/raw/${commit}/common/ci/arch.nix";
    src = builtins.fetchurl { inherit sha256 url; };
  in import src;

  rules = {
    default = arch.core.rules.titleRule {
      products = [ "all" "retrieves" ];
      types = [ ];
    };
    noChore = arch.core.rules.titleRule {
      products = [ "all" "retrieves" ];
      types = [ "feat" "fix" "refac" ];
    };
  };
in {
  pipelines = {
    retrieves = {
      gitlabPath = "/retrieves/pipeline/default.yaml";
      jobs = [
        {
          output = "/pipelineOnGitlab/retrieves";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.noChore ];
            stage = arch.stages.test;
            tags = [ arch.tags.retrieves ];
          };
        }
        {
          output = "/retrieves test";
          gitlabExtra = arch.extras.default // {
            artifacts = {
              paths = [ "retrieves/.coverage/lcov.info" ];
              expire_in = "1 day";
            };
            rules = arch.rules.dev ++ [ rules.noChore ];
            stage = arch.stages.test;
            tags = [ arch.tags.common-large ];
          };
        }
        {
          output = "/retrieves/plugins/intellij lint";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.noChore ];
            stage = arch.stages.test;
            tags = [ arch.tags.retrieves ];
          };
        }
        {
          output = "/retrieves deploy";
          gitlabExtra = arch.extras.default // {
            resource_group = "deploy/$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.common-large ];
          };
        }
        {
          output = "/retrieves lint";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.noChore ];
            stage = arch.stages.test;
            tags = [ arch.tags.common-large ];
          };
        }
        {
          output = "/retrieves/coverage";
          gitlabExtra = arch.extras.default // {
            needs = [ "/retrieves test" ];
            rules = arch.rules.dev ++ [ rules.noChore ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.retrieves ];
            variables.GIT_DEPTH = 1000;
          };
        }
      ];
    };
  };
}
