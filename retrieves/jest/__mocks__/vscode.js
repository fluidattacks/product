/*
 * This is a mock implementation of the vscode API for Jest. If you ever get
 * an error like "Cannot find module 'vscode' from src/file..." the offending
 * method should be added here.
 *
 * To check if a vscode method was called, simply import the library like
 * import { vscodeMethodToBeChecked } from "vscode"; and
 * assert it like
 * expect(vscodeMethodToBeChecked).toHaveBeenCalledWith(...)
 * Make sure the method to be checked has been defined here
 * See https://www.richardkotze.com/coding/unit-test-mock-vs-code-extension-api-jest
 *
 * This should be good enough for unit tests, but integration and e2e testing
 * will require a more thorough approach.
 */
const path = require("path")


class Diagnostic {
    constructor(
        range,
        message,
        severity,
    ) {
        this.range = range,
        this.message = message,
        this.severity = severity
    }
}

const DiagnosticSeverity = { Error: 0, Warning: 1, Information: 2, Hint: 3 };

const InputBoxValidationSeverity = { Info: 1, Warning: 2, Error: 3 };

class InputBoxValidationMessage {
    constructor(
        message,
        severity,
    ) {
        this.message = message,
        this.severity = severity
    }
};

const LogOutputChannel = {
    info: jest.fn(),
    warn: jest.fn(),
    error: jest.fn(),
    show: jest.fn(),
}

class Position {
    constructor(
        line,
        character,
    ) {
        this.line = line,
        this.character = character
    }
};

class Range {
    constructor(
        start,
        end,
    ) {
        this.start = start,
        this.end = end
    }
}

class ThemeIcon {
    static File
    static Folder
}

class TreeItem {
    constructor (
        label,
        collapsibleState,
        id,
        tooltip,
        iconPath,
        description,
        resourceUri,
        command
    ) {
        this.label = label,
        this.collapsibleState = collapsibleState,
        this.id = id,
        this.command = command,
        this.tooltip = tooltip,
        this.iconPath = iconPath,
        this.description = description,
        this.resourceUri = resourceUri
    }
}

const TreeItemCollapsibleState = { NONE: 0, COLLAPSED: 1, EXPANDED: 2 };

const Uri = {
    file: jest.fn(path => { return { path: path } }),
    joinPath: jest.fn(
        (uri, paths) => { return { path: path.join(uri.path, ...paths)}}
    ),
    parse: jest.fn(path => { return { path: path } }),
};

const commands = {
    registerCommand: jest.fn(),
    executeCommand: jest.fn(),
};

const debug = {
    onDidTerminateDebugSession: jest.fn(),
    startDebugging: jest.fn(),
};

const env = {
    openExternal: jest.fn(),
    language: "en",
}

const languages = {
    createDiagnosticCollection: jest.fn()
};

const ExtensionMode = {
    Production: 1,
    Development: 2,
    Test: 3,
};

const ViewColumn = {
    Beside: -2,
    Active: -1,
    One: 1,
    Two: 2,
    Three: 3,
    Four: 4,
    Five: 5,
    Six: 6,
    Seven: 7,
    Eight: 8,
    Nine: 9,
};

const Webview = {
    asWebviewUri: jest.fn(
        (localResource) => {
            return { path: `vscode-resource:${localResource.path}`}
        }
    )
};

const WebviewPanel = {
    webview: Webview
};

const window = {
    activeTextEditor: {
        document: {
            fileName: "testFile.txt",
            getText: jest.fn(() => "Content test"),
        },
        selection: {
            active: {line: 9},
            start: {line: 9},
            end: {line: 9},
        }
    },
    createOutputChannel: jest.fn(() => ({
        info: jest.fn(),
        warn: jest.fn(),
        error: jest.fn(),
        show: jest.fn(),
    })),
    createStatusBarItem: jest.fn(() => ({
        show: jest.fn()
    })),
    createWebviewPanel: jest.fn(
        (viewType, title, showOptions, options) => WebviewPanel),
    showInformationMessage: jest.fn(() => {
        return {
          then: (callback) => {
            callback("Reload");
          },
        };
    }),
    showWarningMessage: jest.fn(() => {
        return {
        then: (callback) => {
            callback("Reload");
        },
        };
    }),
    showErrorMessage: jest.fn(() => {
        return {
          then: (callback) => {
            callback("Reload");
          },
        };
    }),
    showInputBox: jest.fn(),
    showQuickPick: jest.fn((optionList) => optionList[0]),
    createTextEditorDecorationType: jest.fn(),
    onDidChangeActiveTextEditor: jest.fn(),
};

const workspace = {
    getConfiguration: jest.fn(_extName => {
        return {
            get: jest.fn(configName => {
                if (configName === "apiToken") { return "test.jwt.token" }
                else if (configName === "extraGroups") { return ["test-group"] }
                else { return jest.fn() }
            })
        }
    }),
    workspaceFolders: [],
    openTextDocument: jest.fn(),
    onDidSaveTextDocument: jest.fn(),
    onDidChangeTextDocument: jest.fn(),
};

const vscode = {
    Diagnostic,
    DiagnosticSeverity,
    ExtensionMode,
    InputBoxValidationMessage,
    InputBoxValidationSeverity,
    LogOutputChannel,
    Position,
    Range,
    ThemeIcon,
    TreeItem,
    TreeItemCollapsibleState,
    Uri,
    commands,
    debug,
    env,
    languages,
    ViewColumn,
    Webview,
    WebviewPanel,
    window,
    workspace,
};

module.exports = vscode;
