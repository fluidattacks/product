/* eslint-disable jest/require-top-level-describe, jest/no-hooks */
import { server } from "@retrieves/mocks/server";
import { RetrievesAuthentication } from "@retrieves/utils/auth";

// Establish API mocking and initialize token before all tests.
beforeAll((): void => {
  RetrievesAuthentication.init({
    delete: jest.fn(),
    get: jest.fn(
      async (_): Promise<string> => Promise.resolve("test.jwt.token"),
    ),
    onDidChange: jest.fn(),
    store: jest.fn(),
  });

  server.listen({ onUnhandledRequest: "error" });
});

/*
 * Reset any request handlers that we may add during the tests,
 * so they don't affect other tests.
 */
afterEach((): void => {
  server.resetHandlers();
});

// Clean up after the tests are finished.
afterAll((): void => {
  server.close();
});
