import type { Config } from "jest";

const config: Config = {
  bail: 1,
  collectCoverage: true,
  coverageDirectory: ".coverage",
  coveragePathIgnorePatterns: ["<rootDir>/src/mocks"],
  coverageReporters: ["text", "lcov"],
  maxWorkers: 1,
  moduleDirectories: ["node_modules", "src"],
  moduleNameMapper: {
    "@retrieves/(.*)": "<rootDir>/src/$1",
  },
  preset: "ts-jest",
  rootDir: "..",
  roots: ["<rootDir>/src", "<rootDir>/jest"],
  setupFilesAfterEnv: ["<rootDir>/jest/jest.setup.ts"],
  testEnvironment: "node",
  testMatch: ["**/?(*.)+(spec|test).ts"],
};

// eslint-disable-next-line import/no-default-export
export default config;
