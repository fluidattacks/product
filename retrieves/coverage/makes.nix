{ inputs, makeScript, outputs, ... }: {
  jobs."/retrieves/coverage" = makeScript {
    entrypoint = ''
      aws_login dev 3600
      sops_export_vars common/secrets/dev.yaml CODECOV_TOKEN

      codecov-cli upload-process --flag retrieves --dir retrieves
    '';
    name = "retrieves-coverage";
    searchPaths = {
      bin = [ inputs.codecov-cli inputs.nixpkgs.git ];
      source = [ outputs."/common/utils/aws" outputs."/common/utils/sops" ];
    };
  };
}
