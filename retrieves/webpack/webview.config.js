//@ts-check

'use strict';

const path = require('path');
const { EnvironmentPlugin } = require('webpack');

//@ts-check
/** @typedef {import('webpack').Configuration} WebpackConfig **/
/** @typedef {import('webpack-dev-server').Configuration} DevServerConfig **/

/** @type WebpackConfig */
const webviewConfig = {
    name: 'webview',
    target: 'web',
    entry: './src/webview/App.tsx',
    output: {
        filename: 'webview.js',
        path: path.resolve(__dirname, '../dist')
    },
    devtool: 'source-map',
    resolve: {
        alias: {
          "@retrieves": path.resolve(__dirname, "..", "src"),
        },
        extensions: ['.ts', '.js', '.tsx', '.jsx']
    },
    module: {
        rules: [
        {
            test: function testFilePath (filePath) {
              return (
                (filePath.endsWith(".ts") || filePath.endsWith(".tsx")) &&
                (!filePath.endsWith("test.ts") || !filePath.endsWith("test.tsx"))
              );
            },
            exclude: /node_modules|tree-sitter|plugins/,
            loader: 'esbuild-loader',
            options: {
            loader: 'tsx',  // Or 'ts' if you don't need tsx
            target: 'ES2021'
            },
        },
        {
            test: /\.css$/,
            use: ['style-loader', 'css-loader', 'postcss-loader']
        }
        ]
    },
    performance: {
        hints: false
    },
    plugins: [
      new EnvironmentPlugin({
        APP_URL:
          process.env.GITLAB_CI !== undefined &&
          process.env.CI_COMMIT_REF_NAME !== process.env.CI_DEFAULT_BRANCH
            ? `https://${process.env.CI_COMMIT_REF_NAME}.app.fluidattacks.com`
            : "https://app.fluidattacks.com",
      }),
    ],
    devServer: {
        compress: true,
        port: 9000,
        hot: true,
        allowedHosts: "all",
        headers: {
        "Access-Control-Allow-Origin": "*",
        }
    }
}

module.exports = [ webviewConfig ];
