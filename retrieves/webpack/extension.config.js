//@ts-check

'use strict';

const path = require('path');
const { EnvironmentPlugin } = require('webpack');

//@ts-check
/** @typedef {import('webpack').Configuration} WebpackConfig **/

/** @type WebpackConfig */
const extensionConfig = {
  target: 'node', // VS Code extensions run in a Node.js-context 📖 -> https://webpack.js.org/configuration/node/

  entry: './src/extension.ts', // the entry point of this extension, 📖 -> https://webpack.js.org/configuration/entry-context/
  output: {
    // the bundle is stored in the 'dist' folder (check package.json), 📖 -> https://webpack.js.org/configuration/output/
    path: path.resolve(__dirname, '../dist'),
    filename: 'extension.js',
    libraryTarget: 'commonjs2'
  },
  externals: {
    vscode: "commonjs vscode", // the vscode-module is created on-the-fly and must be excluded. Add other modules that cannot be webpack'ed, 📖 -> https://webpack.js.org/configuration/externals/
    bufferutil: "bufferutil",
    "utf-8-validate": "utf-8-validate",
    // modules added here also need to be added in the .vscodeignore file
  },
  resolve: {
    alias: {
      "@retrieves": path.resolve(__dirname, "..", "src"),
    },
    // support reading TypeScript and JavaScript files, 📖 -> https://github.com/TypeStrong/ts-loader
    extensions: ['.ts', '.js']
  },
  module: {
    rules: [
      {
        test: function testFilePath (filePath) {
          return filePath.endsWith('.ts') && !filePath.endsWith('test.ts');
        },
        exclude: /node_modules|tree-sitter|plugins/,
        loader: 'esbuild-loader',
        options: {
          loader: 'ts',  // Or 'ts' if you don't need tsx
          target: 'ES2021'
        },
      }
    ],
  },
  devtool: 'nosources-source-map',
  infrastructureLogging: {
    level: "log", // enables logging required for problem matchers
  },
  plugins: [
    new EnvironmentPlugin({
      APP_URL:
        process.env.GITLAB_CI !== undefined &&
        process.env.CI_COMMIT_REF_NAME !== process.env.CI_DEFAULT_BRANCH
          ? `https://${process.env.CI_COMMIT_REF_NAME}.app.fluidattacks.com`
          : "https://app.fluidattacks.com",
    }),
  ],
};

module.exports = [ extensionConfig ];
