import { messageHandler } from "@estruyf/vscode/dist/client";
import {
  VSCodeButton,
  VSCodeDataGrid,
  VSCodeDataGridCell,
  VSCodeDataGridRow,
} from "@vscode/webview-ui-toolkit/react";
import type React from "react";
import { useCallback, useMemo, useState } from "react";

import type { IGitRoot, IToeLines } from "@retrieves/types";
import { ToeLinesRow } from "@retrieves/webview/components/ToeLinesRow";

import "@retrieves/webview/styles.css";

const ToeLines = (): React.JSX.Element => {
  const [isPriOrder, setIsPriOrder] = useState<number>(0);
  const [toeLines, setToeLines] = useState<IToeLines[]>();
  const [root, setRoot] = useState<IGitRoot>();
  const [hideAttackedFiles, setHideAttackedFiles] = useState<boolean>(false);
  const [hideNoLongerPresent, setHideNoLongerPresent] =
    useState<boolean>(false);
  useMemo((): void => {
    if (!toeLines) {
      void messageHandler
        .request<IToeLines[]>("GET_DATA_TOE_LINES")
        .then((msg): void => {
          setToeLines(msg);
        });
    }
  }, [toeLines]);
  useMemo((): void => {
    void messageHandler.request<IGitRoot>("GET_ROOT").then((msg): void => {
      setRoot(msg);
    });
  }, []);

  const sortPriColumn = (toe1: IToeLines, toe2: IToeLines): number => {
    const toePriority1 = toe1.sortsPriorityFactor;
    const toePriority2 = toe2.sortsPriorityFactor;
    const isToePriority2Less = toePriority2 < toePriority1 ? 1 : 0;
    const isToePriority1Less = toePriority1 < toePriority2 ? 1 : 0;

    if (isPriOrder === 2) {
      setIsPriOrder(1);

      return toePriority2 > toePriority1 ? -1 : isToePriority2Less;
    }

    setIsPriOrder(2);

    return toePriority1 > toePriority2 ? -1 : isToePriority1Less;
  };

  const clickPriColumn = (): VoidFunction => (): void => {
    // eslint-disable-next-line functional/immutable-data
    setToeLines([...(toeLines?.sort(sortPriColumn) ?? [])]);
  };
  const descendingArrow = isPriOrder === 2 ? "🔻" : "";

  return (
    <div>
      <VSCodeButton
        onClick={useCallback((): void => {
          setHideAttackedFiles(!hideAttackedFiles);
        }, [hideAttackedFiles])}
      >
        {hideAttackedFiles ? "Show attacked files" : "Hide attacked files"}
      </VSCodeButton>
      <VSCodeButton
        onClick={useCallback((): void => {
          setHideNoLongerPresent(!hideNoLongerPresent);
        }, [hideNoLongerPresent])}
      >
        {hideNoLongerPresent
          ? "Show non-present files"
          : "Hide non-present files"}
      </VSCodeButton>
      <VSCodeDataGrid>
        <VSCodeDataGridRow>
          {[
            "Filename",
            "Attacked",
            "Attacked lines",
            "LOC",
            "Modified",
            "Comment",
            "Sorts Priority Factor",
          ].map((row, index): React.JSX.Element => {
            if (row === "Sorts Priority Factor") {
              return (
                <VSCodeDataGridCell
                  gridColumn={String(index + 1)}
                  key={row}
                  onClick={clickPriColumn()}
                >
                  {`${row} ${isPriOrder === 1 ? "🔺" : descendingArrow}`}
                </VSCodeDataGridCell>
              );
            }

            return (
              <VSCodeDataGridCell gridColumn={String(index + 1)} key={row}>
                {row}
              </VSCodeDataGridCell>
            );
          })}
        </VSCodeDataGridRow>

        {(toeLines ?? [])
          .filter((item): boolean => {
            if (hideAttackedFiles && item.attackedLines >= item.loc) {
              return false;
            }
            if (hideNoLongerPresent && !item.bePresent) {
              return false;
            }

            return Boolean(root);
          })
          .map((item): React.JSX.Element => {
            return (
              <ToeLinesRow
                groupName={root?.groupName ?? ""}
                key={item.filename}
                node={item}
                rootId={root?.id ?? ""}
              />
            );
          })}
      </VSCodeDataGrid>
    </div>
  );
};

export { ToeLines };
