import { messageHandler } from "@estruyf/vscode/dist/client";
import {
  VSCodeDataGrid,
  VSCodeDataGridCell,
  VSCodeDataGridRow,
} from "@vscode/webview-ui-toolkit/react";
import type React from "react";
import { useMemo, useState } from "react";

import type { IGitRoot } from "@retrieves/types";

import "@retrieves/webview/styles.css";

const GitEnvironmentUrls = (): React.JSX.Element => {
  const [root, setRoot] = useState<IGitRoot>();
  const [rootId, setRootId] = useState<string>();

  useMemo((): void => {
    void messageHandler.request<string>("GET_ROOT_ID").then((msg): void => {
      setRootId(msg);
    });
  }, []);

  useMemo((): void => {
    if (rootId !== undefined) {
      void messageHandler
        .request<IGitRoot>("GET_ROOT", { rootId })
        .then((msg): void => {
          setRoot(msg);
        });
    }
  }, [rootId]);
  if (root === undefined) {
    return <div />;
  }

  return (
    <div>
      <VSCodeDataGrid>
        <VSCodeDataGridRow>
          {["url"].map((row, index): React.JSX.Element => {
            return (
              <VSCodeDataGridCell gridColumn={String(index + 1)} key={row}>
                {row}
              </VSCodeDataGridCell>
            );
          })}
        </VSCodeDataGridRow>

        {root.gitEnvironmentUrls.map((item, index): React.JSX.Element => {
          return (
            <VSCodeDataGridRow key={item.id}>
              <VSCodeDataGridCell gridColumn={String(index + 1)} />
              {item.url}
            </VSCodeDataGridRow>
          );
        })}
      </VSCodeDataGrid>
    </div>
  );
};

export { GitEnvironmentUrls };
