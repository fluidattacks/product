/*
 * You can import and use all API from the 'vscode' module
 * as well as import your extension to test it
 */

describe("extension test suite", (): void => {
  it("placeholder test", (): void => {
    expect.hasAssertions();

    const simpleSum = 1 + 1;

    expect(simpleSum).toBe(2);
  });
});
