import { resolve } from "path";

import { runTests } from "@vscode/test-electron";

import { Logger } from "@retrieves/utils/logging";

async function main(): Promise<void> {
  try {
    /*
     * The folder containing the Extension Manifest package.json
     * Passed to `--extensionDevelopmentPath`
     */
    const extensionDevelopmentPath = resolve(__dirname, "../../");

    /*
     * The path to test runner
     * Passed to --extensionTestsPath
     */
    const extensionTestsPath = resolve(__dirname, "./functional/index");

    // Download VS Code, unzip it and run the integration test
    await runTests({ extensionDevelopmentPath, extensionTestsPath });
  } catch (err) {
    Logger.error("Failed to run tests", err);
    process.exit(1);
  }
}

void main();
