import type { ApolloError } from "@apollo/client";
import type { DiagnosticSeverity, Range } from "vscode";
import { Diagnostic, Uri } from "vscode";

import { getAppUrl } from "./utils/url";

interface IAttackedFile {
  success: boolean;
  message?: string;
}

interface IBugsnagConfig {
  isTelemetryEnabled: boolean;
  stage: string;
  retrievesVersion: string;
}

interface IGroup {
  name: string;
  userRole: string;
  subscription: "continuous" | "oneshot";
  roots: IGitRoot[] | null;
}

interface IStakeholder {
  email: string;
  role: string;
  invitationState: "PENDING" | "REGISTERED" | "UNREGISTERED";
}

interface IOrganization {
  groups: IGroup[];
  name: string;
}

interface IGitRoot {
  id: string;
  nickname: string;
  groupName: string;
  state: "ACTIVE" | "INACTIVE";
  gitignore: string[];
  downloadUrl?: string | null;
  url: string;
  userRole?: string;
  gitEnvironmentUrls: {
    id: string;
    url: string;
  }[];
}

interface IRootInfo {
  groupName: string;
  nickname: string;
  fileRelativePath: string;
}

interface IToeLines {
  attackedLines: number;
  bePresent: boolean;
  filename: string;
  comments: string;
  modifiedDate: string;
  loc: number;
  fileExists?: boolean;
  sortsPriorityFactor: number;
}

interface IVulnerability {
  commitHash: string;
  id: string;
  isAutoFixable: boolean;
  specific: string;
  where: string;
  reportDate: string;
  rootNickname: string;
  state: "REJECTED" | "SAFE" | "SUBMITTED" | "VULNERABLE";
  treatmentStatus?:
    | "ACCEPTED_UNDEFINED"
    | "ACCEPTED"
    | "IN_PROGRESS"
    | "UNTREATED";
  finding: IFinding;
  verification?: "On_hold" | "Requested" | "Verified" | null;
}

interface IFinding {
  description: string;
  id: string;
  title: string;
  groupName: string;
  maxOpenSeverityScoreV4: number;
}

interface IFindingDescription {
  attackVectorDescription: string;
  description: string;
  groupName: string;
  id: string;
  minTimeToRemediate?: number;
  recommendation: string;
  maxOpenSeverityScoreV4: number;
  severityVectorV4: string;
  threat: string;
  title: string;
  vulnerabilitiesSummaryV4: {
    open: number;
  };
}

interface IUserInfo {
  userEmail: string;
  role: string;
  error?: ApolloError;
}

class VulnerabilityDiagnostic extends Diagnostic {
  public vulnerabilityId?: string;

  public findingId?: string;

  public constructor(
    vulnerability: IVulnerability,
    range: Range,
    message: string,
    severity?: DiagnosticSeverity,
  ) {
    super(range, message, severity);

    this.vulnerabilityId = vulnerability.id;
    this.findingId = vulnerability.finding.id;
    const baseUrl = getAppUrl();
    this.code = {
      target: Uri.parse(
        `${baseUrl}/groups/${vulnerability.finding.groupName}/vulns/${vulnerability.finding.id}/locations/${vulnerability.id}`,
      ),
      value: vulnerability.finding.title,
    };
    this.source = "fluidattacks";
  }
}

export type {
  IAttackedFile,
  IBugsnagConfig,
  IFinding,
  IFindingDescription,
  IGitRoot,
  IGroup,
  IOrganization,
  IStakeholder,
  IToeLines,
  IRootInfo,
  IUserInfo,
  IVulnerability,
};

export { VulnerabilityDiagnostic };
