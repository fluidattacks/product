import { basename } from "path";

import type { Uri } from "vscode";

import type { IGitRoot } from "@retrieves/types";

export const shouldEnableHackerMode = (
  currentWorkingUri: Uri,
  gitRoot: IGitRoot | undefined,
  globalRole: string,
): boolean => {
  const nickname = basename(currentWorkingUri.path);

  return (
    currentWorkingUri.path.includes("groups") &&
    (globalRole === "admin" ||
      gitRoot?.userRole === "hacker" ||
      (nickname === "groups" && globalRole === "hacker"))
  );
};
