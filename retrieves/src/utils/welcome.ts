import { window } from "vscode";

import type { FindingsProvider } from "@retrieves/providers/findings";
import type { FindingTreeItem } from "@retrieves/treeItems/finding";

export const showWelcomeTab = (): void => {
  window.createTreeView("vulnerabilities", {
    showCollapseAll: true,
    treeDataProvider: {
      getChildren: async (_element?: FindingTreeItem): Promise<never[]> =>
        Promise.resolve([]),
    } as unknown as FindingsProvider,
  });
};
