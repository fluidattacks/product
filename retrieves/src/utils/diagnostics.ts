import { range } from "ramda";
import type { DiagnosticCollection } from "vscode";
import { window } from "vscode";

import type { VulnerabilityDiagnostic } from "@retrieves/types";

export const getDiagnosticsForTreatment = (
  retrievesDiagnostics: DiagnosticCollection,
): readonly VulnerabilityDiagnostic[] | undefined => {
  const { activeTextEditor } = window;
  if (!activeTextEditor) {
    return undefined;
  }
  const fileDiagnostics: readonly VulnerabilityDiagnostic[] | undefined =
    retrievesDiagnostics.get(activeTextEditor.document.uri);

  if (!fileDiagnostics) {
    void window.showInformationMessage(
      "This line does not contain vulnerabilities",
    );

    return undefined;
  }
  if (
    range(
      activeTextEditor.selection.start.line,
      activeTextEditor.selection.end.line + 1,
    ).length > 1
  ) {
    void window.showErrorMessage(
      "Please make a request to change treatment one vulnerability at a time",
    );

    return undefined;
  }

  return fileDiagnostics;
};
