import { split } from "@apollo/client";
import {
  ApolloClient,
  InMemoryCache,
  createHttpLink,
  resetCaches,
} from "@apollo/client/core";
import type {
  ApolloLink,
  NormalizedCacheObject,
  Operation,
} from "@apollo/client/core";
import { setContext } from "@apollo/client/link/context";
import { RetryLink } from "@apollo/client/link/retry";
import { GraphQLWsLink } from "@apollo/client/link/subscriptions";
import { getMainDefinition } from "@apollo/client/utilities";
import fetch from "cross-fetch";
import {
  type FragmentDefinitionNode,
  Kind,
  type OperationDefinitionNode,
  OperationTypeNode,
} from "graphql";
import { createClient } from "graphql-ws";
import { WebSocket } from "ws";

import packageJson from "@retrieves/../package.json";
import { RetrievesAuthentication } from "@retrieves/utils/auth";
import { getAppUrl } from "@retrieves/utils/url";
// Don't add vscode or node imports here as that may impact webviews

interface IClient {
  headers: {
    authorization: string;
  };
}

const getClient = (): ApolloClient<NormalizedCacheObject> => {
  resetCaches();
  const authLink = setContext(async (): Promise<IClient> => {
    const token = await RetrievesAuthentication.instance.getToken();

    return {
      headers: {
        authorization: `Bearer ${token}`,
      },
    };
  });
  const httpLink = createHttpLink({
    fetch,
    headers: {
      "User-Agent": `Retrieves/${packageJson.version} Apollo/${packageJson.dependencies["@apollo/client"]}`,
    },
    uri: `${getAppUrl()}/api`,
  });
  const retryLink = new RetryLink({
    attempts: {
      max: 6,
      retryIf: (error, _operation): boolean => Boolean(error),
    },
    delay: {
      initial: 2500,
      jitter: true,
    },
  });
  const httpLinkWithRetry = retryLink.concat(httpLink);
  const wsLink = new GraphQLWsLink(
    createClient({
      connectionAckWaitTimeout: Infinity,
      connectionParams: async (): Promise<{ Authorization: string }> => {
        const token = await RetrievesAuthentication.instance.getToken();

        return { Authorization: token };
      },
      keepAlive: 10000,
      shouldRetry: (): boolean => true,
      url: "wss://app.fluidattacks.com/api",
      webSocketImpl: WebSocket,
    }),
  );
  const apiLink: ApolloLink = split(
    ({ query }: Operation): boolean => {
      const definition: FragmentDefinitionNode | OperationDefinitionNode =
        getMainDefinition(query);

      return (
        definition.kind === Kind.OPERATION_DEFINITION &&
        definition.operation === OperationTypeNode.SUBSCRIPTION
      );
    },
    wsLink,
    httpLinkWithRetry,
  );

  return new ApolloClient({
    cache: new InMemoryCache({
      typePolicies: {
        GitRoot: { keyFields: ["id"] },
        Group: { keyFields: ["name"] },
        Me: { keyFields: ["userEmail"] },
        Query: {
          fields: {
            me: { merge: true },
          },
        },
        Vulnerability: { keyFields: ["id"] },
      },
    }),
    defaultOptions: {
      mutate: { fetchPolicy: "network-only" },
      query: { fetchPolicy: "cache-first" },
    },
    link: authLink.concat(apiLink),
  });
};

export { getClient };
