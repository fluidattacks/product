import type { ApolloError } from "@apollo/client";
import { commands, window } from "vscode";

import { Logger } from "./logging";

import { getClient } from "@retrieves/utils/api";

const API_CLIENT = getClient();

const unavailabilityMessage = async (): Promise<void> => {
  const actions = ["Reload", "Later"];

  await window
    .showErrorMessage(
      "Our platform's API endpoint is currently unavailable. Please refresh in a moment or reload the extension",
      ...actions,
    )
    .then(
      async (selectedAction): Promise<void> => {
        if (selectedAction === "Reload") {
          Logger.info("Reloading extension host");
          await commands.executeCommand(
            "workbench.action.restartExtensionHost",
          );
        }
      },
      (reason: unknown): void => {
        Logger.info("Dismissed reload:", reason);
      },
    );
};

const handleGraphQlError = async (apiError: ApolloError): Promise<void> => {
  switch (apiError.message) {
    case "Login required":
      await window.showErrorMessage(
        "This token has no permissions in this repo. Please retry with a valid API token",
      );
      break;
    case "Exception - User token has expired":
      await window.showErrorMessage(
        "This token has expired. Please retry with a valid API token",
      );
      break;
    case "Response not successful: Received status code 504": {
      await unavailabilityMessage();
      break;
    }
    case "Token format unrecognized":
      await window.showErrorMessage(
        "Token format unrecognized. Please retry with a valid API token",
      );
      break;
    case "Exception - Fixes are not enabled for this finding":
      void window.showErrorMessage("Fixes are not enabled for this finding");
      break;
    case "Assigned not valid":
      await window.showErrorMessage("Invalid assigned user for this treatment");
      break;
    default:
      if (apiError.message.includes("Access denied")) {
        await window.showErrorMessage(
          "This token has no permissions in this repo. Please retry with a valid API token",
        );
      } else if (apiError.message.includes("is_under_review")) {
        // Do not alert the user about random frozen groups for now
        break;
      } else if (
        apiError.message.startsWith("Exception - Failed to generate a")
      ) {
        await window.showErrorMessage(
          `Could not generate an automatic fix for this vulnerability. If this
          persists, feel free to contact us and attach the extension log or a
          screenshot of it, which can be found with the Show Extension Log
          command`,
        );
      } else if (
        apiError.message.includes("request") &&
        apiError.message.includes("failed")
      ) {
        await unavailabilityMessage();
        // eslint-disable-next-line no-negated-condition
      } else if (!apiError.message.includes(" ")) {
        /*
         * Most likely an API error coming from the Integrates backend, this
         * condition should catch common dict KeyErrors
         */
        await window.showErrorMessage(
          `Query error. If this persists, feel free to contact us and
          attach the extension log or a screenshot of it, which can be found
          with the Show Extension Log command`,
        );
      } else {
        await window.showErrorMessage(apiError.message);
      }
  }
};

export { API_CLIENT, handleGraphQlError };
