/** Build app url according to environment */
export const getAppUrl = (): string => {
  return process.env.APP_URL ?? "https://app.fluidattacks.com";
};
