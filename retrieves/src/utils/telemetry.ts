/* eslint-disable functional/immutable-data */
import type { Event } from "@bugsnag/core";
import type { Error } from "@bugsnag/core/types/event";
import Bugsnag from "@bugsnag/js";
import BugsnagPluginReact from "@bugsnag/plugin-react";
import type {
  BugsnagErrorBoundary,
  BugsnagPluginReactResult,
} from "@bugsnag/plugin-react";
import _ from "lodash";
import React, { Fragment } from "react";
import { type ExtensionContext, env } from "vscode";

import { getEnvironment } from "@retrieves/utils/environment";

const isCausedByRetrieves = (event: Event): boolean => {
  return event.errors.some((error): boolean => {
    return error.stacktrace.some(
      (stackframe): boolean =>
        stackframe.file.includes("retrieves") ||
        stackframe.file.includes("fluidattacks"),
    );
  });
};

const startBugsnag = (context: ExtensionContext): void => {
  Bugsnag.start({
    apiKey: "ca660653ea33c94979c1bb51f3fb99e8",
    appType: "extension",
    appVersion: (context.extension.packageJSON as Record<string, string>)
      .version,
    onError: (event: Event): boolean => {
      event.setUser(undefined, context.globalState.get("userEmail"));
      // Strip out unneeded device info
      const { osName, osVersion, runtimeVersions, time } = event.device;
      event.device = { osName, osVersion, runtimeVersions, time };
      event.errors.forEach((error: Error): void => {
        const message: string | undefined = event.context;
        event.context = error.errorMessage;
        error.errorMessage = _.isString(message) ? message : "";
        event.groupingHash = event.context;
      });

      // Sending metadata will take the isTelemetryEnabled option into account
      return env.isTelemetryEnabled && isCausedByRetrieves(event);
    },
    plugins: [new BugsnagPluginReact(React)],
    releaseStage: getEnvironment(context),
  }).startSession();
};

const reactPlugin: BugsnagPluginReactResult | undefined =
  Bugsnag.getPlugin("react");

const bugsnagErrorBoundary: BugsnagErrorBoundary = _.isUndefined(reactPlugin)
  ? Fragment
  : reactPlugin.createErrorBoundary(React);

export { startBugsnag, bugsnagErrorBoundary as BugsnagErrorBoundary };
