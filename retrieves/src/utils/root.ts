import { flatten } from "ramda";
import type { QuickPickItem } from "vscode";
import { window } from "vscode";

import { Logger } from "./logging";

import { getGroupGitRootsSimple } from "@retrieves/api/root";
import type { IGitRoot, IGroup } from "@retrieves/types";

const disambiguateRoot = async (
  roots: IGitRoot[],
): Promise<IGitRoot | undefined> => {
  if (roots.length === 0) {
    return undefined;
  } else if (roots.length === 1) {
    return roots[0];
  }
  const pick = await window.showQuickPick(
    roots.map((root): QuickPickItem => {
      return {
        description: root.groupName,
        detail: root.url,
        label: root.nickname,
      };
    }),
    { canPickMany: false, title: "Select the root to analyze" },
  );
  const chosenRoot = roots.find(
    (root): boolean =>
      root.nickname === pick?.label &&
      root.groupName === pick.description &&
      root.url === pick.detail,
  );

  if (chosenRoot === undefined) {
    Logger.info("No root was selected among the options");
  }

  return chosenRoot;
};

const normalizeUrl = (url: string): string => {
  /*
   * For future reference:
   * Git URLs can come in many different protocols like HTTP, SSH, FTP and
   * Git itself.
   * Most can be split from the repository site (gitlab, github, ...) by a
   * .com/ part (HTTP) or .com: (SSH & Git). So that we can compare the inner
   * path, which is all Retrieves truly cares about.
   *
   * There are a couple of special cases though:
   * - Bitbucket SSH URLs use .org/ (SSH) and .org: (Git)
   * - Azure SSH URLs use .com:v3/
   * - Codecommit HTTPS & SSH URLs use .com/v1/
   * - Some private repositories on premises may have .com.<country>:<port>
   * The regex below tries to capture all these variations.
   */
  const regex =
    /(?:\.com:v3\/|\.com\.[^/]+\/|\.(?<topssh>com|org):|\.(?<tophttp>com|org)\/)/u;
  const [trimmedUrl] = url.split(regex).slice(-1);

  // Remove .git but only if its at the end and the /_git/ part of Azure URLs
  return trimmedUrl.replace(/\.git$/u, "").replace(/\/_git\//u, "/");
};

const identifyRoots = async (
  groups: IGroup[],
  gitRemote: string,
  globalRole: string,
  nickname: string,
): Promise<IGitRoot[]> => {
  const trimmedGitRemote = normalizeUrl(gitRemote);
  if (trimmedGitRemote === gitRemote) {
    Logger.info(`Couldn't normalize git remote URL: ${gitRemote}`);
  }

  return flatten(
    await Promise.all(
      groups.map(async (group: IGroup): Promise<IGitRoot[]> => {
        if (group.roots === null) {
          return [];
        }
        const roots =
          group.roots.length || globalRole !== "admin"
            ? group.roots
            : await getGroupGitRootsSimple(group.name);

        return roots
          .filter((root): boolean => root.state === "ACTIVE")
          .map(
            (root): IGitRoot => ({
              ...root,
              groupName: group.name,
              userRole: group.userRole,
            }),
          );
      }),
    ),
  ).filter((root): boolean => {
    return (
      // Remote URLs can vary depending on the cloning method
      root.nickname === nickname ||
      root.groupName === nickname ||
      root.url.includes(trimmedGitRemote) ||
      // Azure URLs sometimes have a _git that can throw off the str comparison
      normalizeUrl(root.url) === trimmedGitRemote
    );
  });
};

export { disambiguateRoot, identifyRoots, normalizeUrl };
