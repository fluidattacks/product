import { existsSync } from "fs";
import path from "path";

import type { ParsedDiff } from "diff";
import { parsePatch } from "diff";
import type { SimpleGit } from "simple-git";
import { GitError } from "simple-git";

import { Logger } from "./logging";

import { getGitRootVulnerabilities } from "@retrieves/api/root";
import type { IGitRoot, IVulnerability } from "@retrieves/types";

/**
 * Fetches relevant vulnerabilities to Retrieves to be fed to the TreeView
 * and Diagnostics
 */
const fetchGitRootVulnerabilities = async (
  gitRoot: IGitRoot,
  rootPath: string,
): Promise<IVulnerability[]> => {
  const vulnerabilities = await getGitRootVulnerabilities(
    gitRoot.groupName,
    gitRoot.nickname,
  );

  return vulnerabilities.filter((vulnerability): boolean => {
    const fullPath = path.join(
      rootPath,
      vulnerability.where.substring(vulnerability.where.indexOf("/") + 1),
    );

    return (
      ["VULNERABLE", "SUBMITTED"].includes(vulnerability.state) &&
      existsSync(fullPath)
    );
  });
};

const getDiff = async (
  git: SimpleGit,
  revA: string,
  revB?: string,
  filePath?: string,
): Promise<ParsedDiff[] | null> => {
  try {
    const diffArgs = [
      "--color=never",
      "--minimal",
      "--patch",
      "--unified=0",
      revA,
      revB === undefined ? "" : `...${revB}`,
      "--",
      ...(filePath === undefined ? [] : [filePath]),
    ];

    const diffOutput = await git.diff(diffArgs);

    if (diffOutput) {
      return parsePatch(diffOutput);
    }

    return null;
  } catch (error) {
    if (error instanceof GitError) {
      // Handle the Git error as necessary.
      Logger.error("Git error occurred:", error);
    }

    return null;
  }
};

async function rebaseOneCommitAtATime(
  git: SimpleGit,
  vulnerabilityPath: string,
  line: number,
  revA: string,
  revB?: string,
): Promise<{ path: string; line: number; rev: string | undefined } | null> {
  try {
    const parsedDiff = await getDiff(git, revA, revB, vulnerabilityPath);
    if (!parsedDiff || parsedDiff.length === 0) {
      return null;
    }

    const fileDiff = parsedDiff.find(
      (diff): boolean => diff.oldFileName === `a/${vulnerabilityPath}`,
    );

    if (fileDiff?.newFileName === undefined) {
      return null;
    }

    // Update the path since the file was moved
    const newPath = fileDiff.newFileName.slice(2);

    const rebasedLine = fileDiff.hunks.reduce((acc, hunk): number => {
      if (line < hunk.oldStart) {
        // Line is before the hunk, no change
        return acc;
      }
      if (line > hunk.oldStart + hunk.oldLines - 1) {
        // Line is after the hunk, adjust the line number
        return acc + hunk.newLines - hunk.oldLines;
      }

      // Line falls within the hunk, cannot rebase deterministically
      return -1;
    }, line);

    if (rebasedLine === -1) {
      return null;
    }

    return { line: rebasedLine, path: newPath, rev: revB };
  } catch (error) {
    Logger.error("Failed to rebase commit", error);

    return null;
  }
}

const rebase = async (
  git: SimpleGit,
  vulnerabilityCommit: string,
  vulnerabilityPath: string,
  vulnerabilityLine: number,
): Promise<number | null> => {
  try {
    const result = (
      await git.raw([
        "blame",
        "--reverse",
        `${vulnerabilityCommit}..HEAD`,
        "-L",
        `${vulnerabilityLine},+1`,
        "-l",
        "-p",
        "--show-number",
        "--show-name",
        "--",
        vulnerabilityPath,
      ])
    ).split("\n");
    const newLine = parseInt(result[0].split(" ")[1], 10);
    if (vulnerabilityLine === newLine) {
      return vulnerabilityLine;
    }
    const rebaseHead = await rebaseOneCommitAtATime(
      git,
      vulnerabilityPath,
      newLine,
      "HEAD",
    );
    if (rebaseHead !== null) {
      return rebaseHead.line;
    }

    return newLine;
  } catch (error) {
    if (error instanceof GitError) {
      // The specified commit is equivalent to HEAD
      if (error.message.includes("More than one commit to dig up from")) {
        return vulnerabilityLine;
      }
      // Handle the Git error as necessary.
      Logger.error("Git error occurred:", error.message);
    }

    return null;
  }
};

const commitExists = async (
  repo: SimpleGit,
  commitHash: string,
): Promise<boolean> => {
  try {
    await repo.show(commitHash);
  } catch {
    return false;
  }

  return true;
};

export { rebase, fetchGitRootVulnerabilities, commitExists };
