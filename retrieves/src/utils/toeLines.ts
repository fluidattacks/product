import { partialRight } from "ramda";
import { window } from "vscode";

import { validAttackedLines } from "./validations";

export const getAttackedLines = async (
  loc: number,
): Promise<number | undefined> => {
  const attackedLines = await window.showInputBox({
    prompt: `Please fill in a value between 0 and ${loc} (LoC)`,
    title: "Update Attacked Lines",
    validateInput: partialRight(validAttackedLines, [loc]),
    value: loc.toString(),
  });

  return attackedLines === undefined ? undefined : Number(attackedLines);
};
