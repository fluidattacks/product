import type { SecretStorage } from "vscode";

/** Resolve token at the extension's activation */
const resolveToken = async (
  secrets: SecretStorage,
): Promise<string | undefined> => {
  return process.env.FLUID_API_TOKEN ?? (await secrets.get("fluidToken"));
};

export { resolveToken };
