import _ from "lodash";

import type { IStakeholder } from "@retrieves/types";

const isFluidUser = (email: string): boolean =>
  email.endsWith("@fluidattacks.com");

const canBeAssignedVulns = (role: string): boolean =>
  ["user", "group_manager", "vulnerability_manager"].includes(role);

export const filterAssignableUsers = (
  requesterEmail: string,
  stakeholders: IStakeholder[],
): IStakeholder[] => {
  return _.sortBy(
    stakeholders.filter((stakeholder): boolean => {
      return (
        stakeholder.invitationState === "REGISTERED" &&
        canBeAssignedVulns(stakeholder.role) &&
        (isFluidUser(requesterEmail) || !isFluidUser(stakeholder.email))
      );
    }),
    "email",
  );
};
