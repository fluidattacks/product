/* eslint-disable jest/max-expects */
/* eslint-disable max-lines */
import { join } from "path";

import type { ParsedDiff } from "diff";
import { parsePatch } from "diff";
import { sync } from "glob";
import { GitError, type SimpleGit } from "simple-git";
import type {
  DiagnosticCollection,
  ExtensionContext,
  QuickPickItem,
  SecretStorage,
  Webview,
} from "vscode";
import {
  DiagnosticSeverity,
  ExtensionMode,
  InputBoxValidationSeverity,
  Position,
  Range,
  Uri,
  ViewColumn,
  window,
  workspace,
} from "vscode";

import { RetrievesAuthentication } from "./auth";
import { getDiagnosticsForTreatment } from "./diagnostics";
import { getGroupsPath, getRootInfoFromPath, ignoreFiles } from "./file";
import { filterAssignableUsers } from "./groups";
import { shouldEnableHackerMode } from "./hackerMode";
import { Logger } from "./logging";
import { disambiguateRoot, identifyRoots, normalizeUrl } from "./root";
import { mapScoreToCategory } from "./severityScore";
import { resolveToken } from "./token";
import { getAppUrl } from "./url";
import {
  validAttackedLines,
  validDateField,
  validJustification,
  validTextField,
} from "./validations";
import {
  commitExists,
  fetchGitRootVulnerabilities,
  rebase,
} from "./vulnerabilities";
import { getUri, getWebviewContent } from "./webview";

import { getGroups } from "@retrieves/api/groups";
import { getGitRootVulnerabilities } from "@retrieves/api/root";
import { getMockFinding, getMockRoots } from "@retrieves/mocks/data";
import type {
  IFinding,
  IGitRoot,
  IGroup,
  IRootInfo,
  IStakeholder,
  IVulnerability,
} from "@retrieves/types";
import { VulnerabilityDiagnostic } from "@retrieves/types";

jest.mock(
  "glob",
  (): Record<string, unknown> => ({
    ...jest.requireActual("glob"),
    sync: jest.fn(),
  }),
);
jest.mock(
  "diff",
  (): Record<string, unknown> => ({
    ...jest.requireActual("diff"),
    parsePatch: jest.fn(),
  }),
);

describe("test utils", (): void => {
  it("should resolve the token at startup", async (): Promise<void> => {
    expect.hasAssertions();

    const unsetToken = await resolveToken({
      get: jest.fn(
        async (_): Promise<string | undefined> => Promise.resolve(undefined),
      ),
    } as unknown as SecretStorage);

    expect(unsetToken).toBeUndefined();

    const token = await resolveToken({
      get: jest.fn(
        async (_): Promise<string | undefined> =>
          Promise.resolve("test.jwt.token"),
      ),
    } as unknown as SecretStorage);

    expect(token).toBe("test.jwt.token");
  });

  it("should retrieve the token from the store", async (): Promise<void> => {
    expect.hasAssertions();

    const token = await RetrievesAuthentication.instance.getToken();

    expect(token).toBe("test.jwt.token");
  });

  it("should retrieve the app url from the store", (): void => {
    expect.hasAssertions();

    const appUrl = getAppUrl();

    expect(appUrl).toBe("https://app.fluidattacks.com");
  });

  it("should identify the root", async (): Promise<void> => {
    expect.hasAssertions();

    const groups = await getGroups();
    const gitRemote = "http://gitrepos.com/org/repo.git";
    const globalRole = "user";
    const nickname = "root1";
    const expectedRoot = {
      groupName: "group2",
      id: "git-root-1",
      nickname: "root1",
      state: "ACTIVE",
      url: "http://gitrepos.com/org/repo.git",
      userRole: "user",
    } as IGitRoot;

    const [gitRoot] = await identifyRoots(
      groups,
      gitRemote,
      globalRole,
      nickname,
    );

    expect(gitRoot).toMatchObject(expectedRoot);

    /*
     * We don't want the identification to fail if the repo was cloned in
     * another way and the root kept its nickname
     */
    const otherGitRemote = "ssh://git@gitrepos.com:org/other-repo.git";
    const [otherRoot] = await identifyRoots(
      groups,
      otherGitRemote,
      globalRole,
      nickname,
    );

    expect(otherRoot).toMatchObject(expectedRoot);

    /*
     * If the nickname aka the root folder doesn't match, the fallback is the
     * registered gitRemote in our Platform
     */
    const otherNickname = "renamedRoot";
    const [renamedRoot] = await identifyRoots(
      groups,
      gitRemote,
      globalRole,
      otherNickname,
    );

    expect(renamedRoot).toMatchObject(expectedRoot);

    // Azure roots tend to have some discrepancies between HTTPS and SSH URLs
    const azureGroup: IGroup = {
      name: "group1",
      roots: [
        {
          downloadUrl: "azure-url-here",
          gitEnvironmentUrls: [{ id: "env-1", url: "https://env1.com/api" }],
          gitignore: [],
          groupName: "group1",
          id: "git-root-1",
          nickname: "root1",
          state: "ACTIVE",
          url: "https://dev.azure.com/my-org/my-project/_git/my-repo",
        },
      ],
      subscription: "continuous",
      userRole: "user",
    };
    const azureGitRemote = "git@ssh.dev.azure.com:v3/my-org/my-project/my-repo";
    const expectedAzureRoot = {
      groupName: "group1",
      id: "git-root-1",
      nickname: "root1",
      state: "ACTIVE",
      url: "https://dev.azure.com/my-org/my-project/_git/my-repo",
      userRole: "user",
    } as IGitRoot;
    const [azureRoot] = await identifyRoots(
      [azureGroup],
      azureGitRemote,
      globalRole,
      otherNickname,
    );

    expect(azureRoot).toMatchObject(expectedAzureRoot);

    // CodeCommit example
    const awsGroup: IGroup = {
      name: "group1",
      roots: [
        {
          downloadUrl: "azure-url-here",
          gitEnvironmentUrls: [{ id: "env-1", url: "https://env1.com/api" }],
          gitignore: [],
          groupName: "group1",
          id: "git-root-1",
          nickname: "root1",
          state: "ACTIVE",
          url: "https://git-codecommit.us-east-2.amazonaws.com/v1/repos/MyDemoRepo",
        },
      ],
      subscription: "continuous",
      userRole: "user",
    };

    const codeCommitGitRemote =
      "ssh://git-codecommit.us-east-2.amazonaws.com/v1/repos/MyDemoRepo";
    const expectedCodeCommitRoot = {
      groupName: "group1",
      id: "git-root-1",
      nickname: "root1",
      state: "ACTIVE",
      url: "https://git-codecommit.us-east-2.amazonaws.com/v1/repos/MyDemoRepo",
      userRole: "user",
    } as IGitRoot;
    const [codeCommitRoot] = await identifyRoots(
      [awsGroup],
      codeCommitGitRemote,
      globalRole,
      otherNickname,
    );

    expect(codeCommitRoot).toMatchObject(expectedCodeCommitRoot);

    // If both attributes cannot be matched, we cannot guess the root
    const [undefinedGitroot] = await identifyRoots(
      groups,
      otherGitRemote,
      globalRole,
      otherNickname,
    );

    expect(undefinedGitroot).toBeUndefined();

    // If url cannot be normalize, we cannot guess the root
    const errorGitRemote = "https://badurl.com";
    const infoSpy = jest.spyOn(Logger, "info");
    Logger.info(`Couldn't normalize git remote URL: ${errorGitRemote}`);
    await identifyRoots(groups, errorGitRemote, globalRole, nickname);

    expect(infoSpy).toHaveBeenCalledWith(
      `Couldn't normalize git remote URL: ${errorGitRemote}`,
    );

    // Role admin
    const adminRole = "admin";

    const [gitRootAdmin] = await identifyRoots(
      groups,
      gitRemote,
      adminRole,
      nickname,
    );
    const expectedRootAdmin = {
      ...expectedRoot,
      groupName: "test-group",
      userRole: "",
    } as IGitRoot;

    expect(gitRootAdmin).toMatchObject(expectedRootAdmin);
  });

  it("should correctly filter assignable stakeholders", (): void => {
    expect.hasAssertions();

    const stakeholders: IStakeholder[] = [
      {
        email: "john@company.com",
        invitationState: "REGISTERED",
        role: "user",
      },
      {
        email: "jane@company.com",
        invitationState: "REGISTERED",
        role: "group_manager",
      },
      {
        email: "bill@company.com",
        invitationState: "REGISTERED",
        role: "vulnerability_manager",
      },
      {
        email: "bob@company.com",
        invitationState: "PENDING",
        role: "vulnerability_manager",
      },
      {
        email: "dev@fluidattacks.com",
        invitationState: "REGISTERED",
        role: "user",
      },
      {
        email: "hacker@fluidattacks.com",
        invitationState: "REGISTERED",
        role: "hacker",
      },
    ];
    const email = "client@outsidedomain.com";

    const assignableUsers = filterAssignableUsers(email, stakeholders);

    expect(assignableUsers).toStrictEqual([
      {
        email: "bill@company.com",
        invitationState: "REGISTERED",
        role: "vulnerability_manager",
      },
      {
        email: "jane@company.com",
        invitationState: "REGISTERED",
        role: "group_manager",
      },
      {
        email: "john@company.com",
        invitationState: "REGISTERED",
        role: "user",
      },
    ]);

    const fluidEmail = "employee@fluidattacks.com";

    const fluidAssignableUsers = filterAssignableUsers(
      fluidEmail,
      stakeholders,
    );

    expect(fluidAssignableUsers).toStrictEqual([
      {
        email: "bill@company.com",
        invitationState: "REGISTERED",
        role: "vulnerability_manager",
      },
      {
        email: "dev@fluidattacks.com",
        invitationState: "REGISTERED",
        role: "user",
      },
      {
        email: "jane@company.com",
        invitationState: "REGISTERED",
        role: "group_manager",
      },
      {
        email: "john@company.com",
        invitationState: "REGISTERED",
        role: "user",
      },
    ]);
  });

  it("should normalize urls properly", (): void => {
    expect.hasAssertions();

    // Should return the same URL if the regex cannot match its structure
    expect(normalizeUrl("https://badurl.com")).toBe("https://badurl.com");

    const privateUrl =
      "https://private.site.com.co:8000/workspace/repository.git";

    expect(normalizeUrl(privateUrl)).toBe("workspace/repository");

    const gitlabHttpsUrl = "https://gitlab.com/fluidattacks/universe.git";
    const gitlabSshUrl = "ssh://git@gitlab.com:fluidattacks/universe.git";
    const gitlabGitUrl = "git@gitlab.com:fluidattacks/universe.git";

    expect(
      [gitlabHttpsUrl, gitlabSshUrl, gitlabGitUrl].map(normalizeUrl),
    ).toStrictEqual(
      Array.from({ length: 3 }, (): string => "fluidattacks/universe"),
    );

    const githubHttpsUrl = "https://github.com/fluidattacks/makes.git";
    const githubSshUrl = "ssh://git@github.com:fluidattacks/makes.git";
    const githubGitUrl = "git@github.com:fluidattacks/makes.git";

    expect(
      [githubHttpsUrl, githubSshUrl, githubGitUrl].map(normalizeUrl),
    ).toStrictEqual(
      Array.from({ length: 3 }, (): string => "fluidattacks/makes"),
    );

    const azureHttpsUrl =
      "https://fluidattacks@dev.azure.com/fluidattacks/prod/vpn_utils.git";
    const azureSshUrl =
      "ssh://git@ssh.dev.azure.com:v3/fluidattacks/prod/vpn_utils.git";
    const azureGitUrl =
      "git@ssh.dev.azure.com:v3/fluidattacks/prod/vpn_utils.git";

    expect(
      [azureHttpsUrl, azureSshUrl, azureGitUrl].map(normalizeUrl),
    ).toStrictEqual(
      Array.from({ length: 3 }, (): string => "fluidattacks/prod/vpn_utils"),
    );

    expect(
      normalizeUrl("git@ssh.dev.azure.com:v3/my-org/my-project/my-repo"),
    ).toBe(
      normalizeUrl("https://dev.azure.com/my-org/my-project/_git/my-repo"),
    );

    const bitbucketHttpsUrl =
      "https://fluidattacks@bitbucket.org/prod/universe.git";
    const bitbucketSshUrl = "ssh://git@bitbucket.org/prod/universe.git";
    const bitbucketGitUrl = "git@bitbucket.org:prod/universe.git";

    expect(
      [bitbucketHttpsUrl, bitbucketSshUrl, bitbucketGitUrl].map(normalizeUrl),
    ).toStrictEqual(Array.from({ length: 3 }, (): string => "prod/universe"));

    const codeCommitHttpsUrl =
      "https://git-codecommit.us-east-2.amazonaws.com/v1/repos/MyDemoRepo";
    const codeCommitSshUrl =
      "ssh://git-codecommit.us-east-2.amazonaws.com/v1/repos/MyDemoRepo";

    expect(
      [codeCommitHttpsUrl, codeCommitSshUrl].map(normalizeUrl),
    ).toStrictEqual(
      Array.from({ length: 2 }, (): string => "v1/repos/MyDemoRepo"),
    );
  });

  it("should select disambiguate root", async (): Promise<void> => {
    expect.hasAssertions();

    const roots: IGitRoot[] = getMockRoots("testGroup");
    const rootOptions = [
      {
        description: "testGroup",
        detail: "http://gitrepos.com/org/repo.git",
        label: "root1",
      },
      {
        description: "testGroup",
        detail: "ssh://git@gitrepos.com:org/dev-utils.git",
        label: "root2",
      },
      {
        description: "testGroup",
        detail: "git@gitrepos.com:org/deprecated-repo.git",
        label: "root3",
      },
    ];
    const selectedRoot = {
      description: "testGroup",
      detail: "ssh://git@gitrepos.com:org/dev-utils.git",
      label: "root2",
    };

    jest.mocked(window.showQuickPick).mockResolvedValue(selectedRoot);

    const root = await disambiguateRoot(roots);

    expect(window.showQuickPick).toHaveBeenCalledWith(
      rootOptions as QuickPickItem[],
      { canPickMany: false, title: "Select the root to analyze" },
    );

    expect(root).toMatchObject(roots[1]);

    // No selected root

    jest.mocked(window.showQuickPick).mockResolvedValue(undefined);

    const rootNoSelect = await disambiguateRoot(roots);

    expect(window.showQuickPick).toHaveBeenCalledWith(
      rootOptions as QuickPickItem[],
      { canPickMany: false, title: "Select the root to analyze" },
    );

    expect(rootNoSelect).toBeUndefined();

    const infoSpy = jest.spyOn(Logger, "info");
    Logger.info("No root was selected among the option");

    expect(infoSpy).toHaveBeenCalledWith(
      "No root was selected among the option",
    );

    // No roots provided

    const rootsNon = await disambiguateRoot([]);

    expect(window.showQuickPick).toHaveBeenCalledTimes(2);
    expect(rootsNon).toBeUndefined();

    // There is just one root, default

    const oneRoot = [
      {
        __typename: "GitRoot",
        downloadUrl: "pre-signed-url-here",
        gitEnvironmentUrls: [{ id: "env-1", url: "https://env1.com/api" }],
        gitignore: [],
        groupName: "testGroup",
        id: "git-root-1",
        nickname: "root1",
        state: "ACTIVE",
        url: "http://gitrepos.com/org/repo.git",
      } as IGitRoot,
    ];
    jest.mocked(window.showQuickPick).mockResolvedValue(selectedRoot);

    const defaultRoot = await disambiguateRoot(oneRoot);

    expect(window.showQuickPick).toHaveBeenCalledTimes(2);
    expect(defaultRoot).toMatchObject(oneRoot[0]);
  });

  it("should correctly map numbers to categories", (): void => {
    expect.hasAssertions();

    const low = mapScoreToCategory(1);

    expect(low).toBe("low");

    const medium = mapScoreToCategory(4);

    expect(medium).toBe("medium");

    const high = mapScoreToCategory(7);

    expect(high).toBe("high");

    const critical = mapScoreToCategory(10);

    expect(critical).toBe("critical");
  });

  it("should correctly extract path info (hacker mode)", (): void => {
    expect.hasAssertions();

    const rootInfo = getRootInfoFromPath(
      "home/user/groups/my-group/my-root/folder/file",
    );

    expect(rootInfo).toStrictEqual({
      fileRelativePath: "folder/file",
      groupName: "my-group",
      nickname: "my-root",
    } as IRootInfo);

    const rootInfoWindows = getRootInfoFromPath(
      "c:/user/groups/my-group/my-root/folder/file",
    );

    expect(rootInfoWindows).toStrictEqual({
      fileRelativePath: "folder/file",
      groupName: "my-group",
      nickname: "my-root",
    } as IRootInfo);

    // Return null
    const rootInfoNull = getRootInfoFromPath("home/user/groups/project");

    expect(rootInfoNull).toBeNull();
  });

  it("should resolve webview resource URIs", (): void => {
    expect.hasAssertions();

    const panel = window.createWebviewPanel(
      "custom-fix-for-vulnerability",
      `Custom Suggested Fix`,
      ViewColumn.One,
      {
        enableScripts: true,
        retainContextWhenHidden: true,
      },
    );
    const extensionUri = Uri.file("/home/user/.vscode/extensions");
    const pathList = [["dist", "webview.js"]];

    const srcUri = getUri(
      panel.webview,
      extensionUri,
      pathList as unknown as string[],
    );

    expect(srcUri.path).toBe(
      "vscode-resource:/home/user/.vscode/extensions/dist/webview.js",
    );
  });

  it("should enable hacker mode if all conditions are met", (): void => {
    expect.hasAssertions();

    const root: IGitRoot = {
      gitEnvironmentUrls: [],
      gitignore: [],
      groupName: "my-group",
      id: "test-id",
      nickname: "my-root",
      state: "ACTIVE",
      url: "git@gitlab.com:org/repo.git",
      userRole: "user",
    };
    const uri = Uri.file("/home/user/groups/my-group/my-root/folder/");
    const uriWithoutGroups = Uri.file("/home/user/my-root/folder/");
    const userRole = "user";

    // Not a hacker
    expect(shouldEnableHackerMode(uriWithoutGroups, root, userRole)).toBe(
      false,
    );
    expect(shouldEnableHackerMode(uri, root, userRole)).toBe(false);

    const hackerRole = "hacker";

    // Does not have the required folder structure
    expect(shouldEnableHackerMode(uriWithoutGroups, root, hackerRole)).toBe(
      false,
    );

    // Not a hacker in the group this repo is located
    expect(shouldEnableHackerMode(uri, root, hackerRole)).toBe(false);

    // Is a hacker and has access to the group where the repo is
    expect(
      shouldEnableHackerMode(uri, { ...root, userRole: "hacker" }, hackerRole),
    ).toBe(true);

    const groupsUri = Uri.file("/home/user/groups/");

    // Is a hacker only in this specific group
    expect(
      shouldEnableHackerMode(
        groupsUri,
        { ...root, userRole: "hacker" },
        userRole,
      ),
    ).toBe(true);

    const adminRole = "admin";

    // Admins have a long banhammer
    expect(shouldEnableHackerMode(uri, root, adminRole)).toBe(true);

    // Nickname is "groups" and globalRole is "hacker"
    const groupsNicknameUri = Uri.file("/home/user/groups/groups/");

    expect(
      shouldEnableHackerMode(
        groupsNicknameUri,
        { ...root, nickname: "groups" },
        hackerRole,
      ),
    ).toBe(true);
  });

  it("should check valid texts and dates", (): void => {
    expect.hasAssertions();

    expect(validTextField(undefined)).toBeUndefined();

    const validText = "Valid justification for a mutation";

    expect(validTextField(validText)).toBeUndefined();

    const invalidText = "=Malicious justification";

    expect(validTextField(invalidText)).toBe(
      "Invalid character at the beginning: '='",
    );

    const anotherInvalidText = "Justification with •";

    expect(validTextField(anotherInvalidText)).toBe("Invalid character: '•'");
    expect(validDateField(undefined)).toBeUndefined();

    const validDate = "2023-09-11";

    expect(validDateField(validDate)).toBeUndefined();

    const invalidDate = "2023-something-11";

    expect(validDateField(invalidDate)).toBe(
      "Invalid date, the date format is yyyy-mm-dd",
    );
  });

  it("should validate sent justifications", (): void => {
    expect.hasAssertions();

    const justification = "test justification";

    expect(validJustification(justification)).toBeUndefined();

    const shortJustification = "I want to";

    expect(validJustification(shortJustification)).toStrictEqual({
      message:
        "The length of the justification must be greater than 10 characters",
      severity: InputBoxValidationSeverity.Error,
    });

    const longJustification = "a".repeat(10001);

    expect(validJustification(longJustification)).toStrictEqual({
      message:
        "The length of the justification must be less than 10000 characters",
      severity: InputBoxValidationSeverity.Error,
    });

    const maliciousJustification = "=nefarious code here";

    expect(validJustification(maliciousJustification)).toStrictEqual({
      message: "Invalid character at the beginning: '='",
      severity: InputBoxValidationSeverity.Error,
    });
  });

  it("should log to output", (): void => {
    expect.hasAssertions();

    const infoSpy = jest.spyOn(Logger, "info");
    Logger.info("Information goes here");

    expect(window.createOutputChannel).toHaveBeenCalledWith("Fluid Attacks", {
      log: true,
    });
    expect(infoSpy).toHaveBeenCalledWith("Information goes here");

    const warnSpy = jest.spyOn(Logger, "warn");
    Logger.warn("Warning! You haven't installed git");

    expect(warnSpy).toHaveBeenCalledWith("Warning! You haven't installed git");

    const errorSpy = jest.spyOn(Logger, "error");
    const error = new Error("Endpoint unavailable!");
    Logger.error("Query error:", error);

    expect(errorSpy).toHaveBeenCalledWith("Query error:", error);

    const showSpy = jest.spyOn(Logger, "show");
    Logger.show();

    expect(showSpy).toHaveBeenCalledWith();
  });

  it("should fetch and filter vulnerabilities", async (): Promise<void> => {
    expect.hasAssertions();

    const root: IGitRoot = {
      gitEnvironmentUrls: [],
      gitignore: [],
      groupName: "my-group",
      id: "test-id",
      nickname: "my-root",
      state: "ACTIVE",
      url: "git@gitlab.com:org/repo.git",
      userRole: "user",
    };
    const rootPath = "/home/user/groups/my-group/my-root/folder/";

    const vulnerabilities = await getGitRootVulnerabilities(
      root.groupName,
      root.nickname,
    );

    expect(vulnerabilities).toHaveLength(6);

    const filteredVulnerabilities = await fetchGitRootVulnerabilities(
      root,
      rootPath,
    );

    expect(filteredVulnerabilities).toHaveLength(0);
    expect(filteredVulnerabilities).toStrictEqual([]);
  });

  it("should validate attacked lines", (): void => {
    expect.hasAssertions();

    const loc = 120;

    expect(validAttackedLines(String(0), loc)).toBeUndefined();

    expect(validAttackedLines(String(55), loc)).toBeUndefined();

    expect(validAttackedLines(String(120), loc)).toBeUndefined();

    expect(validAttackedLines(String(200), loc)).toStrictEqual({
      message:
        "Attacked Lines must be a positive integer between 0 and 120 (LoC)",
      severity: InputBoxValidationSeverity.Error,
    });

    expect(validAttackedLines(String(-1), loc)).toStrictEqual({
      message:
        "Attacked Lines must be a positive integer between 0 and 120 (LoC)",
      severity: InputBoxValidationSeverity.Error,
    });

    expect(validAttackedLines(String(55.5), loc)).toStrictEqual({
      message:
        "Attacked Lines must be a positive integer between 0 and 120 (LoC)",
      severity: InputBoxValidationSeverity.Error,
    });

    expect(validAttackedLines("=nefarious code", loc)).toStrictEqual({
      message:
        "Attacked Lines must be a positive integer between 0 and 120 (LoC)",
      severity: InputBoxValidationSeverity.Error,
    });

    expect(validAttackedLines("123x", loc)).toStrictEqual({
      message:
        "Attacked Lines must be a positive integer between 0 and 120 (LoC)",
      severity: InputBoxValidationSeverity.Error,
    });
  });

  it("should return diagnostic", (): void => {
    expect.hasAssertions();

    const finding: IFinding = getMockFinding("testGroup");
    const vulnerability: IVulnerability = {
      commitHash: "testCommitHash",
      finding,
      id: "testVulnId",
      isAutoFixable: true,
      reportDate: "2023-02-01 15:48:40",
      rootNickname: "",
      specific: "10",
      state: "VULNERABLE",
      where: "folder/subfolder/file.ts",
    };
    const mockDiagnostic: VulnerabilityDiagnostic[] = [
      new VulnerabilityDiagnostic(
        vulnerability,
        new Range(new Position(9, 1), new Position(9, 1)),
        "Found 7 months ago | VULNERABILITY: test description",
        DiagnosticSeverity.Error,
      ),
    ];
    // eslint-disable-next-line functional/immutable-data
    Object.defineProperty(window, "activeTextEditor", {
      configurable: true,
      value: window.activeTextEditor,
    });

    const diagnostic = getDiagnosticsForTreatment({
      get: jest.fn(
        (_uri: Uri): VulnerabilityDiagnostic[] | undefined => mockDiagnostic,
      ),
    } as unknown as DiagnosticCollection);

    expect(window.activeTextEditor?.document.fileName).toBe("testFile.txt");

    expect(diagnostic).toBe(mockDiagnostic);
  });

  it("should show information message if diagnostics is undefined", (): void => {
    expect.hasAssertions();

    const diagnostic = getDiagnosticsForTreatment({
      get: jest.fn(
        (_uri: Uri): VulnerabilityDiagnostic[] | undefined => undefined,
      ),
    } as unknown as DiagnosticCollection);

    expect(window.activeTextEditor?.document.fileName).toBe("testFile.txt");
    expect(window.showInformationMessage).toHaveBeenCalledWith(
      "This line does not contain vulnerabilities",
    );
    expect(diagnostic).toBeUndefined();
  });

  it("should show error message if selection spans multiple lines", (): void => {
    expect.hasAssertions();

    const finding: IFinding = getMockFinding("testGroup");
    const vulnerability: IVulnerability = {
      commitHash: "testCommitHash",
      finding,
      id: "testVulnId",
      isAutoFixable: true,
      reportDate: "2023-02-01 15:48:40",
      rootNickname: "",
      specific: "10",
      state: "VULNERABLE",
      where: "folder/subfolder/file.ts",
    };
    const mockDiagnostic: VulnerabilityDiagnostic[] = [
      new VulnerabilityDiagnostic(
        vulnerability,
        new Range(new Position(9, 1), new Position(9, 1)),
        "Found 7 months ago | VULNERABILITY: test description",
        DiagnosticSeverity.Error,
      ),
    ];

    // eslint-disable-next-line functional/immutable-data
    Object.defineProperty(window, "activeTextEditor", {
      configurable: true,
      value: {
        document: {
          fileName: "testFile.txt",
        },
        selection: {
          end: { line: 10 },
          start: { line: 0 },
        },
      },
    });

    const diagnostic = getDiagnosticsForTreatment({
      get: jest.fn(
        (_uri: Uri): VulnerabilityDiagnostic[] | undefined => mockDiagnostic,
      ),
    } as unknown as DiagnosticCollection);

    expect(window.activeTextEditor?.document.fileName).toBe("testFile.txt");
    expect(window.showErrorMessage).toHaveBeenCalledWith(
      "Please make a request to change treatment one vulnerability at a time",
    );
    expect(diagnostic).toBeUndefined();
  });

  it("should return undefined if no activeTextEditor", (): void => {
    expect.hasAssertions();

    // eslint-disable-next-line functional/immutable-data
    Object.defineProperty(window, "activeTextEditor", {
      configurable: true,
      value: undefined,
    });
    const mockGet = jest.fn();
    const diagnostic = getDiagnosticsForTreatment({
      get: mockGet,
    } as unknown as DiagnosticCollection);

    expect(diagnostic).toBeUndefined();
    expect(mockGet).not.toHaveBeenCalled();
  });

  it("shouldn't resolve groups paths", (): void => {
    expect.hasAssertions();

    // eslint-disable-next-line functional/immutable-data
    Object.defineProperty(workspace, "workspaceFolders", {
      configurable: true,
      value: undefined,
    });
    const withoutWorkSpace = getGroupsPath();

    expect(withoutWorkSpace).toBe("");

    // eslint-disable-next-line functional/immutable-data
    Object.defineProperty(workspace, "workspaceFolders", {
      configurable: true,
      value: [{ uri: { path: "/home/user/my-root/folder/" } }],
    });

    const result = getGroupsPath();

    expect(result).toBe("");
  });

  it("should resolve groups paths", (): void => {
    expect.hasAssertions();

    // eslint-disable-next-line functional/immutable-data
    Object.defineProperty(workspace, "workspaceFolders", {
      configurable: true,
      value: [
        { uri: { path: "home/user/groups/my-group/my-root/folder/file" } },
      ],
    });

    const expectedPath = join("home/user", "groups");
    const result = getGroupsPath();

    expect(result).toBe(expectedPath);
  });

  it("should webview content", (): void => {
    expect.hasAssertions();

    const mockContext = {
      extensionMode: ExtensionMode.Production,
      extensionPath: "/mock/extension/path",
    } as unknown as ExtensionContext;

    const mockWebview = {
      asWebviewUri: jest.fn((uri: Uri): Uri => uri),
    } as unknown as Webview;

    const htmlContent = getWebviewContent(mockContext, mockWebview);

    const expectedScriptUrl = Uri.file(
      join(mockContext.extensionPath, "dist", "webview.js"),
    ).toString();

    expect(htmlContent).toContain('<link href="null" rel="stylesheet">');
    expect(htmlContent).toContain(
      `<script type="module" src="${expectedScriptUrl}" />`,
    );

    const mockContextDev = {
      extensionMode: ExtensionMode.Development,
      extensionPath: "/mock/extension/path",
    } as unknown as ExtensionContext;

    const mockWebviewDev = {
      asWebviewUri: jest.fn((uri: Uri): Uri => uri),
    } as unknown as Webview;

    const htmlContentDev = getWebviewContent(mockContextDev, mockWebviewDev);

    expect(htmlContentDev).not.toContain('<link href="null" rel="stylesheet">');
    expect(htmlContentDev).toContain(
      '<script type="module" src="http://localhost:9000/webview.js" />',
    );
  });

  it("should ignore files", (): void => {
    expect.hasAssertions();

    const mockPaths: string[] = ["file1.js", "file2.js"];
    const pathTest = "/path/to/dir";
    const patterns = ["*.js"];

    jest.mocked(sync).mockReturnValueOnce(mockPaths);

    ignoreFiles(pathTest, patterns);

    expect(sync).toHaveBeenCalledTimes(1);
    expect(sync).toHaveBeenCalledWith("/path/to/dir/*.js");
  });

  it("should return vulnerability line when do rebase", async (): Promise<void> => {
    expect.hasAssertions();

    const git: jest.Mocked<SimpleGit> = {
      raw: jest.fn(),
    } as unknown as jest.Mocked<SimpleGit>;
    git.raw.mockResolvedValueOnce("1 123\n");

    const result = await rebase(git, "commit-hash", "file-path", 123);

    expect(result).toBe(123);

    //  New line number after successful rebase
    git.raw.mockResolvedValueOnce("1 124\n");

    const newLine = await rebase(git, "commit-hash", "file-path", 123);

    expect(newLine).toBe(124);

    // Return null
    jest.spyOn(global.console, "error");

    const error = new Error("Unexpected error");
    git.raw.mockRejectedValueOnce(error);

    const lineNull = await rebase(git, "commit-hash", "file-path", 123);

    expect(lineNull).toBeNull();

    // The specified commit is equivalent to HEAD
    const gitError = new GitError(
      undefined,
      "More than one commit to dig up from",
    );
    git.raw.mockRejectedValueOnce(gitError);

    const sameLine = await rebase(git, "commit-hash-head", "file-path", 123);

    expect(sameLine).toBe(123);

    // Another git error
    const anotherGitError = new GitError(undefined, "Unexpected error");
    git.raw.mockRejectedValueOnce(anotherGitError);

    const errorResult = await rebase(git, "commit-hash-head", "file-path", 123);

    expect(Logger.error).toHaveBeenCalledWith(
      "Git error occurred:",
      "Unexpected error",
    );
    expect(errorResult).toBeNull();
  });

  it("should rebase the line number when diff output is provided", async (): Promise<void> => {
    expect.hasAssertions();

    const git: jest.Mocked<SimpleGit> = {
      diff: jest.fn(),
      raw: jest.fn(),
    } as unknown as jest.Mocked<SimpleGit>;

    git.raw.mockResolvedValueOnce("1 124\n");
    const diffOutput = `
      diff --git a/file-path b/file-path
      index 83db48f..e6a739f 100644
      --- a/file-path
      +++ b/file-path
      @@ -1,3 +1,3 @@
      -line 1
      +line 1 modified
      line 2
      line 3
      `;

    git.diff.mockResolvedValueOnce(diffOutput);
    const mockParsePatch: ParsedDiff[] = [
      {
        hunks: [
          {
            lines: [
              "-line 1",
              "+line 1 modified",
              " line 2",
              "+line 2 modified",
              " line 3",
            ],
            newLines: 40,
            newStart: 10,
            oldLines: 30,
            oldStart: 10,
          },
        ],
        newFileName: "b/file-path",
        oldFileName: "a/file-path",
      },
    ];

    jest.mocked(parsePatch).mockReturnValue(mockParsePatch);

    const result = await rebase(git, "commit-hash", "file-path", 123);

    expect(parsePatch).toHaveBeenCalledWith(diffOutput);
    expect(result).toBe(134);

    // Line is before the hunk, no change
    git.raw.mockResolvedValueOnce("1 9\n");
    git.diff.mockResolvedValueOnce(diffOutput);

    const noChange = await rebase(git, "commit-hash", "file-path", 8);

    expect(parsePatch).toHaveBeenCalledWith(diffOutput);
    expect(noChange).toBe(9);

    // Line falls within the hunk, cannot rebase deterministically
    git.raw.mockResolvedValueOnce("1 26\n");
    git.diff.mockResolvedValueOnce(diffOutput);

    const fallsResult = await rebase(git, "commit-hash", "file-path", 25);

    expect(parsePatch).toHaveBeenCalledWith(diffOutput);
    expect(fallsResult).toBe(26);
  });

  it("should return true if the commit exists", async (): Promise<void> => {
    expect.hasAssertions();

    const repo = {
      show: jest.fn(),
    } as unknown as jest.Mocked<SimpleGit>;
    repo.show.mockResolvedValueOnce("commit details");

    const result = await commitExists(repo, "valid-commit-hash");

    expect(result).toBe(true);
    expect(repo.show).toHaveBeenCalledWith("valid-commit-hash");

    // Return false if the commit does not exist

    repo.show.mockRejectedValueOnce(new Error("Commit not found"));

    const exist = await commitExists(repo, "invalid-commit-hash");

    expect(exist).toBe(false);
    expect(repo.show).toHaveBeenCalledWith("invalid-commit-hash");
  });
});
