import Bugsnag from "@bugsnag/js";
import _ from "lodash";
import { env, window } from "vscode";

interface IRetrievesLoggerAttr {
  error: (msg: string, extra?: unknown) => void;
  info: (msg: string, extra?: unknown) => void;
  warn: (msg: string, extra?: unknown) => void;
  show: () => void;
}

type TSeverity = "error" | "info" | "warning";

const sendBugsnagReport = (
  msg: string,
  extra: unknown,
  severity: TSeverity,
): void => {
  if (
    !_.isNil(extra) &&
    "message" in (extra as Record<string, unknown>) &&
    ["Login required", "Token format unrecognized"].includes(
      (extra as Record<string, unknown>).message as string,
    )
  ) {
    return;
  }
  Bugsnag.notify(msg, (event): void => {
    event.errors.forEach((error): void => {
      // eslint-disable-next-line functional/immutable-data
      error.errorClass = `Log${severity.toUpperCase()}`;

      // eslint-disable-next-line functional/immutable-data
      error.stacktrace.splice(0, 2);
    });
    event.addMetadata("extra", { extra });

    // eslint-disable-next-line functional/immutable-data
    event.severity = severity;
  });
};

const retrievesOutputChannel = window.createOutputChannel("Fluid Attacks", {
  log: true,
});

const sendErrorLog = (msg: string, extra?: unknown): void => {
  retrievesOutputChannel.error(msg, _.isNil(extra) ? "" : extra);
  if (env.isTelemetryEnabled) {
    sendBugsnagReport(msg, extra, "error");
  }
};

const sendInfoLog = (msg: string, extra?: unknown): void => {
  retrievesOutputChannel.info(msg, _.isNil(extra) ? "" : extra);
};

const sendWarningLog = (msg: string, extra?: unknown): void => {
  retrievesOutputChannel.warn(msg, _.isNil(extra) ? "" : extra);
};

const showOutputChannel = (): void => {
  retrievesOutputChannel.show();
};

export const Logger: IRetrievesLoggerAttr = {
  error: sendErrorLog,
  info: sendInfoLog,
  show: showOutputChannel,
  warn: sendWarningLog,
};
