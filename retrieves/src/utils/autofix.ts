const FUNCTION_DECLARATION_MAP = {
  // eslint-disable-next-line camelcase
  c_sharp: [
    "constructor_declaration",
    "conversion_operator_declaration",
    "destructor_declaration",
    "method_declaration",
    "operator_declaration",
    "accessor_declaration",
    "local_function_statement",
  ],
  dart: [
    "function_signature",
    "function_body",
    "getter_signature",
    "setter_signature",
    "method_signature",
  ],
  go: ["function_declaration", "method_declaration"],
  hcl: ["block"],
  java: ["constructor_declaration", "method_declaration", "record_declaration"],
  javascript: [
    "arrow_function",
    "function_declaration",
    "function_expression",
    "generator_function",
    "generator_function_declaration",
    "method_definition",
  ],
  kotlin: ["function_declaration", "primary_constructor", "getter", "setter"],
  php: [
    "function_definition",
    "function_static_declaration",
    "method_declaration",
  ],
  python: ["function_definition"],
  ruby: ["method", "singleton_method", "setter"],
  scala: ["function_declaration", "function_definition"],
  swift: ["function_declaration", "protocol_function_declaration"],
  typescript: [
    "arrow_function",
    "function_declaration",
    "generator_function_declaration",
    "method_definition",
  ],
  yaml: ["block_mapping_pair"],
};

const IMPORTS_DECLARATIONS_MAP = {
  // eslint-disable-next-line camelcase
  c_sharp: ["using_directive"],
  dart: ["import_specification"],
  dockerfile: ["from_instruction"],
  go: ["import_declaration"],
  java: ["import_declaration"],
  javascript: ["import_statement"],
  kotlin: ["import_header"],
  php: ["use_declaration", "namespace_use_declaration"],
  python: ["import_statement", "import_from_statement"],
  scala: ["import_declaration"],
  swift: ["import_declaration"],
  tsx: ["import_statement"],
};

const REQUIRES_SEQUENCE = { dart: ["function_signature", "function_body"] };

const LANGUAGE_CONVERSION: Record<string, string> = {
  aspnetcorerazor: "c_sharp",
  csharp: "c_sharp",
  razor: "c_sharp",
  terraform: "hcl",
};

export {
  LANGUAGE_CONVERSION,
  FUNCTION_DECLARATION_MAP,
  REQUIRES_SEQUENCE,
  IMPORTS_DECLARATIONS_MAP,
};
