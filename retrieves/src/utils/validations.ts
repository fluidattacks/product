import _ from "lodash";
import type { InputBoxValidationMessage } from "vscode";
import { InputBoxValidationSeverity } from "vscode";

const validTextField = (value: string | undefined): string | undefined => {
  if (!_.isNil(value)) {
    const beginTextMatch: RegExpMatchArray | null = /^=/u.exec(value);
    if (!_.isNull(beginTextMatch)) {
      return `Invalid character at the beginning: '${beginTextMatch[0]}'`;
    }

    const textMatch: RegExpMatchArray | null =
      // We use them for control character pattern matching.
      /[^a-zA-Z0-9ñáéíóúäëïöüÑÁÉÍÓÚÄËÏÖÜ\s(){}[\],./:;@&_$%'#*=¿?¡!+-]/u.exec(
        value,
      );
    if (!_.isNull(textMatch)) {
      return `Invalid character: '${textMatch[0]}'`;
    }

    return undefined;
  }

  return undefined;
};

const validDateField = (value: string | undefined): string | undefined => {
  if (!_.isNil(value)) {
    const textMatch: boolean =
      // We use them for control character pattern matching.
      // eslint-disable-next-line prefer-named-capture-group
      /^(19|20)\d\d([- /.])(0[1-9]|1[012])\2(0[1-9]|[12]\d|3[01])$/u.test(
        value,
      );
    if (!textMatch) {
      return "Invalid date, the date format is yyyy-mm-dd";
    }

    return undefined;
  }

  return undefined;
};

const validJustification = (
  message: string,
): InputBoxValidationMessage | undefined => {
  if (message.length < 10) {
    return {
      message:
        "The length of the justification must be greater than 10 characters",
      severity: InputBoxValidationSeverity.Error,
    };
  }
  if (message.length > 10000) {
    return {
      message:
        "The length of the justification must be less than 10000 characters",
      severity: InputBoxValidationSeverity.Error,
    };
  }
  const validationMessage = validTextField(message);

  if (validationMessage !== undefined) {
    return {
      message: validationMessage,
      severity: InputBoxValidationSeverity.Error,
    };
  }

  return undefined;
};

const validAttackedLines = (
  value: string,
  loc: number,
): InputBoxValidationMessage | undefined => {
  const testPositiveInteger: boolean = /^(?<positiveInteger>0|[1-9]\d*)$/u.test(
    value,
  );

  if (testPositiveInteger && Number(value) >= 0 && Number(value) <= loc) {
    return undefined;
  }

  return {
    message: `Attacked Lines must be a positive integer between 0 and ${loc} (LoC)`,
    severity: InputBoxValidationSeverity.Error,
  };
};

export {
  validTextField,
  validDateField,
  validJustification,
  validAttackedLines,
};
