import type { Command, TreeItemCollapsibleState } from "vscode";
import { TreeItem, Uri } from "vscode";

import type { IFinding } from "@retrieves/types";
import { mapScoreToCategory } from "@retrieves/utils/severityScore";

class FindingTreeItem extends TreeItem {
  public contextValue = "finding";

  public constructor(
    public readonly label: string,
    public readonly collapsibleState: TreeItemCollapsibleState,
    public readonly id: string,
    public readonly finding: IFinding,
    public readonly command?: Command,
  ) {
    super(label, collapsibleState);

    this.tooltip = `CVSS score: ${this.finding.maxOpenSeverityScoreV4}`;
    this.iconPath = Uri.joinPath(
      Uri.file(__filename),
      "..",
      "..",
      "media",
      `finding_${mapScoreToCategory(this.finding.maxOpenSeverityScoreV4)}.svg`,
    );
  }
}

export { FindingTreeItem };
