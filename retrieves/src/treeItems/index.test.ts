import { Position, Range, TreeItemCollapsibleState } from "vscode";

import { FindingTreeItem } from "./finding";
import { resolveExtraInformation, toVulnerability } from "./vulnerability";

import { getMockFinding } from "@retrieves/mocks/data";
import type { IVulnerability } from "@retrieves/types";

describe("test tree items", (): void => {
  it("should resolve extra tooltip info", (): void => {
    expect.hasAssertions();

    const finding = getMockFinding("testGroup");
    const vulnerability: IVulnerability = {
      commitHash: "testCommitHash",
      finding,
      id: "testVulnId",
      isAutoFixable: true,
      reportDate: "2023-02-01 15:48:40",
      rootNickname: "",
      specific: "10",
      state: "VULNERABLE",
      where: "folder/subfolder/file.ts",
    };

    const extraInfo = resolveExtraInformation(vulnerability);

    // Nothing to add here, just your typical vulnerability
    expect(extraInfo).toBe("");

    const reattackExtraInfo = resolveExtraInformation({
      ...vulnerability,
      verification: "Requested",
    });

    // Reattack status should be shown
    expect(reattackExtraInfo).toBe(" • Ongoing Reattack");

    const acceptedExtraInfo = resolveExtraInformation({
      ...vulnerability,
      treatmentStatus: "ACCEPTED",
    });

    // Reattack status should be shown
    expect(acceptedExtraInfo).toBe(" • Accepted");
  });

  it("should create vuln and finding tree items", (): void => {
    expect.hasAssertions();

    const finding = getMockFinding("testGroup");
    const findingItem = new FindingTreeItem(
      finding.title,
      TreeItemCollapsibleState.Collapsed,
      finding.id,
      finding,
    );

    expect(findingItem.tooltip).toBe("CVSS score: 7.4");

    const vulnerability: IVulnerability = {
      commitHash: "testCommitHash",
      finding,
      id: "testVulnId",
      isAutoFixable: true,
      reportDate: "2023-02-01 15:48:40",
      rootNickname: "",
      specific: "10",
      state: "VULNERABLE",
      where: "folder/subfolder/file.ts",
    };

    const vulnerabilityItem = toVulnerability(
      "/home/path/to/root/folder",
      vulnerability,
    );

    expect(vulnerabilityItem.tooltip).toBe(
      "/~/path/to/root/folder/subfolder/file.ts @ Line 10 ",
    );
    expect(vulnerabilityItem.command).toStrictEqual({
      arguments: [
        { path: "/home/path/to/root/folder/subfolder/file.ts" },
        // Vscode line numbers are 0-based
        { selection: new Range(new Position(9, 1), new Position(9, 1)) },
      ],
      command: "vscode.open",
      title: "Open file",
    });
  });
});
