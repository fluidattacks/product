import type {
  IGitRoot,
  IOrganization,
  IStakeholder,
  IVulnerability,
} from "@retrieves/types";

interface ICustomFixVulnerability {
  vulnerability: { customFix: string | null };
}
interface IGetGitRootQuery {
  root: IGitRoot;
}

interface IGetGitRootVulnerabilitiesQuery {
  root: {
    __typename: string;
    nickname: string;
    vulnerabilities: IVulnerability[];
  };
}

interface IGetGroupsQuery {
  me: {
    organizations: (IOrganization & { __typename: string })[];
  };
}

interface IGetGroupUsersQuery {
  group: {
    stakeholders: IStakeholder[];
  };
}

interface IGetGitRootsQuery {
  group: {
    roots: (IGitRoot & { __typename: string })[];
  };
}

interface IGetUserInfoQuery {
  me: {
    role: string;
    userEmail: string;
  };
}

interface IRefetchVulnerabilityQuery {
  vulnerability: IVulnerability;
}

export type {
  ICustomFixVulnerability,
  IGetGitRootQuery,
  IGetGitRootVulnerabilitiesQuery,
  IGetGroupsQuery,
  IGetGroupUsersQuery,
  IGetGitRootsQuery,
  IGetUserInfoQuery,
  IRefetchVulnerabilityQuery,
};
