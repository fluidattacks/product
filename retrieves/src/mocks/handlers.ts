import { GraphQLError } from "graphql";
import type { StrictResponse } from "msw";
import { HttpResponse, graphql } from "msw";

import {
  getMockFinding,
  getMockOrganizations,
  getMockRootVulnerabilities,
  getMockRoots,
  mockStakeholders,
} from "./data";

import type {
  IUpdateAttackedFile,
  IVulnerabilitiesEdge,
} from "@retrieves/api/types";
import type {
  IAcceptVulnerabilityData,
  IRequestReattackData,
} from "@retrieves/api/vulnerabilities";
import type {
  ICustomFixVulnerability,
  IGetGitRootQuery,
  IGetGitRootVulnerabilitiesQuery,
  IGetGitRootsQuery,
  IGetGroupUsersQuery,
  IGetGroupsQuery,
  IGetUserInfoQuery,
  IRefetchVulnerabilityQuery,
} from "@retrieves/mocks/types";
import {
  ACCEPT_VULNERABILITY_TEMPORARILY,
  GET_CUSTOM_FIX,
  GET_GIT_ROOT,
  GET_GIT_ROOTS,
  GET_GIT_ROOTS_SIMPLE,
  GET_GROUPS,
  GET_GROUP_STAKEHOLDERS,
  GET_USER_INFO,
  GET_VULNERABILITIES,
  GET_VULNERABILITY,
  REQUEST_VULNERABILITIES_VERIFICATION,
  UPDATE_TOE_LINES_ATTACKED,
} from "@retrieves/queries";

const mockedQueries = [
  graphql.query<
    IGetGroupUsersQuery,
    {
      groupName: string;
    }
  >(
    GET_GROUP_STAKEHOLDERS,
    ({ variables }): StrictResponse<Record<string, unknown>> => {
      if (variables.groupName === "failGroup") {
        return HttpResponse.json({
          errors: [new GraphQLError("Access denied")],
        });
      }

      return HttpResponse.json({
        data: { group: { name: "testGroup", stakeholders: mockStakeholders } },
      });
    },
  ),
  graphql.query<
    IGetGitRootQuery,
    {
      groupName: string;
      rootId: string;
    }
  >(GET_GIT_ROOT, ({ variables }): StrictResponse<Record<string, unknown>> => {
    const { groupName, rootId } = variables;
    if (groupName === "failGroup") {
      return HttpResponse.json({
        errors: [new GraphQLError("Exception - User token has expired")],
      });
    }
    const root = getMockRoots(groupName).find(
      (gitRoot): boolean => gitRoot.id === rootId,
    );

    if (root !== undefined) {
      return HttpResponse.json({
        data: {
          root,
        },
      });
    }

    return HttpResponse.json({
      errors: [new GraphQLError("Exception - Access denied or root not found")],
    });
  }),
  graphql.query<
    IGetGitRootVulnerabilitiesQuery,
    {
      groupName: string;
      id: string;
    }
  >(
    GET_VULNERABILITIES,
    ({ variables }): StrictResponse<Record<string, unknown>> => {
      const { groupName } = variables;
      if (groupName === "failGroup") {
        return HttpResponse.json({
          errors: [new GraphQLError("Token format unrecognized")],
        });
      }
      const finding = getMockFinding(groupName);
      const mockVulnerabilities = getMockRootVulnerabilities(finding);

      return HttpResponse.json({
        data: {
          group: {
            __typename: "Group",
            name: finding.groupName,
            vulnerabilities: {
              edges: mockVulnerabilities.map(
                (vulnerability): IVulnerabilitiesEdge => {
                  return { node: vulnerability };
                },
              ),
              pageInfo: {
                endCursor: "",
                hasNextPage: false,
              },
            },
          },
        },
      });
    },
  ),
  graphql.query<
    IGetGitRootsQuery,
    {
      groupName: string;
    }
  >(
    GET_GIT_ROOTS_SIMPLE,
    ({ variables }): StrictResponse<Record<string, unknown>> => {
      const { groupName } = variables;
      if (groupName === "failGroup") {
        return HttpResponse.json({
          errors: [
            new GraphQLError(
              "Response not successful: Received status code 504",
            ),
          ],
        });
      }

      return HttpResponse.json({
        data: {
          group: {
            roots: getMockRoots(groupName),
          },
        },
      });
    },
  ),
  graphql.query<
    IGetGitRootsQuery,
    {
      groupName: string;
    }
  >(GET_GIT_ROOTS, ({ variables }): StrictResponse<Record<string, unknown>> => {
    const { groupName } = variables;
    if (groupName === "failGroup") {
      return HttpResponse.json({
        errors: [new GraphQLError("Exception - User token has expired")],
      });
    }

    return HttpResponse.json({
      data: {
        group: {
          roots: getMockRoots(groupName),
        },
      },
    });
  }),
  graphql.query<IGetGroupsQuery>(
    GET_GROUPS,
    (): StrictResponse<Record<string, unknown>> => {
      return HttpResponse.json({
        data: {
          me: {
            organizations: getMockOrganizations,
            userEmail: "user@mail.com",
          },
        },
      });
    },
  ),
  graphql.query<IGetUserInfoQuery>(
    GET_USER_INFO,
    (): StrictResponse<Record<string, unknown>> => {
      return HttpResponse.json({
        data: {
          me: {
            role: "user",
            userEmail: "forces.group@fluidattacks.com",
          },
        },
      });
    },
  ),
  graphql.query<
    IRefetchVulnerabilityQuery,
    {
      identifier: string;
    }
  >(
    GET_VULNERABILITY,
    ({ variables }): StrictResponse<Record<string, unknown>> => {
      if (variables.identifier === "failVulnId") {
        return HttpResponse.json({
          errors: [
            new GraphQLError(
              "Response not successful: Received status code 504",
            ),
          ],
        });
      }

      return HttpResponse.json({
        data: {
          vulnerability: {
            commitHash: "",
            finding: getMockFinding("testGroup"),
            id: "testVulnId",
            reportDate: "",
            rootNickname: "",
            specific: "10",
            state: "VULNERABLE",
            treatmentStatus: "UNTREATED",
            verification: "Requested",
            where: "file.ts",
          },
        },
      });
    },
  ),
  graphql.query<
    ICustomFixVulnerability,
    {
      vulnerabilityId: string;
    }
  >(
    GET_CUSTOM_FIX,
    ({ variables }): StrictResponse<Record<string, unknown>> => {
      if (variables.vulnerabilityId === "failVulnId") {
        return HttpResponse.json({
          errors: [
            new GraphQLError(
              "Response not successful: Received status code 504",
            ),
          ],
        });
      }

      return HttpResponse.json({
        data: {
          vulnerability: {
            customFix: "testing custom fix",
          },
        },
      });
    },
  ),
];

const mockedMutations = [
  graphql.mutation<
    IRequestReattackData,
    { findingId: string; justification: string; vulnerabilities: string[] }
  >(
    REQUEST_VULNERABILITIES_VERIFICATION,
    ({ variables }): StrictResponse<Record<string, unknown>> => {
      const { findingId } = variables;

      if (findingId === "should-fail-test-id") {
        return HttpResponse.json({
          errors: [new GraphQLError("Login required")],
        });
      }

      return HttpResponse.json({
        data: {
          requestVulnerabilitiesVerification: {
            success: true,
          },
        },
      });
    },
  ),
  graphql.mutation<
    IAcceptVulnerabilityData,
    {
      findingId: string;
      vulnerabilityId: string;
      acceptanceDate: string;
      assigned: string;
      justification: string;
      treatment: string;
    }
  >(
    ACCEPT_VULNERABILITY_TEMPORARILY,
    ({ variables }): StrictResponse<Record<string, unknown>> => {
      const { assigned, acceptanceDate } = variables;
      if (assigned === "failUser@company.com") {
        return HttpResponse.json({
          errors: [new GraphQLError("Assigned not valid")],
        });
      } else if (acceptanceDate === "1970-01-01") {
        return HttpResponse.json({
          errors: [
            new GraphQLError(
              "Exception - Chosen date is either in the past or exceeds the " +
                "maximum number of days allowed by the defined policy",
            ),
          ],
        });
      }

      return HttpResponse.json({
        data: {
          updateVulnerabilitiesTreatment: {
            success: true,
          },
        },
      });
    },
  ),
  graphql.mutation<
    IUpdateAttackedFile,
    {
      groupName: string;
      attackedLines: number;
      rootId: string;
      fileName: string;
      comments?: string;
    }
  >(
    UPDATE_TOE_LINES_ATTACKED,
    ({ variables }): StrictResponse<Record<string, unknown>> => {
      const { groupName } = variables;

      if (groupName === "failGroup") {
        return HttpResponse.json({
          errors: [new GraphQLError("Error verifying group toe lines")],
        });
      }

      return HttpResponse.json({
        data: {
          updateToeLinesAttackedLines: {
            success: true,
          },
        },
      });
    },
  ),
];

export const handlers = [...mockedQueries, ...mockedMutations];
