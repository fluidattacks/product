/* eslint-disable @typescript-eslint/prefer-destructuring */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable jest/max-expects */
import { existsSync } from "fs";
import { join } from "path";

import { groupBy } from "ramda";
import type {
  DiagnosticCollection,
  ExtensionContext,
  TextDocument,
  TextDocumentChangeEvent,
  TextEditor,
  TextLine,
} from "vscode";
import { DiagnosticSeverity, Range, Uri, window, workspace } from "vscode";

import {
  createDiagnostic,
  setDiagnosticsToAllFiles,
  subscribeToDocumentChanges,
} from "./vulnerabilities";

import {
  getGitRootVulnerabilities,
  getGroupGitRoots,
} from "@retrieves/api/root";
import { getMockFinding } from "@retrieves/mocks/data";
import type { IFinding, IGitRoot, IVulnerability } from "@retrieves/types";
import { getAppUrl } from "@retrieves/utils/url";
import { rebase } from "@retrieves/utils/vulnerabilities";

jest.useFakeTimers().setSystemTime(new Date("2023-10-01"));
jest.mock(
  "ramda",
  (): Record<string, unknown> => ({
    ...jest.requireActual("ramda"),
    groupBy: jest.fn(),
  }),
);
jest.mock(
  "fs",
  (): Record<string, unknown> => ({
    ...jest.requireActual("fs"),
    existsSync: jest.fn(),
  }),
);
jest.mock(
  "simple-git",
  (): Record<string, unknown> => ({
    ...jest.requireActual("simple-git"),
    simpleGit: jest.fn((repoPath: string) => {
      return {
        fetch: jest.fn().mockResolvedValue(true),
        revparse: jest.fn(async (args: string[]) => {
          if (args[0] === "--show-toplevel") {
            return Promise.resolve(repoPath);
          }

          return Promise.resolve("a");
        }),
      };
    }),
  }),
);
jest.mock(
  "@retrieves/utils/git",
  (): Record<string, unknown> => ({
    ...jest.requireActual("@retrieves/utils/git"),
    checkGitInstallation: jest.fn().mockResolvedValueOnce(true),
  }),
);
jest.mock(
  "@retrieves/utils/vulnerabilities",
  (): Record<string, unknown> => ({
    rebase: jest
      .fn()
      .mockResolvedValueOnce(null)
      .mockResolvedValueOnce(4)
      .mockResolvedValueOnce(0)
      .mockResolvedValueOnce(4),
  }),
);
jest.mock(
  "@retrieves/api/root",
  (): Record<string, unknown> => ({
    getGitRootVulnerabilities: jest.fn(
      async (_): Promise<IVulnerability[]> =>
        Promise.resolve([
          {
            commitHash: "test-commit-hash",
            finding: {
              description: "test description",
              groupName: "my-group",
              id: "test-id",
              maxOpenSeverityScoreV4: 7.4,
              title: "011. Use of software with known vulnerabilities",
            },
            id: "test-vuln-id-1",
            isAutoFixable: true,
            reportDate: "2023-04-16 05:48:40",
            rootNickname: "root1",
            specific: "1",
            state: "VULNERABLE",
            treatmentStatus: "UNTREATED",
            verification: null,
            where: "root1/folder/file.ts",
          },
        ]),
    ),
    getGroupGitRoots: jest.fn(
      async (_): Promise<IGitRoot[]> =>
        Promise.resolve([
          {
            __typename: "GitRoot",
            downloadUrl: "pre-signed-url-here",
            gitEnvironmentUrls: [{ id: "env-1", url: "https://env1.com/api" }],
            gitignore: [],
            groupName: "my-group",
            id: "git-root-1",
            nickname: "root1",
            state: "ACTIVE",
            url: "http://gitrepos.com/org/repo.git",
          },
        ]),
    ),
  }),
);

const createMockTextLine = (lineNumber: number, text: string): TextLine => ({
  firstNonWhitespaceCharacterIndex: text.search(/\S/u),
  isEmptyOrWhitespace: text.trim().length === 0,

  lineNumber,
  range: new Range(lineNumber, 0, lineNumber, text.length),

  rangeIncludingLineBreak: new Range(
    lineNumber,
    0,
    lineNumber,
    text.length + 1,
  ),
  text,
});

describe("test diagnostics", (): void => {
  it("should create a diagnostic from a vulnerability", (): void => {
    expect.hasAssertions();

    const finding: IFinding = getMockFinding("testGroup");
    const vulnerability: IVulnerability = {
      commitHash: "testCommitHash",
      finding,
      id: "testVulnId",
      isAutoFixable: true,
      reportDate: "2023-02-01 15:48:40",
      rootNickname: "",
      specific: "10",
      state: "VULNERABLE",
      where: "file.ts",
    };

    const diagnostic = createDiagnostic(
      undefined,
      {
        range: {} as unknown as Range,
      } as unknown as TextLine,
      vulnerability,
    );
    const baseUrl = getAppUrl();

    expect(diagnostic.message).toBe(
      "Found 7 months ago | VULNERABILITY: test description",
    );
    expect(diagnostic.code).toStrictEqual({
      target: Uri.parse(
        `${baseUrl}/groups/testGroup/vulns/test-id/locations/testVulnId`,
      ),
      value: "011. Use of software with known vulnerabilities",
    });
    expect(diagnostic.severity).toBe(DiagnosticSeverity.Error);

    const acceptedVulnerability: IVulnerability = {
      ...vulnerability,
      treatmentStatus: "ACCEPTED",
    };

    const acceptedDiagnostic = createDiagnostic(
      undefined,
      {
        range: {} as unknown as Range,
      } as unknown as TextLine,
      acceptedVulnerability,
    );

    expect(acceptedDiagnostic.severity).toBe(DiagnosticSeverity.Warning);

    const reattackedVulnerability: IVulnerability = {
      ...vulnerability,
      verification: "Requested",
    };
    const reattackedDiagnostic = createDiagnostic(
      undefined,
      {
        range: {} as unknown as Range,
      } as unknown as TextLine,
      reattackedVulnerability,
    );

    expect(reattackedDiagnostic.severity).toBe(DiagnosticSeverity.Information);

    const otherVulnerability: IVulnerability = {
      ...vulnerability,
      reportDate: "",
    };

    const otherDiagnostic = createDiagnostic(
      undefined,
      {
        range: {} as unknown as Range,
      } as unknown as TextLine,
      otherVulnerability,
    );

    expect(otherDiagnostic.message).toBe(" VULNERABILITY: test description");
  });

  it("shouldn't set diagnostics to files", (): void => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mockretrievesDiagnostics = {
      set: jest.fn(),
    } as unknown as DiagnosticCollection;
    const mockNickname = "root1";
    const mockWorkspacePath = "/workspace";
    const finding: IFinding = getMockFinding("testGroup");
    const vulnerability: IVulnerability[] = [
      {
        commitHash: "testCommitHash",
        finding,
        id: "testVulnId",
        isAutoFixable: true,
        reportDate: "2023-02-01 15:48:40",
        rootNickname: "",
        specific: "10",
        state: "VULNERABLE",
        where: "root/file.ts",
      },
    ];

    const mockGroupBy = { "file.ts": [vulnerability[0]] };
    const mockDocument = {
      fileName: "/workspace/root1/file.ts",
      uri: Uri.file("/workspace/root1/file.ts"),
    } as TextDocument;
    jest.mocked(groupBy).mockReturnValue(mockGroupBy);
    jest.spyOn(workspace, "openTextDocument").mockResolvedValue(mockDocument);

    setDiagnosticsToAllFiles(
      mockretrievesDiagnostics,
      mockNickname,
      mockWorkspacePath,
      vulnerability,
    );

    expect(groupBy).toHaveBeenCalledWith(expect.any(Function), vulnerability);

    expect(workspace.openTextDocument).not.toHaveBeenCalledWith(
      Uri.file(join(mockWorkspacePath, "file.ts")),
    );

    jest.clearAllMocks();
  });

  it("should set diagnostics for all files with vulnerabilities", (): void => {
    expect.hasAssertions();

    // eslint-disable-next-line functional/immutable-data
    Object.defineProperty(workspace, "workspaceFolders", {
      configurable: true,
      value: [
        {
          uri: {
            fsPath: "/home/user/root1/",
            path: "/home/user/root1/",
          },
        },
      ],
    });

    const retrievesDiagnostics = {
      set: jest.fn(),
    } as unknown as DiagnosticCollection;
    const rootNickname = "root1";
    const workspacePath = "/home/user/root1";
    const finding: IFinding = getMockFinding("testGroup");
    const vulnerabilities: IVulnerability[] = [
      {
        commitHash: "testCommitHash",
        finding,
        id: "testVulnId",
        isAutoFixable: true,
        reportDate: "2023-02-01 15:48:40",
        rootNickname: "root1",
        specific: "10",
        state: "VULNERABLE",
        where: "root1/file1.ts",
      },
      {
        commitHash: "testCommitHash",
        finding: {
          ...finding,
          title: "181. Insecure service configuration - DynamoDB",
        },
        id: "testVulnId",
        isAutoFixable: true,
        reportDate: "2023-02-01 15:48:40",
        rootNickname: "root1",
        specific: "10",
        state: "VULNERABLE",
        where: "root1/file2.ts",
      },
    ];

    const mockGroupBy = {
      "file1.ts": [vulnerabilities[0]],
      "file2.ts": [vulnerabilities[1]],
    };
    const mockDocument = {
      fileName: "/home/user/root1/file1.ts",
      lineAt: jest
        .fn()
        .mockImplementation(
          (lineNumber: number): TextLine =>
            createMockTextLine(lineNumber, `Line content for ${lineNumber}`),
        ),
      lineCount: 100,
      uri: Uri.file("/home/user/root1/file1.ts"),
    } as unknown as TextDocument;
    jest.mocked(groupBy).mockReturnValue(mockGroupBy);
    jest.spyOn(workspace, "openTextDocument").mockResolvedValue(mockDocument);

    jest.mocked(existsSync).mockReturnValue(true);
    jest
      // eslint-disable-next-line jest/unbound-method
      .mocked(Uri.file)
      .mockImplementation((path): Uri => ({ fsPath: path }) as Uri);
    const onDidSaveTextDocument = jest.fn();
    jest
      .spyOn(workspace, "onDidSaveTextDocument")
      .mockImplementation((listener) => {
        onDidSaveTextDocument.mockImplementation(listener);

        return { dispose: jest.fn() };
      });

    setDiagnosticsToAllFiles(
      retrievesDiagnostics,
      rootNickname,
      workspacePath,
      vulnerabilities,
    );

    const savedDocument = {
      ...mockDocument,
      fileName: "/home/user/root1/file1.ts",
    } as TextDocument;
    onDidSaveTextDocument(savedDocument);

    expect(workspace.onDidSaveTextDocument).toHaveBeenCalledTimes(1);
    expect(groupBy).toHaveBeenCalledTimes(1);
    expect(groupBy).toHaveBeenCalledWith(expect.any(Function), vulnerabilities);
    expect(workspace.openTextDocument).toHaveBeenCalledTimes(2);
    expect(workspace.openTextDocument).toHaveBeenCalledWith({
      fsPath: "/home/user/root1/file1.ts",
    });
    expect(workspace.openTextDocument).toHaveBeenCalledWith({
      fsPath: "/home/user/root1/file2.ts",
    });

    setDiagnosticsToAllFiles(
      retrievesDiagnostics,
      rootNickname,
      workspacePath,
      vulnerabilities,
    );

    const savedDocumentTwo = {
      ...mockDocument,
      fileName: "/home/user/root1/file2.ts",
    } as TextDocument;
    onDidSaveTextDocument(savedDocumentTwo);

    expect(workspace.onDidSaveTextDocument).toHaveBeenCalledTimes(2);
    expect(groupBy).toHaveBeenCalledTimes(2);
    expect(groupBy).toHaveBeenCalledWith(expect.any(Function), vulnerabilities);
    expect(workspace.openTextDocument).toHaveBeenCalledTimes(4);
    expect(workspace.openTextDocument).toHaveBeenCalledWith({
      fsPath: "/home/user/root1/file1.ts",
    });
    expect(workspace.openTextDocument).toHaveBeenCalledWith({
      fsPath: "/home/user/root1/file2.ts",
    });
  });

  it("should subscribe to document changes", (): void => {
    expect.hasAssertions();

    // eslint-disable-next-line functional/immutable-data
    Object.defineProperty(window, "activeTextEditor", {
      configurable: true,
      value: window.activeTextEditor,
    });

    const mockContext = {
      subscriptions: { push: jest.fn() },
    } as unknown as ExtensionContext;
    const mockretrievesDiagnostics = {
      set: jest.fn(),
    } as unknown as DiagnosticCollection;

    subscribeToDocumentChanges(mockContext, mockretrievesDiagnostics);

    expect(mockContext.subscriptions.push).toHaveBeenCalledTimes(2);
    expect(rebase).toHaveBeenCalledTimes(4);

    // eslint-disable-next-line functional/immutable-data
    Object.defineProperty(window, "activeTextEditor", {
      configurable: true,
      value: {
        document: {
          fileName: "home/user/groups/my-group/root1/folder/file.ts",
          lineAt: jest
            .fn()
            .mockImplementation(
              (lineNumber: number): TextLine =>
                createMockTextLine(
                  lineNumber,
                  `Line content for ${lineNumber}`,
                ),
            ),
          lineCount: 100,
        },
      },
    });
    // eslint-disable-next-line functional/immutable-data
    Object.defineProperty(workspace, "workspaceFolders", {
      configurable: true,
      value: [
        {
          uri: Uri.file("home/user/groups/my-group/root1/folder/file.ts"),
        },
      ],
    });

    const mockPushOther = jest.fn();

    subscribeToDocumentChanges(
      {
        subscriptions: { push: mockPushOther },
      } as unknown as ExtensionContext,
      {
        set: jest.fn(),
      } as unknown as DiagnosticCollection,
    );

    expect(mockPushOther).toHaveBeenCalledTimes(2);
    expect(rebase).toHaveBeenCalledTimes(4);
  });

  it("should subscribe to document changes when the active text editor and text document changes", (): void => {
    expect.hasAssertions();

    const mockDocument = {
      fileName: "folder/file.ts",
      isClosed: false,
      isDirty: false,
      uri: Uri.file("/workspace/root1/folder/file.ts"),
    } as unknown as TextDocument;

    const mockEditor = {
      document: mockDocument,
      options: {},
      selection: null,
      selections: [],
      viewColumn: 1,
      visibleRanges: [],
    } as unknown as TextEditor;
    const mockretrievesDiagnostics = {
      set: jest.fn(),
    } as unknown as DiagnosticCollection;
    const mockChangeEvent: TextDocumentChangeEvent = {
      contentChanges: [],

      document: mockDocument,
      reason: undefined,
    };

    const mockContext = {
      subscriptions: { push: jest.fn() },
    } as unknown as ExtensionContext;
    const onDidChangeActiveTextEditor = jest.mocked(
      window.onDidChangeActiveTextEditor,
    ).mock.calls[0][0];
    const onDidChangeTextDocument = jest.mocked(
      workspace.onDidChangeTextDocument,
    ).mock.calls[0][0];
    subscribeToDocumentChanges(mockContext, mockretrievesDiagnostics);
    onDidChangeActiveTextEditor(mockEditor);
    onDidChangeTextDocument(mockChangeEvent);

    expect(getGroupGitRoots).toHaveBeenCalledTimes(2);
    expect(getGitRootVulnerabilities).toHaveBeenCalledTimes(1);

    expect(window.onDidChangeActiveTextEditor).toHaveBeenCalledTimes(3);
    expect(mockContext.subscriptions.push).toHaveBeenCalledTimes(2);
    expect(window.onDidChangeActiveTextEditor).toHaveBeenCalledWith(
      expect.any(Function),
    );
    expect(rebase).toHaveBeenCalledTimes(5);
  });
});
