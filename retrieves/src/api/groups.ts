import type { ApolloError } from "@apollo/client";
import { workspace } from "vscode";

import type { IGetGroupStakeholders, IGetGroups } from "./types";

import { GET_GROUPS, GET_GROUP_STAKEHOLDERS } from "@retrieves/queries";
import type { IGroup, IOrganization, IStakeholder } from "@retrieves/types";
import { API_CLIENT, handleGraphQlError } from "@retrieves/utils/apollo";
import { Logger } from "@retrieves/utils/logging";

const getGroups = async (): Promise<IGroup[]> => {
  const meGroupsResult = await API_CLIENT.query({
    // Groups under is_under_review will return errors that shouldn't affect
    // roots coming from other groups
    errorPolicy: "all",
    query: GET_GROUPS,
  });

  if (meGroupsResult.error) {
    await handleGraphQlError(meGroupsResult.error);
    Logger.error("Failed to get extraGroups: ", meGroupsResult.error);
  }

  const groups: IGroup[] = [
    ...workspace
      .getConfiguration("fluidattacks")
      .get("extraGroups", [])
      .map(
        (extraGroup): IGroup => ({
          name: extraGroup,
          roots: [],
          subscription: "continuous",
          userRole: "",
        }),
      ),
    ...(meGroupsResult as IGetGroups).data.me.organizations
      .map((org: IOrganization): IGroup[] => org.groups)
      .flat(),
  ];

  return groups;
};

const getGroupStakeholders = async (
  groupName: string,
): Promise<IStakeholder[]> => {
  const stakeholders: IStakeholder[] = await Promise.resolve(
    API_CLIENT.query({
      query: GET_GROUP_STAKEHOLDERS,
      variables: { groupName },
    })
      .then(
        (result: IGetGroupStakeholders): IStakeholder[] =>
          result.data.group.stakeholders,
      )
      .catch(async (error: unknown): Promise<IStakeholder[]> => {
        await handleGraphQlError(error as ApolloError);
        Logger.error("Failed to get group stakeholders to assign: ", error);

        return [];
      }),
  );

  return stakeholders;
};

export { getGroups, getGroupStakeholders };
