import type { ApolloError, ApolloQueryResult } from "@apollo/client";

import type {
  IToeLinesEdge,
  IToeLinesPaginator,
  IToeLinesQuery,
} from "./types";

import { GET_TOE_LINES } from "@retrieves/queries";
import { API_CLIENT, handleGraphQlError } from "@retrieves/utils/apollo";
import { Logger } from "@retrieves/utils/logging";

const getLines = async ({
  edges,
  endCursor,
  hasNextPage,
  groupName,
  rootId,
}: {
  endCursor: string;
  edges: IToeLinesEdge[];
  groupName: string;
  hasNextPage: boolean;
  rootId: string;
}): Promise<IToeLinesEdge[]> => {
  if (!hasNextPage) {
    return edges;
  }

  const result = await Promise.resolve(
    API_CLIENT.query<IToeLinesQuery>({
      query: GET_TOE_LINES,
      variables: { after: endCursor, first: 500, groupName, rootId },
    })
      .then(
        (_result: ApolloQueryResult<IToeLinesQuery>): IToeLinesPaginator => {
          return _result.data.group.toeLines;
        },
      )
      .catch(async (error: unknown): Promise<IToeLinesPaginator> => {
        await handleGraphQlError(error as ApolloError);
        Logger.error(
          `Failed to get ToE Lines from a root in ${groupName}:`,
          error,
        );

        return { edges: [], pageInfo: { endCursor: "", hasNextPage: false } };
      }),
  );

  return getLines({
    edges: [...edges, ...result.edges],
    endCursor: result.pageInfo.endCursor,
    groupName,
    hasNextPage: result.pageInfo.hasNextPage,
    rootId,
  });
};

const getToeLines = async (
  groupName: string,
  rootId: string,
): Promise<IToeLinesEdge[]> => {
  try {
    const result = await getLines({
      edges: [],
      endCursor: "",
      groupName,
      hasNextPage: true,
      rootId,
    });

    return result;
  } catch (error) {
    Logger.error(`Failed to get ToE Lines from a root in ${groupName}:`, error);
  }

  return [];
};

export { getToeLines };
