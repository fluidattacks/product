import type { ApolloError } from "@apollo/client";

import {
  ACCEPT_VULNERABILITY_TEMPORARILY,
  GET_VULNERABILITY,
  REQUEST_VULNERABILITIES_VERIFICATION,
} from "@retrieves/queries";
import type { IVulnerability } from "@retrieves/types";
import { API_CLIENT, handleGraphQlError } from "@retrieves/utils/apollo";
import { Logger } from "@retrieves/utils/logging";

interface IRefetchVulnerability {
  data: { vulnerability: IVulnerability };
}

interface IRequestReattackData {
  requestVulnerabilitiesVerification: { success: boolean; message?: string };
}

interface IRequestReattackResponse {
  data: IRequestReattackData;
}

interface IAcceptVulnerabilityData {
  updateVulnerabilitiesTreatment: { success: boolean; message?: string };
}

interface IAcceptVulnerabilityResponse {
  data: IAcceptVulnerabilityData;
}

const refetchVulnerability = async (
  oldVulnerability: IVulnerability,
): Promise<IVulnerability> => {
  const result: IRefetchVulnerability = await API_CLIENT.query({
    fetchPolicy: "network-only",
    query: GET_VULNERABILITY,
    variables: {
      identifier: oldVulnerability.id,
    },
  }).catch(async (error: unknown): Promise<IRefetchVulnerability> => {
    await handleGraphQlError(error as ApolloError);
    Logger.error(`Failed to refetch vulnerability:`, error);

    return {
      data: {
        vulnerability: oldVulnerability,
      },
    };
  });

  return result.data.vulnerability;
};

const requestReattack = async (
  findingId: string,
  justification: string,
  vulnerabilities: string[],
): Promise<IRequestReattackData> => {
  const result: IRequestReattackData = (
    await API_CLIENT.mutate({
      mutation: REQUEST_VULNERABILITIES_VERIFICATION,
      variables: {
        findingId,
        justification,
        vulnerabilities,
      },
    }).catch(async (error: unknown): Promise<IRequestReattackResponse> => {
      await handleGraphQlError(error as ApolloError);
      Logger.error(`Failed to request reattack:`, error);

      return {
        data: {
          requestVulnerabilitiesVerification: {
            message: (error as ApolloError).message,
            success: false,
          },
        },
      };
    })
  ).data;

  return result;
};

const acceptVulnerabilityTemporarily = async (
  findingId: string,
  vulnerabilityId: string,
  acceptanceDate: string,
  assigned: string,
  justification: string,
  treatment: string,
): Promise<IAcceptVulnerabilityData> => {
  const result: IAcceptVulnerabilityData = (
    await API_CLIENT.mutate({
      mutation: ACCEPT_VULNERABILITY_TEMPORARILY,
      variables: {
        acceptanceDate,
        assigned,
        findingId,
        justification,
        treatment,
        vulnerabilityId,
      },
    }).catch(async (error: unknown): Promise<IAcceptVulnerabilityResponse> => {
      await handleGraphQlError(error as ApolloError);
      Logger.error(`Failed to accept vulns:`, error);

      return {
        data: {
          updateVulnerabilitiesTreatment: {
            message: (error as ApolloError).message,
            success: false,
          },
        },
      };
    })
  ).data;

  return result;
};

export {
  refetchVulnerability,
  requestReattack,
  acceptVulnerabilityTemporarily,
};
export type { IRequestReattackData, IAcceptVulnerabilityData };
