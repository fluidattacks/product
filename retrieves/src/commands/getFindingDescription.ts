import type { MessageHandlerData } from "@estruyf/vscode";
import type { DiagnosticCollection, ExtensionContext } from "vscode";
import { ViewColumn, env, window } from "vscode";

import { fetchFindingDescription } from "@retrieves/api/finding";
import type { FindingTreeItem } from "@retrieves/treeItems/finding";
import type {
  IBugsnagConfig,
  IFinding,
  IFindingDescription,
  VulnerabilityDiagnostic,
} from "@retrieves/types";
import { getEnvironment } from "@retrieves/utils/environment";
import { getWebviewContent } from "@retrieves/utils/webview";

const findingDescription = async (
  context: ExtensionContext,
  finding: IFinding,
): Promise<void> => {
  const descriptionData = await fetchFindingDescription(finding.id);
  const panel = window.createWebviewPanel(
    "finding-description",
    finding.title,
    ViewColumn.Beside,
    {
      enableScripts: true,
      retainContextWhenHidden: true,
    },
  );
  // eslint-disable-next-line functional/immutable-data
  panel.webview.html = getWebviewContent(context, panel.webview);
  panel.webview.onDidReceiveMessage(
    (message: {
      command: string;
      requestId: string;
      payload: { message: string; description?: IFindingDescription };
    }): void => {
      const { command, requestId } = message;
      switch (command) {
        case "GET_ROUTE": {
          void panel.webview.postMessage({
            command,
            payload: "findingDescription",
            requestId,
          } as MessageHandlerData<string>);
          break;
        }
        case "GET_BUGSNAG_CONFIG": {
          void panel.webview.postMessage({
            command,
            payload: {
              isTelemetryEnabled: env.isTelemetryEnabled,
              retrievesVersion: (
                context.extension.packageJSON as Record<string, string>
              ).version,
              stage: getEnvironment(context),
            },
            // The requestId is used to identify the response
            requestId,
          } as MessageHandlerData<IBugsnagConfig>);
          break;
        }
        case "GET_DESCRIPTION": {
          void panel.webview.postMessage({
            command,
            payload: descriptionData,
            requestId,
          } as MessageHandlerData<IFindingDescription>);
          break;
        }
        default:
          break;
      }
    },
  );
};

const getFindingDescription = async (
  context: ExtensionContext,
  retrievesDiagnostics: DiagnosticCollection,
): Promise<void> => {
  const { activeTextEditor } = window;
  if (!activeTextEditor) {
    return;
  }
  const fileDiagnostics: readonly VulnerabilityDiagnostic[] | undefined =
    retrievesDiagnostics.get(activeTextEditor.document.uri);
  if (!fileDiagnostics) {
    void window.showInformationMessage(
      "This line does not contain vulnerabilities",
    );

    return;
  }
  const diagnostic = fileDiagnostics.find(
    (item): boolean =>
      item.range.start.line === activeTextEditor.selection.active.line &&
      item.source === "fluidattacks",
  );

  if (
    diagnostic?.code === undefined ||
    typeof diagnostic.code === "string" ||
    typeof diagnostic.code === "number"
  ) {
    void window.showInformationMessage(
      "This line does not contain vulnerabilities",
    );

    return;
  }

  const findingTitle: string = diagnostic.code.value.toString();

  await findingDescription(context, {
    id: diagnostic.findingId,
    title: findingTitle,
  } as IFinding);
};

const getFindingDescriptionTreeItem = async (
  context: ExtensionContext,
  item: FindingTreeItem,
): Promise<void> => {
  await findingDescription(context, item.finding);
};

export {
  findingDescription,
  getFindingDescription,
  getFindingDescriptionTreeItem,
};
