import { Uri, env } from "vscode";

import type { FindingTreeItem } from "@retrieves/treeItems/finding";
import { getAppUrl } from "@retrieves/utils/url";

const goToFindingTreeItem = (item: FindingTreeItem): void => {
  const baseUrl = getAppUrl();
  const url = Uri.parse(
    `${baseUrl}/groups/${item.finding.groupName}/vulns/${item.finding.id}/locations`,
  );
  void env.openExternal(url);
};

export { goToFindingTreeItem };
