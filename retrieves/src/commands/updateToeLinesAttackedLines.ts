import { window } from "vscode";

import {
  getGroupGitRootsSimple,
  markFileAsAttacked,
} from "@retrieves/api/root";
import type { IAttackedFile } from "@retrieves/types";
import { getRootInfoFromPath } from "@retrieves/utils/file";
import { getAttackedLines } from "@retrieves/utils/toeLines";

const showMessage = (attackedFileResult: IAttackedFile): void => {
  if (attackedFileResult.success) {
    void window.showInformationMessage("The file has been updated");
  } else {
    void window.showErrorMessage(
      attackedFileResult.message ?? "Failed to update file",
    );
  }
};

const updateToeLinesAttackedLines = async (
  item:
    | {
        comments: string;
        loc: number;
        filename: string;
        groupName: string;
        rootId: string;
      }
    | { path: string; loc: number; schema: string },
): Promise<void> => {
  const attackedLines = await getAttackedLines(item.loc);
  if (attackedLines === undefined) {
    return;
  }
  if ("path" in item) {
    const pathInfo = getRootInfoFromPath(item.path);
    if (pathInfo === null) {
      return;
    }

    const { fileRelativePath, groupName, nickname } = pathInfo;
    const result = await getGroupGitRootsSimple(groupName);
    const gitRoot = result.find((root): boolean => root.nickname === nickname);
    if (gitRoot === undefined) {
      return;
    }

    const resultMutation = await markFileAsAttacked(
      groupName,
      attackedLines,
      gitRoot.id,
      fileRelativePath,
      "",
    );

    showMessage(resultMutation);
  } else {
    const resultMutation = await markFileAsAttacked(
      item.groupName,
      attackedLines,
      item.rootId,
      item.filename,
      item.comments,
    );

    showMessage(resultMutation);
  }
};

export { updateToeLinesAttackedLines };
