let
  arch = let
    commit = "f292750f6681747f06b81800fa319b5bb853fe02";
    sha256 = "sha256:08p0a6kh0pzvvyx641bmndxwvva14wvr95p01327c12q3xk0kxd8";
    url =
      "https://gitlab.com/fluidattacks/universe/-/raw/${commit}/common/ci/arch.nix";
    src = builtins.fetchurl { inherit sha256 url; };
  in import src;

  rules = {
    default = arch.core.rules.titleRule {
      products = [ "all" "sifts" ];
      types = [ ];
    };
    noChore = arch.core.rules.titleRule {
      products = [ "all" "sifts" ];
      types = [ "feat" "fix" "refac" ];
    };
  };
in {
  pipelines = {
    sifts = {
      gitlabPath = "/sifts/pipeline/default.yaml";
      jobs = [{
        output = "/sifts/lint";
        gitlabExtra = arch.extras.default // {
          cache = {
            key = "$CI_COMMIT_REF_NAME-sifts-lint";
            paths = [ "sifts/.ruff_cache" "sifts/.mypy_cache" ];
          };
          rules = arch.rules.dev ++ [ rules.noChore ];
          stage = arch.stages.test;
          tags = [ arch.tags.sifts ];
        };
      }];
    };
  };
}
