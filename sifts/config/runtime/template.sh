# shellcheck shell=bash

function sifts_setup_runtime {
  # Leave sifts use the host's home in order to allow credentials to live
  # many hours
  export HOME
  export HOME_IMPURE

  if test -n "${HOME_IMPURE-}"; then
    HOME="${HOME_IMPURE}"
  fi
}

function sifts {
  python3.12 '__argSrcSifts__/src/cli.py' "$@"
}

sifts_setup_runtime
