{ makePythonEnvironment, inputs, makeTemplate, projectPath, ... }:
let
  pythonRequirements = makePythonEnvironment {
    pythonProjectDir = projectPath "/sifts";
    pythonVersion = "3.12";
  };
in {
  jobs."/sifts/config/runtime" = makeTemplate {
    replace = { __argSrcSifts__ = projectPath "/sifts"; };
    name = "sifts-config-runtime";
    searchPaths = {
      bin = [ inputs.nixpkgs.python312 ];
      pythonPackage = [ (projectPath "/sifts") ];
      source = [ pythonRequirements ];
    };
    template = ./template.sh;
  };
}
