{ makeScript, outputs, ... }: {
  imports = [ ./config/runtime/makes.nix ./pipeline/makes.nix ];
  jobs = {
    "/sifts" = makeScript {
      name = "sifts";
      searchPaths.source = [ outputs."/sifts/config/runtime" ];
      entrypoint = ./entrypoint.sh;
    };
    "/sifts/lint" = makeScript {
      name = "sifts-lint";
      searchPaths.source = [ outputs."/sifts/config/runtime" ];
      entrypoint = ''
        pushd sifts

        if test -n "''${CI:-}"; then
          ruff format --config ruff.toml --diff
          ruff check --config ruff.toml
        else
          ruff format --config ruff.toml
          ruff check --config ruff.toml --fix
        fi

        mypy --config-file mypy.ini src
      '';
    };
  };
}
