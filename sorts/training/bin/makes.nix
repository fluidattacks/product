{ makeScript, outputs, projectPath, ... }@makes_inputs:
let
  root = projectPath "/sorts/training";
  bundle = import "${root}/entrypoint.nix" makes_inputs;
  env = bundle.env.runtime;
in {
  jobs."/sorts/training/bin" = makeScript {
    searchPaths = {
      bin = [ env ];
      source = [ outputs."/common/utils/aws" outputs."/common/utils/sops" ];
    };
    name = "sorts-training-bin";
    entrypoint = ./entrypoint.sh;
  };
}
