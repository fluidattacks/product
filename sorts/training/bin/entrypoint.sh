# shellcheck shell=bash

function main {
  local mode="${1}"

  : \
    && sops_export_vars 'sorts/secrets/prod.yaml' \
      'SNOWFLAKE_ACCOUNT' \
      'SNOWFLAKE_PRIVATE_KEY' \
      'SNOWFLAKE_USER' \
    && aws_login "dev" "3600" \
    && sorts-training --mode "${mode}"

}

main "${@}"
