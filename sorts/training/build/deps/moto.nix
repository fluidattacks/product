{ lib, python_pkgs, }:
let version = "5.0.20";
in python_pkgs.moto.overridePythonAttrs (old: {
  inherit version;
  src = lib.fetchPypi {
    pname = "moto";
    inherit version;
    hash = "sha256-JLExnMZvgfQIF6V6yAYCpfGGJmm91iHw2Wq5iaZXglU=";
  };
  doCheck = false;
})
