import boto3
from sagemaker.tuner import (  # type: ignore [import]
    CategoricalParameter,
    ContinuousParameter,
    IntegerParameter,
)
from sklearn.ensemble import (  # type: ignore [import]
    GradientBoostingClassifier,
    HistGradientBoostingClassifier,
    RandomForestClassifier,
)
from sklearn.neural_network import (  # type: ignore [import]
    MLPClassifier,
)
from typing import (
    Dict,
    List,
    Union,
)
from xgboost import (
    XGBClassifier,
)

ModelType = Union[
    GradientBoostingClassifier,
    HistGradientBoostingClassifier,
    RandomForestClassifier,
    XGBClassifier,
]

PARALLEL_JOBS = 32

# AWS-related
S3_BUCKET_NAME: str = "sorts"
S3_RESOURCE = boto3.resource("s3")
S3_BUCKET = S3_RESOURCE.Bucket(S3_BUCKET_NAME)

DATASET_PATH: str = "s3://sorts/training/binary_encoded_training_data.csv"

SAGEMAKER_METRIC_DEFINITIONS: List[Dict[str, str]] = [
    {"Name": "precision", "Regex": "Precision: (.*?)%"},
    {"Name": "recall", "Regex": "Recall: (.*?)%"},
    {"Name": "fscore", "Regex": "F1-Score: (.*?)%"},
    {"Name": "accuracy", "Regex": "Accuracy: (.*?)%"},
    {"Name": "overfit", "Regex": "Overfit: (.*?)%"},
]

RESULT_HEADERS: List[str] = [
    "Model",
    "Features",
    "Precision",
    "Recall",
    "F1",
    "Accuracy",
    "Overfit",
    "TunedParams",
    "Time",
]
MODELS: Dict[str, ModelType] = {
    "gradientboostingclassifier": GradientBoostingClassifier,
    "histgradientboostingclassifier": HistGradientBoostingClassifier,
    "randomforestclassifier": RandomForestClassifier,
    "xgbclassifier": XGBClassifier,
    "mlpclassifier": MLPClassifier,
}

# Hyperparameters
MODEL_HYPERPARAMETERS = {
    "gradientboostingclassifier": {
        "n_estimators": IntegerParameter(20, 150, scaling_type="Logarithmic"),
        "learning_rate": ContinuousParameter(
            0.01, 0.7, scaling_type="Logarithmic"
        ),
    },
    "histgradientboostingclassifier": {
        "max_leaf_nodes": CategoricalParameter([0, 5, 25, 50, 100, 250, 500]),
        "learning_rate": ContinuousParameter(
            0.01, 1, scaling_type="Logarithmic"
        ),
    },
    "randomforestclassifier": {
        "n_estimators": IntegerParameter(1, 200),
        "max_depth": IntegerParameter(1, 50),
        "min_samples_leaf": IntegerParameter(4, 20),
    },
    "xgbclassifier": {
        "learning_rate": ContinuousParameter(
            0.01, 0.2, scaling_type="Logarithmic"
        ),
        "max_depth": IntegerParameter(3, 7),
        "min_child_weight": IntegerParameter(3, 7),
    },
    "mlpclassifier": {
        "hidden_layer_sizes": [(50, 50, 50), (50, 100, 50), (100,)],
        "activation": CategoricalParameter(
            ["relu", "tanh", "identity", "logistic"]
        ),
        "solver": CategoricalParameter(["lbfgs", "sgd", "adam"]),
    },
}
