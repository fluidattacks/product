#!/usr/bin/env python3

from botocore.exceptions import (
    ClientError,
)
from concurrent.futures import (
    ThreadPoolExecutor,
)
import os
from sagemaker.sklearn import (  # type: ignore [import]
    SKLearn,
)
from sagemaker.sklearn.estimator import (  # type: ignore [import]
    SKLearn as SKLearnEstimator,
)
from snowflake_connection import (
    db as snowflake,
)
import time
from training_deployment.constants import (
    DATASET_PATH,
    MODELS,
    PARALLEL_JOBS,
    RESULT_HEADERS,
    S3_BUCKET,
    SAGEMAKER_METRIC_DEFINITIONS,
)
from training_deployment.sagemaker.logs import (
    log,
)
from training_deployment.sagemaker.utils import (
    get_previous_training_results,
    update_results_csv,
)


def get_estimator(
    model: str,
    job_type: str,
    job_index: int = 0,
    training_script: str = "sorts/training/training_scripts/train.py",
) -> SKLearnEstimator:
    hyperparameters = (
        {"model": model.split(":")[0], "index": job_index}
        if job_type == "training"
        else {"model": model.split(":")[0]}
    )
    sklearn_estimator: SKLearnEstimator = SKLearn(
        entry_point=training_script,
        dependencies=[
            "sorts/training/training_scripts",
            "sorts/training/training_scripts/requirements.txt",
        ],
        framework_version="1.2-1",
        instance_count=1,
        role="arn:aws:iam::205810638802:role/prod_sorts",
        output_path="s3://sorts/training-output",
        base_job_name=f"sorts-training-test-{model.split(':')[0].lower()}",
        hyperparameters=hyperparameters,
        metric_definitions=SAGEMAKER_METRIC_DEFINITIONS,
        debugger_hook_config=False,
        instance_type="ml.m5.2xlarge",
        tags=[
            {"Key": "fluidattacks:line", "Value": "cost"},
            {"Key": "fluidattacks:comp", "Value": "sorts"},
        ],
    )

    return sklearn_estimator


def deploy_training_job(model: str, job_index: int, wait_time: int) -> None:
    # Wait so the name of the job isn't repeated and fail to run
    time.sleep(wait_time)
    log("info", "Deploying training job for %s...", model)
    sklearn_estimator: SKLearnEstimator = get_estimator(
        model, "training", job_index
    )
    sklearn_estimator.fit({"train": DATASET_PATH})


def process_training_results(model: str) -> None:
    model_results = [RESULT_HEADERS]

    for obj in S3_BUCKET.objects.filter(
        Prefix=f"training-output/results/{model}_train_results_"
    ):
        previous_results = get_previous_training_results(
            os.path.basename(obj.key)
        )
        model_results = model_results + previous_results[1:]
        try:
            obj.delete()
        except ClientError as error:
            if error.response["Error"]["Code"] == "404":
                log(
                    "info",
                    f"[INFO] File not found: {obj.key}",
                )

    results_filename: str = f"{model}_train_results.csv"
    update_results_csv(results_filename, model_results)

    for result in model_results[1:]:
        combination_train_results = {
            "model": result[0],
            "features": result[1],
            "precision": result[2],
            "recall": result[3],
            "f_score": result[4],
            "overfit": result[6],
            "tuned_parameters": result[7],
            "training_time": result[8],
        }
        snowflake.insert("training", combination_train_results)


def train_sorts_model() -> None:
    all_models: list[str] = list(MODELS.keys())
    with ThreadPoolExecutor(
        max_workers=PARALLEL_JOBS * len(all_models)
    ) as executor:
        executor.map(
            lambda x: deploy_training_job(*x),
            zip(
                sorted(all_models * PARALLEL_JOBS),
                list(range(PARALLEL_JOBS)) * len(all_models),
                list(range(0, len(all_models) * PARALLEL_JOBS * 2, 2)),
            ),
        )
    for model_name in all_models:
        process_training_results(model_name)
