from botocore.exceptions import (
    ClientError,
)
import csv
import os
import tempfile
from training_deployment.constants import (
    S3_BUCKET,
)
from typing import (
    List,
)


def get_previous_training_results(results_filename: str) -> List[List[str]]:
    previous_results: list[list[str]] = []
    with tempfile.TemporaryDirectory() as tmp_dir:
        local_file: str = os.path.join(tmp_dir, results_filename)
        remote_file: str = f"training-output/results/{results_filename}"
        try:
            S3_BUCKET.Object(remote_file).download_file(local_file)
        except ClientError as error:
            if error.response["Error"]["Code"] == "404":
                return previous_results
        else:
            with open(local_file, "r", encoding="utf8") as csv_file:
                csv_reader = csv.reader(csv_file)
                previous_results.extend(csv_reader)

    return previous_results


def update_results_csv(filename: str, results: List[List[str]]) -> None:
    with open(filename, "w", newline="", encoding="utf8") as results_file:
        csv_writer = csv.writer(results_file)
        csv_writer.writerows(results)
    S3_BUCKET.Object(f"training-output/results/{filename}").upload_file(
        filename
    )
