{ makePythonVscodeSettings, projectPath, ... }@makes_inputs:
let
  root = projectPath "/sorts/training";
  bundle = import "${root}/entrypoint.nix" makes_inputs;
in {
  jobs."/sorts/training/env/dev" = makePythonVscodeSettings {
    env = bundle.env.dev;
    bins = [ ];
    name = "sorts-training-env-dev";
  };
}
