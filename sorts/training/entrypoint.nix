{ inputs, ... }@makes_inputs:
let
  python_version = "python311";
  nixpkgs = inputs.nixpkgs-sorts // { inherit (inputs) nix-filter; };
  out = import ./build {
    inherit makes_inputs nixpkgs python_version;
    src = nixpkgs.nix-filter {
      root = ./.;
      include = [
        "training_deployment"
        "training_scripts"
        "tests"
        "pyproject.toml"
        "mypy.ini"
      ];
    };
  };
in out
