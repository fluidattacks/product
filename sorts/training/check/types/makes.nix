{ makeScript, projectPath, ... }@makes_inputs:
let
  root = projectPath "/sorts/training";
  bundle = import "${root}/entrypoint.nix" makes_inputs;
  check = bundle.check.types;
in {
  jobs."/sorts/training/check/types" = makeScript {
    searchPaths = { bin = [ check ]; };
    name = "sorts-training-check-types";
    entrypoint = "";
  };
}
