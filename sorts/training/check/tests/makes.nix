{ makeScript, projectPath, ... }@makes_inputs:
let
  root = projectPath "/sorts/training";
  bundle = import "${root}/entrypoint.nix" makes_inputs;
  env = bundle.env.dev;
in {
  jobs."/sorts/training/check/tests" = makeScript {
    searchPaths = { bin = [ env ]; };
    name = "sorts-training-check-tests";
    entrypoint = ./entrypoint.sh;
  };
}
