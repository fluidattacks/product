# shellcheck shell=bash

function main {
  local pytest_args=(
    ./tests
    --cov 'training_deployment'
    --cov 'training_scripts'
    --cov-branch
    --cov-report 'term'
    --cov-report 'xml:coverage.xml'
    --disable-warnings
    --no-cov-on-fail
    -vvl
  )

  pushd sorts/training || return 1
  pytest "${pytest_args[@]}"
}

main "${@}"
