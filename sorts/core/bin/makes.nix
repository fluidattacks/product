{ makeScript, projectPath, outputs, ... }@makes_inputs:
let
  root = projectPath "/sorts/core";
  static = projectPath "/sorts/static";
  criteria = projectPath "/common/criteria/src/vulnerabilities/data.yaml";
  bundle = import "${root}/entrypoint.nix" makes_inputs;
  env = bundle.env.runtime;
in {
  jobs."/sorts/core/bin" = makeScript {
    name = "sorts";
    searchPaths = {
      bin = [ env outputs."/melts" ];
      export = [
        [
          "CORALOGIX_LOG_URL"
          "https://ingress.cx498-aws-us-west-2.coralogix.com:443/api/v1/logs"
          ""
        ]
        [ "SORTS_STATIC_PATH" static "" ]
        [ "SORTS_CRITERIA_VULNERABILITIES" criteria "" ]
      ];
    };
    entrypoint = ''sorts "''${@}"'';
  };
}
