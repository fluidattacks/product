{ inputs, makePythonVscodeSettings, projectPath, ... }@makes_inputs:
let
  root = projectPath "/sorts/core";
  bundle = import "${root}/entrypoint.nix" makes_inputs;
in {
  jobs."/sorts/core/env/dev" = makePythonVscodeSettings {
    env = bundle.env.dev;
    bins = [ ];
    name = "sorts-core-env-dev";
  };
}
