from collections.abc import (
    Iterator,
)
from git.cmd import (
    Git,
)
from git.repo import (
    Repo,
)
import os
import pytest
import tempfile
from unittest.mock import (
    patch,
)


@pytest.fixture(autouse=True, scope="session")
def test_clone_repo() -> Iterator[str]:
    with tempfile.TemporaryDirectory() as tmp_dir:
        repo_path: str = os.path.join(tmp_dir, "requests")
        repo_url: str = "https://github.com/psf/requests.git"
        repo_version: str = "v2.24.0"
        Repo.clone_from(repo_url, repo_path)
        git_repo: Git = Git(repo_path)
        git_repo.checkout(repo_version)
        yield tmp_dir


@pytest.fixture(name="test_excluded_repos")
def fixture_test_excluded_repos(
    monkeypatch: pytest.MonkeyPatch,
) -> Iterator[None]:
    with patch.dict(os.environ, clear=True):
        envvars = {"EXCLUDED_REPOS": "test"}
        for key, value in envvars.items():
            monkeypatch.setenv(key, value)
        yield
