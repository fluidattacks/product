from click.testing import (
    CliRunner,
)
import os
import pytest
from sorts.cli import (
    cli,
)
import tarfile
import tempfile
from unittest.mock import (
    AsyncMock,
    MagicMock,
    patch,
)

DATA_PATH: str = f"{os.path.dirname(__file__)}/data"


@pytest.mark.parametrize(
    "correct_path, out", ((True, "json"), (False, "csv"), (True, "csv"))
)
def test_repo_mode(correct_path: bool, out: str) -> None:
    runner = CliRunner()
    if correct_path:
        with tempfile.TemporaryDirectory() as tmp_dir:
            tar_path = os.path.join(DATA_PATH, "test_repo.tgz")
            with tarfile.open(tar_path, "r:gz") as repo_tar:
                repo_tar.extractall(tmp_dir)
                repo_path = f"{tmp_dir}/test_repo"
                params = [repo_path, "--out", out]
                result = runner.invoke(cli=cli, args=params)
                assert result.exit_code == 0
    else:
        params = ["incorrect_path", "--out", out]
        result = runner.invoke(cli=cli, args=params)
        assert result.exit_code == 1


@patch("sorts.integrates.snowflake_dal.snowflake", new_callable=MagicMock)
@patch("sorts.predict.file.download_repo", new_callable=AsyncMock)
@patch("sorts.cli._send_analytics", new_callable=MagicMock)
@patch("sorts.predict.file.S3_BUCKET", new_callable=MagicMock)
def test_subscription_mode(
    _: MagicMock,
    mock_analytics: MagicMock,
    mock_download_repo: AsyncMock,
    mock_execute: MagicMock,
) -> None:
    mock_execute.fetch_data.return_value = [("PK#root_id", "test_repo")]
    with tempfile.TemporaryDirectory() as tmp_dir:
        tar_path = os.path.join(DATA_PATH, "test_repo.tgz")
        with tarfile.open(tar_path, "r:gz") as repo_tar:
            repo_path = f"{tmp_dir}/test_group"
            repo_tar.extractall(repo_path)
            params = [
                repo_path,
                "--mode",
                "sub",
            ]
            runner = CliRunner()
            result = runner.invoke(cli=cli, args=params)
            mock_download_repo.assert_called_once()
            mock_analytics.assert_called_once()
            assert result.exit_code == 0


@patch("sorts.training.file.get_group_toe_lines", new_callable=MagicMock)
@patch("sorts.training.file.get_vulnerable_files", new_callable=MagicMock)
@patch("sorts.training.file.download_repo", new_callable=AsyncMock)
@patch("sorts.cli._send_analytics", new_callable=MagicMock)
@patch("sorts.training.file.get_group_roots", new_callable=MagicMock)
def test_extraction_mode(
    _: MagicMock,
    mock_analytics: MagicMock,
    mock_download_repo: AsyncMock,
    mock_get_vulnerable_files: MagicMock,
    mock_get_toe_lines: MagicMock,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    monkeypatch.setenv("EXCLUDED_REPOS", "test")
    mock_get_vulnerable_files.return_value = ["test_repo/test.py"]
    mock_get_toe_lines.return_value = {}
    with tempfile.TemporaryDirectory() as tmp_dir:
        tar_path = os.path.join(DATA_PATH, "test_repo.tgz")
        with tarfile.open(tar_path, "r:gz") as repo_tar:
            repo_path = f"{tmp_dir}/test_group"
            repo_tar.extractall(repo_path)
            params = [
                repo_path,
                "--mode",
                "extract-features",
            ]
            runner = CliRunner()
            result = runner.invoke(cli=cli, args=params)
            mock_download_repo.assert_called_once()
            mock_analytics.assert_called_once()
            assert result.exit_code == 0


@patch("sorts.utils.merge_features.snowflake", new_callable=MagicMock)
@patch("sorts.utils.merge_features.S3_BUCKET", new_callable=MagicMock)
def test_merge_mode(
    _: MagicMock,
    mock_snowflake: MagicMock,
) -> None:
    runner = CliRunner()
    params = [
        "placeholder",
        "--mode",
        "merge-features",
    ]
    result = runner.invoke(cli=cli, args=params)
    mock_snowflake.insert.assert_called()
    assert result.exit_code == 0
