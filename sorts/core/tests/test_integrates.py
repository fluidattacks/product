from sorts.integrates.snowflake_dal import (
    get_findings,
    get_group_roots,
    get_group_toe_lines,
    get_vulnerabilities,
)
from sorts.integrates.domain import (
    get_vulnerable_files,
)
from sorts.integrates.typing import (
    Finding,
    ToeLines,
    Vulnerability,
    VulnerabilityKindEnum,
)
from unittest.mock import (
    MagicMock,
    patch,
)


@patch("sorts.integrates.domain.get_group_roots", new_callable=MagicMock)
@patch("sorts.integrates.domain.get_group_toe_lines", new_callable=MagicMock)
@patch("sorts.integrates.domain.get_findings", new_callable=MagicMock)
@patch("sorts.integrates.domain.get_vulnerabilities", new_callable=MagicMock)
def test_get_vulnerable_files(
    mock_get_vulnerabilities: MagicMock,
    mock_get_finding_ids: MagicMock,
    mock_get_group_toe_lines: MagicMock,
    _: MagicMock,
) -> None:
    mock_get_group_toe_lines.return_value = {
        "test_repo/test.py": ToeLines(
            attacked_lines=2,
            filename="test.py",
            loc=2,
            root_nickname="test_repo",
        ),
        "test_repo/test_vuln.py": ToeLines(
            attacked_lines=5,
            filename="test_vuln.py",
            loc=5,
            root_nickname="test_repo",
        ),
    }
    mock_get_vulnerabilities.return_value = [
        Vulnerability(
            current_state="OPEN",
            kind=VulnerabilityKindEnum.LINES,
            title="test_title",
            where="test_repo/test_vuln.py",
        )
    ]
    mock_get_finding_ids.return_value = ["test_finding_id"]
    result = get_vulnerable_files("test_group")
    assert result == ["test_repo/test_vuln.py"]


@patch("sorts.integrates.snowflake_dal.snowflake", new_callable=MagicMock)
def test_get_group_git_roots(
    mock_snowflake: MagicMock,
) -> None:
    mock_snowflake.fetch_data.return_value = [
        ("test#id_1", "nickname_1"),
        ("test#id_2", "nickname_2"),
    ]
    result = get_group_roots("test_group")
    assert result == {"id_1": "nickname_1", "id_2": "nickname_2"}


@patch("sorts.integrates.snowflake_dal.snowflake", new_callable=MagicMock)
def test_get_vulnerabilities(
    mock_snowflake: MagicMock,
) -> None:
    mock_snowflake.fetch_data.return_value = [
        ("SAFE", "lines", "test_where"),
    ]
    result = get_vulnerabilities(Finding(id="test_id", title="test_title"))
    expected_result = [
        Vulnerability(
            current_state="SAFE",
            kind=VulnerabilityKindEnum.LINES,
            title="test_title",
            where="test_where",
        )
    ]
    assert result == expected_result


@patch("sorts.integrates.snowflake_dal.snowflake", new_callable=MagicMock)
def test_get_toe_lines(
    mock_snowflake: MagicMock,
) -> None:
    mock_snowflake.fetch_data.return_value = {
        "group": ToeLines(
            attacked_lines=20,
            filename="test.py",
            loc=20,
            root_nickname="test_nickname",
        )
    }
    mock_snowflake.fetch_data.return_value = [
        (20, "test.py", 20, "root_id")
    ]
    result = get_group_toe_lines("test_group", {"root_id": "test_nickname"})
    expected_result = {
        "test_nickname/test.py": ToeLines(
            attacked_lines=20,
            filename="test.py",
            loc=20,
            root_nickname="test_nickname",
        )
    }
    assert result == expected_result


@patch("sorts.integrates.snowflake_dal.snowflake", new_callable=MagicMock)
def test_get_finding_ids(
    mock_snowflake: MagicMock,
) -> None:
    mock_snowflake.fetch_data.return_value = [
        ("test#test_id_1", "test_title_1"),
        ("test#test_id_2", "test_title_2"),
        ("test#test_id_3", "test_title_3"),
    ]
    result = get_findings("test_group")
    expected_result = [
        Finding(
            id="test_id_1",
            title="test_title_1",
        ),
        Finding(
            id="test_id_2",
            title="test_title_2",
        ),
        Finding(
            id="test_id_3",
            title="test_title_3",
        ),
    ]
    assert result == expected_result
