from snowflake_connection import (
    db as snowflake,
)
from sorts.integrates.typing import (
    Finding,
    ToeLines,
    Vulnerability,
    VulnerabilityKindEnum,
)


def get_findings(group: str) -> list[Finding]:
    """Fetches all finding ids for a group"""
    findings: list[Finding] = []
    query = """
        SELECT pk_str, title_str
        FROM dynamodb.integrates_vms
        WHERE pk_str LIKE 'FIN#%%'
        AND sk_str LIKE %s
    """
    result = snowflake.fetch_data(query, (f"GROUP#{group}",))
    if result:
        findings = [
            Finding(id=finding[0].split("#")[1], title=finding[1])
            for finding in result
        ]
    return findings


def get_vulnerabilities(finding: Finding) -> list[Vulnerability]:
    """Fetches all reported vulnerabilities in a finding, open or closed"""
    vulnerabilities: list[Vulnerability] = []
    query = """
        SELECT state__status_str, type_str, state__where_str
        FROM dynamodb.integrates_vms
        WHERE pk_str LIKE 'VULN#%%'
        AND sk_str LIKE %s
    """
    result = snowflake.fetch_data(query, (f"FIN#{finding.id}",))

    if result:
        vulnerabilities.extend(
            [
                Vulnerability(
                    current_state=vuln[0],
                    kind=VulnerabilityKindEnum(vuln[1].lower()),
                    title=finding.title,
                    where=vuln[2],
                )
                for vuln in result
            ]
        )

    return vulnerabilities


def get_group_roots(group_name: str) -> dict[str, str]:
    roots: dict[str, str] = {}
    query = """
        SELECT pk_str, state__nickname_str, type_str
        FROM dynamodb.integrates_vms
        WHERE pk_str LIKE 'ROOT#%%'
        AND sk_str LIKE %s
        AND type_str = 'Git'
    """
    result = snowflake.fetch_data(query, (f"GROUP#{group_name}",))

    if result:
        for root in result:
            roots[root[0].split("#")[1]] = root[1]

    return roots


def get_group_toe_lines(
    group_name: str, roots: dict[str, str]
) -> dict[str, ToeLines]:
    group_toe_lines: dict[str, ToeLines] = {}
    query = """
        SELECT state__attacked_lines_int, filename_str, state__loc_int, root_id_str
        FROM dynamodb.integrates_vms
        WHERE pk_str LIKE %s
        AND sk_str LIKE 'LINES#ROOT#%%#FILENAME#%%'
        AND state__be_present_bool = true
    """
    result = snowflake.fetch_data(query, (f"GROUP#{group_name}",))
    if result:
        for toe_line in result:
            root_nickname = roots[toe_line[3]]
            filename = toe_line[1]
            group_toe_lines[f"{root_nickname}/{filename}"] = ToeLines(
                attacked_lines=toe_line[0],
                filename=filename,
                loc=toe_line[2],
                root_nickname=root_nickname,
            )

    return group_toe_lines
