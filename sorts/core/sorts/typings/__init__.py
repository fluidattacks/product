from sklearn.ensemble import (  # type: ignore [import]
    GradientBoostingClassifier,
    HistGradientBoostingClassifier,
    RandomForestClassifier,
)
from typing import (
    Any,
    Callable,
    Dict,
    List,
    Tuple,
    TypedDict,
    TypeVar,
    Union,
)
from xgboost import (
    XGBClassifier,
)

Item = Dict[str, Any]
Score = Tuple[float, float, float, None]
Model = Union[
    GradientBoostingClassifier,
    HistGradientBoostingClassifier,
    RandomForestClassifier,
    XGBClassifier,
]

Tfun = TypeVar("Tfun", bound=Callable[..., Any])


class GitMetrics(TypedDict):
    author_email: List[str]
    commit_hash: List[str]
    date_iso_format: List[str]
    current_filepath: Union[str, None]
    stats: List[str]
