from category_encoders import (  # type: ignore [import]
    BinaryEncoder,
)
from datetime import (
    datetime,
)
from functools import (
    partial,
)
import os
import pandas as pd
from pandas import (
    DataFrame,
    Series,
)
import pytz
from sorts.constants import (
    FERNET,
)
from sorts.typings import (
    GitMetrics,
)
from sorts.utils.logs import (
    log,
    log_exception,
)
from sorts.utils.repositories import (
    get_log_file_metrics,
    get_repository_log,
    parse_git_shortstat,
)
from sorts.utils.static import (
    get_extensions_list,
)
import tempfile
from typing import (
    NamedTuple,
)

FILE_FEATURES = [
    "num_commits",
    "num_unique_authors",
    "file_age",
    "midnight_commits",
    "risky_commits",
    "seldom_contributors",
    "num_lines",
    "commit_frequency",
    "busy_file",
    "last_commit_days",
    "extension",
]


class FileFeatures(NamedTuple):
    num_commits: int = -1
    num_unique_authors: int = -1
    file_age: int = -1
    midnight_commits: int = -1
    risky_commits: int = -1
    seldom_contributors: int = -1
    num_lines: int = -1
    commit_frequency: float = -1.0
    busy_file: int = -1
    last_commit_days: int = -1
    extension: str = ""


def encode_extensions(training_df: DataFrame) -> None:
    extensions: list[str] = get_extensions_list()
    extensions_df: DataFrame = pd.DataFrame(extensions, columns=["extension"])
    encoder: BinaryEncoder = BinaryEncoder(cols=["extension"], return_df=True)
    encoder.fit(extensions_df)
    encoded_extensions = encoder.transform(training_df[["extension"]])
    training_df[
        encoded_extensions.columns.tolist()
    ] = encoded_extensions.values.tolist()


def get_features(
    row: Series, git_metrics_dict: dict[str, GitMetrics]
) -> FileFeatures:
    features = FileFeatures()
    try:
        repo_path: str = row["repo"]
        repo_name: str = os.path.basename(repo_path)
        file_relative: str = row["file"].replace(f"{repo_name}/", "", 1)
        git_metrics = git_metrics_dict[file_relative]
        file_age = get_file_age(git_metrics)
        num_commits = get_num_commits(git_metrics)
        unique_authors_count = get_unique_authors_count(git_metrics)
        features = FileFeatures(
            num_commits=num_commits,
            num_unique_authors=unique_authors_count,
            file_age=file_age,
            midnight_commits=get_midnight_commits(git_metrics),
            risky_commits=get_risky_commits(git_metrics),
            seldom_contributors=get_seldom_contributors(git_metrics),
            num_lines=get_num_lines(os.path.join(repo_path, file_relative)),
            commit_frequency=(
                round(num_commits / file_age, 4) if file_age else num_commits
            ),
            busy_file=1 if unique_authors_count > 9 else 0,
            last_commit_days=get_last_commit_days(git_metrics),
            extension=file_relative.split(".")[-1].lower(),
        )
    except FileNotFoundError as exc:
        log(
            "warning",
            f"Log file for repo {repo_name} does not exist "
            f" - Error: {exc}",
        )
    except (IndexError, KeyError) as exc:
        log(
            "warning",
            f"File {os.path.join(repo_name, file_relative)} "
            f"has no git history - Error: {exc}",
        )
    return features


def get_file_age(git_metrics: GitMetrics) -> int:
    """Gets the number of days since the file was created"""
    today: datetime = datetime.now(pytz.utc)
    commit_date_history: list[str] = git_metrics["date_iso_format"]
    file_creation_date: str = commit_date_history[-1]

    return (today - datetime.fromisoformat(file_creation_date)).days


def get_num_commits(git_metrics: GitMetrics) -> int:
    """Gets the number of commits that have modified a file"""
    commit_history: list[str] = git_metrics["commit_hash"]

    return len(commit_history)


def get_num_lines(file_path: str) -> int:
    """Gets the number of lines that a file has"""
    result: int = 0
    try:
        with open(file_path, "rb") as file:
            bufgen = iter(partial(file.raw.read, 1024 * 1024), b"")
            result = sum(buf.count(b"\n") for buf in bufgen)  # type: ignore
    except FileNotFoundError:
        log("warning", "File %s not found", file_path)

    return result


def get_midnight_commits(git_metrics: GitMetrics) -> int:
    """Gets the number of times a file was modified between 12-6 AM"""
    commit_date_history: list[str] = git_metrics["date_iso_format"]
    commit_hour_history: list[int] = [
        datetime.fromisoformat(date).hour for date in commit_date_history
    ]

    return sum(1 for hour in commit_hour_history if 0 <= hour < 6)


def _count_deltas(stat: str) -> int:
    insertions, deletions = parse_git_shortstat(stat.replace("--", ", "))
    deltas = insertions + deletions
    return deltas


def _is_risky_commit(stat: str, threshold: int = 200) -> bool:
    deltas = _count_deltas(stat)
    return deltas > threshold


def get_risky_commits(git_metrics: GitMetrics) -> int:
    """Gets the number of commits that are considered risky based on deltas"""
    commit_stat_history: list[str] = git_metrics["stats"]
    risky_commits: int = sum(map(_is_risky_commit, commit_stat_history))

    return risky_commits


def _is_seldom_contributor(
    authors: list[str], author: str, avg: float
) -> bool:
    commits_count: int = authors.count(author)
    return commits_count < avg


def get_seldom_contributors(git_metrics: GitMetrics) -> int:
    """Gets the number of authors that contributed below the average
    Note that it considers different emails to be different users, which
    is different to `get_unique_authors_count`, which considers only different
    usernames
    """
    authors_history: list[str] = git_metrics["author_email"]
    unique_authors: set[str] = set(authors_history)
    avg_commits_per_author: float = round(
        len(authors_history) / len(unique_authors), 4
    )
    seldom_contributors: int = sum(
        map(
            lambda author: _is_seldom_contributor(
                authors_history, author, avg_commits_per_author
            ),
            unique_authors,
        )
    )

    return seldom_contributors


def get_unique_authors_count(git_metrics: GitMetrics) -> int:
    """Gets the number of unique authors that modified a file
    counting them by username, not by complete email including domain
    """
    unique_emails: set[str] = set(git_metrics["author_email"])
    authors_usernames: list[str] = list(
        map(lambda email: email.split("@")[0], unique_emails)
    )
    unique_usernames = set(authors_usernames)
    unique_authors_count: int = len(unique_usernames)

    return unique_authors_count


def get_last_commit_days(git_metrics: GitMetrics) -> int:
    """Gets the number of days since the file was last changed"""
    today: datetime = datetime.now(pytz.utc)
    commit_date_history: list[str] = git_metrics["date_iso_format"]
    last_commit_date: str = commit_date_history[0]

    return (today - datetime.fromisoformat(last_commit_date)).days


def encrypt_column_values(value: str) -> str:
    return FERNET.encrypt(value.encode()).decode()


def format_dataset(
    training_df: DataFrame, encrypt_filenames: bool
) -> DataFrame:
    training_df.drop(
        training_df[training_df["file_age"] == -1].index, inplace=True
    )
    training_df.drop(
        training_df[training_df["num_lines"] == 0].index, inplace=True
    )
    training_df.reset_index(inplace=True, drop=True)
    if not training_df.empty:
        encode_extensions(training_df)

    # Encode string-like columns
    if encrypt_filenames:
        training_df["file"] = training_df["file"].apply(encrypt_column_values)
        training_df["repo"] = training_df["repo"].apply(encrypt_column_values)

    return training_df


def extract_features(
    repo_files_df: DataFrame, encrypt_filenames: bool = True
) -> DataFrame:
    """Extract features from the file Git history and add them to the DF"""
    try:
        with tempfile.TemporaryDirectory() as tmp_dir:
            repo_path: str = repo_files_df["repo"][0]
            get_repository_log(tmp_dir, repo_path)

            # Get features into dataset
            repo_name: str = os.path.basename(repo_path)
            git_metrics = get_log_file_metrics(tmp_dir, repo_name)
            repo_files_df[FILE_FEATURES] = repo_files_df.apply(
                get_features, args=(git_metrics,), axis=1, result_type="expand"
            )
            format_dataset(repo_files_df, encrypt_filenames)
    except KeyError as exc:
        log_exception(
            "error",
            exc,
            message=(
                "DataFrame does not have one of the required keys "
                "'file'/'repo', there are probably no files to scan"
            ),
        )
    except IndexError as exc:
        log_exception(
            "warning",
            exc,
            message="This repository is empty. Skipping...",
        )

    return repo_files_df
