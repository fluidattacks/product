import boto3
import bugsnag  # type: ignore [import]
from coralogix.handlers import (  # type: ignore [import]
    CoralogixLogger,
)
from cryptography.fernet import (
    Fernet,
)
import logging
import os
from os import (
    environ,
)
import re
from sorts.typings import (
    Item,
)
import yaml

STATIC_DIR: str = environ["SORTS_STATIC_PATH"]

STAT_REGEX: re.Pattern = re.compile(
    r"([0-9]+ files? changed)?"
    r"(, (?P<insertions>[0-9]+) insertions \(\+\))?"
    r"(, (?P<deletions>[0-9]+) deletions \(\-\))?"
)
RENAME_REGEX: re.Pattern = re.compile(
    r"(?P<pre_path>.*)\{(?P<old_name>.*) "
    r"=> (?P<new_name>.*)\}(?P<post_path>.*)"
    r"|(?P<simple_old_name>.*) => (?P<simple_new_name>.*)"
)

with open(
    os.environ["SORTS_CRITERIA_VULNERABILITIES"], encoding="utf-8"
) as handler:
    CRITERIA_VULNERABILITIES: Item = yaml.safe_load(handler.read())

# Logging
LOGGER_HANDLER: logging.StreamHandler = logging.StreamHandler()
LOGGER: logging.Logger = logging.getLogger("Sorts")
LOGGER_BUGSNAG_HANDLER = bugsnag.handlers.BugsnagHandler()

# Encryption
FERNET = Fernet(environ.get("FERNET_TOKEN", Fernet.generate_key()))

# AWS-related
S3_BUCKET_NAME: str = "sorts"
S3_RESOURCE = boto3.resource("s3")
S3_BUCKET = S3_RESOURCE.Bucket(S3_BUCKET_NAME)
REPOS_S3_BUCKET = S3_RESOURCE.Bucket("integrates.continuous-repositories")
_CORALOGIX_PRIVATE_KEY = "cxtp_6O5a6PUUzQziUhAZpUigRwEmVkc1H7"
LOGGER_CORALOGIX_HANDLER = CoralogixLogger(
    _CORALOGIX_PRIVATE_KEY,
    "sorts",
    "sorts-execution",
)

# Threshold defining the minimum elements that our dataset must have (*1000)
DATASET_THRESHOLD: int = 40
