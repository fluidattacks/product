import numpy as np
from numpy import (
    ndarray,
)
import pandas as pd
from pandas import (
    DataFrame,
)
import shap  # type: ignore [import]
from sklearn.preprocessing import (  # type: ignore [import]
    MinMaxScaler,
)
from sorts.typings import (
    Model as ModelType,
)
from sorts.utils.logs import (
    log,
)
from sorts.utils.static import (
    load_model,
)


def build_results(data: DataFrame, filename: str) -> None:
    scope: str = filename.split(".")[0].split("_")[-1]
    copy: DataFrame = data.copy()
    copy["prob_vuln"] = round(copy.prob_vuln * 100, 1)
    copy["prob_vuln"] = copy["prob_vuln"].clip(lower=0)
    sorted_files: DataFrame = copy.sort_values(
        by="prob_vuln", ascending=False
    ).reset_index(drop=True)[[scope, "prob_vuln"]]
    if filename.split(".")[-1] == "json":
        sorted_files.to_json(filename, orient="records", indent=2)
    else:
        sorted_files.to_csv(filename, index=False)

    log("info", "Results saved to file %s.", filename)


def display_results(data: DataFrame, size: int = 20) -> None:
    copy: DataFrame = data.copy()
    copy["prob_vuln"] = round(copy.prob_vuln * 100, 1)
    copy["prob_vuln"] = copy["prob_vuln"].clip(lower=0)
    copy_sorted: DataFrame = (
        copy.sort_values(by="prob_vuln", ascending=False)
        .reset_index(drop=True)
        .drop(columns=["pred", "prob_safe"])
    )
    table: str = copy_sorted.head(size).to_markdown(index=False)
    log("info", f"Here are the top {size} files to check:\n\n{table}")


def get_risky_features(input_data: DataFrame) -> list[list[float]]:
    model: ModelType = load_model("model")
    explainer = shap.TreeExplainer(model)
    shap_values: list[list[float]] = explainer.shap_values(input_data)[
        1
    ].tolist()
    risky_feature_indices = [
        shap_value.index(max(shap_value[:6])) for shap_value in shap_values
    ]
    return [model.feature_names[index] for index in risky_feature_indices]


def predict_vuln_prob(
    predict_df: DataFrame,
    extension_features: list[str],
    enable_risky_features: bool,
) -> DataFrame:
    """Uses model to make predictions based on the input"""
    model: ModelType = load_model("model")
    input_data: DataFrame = predict_df[
        model.feature_names + extension_features
    ]
    if hasattr(model, "scaled_input"):
        scaler: MinMaxScaler = load_model("scaler")
        input_data = pd.DataFrame(scaler.transform(input_data))

    probability_prediction: ndarray = model.predict_proba(input_data)
    class_prediction: ndarray = model.predict(input_data)

    log(
        "info",
        "Model info -> "
        f"recall: {model.recall}%, precision: {model.precision}%",
    )
    risky_features = []
    if enable_risky_features:
        try:
            risky_features = get_risky_features(input_data)
        except (AttributeError, IndexError, ValueError):
            log(
                "warning",
                "Risky features cannot be determined with current model",
            )
    merged_predictions: ndarray = np.column_stack(
        [class_prediction, probability_prediction]
    )
    return pd.concat(
        [
            predict_df[["file"]],
            pd.DataFrame(
                merged_predictions, columns=["pred", "prob_safe", "prob_vuln"]
            ),
            pd.DataFrame(risky_features, columns=["risky_feature"])
            if risky_features
            else None,
        ],
        axis=1,
    )
