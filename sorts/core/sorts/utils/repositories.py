from pathlib import (
    Path,
)
import shutil
import tarfile
from git.cmd import (
    Git,
)
from git.exc import (
    GitCommandError,
    GitCommandNotFound,
)
from git.repo import (
    Repo,
)
import os
import re
from sorts.constants import (
    RENAME_REGEX,
    REPOS_S3_BUCKET,
    STAT_REGEX,
)
from sorts.typings import (
    GitMetrics,
)
from sorts.utils.logs import (
    log,
    log_exception,
)
from botocore.exceptions import (
    ClientError,
)

def _reset_repo(repo_path: str) -> bool:
    try:
        Git().execute(
            [
                "git",
                "config",
                "--global",
                "--add",
                "safe.directory",
                "*",
            ],
        )
    except GitCommandError as exc:
        log_exception(
            "warning", exc, message=f"Could not add {repo_path} as safe directory"
        )
    try:
        repo = Repo(repo_path)
        repo.git.reset("--hard", "HEAD")
    except GitCommandError as exc:
        log_exception(
            "warning", exc, message=f"Expand repositories has failed for repository {repo_path}"
        )
        return False

    return True


def _is_member_safe(
    member: tarfile.TarInfo,
) -> bool:
    return not (
        member.issym() or member.islnk() or os.path.isabs(member.name) or "../" in member.name
    )


def _safe_extract_tar(tar_handler: tarfile.TarFile, file_path: Path) -> None:
    for member in tar_handler.getmembers():
        if not _is_member_safe(member):
            log("error", f"Unsafe path detected: {member.name}")
            continue
        try:
            tar_handler.extract(member, path=file_path, numeric_owner=True)
            log("info", f"Extracted: {member.name}")
        except tarfile.ExtractError as exc:
            log("error", f"Error extracting {member.name} - Error: {exc}")


def download_repo(group_name: str, repo_nickname: str) -> None:
    """Download a specific repository from a group stored in S3"""
    tar_name = f"{repo_nickname}.tar.gz"
    tar_path = Path("groups") / group_name / tar_name
    repo_path = Path(tar_path.parent) / f"{repo_nickname}"
    os.makedirs(tar_path.parent, exist_ok=True)
    try:
        REPOS_S3_BUCKET.download_file(f"{group_name}/{tar_name}", tar_path)
    except ClientError as error:
        if error.response["Error"]["Code"] == "404":
            log(
                "error",
                f"[INFO] Repo {repo_nickname} from group {group_name} not found in S3",
            )
            return
    try:
        shutil.rmtree(tar_path, ignore_errors=True)
        with tarfile.open(tar_path, "r:gz") as repo_tar:
            _safe_extract_tar(repo_tar, tar_path.parent)
    except PermissionError:
        log("error", f"Failed to extract repository from {repo_nickname}")
        return

    os.remove(tar_path)
    if not _reset_repo(str(repo_path.absolute())):
        shutil.rmtree(repo_path, ignore_errors=True)


def is_working_repo(repo: str) -> bool:
    excluded_repos = os.environ["EXCLUDED_REPOS"].split(" ")
    return test_repo(repo) and repo not in excluded_repos


def _get_changed_file_paths(path_info: dict[str, str]) -> tuple[str, str]:
    if path_info["simple_old_name"]:
        newer_file_path = (
            f'{path_info["pre_path"]}'
            f'{path_info["simple_new_name"]}'
            f'{path_info["post_path"]}'
        )
        older_file_path = (
            f'{path_info["pre_path"]}'
            f'{path_info["simple_old_name"]}'
            f'{path_info["post_path"]}'
        )
    else:
        newer_file_path = (
            (
                f'{path_info["pre_path"]}'
                f'{path_info["new_name"]}'
                f'{path_info["post_path"][1:]}'
            )
            if path_info["new_name"] == ""
            else (
                f'{path_info["pre_path"]}'
                f'{path_info["new_name"]}'
                f'{path_info["post_path"]}'
            )
        )
        older_file_path = (
            (
                f'{path_info["pre_path"][:-1]}'
                f'{path_info["old_name"]}'
                f'{path_info["post_path"]}'
            )
            if path_info["old_name"] == ""
            else (
                f'{path_info["pre_path"]}'
                f'{path_info["old_name"]}'
                f'{path_info["post_path"]}'
            )
        )

    return (newer_file_path, older_file_path)


def _validate_name_change(
    filename: str, git_metrics_dict: dict[str, GitMetrics]
) -> str:
    rename_match = re.match(RENAME_REGEX, filename)
    if rename_match:
        path_info: dict[str, str] = rename_match.groupdict(default="")
        newer_file_path, older_file_path = _get_changed_file_paths(path_info)
        if not git_metrics_dict.get(newer_file_path, {}):
            git_metrics_dict[newer_file_path] = GitMetrics(
                author_email=[],
                commit_hash=[],
                date_iso_format=[],
                current_filepath=None,
                stats=[],
            )
        git_metrics_dict[older_file_path] = git_metrics_dict.pop(
            newer_file_path
        )
        if not git_metrics_dict[older_file_path]["current_filepath"]:
            git_metrics_dict[older_file_path][
                "current_filepath"
            ] = newer_file_path
        filename = older_file_path
    return filename


def get_log_file_metrics(logs_dir: str, repo: str) -> dict[str, GitMetrics]:
    """Read the log file and extract the author, hash, date and diff"""
    git_metrics_dict: dict[str, GitMetrics] = {}
    cursor: str = ""
    with open(
        os.path.join(logs_dir, f"{repo}.log"),
        "r",
        encoding="utf8",
    ) as log_file:
        for line in log_file:
            # An empty line marks the start of a new commit diff
            if not line.strip("\n"):
                cursor = "info"
                continue
            # Next, there is a line with the format 'Hash,Author,Date'
            if cursor == "info" or (
                re.match(r"\b[0-9a-f]{40}\b", line.strip("\n").split(",")[0])
            ):
                commit: str = line.strip("\n").split(",")[0]
                author: str = line.strip("\n").split(",")[1]
                date: str = line.strip("\n").split(",")[-1]
                cursor = "diff"
                continue
            # Next, there is a list of changed files and the changed lines
            # with the format 'Additions    Deletions   File'
            if cursor == "diff":
                diff_info: list[str] = line.strip("\n").split("\t")
                # Keeps track of the file if its name was changed
                filename = _validate_name_change(
                    diff_info[2], git_metrics_dict
                )
                if not git_metrics_dict.get(filename, {}):
                    git_metrics_dict[filename] = GitMetrics(
                        author_email=[],
                        commit_hash=[],
                        date_iso_format=[],
                        current_filepath=None,
                        stats=[],
                    )
                git_metrics_dict[filename]["author_email"].append(author)
                git_metrics_dict[filename]["commit_hash"].append(commit)
                git_metrics_dict[filename]["date_iso_format"].append(date)
                git_metrics_dict[filename]["stats"].append(
                    f"1 file changed, {diff_info[0]} insertions (+), "
                    f"{diff_info[1]} deletions (-)"
                )
    # Create new dictionary with current filepaths as keys if they exist
    git_metrics_dict_updated = {}
    for key, value in git_metrics_dict.items():
        if current_filepath := value["current_filepath"]:
            git_metrics_dict_updated[current_filepath] = value
        else:
            git_metrics_dict_updated[key] = value
    return git_metrics_dict_updated


def get_repository_log(dir_: str, repo_path: str) -> None:
    """Gets the complete log of the repository and saves it to a file"""
    repo: str = os.path.basename(repo_path)
    try:
        git_repo: Git = Git(repo_path)
        Git().execute(
            [
                "git",
                "config",
                "--global",
                "--add",
                "safe.directory",
                "*",
            ]
        )
        git_log: str = git_repo.log(
            "--no-merges", "--numstat", "--pretty=%n%H,%ae,%aI%n"
        ).replace("\n\n\n", "\n")
        with open(
            os.path.join(dir_, f"{repo}.log"),
            "w",
            encoding="utf8",
        ) as log_file:
            log_file.write(git_log)
    except GitCommandNotFound as exc:
        log_exception("warning", exc, message=f"Repo {repo} doesn't exist")
    except GitCommandError as exc:
        log_exception(
            "warning", exc, message=f"Could not add {repo} as safe directory"
        )


def get_repository_files(repo_path: str) -> list[str]:
    """lists all the files inside a repository relative to the repository"""
    ignore_dirs: list[str] = [".git"]
    return [
        os.path.join(path, filename).replace(
            f"{os.path.dirname(repo_path)}/", ""
        )
        for path, _, files in os.walk(repo_path)
        for filename in files
        if all(  # pylint: disable=use-a-generator
            [dir_ not in path for dir_ in ignore_dirs]
        )
    ]


def parse_git_shortstat(stat: str) -> tuple[int, int]:
    insertions: int = 0
    deletions: int = 0
    match = re.match(STAT_REGEX, stat.strip())
    if match:
        groups: dict[str, str] = match.groupdict()
        if groups["insertions"]:
            insertions = int(groups["insertions"])
        if groups["deletions"]:
            deletions = int(groups["deletions"])
    return insertions, deletions


def test_repo(repo_path: str) -> bool:
    """Checks correct configuration of a repository by running `git log`"""
    git_repo: Git = Git(repo_path)
    is_repo_ok: bool = True
    try:
        git_repo.log()
    except GitCommandError:
        is_repo_ok = False
    return is_repo_ok
