import bugsnag  # type: ignore [import]
from contextvars import (
    ContextVar,
)
import os

# Constants
META: ContextVar[dict[str, str] | None] = ContextVar("META", default=None)


def guess_environment() -> str:
    if any(
        (
            "product/" in os.path.dirname(__file__),
            os.environ.get("CI_COMMIT_REF_NAME", "trunk") != "trunk",
        )
    ):
        return "development"

    return "production"  # pragma: no cover


def configure_bugsnag(**data: str) -> None:
    # Metadata configuration
    META.set(data)
    # Initialization
    bugsnag.configure(notify_release_stages=["production", "development"])
    bugsnag.configure(
        # There is no problem in making this key public
        # it's intentional so we can monitor Sorts stability in remote users
        api_key="1d6da191337056ca6fa2c47f47be2a3a",
        # Assume development stage if this source file is within repository
        release_stage=guess_environment(),
    )
    bugsnag.start_session()
    bugsnag.send_sessions()
