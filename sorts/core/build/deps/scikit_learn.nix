{ lib, python_pkgs, }:
let version = "1.2.2";
in python_pkgs.scikit-learn.overridePythonAttrs (old: {
  inherit version;
  src = lib.fetchPypi {
    pname = "scikit-learn";
    inherit version;
    hash = "sha256-hCmuow7CTnqMftij+mITrfOBSm776gnhbgoMceGho9c=";
  };
})
