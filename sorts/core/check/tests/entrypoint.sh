# shellcheck shell=bash

function main {
  local pytest_args=(
    ./tests
    --cov 'sorts'
    --cov-branch
    --cov-report 'term'
    --cov-report 'xml:coverage.xml'
    --disable-warnings
    --no-cov-on-fail
    -vvl
  )

  aws_login "dev" "3600"
  pushd sorts/core || return 1
  pytest "${pytest_args[@]}"
}

main "${@}"
