{ inputs, makeScript, projectPath, outputs, ... }@makes_inputs:
let
  root = projectPath "/sorts/core";
  static = projectPath "/sorts/static";
  criteria = projectPath "/common/criteria/src/vulnerabilities/data.yaml";
  bundle = import "${root}/entrypoint.nix" makes_inputs;
  env = bundle.env.dev;
in {
  jobs."/sorts/core/check/tests" = makeScript {
    name = "sorts-core-check-tests";
    searchPaths = {
      bin = [ env inputs.nixpkgs.git ];
      source = [ outputs."/common/utils/aws" outputs."/common/utils/sops" ];
      export = [
        [ "SORTS_STATIC_PATH" static "" ]
        [ "SORTS_CRITERIA_VULNERABILITIES" criteria "" ]
      ];
    };
    entrypoint = ./entrypoint.sh;
  };
}
