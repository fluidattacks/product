{ makeScript, projectPath, ... }@makes_inputs:
let
  root = projectPath "/sorts/core";
  bundle = import "${root}/entrypoint.nix" makes_inputs;
  inherit (bundle.env) types;
in {
  jobs."/sorts/core/check/types" = makeScript {
    searchPaths = { bin = [ types ]; };
    name = "sorts-core-check-types";
    entrypoint = "";
  };
}
