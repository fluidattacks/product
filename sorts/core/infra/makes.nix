{ outputs, ... }: {
  deployTerraform = {
    modules = {
      sorts = {
        setup = [ outputs."/secretsForAwsFromGitlab/prodSorts" ];
        src = "/sorts/core/infra/src";
        version = "1.0";
      };
    };
  };
  lintTerraform = {
    modules = {
      sorts = {
        setup = [ outputs."/secretsForAwsFromGitlab/dev" ];
        src = "/sorts/core/infra/src";
        version = "1.0";
      };
    };
  };
  testTerraform = {
    modules = {
      sorts = {
        setup = [ outputs."/secretsForAwsFromGitlab/dev" ];
        src = "/sorts/core/infra/src";
        version = "1.0";
      };
    };
  };
}
