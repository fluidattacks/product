resource "aws_ecr_repository" "sorts" {
  name                 = "sorts"
  image_tag_mutability = "MUTABLE"

  tags = {
    "Name"              = "sorts.ecr"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "sorts"
  }
}
