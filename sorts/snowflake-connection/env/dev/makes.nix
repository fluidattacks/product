{ makePythonVscodeSettings, projectPath, ... }@makes_inputs:
let
  root = projectPath "/sorts/snowflake-connection";
  bundle = import "${root}/entrypoint.nix" makes_inputs;
in {
  jobs."/sorts/snowflake-connection/env/dev" = makePythonVscodeSettings {
    env = bundle.env.dev;
    bins = [ ];
    name = "sorts-snowflake-connection-env-dev";
  };
}
