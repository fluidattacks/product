{ inputs, makePythonPyprojectPackage, projectPath, pythonOverrideUtils, ... }:
let
  python_version = "python311";
  makes_inputs = {
    inherit (inputs) nix-filter;
    inherit projectPath makePythonPyprojectPackage pythonOverrideUtils;
  };
  nixpkgs = inputs.nixpkgs-sorts;
  out = import ./build {
    inherit makes_inputs nixpkgs python_version;
    src = inputs.nix-filter {
      root = ./.;
      include = [ "snowflake_connection" "tests" "pyproject.toml" "mypy.ini" ];
    };
  };
in out
