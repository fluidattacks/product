{ makeScript, projectPath, ... }@makes_inputs:
let
  root = projectPath "/sorts/snowflake-connection";
  bundle = import "${root}/entrypoint.nix" makes_inputs;
  check = bundle.check.types;
in {
  jobs."/sorts/snowflake-connection/check/tests" = makeScript {
    searchPaths = { bin = [ check ]; };
    name = "sorts-snowflake-connection-check-tests";
    entrypoint = "";
  };
}
