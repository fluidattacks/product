{ outputs, ... }: {
  dev = {
    sorts = { source = [ outputs."/sorts/core/env/dev" ]; };
    snowflakeConnection = {
      source = [ outputs."/sorts/snowflake-connection/env/dev" ];
    };
    sortsTraining = { source = [ outputs."/sorts/training/env/dev" ]; };
  };
}
