{ makeScript, outputs, ... }: {
  imports = [ ./test/makes.nix ];
  jobs."/sorts/jobs/sorts-execute" = makeScript {
    name = "sorts-jobs-sorts-execute";
    searchPaths = {
      bin = [ outputs."/sorts/core/bin" ];
      source = [
        outputs."/common/utils/aws"
        outputs."/common/utils/sops"
        outputs."/common/utils/common"
        outputs."/observes/common/list-groups"
      ];
    };
    entrypoint = ./entrypoint.sh;
  };
}
