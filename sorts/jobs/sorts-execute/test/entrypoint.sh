# shellcheck shell=bash

function execute {
  local group="${1}"

  echo "[INFO] Running sorts on group ${group}:" \
    && if sorts --mode sub "groups/${group}"; then
      echo "[INFO] Successfully executed on: ${group}"
    else
      echo "[ERROR] While running Sorts on: ${group}"
    fi \
    && rm -rf "groups/${group}"
}

function main {

  : \
    && sops_export_vars 'sorts/secrets/prod.yaml' \
      'FERNET_TOKEN' \
      'SNOWFLAKE_ACCOUNT' \
      'SNOWFLAKE_PRIVATE_KEY' \
      'SNOWFLAKE_USER' \
    && sops_export_vars 'common/secrets/dev.yaml' \
      INTEGRATES_API_TOKEN \
    && aws_login "dev" "3600" \
    && execute "abingdon"

}

main "${@}"
