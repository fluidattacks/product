{ makeScript, outputs, ... }: {
  jobs."/sorts/jobs/sorts-execute/test" = makeScript {
    name = "sorts-jobs-sorts-execute-test";
    searchPaths = {
      bin = [ outputs."/sorts/core/bin" ];
      source = [
        outputs."/common/utils/aws"
        outputs."/common/utils/sops"
        outputs."/common/utils/common"
      ];
    };
    entrypoint = ./entrypoint.sh;
  };
}
