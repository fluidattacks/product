# shellcheck shell=bash

function upload_sorts_results_to_s3 {
  local group="${1}"
  local target="s3://sorts/features/"

  : \
    && aws_login "prod_sorts" "3600" \
    && echo "[INFO] Uploading Sorts feature extraction results to S3" \
    && aws_s3_sync "${PWD}" "${target}" --exclude "*" --include "${group}*.csv" \
    && rm -rf "${group}"*".csv"
}

function extract_features {
  local group="${1}"

  echo "[INFO] Running feature extraction on ${group}:" \
    && if sorts --mode extract-features "groups/${group}"; then
      echo "[INFO] Successfully executed on: ${group}"
    else
      echo "[ERROR] While running Sorts on: ${group}"
    fi \
    && upload_sorts_results_to_s3 "${group}" \
    && rm -rf "groups/${group}"
}

function main {
  local parallel="${1}"
  local groups_file
  local sorted_groups_file

  : \
    && aws_login "dev" "3600" \
    && sops_export_vars 'sorts/secrets/prod.yaml' \
      'EXCLUDED_REPOS' \
      'SNOWFLAKE_ACCOUNT' \
      'SNOWFLAKE_PRIVATE_KEY' \
      'SNOWFLAKE_USER' \
    && groups_file="$(mktemp)" \
    && sorted_groups_file="$(mktemp)" \
    && list_groups "${groups_file}" \
    && sort "${groups_file}" -o "${sorted_groups_file}" \
    && execute_chunk_parallel extract_features "${sorted_groups_file}" "${parallel}" "batch"

}

main "${@}"
