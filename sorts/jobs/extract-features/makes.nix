{ makeScript, outputs, ... }: {
  jobs."/sorts/jobs/extract-features" = makeScript {
    name = "sorts-jobs-extract-features";
    searchPaths = {
      bin = [ outputs."/sorts/core/bin" ];
      source = [
        outputs."/common/utils/aws"
        outputs."/common/utils/sops"
        outputs."/common/utils/common"
        outputs."/observes/common/list-groups"
      ];
    };
    entrypoint = ./entrypoint.sh;
  };
}
