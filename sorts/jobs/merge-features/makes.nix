{ makeScript, outputs, ... }: {
  jobs."/sorts/jobs/merge-features" = makeScript {
    name = "sorts-jobs-merge-features";
    searchPaths = {
      bin = [ outputs."/sorts/core/bin" ];
      source = [ outputs."/common/utils/aws" outputs."/common/utils/sops" ];
    };
    entrypoint = ./entrypoint.sh;
  };
}
