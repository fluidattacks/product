# shellcheck shell=bash

function main {
  : \
    && sops_export_vars 'sorts/secrets/prod.yaml' \
      'SNOWFLAKE_ACCOUNT' \
      'SNOWFLAKE_PRIVATE_KEY' \
      'SNOWFLAKE_USER' \
    && echo "[INFO] Preparing extracted features data..." \
    && sorts --mode merge-features placeholder \
    || return 1

}

main "${@}"
