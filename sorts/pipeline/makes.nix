let
  arch = let
    commit = "8aa9c7eb6fe5bc870a4c8494049dd41f27819a27";
    sha256 = "sha256:0fkpzjywjlcvjwpav6im3020ff2f9y2k7kb9kdrkyd2w7v7dmifz";
    url =
      "https://gitlab.com/fluidattacks/universe/-/raw/${commit}/common/ci/arch.nix";
    src = builtins.fetchurl { inherit sha256 url; };
  in import src;

  rules = {
    default = arch.core.rules.titleRule {
      products = [ "all" "sorts" ];
      types = [ ];
    };
    noChore = arch.core.rules.titleRule {
      products = [ "all" "sorts" ];
      types = [ "feat" "fix" "refac" ];
    };
  };
in {
  pipelines = {
    sorts = {
      gitlabPath = "/sorts/pipeline/default.yaml";
      jobs = [
        {
          output = "/pipelineOnGitlab/sorts";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.noChore ];
            stage = arch.stages.test;
            tags = [ arch.tags.sorts ];
          };
        }
        {
          output = "/sorts/core/check/types";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.noChore ];
            stage = arch.stages.test;
            tags = [ arch.tags.sorts ];
          };
        }
        {
          output = "/sorts/core/check/tests";
          gitlabExtra = arch.extras.default // {
            artifacts = {
              paths = [ "sorts/core/coverage.xml" ];
              expire_in = "1 day";
            };
            rules = arch.rules.dev ++ [ rules.noChore ];
            stage = arch.stages.test;
            tags = [ arch.tags.sorts ];
          };
        }
        {
          output = "/sorts/training/check/types";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.noChore ];
            stage = arch.stages.test;
            tags = [ arch.tags.sorts ];
          };
        }
        {
          output = "/sorts/training/check/tests";
          gitlabExtra = arch.extras.default // {
            artifacts = {
              paths = [ "sorts/training/coverage.xml" ];
              expire_in = "1 day";
            };
            rules = arch.rules.dev ++ [ rules.noChore ];
            stage = arch.stages.test;
            tags = [ arch.tags.sorts ];
          };
        }
        {
          output = "/sorts/snowflake-connection/check/types";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.noChore ];
            stage = arch.stages.test;
            tags = [ arch.tags.sorts ];
          };
        }
        {
          output = "/sorts/snowflake-connection/check/tests";
          gitlabExtra = arch.extras.default // {
            artifacts = {
              paths = [ "sorts/snowflake-connection/coverage.xml" ];
              expire_in = "1 day";
            };
            rules = arch.rules.dev ++ [ rules.noChore ];
            stage = arch.stages.test;
            tags = [ arch.tags.sorts ];
          };
        }
        {
          output = "/sorts/coverage";
          gitlabExtra = arch.extras.default // {
            needs = [ "/sorts/core/check/tests" "/sorts/training/check/tests" ];
            rules = arch.rules.dev ++ [ rules.noChore ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.sorts ];
            variables.GIT_DEPTH = 1000;
          };
        }
        {
          output = "/lintTerraform/sorts";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.noChore ];
            stage = arch.stages.test;
            tags = [ arch.tags.sorts ];
          };
        }
        {
          output = "/testTerraform/sorts";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.noChore ];
            stage = arch.stages.test;
            tags = [ arch.tags.sorts ];
          };
        }
        {
          output = "/deployTerraform/sorts";
          gitlabExtra = arch.extras.default // {
            resource_group = "deploy/$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.sorts ];
          };
        }
      ];
    };
  };
}
