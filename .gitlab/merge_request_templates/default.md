%{first_multiline_commit}
---
These checklists encourage us to confirm any changes have been analyzed to reduce risks in quality, performance, reliability, security, and maintainability.

### Author Checklist
- [ ] I ensured the use case that I'm working on is covered by tests
- [ ] I checked on my ephemeral environment (if there is one) that the product works as expected after this change

### Reviewer Checklist
- [ ] I understand what is changing and its impacts
- [ ] I assessed the need for providing recommendations on how the issue is being addressed
- [ ] I checked on the author's ephemeral environment (if there is one) that the product works as expected after this change
- [ ] I will assist the author in resolving any issues arising from the change
