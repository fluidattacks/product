<!-- Issues are public, they should not contain confidential information -->

### What is the current _bug_ behavior? how can we reproduce it?

### What is the expected _correct_ behavior?

### Relevant logs and/or screenshots

### Possible fixes

### Steps

- [ ] Make sure that the
      [code contributions checklist](https://help.fluidattacks.com/portal/en/kb/articles/development-contributing)
      has been followed.

### Postmortem

#### Impact:
<!-- What was the problem?
Who or what was affected?
How bad was the impact? -->

#### Cause:
<!-- What caused the issue? Reference the Merge Request that caused the issue.
Why did it happen? -->

### Solution:
<!-- How was it fixed? Reference the Merge Request that fixed the issue.
What changes were made?
What did you do to prevent this from happening again? -->

### Conclusion:
<!-- Fill in by RCA team. -->

/label ~"type::bug"
