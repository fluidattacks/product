<!-- Issues are public, they should not contain confidential information -->

# [short title of solved problem and solution]

Status: <!-- * [proposed | rejected | accepted | deprecated ] optional -->
* Deciders: <!--[list everyone involved in the decision]  optional -->
* Date: <!--[YYYY-MM-DD when the decision was last updated]  optional -->

Technical Story:<!--  [description | ticket/issue URL] optional -->

## Context and Problem Statement

<!-- [Describe the context and problem statement, e.g., in free form using two to three sentences. You may want to articulate the problem in form of a question.] -->

## Decision Drivers <!-- optional -->

<!--
Decision Drivers are the key factors or forces that influence decision-making in an Architectural Decision Record (ADR). These drivers can include technical requirements, time constraints, cost considerations, compatibility with existing systems, or any other aspect crucial to the decision. Clearly identifying these drivers helps justify the chosen solution and ensures that decisions align with the project's needs and constraints.
-->

<!-- * Scalability: The solution must support continuous growth in the number of users and data without compromising performance.
* Cost: A cost-effective solution is needed due to the project's limited budget. -->


## Considered Options

* [option 1]
* [option 2]
* [option 3]
<!-- numbers of options can vary -->

## Decision Outcome

<!-- Chosen option: "[option 1]", because [justification. e.g., only option, which meets k.o. criterion decision driver | which resolves force force | … | comes out best (see below)]. -->

### Positive Consequences <!-- optional -->

<!-- * [e.g., improvement of quality attribute satisfaction, follow-up decisions required, …]
* … -->

### Negative Consequences <!-- optional -->

<!-- * [e.g., compromising quality attribute, follow-up decisions required, …]
* … -->

## Pros and Cons of the Options <!-- optional --># [short title of solved problem and solution]

### [option 1]

<!-- [example | description | pointer to more information | …] <!-- optional -->
<!--
* Good, because [argument a]
* Good, because [argument b]
* Bad, because [argument c] -->

<!-- numbers of pros and cons can vary -->

### [option 2]

<!-- [example | description | pointer to more information | …] <!-- optional -->
<!--
* Good, because [argument a]
* Bad, because [argument c] -->

<!-- numbers of pros and cons can vary -->


## Links <!-- optional -->

<!-- * [Link type] [Link to ADR] -->
<!-- numbers of links can vary -->
