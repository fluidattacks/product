let
  commit = "2c33fd98c30fa5e8075fba45fef72027694254f8";
  sha256 = "sha256:1q3js002wz0sprr02fja50wp3sb5zm0a19h0mvmmpsadh1sprb1j";
in {
  makesSrc = builtins.fetchTarball {
    inherit sha256;
    url = "https://api.github.com/repos/fluidattacks/makes/tarball/${commit}";
  };
}
